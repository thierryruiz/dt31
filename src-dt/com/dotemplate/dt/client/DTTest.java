package com.dotemplate.dt.client;

import com.dotemplate.core.client.Client;
import com.dotemplate.core.client.frwk.CommandCallback;
import com.dotemplate.core.client.frwk.RPC;
import com.dotemplate.core.shared.command.GetSymbol;
import com.dotemplate.core.shared.command.GetSymbolResponse;
import com.dotemplate.core.shared.properties.PropertySet;
import com.dotemplate.theme.client.ThemeClient;
import com.dotemplate.theme.shared.Theme;
import com.dotemplate.theme.shared.ThemeUpdate;
import com.dotemplate.theme.shared.command.Export;
import com.dotemplate.theme.shared.command.ExportResponse;
import com.dotemplate.theme.shared.command.UpdateSet;
import com.dotemplate.theme.shared.command.UpdateSetResponse;
import com.google.gwt.dom.client.StyleInjector;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;



public class DTTest extends DT {

	
	@Override
	public void onModuleLoad() {		
		
		loadTheme() ;
		
		( new Timer() {
			@Override
			public void run() {
				updateBackground() ;
			}
		} ).schedule( 5000 ) ;
	
	}
	
	
	
	
	protected void updateBackground() {
	
		Theme theme = ThemeClient.getTheme() ;
		
		PropertySet wrapper = theme.getPropertySetMap().get( "wrapper" ) ;
		PropertySet wrapperBg =  ( PropertySet ) wrapper.get( "bg" ) ;
		final PropertySet wrapperBgFill =  ( PropertySet ) wrapperBg.get( "fill" ) ;
		
		
		// load symbol from server 
		GetSymbol getSymbol = new GetSymbol() ;
		getSymbol.setDesignUid( Client.get().getDesign().getUid() ) ;
		
		getSymbol.setName( "fill-texture-cement-1-fading-to-black" ) ;
		
		Client.get().getDesignService().execute( getSymbol , new CommandCallback< GetSymbolResponse >()  {
			
			public void onSuccess( GetSymbolResponse response ) {
				
				PropertySet symbol = response.getSymbol() ;
				
				wrapperBgFill.setParent ( symbol.getName () ) ;
				wrapperBgFill.setPath ( symbol.getPath () ) ;
				wrapperBgFill.setThumbnailHtml ( symbol.getThumbnailHtml () ) ;
				wrapperBgFill.setThumbnailUrl( symbol.getThumbnailUrl() ) ;
				wrapperBgFill.setFilter ( symbol.getFilter () ) ;
				wrapperBgFill.setIcon( symbol.getIcon () ) ;
				
								
				wrapperBgFill.setPropertyMap ( symbol.getPropertyMap () ) ;
				

				
				UpdateSet updateSet = new UpdateSet() ;
				
				updateSet.setDesignUid( ThemeClient.get().getDesign().getUid() ) ;
				
				updateSet.setThemePropertySet ( wrapperBgFill ) ;
				
				
				( new RPC< UpdateSetResponse >( ThemeClient.get().getDesignService() ) ).execute ( 
						updateSet, new CommandCallback< UpdateSetResponse >() {
					
					@Override
					public void onSuccess ( UpdateSetResponse response ) {

						final ThemeUpdate refresh = response.getClientRefresh () ;
						
						StyleInjector.inject( refresh.getCss() );
						
						
						( new Timer() {
							@Override
							public void run() {
								Export export = new Export() ;
								export.setDesignUid( getDesign().getUid() ) ;

								export.setCmsType ( "xhtml" ) ;
								export.setZip(true) ;
								
								getDesignService().execute( export, new CommandCallback< ExportResponse >() {
									@Override
									public void onSuccess ( ExportResponse response ) {
										Window.open( "http://localhost:8888/dt/createTheme?id=ppc", "_self", "" ) ;
									}
								}) ;
								
								
							}
						} ).schedule( 5000 ) ;

					}
				}) ;
			};
		
		}) ;
		
	}
	
	
	
	
}
