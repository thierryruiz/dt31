package com.dotemplate.dt.client.resources;

import com.dotemplate.theme.client.resources.ThemeImages;
import com.google.gwt.resources.client.ImageResource;



public interface DoTemplateImages extends ThemeImages {

	
	@Source("cart-32.png" )
	ImageResource cart32() ;

	@Source("export-blogger-32.png" )
	ImageResource exportBlogger32() ;
	
	@Source("export-html-32.png" )
	ImageResource exportHtml32() ;

	@Source("export-joomla-32.png" )
	ImageResource exportJoomla32() ;

	@Source("export-wp-32.png" )
	ImageResource exportWp32() ;	
	
	@Source("save-32.png" )
	ImageResource save32() ;
	  
	
}
