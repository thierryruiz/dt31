package com.dotemplate.dt.client.view;

import com.dotemplate.core.client.frwk.View;
import com.google.gwt.event.dom.client.HasClickHandlers;

public interface ConfirmPurchaseView extends View {

	HasClickHandlers getConfirmControl() ;
		
	void open() ;
	
	void close();
	
}
