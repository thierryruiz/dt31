package com.dotemplate.dt.client.view;

import com.dotemplate.core.client.frwk.View;
import com.google.gwt.event.dom.client.HasClickHandlers;


public interface DTControlView extends View {
	
	void hideBuyControl() ;
	
	HasClickHandlers enableBuyControl() ;
		
	HasClickHandlers enableExportHTMLControl() ;
	
	HasClickHandlers enableExportWordpressControl() ;
	
	HasClickHandlers enableSaveControl() ;

	void show( int left, int top ) ;

	
	
}
