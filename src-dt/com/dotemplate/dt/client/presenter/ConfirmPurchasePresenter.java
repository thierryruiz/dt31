package com.dotemplate.dt.client.presenter;

import com.dotemplate.core.client.frwk.AbstractPresenter;
import com.dotemplate.core.shared.EditService;
import com.dotemplate.core.shared.EditServiceAsync;
import com.dotemplate.dt.client.DT;
import com.dotemplate.dt.client.view.ConfirmPurchaseView;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;


public class ConfirmPurchasePresenter extends AbstractPresenter< ConfirmPurchaseView > {
	
	protected EditServiceAsync service ;
			
	public ConfirmPurchasePresenter( ConfirmPurchaseView view ) {
		super( view ) ;
		service = GWT.create( EditService.class ) ;
	}
	

	@Override
	public void bind() {
		
		view.getConfirmControl().addClickHandler( new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				view.close() ;
				buyTemplate() ;
			}
		} ) ;
	}
	
	
	public void display() {
		view.open() ;
	}
	
	
	public void buyTemplate() {
		
		DT.get().confirmClosing( false ) ;
		String baseUrl = GWT.getModuleBaseURL() ;
		baseUrl =  baseUrl.substring( 0, baseUrl.indexOf( "gwt" ) );
		Window.Location.replace ( baseUrl + "dt/buy" ) ;
	
	}

	
}



