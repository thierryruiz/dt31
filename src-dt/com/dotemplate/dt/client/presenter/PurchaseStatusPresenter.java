package com.dotemplate.dt.client.presenter;

import com.dotemplate.core.client.frwk.AbstractPresenter;
import com.dotemplate.core.shared.EditService;
import com.dotemplate.core.shared.EditServiceAsync;
import com.dotemplate.dt.client.view.PurchaseStatusView;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;


@Deprecated
public class PurchaseStatusPresenter extends AbstractPresenter< PurchaseStatusView > {
	
	protected EditServiceAsync service ;
			
	public PurchaseStatusPresenter( PurchaseStatusView view ) {
		super( view ) ;
		service = GWT.create( EditService.class ) ;
	}
	

	@Override
	public void bind() {
		
		view.getOkControl().addClickHandler( new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				view.close() ;
				
			}
		} ) ;
		
	}

	
	public void display( String uri ) {
		view.open( uri ) ;
	}
			

}



