package com.dotemplate.dt.client.presenter;

import com.dotemplate.core.client.frwk.AbstractPresenter;
import com.dotemplate.core.client.frwk.CommandCallback;
import com.dotemplate.core.client.frwk.RPC;
import com.dotemplate.core.shared.EditService;
import com.dotemplate.core.shared.EditServiceAsync;
import com.dotemplate.dt.client.DT;
import com.dotemplate.dt.client.view.ConvertView;
import com.dotemplate.theme.shared.command.Export;
import com.dotemplate.theme.shared.command.ExportResponse;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;



public class ConvertPresenter extends AbstractPresenter< ConvertView > {
	
	protected EditServiceAsync service ;
			
	public ConvertPresenter( ConvertView view ) {
		super( view ) ;
		service = GWT.create( EditService.class ) ;
	}
	

	@Override
	public void bind() {
		
		view.getBuyControl().addClickHandler( new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				view.close() ;
				buyTemplate() ;
				
			}
		} ) ;
		
		view.getDownloadControl().addClickHandler( new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				view.close() ;
				export() ;
			}
		} ) ;
	}

	
	public void display( String heading ) {
		view.open( heading ) ;
	}
	
	
	public void display( String heading, String export ) {
		view.open( heading, export ) ;
	}
	
	
	
	public void buyTemplate() {
		
		DT.get().confirmClosing( false ) ;
		String baseUrl = GWT.getModuleBaseURL() ;
		baseUrl =  baseUrl.substring( 0, baseUrl.indexOf( "gwt" ) );
		Window.Location.replace ( baseUrl + "dt/buy" ) ;
	
	}
	

	
	private void export( ){
	
		Export export = new Export() ;
		
		export.setDesignUid( DT.getTheme().getUid() ) ;
		export.setCmsType ( "xhtml" ) ;
		export.setZip( true ) ;
		
		
		new RPC< ExportResponse > ( service ).execute ( export, 
				new CommandCallback< ExportResponse >() {
			
			@Override
			public void onSuccess ( ExportResponse response ) {
				
				String baseUrl = GWT.getModuleBaseURL() ;
				baseUrl =  baseUrl.substring( 0, baseUrl.indexOf( "gwt" ) );
				
				String downloadUrl = 	baseUrl + "download/doTemplate-" 
						+ DT.getTheme().getUid() + ".zip" ;
				
				download( downloadUrl ) ;
				
			}
			
	
		} ) ;
	
		
	}



	private static native void download( String url ) /*-{
		location.href = url ;
	}-*/;

	

}



