package com.dotemplate.dt.client.presenter;


import com.dotemplate.core.client.frwk.CommandCallback;
import com.dotemplate.core.client.frwk.RPC;
import com.dotemplate.dt.client.DT;
import com.dotemplate.dt.shared.command.GetThemePermalink;
import com.dotemplate.dt.shared.command.GetThemePermalinkResponse;
import com.dotemplate.dt.shared.command.PurchaseStatus;
import com.dotemplate.dt.shared.command.PurchaseStatusResponse;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;


@Deprecated
class PurchaseStatusTimer {
	
	private Timer purchaseStatusTimer ;
	
	private PurchaseStatus purchaseStatus ;
	
	private int nbOfAttempts = 0 ;

	private int REPEAT_DELAY = 5000 ; // 5s
	
	private int MAX_ATTEMPTS = 12 * 5  ; // 5 minutes
	
	
	PurchaseStatusTimer() {
		
		purchaseStatus = new PurchaseStatus() ;
		
		purchaseStatusTimer = new Timer() {
			
			public void run() {
				
				if ( nbOfAttempts < MAX_ATTEMPTS ) {
					
					getPurchaseStatus() ;
				
				} else {
					
					cancel() ;
					
				}
			};
		};
		
		purchaseStatusTimer.scheduleRepeating( REPEAT_DELAY ) ;
		
	}


	protected void getPurchaseStatus() {
		
		purchaseStatus.setDesignUid( DT.get().getDesign().getUid() ) ;
		
		new RPC< PurchaseStatusResponse >( DT.get().getDesignService() ).execute ( 
				purchaseStatus, 
				new CommandCallback < PurchaseStatusResponse >() {
			
			@Override
			public void onSuccess ( PurchaseStatusResponse response ) {
				
				if( response.getStatus() == 1 ){
					
					paymentSuccess() ;
					
				} else {
					
					nbOfAttempts++ ;
				
				}
			}
			
			
			@Override
			public void onFailure(Throwable oops) {
				purchaseStatusTimer.cancel() ;
			}
			
			
			
		}, null, false ) ;
		
	}



	
	protected void paymentSuccess() {
		
		purchaseStatusTimer.cancel() ;
		
		// automatic reload of editor with purchased theme so disable closing handler
		DT.get().confirmClosing( false ) ;
		
		GetThemePermalink getThemePermalink = new GetThemePermalink() ;
		getThemePermalink.setDesignUid( DT.get().getDesign().getUid() ) ;
		
		new RPC< GetThemePermalinkResponse > ( DT.get().getDesignService() ).execute (
				getThemePermalink,
				new CommandCallback< GetThemePermalinkResponse > ( ){
					
					public void onSuccess(GetThemePermalinkResponse result) {
						Window.Location.replace( result.getPermalink() ); 
					};
					
				}
		) ;
		
	}
	

		
}
