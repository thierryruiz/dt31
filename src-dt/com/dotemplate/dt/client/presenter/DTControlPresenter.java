package com.dotemplate.dt.client.presenter;


import com.dotemplate.core.client.frwk.AbstractPresenter;
import com.dotemplate.core.client.frwk.CommandCallback;
import com.dotemplate.core.client.frwk.RPC;
import com.dotemplate.core.client.frwk.Utils;
import com.dotemplate.core.shared.EditService;
import com.dotemplate.core.shared.EditServiceAsync;
import com.dotemplate.dt.client.DT;
import com.dotemplate.dt.client.view.DTControlView;
import com.dotemplate.theme.shared.Theme;
import com.dotemplate.theme.shared.command.Export;
import com.dotemplate.theme.shared.command.ExportResponse;
import com.dotemplate.theme.shared.command.SaveTheme;
import com.dotemplate.theme.shared.command.SaveThemeResponse;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;



public class DTControlPresenter extends AbstractPresenter< DTControlView > {

	protected EditServiceAsync service ;
		
	
	public DTControlPresenter( DTControlView view ) {

		super( view );
		service = GWT.create( EditService.class ) ;
		
	}

	
		
	@Override
	public void bind() {
		
		Theme theme = DT.getTheme() ;
		
		if ( theme.isPurchased() ){
		
			view.hideBuyControl() ;
			
			view.enableSaveControl().addClickHandler(  new ClickHandler() {
				@Override
				public void onClick( ClickEvent event ) {
					save() ;
				}
			} ) ;
		
			
			view.enableExportHTMLControl().addClickHandler(  new ClickHandler() {
				@Override
				public void onClick( ClickEvent event ) {
					export( "xhtml" ) ;
				}
			} ) ;
			
		
			view.enableExportWordpressControl().addClickHandler(  new ClickHandler() {
				@Override
				public void onClick( ClickEvent event ) {
					export( "wp" ) ;
				}
			} ) ;
		
			
		} else {
			
			if ( !theme.isPremium() ){
				
				view.enableSaveControl().addClickHandler(  new ClickHandler() {
					@Override
					public void onClick( ClickEvent event ) {
						save() ;
					}
				} ) ;
			
				
				view.enableExportHTMLControl().addClickHandler(  new ClickHandler() {
					@Override
					public void onClick( ClickEvent event ) {
						export( "xhtml" ) ;
					}
				} ) ;
				
			
				view.enableExportWordpressControl().addClickHandler(  new ClickHandler() {
					@Override
					public void onClick( ClickEvent event ) {
						export( "wp" ) ;
					}
				} ) ;

			} else {
				
				view.enableSaveControl().addClickHandler(  new ClickHandler() {
					@Override
					public void onClick( ClickEvent event ) {
						callToAction();
					}
				} ) ;

				
				view.enableExportHTMLControl().addClickHandler(  new ClickHandler() {
					@Override
					public void onClick( ClickEvent event ) {
						callToAction();
					}
				} ) ;
				
			
				view.enableExportWordpressControl().addClickHandler(  new ClickHandler() {
					@Override
					public void onClick( ClickEvent event ) {
						callToAction();
					}
				} ) ;
				
			}
			
			
			
			view.enableBuyControl().addClickHandler(  new ClickHandler() {
				@Override
				public void onClick( ClickEvent event ) {
					buy() ;
				}
			} ) ;

		}
		
	}
	
	
	
	
	
	
	protected void save() {
		
		Theme theme = DT.getTheme() ;
		
		if ( !theme.isPurchased() && !theme.isPremium() ){
			
			callToAction();
			return ;
			
		} 
			
		
		SaveTheme saveTheme = new SaveTheme() ;
		saveTheme.setDesignUid( DT.getTheme().getUid() ) ;
		
		
		new RPC< SaveThemeResponse > ( service ).execute ( saveTheme, 
				new CommandCallback< SaveThemeResponse >() {
			
			@Override
			public void onSuccess ( SaveThemeResponse response ) {
				Utils.alert( "Theme saved." ) ;			
			}
			
		} ) ;		
	}


	
	public void buy() {
		
		Theme theme = DT.getTheme() ;
		if ( !theme.isPremium() && !theme.isPurchased() ){
			
			DT.get().getConvertPresenter().display( 
				"Buy this template for <span>$9</span> only and get much more..." )  ;
			
		} else {
			DT.get().getConfirmPurchasePresenter().display() ;
		
		}
		
	}
	
	
	private void callToAction() {
		DT.get().getConvertPresenter().display( 
				"Purchase this template for <span>$9</span> only and get much more..." )  ;
	}


	
	public void display() {
		view.show( 0, 160  ) ;
	}


	
	public void export( String cms ) {
				

		/*
		String downloadMessage = "Creating template zip, please wait..." ;
		if ( "blogger".equals ( cmsType ) ){
			downloadMessage = "Creating zip and uploading images to Picasa, please wait..." ;
		}*/
	
		Theme theme = DT.getTheme() ;
		
			
		// theme is premium and not purchased
		if (  theme.isPremium() && !theme.isPurchased() ){
			DT.get().getConvertPresenter().display( "Download this template for <span>$9</span> only...", cms )  ;
			return ;			
		}

		
		// or theme is free and wp download is asked   
		if (  "wp".equals( cms ) && !theme.isPremium() && !theme.isPurchased() ){
			DT.get().getConvertPresenter().display(  "Download for Wordpress for <span>$9</span> only and get much more...", cms )  ;
			return ;			
		}
		
		
		if ( !theme.isPremium() && !theme.isPurchased() ){
			DT.get().getConvertPresenter().display(  "Buy this template for <span>$9</span> only and get much more...", cms )  ;
			return ;
		}
		
		
		Export export = new Export() ;
		
		export.setDesignUid( DT.getTheme().getUid() ) ;
		export.setCmsType ( cms ) ;
		export.setZip( true ) ;
		
		
		new RPC< ExportResponse > ( service ).execute ( export, 
				new CommandCallback< ExportResponse >() {
			
			@Override
			public void onSuccess ( ExportResponse response ) {
				
				String baseUrl = GWT.getModuleBaseURL() ;
				baseUrl =  baseUrl.substring( 0, baseUrl.indexOf( "gwt" ) );
				
				String downloadUrl = 	baseUrl + "download/doTemplate-" 
						+ DT.getTheme().getUid() + ".zip" ;
				
				download( downloadUrl ) ;
				
			}
			
	
		} ) ;

		
	}
	
	private static native void download( String url ) /*-{
		location.href = url ;
	}-*/;
	

		
	private static native void openPurchaseDialog( String url ) /*-{
		purchaseWin = window.open( url, "buyTemplateWindow", 
			"toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=1000, height=700" );
		$wnd.purchaseWin = purchaseWin ;
				
		purchaseWin.focus();
		purchaseWin.moveTo( 50, 50 );
		purchaseWin.resizeTo( 1000, 800 );

	}-*/;



	
	
	

	
	
	
	
	
	
}
