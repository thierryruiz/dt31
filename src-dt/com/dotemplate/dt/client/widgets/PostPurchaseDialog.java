package com.dotemplate.dt.client.widgets;

import com.dotemplate.core.client.widgets.Window;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Frame;


public class PostPurchaseDialog extends Window {
	
	private static PostPurchaseDialog _instance ;
	
	private FlowPanel root = new FlowPanel();
	
	
	private PostPurchaseDialog() {
		setSize( 460, 430 ) ;
		setHeading ( "Thank you for purchasing" ) ;
		build() ;
	}


    public static PostPurchaseDialog get() {
    	
    	if ( _instance == null ){
    		_instance = new PostPurchaseDialog() ;
    	}
    	
    	return _instance ;
    }

    
	private void build () {
		add (  root ) ;
		
		Frame frame = new Frame() ; 
		frame.setWidth ( "100%" ) ;
		
		// due to FF 3 issue, by default display=none for gwt-Frame class
		
		frame.setUrl ( "dt/download" ) ;
		frame.setStyleName ( "ez-dwld-frame" ) ;
		root.add ( frame ) ;
	
	}
	
	
}
