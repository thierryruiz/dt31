package com.dotemplate.dt.client;

import com.dotemplate.core.client.ClosingEditorHandler;
import com.dotemplate.core.client.widgets.Tip;
import com.dotemplate.dt.client.presenter.ConfirmPurchasePresenter;
import com.dotemplate.dt.client.presenter.ConvertPresenter;
import com.dotemplate.dt.client.presenter.DTControlPresenter;
import com.dotemplate.dt.client.ui.ConfirmPurchaseViewImpl;
import com.dotemplate.dt.client.ui.ConvertDialog;
import com.dotemplate.dt.client.ui.DTControlPanel;
import com.dotemplate.theme.client.ThemeClient;
import com.dotemplate.theme.shared.Theme;


public class DT extends ThemeClient {
	
	private DTControlPresenter dtControlPresenter ;
	
	private ConvertPresenter convertPresenter ;
	
	private ConfirmPurchasePresenter confirmPurchasePresenter ;
	
	private ClosingEditorHandler closingHandler ;
	
	
	
	@Override
	public void onModuleLoad() {

		loadTheme();

		exportNativeMethods();
		
		closingHandler = new ClosingEditorHandler() ;

	}

	
	
	public static DT get() {
		return ( DT ) instance ;
	}
	
	
	
	@Override
	protected void onThemeLoaded(Theme theme) {

		super.onThemeLoaded( theme );

		dtControlPresenter = new DTControlPresenter( new DTControlPanel() ) ;
		
		dtControlPresenter.display() ;
		
		// showTip() ;
		
	}

	
	

	@SuppressWarnings("unused")
	private void showTip() {
		
		String address = "thierry.ruiz" + "" + "@" + "gmail.com";

		String tip = "<p><b>Beta preview</b> of the new editor.</p><p>Please report us comments and bugs:<br/>"
				+ address + "</p>";

		new Tip( tip, null );
	
		
	}

	
	public ConfirmPurchasePresenter getConfirmPurchasePresenter() {
		
		if ( confirmPurchasePresenter == null ){
			confirmPurchasePresenter = new ConfirmPurchasePresenter( new ConfirmPurchaseViewImpl() ) ;
		}
		
		return confirmPurchasePresenter ;
	
	}
	
	
	public ConvertPresenter getConvertPresenter() {
		
		if ( convertPresenter == null ){
			convertPresenter = new ConvertPresenter( new ConvertDialog() ) ;
		}
		
		return convertPresenter ;
	
	}
	
	
	public void confirmClosing( boolean confirm ){
		closingHandler.confirmClosing( confirm ) ;
	}



	@Override
	public void invitePurchase() {
		getConvertPresenter().display( "Buy template" ) ;
	}

}
