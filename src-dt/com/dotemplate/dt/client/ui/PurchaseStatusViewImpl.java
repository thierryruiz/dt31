package com.dotemplate.dt.client.ui;

import com.dotemplate.core.client.widgets.Window;
import com.dotemplate.dt.client.view.PurchaseStatusView;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;


@Deprecated
public class PurchaseStatusViewImpl implements PurchaseStatusView {
	
	private Window win ; 
		
	private FlowPanel layout ;
	
	private Image editingImage ;
	
	private Image downloadImage ;
	
	private HTML editUrl = new HTML() ; 

	@SuppressWarnings("unused")
	private static String _HTML =
			"<h4>Thansks for purchasing</h4>" ;
	
	
	public PurchaseStatusViewImpl() {
		
		
		win = new Window() ;

		win.setSize( 940, 740 ) ;
		win.setHeading ( "Get more" ) ;
		
		layout = new FlowPanel() ;
		win.add( layout ) ;	


		HTML text = new HTML() ;
		
					
		layout.add( text ) ;
		layout.add( editUrl ) ;
		
		editingImage = new Image() ;
		
		downloadImage = new Image() ;
		
		
	}


	
	
	@Override
	public String getId() {
		return "dtConvertDialog" ;
	}

	@Override
	public Widget asWidget() {
		return win;
	}

	@Override
	public HasClickHandlers getOkControl() {
		return downloadImage ;
	}
	
	
	@Override
	public void open( String uri ) {
		
		win.openCenter() ;

		//RootPanel.get( "premiumHeading" ).getElement().setInnerHTML( heading ) ;

		if ( editingImage.isAttached() ){
			editingImage.removeFromParent() ;
		}

		
		if ( downloadImage.isAttached() ){
			downloadImage.removeFromParent() ;
		}

		
		editUrl.setHTML(uri) ;
		
		//editingImage.setUrl("images/dotemplate/buy.png") ;
		//RootPanel.get( "buyBtn" ).add( editingImage  ) ;
						
		
	}

	
	
	

	@Override
	public void close() {
		win.close() ;
		
	}







}
