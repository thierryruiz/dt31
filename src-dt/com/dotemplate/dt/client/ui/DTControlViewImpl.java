package com.dotemplate.dt.client.ui;


import com.dotemplate.core.client.frwk.Utils;
import com.dotemplate.core.client.widgets.DeprecatedWindow;
import com.dotemplate.core.client.widgets.Window;
import com.dotemplate.dt.client.DT;
import com.dotemplate.dt.client.resources.DoTemplateImages;
import com.dotemplate.dt.client.view.DTControlView;
import com.dotemplate.theme.shared.Theme;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;


@Deprecated
public class DTControlViewImpl implements DTControlView {

	private DoTemplateImages IMAGES = GWT.create( DoTemplateImages.class ) ;
	
	private DeprecatedWindow innerWnd ;
	
	private FlowPanel icons ;

	private CommandButton buyButton ;
	
	private CommandButton saveButton ;
	
	private CommandButton exportHTMLButton ;
	
	private CommandButton exportWPButton ;
	
	
	public DTControlViewImpl(){
		
		innerWnd = new DeprecatedWindow();
				
		innerWnd.setClosable( false ) ;
		innerWnd.setResizable( false ) ;
		innerWnd.setCollapsible( false ) ;
		innerWnd.setSize( 84, 600 );
		
		icons = new FlowPanel() ;
		Utils.setId ( icons, getId() ) ;

		innerWnd.add( icons ) ;
		innerWnd.setId( "commandPanel" ) ;
		

		buyButton = new CommandButton( IMAGES.cart32(), "Buy template", 
				"Purchase your template" ) ;
		
		saveButton = new CommandButton( IMAGES.save32(), "Save template", 
			"Save your template" ) ; 
	
		
		
		Theme theme = DT.getTheme() ; 
		
		String lbl = ( theme.isPremium() || ( !theme.isPremium() && theme.isPurchased() ) ) ?
				"Download as HTML" : "Download as HTML (free)" ;
		
		
		exportHTMLButton = new CommandButton( IMAGES.exportHtml32(), lbl, 
				"Download your template as a standard HTML file" ) ; 

		
		exportWPButton = new CommandButton( IMAGES.exportWp32(), "Download for Wordpress", 
					"Download your template for Wordpress 3" ) ; 
		
		
		icons.add( buyButton ) ;
		icons.add( saveButton ) ;
		icons.add( exportHTMLButton ) ;
		icons.add( exportWPButton ) ;
		
		
		
	}
	
	
	@Override
	public String getId() {
		return "dtControlPanel" ;
	}
	
	
	public void show( int left, int top ){
		innerWnd.setPosition(left, top) ;
		innerWnd.open() ;
		innerWnd.setFixedTopPosition( top ) ;
	}

	
	
	@Override
	public Widget asWidget() {
		return innerWnd.asWidget() ;
	}
	

	@Override
	public HasClickHandlers enableBuyControl() {
		buyButton.setEnabled( true );
		return buyButton ;
	}


	@Override
	public HasClickHandlers enableExportHTMLControl() {
		exportHTMLButton.setEnabled( true ) ;
		return exportHTMLButton ;
	}


	@Override
	public HasClickHandlers enableExportWordpressControl() {
		exportWPButton.setEnabled( true ) ;
		return exportWPButton ;
	}


	@Override
	public HasClickHandlers enableSaveControl() {
		saveButton.setEnabled( true ) ;
		return saveButton ;
	}

	
	@Override
	public void hideBuyControl() {
		buyButton.setVisible( false );
	}
	
	
	
	
	
	private class CommandButton extends FlowPanel implements HasClickHandlers {
		
		private Image icon ;
		
		private boolean enabled = true ;
				
		public CommandButton( ImageResource image, String txt, String title ) {

			add ( icon = new Image ( image ) ) ;
			add ( new HTML ( txt ) ) ;
			
			setTitle( title ) ;
			setEnabled( false ) ;
			
		}

		
		public void setTitle( String title ){
			icon.setTitle( title ) ;
		}
		
		
		@Override
		public HandlerRegistration addClickHandler( ClickHandler handler ) {
			return icon.addClickHandler( handler ) ;
		}
		
		
		
		protected void setEnabled ( boolean e ){
			
			if ( enabled == e ) return ;
			
			enabled = e ;
			
			if ( !enabled ) {
			
				addStyleName( "disabled" ) ;
			
			} else {
				
				removeStyleName("disabled" ) ;
			}
		
		}
		
	}






	
	

}
