package com.dotemplate.dt.client.ui;

import com.dotemplate.core.client.frwk.Utils;
import com.dotemplate.core.client.widgets.Window;
import com.dotemplate.dt.client.view.ConvertView;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;

public class ConvertDialog implements ConvertView {
	
	private Window win ; 
		
	private FlowPanel layout ;
	
	private HTML heading ;
	
	private HorizontalPanel footer ;
	
	private Image buyImage ;
	
	private Image downloadImage ;

	private FlowPanel buyImageCtnr ;
	
	private FlowPanel downloadImageCtnr ;
	
	
	private static String _HTML = 
			"<div><ul class=\"ez-features\">" +
			"<li><div><h4>Save your work</h4>You can edit your template after payment at anytime and rework it to build the best Web design.</div><img src=\"images/dotemplate/save.png\"/></li>" +
			"<li><div><h4 class='highlight'>Commercial use allowed</h4>Purchased templates can be used for a commercial Website. No backlink is required on footer as it is for free templates.</div><img src=\"images/dotemplate/dollar.png\"/></li>" +
			"<li><div><h4 class='highlight'>Responsive design</h4>Your template will look great on all devices, smartphones, tablets and wide screens.</div><img src=\"images/dotemplate/responsive.png\"/></li>" +
			"<li><div><h4>Email support</h4>We wil assist you to code your website from this template, whatever your question is let us help!</div><img src=\"images/dotemplate/support.png\"/></li>" +			
			"<li><div><h4>Premium graphics</h4>Use hundreds of premium graphics, backgrounds, styled text, professionnal photos... and create your very unique design.</div><img src=\"images/dotemplate/graphics.png\"/></li>" +
			"<li><div><h4>One-click color schemes</h4>A choice color schemes you can apply in one click on your design.</div><img src=\"images/dotemplate/schemes.png\"/></li>" +
			"</ul>" +
			"<ul class=\"ez-features\" style=\"margin-left:40px\">" +
			"<li><div><h4>Premium fonts</h4>Use stylish fonts from a growing library of Google fonts.</div><img src=\"images/dotemplate/font.png\"/></li>" +			
			"<li><div><h4>Page layouts</h4>Your custom template is bundled with multiple page layouts with and without sidebars.</div><img src=\"images/dotemplate/layout.png\"/></li>" +
			"<li><div><h4>Pre made page samples</h4>Speed up your Website building with pre-coded pages (home page, contact form, image gallery...).</div><img src=\"images/dotemplate/sitemap.png\"/></li>" +
			"<li><div><h4>Drop down navigation menu</h4>Get a pure CSS dropdown menu which clean HTML code can be easily adapted.</div><img src=\"images/dotemplate/ddmenu.png\"/></li>" +
			"<li><div><h4>Stylish design elements</h4>Speed up your development using pre coded HTML design elements (styled buttons, tables, columns...).</div><img src=\"images/dotemplate/elements.png\"/></li>" +
			"<li><div><h4>Wordpress export</h4>Download your template as a Wordress theme, and create your custom Wordpress website in minutes.</div><img src=\"images/dotemplate/wp.png\"/></li>" +
			"</ul></div><div style=\"clear:both\"></div>" ;
			
			//"<ul class=\"buttons-ctnr\">"
			//"<li class=\"button\"><div id=\"buyBtn\"></div><div style=\"font-size:0.8em;margin-top:0\"><i>Secured with PayPal - No PayPal account needed</i></div></li><li class=\"button\"><div id=\"downloadBtn\"></div></li></ul>" ;
	
	
	public ConvertDialog() {
		
		win = new Window() ;
		win.setSize( 940, 780 ) ;
		
		win.add( layout = new FlowPanel() ) ;
		
		layout.addStyleName ( "ez-dialog" ) ;
		
		HTML features ;
		layout.add( heading = new HTML() ) ;
		layout.add(  features = new HTML( _HTML ) ) ;
		Utils.margins( features, "15px auto" ) ;
		
		layout.add( footer = new HorizontalPanel() ) ;
		Utils.margins( footer, "15px auto" ) ;

			
		footer.addStyleName( "buttons-ctnr" ) ;
		
		HTML paypalTip = new HTML("<div style=\"font-size:0.9em;margin-top:0\">Secured with PayPal - No PayPal account needed</div>" ) ;  

		buyImageCtnr = new FlowPanel() ;
		downloadImageCtnr = new FlowPanel() ;

		buyImageCtnr.add( buyImage = new Image("images/dotemplate/buy.png" ) ) ;	
		buyImageCtnr.add( paypalTip ) ;	
		Utils.style(buyImage, "cursor", "pointer") ;
		
		downloadImageCtnr.add( downloadImage = new Image( "images/dotemplate/download.png" ) ) ;
		
		features.setWidth( "840px" ) ;
		
	}


	
	
	@Override
	public String getId() {
		return "dtConvertDialog" ;
	}

	@Override
	public Widget asWidget() {
		return win;
	}

	@Override
	public HasClickHandlers getDownloadControl() {
		return downloadImage ;
	}
	
	@Override
	public HasClickHandlers getBuyControl() {
		return buyImage ;
	}
	

	
	@Override
	public void open( final String headingHtml ) {
		
		footer.clear() ;
		
		win.openCenter() ;

		heading.setHTML( "<h4>" + headingHtml + "</h4>" ) ;
	
		footer.add( buyImageCtnr  );
		
		
	}

	
	
	@Override
	public void open( String heading, String export ) {
		
		open( heading ) ;
	
		footer.add( downloadImageCtnr  );
		
	}
	
	

	@Override
	public void close() {
		win.close() ;
		
	}







}
