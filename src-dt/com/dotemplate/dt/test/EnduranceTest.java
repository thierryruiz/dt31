package com.dotemplate.dt.test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import com.dotemplate.theme.test.ThemeTest;

public class EnduranceTest {
	

	public static void main( String args [] ){
		new EnduranceTest().run() ;
	}


	private void run() {
		
		ExecutorService executor = Executors.newFixedThreadPool( 20 ) ;
		
		ArrayList< ThemeTest > themeTests = new ArrayList<ThemeTest>() ;
		
		String url = "ns225144.ovh.net:8180/dt2" ;
		
		
		ThemeTest test1 = new ThemeTest( url, "ppc" ){
			@Override
			protected void run() {
				NEW_THEME  		() ;
				SELECT_TAB   	( "Background" ) ;
				CHANGE_SYMBOL	( "wrapper_bg_fill" , "fill-vgradient-green" ) ;	
				CHANGE_SYMBOL	( "wrapper_bg_fill" , "fill-texture-cement-1-fading-to-black" ) ;
			}
		};
		
		ThemeTest test2 = new ThemeTest( url, "ppc" ){
			@Override
			protected void run() {
				NEW_THEME  		() ;
				SELECT_TAB   	( "Background" ) ;
				CHANGE_SYMBOL	( "wrapper_bg_fill" , "fill-vgradient-green" ) ;	
				CHANGE_SYMBOL	( "wrapper_bg_fill" , "fill-texture-cement-1-fading-to-black" ) ;
			}
		};
		

		
		themeTests.add( test1 ) ;
		themeTests.add( test2 ) ;
		
		
		
		
		
		while ( true ){
			
			try {
				List<Future<Void>> results = executor.invokeAll ( themeTests ) ;

				for ( Future<Void> future : results ){
					future.get () ; 
				}
			
			
			} catch ( Exception e) {
				e.printStackTrace() ;
				break ;
			}
			
		}
		
		
	}
	
	
	
	
	
	
	
	

}
