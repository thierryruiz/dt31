package com.dotemplate.dt.server.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.PurchaseInfo;
import com.dotemplate.core.server.PurchaseInfoHelper;
import com.dotemplate.core.server.frwk.AppException;
import com.dotemplate.core.server.web.BaseHttpServlet;
import com.dotemplate.theme.server.ThemeApp;

public class DownloadPremiumResource extends BaseHttpServlet {
	
	private static final long serialVersionUID = 4327063084622004640L;

	private static Log log = LogFactory.getLog ( DownloadPremiumResource.class );
	
	public final static String URL_MAPPING = "dt/dwldprem" ;
	
	protected void doGet( HttpServletRequest request, HttpServletResponse response )
			throws ServletException, IOException {
		
		String uid = request.getParameter( "uid" ) ;
		String pack = request.getParameter( "pack" ) ;
		
		
		if ( uid == null || uid.length() == 0 || pack == null || pack.length() == 0 ){
			forward404( request, response ) ;
			return ;
		}
		
		PurchaseInfo purchaseInfo;
		
		try {
			
			purchaseInfo = PurchaseInfoHelper.load ( uid );
			
		} catch ( AppException e ) {
			
			log.warn ( "Error on  download resource attempt uid=" + uid + ", pack=" + pack   ) ;
			forwardUnexpectedError( request, response ) ;
			return ;
			
		}
		
		if ( purchaseInfo == null ) {
			
			log.warn( "Error on  download resource attempt uid=" + uid + ", pack=" + pack + " No purchase info found" ) ;
			forward404( request, response ) ;
			
		}
		
		String downloadUrl = getBaseUrl( request ) + "downloadres/" + pack ;
		
		request.setAttribute( "res", downloadUrl ) ;
		
		forwardTo( "downloadres.jsp", request, response ) ;
			
	}
	
	
	
}
