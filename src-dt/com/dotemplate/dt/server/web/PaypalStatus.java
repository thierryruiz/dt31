package com.dotemplate.dt.server.web;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.DesignSession;
import com.dotemplate.core.server.web.BaseHttpServlet;
import com.dotemplate.dt.server.checkout.paypal.IPNServlet;
import com.dotemplate.theme.server.ThemeApp;


public  class PaypalStatus extends BaseHttpServlet {
	
	private static final long serialVersionUID = -3761303212178158024L;

	private static Log log = LogFactory.getLog( PaypalStatus.class );
	
	
	
	@Override
	protected void doPost( HttpServletRequest request, HttpServletResponse response )
			throws ServletException, IOException {

		log.info( "PaypalStatus invocation..." ) ;

		if (  !checkDesignSession( request, response, false ) ) {
			
			try {
				
				response.setContentType( "text/plain" ) ;
				response.setHeader( "Content-Length", "KO".getBytes().length + "" ) ;
				response.setStatus( HttpServletResponse.SC_OK ) ;
				response.getWriter().write( "KO" ) ;
				response.getWriter().flush() ;

			} catch ( Exception e ) {
				
				log.error( "Design session check invalid. Failed to respond back to IPN servlet", e ) ;
				e.printStackTrace();
				
			} finally {
				
				try {
					
					response.getWriter().close() ;
					
				} catch ( Exception e ){
					e.printStackTrace();
				}

			}
			
			return ;
		}
		
		String origin = request.getRemoteHost() ;
		
		log( request ) ;
		
		nocache( response ) ;
	
		// check request is coming from local host IPNServlet
		if ( ! "localhost".equals( origin ) && ! "127.0.0.1".equals( origin ) ){
			response.setStatus( HttpServletResponse.SC_FORBIDDEN ) ;
			return ;
		}
		
		
		// check secret shared token
		if ( IPNServlet.SECURITY_TOKEN.equals( request.getParameter( "token" ) ) ){
			// TODO check security token
		}
		
		boolean paymentOk = "1".equals( request.getParameter( "s" ) ) ;
		
		
		String themeUid = request.getParameter( "themeUid" ) ;
		String txnId = request.getParameter( "txn_id" ) ;
		String payerEmail = request.getParameter( "payer_email" ) ;
		
		if ( log.isDebugEnabled () ){
			log.debug ( "Get DesignSession from HTTP session" ) ;
		}
		
		DesignSession designSession = ( DesignSession ) request.getSession( false ).getAttribute ( 
				DesignSession.class.getName () ) ;
		
		
		if ( designSession == null ){
			log.error( "No designSession in httpSession !" ) ;
			return ;
		}

		
		try {
			
			if( paymentOk ){
				
				log.info( themeUid + " Purchased " ) ;
				
				DesignSession.set ( designSession ) ;
				
				ThemeApp.getThemeManager().onDesignPurchased( ThemeApp.getTheme(), 
						"paypal", txnId, payerEmail ) ;
				
			}
			
			
			response.setContentType( "text/plain" ) ;
			response.setHeader( "Content-Length", "OK".getBytes().length + "" ) ;
			response.setStatus( HttpServletResponse.SC_OK ) ;
			response.getWriter().write( "OK" ) ;
			response.getWriter().flush() ;

		
		} catch ( Exception e ) {
			
			log.error( "Failed to respond back to IPN servlet", e ) ;
			e.printStackTrace();
			
		} finally {
			
			try {
				
				response.getWriter().close() ;
				
			} catch ( Exception e ){
				e.printStackTrace();
			}

		}
		
	}
	
	
	@SuppressWarnings("unchecked")
	public  void log( HttpServletRequest req ) {

     	StringBuffer sb = new StringBuffer ()
                .append("\n\n\n\n=========  INCOMING REQUEST =========================================================\n" )
                .append( "\trequest url : ")
                .append( req.getRequestURL () )                    
                .append( "\n")
                
                .append( "\trequest method : ")
                .append( req.getMethod () )
                .append( "\n")

                .append( "\trequest path info : ")
                .append( req.getPathInfo () )  
                .append( "\n")
                .append( "\trequest uri : ")
                .append( req.getRequestURI () )
                .append( "\n")
                .append( "\trequest query : ")
                .append( req.getQueryString() )
                .append( "\n")                    
                .append( "\trequested sessionId : ")          
                .append( req.getRequestedSessionId () )
    	 		.append( "\n") ;
                
        	String key ;
            for ( Enumeration<String> e = req.getParameterNames ()  ; e.hasMoreElements () ;) {
            	sb.append ( "\t\t").append( key =  e.nextElement () ).append("=").append( 
            			req.getParameter ( key ) ).append("\n") ;
            }
	            
            System.out.println ( sb.toString()  ) ;     
	    	  
        } 
	}

