package com.dotemplate.dt.server.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.web.BaseHttpServlet;
import com.dotemplate.dt.server.DoTemplateApp;
import com.dotemplate.dt.server.DoTemplateConfig;
import com.dotemplate.dt.server.checkout.PaypalContext;


public class BuyTemplate extends BaseHttpServlet {


	private static final long serialVersionUID = 4765976725427297100L;

	
	@SuppressWarnings("unused")
	private static Log log = LogFactory.getLog( BuyTemplate.class );

	
	
	@Override
	protected void doGet( HttpServletRequest request, HttpServletResponse response )
			throws ServletException, IOException {
		
		if (  !checkDesignSession( request, response, true ) ) {
			return ;
		}
		
		nocache( response ) ;
	
		setPayPalContext( request  ) ;
		// set ClickBankConfig() ;
		
		super.forwardTo ( "buy.jsp" , request, response ) ;
		
		
	}
	
		
	/*
	 * The same code works both with real Paypal Sandbox paypal test environment
	 * 
	 * 
	 * PAYPAL
	 * ------
	 * Sandbox account 	: 	darooze@gmail.com / ...pop...
	 * Sandbox buyer 	:	darooz_1225205557_per@gmail.com / saysayb... 
	 * Sandbox seller 	:	darooz_1225206297_biz@gmail.com / saysayb...
	 * 
	 * Email test1		: 	saysayboomak@yahoo.com / yes....
	 * Email test2		: 	darooze@hotmai.com / saysayb...
	 * 
	 * SMPT account 	: 	dotemplate@gmail.com  / saisa...
	 * 
	 * 
	 * 
	 * 
	 */
	public void setPayPalContext( HttpServletRequest request ){
		
		DoTemplateConfig config = ( DoTemplateConfig ) DoTemplateApp.getConfig() ;
		
		String notifyUrl = config.getPaypalNotifyUrl() ;
		
		//....;jsessionid=%JSESSION_ID%?uid=%THEME_UID%&port=%CALLBACK_PORT%
				
		String sessionId = request.getSession( false ).getId() ;
		
		notifyUrl = StringUtils.replace( notifyUrl ,"%JSESSION_ID%" , sessionId ) ;
		notifyUrl = StringUtils.replace( notifyUrl ,"%THEME_UID%" , DoTemplateApp.getDesignUid() ) ;
		notifyUrl = StringUtils.replace( notifyUrl ,"%CALLBACK_PORT%" , "" + getPort( sessionId ) ) ;

		
		PaypalContext pc = new PaypalContext() ;
		
		pc.setUrl( config.getPaypalUrl() ) ;
		pc.setReturnUrl( config.getPaypalReturnUrl() );
		pc.setNotifyUrl( notifyUrl ) ;
		pc.setButtonId( config.getPaypalButtonId() ) ;
		
		request.setAttribute( "paypal" , pc ) ;
	
	}
	
	
	
	public String getPort( String sessionId ){
		
		DoTemplateConfig config =  ( DoTemplateConfig ) DoTemplateApp.getConfig() ;
		
		String ports = config.getPorts() ;
		
		String[] instances = StringUtils.split( ports, "," ) ;
		
		if ( instances.length == 0 ){
			return ports ; // 8888 
		}
		
		/// xxxxxx.tomcatN
		String activeInstance = StringUtils.substringAfter( sessionId, "." ) ;
		
		for ( String instance : instances ){
			
			if( activeInstance.equals( StringUtils.substringBefore( instance, ":" ) ) ){
				
				return StringUtils.substringAfter( instance, ":" );
			
			}
		}
		
		return ports ;
		
	}
	
	
	
	
	
	
	
	
	
	

}
