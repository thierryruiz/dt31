package com.dotemplate.dt.server.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.PurchaseInfoHelper;
import com.dotemplate.core.server.frwk.AppException;
import com.dotemplate.core.server.web.BaseHttpServlet;
import com.dotemplate.theme.server.ThemeApp;
import com.dotemplate.theme.server.ThemeUtils;
import com.dotemplate.theme.server.web.EditTheme;
import com.dotemplate.theme.shared.Theme;


public class PaypalPostPayment extends BaseHttpServlet {
	
	private static final long serialVersionUID = -2513758550307757475L;

	private static Log log = LogFactory.getLog( PaypalPostPayment.class );
	
	
	@Override
	protected void doGet( HttpServletRequest request, HttpServletResponse response )
			throws ServletException, IOException {
		
		log.info( "After payment processing." ) ;
		
		if (  !checkDesignSession( request, response, true ) ) {
			log.error( "Session expired just after payment !!" ) ;
			return ;
		}
		
		nocache( response ) ;
		
		Theme theme = ThemeApp.getTheme() ;
		
		
		if( theme.isPurchased() ){
			
			log.info( "Theme has been purchased. Display thank you page" ) ;
			
			
			String payerEmail = null ;
			
			try {

				payerEmail = PurchaseInfoHelper.load( theme.getUid() ).getPayer() ;
				
			} catch ( AppException e ) {
				
				throw new ServletException( "Failed to get payerEmail",  e ) ;
				
			}
			
			request.getSession().setAttribute( "payerEmail" , payerEmail ) ;
			
		}
		
		
		request.setAttribute( "permalink" , ThemeUtils.getPermalink() ) ;
		request.setAttribute( "editUrl" ,  ThemeApp.getConfig().getBaseUrl() +  EditTheme.URL_MAPPING ) ;
		
		log.info( "Forward to thank you page..." ) ;
		
		forwardTo( "paypal-post-payment.jsp", request, response ) ;
		
		
	}
	
	
	
}
