package com.dotemplate.dt.server;

import java.io.File;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.AppConfigReader;


public class DoTemplateBootstrap implements ServletContextListener {

	private static Log log = LogFactory.getLog( ServletContextListener.class);


	public void contextInitialized( ServletContextEvent event ) {
		
		log.info ( "Launching doTemplate..." ) ;
		
		ServletContext context = event.getServletContext () ;
		context.getServletContextName ();	
		
		DoTemplateApp app = new DoTemplateApp() ;
		
		DoTemplateConfig config = ( new AppConfigReader< DoTemplateConfig >( DoTemplateConfig.class )).read( 
				"application.dt.xml" ) ;
	
		config.setWebContext( event.getServletContext().getContextPath() ) ;
	
		String realPath = event.getServletContext().getRealPath( "" ) ;
		
		if ( realPath.endsWith( File.separator ) ){
			// Windows adds  a '/' suffix, Linux doesn't
			realPath = StringUtils.removeEnd( realPath, File.separator ) ;
		}
		
		app.init( realPath , config ) ;
		
		context.setAttribute ( "devMode", config.isDevMode () ) ;
		
		log.info ( "doTemplate web app READY at context path '" + config.getWebContext()+ "'." ) ;
		
		
	}

	
	
	public void contextDestroyed( ServletContextEvent event ) {
		log.info ( "Stopping doTemplate..." ) ;
		DoTemplateApp.get().terminate() ;
	}


	
}
