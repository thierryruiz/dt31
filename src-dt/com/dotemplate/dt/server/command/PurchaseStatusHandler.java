package com.dotemplate.dt.server.command;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.frwk.CommandHandler;
import com.dotemplate.core.shared.frwk.Command;
import com.dotemplate.core.shared.frwk.WebException;
import com.dotemplate.dt.shared.command.PurchaseStatus;
import com.dotemplate.dt.shared.command.PurchaseStatusResponse;
import com.dotemplate.theme.server.ThemeApp;


public class PurchaseStatusHandler extends CommandHandler< PurchaseStatusResponse > {
	
	private static Log log = LogFactory.getLog ( PurchaseStatusHandler.class );
	
	
	@Override
	public PurchaseStatusResponse handleAction ( Command< PurchaseStatusResponse > action )
							throws WebException {
		
		PurchaseStatus purchaseStatus =  ( PurchaseStatus  ) action ;
		
		if( log.isInfoEnabled () ){
			log.info ( "Get theme purchase status for theme " + purchaseStatus.getDesignUid() ) ;
		}
		
		PurchaseStatusResponse response = new PurchaseStatusResponse() ;
		
		response.setStatus( ThemeApp.getTheme().isPurchased() ? 1 : 0 ) ;
		
		return response ;
		
	}
	

}
