package com.dotemplate.dt.server.command;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.frwk.CommandHandler;
import com.dotemplate.core.shared.frwk.Command;
import com.dotemplate.core.shared.frwk.WebException;
import com.dotemplate.dt.shared.command.GetThemePermalink;
import com.dotemplate.dt.shared.command.GetThemePermalinkResponse;
import com.dotemplate.theme.server.ThemeUtils;



public class GetThemePermalinkHandler extends CommandHandler< GetThemePermalinkResponse > {
	
	private static Log log = LogFactory.getLog ( GetThemePermalinkHandler.class );
	
	
	@Override
	public GetThemePermalinkResponse handleAction ( Command< GetThemePermalinkResponse > action )
							throws WebException {
		
		GetThemePermalink getThemePermalink =  ( GetThemePermalink  ) action ;
		
		
		if( log.isInfoEnabled () ){
			log.info ( "Get theme permalink " + getThemePermalink.getDesignUid() ) ;
		}
		
		
		GetThemePermalinkResponse response = new GetThemePermalinkResponse() ;
		
		response.setPermalink( ThemeUtils.getPermalink() ) ;
		
		return response ;
		
	}
	

}
