package com.dotemplate.dt.tools;

import java.awt.Rectangle;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

import java.util.Iterator;
import java.util.Properties;

import org.apache.batik.apps.ttf2svg.Main;
import org.apache.batik.bridge.BridgeContext;
import org.apache.batik.bridge.DocumentLoader;
import org.apache.batik.bridge.GVTBuilder;
import org.apache.batik.bridge.UserAgent;
import org.apache.batik.bridge.UserAgentAdapter;
import org.apache.batik.dom.svg.SAXSVGDocumentFactory;
import org.apache.batik.util.XMLResourceDescriptor;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.context.Context;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.svg.SVGLocatable;
import org.w3c.dom.svg.SVGRect;


import com.dotemplate.core.server.App;
import com.dotemplate.core.server.RenderEngine;
import com.dotemplate.core.server.frwk.AppRuntimeException;
import com.dotemplate.core.server.svg.PNGTranscoder;
import com.dotemplate.dt.server.DoTemplateApp;


/**
 * 
 * 
 * Reads the TTF file from ttf/todo directory and create/updates all SVG font resources
 * 	- Generate the font SVG definition,
 *  - Compute the font metrics ascent and descent,
 *  - Update the free and premium properties file,
 *  - Generate the font thumbnail.
 * 	
 * @author truiz
 *
 */

/*




How to automate SVG fonts creation from font web sites
Example with fontsquirrel.com


I) Get list of downlod URLs

1)Install Link Gopher plugin in Firefox. 
2)Browse the font page and lookup font download url pattern 
3)Right click on Links menu at right bottom firefox window and select "Extract Links By Filter"
4)Enter the URL pattern and click OK.

=> Now we have list of all font URLs download 


II) Wget all fonts
1) Open notepad++
2) Copy past the list of URL in a new window
3) Open find replace
4) In find field type "^" .  In replace field "wget -qO- -O tmp.zip "
4) Again in find field type "$" and in replace field "  


If fonts are in otf. Convert to ttf
1) Mode all otf fonts to a folder
2) Install fontforge ubuntu package
3) Create the following script:

#!/usr/local/bin/fontforge
# Quick and dirty hack: converts a font to truetype (.ttf)
Print("Opening "+$1);
Open($1);
Print("Saving "+$1:r+".ttf");
Generate($1:r+".ttf");
Quit(0);

4)Save the script as otf2ttf.sh and type:

5) Run the script on the folder to convert all fonts
for i in *.otf; do fontforge -script otf2ttf.sh $i; done



*/
public class SVGFontTools {

	private final static Log log = LogFactory.getLog ( SVGFontTools.class );
	
	private final static String TODO = "../ttf/todo/" ;
	
	private final static String FONTS_DIR = "WEB-INF/templates/_fonts/" ;
	
	//private Map<String, String> forbidden = new HashMap<String, String>() ;
	
	private final static String[] forbidden = {
		"&#x0", "&#x5", "&#x6", "&#x8", "&#x14", 
		"&#x19", "&#x1a", "&#x1b", "&#x1c", "&#x1d", 
		"&#xb", "&#xc", "&#x1e", "&#x1f", "&#x2", "&#x1", "&#x3", "&#x4", "&#x7"
	} ;
	
	
	private String parser ; 
	
	private SAXSVGDocumentFactory factory  ;  
	
	private BridgeContext bridgeContext ;
	
	private GVTBuilder builder ;
	
	private Properties freeProperties = new Properties() ;
	
	private Properties premiumProperties = new Properties() ;
	
	
	public SVGFontTools (){
		
		parser = XMLResourceDescriptor.getXMLParserClassName ();
		factory = new SAXSVGDocumentFactory ( parser );
		factory.setValidating ( false ) ;
		UserAgent userAgent = new UserAgentAdapter ();
		bridgeContext = new BridgeContext ( userAgent, new DocumentLoader ( userAgent ) );
		bridgeContext.setDynamicState ( BridgeContext.DYNAMIC );
		builder = new GVTBuilder ();

	}
	
	
	public static void main ( String[] args ){
		
		log.info( "Launching font tools") ;
		
		try {
			
			( new DoTemplateApp() ).init() ;
			
			SVGFontTools tool = new SVGFontTools() ;
			
			tool.convert( true ) ;
			tool.convert( false ) ;
		
		} catch ( Exception e ) {
			e.printStackTrace() ;
			log.error ( e ) ;
		}
	}
	

	
	private void convert( boolean free ) throws Exception {

		RenderEngine themeEngine = App.getRenderEngine() ;
		
		String ttfDirName = free ? TODO + "free" : TODO + "premium" ;
		String [] ttf = {"TTF", "ttf" } ;
		
		
		File ttfDir = new File( ttfDirName ) ;
				
		@SuppressWarnings("unchecked")
		Iterator<File> files = FileUtils.iterateFiles( ttfDir , ttf, false ) ;
		
		File ttfFile ;
		
		String fontName ;
		
	
		
		Context ctx = new VelocityContext() ;
		
	
		
		freeProperties.load( new FileInputStream ( FONTS_DIR + "free.properties" ) ) ;
		premiumProperties.load( new FileInputStream ( FONTS_DIR + "premium.properties" ) ) ;
				
		
		while ( files.hasNext() ){
			
			ttfFile = files.next() ;
			fontName = StringUtils.substringBefore( ttfFile.getName(), ".") ;
			

			log.info("Font " + fontName.toLowerCase () + " : Converting..." ) ; 
			
			
			// Use batik ttf2svg to generate font SVG definition in a temporary file
			// Read the tmp file and get only the <defs> tag content.
			
			String tmpOut = "../tmp/fontDef.svg" ;
			
			String args[] = new String[7] ;
			args [ 0 ] = ttfDirName + "/" + ttfFile.getName() ;
			args [ 1 ] = "-l" ;
			args [ 2 ] = "0" ;			
			args [ 3 ] = "-h" ;
			args [ 4 ] = "255" ;
			args [ 5 ] = "-o" ;			
			args [ 6 ] = tmpOut ;
			
			try {
				Main.main( args ) ;
			} catch ( Exception e ){
				log.error( "Failed to convert font " + fontName,e  ) ;
				continue ;
			}	
			
			
			log.info( "Font " + fontName.toLowerCase () + " converted to SVG" ) ; 
			
			String fontDef = "" ;
			
			fontName = fontName.toLowerCase () ;
			
			try {
				fontDef = FileUtils.readFileToString ( new File ( tmpOut ) ) ;
			} catch ( IOException e1 ) {
				log.error( "Failed to read font def " + fontName, e1  ) ;
				e1.printStackTrace();
			}
			
			fontDef = StringUtils.substringBetween ( fontDef,  "<defs >", "</defs>" ) ; // keep the blank here
			
			String fontFamily = StringUtils.substringBetween ( fontDef,  "font-family=\"", "\"" ) ;
			
			fontName = transform ( fontName ) ;
			
			fontDef = StringUtils.replaceOnce ( fontDef, fontFamily, fontName ) ;
			
			//fontDef = stripNonValidXMLCharacters( fontDef ) ;
			
			fontDef = stripNonValidXMLCharacters( fontDef ) ;
			
			String out = FONTS_DIR  + fontName + ".svg" ;
			FileUtils.writeStringToFile ( new File( out ), "<defs>" + fontDef + "</defs>" ) ;
			
			StringWriter sw = null ;
			
			ctx.put( "fontFamily", fontName ) ;
			ctx.put( "fontLabel", ( "" + fontName.charAt(0) ) .toUpperCase() + fontName.substring( 1 ) ) ;
			ctx.put( "fontDef", fontDef ) ;
			
					
			try {
				
				themeEngine.generate ( ctx, "fontTools.svg.vm", sw = new StringWriter() ) ;
				
			} catch (Exception e) {
				log.error( "Failed to create font svg for" + fontName, e ) ;
				e.printStackTrace();
				continue ;
			
			} finally {
				try {
					sw.flush() ;
					sw.close();
				} catch (IOException e) {
					System.out.println( "Failed to close output stream for font " + fontName ) ;
					e.printStackTrace();
					continue ;
				}
			}
			
			log.info( "Font " + fontName.toLowerCase () + " computing metrics" ) ; 
			
			String svgDoc = sw.getBuffer().toString() ;
						
			// some font create glyphs that cannot be parsed by XML reader
			try {
				computeMetrics( svgDoc , fontName, free  ) ;
			} catch (Exception e1) {
				log.error( "Failed to compute metrics for font " + fontName, e1  ) ;
				continue ;
			}
				
			String thumbnail = free ? "images/svgfonts/free/" + fontName + ".png"  : 
				"images/svgfonts/premium/" + fontName + ".png" ;
			
			try {
				
				PNGTranscoder.transcode( svgDoc, new Rectangle ( 530, 40 ), thumbnail ) ;
			
			} catch ( Exception e ){
				log.error( "Failed to create thumbnail for font " + fontName, e  ) ;
				continue ;
			}
		
			log.info( "Font " + fontName.toLowerCase () + " : Metrics computed." ) ; 
			log.info( "Font " + fontName.toLowerCase () + " : Done !\n\n" ) ;
			
			
		}		
		
		
		
		freeProperties.store( new FileOutputStream( FONTS_DIR + "free.properties" ), "fontFamily=ascent,descent" ) ;
		premiumProperties.store( new FileOutputStream( FONTS_DIR + "premium.properties" ), "fontFamily=ascent,descent" ) ;
		
		
	}


	
	private String transform( String fontName ) {
		fontName = StringUtils.replace( fontName, "_", "-" ) ;
		
		fontName = splitStyle( fontName, "italic" ) ;
		fontName = splitStyle( fontName, "bold" ) ;
		fontName = splitStyle( fontName, "semibold" ) ;
		fontName = splitStyle( fontName, "condensed" ) ;
		fontName = splitStyle( fontName, "cond" ) ;
		fontName = splitStyle( fontName, "black" ) ;
		fontName = splitStyle( fontName, "heavy" ) ;
		fontName = splitStyle( fontName, "light" ) ;
		fontName = splitStyle( fontName, "thin" ) ;		
		fontName = splitStyle( fontName, "regular" ) ;
		fontName = splitStyle( fontName, "oblique" ) ;
				
		//fontName = ( "" + fontName.charAt(0) ).toUpperCase() + fontName.substring( 1 ) ; 
		
		return fontName ;
	}
	
	
	private String splitStyle( String fontName, String style ){
		
		// nameitalic -> name-italic
		fontName = fontName.toLowerCase() ;
		
		int i = fontName.indexOf(style) ;
		
		if ( i < 0 ){
			return fontName ; 
		} 
		
		if( fontName.charAt( i-1 ) == ' ' ){
			return fontName ;
		}
		
		if( fontName.charAt( i-1 ) == '-' ){
			return fontName ;
		}
		
		fontName = fontName.substring(0,i) + "-" + fontName.substring( i ) ;
		
		return transform ( fontName ) ;
		
		
	}


	private void computeMetrics( String svgDoc, String fontName, boolean free ) {
		
		Document doc = null;
		try {
			doc = factory.createDocument ( null, new StringReader( svgDoc ) );
		} catch ( IOException e ) {
			log.error( svgDoc ) ;
			throw new AppRuntimeException ( "Unable to set TextProperty dimensions for font " + fontName , e );
		}
		
		builder.build ( bridgeContext, doc );
		
		// get bounds
		Element el = doc.getElementById ( "ascent" ) ;
		SVGRect rect = ( ( SVGLocatable ) el ).getBBox() ;
		
		int ascent = ( int ) rect.getHeight() ;
		
		el = doc.getElementById ( "height" ) ;
		rect = ( ( SVGLocatable ) el ).getBBox() ;
		int height =  ( int ) rect.getHeight() ;
		
		int descent = height - ascent ;
		
		if ( free ){
			freeProperties.put( fontName, ascent +"," + descent ) ;
		} else {
			premiumProperties.put( fontName, ascent +"," + descent ) ;
		}

	}
		
	
    public String stripNonValidXMLCharacters(String svgDoc) {
    	
    	for ( String fb : forbidden ){
 
    		svgDoc = StringUtils.replace ( svgDoc, fb, "" ) ;
    		
    	}
		
        return svgDoc ;
    }    
	
    
    
}


