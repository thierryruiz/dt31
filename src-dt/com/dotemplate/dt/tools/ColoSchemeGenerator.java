package com.dotemplate.dt.tools;



import java.awt.Color;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Properties;


import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

import com.dotemplate.core.server.App;
import com.dotemplate.core.server.HSLColor;
import com.dotemplate.core.shared.Scheme;
import com.dotemplate.dt.server.DoTemplateApp;
import com.dotemplate.theme.server.symbol.ColorSchemeProvider;



/**
 * 
 * Creates color scheme html preview
 * 
 * @author Thierry Ruiz
 * 
 */
public class ColoSchemeGenerator {

	
	public static VelocityContext context = new VelocityContext();

	
	public static void main( String[] args ) {

		( new DoTemplateApp() ) .init()   ;
		
		ColorSchemeProvider schemeProvider = 
				( ColorSchemeProvider ) App.getSingleton( ColorSchemeProvider.class ) ;
		
		
		schemeProvider.load() ;
		
		ArrayList< Scheme > schemes = schemeProvider.list() ;
		
		
		try {
			
			 Properties p = new Properties();
			 
			 String vmPath = "WEB-INF\\templates\\_schemes" ;
			 
			 p.setProperty( "file.resource.loader.path", vmPath );
			 
			 System.out.println( "Using path " + vmPath ) ;
			 
			 Velocity.init( p );
			
		
		} catch ( Exception e1 ) {
			
			e1.printStackTrace();
		
		}
		
		
		context.put( "schemes", schemes ) ;
		context.put( "_", new ColorHelper() ) ;	
		
		
		Template template = null;

		String out = "../tmp/schemes.html" ;
		
		try {
		   
			
			template = Velocity.getTemplate( "schemes.html.vm" );

			FileWriter w = new FileWriter( out );

			
			template.merge( context, w );
			
			w.flush();
			w.close() ;

			
		} catch( Exception e ) {
			
		   e.printStackTrace();
		
		}
		
		System.out.println( "Done ! Generated in  " + out ) ;
		
	}
	
	
	
	public static class ColorHelper {
		
		public ColorCell c ( String type ){
			try {
				String hexa = ( ( Scheme ) context.get( "scheme" ) ) .getColor( type ) ;
				HSLColor hslc = new HSLColor( Color.decode( hexa  ) ) ;
				String hsl = "<div style='font-size:10px'>H=" + ( int ) hslc.getHue() +  " ,S=" + ( int ) hslc.getSaturation() + ", L=" + ( int )  hslc.getLuminance() + "</div>"; 
				return new ColorCell( hexa, hsl ) ;
			
			} catch ( Exception e ){
				e.printStackTrace() ;
				return null ;
			}
		}
		
	}

	
	public static class ColorCell {
		
		String hexa ;
		
		String hsl ;
		
		public ColorCell( String hexa, String hsl ){
			this.hexa = hexa ;	
			this.hsl = hsl ;
		}
		
		public String getHexa() {
			return hexa;
		}
		
		public String getHsl() {
			return hsl ;
		}
		
		
		
		
		
	}

	
	
}
