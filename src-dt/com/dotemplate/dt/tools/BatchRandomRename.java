package com.dotemplate.dt.tools;

import java.io.File;
import java.util.Collection;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;

public class BatchRandomRename {
	
	
	@SuppressWarnings("unchecked")
	public static void main ( String[] args ){
		
		String path = args[ 0 ] ;
		String ext, newname ;
		
		for ( File file : ( Collection< File> ) FileUtils.listFiles( new File( path ), null, false ) ) {
		
			ext = StringUtils.substringAfterLast(file.getName(), "." ) ;
			newname = path + "/img-" + RandomStringUtils.randomAlphabetic(1).toLowerCase() +  
					RandomStringUtils.randomAlphanumeric(5).toLowerCase() + "." + ext ;
			
			file.renameTo( new File( newname ) ) ;
			
		}		
		
	}
	
	

}
