package com.dotemplate.dt.tools.jsontest;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;


public class CopyOfJSONTest {


	public CopyOfJSONTest (){
		
	}
	
	
	public static void main ( String[] args ){
		
		try {
			String json = FileUtils.readFileToString(new File ( 
					"WebContent/WEB-INF/templates/_symbols/content/abstract-home-layout-2/symbols.json") ) ;
			
			
			JsonElement root = new JsonParser().parse( json );
			
			
			RuntimeTypeAdapterFactory<Row> rowAdapter
			        = RuntimeTypeAdapterFactory.of( Row.class, "type" );
			
			rowAdapter.registerSubtype(Header.class, "header" ) ;
			rowAdapter.registerSubtype(Columns.class, "columns" ) ;
			
			Gson gson = new GsonBuilder()
		        .registerTypeAdapterFactory( rowAdapter )		        
		        .create();
			        
	        Layouts layouts = gson.fromJson( root, Layouts.class ) ;
	        
	        System.out.println( (( Header ) layouts.layouts.get(0).rows.get( 0) ).style ) ;
	        //System.out.println( (( Columns ) layouts.layouts.get(1).rows.get( 1 ) ).nb ) ;
	        
			
		} catch (JsonSyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
    
    
}


