package com.dotemplate.dt.tools.jsontest;

import java.lang.reflect.Type;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gwt.json.client.JSONException;

public class RowAdapter implements JsonSerializer<Row>,
		JsonDeserializer<Row> {

	@Override
	public Row deserialize(JsonElement el, Type type,
			JsonDeserializationContext context ) throws JsonParseException {
		
		JsonObject object = el.getAsJsonObject();
		
		
		if( "content".equals( object.get( "type" ).getAsString() ) ) {
			
			return context.deserialize( object.get("content" ), Header.class ) ; 
			
		}
		
		if( "heading".equals( object.get( "type" ).getAsString() ) ) {
			
			return context.deserialize( object.get("content" ), Header.class ) ; 
			
		}
		
		throw new JSONException( "Missing or undefined row type" ) ;

	}

	@Override
	public JsonElement serialize(Row row , Type arg1,
			JsonSerializationContext arg2) {
		return null;
	}

}
