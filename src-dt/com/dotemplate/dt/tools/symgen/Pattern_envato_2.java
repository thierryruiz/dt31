package com.dotemplate.dt.tools.symgen;

import java.io.File;


public class Pattern_envato_2 extends AbstractPattern {
	
	
	public Pattern_envato_2( File file ) {
		super( file, -1 ) ;
	}
	
	
	@Override
	protected String getAbstractSymbolFolder() {
		return "abstract-envato-2" ;
	}
	
	
	@Override
	protected String getSymbolFolder() {
		return "envato-2-" + id  ;
	}
	
	
}
