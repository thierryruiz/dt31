package com.dotemplate.dt.tools.symgen;

import java.io.FileWriter;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Properties;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;


import com.dotemplate.core.server.DesignUtils;
import com.dotemplate.core.shared.properties.PropertySet;
import com.dotemplate.dt.server.DoTemplateApp;
import com.dotemplate.theme.server.GoogleFont;
import com.dotemplate.theme.server.symbol.HeadingFontProvider;
import com.dotemplate.theme.shared.ThemeSymbolType;


public class GoogleFontsRenderer {
	
	
	private LinkedHashMap< String, GoogleFont > fonts = new LinkedHashMap< String, GoogleFont >(); 
	
	public static void main( String[] args ) {

		( new DoTemplateApp() ) .init()   ;
				
		HeadingFontProvider provider = ( HeadingFontProvider ) 
				DesignUtils.getSymbolProvider( ThemeSymbolType.HEADINGFONT );
		
		provider.load() ;

		
		GoogleFontsRenderer renderer = new GoogleFontsRenderer() ;
		ArrayList< PropertySet > fonts = provider.list() ;
		
		for ( PropertySet font : fonts ){
			
			if( !( ( String ) font.get("ftype" )).equals("google")) {

				continue ;
			}

			
			GoogleFont gfont = new GoogleFont() ;
			gfont.setUrlParams(  ( String ) font.get("urlParams" ) ) ;
			gfont.setName(font.getName() ) ;
			gfont.setTitle( ( String ) font.get("title" )) ;
			gfont.setFamily( ( String ) font.get("family" ) ) ;
			gfont.setWeight( ( String ) font.get("weight" ) ) ;
			gfont.setStyle( ( String ) font.get("style" ) ) ;
			
			renderer.addGoogleFont(gfont) ;
		} 
		
		renderer.generate() ;
		
		
		
	}
	

	void addGoogleFont( GoogleFont font ){
		System.out.println( "Add font " + font.getName() ) ;	
		fonts.put( font.getName(), font) ;
	}
	
		
	public void generate() {

		Properties p = new Properties();
		 
		String vmPath = "WEB-INF\\templates\\" ;
		 
		p.setProperty( "file.resource.loader.path", vmPath );
		 
		System.out.println( "Using path " + vmPath ) ;
		 
		Velocity.init( p );
		
		
		VelocityContext context = new VelocityContext();

		context.put( "fonts", fonts ) ;
		
		Template template = null;

		try {
		   
			template = Velocity.getTemplate( "googlefonts.html.vm" );

			FileWriter w = new FileWriter( "../tmp/googlefonts.html" );

			template.merge( context, w );
			
			w.flush();
			w.close() ;
			
			System.out.println( "done generate fonts in tmp/googlefonts.html" ) ;
			
		} catch( Exception e ) {
			
		   e.printStackTrace();
		
		}

	}
	

}
