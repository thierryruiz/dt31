package com.dotemplate.dt.tools.symgen;

import java.io.File;


public class Pattern_stripes extends AbstractPattern {
	
	
	public Pattern_stripes( File file ) {
		super( file, -1 ) ;
	}
	
	
	@Override
	protected String getAbstractSymbolFolder() {
		return "abstract-stripes" ;
	}
	
	
	@Override
	protected String getSymbolFolder() {
		return "stripes-" + id  ;
	}
	
	
}
