package com.dotemplate.dt.tools.symgen;


import com.dotemplate.theme.shared.ThemeSymbolType;

public class Fill_pattern extends AbstractGenerator implements FillGradient {

	protected String patternId ;
		
	protected String color ;
		
	
	public Fill_pattern( String patternId, String color, int sortIndex ) {
		super( sortIndex ) ;
		this.patternId = patternId ;
		this.color = color ;
	}

	
	
	@Override
	protected void initContext() {
		context.put( "id", patternId ) ;
		context.put( "color", color ) ;
		context.put( "sortIndex", sortIndex ) ;
	}


	@Override
	protected String getSymbolFolderRoot() {
		return ThemeSymbolType.FILL.getFolder();
	}


	@Override
	protected String getSymbolFolder() {
		return "pattern-" + patternId ;	
	}


	@Override
	protected String getAbstractSymbolFolder() {
		return "abstract-pattern"	;
	}
	
	
	
	@Override
	protected void generate() throws Exception {
		super.generate();
		generate_css();
		generate_svgf();
	}
	
	
}
