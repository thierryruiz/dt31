package com.dotemplate.dt.tools.symgen;

public interface FillGradient {
	
	public final static int SOFT = 20 ;
	
	public final static int STANDARD = 40 ;
	
	public final static int STRONG = 60 ;
	

}
