package com.dotemplate.dt.tools.symgen;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;

import com.dotemplate.dt.tools.symgen.home.LoopIterator;

public class SynonymDictionary {
	
	private Map< String, LoopIterator < String > > wordMap ;

	private SynonymDictionary( Map< String, LoopIterator < String > >  wordMap ) {
		this.wordMap = wordMap ;
	}
	
	public static SynonymDictionary load( String propertiesFile )  {
		
		Properties words  =  new Properties() ;
		
		try {
			
			words.load( new FileInputStream(  propertiesFile ) ) ;
			
		} catch (Exception e) {
			e.printStackTrace() ;
			throw new RuntimeException( e ) ;
		}
		
		Map< String, LoopIterator < String > > wordMap = new HashMap< String, LoopIterator < String > >() ;
		
		
		List< String > synonyms  ;
		ArrayList< String > extended ;
		
		String word ;
	
		for ( Map.Entry< Object, Object > prop : words.entrySet() ){
			
			word = (( String ) prop.getKey() ).trim() ;
			
			synonyms = Arrays.asList( ( ( String ) prop.getValue() ).trim().split( "\\s*,\\s*" )) ;
			
			//wordMap.put( word,  new LoopIterator < String > ( synonyms )  ) ;
			
			for ( String synonym : synonyms ) {
				
				extended = new ArrayList< String >( synonyms ) ;
				// wordMap.put ( synonym, new LoopIterator < String > ( extended ) ) ;
				
			}
			
			
			
		}
		
		
		
			
		return new SynonymDictionary ( wordMap )   ;
		
	}
	
	
	
	private String nextSynonym( String word ){
		
		LoopIterator < String > synonyms = wordMap.get( word ) ;
		
		if ( synonyms == null ){
			throw new RuntimeException( "No registered word '" +  word + "' in dictionary" ) ;
		}
		
		String synomym = synonyms.next() ;
		
		
		if ( synomym == null ){
			throw new RuntimeException( "No synonym defined fo '" +  word + "' in dictionary" ) ;
		}
		
		return synomym ;
		
	}
	
	
	public String applySynonyms( String phrase ){
		int i ; String word ; String synonym ;
		
		while( ( i = phrase.indexOf( "$" ) ) > -1 ){
			word = phrase.substring( i ) ;
			word = StringUtils.substringBefore( word, " " ) ;
			word = StringUtils.removeEnd(word, "," ) ;
			word = StringUtils.removeEnd(word, "." ) ;
			
			synonym = nextSynonym( word.substring( 1 ) ) ;
			
			
			
			phrase = StringUtils.replace ( phrase, word, synonym ) ;
			
		}
		
		return phrase ;
		
	}
	
	
	
	
}
