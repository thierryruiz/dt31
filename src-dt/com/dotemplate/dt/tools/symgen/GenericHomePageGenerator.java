package com.dotemplate.dt.tools.symgen;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.velocity.app.Velocity;


import com.dotemplate.dt.tools.symgen.home.LoopIterator;
import com.dotemplate.theme.shared.ThemeSymbolType;


@Deprecated
public class GenericHomePageGenerator extends AbstractHomePageGenerator {

	protected int index ;
	
	protected SynonymDictionary dictionary  ;
	
	Map< String, String > symbol  ;
	
	Map<String, String> headers ;
	
	LoopIterator< Map<String, String > > columns ;

	LoopIterator< Map<String, String > > panoImages ;
	
	LoopIterator< Map<String, String > > standardImages ;
	
	LoopIterator< Map<String, String > > circularImages ;
	
	Map< String, LoopIterator< File > > images ;
	
	
	public GenericHomePageGenerator( 
			int i , 
			Map<String, String> symbol, 
			Map<String, String> header,
			LoopIterator< Map<String, String > >columns,
			SynonymDictionary dictionary, 
			Map< String, LoopIterator< File > > images ) {
		
		this.index = i ;
		this.symbol = symbol;
		this.headers = header;
		this.columns = columns;
		this.dictionary = dictionary ;
		this.images = images ;
		
	}


	@Override
	protected void initContext() {
		// store all symbol entries to generate symbol.xml
		
		System.out.println();
		System.out.println();
		System.out.println( "Symbols:" ) ;
		
		String value, key ;
		
		for ( Map.Entry< String, String > entry : symbol.entrySet() ){
			
			//key = StringUtils.substringAfter ( entry.getKey(), "." ) ;
			//value = this.dictionary.applySynonyms ( entry.getValue() ) ;
			
			context.put( StringUtils.substringAfter ( entry.getKey(), "." ) , entry.getValue() ) ;
			
			//System.out.println( key +  "=" + entry.getValue() ) ;
		
		}

		
		System.out.println( "\n\nHeaders:" ) ;
		

		
		for ( Map.Entry< String, String > entry : headers.entrySet() ){
			
			key = StringUtils.substringAfter ( entry.getKey(), ".headers." ) ;
			value = this.dictionary.applySynonyms ( entry.getValue() ) ;
			
			value = capitalizeWords( value ) ;
			
			context.put( "headers_" + key , value) ;
			
			System.out.println( key +  "=" + value ) ;
		
		}
		
		int nbOfColumns = Integer.parseInt( ( String ) context.get( "nbOfColumns" ) );
		
		ArrayList<  Map< String, String > > cols = new ArrayList<  Map< String, String > >() ;
		Map< String, String  > column = new HashMap< String, String >() ;
		

		String imageType = ( String ) context.get( "columnImageType" ) ;
		
		if ( imageType == null ){
			throw new RuntimeException( "Dude ! Missing " + index + ".columnImageType in symbols.properties" ) ;
		}
		
		
		LoopIterator< File > imageFiles = images.get( imageType ) ;
		
		
		for ( int i = 0 ; i < nbOfColumns ; i++ ){

			System.out.println( "\n\nColumn " + i  ) ;

			column = new HashMap< String, String >() ;

			for ( Map.Entry< String, String > entry : columns.next().entrySet() ){
				
				key = StringUtils.substringAfter ( entry.getKey(), ".column." ) ;
				value = this.dictionary.applySynonyms ( entry.getValue() ) ;
			
				if ( key.equals( "h1" ) || key.equals( "h2" )){
					value = capitalizeWords( value ) ;
				} else {
				
					value = capitalizeParagraph( value, "." ) ;
					value = capitalizeParagraph( value, ":" ) ;
					value = capitalizeParagraph( value, "!" ) ;
					value = capitalizeParagraph( value, "?" ) ;
				}
				column.put( key, value ) ;
			
			}
			
			column.put( "image", "generic/30/" + imageType + "/"  + 
					imageFiles.next().getName() ) ;
			
			cols.add( column ) ;
		
		}
		
		context.put( "columns", cols ) ;
		
	}
	
	



	@Override
	protected String getSymbolFolderRoot() {
		return ThemeSymbolType.CONTENT.getFolder();
	}


	@Override
	protected String getSymbolFolder() {
		return "home-generic-" + index ;	
	}


	@Override
	protected String getAbstractSymbolFolder() {
		return "abstract-home-generic"	;
	}
	
	
	
	@Override
	protected void generate() throws Exception {		
		// generate symbol.xml
		super.generate();
		
		String layout = ( String ) context.get("layout") ;
		
		String layoutTemplate = "_symbols/" + getSymbolFolderRoot() + "/" + getAbstractSymbolFolder() + "/layouts/" + layout 
				+  ".html.vm"  ;

		FileWriter w = new FileWriter( getOutputFolder() + getSymbolFolder() + "/html.vm" ) ;
		
		( Velocity.getTemplate( layoutTemplate )) .merge( context, w );
		
		w.flush();
		w.close() ;
		
		
		
		super.generate_css() ;
		super.generate_js() ;
		
	}
	
	
	
	protected String capitalizeParagraph ( String paragraph, String dot ){
	
		String[] phrases = StringUtils.split( paragraph,  dot ) ;
		
		if ( phrases == null || phrases.length == 0  ) {
			return paragraph ;
		}
		
		
		StringBuffer buf = new StringBuffer() ;
		
		int i = 0 ;
		for ( String phrase : phrases ){
			
			phrase = phrase.trim() ;
			phrase = StringUtils.capitalize( phrase ) ;
			
			buf.append( phrase ) ;
			
			if ( i < phrases.length - 1 ) {
				buf.append( dot ).append( " " ) ;
			}
		}
		
		return buf.toString() ;
	
	}
	
	protected String capitalizeWords ( String phrase ){
		
		String[] words = StringUtils.split( phrase ) ;
		
		if ( words == null || words.length == 0  ) {
			return phrase ;
		}
		
		
		StringBuffer buf = new StringBuffer() ;
		
		for ( String word : words ){
			
			word = word.trim() ;
			word = StringUtils.capitalize( word ) ;
			
			buf.append( word ).append( " " ) ;
			
		}
		
		return buf.toString().trim() ;
	
	}
	

}
