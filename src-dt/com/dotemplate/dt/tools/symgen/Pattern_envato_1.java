package com.dotemplate.dt.tools.symgen;

import java.io.File;


public class Pattern_envato_1 extends AbstractPattern {
	
	
	public Pattern_envato_1( File file ) {
		super( file, -1 ) ;
	}
	
	
	@Override
	protected String getAbstractSymbolFolder() {
		return "abstract-envato-1" ;
	}
	
	
	@Override
	protected String getSymbolFolder() {
		return "envato-1-" + id  ;
	}
	
	
}
