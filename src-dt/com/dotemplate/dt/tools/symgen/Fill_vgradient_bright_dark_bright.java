package com.dotemplate.dt.tools.symgen;


public class Fill_vgradient_bright_dark_bright extends AbstractFill_vgradient implements FillGradient {
	
		
	public Fill_vgradient_bright_dark_bright( int gradient, int sortIndex ) {
		super( gradient, sortIndex ) ;
	}


	@Override
	protected String getSymbolFolder() {
		String suffix = "soft" ;
		if ( gradient == FillGradient.STANDARD ) suffix = "standard" ;
		if ( gradient == FillGradient.STRONG ) suffix = "strong" ;
		return "vgradient-bright-dark-bright-" + suffix ;	
	}


	@Override
	protected String getAbstractSymbolFolder() {
		return "abstract-vgradient-bright-dark-bright"	;
	}
	

}
