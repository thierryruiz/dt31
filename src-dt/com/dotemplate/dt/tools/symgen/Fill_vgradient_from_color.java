package com.dotemplate.dt.tools.symgen;


public class Fill_vgradient_from_color extends AbstractFill_vgradient implements FillGradient {
	
	protected Colors gradientColor ;
		
	public Fill_vgradient_from_color( Colors gradientColor, int sortIndex ) {
		super( -1, sortIndex ) ;
		this.gradientColor = gradientColor ;
	}


	@Override
	protected void initContext() {
		super.initContext();
		context.put( "gradientColor", gradientColor.getCode() ) ;	
	}
	
	
	@Override
	protected String getSymbolFolder() {
		return  ( Colors.GREEN == gradientColor ) ? "vgradient-from-color" : "vgradient-from-" + gradientColor.getName()  ;
	}


	@Override
	protected String getAbstractSymbolFolder() {
		return "abstract-vgradient-from-color"	;
	}
	

}
