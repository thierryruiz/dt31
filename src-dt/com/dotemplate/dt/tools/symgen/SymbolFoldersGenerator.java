package com.dotemplate.dt.tools.symgen;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Properties;


import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.apache.commons.lang.StringUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

import com.dotemplate.core.server.frwk.AppRuntimeException;
import com.dotemplate.theme.server.GoogleFont;


/* creates sub symbols folder from an abstract one */
public class SymbolFoldersGenerator {
	
	
	
	/**
	 * @param args
	 */
	public static void main( String[] args ) {
		
		try {
			
			// create output symbol folder
			File outputFolder = new File ( AbstractGenerator.getOutputFolder() ) ;
			FileUtils.cleanDirectory( outputFolder ) ;
			
			//fill_vgardient( args ) ;			
			
			//fill_pattern() ;
			//fill_texture() ;
			
			
			//pattern_envato_0() ;

			
			//pattern_envato_1() ;
			//pattern_envato_2() ;
			//pattern_envato_3() ;
			
			//pattern_stripes() ;
			
			//pattern_patterrific() ;
			
			//pattern_subtlepatterns() ;
			
			//texture() ;
			//fill_texture() ;
			
			googleFonts() ;
			
			
		} catch ( Exception e ) {
			
			e.printStackTrace();
		}
	
	}
	
	
	static void fill_vgardient( String[] args ) throws Exception {
	
		int i = 1 ;
		
		( new Fill_vgradient_bright_dark( FillGradient.SOFT, 10 * i++ ) ).generate();
		( new Fill_vgradient_bright_dark(  FillGradient.STANDARD, 10 * i++  ) ).generate();
		( new Fill_vgradient_bright_dark(  FillGradient.STRONG, 10 * i++ )  ).generate();
		
		( new Fill_vgradient_dark_bright(  FillGradient.SOFT, 10 * i++ ) ).generate();
		( new Fill_vgradient_dark_bright(  FillGradient.STANDARD, 10 * i++ ) ).generate();
		( new Fill_vgradient_dark_bright(  FillGradient.STRONG, 10 * i++ ) ).generate();
		
		( new Fill_vgradient_dark_bright_dark( FillGradient.SOFT, 10 * i++  ) ).generate();
		( new Fill_vgradient_dark_bright_dark(  FillGradient.STANDARD, 10 * i++  ) ).generate();
		( new Fill_vgradient_dark_bright_dark(  FillGradient.STRONG, 10 * i++  ) ).generate();
		
		
		( new Fill_vgradient_bright_dark_bright(  FillGradient.SOFT, 10 * i++  ) ).generate();
		( new Fill_vgradient_bright_dark_bright(  FillGradient.STANDARD, 10 * i++  ) ).generate();
		( new Fill_vgradient_bright_dark_bright(  FillGradient.STRONG, 10 * i++  ) ).generate();
		
		( new Fill_vgradient_glow(  FillGradient.SOFT, 10 * i++  ) ).generate();
		( new Fill_vgradient_glow(  FillGradient.STANDARD, 10 * i++  ) ).generate();
		( new Fill_vgradient_glow(  FillGradient.STRONG, 10 * i++  ) ).generate();
		
		
		( new Fill_vgradient_to_color( Colors.WHITE, 10 * i++  ) ).generate();
		( new Fill_vgradient_from_color( Colors.WHITE, 10 * i++  ) ).generate();		
		( new Fill_vgradient_to_color( Colors.BLACK, 10 * i++  ) ).generate();
		( new Fill_vgradient_from_color( Colors.BLACK, 10 * i++  ) ).generate();
		
		( new Fill_vgradient_to_color( Colors.GREEN, 10 * i++  ) ).generate();
		( new Fill_vgradient_from_color( Colors.GREEN, 10 * i++  ) ).generate();
				
		
	}	
	
	
	
	static void pattern_envato_0() throws Exception {
		Collection<?> patterns = FileUtils.listFiles( new File( "WEB-INF\\templates\\_symbols\\pattern\\abstract-envato-0\\pngs" ), 
				TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE ) ;
		
		for ( Object file : patterns ){
			new Pattern_envato_0 ( ( File ) file ).generate() ;
		}
	}
	
	
	static void pattern_envato_1() throws Exception {
		Collection<?> patterns = FileUtils.listFiles( new File( "WEB-INF\\templates\\_symbols\\pattern\\abstract-envato-1\\pngs" ), 
				TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE ) ;
		
		for ( Object file : patterns ){
			new Pattern_envato_1 ( ( File ) file ).generate() ;
		}
	}
	
	static void pattern_envato_2() throws Exception {
		Collection<?> patterns = FileUtils.listFiles( new File( "WEB-INF\\templates\\_symbols\\pattern\\abstract-envato-2\\pngs" ), 
				TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE ) ;
		
		for ( Object file : patterns ){
			new Pattern_envato_2 ( ( File ) file ).generate() ;
		}
	}

	/*
	static void pattern_envato_3() throws Exception {
		Collection<?> patterns = FileUtils.listFiles( new File( "WEB-INF\\templates\\_symbols\\pattern\\abstract-envato-3\\pngs" ), 
				TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE ) ;
		
		for ( Object file : patterns ){
			new Pattern_envato_3 ( ( File ) file ).generate() ;
		}
	}*/
	
	static void pattern_stripes() throws Exception {
		Collection<?> patterns = FileUtils.listFiles( new File( "WEB-INF\\templates\\_symbols\\pattern\\abstract-stripes\\pngs" ), 
				TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE ) ;
		
		for ( Object file : patterns ){
			new Pattern_stripes ( ( File ) file ).generate() ;
		}
	}	
	
	
	static void pattern_patterrific() throws Exception {
		Collection<?> patterns = FileUtils.listFiles( new File( "WEB-INF\\templates\\_symbols\\pattern\\abstract-patterrific\\pngs" ), 
				TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE ) ;
		
		for ( Object file : patterns ){
			new Pattern_patterrific( ( File ) file ).generate() ;
		}
	}
	
	
	static void fill_pattern() throws Exception {
		
		String patterns[] = { "envato-0-1", "envato-0-13", "envato-0-15", "envato-0-17", "envato-0-18",
			"stripes-r-10", "stripes-r-20","stripes-r-30","stripes-r-40","stripes-r-50",
			"stripes-l-10","stripes-l-20","stripes-l-30","stripes-l-40","stripes-l-50",
			"envato-1-12", "envato-0-1", "envato-1-15", "envato-1-17", "envato-1-2", "envato-1-5", "envato-1-17",
			"envato-2-2", "envato-2-5", "envato-2-7", 
			"envato-3-14", "envato-3-2","envato-3-4", "envato-3-7", "envato-3-6",
			"patterrific-1","patterrific-2","patterrific-3","patterrific-5","patterrific-8",
			
			"subtlepatterns-redox-01", "subtlepatterns-redox-02", "subtlepatterns-dust", "subtlepatterns-fabric-plaid",
			"subtlepatterns-gridme","subtlepatterns-knitted-netting", "subtlepatterns-handmadepaper", "subtlepatterns-hexellence", 
			"subtlepatterns-lghtmesh","subtlepatterns-light-alu", "subtlepatterns-leather-1","subtlepatterns-paper-2",
			"subtlepatterns-light-toast","subtlepatterns-subtle-stripes","subtlepatterns-subtlenet2","subtlepatterns-wall",
			"subtlepatterns-white-plaster"
			
		} ;
	
		int i = 1 ;
		for ( String pattern : patterns ){
			new Fill_pattern ( pattern, Colors.BLUE.getCode(), ( 10 * i++ ) ).generate() ;
			new Fill_pattern_fade ( pattern, Colors.BLUE.getCode(), ( 10 * i++ ) ).generate() ;
		}
	
		
	}
	
	
	
	
	static void texture() throws Exception {
		
		String textures[] = { "metal", "webtreats-metal-5", "webtreats-metal-4", "parquet4", "wood-2", "stone-1", "paper-2", "paper-4",
				"paper-5", "paper-7"	
		};
		
		
		int i = 1 ;
		for ( String texture : textures ){
			new Fill_texture ( texture, ( 10 * i++ ) ).generate() ;
		}
		
		
	}
	

	
	
	static void pattern_subtlepatterns() throws Exception {
		Collection<?> patterns = FileUtils.listFiles( new File( "WEB-INF\\templates\\_symbols\\pattern\\abstract-subtlepatterns\\pngs" ), 
				TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE ) ;
		
		for ( Object file : patterns ){
			new Pattern_subtlepatterns ( ( File ) file ).generate() ;
		}
		
		
	}
	
	
	static void fill_texture() throws Exception {
		String textures[] = { "metal", "webtreats-metal-5", "webtreats-metal-4", "parquet4", "wood-2", "stone-1", "paper-2", "paper-4",
				"paper-5", "paper-7"	
		};
		
		
		int i = 1 ;
		for ( String texture : textures ){
			new Fill_texture ( texture, ( 10 * i++ ) ).generate() ;
		}
		
	}
	
	
	
	@SuppressWarnings("unchecked")
	static void googleFonts() throws Exception {
		
		List< String > lines = null ;
		
		ArrayList< GoogleFont > fonts = new ArrayList< GoogleFont >() ;
		
		try {
			
			lines = ( List< String > ) FileUtils.readLines( new File( "WEB-INF\\templates\\_symbols\\headingFont\\abstract-google\\googlefonts.txt" ) ) ;
		
		} catch ( Exception e ) {
			throw new AppRuntimeException ( "Failed to load googlefonts", e ) ;
		}
		
		
		
		String[] tokens ;
		
		
		GFont gfont ;
		int i = 20 ;
		for ( String line : lines ){
			tokens = StringUtils.splitPreserveAllTokens( line, ';' ) ;
			if ( tokens == null || tokens.length == 0 ) continue ;
			System.out.println(  tokens[ 0 ]  ) ;
			GoogleFont font = new GoogleFont() ;
			font.setName(tokens [ 0 ].trim() ) ;
			font.setTitle(tokens [ 2 ].trim() ) ;
			font.setFamily(tokens [ 1 ].trim() ) ;		
			font.setUrlParams(tokens [ 5 ].trim() ) ;
			font.setWeight( tokens [ 3 ].trim()) ;
			font.setStyle( tokens [ 4 ].trim()) ;			
			fonts.add( font ) ;
			
			gfont = new GFont( font, i ) ;
			
			i  = i + 10 ;
			
			gfont.generate() ;
			
			
		}
	
	
		try {		
		
			Template template ;
			
			VelocityContext context = new VelocityContext();

			context.put( "fonts", fonts ) ;

			Properties p = new Properties();
			
			String vmPath = "WEB-INF\\templates\\" ;
			 
			p.setProperty( "file.resource.loader.path", vmPath );
			 
			Velocity.init( p );
		   
			template = Velocity.getTemplate( "googlefonts.html.vm" );

			FileWriter w = new FileWriter( "../tmp/googlefonts.html" );

			template.merge( context, w );
			
			w.flush();
			w.close() ;
			
			System.out.println( "done generate fonts in tmp/googlefonts.html" ) ;
			
		} catch( Exception e ) {
			
		   e.printStackTrace();
		
		}
		
		
	
	}
	

}
