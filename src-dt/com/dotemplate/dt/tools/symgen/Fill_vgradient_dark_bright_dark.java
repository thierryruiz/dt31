package com.dotemplate.dt.tools.symgen;


public class Fill_vgradient_dark_bright_dark extends AbstractFill_vgradient implements FillGradient {
	
	
	
	public Fill_vgradient_dark_bright_dark( int gradient, int sortIndex ) {
		super( gradient, sortIndex ) ;
	}


	@Override
	protected String getSymbolFolder() {
		String suffix = "soft" ;
		if ( gradient == FillGradient.STANDARD ) suffix = "standard" ;
		if ( gradient == FillGradient.STRONG ) suffix = "strong" ;
		return "vgradient-dark-bright-dark-" + suffix ;	
	}


	@Override
	protected String getAbstractSymbolFolder() {
		return "abstract-vgradient-dark-bright-dark"	;
	}
	
	
}
