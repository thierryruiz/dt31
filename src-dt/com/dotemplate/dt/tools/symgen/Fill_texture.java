package com.dotemplate.dt.tools.symgen;


import com.dotemplate.theme.shared.ThemeSymbolType;

public class Fill_texture extends AbstractGenerator {

	private String textureId ;
	
			
	public Fill_texture( String textureId, int sortIndex  ) {
		super( sortIndex ) ;
		this.textureId = textureId ;
	}

	
	
	@Override
	protected void initContext() {
		context.put( "id", textureId ) ;
		context.put( "sortIndex", sortIndex  ) ;
	}


	@Override
	protected String getSymbolFolderRoot() {
		return ThemeSymbolType.FILL.getFolder();
	}


	@Override
	protected String getSymbolFolder() {
		return "texture-" + textureId ;	
	}


	@Override
	protected String getAbstractSymbolFolder() {
		return "abstract-texture"	;
	}
	
	
	@Override
	protected void generate() throws Exception {
		super.generate();
		generate_css();
		generate_svgf();
	}
	
	
}
