package com.dotemplate.dt.tools;


import org.apache.commons.lang.StringUtils;

import com.dotemplate.core.server.DesignUtils;
import com.dotemplate.core.server.symbol.SymbolProvider;
import com.dotemplate.core.shared.SymbolType;
import com.dotemplate.dt.server.DoTemplateApp;


/**
 * 
 * Creates symbol thumbnail image
 * 
 * @author Thierry Ruiz
 * 
 */
public class ThumbnailGenerator {


	public static void main( String[] args ) {

		
		( new DoTemplateApp() ) .init()   ;
				
		SymbolType type  = DesignUtils.getSymbolType( args[ 0 ] ) ;
		
		generate ( type ) ;
		
		//generate( "fill-vgradient-dark-bright-dark-standard" ) ;
		//generate( "fill-texture-dark-wood-1" );
		//generate( "fill-texture-dark-wood-1" );
		
	}

	
	
	private static void generate( SymbolType type ) {

		SymbolProvider provider = DesignUtils.getSymbolProvider( type );
		
		System.out.println( provider.getClass().getName() ) ;
		
		provider.load();

		try {
			
			provider.createThumbnails();
		
		} catch ( Exception e ) {
			e.printStackTrace() ;
			throw new RuntimeException(e);
		
		}

	}

	
	
	@SuppressWarnings("unused")
	private static void generate( String name ) {
		
		String folder = StringUtils.substringBefore( name, "-" ) ;
		
		SymbolProvider provider = DesignUtils.getSymbolProvider( folder ) ;
		
		provider.load();

		try {
			
			provider.createThumbnail( name );
			
		
		} catch ( Exception e ) {
			
			throw new RuntimeException(e);
		
		}

	}
	
	
	

}
