package com.dotemplate.xprs.engine;

import java.awt.Rectangle;
import java.io.File;
import java.io.FileOutputStream;
import java.io.StringWriter;

import org.apache.velocity.VelocityContext;

import com.dotemplate.core.server.svg.JPEGTranscoder;



public class VisualEngine {
	
	JPEGTranscoder jpegTranscoder ;
	
	public static void exportFirstVisual(  String serverUrl, String siteUid ) {
		
		StringWriter writer = new StringWriter() ;
		
		VelocityContext context = new VelocityContext() ;
		
		context.put("serverUrl", serverUrl) ;
		context.put("siteUid", siteUid) ;
		
		try {
			
			XprsGenApp.getRenderEngine().generate(context, "svg/visual1.svg.vm", writer);
		
			JPEGTranscoder.transcode(writer.getBuffer().toString(), new Rectangle(699, 700), 0.95f, new FileOutputStream(
					new File( "test.jpg" ) ) ) ;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}
	
	
	

}
