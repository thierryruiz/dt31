package com.dotemplate.xprs.engine;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.App;
import com.dotemplate.core.server.DesignSession;
import com.dotemplate.core.server.affiliate.Affiliate;
import com.dotemplate.core.server.frwk.AppException;
import com.dotemplate.core.server.util.Path;
import com.dotemplate.core.server.util.UIDGenerator;
import com.dotemplate.theme.server.ThemeApp;
import com.dotemplate.theme.server.ThemeContext;
import com.dotemplate.theme.server.ThemeDescriptorNotFound;
import com.dotemplate.theme.server.ThemeManager;
import com.dotemplate.theme.server.cms.CMS;
import com.dotemplate.theme.server.generator.PropertySetGenerator;
import com.dotemplate.theme.server.generator.ThemeGenerator;
import com.dotemplate.theme.shared.Theme;
import com.dotemplate.theme.shared.ThemeSymbolType;


public class XprsThemeManager extends ThemeManager {

	private final static Log log = LogFactory.getLog ( XprsThemeManager.class );
	
	public XprsThemeManager() {
		add ( ThemeSymbolType.DOC ) ;			
	}
	
	
	@Override
	public void init() {
		
		themeGenerator = ( ThemeGenerator ) App.getSingleton ( ThemeGenerator.class ) ;
		setGenerator = ( PropertySetGenerator ) App.getSingleton ( PropertySetGenerator.class ) ;		
		
	}
	

	
	void createTheme( String category, String themeId, String companyName ) {
		
		try {
			
			createTheme( "xprs" , UIDGenerator.pickId( 32 ), "doTemplate", false );
			
			ThemeContext themeContext = ThemeApp.getThemeContext() ;
			
			themeContext.put( "category", category ) ;
			themeContext.put( "companyName", companyName ) ;
			themeContext.put( "xprsTemplate", themeId ) ;

			generateDesign( category, themeId ) ;

			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	
	}
	
	


	@Override
	protected void doCreateTheme ( File descriptor, String name, String uid, String affiliateId, boolean randomize ) throws AppException {
		
		try {
			
			Theme theme =  ThemeApp.getThemeFactory().create( descriptor ) ;
			
			theme.setUid( uid ) ;
			
			theme.setName( name ) ;
			
			XprsThemeContextFactory themeContextFactory = ( XprsThemeContextFactory ) XprsGenApp.getSingleton ( 
					XprsThemeContextFactory.class ) ;
			
			ThemeContext themeContext = themeContextFactory.create ( theme, randomize ) ;
			
			Affiliate affiliate = ThemeApp.getAffiliateManager().find( affiliateId ) ; 
			
			themeContext.setAffiliate( affiliate ) ;
			
			
			DesignSession.set( new DesignSession() );
			
			DesignSession session = DesignSession.get() ;
			
			session.addDesignContext ( themeContext ) ;
			session.setDesignContext ( themeContext ) ;
			
			
		} catch ( ThemeDescriptorNotFound e ){
			
			throw e ;
			
		} catch ( Exception e ) {
			
			e.printStackTrace() ;
			
			log.error( e ) ;
			
			throw new AppException( "Unable to create  theme from decriptor '" 
					+ descriptor.getAbsolutePath() , e  ) ;
			
		} 

	}
	
	
	
	public void generateDesign( String category, String themeId ) throws AppException  {
		
		if ( log.isDebugEnabled () ){
			log.debug ( "generate user theme..." ) ;
		}
		
		ThemeContext themeContext = ThemeApp.getThemeContext() ;
		
		Theme theme = themeContext.getDesign() ;
		
		if ( log.isInfoEnabled () ){
			log.info (  "Generate theme for edition '" + theme.getUid() +  "'..." ) ;
		}
		
		
		Path outputPath = new Path ( App.realPath( ThemeApp.getConfig().getWorkRoot() ) ) ;
		
		outputPath.append( theme.getUid() ) ; 
		
		File outputDir = outputPath.asFile();
		
		if ( log.isDebugEnabled () ){
			log.debug (  "Creating theme directory structure "
					+ outputDir.getAbsolutePath() + "..." ) ;
		}
		
		
		try {
			
			FileUtils.forceMkdir ( outputDir )   ;
			
			CMS.set( themeContext, CMS.XHTML ) ;
			
			themeGenerator.generate () ;
			
			File resDir = new File ( ThemeApp.realPath( ThemeApp.getConfig().getVmTemplatesDir() + "/_symbols/doc/xprs/" + category + "/" + themeId + "/res" ) ) ; 
			
			FileUtils.copyDirectoryToDirectory(resDir, outputDir);
			
			
		} catch ( Exception e ){
			
			log.error( e ) ;

			throw new AppException( "Unable to generate  theme" , e  ) ;
		}
		
		
	}	
	
	
	
	
	
}
