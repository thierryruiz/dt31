package com.dotemplate.xprs.engine;


import java.io.FileWriter;
import java.io.StringWriter;
import java.io.Writer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.context.Context;

import com.dotemplate.core.server.frwk.AppException;
import com.dotemplate.core.server.util.Path;
import com.dotemplate.theme.server.ThemeApp;
import com.dotemplate.theme.server.ThemeContext;
import com.dotemplate.theme.server.cms.CMS;
import com.dotemplate.theme.server.cms.xhtml.XHTMLEngine;


public class XprsExportEngine extends XHTMLEngine {

	private static Log log = LogFactory.getLog( XprsExportEngine.class ) ;
	
	public XprsExportEngine() {
		
	}
	
	
	@Override
	public void export() throws Exception {
		
		ThemeContext themeContext = ThemeApp.getThemeContext() ;
		
		CMS.set (  themeContext, CMS.TYPE_XHTML ) ;
		
		// clearExportFolder() ;
		
		exportDoc() ;
		exportRes() ;
		
	}
	
	
	
	private void exportRes() {
		// TODO Auto-generated method stub
	}



	public void exportDoc() throws Exception {
	
		ThemeContext themeContext = ThemeApp.getThemeContext() ;

		CMS.set (  themeContext, CMS.TYPE_XHTML ) ;
		
		boolean download  = themeContext.isExportMode() ;
		
		Path exportPath ; 
		
		Writer writer ;
		
		String vmTemplate, file, exportDir ; 
								
		file = "index.html" ;
		
		exportDir = download ? ThemeApp.get().getExportDirectoryRealPath() :
			ThemeApp.get().getWorkRealPath() ;
		
		exportPath = new Path ( exportDir ).append ( file ) ;
		
		writer = ( download ) ?  new StringWriter() : new FileWriter( exportPath.toString () ) ;
		
		vmTemplate =  "_symbols/doc/xprs/html.vm" ;
		
		VelocityContext context = new VelocityContext ( themeContext  );
		
		
		try {
			
			engine.mergeTemplate( vmTemplate ,"UTF-8", context, writer ) ;
			

		} catch ( Exception e ) {
			
			log.error( e ) ;
			throw new AppException( "Failed to export to xhtml.", e ) ;
		
		} finally {

			try {
				writer.flush () ;
				writer.close () ;
			} catch ( Exception e ){
				
				log.error ( e.getMessage (), e ) ;
			
			} 
		}
		
		
		if ( log.isDebugEnabled() ){
			log.debug( "Export successful" ) ;
		}
		
	}

	
	
//	protected void clearExportFolder() {
//		
//		File exportDir = new Path( ThemeApp.get().getExportDirectoryRealPath() ).asFile() ;
//		
//		if ( exportDir.exists() ){
//			try {
//				FileUtils.cleanDirectory( exportDir ) ;
//			} catch ( Exception e ) {
//				throw new AppRuntimeException( "Failed to export template ", e ) ;
//			}
//		}
//	}
   
    
    

	
}
