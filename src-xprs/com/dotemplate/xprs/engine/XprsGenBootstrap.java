package com.dotemplate.xprs.engine;

public class XprsGenBootstrap {
	
	public static void main( String[] args ){
	
		XprsGenApp app = new XprsGenApp() ;			
		
		app.init() ;
		app.runCampaign() ;
	
		System.out.println( "Bye!" ) ;
		System.exit(0 );

		
	}
	
	
}
