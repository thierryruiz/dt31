/**
 * Copyright Thierry Ruiz - 2004-2008. All rights reserved.
 * Created on 4 mars 08 - 12:04:39
 *
 */
package com.dotemplate.xprs.engine;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.DesignContextFactory;
import com.dotemplate.theme.server.BaseThemeContext;
import com.dotemplate.theme.server.DevModeThemeContext;
import com.dotemplate.theme.server.ThemeApp;
import com.dotemplate.theme.server.ThemeContext;
import com.dotemplate.theme.server.ThemeHelper;
import com.dotemplate.theme.shared.Theme;



/**
 * 
 * 
 * @author Thierry Ruiz
 *
 */
public class XprsThemeContextFactory extends DesignContextFactory< Theme > {
		
	private final static Log log = LogFactory.getLog ( XprsThemeContextFactory.class );
		
	private XprsThemeContextFactory() {}
	
	
	public ThemeContext create( Theme theme ) {
		return create( theme, false ) ;
	}
	
	public ThemeContext create( Theme theme, boolean randomize ) {
	
		if ( log.isDebugEnabled () ){
			log.debug (  "Creating new ThemeContext..." ) ;
		}
	
		XprsGenConfig config = ( XprsGenConfig ) XprsGenApp.getXprsGenConfig ();
		
		ThemeContext ctx = ThemeApp.isDevMode () ? new DevModeThemeContext(): new BaseThemeContext() ;
				
		ThemeHelper helper = new ThemeHelper() ;
		
    	ctx.put ( ThemeContext.THEME, theme ) ;
    	

		ctx.put ( ThemeContext.HELPER, helper ) ;
		ctx.put ( "_", helper ) ; // alternate key for shorter syntax in vm files
				
		ctx.put ( ThemeContext.EXPORT, false ) ;
		ctx.put ( ThemeContext.WORK_DIR, "sites/" + theme.getUid () + "/" ) ;
		ctx.put ( ThemeContext.VOID_LINK, "onclick='javascript:return false;'" ) ;
				
		
		if ( ThemeApp.isDevMode () ){
			ctx.put ( ThemeContext.DEV_MODE, true ) ;
		}
						
		if ( config.isTestMode() ){
			ctx.put( ThemeContext.TEST_MODE, true ) ;
		}
				
		ctx.preprocess() ;
		
		return ctx ;
	
	}

}
