package com.dotemplate.xprs.engine;

import java.io.File;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.DesignMerger;
import com.dotemplate.core.server.frwk.AppException;
import com.dotemplate.core.server.util.Path;
import com.dotemplate.theme.server.ThemeApp;
import com.dotemplate.theme.server.ThemeDescriptorNotFound;
import com.dotemplate.theme.server.ThemeDescriptorReader;
import com.dotemplate.theme.server.ThemeDescriptorReaderPool;
import com.dotemplate.theme.server.ThemeFactory;
import com.dotemplate.theme.shared.Theme;


public class XprsThemeFactory extends ThemeFactory {

	private final static Log log = LogFactory.getLog ( XprsThemeFactory.class ) ;
	

	public Theme create( File descriptor ) throws AppException {
		
		if( !descriptor.exists() ){
			
			throw new ThemeDescriptorNotFound( "Theme descriptor " + descriptor.getAbsolutePath() + " not found " ) ;
		
		}

		
		if ( themeReaderPool == null  ){		
			themeReaderPool = new ThemeDescriptorReaderPool () ;
		}
		
		
		Theme theme = null ;

		ThemeDescriptorReader themeReader = null ;
			
		try {
			
			themeReader =  ( ThemeDescriptorReader ) themeReaderPool.borrow() ;
						
			theme = themeReader.read ( descriptor  ) ;
			
			
		} catch ( Exception e ) {
			
			log.error( e ) ;

			throw new AppException( "Unable to generate theme from decriptor '" 
					+ descriptor.getAbsolutePath() , e  ) ;
			
		} finally {
			
			try {
				
				themeReaderPool.release( themeReader ) ;
			
			} catch ( Exception e ) {
				
				throw new AppException( "Failed to return ThemeReader instance to pool! " , e )  ;
			
			}
		}
		
		
		String model = theme.getParent() ;
		
		
		if ( model == null ) {
			return theme ;
		}
	
	
		Path modelPath = new Path ( ThemeApp.realPath( ThemeApp.getConfig ().getModelsPath () ) )
			.append( model ).append( "theme.xml" ) ;
		
		
		if ( ! modelPath.asFile().exists() ){
			throw new AppException ( "Unable to locate parent theme file '" + model + "' descriptor" ) ;
		}
		
		
		Theme parentTheme = create ( modelPath.asFile() ) ;
		
		parentTheme.setName( model ) ;
		
//		if( ! model.startsWith("abstract") ){
//			
//			ThemeUtils.applyColorSchemeToModel( parentTheme, theme.getScheme() ) ;	
//		}
		
		try {
			
			DesignMerger.merge( theme, parentTheme ) ;
			
		} catch ( Exception e ) {
			
			throw new AppException( "Unable to build theme " + descriptor.getAbsolutePath() + " from its parent", e ) ;
		
		}
		
		return theme ;
		
	}
	
	
	
	
	
	
}
