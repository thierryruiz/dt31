package com.dotemplate.xprs.engine;

import com.dotemplate.core.server.AppConfig;
import com.dotemplate.core.server.AppConfigReader;
import com.dotemplate.core.server.RenderEngine;
import com.dotemplate.theme.server.ThemeApp;
import com.dotemplate.theme.server.cms.ExportEngine;
import com.dotemplate.xprs.mail.EmailBuilder;
import com.dotemplate.xprs.mail.EmailService;

public class XprsGenApp extends ThemeApp {
	
	private EmailBuilder emailBuilder ;
	
	private EmailService emailService ;
	
	
	@Override
	protected String getConfigFileName() {
		return "application.xprs.xml" ;
	}
	

	public static XprsGenApp get() {
		return ( XprsGenApp ) _app ;
	}
	
	
	
	@Override
	public void init( String realPath, AppConfig config ) {
		this.config = config ;
		this.realPath = realPath ;
		
		designManager = ( XprsThemeManager ) getSingleton( config.getDesignManager() ) ;
		designManager.init() ;
		
		renderEngine = ( RenderEngine ) getSingleton( config.getRenderEngine() ) ;
		emailBuilder = new EmailBuilder() ;
		emailService = new EmailService("smtp.gmail.com", 465, "dotemplate@gmail.com", "saisaiboomak") ;

	}
	
	
	public void init () {
		this.config = ( new AppConfigReader< XprsGenConfig >( XprsGenConfig.class ) ).read(
				getConfigFileName() ) ;	
		super.init( config ) ;
	}
	
	
	@Override
	public ExportEngine getExportEngine(String cmsId) {
		return getExportEngine() ;
	}



	public static XprsGenConfig getXprsGenConfig() {
		return ( XprsGenConfig ) getConfig();
	}
	
	public static XprsExportEngine getExportEngine(){
		return (XprsExportEngine) XprsGenApp.getSingleton ( XprsExportEngine.class ) ;
	}


	public void runCampaign() {
		
		XprsThemeManager themeManager = ( XprsThemeManager ) getThemeManager() ;
		
		System.out.println( "Starting campaign...") ;
		
		themeManager.createTheme( "architecture", "architecture1", "Sawitri SARL");
		
		String siteUid = ThemeApp.getTheme().getUid() ;
		
		System.out.println( "Site " + siteUid + " created."  ) ;
		
		Process takeScreenShot;		
		
		try {
			
			System.out.println( "Capturing screenshots..."  ) ;
			takeScreenShot = Runtime.getRuntime().exec( "C:\\_Data\\__git\\workspace-31\\dt31\\xprs\\screenshot\\node_modules\\casperjs\\bin\\casperjs.exe screenshot/widescreen.js --site=" + siteUid  );
			takeScreenShot.waitFor() ;
			takeScreenShot = Runtime.getRuntime().exec( "C:\\_Data\\__git\\workspace-31\\dt31\\xprs\\screenshot\\node_modules\\casperjs\\bin\\casperjs.exe screenshot/smallscreen.js --site=" + siteUid  );
			takeScreenShot.waitFor() ;
			
			
			VisualEngine.exportFirstVisual("http://demo.123clic.com", siteUid);
		
			
		} catch (Exception e) {

			e.printStackTrace();
		}

		
		try {
			String emailContent = emailBuilder.getEmailContent("Neo Archi", siteUid, "http://demo.123clic.com" );
			emailService.sendEmail("thierry.ruiz@gmail.com", emailContent );
		} catch (Exception e) {
			e.printStackTrace();
		}
		

	}
	
	
	
}
