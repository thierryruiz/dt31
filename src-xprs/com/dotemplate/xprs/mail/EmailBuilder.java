package com.dotemplate.xprs.mail;

import java.io.StringWriter;

import org.apache.velocity.VelocityContext;

import com.dotemplate.xprs.engine.XprsGenApp;

public class EmailBuilder {
	
	public String getEmailContent( String companyName, String siteUid, String serverUrl ) throws Exception {
		
		StringWriter writer = new StringWriter() ;
	
		VelocityContext context = new VelocityContext() ;
		
		context.put("companyName", companyName) ;
		context.put("serverUrl", serverUrl) ;
		context.put("siteUid", siteUid) ;
		
		XprsGenApp.getRenderEngine().generate(context, "email/template-modified.html", writer);
		
		return writer.getBuffer().toString() ;
		
		
	}

}
