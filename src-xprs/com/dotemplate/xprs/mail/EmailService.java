package com.dotemplate.xprs.mail;

import org.apache.commons.mail.HtmlEmail;

/**
 * 
 * dotemplate Gmail account password saisa...
 * darooze Hotmail account for test
 * 
 * 
 * @author Admin
 *
 */
public class EmailService {

	private String smtpHost ;
	
	private String smtpAccount ;
	
	private String smtpKey ;
	
	private int smtpPort ;
	
	
	public EmailService( String smtpHost, int smtpPort, String smtpAccount, String smtpKey ) {
		
		this.smtpHost = smtpHost ;
		this.smtpPort = smtpPort ;
		this.smtpAccount = smtpAccount ;
		this.smtpKey = smtpKey ;
		
	}
	
	
	public void sendEmail ( String recipient,  String content ) {
		
		System.out.println( "Sending email to " + recipient );

		HtmlEmail email = new HtmlEmail ();
		email.setCharset("utf-8");
		
		try {

			email.setHostName ( smtpHost ) ;
			email.setAuthentication ( smtpAccount, smtpKey ) ;
			email.setSSL ( true ) ;
			
			email.setSslSmtpPort ( "" + smtpPort ) ;
			email.setSmtpPort (  smtpPort ) ;
						
			email.addTo ( recipient ) ;
			//email.addTo ( "thierry.ruiz@gmail.com" ) ;
			email.setSubject ( "Nous avons cr�� un site Internet pour votre entreprise. D�couvrez le!" )  ;
			email.setFrom ( smtpAccount , "123Clic.com" ) ;
						
			// set the html message
			email.setHtmlMsg( content );

			email.setBounceAddress ( "thierry.ruiz@gmail.com" );
			
			email.setTLS(true);
			
			
			
			email.send ();
			
		} catch ( Exception e ) {
			e.printStackTrace () ;
		}

	}
	
		

}
