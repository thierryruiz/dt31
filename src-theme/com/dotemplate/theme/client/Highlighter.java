package com.dotemplate.theme.client;

import com.google.gwt.dom.client.Element ;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;


public class Highlighter {
	
	private AbsolutePanel north ;
	
	private AbsolutePanel south ;
	
	private AbsolutePanel east ;
	
	private AbsolutePanel west ;
	
	
	public Highlighter () {
		
		RootPanel root = RootPanel.get ();
		
		south = new AbsolutePanel() ;
		south.add ( new Label("") ) ;
		south.setStyleName ( "hl" ) ;
		root.add( south ) ;
		
		north = new AbsolutePanel() ;
		north.add ( new Label("") ) ;
		north.setStyleName ( "hl" ) ;
		root.add( north ) ;
		
		east = new AbsolutePanel() ;
		east.add ( new Label("") ) ;
		east.setStyleName ( "hl" ) ;
		root.add( east ) ;

		west = new AbsolutePanel() ;
		west.add ( new Label("") ) ;
		west.setStyleName ( "hl" ) ;
		root.add( west ) ;	
	}

	
	public void highlight( Element topEl, Element  bottomEl ) {
		
		int top =  DOM.getAbsoluteTop ( topEl )  ;
		int left = DOM.getAbsoluteLeft ( topEl )  ;

		int width = topEl.getOffsetWidth () ;
		
		if ( width  > Window.getClientWidth () ) width = width - 1 ;
		
		
		int height = topEl.getOffsetHeight ()  ;
		
		if ( bottomEl != null ){
			height = DOM.getAbsoluteTop ( bottomEl ) + bottomEl.getOffsetHeight () - top ;
		}
		
		RootPanel root = RootPanel.get ();

    	root.setWidgetPosition ( north, left , top  ) ;
    	north.setPixelSize ( width , 4 ) ;
    	
		root.setWidgetPosition( south, left ,  top + height - 4 ) ;
		south.setPixelSize ( width , 4 ) ;
    	
		
    	root.setWidgetPosition ( west, left , top  ) ;
    	west.setPixelSize ( 4 , height ) ;
    	
		root.setWidgetPosition( east, left + width - 4 ,  top ) ;
		east.setPixelSize ( 4 , height ) ;
    
    	setVisible ( true ) ;
	}

	
	public void setVisible ( boolean visible ) {
		south.setVisible (  visible ) ;
		north.setVisible (  visible ) ;
		east.setVisible (  visible ) ;
		west.setVisible (  visible ) ;
	
	}
	
	
	
}
