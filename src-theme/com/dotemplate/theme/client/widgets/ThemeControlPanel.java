package com.dotemplate.theme.client.widgets;


import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.RootPanel;


@Deprecated
public class ThemeControlPanel extends PopupPanel {

	public static final int HEIGHT = 125;
	
	protected ThemeTabPanel tabPanel ;
	
	public ThemeControlPanel () {
		
		super( false ) ;
		setPopupPosition ( -1, 0 ) ;
		setStyleName( "ez-controlPanel" ) ;
		setHeight( HEIGHT + "px" ) ;
		
		add( tabPanel = new ThemeTabPanel() ) ;

	}

	
	@Override
	public void show () {
		super.show ();
		DOM.setStyleAttribute ( this.getElement(), "position", "fixed" ) ;
		DOM.setStyleAttribute ( RootPanel.getBodyElement(), "marginTop", HEIGHT + "px" ) ;
	}
	
	
	public ThemeTabPanel getTabPanel() {
		return tabPanel;
	}
	
	

}
