package com.dotemplate.theme.client.widgets;

import java.util.HashMap;

import com.dotemplate.core.client.editors.PropertyEditor;
import com.dotemplate.core.client.editors.PropertySetEditor;
import com.dotemplate.core.shared.properties.Property;
import com.dotemplate.core.shared.properties.PropertySet;
import com.extjs.gxt.ui.client.widget.TabPanel;
import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;


@Deprecated
public class ThemeTabPanel implements IsWidget {

	protected TabPanel extTabs ;
	
	protected HashMap < String, GroupTab >  groupTabs ;
	
	public ThemeTabPanel() {
		
		extTabs = new TabPanel() ;
		extTabs.setAnimScroll(true);  
		extTabs.setTabScroll(true);  
		extTabs.setHeight( ThemeControlPanel.HEIGHT ) ;

		Window.addResizeHandler( new ResizeHandler( ) {
			@Override
			public void onResize( ResizeEvent e ) {
				extTabs.setWidth( "100%" ) ;
			}
		});
		
		groupTabs = new HashMap < String, GroupTab >() ;
		
		addSchemeTab() ;

	}
	

	protected void addSchemeTab() {
		extTabs.add( new SchemeTab().asTab() ) ;
	}

	public void showTab( PropertySet set ) {
		
		GroupTab tab = groupTabs.get( set.getGroup() );
		
		extTabs.setSelection( tab.asTab() ) ;
		
		extTabs.scrollToTab( tab.asTab(), true ) ;
	
	}

	
	@Override
	public Widget asWidget() {
		return extTabs ;
	}
	
	
	public void add ( PropertySetEditor editor ){
		
		String group = editor.getProperty().getGroup() ;
		
		if ( group == null ){
			return ;
		}

		GroupTab groupTab = groupTabs.get( group ) ;
		
		if ( groupTab == null ){
			
			groupTab = new GroupTab( group ) ; 
			groupTabs.put( group, groupTab ) ;
			extTabs.add( groupTab.asTab() ) ;
			groupTab.asTab().setId( "grp_" + group.replace( ' ', '_' ) ) ;
		}

		
		groupTab.addEditor( editor ) ;

	}
	
	
	
	public void add ( PropertySetEditor parentEditor,  PropertyEditor<? extends Property > editor ){
		
		try {
			parentEditor.getControl().add( editor.asWidget() ) ;
		} catch ( Exception  e ){
			e.printStackTrace() ;
			return ;
			
		}
			
			
		Property p = editor.getProperty() ;
		
		boolean enable = p.getEnable() != null &&  p.getEnable () ;
		
		if( enable ){
			editor.enable() ;
		} else {
			editor.disable() ;
		}
		
		// deprecated
		//parentEditor.getControl().layout() ;
	}
	
	
	public void insert ( PropertySetEditor parentEditor,  PropertyEditor<? extends Property > editor, int index ){
		parentEditor.getControl().insert( editor.asWidget(), index ) ;
		//deprecated parentEditor.getControl().layout() ;
	}
	

	
	
}
