package com.dotemplate.theme.client.widgets;



import com.extjs.gxt.ui.client.widget.TabItem;
import com.extjs.gxt.ui.client.widget.toolbar.ToolBar;
import com.google.gwt.user.client.ui.DockPanel;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;

@Deprecated
public abstract class ThemeTabItem implements IsWidget {
	
	protected TabItem extTabItem ;
			
	protected ToolBar extToolbar ;	
	
	protected ThemeTabItem( String label ) {
		extTabItem = new TabItem( label ) ;
		extTabItem.setWidth( "100%" ) ; 
	}
	
	
	@Override
	public Widget asWidget() {
		return extTabItem ;
	}
	
	
	public TabItem asTab() {
		return extTabItem ; 
	}

	
	public boolean isSetTab () {
		return false ;
	}
	
	
	public void init() {
		
		DockPanel dock = new DockPanel() ;
		dock.addStyleName("ez-toolbar" ) ;
		dock.setWidth( "100%" ) ; 
		
		extToolbar = new ToolBar() ;
		extToolbar.setHeight( ThemeControlPanel.HEIGHT ) ;		
		extToolbar.setWidth( "100%" ) ; 
		
		dock.add( extToolbar, DockPanel.WEST ) ;
		dock.setCellWidth(extToolbar, "80%") ;
		
		RightPanel rightPanel = new RightPanel() ;
		dock.add( rightPanel, DockPanel.EAST ) ;
		dock.setCellHorizontalAlignment( rightPanel, DockPanel.ALIGN_RIGHT ) ;
		
		extTabItem.add( dock ) ;
	
	}
	
	

}
