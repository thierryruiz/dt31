package com.dotemplate.theme.client.widgets;

import com.dotemplate.core.client.editors.PropertyEditorControl;
import com.dotemplate.core.client.widgets.resource.DesignResourceChangeHandler;
import com.dotemplate.core.shared.Scheme;
import com.dotemplate.theme.client.ThemeClient;
import com.extjs.gxt.ui.client.util.IconHelper;
import com.extjs.gxt.ui.client.widget.button.ButtonGroup;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Widget;


@Deprecated
public class SchemeTab extends ThemeTabItem implements DesignResourceChangeHandler< Scheme >{

	protected ButtonGroup extButtons ;
	
	public SchemeTab() {
		
		super( "Color scheme" ) ;
		
		init() ; 
		
		extTabItem.setIcon( IconHelper.createPath( "client/icons/themetoolbar/scheme-tab.png", 16, 16 ) ) ;

		extButtons = new ButtonGroup( 1 ) ;
		extButtons.setHeading( "Color scheme" ) ;
		
		extToolbar.add( extButtons ) ;
		
		PropertyEditorControl schemeBtn = new PropertyEditorControl(){

			@Override
			protected Widget createControl() {
				// TODO Auto-generated method stub
				return new Button ( "fixme");
			}

			@Override
			public void enable() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void disable() {
				// TODO Auto-generated method stub
				
			}

			@Override
			protected Widget createIcon() {
				// TODO Auto-generated method stub
				return null;
			}
			
		};
		schemeBtn.setText( "Change scheme" ) ;
		schemeBtn.setIcon ( "client/icons/themetoolbar/schemes.png" ) ;

		/* FIXME NG 
		schemeBtn.addClickHandler( new ClickHandler( ) {
			@Override
			public void onClick( ClickEvent arg0 ) {
				SchemeSelector.show ( SchemeTab.this, ThemeClient.get().getColorScheme() ) ;
			}
		}) ;*/
		
		extButtons.add( schemeBtn.asWidget() ) ;
					
		extTabItem.layout( true ) ;
		extTabItem.show();
	
	}


	@Override
	public void onValueChange(ValueChangeEvent<Scheme> event) {
		//ThemeClient.get().changeColorScheme( event.getValue() );	
	}
	
	
	
	
}
