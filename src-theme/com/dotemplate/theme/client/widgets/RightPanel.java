package com.dotemplate.theme.client.widgets;

import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;



public class RightPanel implements IsWidget {

	protected VerticalPanel layout ;

	
	public RightPanel() {

		Image logoIcon = new Image( "client/icons/themetoolbar/logo-dt.png" ) ;
		
		/*
		logoIcon.addClickHandler( new ClickHandler(){
			@Override
			public void onClick(ClickEvent arg0) {
				Window.open( "http://www.dotemplate.com", "_self", "" );
			}
		});
		*/
		
		
		layout = new VerticalPanel() ;
		layout.setStyleName( "ez-right-panel" ) ;
		layout.setHorizontalAlignment( HorizontalPanel.ALIGN_CENTER ) ;

		layout.add( logoIcon );
		
		String at = "@" ;
		layout.add( new HTML( "<b>doTemplate v3.0 </b><br/>email" + at + "dotemplate.com" ) ) ;
			
	}
	
	
	@Override
	public Widget asWidget() {
		return layout ;
	}

}
