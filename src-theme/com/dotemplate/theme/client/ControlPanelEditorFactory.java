package com.dotemplate.theme.client;

import com.dotemplate.core.client.editors.EditorFactory;

public class ControlPanelEditorFactory extends EditorFactory {

	
	private static ControlPanelEditorFactory instance  ;
		
	
	public static ControlPanelEditorFactory get() {
		
		if ( instance == null ) {
			instance = new ControlPanelEditorFactory() ;
		}
		
		return instance ;
		
	}
	
}
