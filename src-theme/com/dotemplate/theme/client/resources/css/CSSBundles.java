package com.dotemplate.theme.client.resources.css;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;

public interface CSSBundles extends ClientBundle {
    
    public static CSSBundles INSTANCE = GWT.create (CSSBundles.class);
    
    //@Source("boot-side-menu.css")
    //TextResource getBootSideMenuCss();
    
}