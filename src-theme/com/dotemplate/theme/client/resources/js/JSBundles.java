package com.dotemplate.theme.client.resources.js;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;

public interface JSBundles extends ClientBundle {
    
    public static JSBundles INSTANCE = GWT.create (JSBundles.class);
    
    //@Source("BootSideMenu.js")
    //TextResource getBootSideMenuJS();
    
}