package com.dotemplate.theme.client;

import java.util.logging.Logger;

import com.dotemplate.core.client.editors.PropertyEditor;
import com.dotemplate.core.client.frwk.Utils;
import com.dotemplate.core.shared.properties.Property;
import com.dotemplate.core.shared.properties.PropertySet;
import com.google.gwt.dom.client.NodeList;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.DOM;
import com.google.gwt.dom.client.Element;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.EventListener;



@SuppressWarnings("unused")
public class ThemeDecorator implements HasClickHandlers {
	
	private static Logger logger = Logger.getLogger( ThemeDecorator.class.getName() );

	private static final long serialVersionUID = 1L;

	private Highlighter highlighter ;

	protected HandlerManager handlerManager ; 
	
	protected PropertySet selected = null ;
	
	public ThemeDecorator() {
		
		handlerManager = new HandlerManager( this ) ;
	
		highlighter = new Highlighter() ;
	
	}
	

	public void decorate( PropertySet set ){
		
		// Log.debug ( "Decorate customizable " + set.getName() ) ;
		
		String selectors = set.getSelectors () ;
		
		String styleClass = set.getStyleClass() ;

		String highlight = set.getHighlight () ;

		Element topEl, bottomEl ; topEl = bottomEl = null ;
		
		
		// Get the top and bottom elements defining the borders to highlight for this set
		if ( highlight != null ){
			
			String[] els = highlight.split ( "," ) ;
			
			topEl = DOM.getElementById ( els[ 0 ].trim () ) ;
			
			bottomEl = DOM.getElementById ( els[ 1 ].trim () ) ;	
		
		}
		
		
		if ( selectors  != null ){
			
			String[] xpath = selectors.split ( "," ) ;
						
			for( int i = 0 ; i < xpath.length ; i++ ){
				
				NodeList< com.google.gwt.user.client.Element > nodes = Utils.selectDOM ( xpath [ i ] )  ;
		
				for( int j = 0 ; j < nodes.getLength() ; j++ ){
					
					decorate( nodes.getItem ( j ), set, topEl, bottomEl ) ;
				
				}
			}
			
		} 
		
		
		/*
		else if ( styleClass != null ){
			
			NodeList<Element> nodes = Utils.selectDOM ( "div[class="+styleClass+ "]" )  ;
			
			for( int i = 0 ; i < nodes.getLength() ; i++ ){
				
				decorate( nodes.getItem ( i ), set, null, null ) ;
			
			}
			
		} else {
			
		} ;
		*/
		
		for ( Property property : set.getPropertyMap ().values() ) {
						
			if ( property.isSet() ){
				
				decorate ( ( PropertySet ) property ) ;
			
			}
		}
	}

	
	private  void decorate ( 
			final Element element, 
			final PropertySet set, 
			final Element topEl, 
			final Element bottomEl ){
		

		//Log.debug ( "Decorating customizable element for " + set.getName() ) ;
		
		if ( element == null ){
			return ;
		}
		
		// intercept menu click
		DOM.sinkEvents( element, Event.MOUSEEVENTS ) ;
		
		DOM.setStyleAttribute ( element, "cursor", "pointer" ) ;
				
		DOM.setEventListener( element, new EventListener() {
			
	            public void onBrowserEvent( Event event ) {
	
	            	switch ( DOM.eventGetType( event ) ){
	            	
	            		case ( Event.ONMOUSEDOWN ):
	            			selected = set ;
	            			fireEvent( new ClickEvent(){} ) ;
	            			event.stopPropagation ();
	                        return ;
	                        
	                        
	                    case ( Event.ONMOUSEOVER ):
	                    	if ( topEl != null ){
	                    		highlighter.highlight ( topEl, bottomEl ) ;
	                    	} else {
	                    		highlighter.highlight ( element, null ) ;
	                    	}

	                    	event.stopPropagation ();
	                    	return ;
	                    	
	                    case ( Event.ONMOUSEOUT ):
	                    	highlighter.setVisible ( false ) ;
	                    	event.stopPropagation ();
	                    	return ;
	                }
	            }
		}) ;
	}
	
	
	public PropertySet getSelected() {
		return selected;
	}
	
	
	@Override
	public void fireEvent( GwtEvent<?> e ) {
		handlerManager.fireEvent( e ) ;
	}



	@Override
	public HandlerRegistration addClickHandler(ClickHandler handler ) {
		return handlerManager.addHandler( ClickEvent.getType(), handler );
	}

	
	public void redecorate(){
		
		PropertySet set ;
		
		for( PropertyEditor<? extends Property >  e : ThemeClient.get().getEditor().getPropertyEditors().values() ){
			Property p = e.getProperty() ;
			if( !p.isSet() ) continue ;
			set = ( PropertySet ) p ;
			if( set.selectable() ){
				decorate( set ) ;
			}
		}
		
	}
	


}


