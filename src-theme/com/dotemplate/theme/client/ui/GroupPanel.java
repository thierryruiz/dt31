package com.dotemplate.theme.client.ui;

import org.gwtbootstrap3.client.ui.PanelBody;

import com.dotemplate.core.client.editors.PropertySetEditor;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public class GroupPanel extends Composite {

	private static GroupPanelUiBinder uiBinder = GWT
			.create(GroupPanelUiBinder.class);

	interface GroupPanelUiBinder extends UiBinder<Widget, GroupPanel> {
	}


	@UiField
	PanelBody body;


	public GroupPanel() {
		initWidget( uiBinder.createAndBindUi(this));
	}

	public void addEditor(PropertySetEditor editor) {
		body.add(editor.asWidget());
	}
	
	public void setId(String id) {
		this.getElement().setId(id);
	}
	
	
	
}
