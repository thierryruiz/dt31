package com.dotemplate.theme.client.ui;

import org.gwtbootstrap3.client.ui.PanelGroup;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;


public class Accordion extends Composite  {

	private static AccordionUiBinder uiBinder = GWT
			.create(AccordionUiBinder.class);

	interface AccordionUiBinder extends UiBinder<Widget, Accordion> {
	}
	
	@UiField
	PanelGroup accordion ;
	

	public Accordion() {
		initWidget(uiBinder.createAndBindUi(this));		
	}

	public Accordion(String firstName) {
		initWidget(uiBinder.createAndBindUi(this));
	}

	public void add( Widget w ){
		accordion.add( w );
		
	}
	
	
	
	

}
