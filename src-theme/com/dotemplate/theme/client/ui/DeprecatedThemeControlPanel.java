package com.dotemplate.theme.client.ui;

import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.gwtbootstrap3.client.shared.event.ShowEvent;
import org.gwtbootstrap3.client.shared.event.ShowHandler;

import com.dotemplate.core.client.editors.PropertyEditor;
import com.dotemplate.core.client.editors.PropertySetEditor;
import com.dotemplate.core.client.widgets.SideMenu;
import com.dotemplate.core.shared.properties.Property;
import com.dotemplate.theme.client.view.ThemePropertyView;
import com.google.gwt.user.client.ui.RootPanel;


@Deprecated
public class DeprecatedThemeControlPanel extends SideMenu implements ThemePropertyView {

	private static Logger logger = Logger.getLogger( DeprecatedThemeControlPanel.class.getName() );

	protected HashMap < String, GroupPanel >  groupPanels ;
	
	protected Accordion layout ;
	
	
	public DeprecatedThemeControlPanel() {
		
		groupPanels = new HashMap< String, GroupPanel >() ;
		
		//setBody ( layout = new Accordion() ) ;
	
	}
	    
	public void show() {
		RootPanel.get().add( this );
	}
	

	public void add ( PropertySetEditor editor ){
		
		String group = editor.getProperty().getGroup() ;
		
		if ( group == null ){
			return ;
		}

		
		GroupPanel groupPanel = groupPanels.get( group ) ;
		
		if ( groupPanel == null ){
			
			groupPanel = new GroupPanel( /*this*/ ) ; 
			//groupPanel.setLabel(group);
			groupPanels.put( group, groupPanel ) ;
						
			layout.add( groupPanel ) ;
			
			groupPanel.setId( "grp_" + group.replace( ' ', '_' ) ) ;
		}

		
		groupPanel.addEditor( editor ) ;
		
	}
	
	
	
	public void add ( PropertySetEditor parentEditor,  PropertyEditor<? extends Property > editor ){
		
		logger.log( Level.FINE, "Adding property editor '" + editor.getProperty().getLongName() 
				+ "' to parent editor " + parentEditor.getEditorId() + " ") ;
		
		try {
			parentEditor.getControl().add( editor.asWidget() ) ;
		} catch ( Exception  e ){
			logger.log(Level.SEVERE , e.getMessage() ) ;
			throw e ;
			
		}
			
			
		Property p = editor.getProperty() ;
		
		boolean enable = p.getEnable() != null &&  p.getEnable () ;
		
		if( enable ){
			editor.enable() ;
		} else {
			editor.disable() ;
		}
				
	}
	
	
	public void insert ( PropertySetEditor parentEditor,  PropertyEditor<? extends Property > editor, int index ){
		parentEditor.getControl().insert( editor.asWidget(), index ) ;
	}


	@Override
	public String getId() {
		return "DTControlPanel";
	}


	
	public void select( PropertySetEditor editor ) {		
		
		GroupPanel groupPanel = groupPanels.get( editor.getProperty().getGroup() ) ;
		//groupPanel.expand();
		
		editor.select();
		
	}
	
	
	
}
