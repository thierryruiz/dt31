package com.dotemplate.theme.client.ui;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.dotemplate.core.client.editors.PropertyEditor;
import com.dotemplate.core.client.editors.PropertySetEditor;
import com.dotemplate.core.client.widgets.ListBox;
import com.dotemplate.core.client.widgets.SideMenu;
import com.dotemplate.core.shared.properties.Property;
import com.dotemplate.theme.client.view.ThemePropertyView;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.RootPanel;


public class ThemePropertyPanel extends SideMenu implements ThemePropertyView {

	private static Logger logger = Logger.getLogger( ThemePropertyPanel.class.getName() );

	protected HashMap < String, GroupPanel >  groupPanels ;
	
	protected int deckIndex = 0 ;
	
	protected GroupListBox groupListBox ; 
	
	public ThemePropertyPanel() {
		
		groupPanels = new LinkedHashMap< String, GroupPanel >() ;
		
		groupListBox = new GroupListBox() ;
		
		groupListBox.addClickHandler(new ClickHandler() {
			@Override
			public void onClick( ClickEvent event) {
				String group = (String ) event.getSource() ;
				controlsContainer.showWidget( controlsContainer.getWidgetIndex( groupPanels.get( group ) ) );
			}
		}) ;
		
		
		top.add(groupListBox);
		
	}
	
    
	
	public void show() {
		RootPanel.get().add( this );
		
		
	}
	

	public void add ( PropertySetEditor editor ){
		
		final String group = editor.getProperty().getGroup() ;
		
		if ( group == null ){
			return ;
		}

		
		GroupPanel groupPanel = groupPanels.get( group ) ;
		
		if ( groupPanel == null ){
			
			groupListBox.add( group );
			
			groupPanel = new GroupPanel() ; 
			
			groupPanels.put( group, groupPanel ) ;
			
			controlsContainer.add( groupPanel ) ;
			
			groupPanel.setId( "grp_" + group.replace( ' ', '_' ) ) ;
		
		}

		
		groupPanel.addEditor( editor ) ;
		
	}
	
	
	
	public void add ( PropertySetEditor parentEditor,  PropertyEditor<? extends Property > editor ){
		
		logger.log( Level.FINE, "Adding property editor '" + editor.getProperty().getLongName() 
				+ "' to parent editor " + parentEditor.getEditorId() + " ") ;
		
		try {
			
			parentEditor.getControl().add( editor.asWidget() ) ;
		
		} catch ( Exception  e ){
			logger.log(Level.SEVERE , e.getMessage() ) ;
			throw e ;
			
		}
			
			
		Property p = editor.getProperty() ;
		
		boolean enable = p.getEnable() != null &&  p.getEnable () ;
		
		if( enable ){
			editor.enable() ;
		} else {
			editor.disable() ;
		}
				
	}
	
	
	public void insert ( PropertySetEditor parentEditor,  PropertyEditor<? extends Property > editor, int index ){
		parentEditor.getControl().insert( editor.asWidget(), index ) ;
	}


	@Override
	public String getId() {
		return "DTControlPanel";
	}


	
	public void select( PropertySetEditor editor ) {		
		
		String group = editor.getProperty().getGroup();
		
		GroupPanel groupPanel = groupPanels.get( group ) ;
		controlsContainer.showWidget( controlsContainer.getWidgetIndex( groupPanel ) );
		
		groupListBox.setSelected( group );
		
		editor.select();
		
	}


	
	
	class GroupListBox extends ListBox < String > {
		
		@Override
		protected String dataAsString( String data ) {
			return data ;
		}

		@Override
		protected String uuid(String data) {
			return data;
		}
		
	}

	
}
