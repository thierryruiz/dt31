package com.dotemplate.theme.client.view;

import com.dotemplate.core.client.frwk.View;

public interface ThemePropertyView extends View {
	
	public void show() ;

}
