package com.dotemplate.theme.shared;

import com.dotemplate.core.shared.SymbolType;

public class ThemeSymbolType extends SymbolType {

	private static final long serialVersionUID = -5951259971340286317L;
	
	
	public final static ThemeSymbolType NAVBAR = new ThemeSymbolType( "navbar", "com.dotemplate.theme.server.symbol.NavbarProvider" ) ;

	public final static ThemeSymbolType HERO = new ThemeSymbolType( "hero" , "com.dotemplate.theme.server.symbol.HeroProvider") ; 

	public final static ThemeSymbolType BANNER = new ThemeSymbolType( "banner", "com.dotemplate.theme.server.symbol.BannerProvider" ) ;
	
	public final static ThemeSymbolType TAGLINE = new ThemeSymbolType( "tagline", "com.dotemplate.theme.server.symbol.TaglineProvider" ) ;
	
	public final static ThemeSymbolType COLOR_SCHEME = new ThemeSymbolType( "colorScheme", "com.dotemplate.theme.server.symbol.ColorSchemeProvider" ) ; 	
	
	public final static ThemeSymbolType TOP = new ThemeSymbolType( "top", "com.dotemplate.theme.server.symbol.TopProvider" ) ;
	
	public final static ThemeSymbolType BLOC = new ThemeSymbolType( "bloc", "com.dotemplate.theme.server.symbol.BlocProvider" ) ;
		
	public final static ThemeSymbolType BLOCSET = new ThemeSymbolType( "blocset", "com.dotemplate.theme.server.symbol.BlocSetProvider" ) ;
	
	public final static ThemeSymbolType SIDEBOX = new ThemeSymbolType( "sidebox", "com.dotemplate.theme.server.symbol.SideboxProvider" ) ;
	
	public final static ThemeSymbolType BG = new ThemeSymbolType( "bg", "com.dotemplate.theme.server.symbol.BgProvider" ) ;
	
	public final static ThemeSymbolType FILL = new ThemeSymbolType( "fill", "com.dotemplate.theme.server.symbol.FillProvider" ) ;
		
	public final static ThemeSymbolType BORDER = new ThemeSymbolType( "border", "com.dotemplate.theme.server.symbol.BorderProvider" ) ;
	
	public final static ThemeSymbolType BRUSH = new ThemeSymbolType( "brush", "com.dotemplate.theme.server.symbol.BrushProvider" ) ;
		
	public final static ThemeSymbolType DOC = new ThemeSymbolType( "doc", "com.dotemplate.theme.server.symbol.DocProvider" ) ;
	
	public final static ThemeSymbolType NAVIGATION = new ThemeSymbolType( "navigation", "com.dotemplate.theme.server.symbol.NavigationProvider" ) ;
	
	public final static ThemeSymbolType MENUSET = new ThemeSymbolType( "menuset", "com.dotemplate.theme.server.symbol.MenuSetProvider" ) ;
	
	public final static ThemeSymbolType TOPWRAPPER = new ThemeSymbolType( "topWrapper", "com.dotemplate.theme.server.symbol.TopWrapperProvider" ) ;
		
	public final static ThemeSymbolType WRAPPER = new ThemeSymbolType( "wrapper", "com.dotemplate.theme.server.symbol.WrapperProvider" ) ;
	
	public final static ThemeSymbolType WRAPPERBG = new ThemeSymbolType( "wrapperbg", "com.dotemplate.theme.server.symbol.WrapperBgProvider" ) ; 
	
	public final static ThemeSymbolType CANVAS = new ThemeSymbolType( "canvas", "com.dotemplate.theme.server.symbol.CanvasProvider" ) ; 
	
	public final static ThemeSymbolType CONTAINER = new ThemeSymbolType( "container", "com.dotemplate.theme.server.symbol.ContainerProvider" ) ; 
			
	public final static ThemeSymbolType HEADER = new ThemeSymbolType( "header" , "com.dotemplate.theme.server.symbol.HeaderProvider") ; 
	
	public final static ThemeSymbolType MAINBLOC = new ThemeSymbolType( "mainbloc", "com.dotemplate.theme.server.symbol.MainBlocProvider" ) ;
	
	public final static ThemeSymbolType HMENU = new ThemeSymbolType( "hmenu", "com.dotemplate.theme.server.symbol.HmenuProvider" ) ; 
	
	public final static ThemeSymbolType VMENU = new ThemeSymbolType( "vmenu", "com.dotemplate.theme.server.symbol.VMenuProvider" ) ; 
		
	public final static ThemeSymbolType SIDEBAR = new ThemeSymbolType( "sidebar", "com.dotemplate.theme.server.symbol.SidebarProvider" ) ; 	
	
	public final static ThemeSymbolType TYPO = new ThemeSymbolType( "typo", "com.dotemplate.theme.server.symbol.TypoProvider" ) ;
	
	public final static ThemeSymbolType FONT = new ThemeSymbolType( "font", "com.dotemplate.theme.server.symbol.FontProvider" ) ; 	
		
	public final static ThemeSymbolType PARAGRAPH = new ThemeSymbolType( "paragraph", "com.dotemplate.theme.server.symbol.ParagraphProvider" ) ; 		
		
	public final static ThemeSymbolType HNAV = new ThemeSymbolType( "hnav", "com.dotemplate.theme.server.symbol.HnavProvider" ) ;
	
	public final static ThemeSymbolType FOOTER = new ThemeSymbolType( "footer", "com.dotemplate.theme.server.symbol.FooterProvider" ) ;
	
	public final static ThemeSymbolType FOOTERCONTENT = new ThemeSymbolType( "footerContent", "com.dotemplate.theme.server.symbol.FooterContentProvider" ) ;
	
	public final static ThemeSymbolType BOTTOM = new ThemeSymbolType( "bottom", "com.dotemplate.theme.server.symbol.BottomProvider" ) ;
	
	public final static ThemeSymbolType FORM = new ThemeSymbolType( "form", "com.dotemplate.theme.server.symbol.FormProvider" ) ;
		
	public final static ThemeSymbolType CONTENT = new ThemeSymbolType( "content", "com.dotemplate.theme.server.symbol.ContentProvider" ) ;
	
	public final static ThemeSymbolType HMENUSTYLE = new ThemeSymbolType( "hmenuStyle", "com.dotemplate.theme.server.symbol.HmenuStyleProvider" ) ;
	
	public final static ThemeSymbolType PAGE = new ThemeSymbolType ( "page" , "com.dotemplate.theme.server.symbol.PageProvider" ) ;
		
	public final static ThemeSymbolType HEADING = new ThemeSymbolType( "heading", "com.dotemplate.theme.server.symbol.HeadingProvider" ) ;
	
	public final static ThemeSymbolType HEADINGFONT = new ThemeSymbolType( "headingFont", "com.dotemplate.theme.server.symbol.HeadingFontProvider" ) ; 	

	public final static ThemeSymbolType HEADINGJSFONT = new ThemeSymbolType( "headingJSfont", "com.dotemplate.theme.server.symbol.HeadingJSFontProvider" ) ; 	
	
	public final static ThemeSymbolType ELEMENT = new ThemeSymbolType( "element", "com.dotemplate.theme.server.symbol.ElementProvider" ) ;

	public final static ThemeSymbolType SLIDER = new ThemeSymbolType( "slider", "com.dotemplate.theme.server.symbol.SliderProvider" ) ;
	
	public final static ThemeSymbolType COMPONENT = new ThemeSymbolType( "component", "com.dotemplate.theme.server.symbol.ComponentProvider" ) ;
	

	
	public ThemeSymbolType () {
	}
	
	
	public ThemeSymbolType( String folder, String providerClassName ) {
		super ( folder, providerClassName ) ;
	}
	
	
	
	
}
