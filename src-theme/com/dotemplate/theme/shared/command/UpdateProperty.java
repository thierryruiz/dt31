package com.dotemplate.theme.shared.command;

import com.dotemplate.core.shared.properties.Property;


public class UpdateProperty extends ThemeCommand< UpdatePropertyResponse > {

	private static final long serialVersionUID = -7004502255867307196L;

	private Property property ;

	public Property getProperty() {
		return property;
	}

	public void setProperty(Property property) {
		this.property = property;
	}
	
	
	
}
