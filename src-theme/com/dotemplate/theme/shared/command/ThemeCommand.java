package com.dotemplate.theme.shared.command;

import com.dotemplate.core.shared.command.DesignCommand;
import com.dotemplate.core.shared.frwk.Response;


public abstract class ThemeCommand< RESPONSE extends Response > extends DesignCommand< RESPONSE > {

	private static final long serialVersionUID = 2926598122789479004L ;
	
	
}
