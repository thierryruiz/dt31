package com.dotemplate.theme.shared.command;


import com.dotemplate.core.shared.command.DesignCommand;



public class GetTheme extends DesignCommand< GetThemeResponse > {

	private static final long serialVersionUID = -1116533223679289522L;
	
	// just a test for planned WP dotemplate plugin 
	private String userThemeId = "";

		
	public String getUserThemeId () {
		return userThemeId;
	}

	public void setUserThemeId ( String userThemeId ) {
		this.userThemeId = userThemeId;
	}
	
	
	@Override
	public boolean isDesignUidRequired() {
		return false ;
	}
	

}
