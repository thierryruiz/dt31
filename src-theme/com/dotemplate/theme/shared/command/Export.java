package com.dotemplate.theme.shared.command;

import com.dotemplate.core.shared.command.DesignCommand;




public class Export extends DesignCommand< ExportResponse > {

	private static final long serialVersionUID = -7281335231048300896L;

	private String cmsType ;
	
	private boolean zip ;
	
	public String getCmsType () {
		return cmsType;
	}
	
	public void setCmsType ( String cmsType ) {
		this.cmsType = cmsType;
	}
	
	
	public void setZip( boolean zip ) {
		this.zip = zip;
	}
	
	
	public boolean isZip() {
		return zip;
	}
	
	
	
}
