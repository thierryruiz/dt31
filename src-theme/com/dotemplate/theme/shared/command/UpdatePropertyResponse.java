package com.dotemplate.theme.shared.command;

import com.dotemplate.core.shared.frwk.Response;
import com.dotemplate.theme.shared.ThemeUpdate;


public class UpdatePropertyResponse implements Response {

	private static final long serialVersionUID = 4758748790846183676L;
	
	private ThemeUpdate clientRefresh ;
	
	public ThemeUpdate getClientRefresh () {
		return clientRefresh;
	}
	
	public void setClientRefresh ( ThemeUpdate clientRefresh ) {
		this.clientRefresh = clientRefresh;
	}
	
}
