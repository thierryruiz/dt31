package com.dotemplate.theme.shared;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import com.dotemplate.core.shared.DesignUpdate;
import com.dotemplate.core.shared.properties.Property;
import com.dotemplate.core.shared.properties.PropertySet;


public class ThemeUpdate implements DesignUpdate< Theme >  {

	private static final long serialVersionUID = -1186860582312095645L;

	private HashMap< String, String > html = new HashMap<String, String>() ;
		
	private String js ;
	
	private ArrayList< PropertySet > updatedProperties ;

	//private ArrayList< Stylesheet > newRules ;
	
	private HashSet< String > disabledSets ;
	
	private HashSet< String > enabledSets ;
	
	private String css ;
	
	private Property property ;
	
	
	public ThemeUpdate() {
	}
	

	/**
	 * @return the js
	 */
	public String getJs () {
		return js;
	}

	/**
	 * @param js the js to set
	 */
	public void setJs ( String js ) {
		this.js = js;
	}
	
	public String getCss() {
		return css;
	}
	
	public void setCss(String css) {
		this.css = css;
	}
	
	
	public void addHtml( String selector, String content ){
		html.put( selector, content ) ;
	}
	
	
	public HashMap< String, String > getHtml(){
		return html ;
	}
	
	
	@Deprecated
	public Property getProperty() {
		return property;
	}

	@Deprecated
	public void setProperty(Property property) {
		this.property = property;
	}


	@Deprecated
	public HashSet<String> getDisabledSets() {
		return disabledSets;
	}

	@Deprecated
	public void setDisabledSets( HashSet<String> disabledSets) {
		this.disabledSets = disabledSets;
	}

	@Deprecated
	public HashSet<String> getEnabledSets() {
		return enabledSets;
	}

	@Deprecated
	public void setEnabledSets(HashSet<String> enabledSets) {
		this.enabledSets = enabledSets;
	}
	

	public void setUpdatedProperties( ArrayList< PropertySet > updatedProperties ) {
		this.updatedProperties = updatedProperties;
	}
	

	public ArrayList< PropertySet > getUpdatedProperties() {
		return updatedProperties;
	}

	
	
}
