package com.dotemplate.theme.server;

import java.io.Serializable;

import com.google.gwt.user.client.rpc.IsSerializable;



public enum ThemeColorType implements Serializable, IsSerializable  {

	 dominant( 0 ), 
	 body( 1 ), 
	 complement( 2 ),
	 bodyComplement( 3 ),
	 
	 dominantBright( 4 ),
	 dominantDark( 5 ),
	 
	 dominantDarkest,
	 dominantOpposite,
	 dominantBrightOpposite,	 
	 dominantDarkOpposite,
	 
	 bodyOppositeSoft,
	 bodyShade,
	 
	 complementOpposite,
	 complementShade,
	 
	 dominantShade,
	 dominantDarkShade,
	 dominantDarkestShade,
	 
	 
	 //neutralBright( 5 ),
	 //neutralBrighter( 6 ),
	 //neutralDark( 7 ),
	 //neutralDarker( 8 )
	 
	 ;
	
	
	/*
	 dominant, 
	 page, 
	 complement,
	 dominantBright	,
	 dominantDark, 
	 dominantOpposite,
	 dominantOppositeSoft,
	 pageOppositeSoft,
	 pageTone,
	 complementOppsite,
	 complementOppsiteSoft,
	 neutralBright,
	 neutralBrighter,
	 neutralDark,
	 neutralDarker;
	
	 dominant				( 		"dominant" 					),
	 page					( 		"page"						),
	 complement				(		"complement" 				),
	 dominantBright			(		"dominantBright"			),	
	 dominantDark			(		"dominantDark"				),		 
	 dominantOpposite		(		"dominantDark"				),
	 dominantOppositeSoft	(		"dominantDark"				),
	 pageOppositeSoft		( 		"page"						),
	 pageTone				( 		"pageTone"					),
	 complementOppsite		(		"complementOppsite" 		),
	 complementOppsiteSoft	(		"complementOppsiteSoft" 	),
	 neutralBright			(		"neutralBright" 			),
	 neutralBrighter		(		"neutralBrighter" 			),
	 neutralDark			(		"neutralDark" 				), 
	 neutralDarker			(		"neutralDarker" 			)	 
	 ;
*/	 

	
	 private int index ;
	 
	 
	 private ThemeColorType( int i ) {
		 this.index = i ;
	 }
	 
	 private ThemeColorType() {
	 }
	 
	 public int getIndex() {
		 return index ;
	 }

	 public static int lenght() {
		 return 15 ;
	 }
	
	
	
}
