package com.dotemplate.theme.server;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.velocity.VelocityContext;

import com.dotemplate.core.server.CachedImage;
import com.dotemplate.core.server.ColorUtils;
import com.dotemplate.core.server.DesignHelper;
import com.dotemplate.core.server.SetHierarchy;
import com.dotemplate.core.server.frwk.AppException;
import com.dotemplate.core.server.frwk.AppRuntimeException;
import com.dotemplate.core.server.svg.ImageUtils;
import com.dotemplate.core.server.svg.Slices;
import com.dotemplate.core.shared.Scheme;
import com.dotemplate.core.shared.properties.ColorProperty;
import com.dotemplate.core.shared.properties.Property;
import com.dotemplate.core.shared.properties.PropertySet;
import com.dotemplate.core.shared.properties.RefProperty;
import com.dotemplate.core.shared.properties.StringProperty;
import com.dotemplate.theme.server.cms.CMS;
import com.dotemplate.theme.server.generator.HTMLGeneratorDelegate;
import com.dotemplate.theme.server.navigation.MenuSet;
import com.dotemplate.theme.server.navigation.MenuSetFactory;
import com.dotemplate.theme.server.web.GetCssImage;
import com.dotemplate.theme.shared.Theme;


public class ThemeHelper extends DesignHelper< Theme > {
	
	
	private static String  googleFontJs = 
			"$('head').append('<link rel=\"stylesheet\" href=\"http://fonts.googleapis.com/css?{urlParams}\" type=\"text/css\" />' );" ;
	
	private static String  googleFontLink = 
			"<link rel=\"stylesheet\" href=\"http://fonts.googleapis.com/css?family={urlParams}\" type=\"text/css\"></link>" ;
	
	
	public Object getSymbolHtml ( PropertySet symbol ) {
		String vm = CMS.get ( ThemeApp.getThemeContext () ).getHtmlTemplate ( symbol );

		return ( vm != null ) ? vm : new Boolean ( false );
	}

	public Object getSymbolCss ( PropertySet symbol ) {
		String vm = CMS.get ( ThemeApp.getThemeContext () ).getCssTemplate ( symbol );
		return ( vm != null ) ? vm : new Boolean ( false );
	}

	public Object getSymbolJs ( PropertySet symbol ) {
		String vm = CMS.get ( ThemeApp.getThemeContext () ).getJsTemplate ( symbol );
		return ( vm != null ) ? vm : new Boolean ( false );
	}

	public Object getSymbolRandom ( PropertySet symbol ) {
		String vm = CMS.get ( ThemeApp.getThemeContext () ).getRandomTemplate ( symbol );
		return ( vm != null ) ? vm : new Boolean ( false );
	}

	
	public Object getRootTemplate ( String vm ) {
		return StringUtils.substringBeforeLast( vm, "/" )
			+ "/../" + StringUtils.substringAfterLast( vm, "/" ) ; 
	}
	
	
	public void addScript ( String url ) {
		ThemeApp.getThemeContext ().addScript ( url ) ;
	}
	
	public void addPolyfill ( String url ) {
		ThemeApp.getThemeContext ().addPolyfill ( url ) ;
	}
	
	public ColorHelper color() {
		return new ColorHelper() ;
	}
	


	/*
	public String image ( String path ) {

		// copy the image in the site directory if needed and return the image uri
		ImageUtils.copyContentImage ( path );

		// build copied image uri
		StringBuffer sb = new StringBuffer ();
		
		boolean export = ThemeApp.getThemeContext ().isExportMode ();

		if ( export ) {
			
			sb.append ( "images" ).append ( path.substring ( path.lastIndexOf ( "/" ) ) );
		
		} else {
			
			sb.append ( ThemeApp.get().getWorkUri ().replace ( '\\', '/' ) ).append ( "/images" ).append (
									path.substring ( path.lastIndexOf ( "/" ) ) ).append ( "?" )
									.append ( RandomStringUtils.randomAlphanumeric ( 4 ) );
		}

		return sb.toString ();
		
	}*/
	
	
	
	public String image ( String path ) {

		boolean export = ThemeApp.getThemeContext ().isExportMode ();
		
		StringBuffer sb = new StringBuffer ();
		
		if ( export ){
		
			// copy the image in the site directory if needed and return the image uri
			ImageUtils.exportContentImage ( path );
			
			return sb.append ( "images" ).append ( path.substring ( path.lastIndexOf ( "/" ) ) ).toString();
			
			
			
		} else {
			
			return path ;
			
		}
		
		
	}
		
	
	
	
	
	
	@Deprecated
	public String image ( String path, int rx ) {
	
		return ImageUtils.clipImage ( path, rx );
	
	}


	
	

	public void cacheCssImage ( String src, String dest ) {
	    
		ThemeContext ctx = ThemeApp.getThemeContext () ;
		
		if( ctx.isExportMode () ) return ;
        
		String imgId = StringUtils.substringBeforeLast ( dest, "." ) ;
		String format = StringUtils.substringAfterLast ( dest, "." ) ; 
	
		
		ctx.getCssImageCache().setImage( new CssImage( imgId ,
				ImageUtils.read(  new File ( src ) ), 
					format.equalsIgnoreCase( "png" ) ? 
						CachedImage.Format.PNG : 
							CachedImage.Format.JPG ) ) ;		
		
		
	}

	
	
	public String cssImg( String img ) {
		
		String extension = StringUtils.substringAfterLast( img , "." ) ;
		
		img = CssImage.clean( StringUtils.substringBeforeLast( img, "." ) )  ;
		
		CachedImage cachedImage = ThemeApp.getThemeContext ().getCssImageCache ().getImage( img ) ;
		
		StringBuffer sb = new StringBuffer() ;
		
		ThemeContext ctx = ThemeApp.getThemeContext () ;
		
		sb.append ( "url('" ) ;
					
		if ( ctx.isExportMode() ){
			
			sb.append ( ThemeApp.getThemeContext ().get (  ThemeContext.CSS_IMAGE_DIR ) )
				.append (  "/" ).append( img ).append( "." ).append( extension ).append("')" ) ;			 
			 
		} else {
						
			//CachedImage cachedImage = ThemeApp.getThemeContext ().getCssImageCache ().getImage( img ) ;
			
			if ( cachedImage != null && !cachedImage.isChanged() ){
				return "inherited" ;
			}
			
			String webContext = ( ( ThemeAppConfig ) ThemeApp.getConfig() ).getWebContext() ;
	
			 sb.append( webContext  ).append(  GetCssImage.URL_PREFIX ).append( img ).append( "." ).append( extension )
				.append( "&" ) // add a random to make css value different compared to previous
					.append( RandomStringUtils.randomAlphanumeric ( 4 ) )
						.append("')") ;
		}
		
		return sb.toString() ;
		
	}
	
	
	/*
	public String cssImg( String img ) {
		
		String extension = StringUtils.substringAfterLast( img , "." ) ;
		
		img = CssImage.clean( StringUtils.substringBeforeLast( img, "." ) )  ;
		
		StringBuffer sb = new StringBuffer() ;
		
		ThemeContext ctx = ThemeApp.getThemeContext () ;
		
		sb.append ( "url('" ) ;
					
		if ( ctx.isExportMode() ){
			
			 sb.append ( ThemeApp.getThemeContext ().get (  ThemeContext.CSS_IMAGE_DIR ) )
				.append (  "/" ).append( img ).append( "." ).append( extension )
					.append("')" ) ;
			
		} else {
						
			CachedImage cachedImage = ThemeApp.getThemeContext ().getCssImageCache ().getImage( img ) ;
			
			if ( cachedImage != null && !cachedImage.isChanged() ){
				return "inherited" ;
			}
			
			String webContext = ( ( ThemeAppConfig ) ThemeApp.getConfig() ).getWebContext() ;
	
			 sb.append( webContext  ).append(  GetCssImage.URL_PREFIX ).append( img ).append( "." ).append( extension )
				.append( "&" ) // add a random to make css value different compared to previous
					.append( RandomStringUtils.randomAlphanumeric ( 4 ) )
						.append("')") ;
		}
		
		return sb.toString() ;
		
	}*/
		
	
	public void addSlice( Slices slices, String name, int left, int top, int width, int height, double quality  ) {
		super.addSlice( slices, CssImage.clean( name ) , left, top, width, height, quality ) ;
	}
	
	
	public void addSlicePng( Slices slices, String name, int left, int top, int width, int height, double quality  ) {
		super.addSlicePng( slices, CssImage.clean( name ) , left, top, width, height, quality ) ;
	}
	
	

	public void copyCssImage ( String path, String destName ) {
     
		boolean download = ( Boolean ) ThemeApp.getThemeContext ().isExportMode ();

		StringBuffer sb = new StringBuffer ();

		// already copied
		if ( download ) return ;
		
        String dest = sb.append( ThemeApp.get().getWorkRealPath()).append( "/images/css/" )
        	.append( destName ).toString() ; 
    
		// File output = new File (  Ease.getSiteRealPath () + "/images" ) ;
        
		File destFile = new File ( dest.toString() ) ;
		
		File srcFile = new File ( path ) ;
		
		try {
			
			FileUtils.copyFile( srcFile, destFile ) ;
		
		} catch ( IOException e ) {
			
			throw new AppRuntimeException ( "Unable to create images directory ", e ) ;
		
		}
	
	}
	
	
	/**
	 * 
	 * Computes URI of images used in box paragraphs.
	 * 
	 * @param path
	 *            - the image path as defined in theme descriptor
	 * @return the computed image uri.
	 * 
	 */
	public String imageUri ( String path ) {

		boolean download = ( Boolean ) ThemeApp.getThemeContext ().isExportMode ();

		path = StringUtils.replace ( path, ".png", ".jpg" );

		StringBuffer sb = new StringBuffer ();

		if ( download ) {
			
			sb.append ( "images" ).append ( path.substring ( path.lastIndexOf ( "/" ) ) );
		
		} else {
			sb.append ( ThemeApp.get().getWorkUri ().replace ( '\\', '/' ) ).append ( "/images" ).append (
									path.substring ( path.lastIndexOf ( "/" ) ) ).append ( "?" )
									.append ( RandomStringUtils.randomAlphanumeric ( 4 ) );
		}

		return sb.toString ();

	}

	
	
	public CreativeCommonsImage slideshowImage ( String key ) {
		return ThemeApp.getThemeManager ().getSlideshowImage ( key );
	}

	
	
	public String rjs ( String js ) {
		
		ThemeRefresh themeRefresh =  ThemeApp.getThemeContext ().getThemeRefresh() ;
		
		String hashcode = "_" + js.hashCode() ;
		
		if ( !themeRefresh.containsJs( hashcode ) ){
			
			themeRefresh.addJs ( hashcode, js ) ;
			return js.trim();
		
		} else {
			
			return "" ;
		
		}
	
	}
	
	
	
	
	
	public String rjsfile ( String file ) {
		
		ThemeRefresh themeRefresh =  ThemeApp.getThemeContext ().getThemeRefresh() ;
		
		try {
			
			if ( !themeRefresh.containsJs( file ) ){
				
				String js = FileUtils.readFileToString( 
						new File ( ThemeApp.realPath( "WEB-INF/templates/" ) + file ) ) ;
				
				themeRefresh.addJs ( file, js ) ;
				
				return js.trim();
			
			} else {
				
				return "" ;
			
			}
						
		} catch ( IOException e ) {
			
			throw new AppRuntimeException ( e ) ;
		
		}
		
	}
	
		
	public String rhtml ( String id, PropertySet set ) {

		if ( set == null ) return "" ;
		
		createHierarchy ( set );


		StringWriter writer = new StringWriter ();

		try {
			
			HTMLGeneratorDelegate.generate ( set, writer ) ;
		
		} catch ( AppException e ) {
			
			throw new AppRuntimeException ( "Failed to generate.", e );
		}

		String html = writer.getBuffer ().toString ();

		ThemeApp.getThemeContext ().getThemeRefresh ().addHtml ( id, html );

		return html;

	}

	
	public String rhtml ( PropertySet set, String id, String template ) {

		createHierarchy ( set );


		StringWriter writer = new StringWriter ();

		VelocityContext context = new VelocityContext ( ThemeApp.getThemeContext() );

		context.put ( "s", set );
		
		try {

			ThemeApp.getRenderEngine ().generate ( context, template, writer );

		} catch ( Exception e ) {

			throw new AppRuntimeException ( "Failed to generate html for set '" + set.getName () + "'", e );
		}
		

		String html = writer.getBuffer ().toString ();

		ThemeApp.getThemeContext ().getThemeRefresh ().addHtml ( id, html );
		

		return html;

	}
	
	
	public String rhtml ( String selector, String html ) {

		if ( !ThemeApp.getThemeContext ().isExportMode () ) {
			ThemeApp.getThemeContext ().getThemeRefresh ().addHtml ( selector, html );
		}

		return html;
	}
	
	
	public Object superHtml ( PropertySet set ) throws AppException {
		PropertySet symbol = SetHierarchy.next ( set );
		return getSymbolHtml ( symbol );
	}

	public Object superCss ( PropertySet set ) throws AppException {
		PropertySet symbol = SetHierarchy.next ( set );
		return getSymbolCss ( symbol );
	}

	public Object superJs ( PropertySet set ) throws AppException {
		PropertySet symbol = SetHierarchy.next ( set );
		return getSymbolJs ( symbol );
	}

	
	
	public Collection<PropertySet> getBlocs ( PropertySet set )  {

		ArrayList<PropertySet> blocs = new ArrayList<PropertySet> ();

		ThemeContext ctx = ThemeApp.getThemeContext ();

		PropertySet bloc;

		if ( set != null ) {

			for ( Property p : set.getProperties () ) {

				if ( p.isSet () ) {

					blocs.add ( ( PropertySet ) p );

				} else if ( p.getType () == Property.REF ) {

					bloc = ( PropertySet ) ctx.get ( ( ( RefProperty ) p ).getValue () );
					
					if ( bloc != null ){
						blocs.add ( bloc );
					}
				}
			}
		}

		return blocs ;

	}
	

	/*
	PropertySet getBloc( PropertySet set, String name ) {
		
		ThemeContext ctx = ThemeApp.getThemeContext ();
		
		if ( set != null ) {
			
			Property p = set.getProperty( name ) ;
			
			if ( p.isSet() ){
				
				return ( PropertySet ) p ;
			
			} else if( p.getType () == Property.REF ){
				
			} else {
				throw new AppRuntimeException( "No set '" + name + "' in set " 
						+ set.getLongName() + "' blocs ") ; 

			}	
			
			
			for ( Property p : set.getProperties () ) {

				if ( p.isSet () ) {

					blocs.add ( ( PropertySet ) p );

				} else if ( p.getType () == Property.REF ) {

					bloc = ( PropertySet ) ctx.get ( ( ( RefProperty ) p ).getValue () );
					
					if ( bloc != null ){
						blocs.add ( bloc );
					}
				}
			}
		}
	}*/
	

	
	// compute the with of 3 column box
	public int oneThird ( int containerWidth, int margin ) {
		return ( int ) Math.rint ( ( ( double ) ( containerWidth - 2 * margin ) / ( double ) 3 ) );
	}

	
	/*
	public void enable( PropertySet set ){
		
		Set< String > enabled = ThemeApp.getThemeContext().getThemeRefresh().getEnabledPropertySet() ;
		
		for ( String uid : DesignUtils.enable ( set ) ){
			enabled.add( uid ) ;
		}
		
		Collection< PropertySet > blocs = getBlocs( set ) ;
		
		for( PropertySet bloc : blocs ){
			enable( bloc ) ;
		}
		
		
		
	}
	

	public void disable( PropertySet set ){
		
		Set< String > disabled = ThemeApp.getThemeContext().getThemeRefresh().getDisabledPropertySet() ;
		
		for ( String uid : DesignUtils.disable ( set ) ){
			disabled.add( uid ) ;
		}
		
		Collection< PropertySet > blocs = getBlocs( set ) ;
		
		for( PropertySet bloc : blocs ){
			disable( bloc ) ;
		}
		
	}
	*/
	
	
	public void setSelector ( Property p ) {
		
		if ( p == null || !p.isSet() ) return ;
		
		PropertySet set = ( PropertySet ) p ;
		
		StringProperty selector = ( StringProperty ) set.getProperty( "selector" ) ;
		
		if ( selector == null ) { 
			return ;
		}
		
		try {
			if ( selector.getValue() != null && selector.getValue().length() > 0 ) {
				return ;
			}
		} catch (NullPointerException e) {
			throw new NullPointerException(set.getLongName() + " selector getValue is null" ) ;

		}
		
		StringBuffer sb = new StringBuffer () ;
		 
		if ( set.getStyleId() != null ){
			sb.append(  '#') .append( set.getStyleId() ) ;
		} else {
			return ;
		}
		
		/*
		String styleClass = set.getStyleClass() ;
		
		if ( styleClass != null ){
			for ( String s : StringUtils.split( styleClass )  )
			sb.append('.').append( s );
		}
		*/
		
		selector.setValue( sb.toString() ) ;		
		selector.setComputed(true) ;
	
	}
	
	
	public void setSelector ( Property p , String value ){
		
		if ( p == null || !p.isSet() ) return ;
		
		PropertySet set = ( PropertySet ) p ;
		
		StringProperty selector = ( StringProperty ) set.getProperty( "selector" ) ;
		if ( selector == null ) {
			return ;
		} else {
			selector.setValue( value ) ;
		}
		
		selector.setComputed(true) ;
	}
	
	
	public MenuSet createMenuSet( PropertySet set ){
		return MenuSetFactory.create( set ) ;
	}

	
	@Override
	public String oppositeColor( String c ){
		
		if ( c.startsWith( "#" ) ){
			
			return super.oppositeColor( c ) ;
			
		} else {
			
			Scheme scheme = ThemeApp.getThemeManager().getScheme() ;
			
			if ( scheme == null ){
				throw new AppRuntimeException ( "Unknown scheme '" + ThemeApp.getTheme().getScheme() + "'" ) ;
			}
			
			return ColorUtils.getOpposite( scheme.getColor( c ) ) ;
		
		}
	
	}
	
	
	
	
	public void applySchemeColor( ColorProperty property, String schemeColor ){
		
		Scheme scheme = ThemeApp.getThemeManager().getScheme() ;
		
		if ( scheme == null ){
			throw new AppRuntimeException ( "Unknown scheme '" + ThemeApp.getTheme().getScheme() + "'" ) ;
		}
		
		if ( property == null ){
			return ;
		}
		
		
		String color = ( schemeColor.startsWith( "#" ) )? schemeColor :
				scheme.getColor( schemeColor ) ;
		
		
		if ( color == null ){
			throw new AppRuntimeException ( "Scheme color type '" + schemeColor + "' is undefined." );
		}
		
		property.setValue( color ) ;
	
	}
	
	
	
	public void applySchemeColor( PropertySet set, String schemeColor ){
		
		Scheme scheme = ThemeApp.getThemeManager().getScheme() ;
		
		if ( scheme == null ){
			throw new AppRuntimeException ( "Unknown scheme '" + ThemeApp.getTheme().getScheme() + "'" ) ;
		}
		
		if( set == null ){
			return ;
		}
		
		Property property = set.getProperty( "color" ) ;
		
		
		if ( property == null ){
			throw new NullPointerException ( "No 'color' property defined in set " + set.getLongName() ) ;
		}
		
		
		String color = ( schemeColor.startsWith( "#" ) )? schemeColor :
				scheme.getColor( schemeColor ) ;
		
		
		if ( color == null ){
			throw new AppRuntimeException ( "Scheme color type '" + schemeColor + "' is undefined." );
		}
		
		( ( ColorProperty ) property ).setValue( color ) ;
		
		//property.setComputed( true );
	
	}
	
	
	
	public int max( int i1, int i2 ){
		return Math.max(i1, i2) ;
	}
	
	
	public void addGoogleFont( String selector,  PropertySet set ){
		
		ThemeContext ctx = ThemeApp.getThemeContext() ;
		
		if ( !ctx.isExportMode() ) {
			return ;
		}
		
		GoogleFont font = new GoogleFont( ) ;
		font.setUrlParams( ( String )  set.get( "urlParams" ) ) ;
		font.setTitle( ( String ) set.get( "title" ) ) ;
		
		ctx.getGoogleFonts().put( selector, font ) ;
	
	}
	
	
	public Collection< GoogleFont > googleFonts(){
		return ThemeApp.getThemeContext().getGoogleFonts().values() ;
	}
	
	
	public String getGoogleFontJs( String urlParams  ){
		return StringUtils.replace( googleFontJs, "{urlParams}", urlParams )  ;
	}

	
	public String getGoogleFontLink(){
		
		ThemeContext ctx = ThemeApp.getThemeContext() ;
		
		if ( !ctx.isExportMode() ){
			return "" ;
		}
		
		
		Map< String, GoogleFont > gfonts =  ctx.getGoogleFonts() ;
		
		if ( gfonts == null || gfonts.size() == 0 ){
			return "" ;
		}
		
		StringBuffer sb = new StringBuffer() ;
		
		for ( GoogleFont font : gfonts.values() ){
			sb.append( StringUtils.removeStart( font.getUrlParams(), "family=" ) ) ;
			sb.append( "|" ) ;
		}
		
		String params =  sb.toString() ;
		params = StringUtils.removeEnd( params, "|" ) ;
		
		return StringUtils.replace( googleFontLink, "{urlParams}", params ) +"\n" ;
		
	}

	
	public boolean available() {
		return ThemeUtils.hasFullFeatures();
	}
	
	
	public Collection<PropertySet> getNavigationNodes() {
		
		ArrayList< PropertySet > nodes = new ArrayList< PropertySet >() ;
		
		for ( Property p :  ( (  PropertySet ) ThemeApp.getThemeContext().get( "navigation" ) ).getProperties() ){
			
			if ( p.isSet() ){
				
				nodes.add( ( PropertySet ) p ) ;
			
			}
		}
		
		return nodes ;
		
	}
	
	
	
	public void addStyleClass( PropertySet set, String klass ){
		
		String currentClass = set.getStyleClass() ;
		
		if ( currentClass == null ){
			currentClass = klass ;
		} else {
			currentClass = ( currentClass + " " + klass ) ;
		}
		
		set.setStyleClass( currentClass  ) ;
		
	}
	
	public void removeStyleClass( PropertySet set, String klass ){
		
		String currentClass = set.getStyleClass() ;
		
		currentClass = StringUtils.remove( currentClass ,klass ) ;
		
		
		if ( currentClass != null ){
			
			currentClass = currentClass.trim() ;

			if ( currentClass.length() == 0 ){
				currentClass = null ;
			}
		} 
		
		set.setStyleClass( currentClass  ) ;
		
	}
	
	
	
	
}
