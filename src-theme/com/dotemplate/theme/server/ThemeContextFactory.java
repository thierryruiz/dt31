/**
 * Copyright Thierry Ruiz - 2004-2008. All rights reserved.
 * Created on 4 mars 08 - 12:04:39
 *
 */
package com.dotemplate.theme.server;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.DesignContextFactory;
import com.dotemplate.theme.shared.Theme;



/**
 * 
 * 
 * @author Thierry Ruiz
 *
 */
public class ThemeContextFactory extends DesignContextFactory< Theme > {
		
	private final static Log log = LogFactory.getLog ( ThemeContextFactory.class );
		
	private Calendar calendar = new GregorianCalendar() ;

	private ThemeContextFactory() {}
	
	public ThemeContext create( Theme theme ) {
		return create( theme, false ) ;
	}
	
	
	public ThemeContext create( Theme theme, boolean randomize ) {
	
		if ( log.isDebugEnabled () ){
			log.debug (  "Creating new ThemeContext..." ) ;
		}
	
		ThemeAppConfig config = ThemeApp.getThemeAppConfig ();
		
		ThemeContext ctx = ThemeApp.isDevMode () ? new DevModeThemeContext(): new BaseThemeContext() ;
				
		ThemeHelper helper = new ThemeHelper() ;
		
    	ctx.put ( ThemeContext.THEME, theme ) ;
    	
    	ctx.put ( ThemeContext.WATERMARK, theme.isPremium() && !theme.isPurchased() ) ;

		ctx.put ( ThemeContext.HELPER, helper ) ;
		ctx.put ( "_", helper ) ; // alternate key for shorter syntax in vm files
		
		ctx.put ( ThemeContext.DOSLICE, true ) ;
		ctx.put ( ThemeContext.JS, new JavascriptContext() );
		ctx.put ( ThemeContext.CSS_IMAGE_DIR, "/sites/"+ theme.getUid () + "/images/css" ) ;
		
		ctx.put ( ThemeContext.EXPORT, false ) ;
		ctx.put ( ThemeContext.WORK_DIR, "sites/" + theme.getUid () + "/" ) ;
		ctx.put ( ThemeContext.VOID_LINK, "onclick='javascript:return false;'" ) ;
		
		ctx.put (  ThemeContext.YEAR , calendar.get ( Calendar.YEAR ) ) ;
		ctx.put (  ThemeContext.BASE_URL , config.getBaseUrl() ) ;
		
		//ctx.put (  ThemeContext.CONTENT , ThemeUtils.getCategoryContent( theme ) ) ;
		
		ctx.put (  ThemeContext.BACKLINK , Backlinks.generate() ) ;
		
		if ( ThemeApp.isDevMode () ){
			ctx.put ( ThemeContext.DEV_MODE, true ) ;
		}
		
		ctx.updateRandom () ;
				
		if ( config.isTestMode() ){
			ctx.put( ThemeContext.TEST_MODE, true ) ;
		}
		
		if ( randomize ){
			ctx.put(ThemeContext.RANDOMIZE, true) ;
		}
		
		ctx.preprocess() ;
		
		return ctx ;
	
	}

}
