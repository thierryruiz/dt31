/**
 * Copyright Thierry Ruiz ( Athesanet ) 2004 - 2008
 * All rights reserved.
 *
 * Created on 22 mai 08 : 17:04:23
 */
package com.dotemplate.theme.server;

import com.dotemplate.core.server.frwk.AppRuntimeException;



/**
 * 
 * 
 * @author Thierry Ruiz
 *
 */
public class ThemeValidationError extends AppRuntimeException {

	private static final long serialVersionUID = -704552307692192430L;
	
	public ThemeValidationError ( String msg ) {
		super( msg ) ;
	}

}
