package com.dotemplate.theme.server.generator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Future;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.velocity.context.Context;

import com.dotemplate.core.server.DesignUtils;
import com.dotemplate.core.server.SetHierarchy;
import com.dotemplate.core.server.frwk.AppRuntimeException;
import com.dotemplate.core.server.util.ConcurrentTaskContext;
import com.dotemplate.core.shared.properties.PropertySet;
import com.dotemplate.theme.server.ThemeApp;


public class PropertySetGenerator {

	private static Log log = LogFactory.getLog( PropertySetGenerator.class ) ;
	
	protected GenerateTaskExecutor executor = new GenerateTaskExecutor() ;
	
	public void regenerate( Collection< PropertySet > sets ){
	
		if ( log.isInfoEnabled () ){
			log.info ( "Regenerating sets..."  ) ;
		}
		
		GenerateTask.SVGSetContext 	SVGContext ;
		GenerateTask.CSSSetContext 	CSSContext ;
		GenerateTask.HTMLSetContext HTMLContext ;		
		GenerateTask.JSSetContext 	JSContext ;  
		
		
		List< ConcurrentTaskContext > svgTaskContexts = new ArrayList< ConcurrentTaskContext > () ;
		
		List< ConcurrentTaskContext > documentTaskContexts = new ArrayList< ConcurrentTaskContext > () ;
		
	
		for ( PropertySet set : sets ) {
			
			if ( log.isInfoEnabled () ){
				log.info ( "Regenerating set " + set.getLongName()  ) ;
			}
			
			
			SVGContext 	= new GenerateTask.SVGSetContext() ;
			SVGContext.setSet ( set ) ;
			svgTaskContexts.add( SVGContext ) ;
				
			CSSContext 	= new GenerateTask.CSSSetContext() ;
			CSSContext.setSet ( set ) ;
			documentTaskContexts.add( CSSContext ) ;
			
			JSContext 	= new GenerateTask.JSSetContext() ;
			JSContext.setSet ( set ) ;
			documentTaskContexts.add( JSContext ) ;
			
			HTMLContext 	= new GenerateTask.HTMLSetContext() ;
			HTMLContext.setSet ( set ) ;
			documentTaskContexts.add( HTMLContext ) ;
			
			applySettings( set ) ;
						
		}

				
		try {
		
			List< Future< Void > > results = executor.execute ( svgTaskContexts ) ;
			
			for ( Future<Void> future : results ){
				future.get () ; // will rethrow exception raised by a task
			}
		
		} catch ( Exception e ) {
			
			throw new AppRuntimeException ( "Unable to generate slices for update", e ) ;
		
		}
		
		
		try {
			
			List< Future< Void > > results = executor.execute ( documentTaskContexts ) ;
			
			for ( Future<Void> future : results ){
				future.get () ; // will rethrow exception raised by a task
			}
		
		} catch ( Exception e ) {
			
			throw new AppRuntimeException ( "Unable to generate slices for property update", e ) ;
		
		}
	}
		

	public void applySettings ( PropertySet set ) throws AppRuntimeException {
		
		PropertySet symbol = DesignUtils.getSymbol ( set ) ;
		
		if ( symbol == null ) return ; 
		
		
        String template  = DesignUtils.getSymbol (
        		set.getParent() ).getPath () + "/settings.vm" ;
        
        if ( ! ThemeApp.getRenderEngine ().templateExists( template ) ) return ;
        
			
		// System.out.println ( "\n\nsettings	>  " + set.getLongName() ) ;
		
        
        Context themeContext = ThemeApp.getThemeContext() ;
        
        themeContext.put ( "s", set ) ;
		
        // generate css
		try {
			
			SetHierarchy.create( set ) ;
			
			ThemeApp.getRenderEngine ().generate (  themeContext, template, new VoidWriter() ) ;
		
		} catch ( Exception e ) {
			
			throw new AppRuntimeException ( "Failed to apply settings for set " + set.getLongName () , e ) ;
		
		} finally {
			
			SetHierarchy.clear() ;
		
		}
		
		
	}
	

	
	
}
