package com.dotemplate.theme.server.generator;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.velocity.context.Context;

import com.dotemplate.core.server.SetHierarchy;
import com.dotemplate.core.server.frwk.AppRuntimeException;
import com.dotemplate.core.server.util.ConcurrentTaskContext;
import com.dotemplate.core.server.util.Path;
import com.dotemplate.core.shared.properties.Property;
import com.dotemplate.core.shared.properties.PropertySet;
import com.dotemplate.theme.server.ThemeApp;
import com.dotemplate.theme.server.ThemeContext;
import com.dotemplate.theme.server.css.CSSHelper;
import com.dotemplate.theme.server.css.SimpleCSSParser;
import com.dotemplate.theme.server.css.Stylesheet;

import com.dotemplate.theme.shared.Theme;


public class ThemeGenerator extends PropertySetGenerator {

	private static Log log = LogFactory.getLog( ThemeGenerator.class ) ;
	
	
	@Override
	public void regenerate( Collection<PropertySet> sets ) {
		
		applyModelSettings() ;
		
		super.regenerate(sets);
	}
	
	
	
	public void generate(){
		
		if ( log.isInfoEnabled () ) {
			log.info ( "Generating theme..." ) ;
		}
				
		Map< String, PropertySet > sets = new LinkedHashMap < String, PropertySet >() ;
		
		ThemeContext themeContext = ThemeApp.getThemeContext() ;
		
		Theme theme = ThemeApp.getTheme()  ;
		
		applyModelSettings() ;
		
		/*
		if ( !theme.isPurchased() ){
			
			applyColorScheme() ;	
		
		}*/
		
		
		for ( PropertySet set : theme.getPropertySets() ){
			
			addSubSets( set, sets ) ;
		
		}
		
		GenerateTask.SVGSetContext SVGContext ;

		List< ConcurrentTaskContext > taskContexts = new ArrayList< ConcurrentTaskContext > () ;

		themeContext.clearRowScripts() ;
			
		for ( PropertySet s : sets.values () ){

			// slice all required svg
			SVGContext 	= new GenerateTask.SVGSetContext() ;
			SVGContext.setSet ( s ) ;
			taskContexts.add( SVGContext ) ;
		}
		

		// generate css
		taskContexts.add( new GenerateTask.CSSThemeContext() ) ;	

		
		// generate js
		taskContexts.add( new GenerateTask.JSThemeContext() ) ;	
		
	
		try {
			
			List< Future< Void > > results = executor.execute ( taskContexts ) ;
			
			for ( Future< Void > future : results ){
				
				future.get () ; // will rethrow exception raised by a task
			
			}
		
		} catch ( Exception e ) {
			throw new AppRuntimeException ( "Unable to generate theme, site " , e  ) ;
		}
		
		
		
		// Generate HTML export seperately to avoid concurrent modification of image cache
		
		taskContexts.clear() ;
		
		
		// generate HTML
		taskContexts.add( new  GenerateTask.HTMLThemeContext() ) ;	
		
		
		try {
			
			List< Future< Void > > results = executor.execute ( taskContexts ) ;
			
			for ( Future< Void > future : results ){
				future.get () ; // will rethrow exception raised by a task
			}
		
		} catch ( Exception e ) {
			
			throw new AppRuntimeException ( "Unable to generate theme, site " , e  ) ;
		
		}
		
	}

	
	public void applyModelSettings () throws AppRuntimeException {
		
		Theme theme = ThemeApp.getTheme()  ;
		
        String template  = new Path( "models" ).append( theme.getParent() )
        		.append( "settings.vm" ).asString() ;
        
        if ( ! ThemeApp.getRenderEngine ().templateExists( template ) ) return ;
                
        Context themeContext = ThemeApp.getThemeContext() ;
        		
        // generate settings.vm
		try {
						
			ThemeApp.getRenderEngine ().generate (  themeContext, template, new VoidWriter() ) ;
		
		} catch ( Exception e ) {
			
			throw new AppRuntimeException ( "Failed to apply settings for set " + theme.getParent() , e ) ;
		
		}
	}

	
	
	public void applyColorScheme() throws AppRuntimeException {
		
		Theme theme = ThemeApp.getTheme()  ;
		
		if ( theme.getScheme() == null ) {
			return ;
		}
		
        String template  = new Path( "models" ).append( theme.getParent() )
        		.append( "colorize.vm" ).asString() ;
        
        if ( ! ThemeApp.getRenderEngine ().templateExists( template ) ) {
        	return ;
        }
        
        Context themeContext = ThemeApp.getThemeContext() ;
        		
		try {
						
			ThemeApp.getRenderEngine ().generate (  themeContext, template, new VoidWriter() ) ;
		
		} catch ( Exception e ) {
			
			throw new AppRuntimeException ( "Failed to apply settings for set " + theme.getParent() , e ) ;
		
		}
	}
	
	
	
	public void applyColorScheme( ThemeContext themeContext ) throws AppRuntimeException {
		
		Theme theme = themeContext.getDesign()  ;
		
		if ( theme.getScheme() == null ) {
			return ;
		}
		
        String template  = new Path( "models" ).append( theme.getParent() )
        		.append( "colorize.vm" ).asString() ;
        
        if ( ! ThemeApp.getRenderEngine ().templateExists( template ) ) {
        	return ;
        }
        		
		try {
						
			ThemeApp.getRenderEngine ().generate (  themeContext, template, new VoidWriter() ) ;
		
		} catch ( Exception e ) {
			
			throw new AppRuntimeException ( "Failed to apply settings for set " + theme.getParent() , e ) ;
		
		}
		
	}
	
	
		
	
	
	
	public String generateOnColorSchemeChange() throws AppRuntimeException {
		
		// regenerate graphics for all theme property sets and return a new regenerated  
		// stylesheet containing only color sensitive CSS properties  
		
		Theme theme = ThemeApp.getTheme()  ;
		ThemeContext themeContext = ThemeApp.getThemeContext() ;
		
		applyColorScheme() ;
		
		// needed ?
		applyModelSettings() ;
		

		
		// regenerate slices (in memory)
		themeContext.updateRandom () ;
		themeContext.getCssImageCache().trackChanges () ;
		
		Map< String, PropertySet > sets = new LinkedHashMap < String, PropertySet >() ;

		for ( PropertySet set : theme.getPropertySets() ){
			
			addSubSets( set, sets ) ;
		
		}
		
		GenerateTask.SVGSetContext SVGContext ;

		List< ConcurrentTaskContext > taskContexts = new ArrayList< ConcurrentTaskContext > () ;

			
		for ( PropertySet s : sets.values () ){

			// slice all required svg
			SVGContext 	= new GenerateTask.SVGSetContext() ;
			SVGContext.setSet ( s ) ;
			taskContexts.add( SVGContext ) ;
		}
		
		try {
			
			List< Future< Void > > results = executor.execute ( taskContexts ) ;
			
			for ( Future< Void > future : results ){
				
				future.get () ; // will rethrow exception raised by a task
			
			}
		
		} catch ( Exception e ) {
			throw new AppRuntimeException ( "Unable to generate theme, site " , e  ) ;
		}
		
		
		/// regenerate complete stylesheet AFTER slice created in memory
		StringWriter writer = new StringWriter() ;
		
		
		try {
			
			CSSGeneratorDelegate.generate( writer ) ;
			
		} catch ( Exception e ) {
			
			throw new AppRuntimeException ( "Unable to generate CSS from updated color scheme." , e  ) ;
		
		}
				
		try {
			
			// filter generated stylesheet and get only color related rules
			Stylesheet stylesheet =  new SimpleCSSParser().parseStyleSheet( 
					writer.getBuffer().toString() ) ;

			return CSSHelper.getColorStylesheetAsString( stylesheet );
		
		} catch ( Exception e ) {
			
			throw new AppRuntimeException ( "Unable to parse new CSS from updated color scheme." , e  ) ;
			
		}
		
	}
	
	
	public void generateStylesheet () {

		if ( log.isDebugEnabled () ){
			log.debug ( "Generating theme stylesheet..." ) ;
		}
		
		List< ConcurrentTaskContext > taskContexts = new ArrayList< ConcurrentTaskContext > () ;
				
		// generate stylesheet
		taskContexts.add( new GenerateTask.CSSThemeContext() ) ;
		
		
		try {
			
			List<Future< Void > > results = executor.execute ( taskContexts ) ;
			
			for ( Future<Void> future : results ){
				future.get () ; // will rethrow exception raised by a task
			}
			
		} catch ( Exception e ) {
			
			throw new AppRuntimeException ( "Unable to generate theme, site ", e ) ;
		
		}
		
	}
	

	
	public void generateJavascript () {
		
		if ( log.isDebugEnabled () ){
			log.debug ( "Generating theme javascript..." ) ;
		}
		
		// go throught all sets to build JS data
		
		ThemeContext themeContext = ThemeApp.getThemeContext() ;
		
		themeContext.clearRowScripts () ;
		
		List< ConcurrentTaskContext > taskContexts = new ArrayList< ConcurrentTaskContext > () ;
		
		taskContexts.add( new GenerateTask.JSThemeContext() ) ;
				
		try {
			List< Future< Void > > results = executor.execute ( taskContexts ) ;
			
			for ( Future<Void> future : results ){
				future.get () ; // will rethrow exception raised by a task
			}
		} catch ( Exception e ) {
			
			throw new AppRuntimeException ( "Unable to generate theme, site " , e  ) ;
		
		}
	
	}
	
	
	
	public String generateOnReadyScript () {
		
		if ( log.isDebugEnabled () ){
			
			log.debug ( "Get theme dotemplate javascript..." ) ;
		
		}
		
		ThemeContext themeContext = ThemeApp.getThemeContext() ;
		
		PropertySet docSet  = ( PropertySet) themeContext.get( "doc" ) ;

		themeContext.clearRowScripts () ;
		
		SetHierarchy.create( docSet ) ;
		
		try {
			
			JSGeneratorDelegate.generate( docSet ) ;
		
		} catch ( Exception e ){
			
			throw new AppRuntimeException ( "Unable to generate custom Javascript" , e  ) ;
			
		}
		
		finally {
			
			SetHierarchy.clear() ;
		
		}
		
		return themeContext.getThemeRefresh().getDesignUpdate().getJs() ;
		
		//return ThemeUtils.getOnReadyScripts( themeContext ) ;
		
		
	}	
	
	
	
	protected void addSubSets ( PropertySet set,  Map< String, PropertySet > map ){
		
		// add set itself
		map.put ( set.getUid(), set ) ;
		
		for ( Property property : set.getPropertyMap ().values() ){
			if ( property.isSet() ){
				addSubSets( ( PropertySet ) property , map ) ;
			}
		}
		
	}

	
	
	
}
