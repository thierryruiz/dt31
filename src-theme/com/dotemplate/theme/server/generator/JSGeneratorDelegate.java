package com.dotemplate.theme.server.generator;


import java.io.File;
import java.io.IOException;
import java.io.StringWriter;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.velocity.VelocityContext;

import com.dotemplate.core.server.DesignUtils;
import com.dotemplate.core.server.frwk.AppException;
import com.dotemplate.core.server.frwk.AppRuntimeException;
import com.dotemplate.core.server.util.Path;
import com.dotemplate.core.shared.properties.PropertySet;
import com.dotemplate.theme.server.ThemeApp;
import com.dotemplate.theme.server.ThemeContext;
import com.dotemplate.theme.server.ThemeUtils;
import com.dotemplate.theme.server.cms.CMS;


public class JSGeneratorDelegate {

	private final static Log log = LogFactory.getLog ( JSGeneratorDelegate.class );
	
	
	public static void generate ( PropertySet set ) throws AppException {
		
		if ( log.isDebugEnabled() ){
			log.debug( "Generating js for set " + set.getName() ) ;
		}
		
		VelocityContext context = new VelocityContext( ThemeApp.getThemeContext() ) ;
		
		context.put ( "s", set ) ;
		
		PropertySet symbol = DesignUtils.getSymbol ( set.getParent() ) ;
		
		String template = CMS.get ( ThemeApp.getThemeContext () ).getJsTemplate ( symbol )  ;
		
        StringWriter writer = new StringWriter() ;
        
        // generate js
        try {
        	
			ThemeApp.getRenderEngine ().generate ( context, template, writer ) ;
		
        } catch ( Exception e ) {
			
			throw new AppException( "Failed generate JS for set " + set.getLongName() , e ) ;
		}
        
        ThemeApp.getThemeContext().addRowScript ( set.getUid(), writer.getBuffer().toString () ) ;
        
        
        /*
        ThemeApp.getThemeContext().addOnReadyScript ( 
        		set.getUid(), ThemeUtils.trimJavascript( writer.getBuffer().toString () ) ) ;
        */
	}
	
		
	public static void generate () throws AppException {
	    
		if ( log.isDebugEnabled() ){
			
			log.debug( "Generating js resources " ) ;
		
		}
		
		ThemeContext themeContext = ThemeApp.getThemeContext() ;

		String modelName = themeContext.getDesign().getParent() ;

		String template = new Path ( "models" ).append ( modelName ).append( "theme.js.vm" ).asString() ;
					
		VelocityContext context = new VelocityContext ( themeContext ) ;
		
	    StringWriter writer = new StringWriter() ;
		
        // generate js
        try {
        	
			ThemeApp.getRenderEngine ().generate ( context, template, writer ) ;
		
        } catch ( Exception e ) {
			
			throw new AppException( "Failed generate model from " + template , e ) ;
		}
		
        themeContext.addRowScript ( "rowScripts" , writer.getBuffer().toString () ) ;
        
        
		try {
			
			exportScripts() ;
			exportThemeScripts() ;
			exportPolyfills() ;
			

		} catch ( Exception e ) {
			
			throw new AppException( "Failed generate scripts resource ", e ) ;
		
		}

		
		if ( log.isDebugEnabled() ){
			log.debug( "Javascript resources generated." ) ;
		}
	
	}
	
	
	
	/*
	public static void generate () throws AppException {
	    
		if ( log.isDebugEnabled() ){
			
			log.debug( "Generating js resources " ) ;
		
		}
		
		
		ThemeContext themeContext = ThemeApp.getThemeContext () ;
		
		PropertySet docSet  = ( PropertySet) themeContext.get( "doc" ) ;
		
		SetHierarchy.create( docSet ) ;
		
		
		ThemeRefresh themeRefresh = ThemeApp.getThemeContext() .getThemeRefresh() ;
		themeRefresh.clear() ;
		
		
		try {
			
			generate( docSet ) ;
		
		} finally {
			
			SetHierarchy.clear() ;
		
		}
		
		try {
			
			writeAddOnsScript() ;
			writeScripts() ;
			

		} catch ( Exception e ) {
			
			throw new AppException( "Failed generate scripts resource ", e ) ;
		
		}

		
		if ( log.isDebugEnabled() ){
			log.debug( "Javascript resources generated." ) ;
		}
	
	}*/
	
	
	
	
	private static void exportScripts () throws Exception {
		
		if ( log.isDebugEnabled() ){
			log.debug( "Merging all imported scripts..." ) ;
		}
		
		ThemeContext context = ThemeApp.getThemeContext() ;
		
		StringBuffer scripts = new StringBuffer () ;
		
		String path ;
		
		for ( String scriptId : context.getScripts() ) {
			
			try {
				
				path = ( "js/" + scriptId ).replace( '/', File.separatorChar ) ;

				
				if ( log.isDebugEnabled () ){
					log.debug ( "Merging script " + path ) ;
				}
				
				scripts.append( FileUtils.readFileToString( 
						new File ( ThemeApp.realPath ( path ) ) ) ).append( "\n\n" ) ;
				
			} catch ( IOException e ) {
				
				throw new AppRuntimeException( "Failed to read local scripts " +
						"while  generating theme " + context.getDesign().getUid(), e ) ;
			}
		
		}
		
		boolean download = ThemeApp.getThemeContext().isExportMode () ;
		
		Path scriptsPath 	= ( download ) ? new Path( ThemeApp.get().getExportDirectoryRealPath () ) : 
			new Path ( ThemeApp.get().getWorkRealPath () ) ;
					
		scriptsPath.append ( "js" ).append( "scripts.js" ) ;

		if ( scripts.length() > 0 ){
			
			FileUtils.writeStringToFile( scriptsPath.asFile (), scripts.toString ().trim() ) ;
			
		} else {
			
			FileUtils.deleteQuietly( scriptsPath.asFile () ) ;
		
		}
	}
	
	
	private static void exportThemeScripts () throws Exception {
		
		ThemeContext themeContext = ThemeApp.getThemeContext() ;
		
		boolean download = themeContext.isExportMode () ;
		
		Path scriptsFilePath 	= ( download ) ? new Path( ThemeApp.get().getExportDirectoryRealPath () ) : 
			new Path ( ThemeApp.get().getWorkRealPath () ) ; 
		
		scriptsFilePath.append ( "js" ).append( "theme.js" ) ;

		FileUtils.writeStringToFile( scriptsFilePath.asFile () , 
			"$(document).ready( function() {\n\n\n"
					+ ThemeUtils.getRowScripts( themeContext ) + "\n\n\n"  + 
			"});" ) ;
	}
	
	
	
	private static void exportPolyfills( ) throws AppException {

		ThemeContext themeContext = ThemeApp.getThemeContext() ;
		
		for ( String polyfill : themeContext.getPolyfills() ){
			try {
			
				FileUtils.copyFileToDirectory( new File( ThemeApp.realPath( "js/" + polyfill ) ), 
							new Path ( ThemeApp.get().getExportDirectoryRealPath() ).append( "js" ).asFile() ) ;
			
		
			} catch ( Exception e ){
				
				throw new AppException( "Failed to copy polyfill", e ) ;
			
			}
		}
	}
	
	
}
