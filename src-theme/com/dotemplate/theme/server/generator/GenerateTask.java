package com.dotemplate.theme.server.generator;



import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.DesignSession;
import com.dotemplate.core.server.DesignTaskContext;
import com.dotemplate.core.server.DesignUtils;
import com.dotemplate.core.server.SetHierarchy;
import com.dotemplate.core.server.frwk.AppException;
import com.dotemplate.core.server.util.ConcurrentTask;
import com.dotemplate.core.shared.properties.PropertySet;
import com.dotemplate.theme.server.ThemeApp;
import com.dotemplate.theme.server.cms.CMS;


public class GenerateTask extends ConcurrentTask< Void > {
	
	@SuppressWarnings("unused")
	private final static Log log = LogFactory.getLog ( GenerateTask.class );
	

	final static int SET_SVG = 0 ;
	
	final static int SET_CSS = 1 ;
	
	final static int SET_HTML = 2 ;	
	
	final static int SET_JS = 3 ;
	
	final static int THEME_CSS = 10 ;
	
	final static int THEME_JS = 11 ;
	
	final static int THEME_HTML = 20 ;
	
	
	
	
	@Override
	public Void call () throws Exception {

		GenerateTaskContext context = ( GenerateTaskContext ) super.taskContext ;

		// task executor use a thread pool, even using a InheritableThreadLocal to store EaseSession,
		// child thread inherits the correct EaseSession the first time only (at thread creation). 
		// As thread is reused and not re-created EaseSession must be set explicitly using the task context.
		DesignSession.set ( context.getDesignSession () ) ;
		
		try {
			
			PropertySet set ;
			
			switch ( context.getType () ){
	
				case SET_SVG :
					set = ( ( SVGSetContext ) context ).getSet () ;
					SetHierarchy.create( set ) ;
					SVGGeneratorDelegate.generate ( set ) ;
					break ;
			
				case SET_CSS :
					set = ( ( CSSSetContext ) context ).getSet () ;
					SetHierarchy.create( set ) ;
					CSSGeneratorDelegate.generate ( set ) ;
					break ;

				case SET_HTML :
					set = ( ( HTMLSetContext ) context ).getSet () ;
					SetHierarchy.create( set ) ;
					HTMLGeneratorDelegate.generate ( set ) ;
					break ;
					
				case SET_JS :
					set = ( ( JSSetContext ) context ).getSet () ;
					SetHierarchy.create( set ) ;
					JSGeneratorDelegate.generate ( set )  ;
					break ;
		
					
				
				case THEME_CSS :
					CSSGeneratorDelegate.generate () ;
					break ;
	
				case THEME_JS :
					JSGeneratorDelegate.generate () ;
					break ;
					
				case THEME_HTML :
					HTMLGeneratorDelegate.generate() ;
					break ;
			}
					
		} catch ( Exception e ){
			
			throw new AppException ( "Failed to execute generate task ", e ) ;
			
		} finally {
			
			SetHierarchy.clear() ;
			
			DesignSession.clear() ;
		}
		
		return null;
	}
	

	
	
	static abstract class GenerateTaskContext extends DesignTaskContext {
		abstract int getType() ;
	}
	
	
	
	static abstract class ThemeTaskContext extends GenerateTaskContext {	
	}
	
	
	static abstract class SetTaskContext extends GenerateTaskContext {
		
		protected PropertySet set ;

		
		public PropertySet getSet () {
			return set;
		}

		public void setSet ( PropertySet set ) {
			this.set = set;
		}
	
	}
	
	
	
	public static class SVGSetContext extends SetTaskContext {
			
		@Override
		public int getType () {
			return SET_SVG ;
		}
		
		
		public boolean isValid () {
			String parent ;
			
			if ( set == null || ( parent = set.getParent () ) == null 
				|| ( set.getEnable () != null && !set.getEnable ().booleanValue () ) ) {
				
				return false ;
			
			}
					
			PropertySet symbol = DesignUtils.getSymbol ( parent );
			
			// Generate SVG template so it has to be generated 
			return symbol != null && ThemeApp.getRenderEngine ().templateExists (
					symbol.getPath () + "/svg.vm" ) ;
		
		}
		
	}

	
	public static class CSSSetContext extends SetTaskContext {

		@Override
		public int getType () {
			return SET_CSS ;
		}

		public boolean isValid () {

			String parent ;
			
			if ( set == null || ( parent = set.getParent () ) == null ) return false ;
					
			PropertySet symbol = DesignUtils.getSymbol ( parent );
		
			// symbol has CSS template so we must regenerate it
			return symbol != null && CMS.get ( CMS.XHTML ).getCssTemplate ( symbol ) != null ;	
		
		}
	}
	
	
	
	public static class JSSetContext extends SetTaskContext {

		@Override
		public int getType () {
			return SET_JS ;
		}

		public boolean isValid () {

			String parent ;
			
			if ( set == null || ( parent = set.getParent () ) == null ) return false ;
					
			PropertySet symbol = DesignUtils.getSymbol ( parent );
		
			// symbol has JS template so we must regenerate it
			return symbol != null && CMS.get ( CMS.XHTML ).getJsTemplate ( symbol ) != null ;	
		
		}
	}
	
	
	public static class HTMLSetContext extends SetTaskContext {

		@Override
		public int getType () {
			return SET_HTML ;
		}

		
		public boolean isValid () {

			String parent ;
			
			if (  set.getChangeLayout() == null || ( set.getChangeLayout() != null && !set.getChangeLayout().booleanValue() ) ) {
				return false ;
			}
			
			if ( set == null || ( parent = set.getParent () ) == null ) return false ;
					
			PropertySet symbol = DesignUtils.getSymbol ( parent );
		
			// symbol has HTML template so we must regenerate it
			return symbol != null && CMS.get ( CMS.XHTML ).getHtmlTemplate ( symbol ) != null ;
		}
		
	}
	
	
	
	public static class JSThemeContext extends ThemeTaskContext {

		@Override
		public int getType () {
			return THEME_JS ;
		}

		public boolean isValid () {
			return true ;
		}
	}
	

	
	public static class CSSThemeContext extends ThemeTaskContext {

		@Override
		public int getType () {
			return THEME_CSS ;
		}

		public boolean isValid () {
			return true ;
		}
	}
	
	
	public static class HTMLThemeContext extends ThemeTaskContext {

		@Override
		public int getType () {
			return THEME_HTML ;
		}

		public boolean isValid () {
			return true ;
		}

	}
	

}
