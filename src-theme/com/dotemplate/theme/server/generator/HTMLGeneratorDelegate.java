package com.dotemplate.theme.server.generator;


import java.io.Writer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.velocity.VelocityContext;

import com.dotemplate.core.server.App;
import com.dotemplate.core.server.DesignUtils;
import com.dotemplate.core.server.frwk.AppException;
import com.dotemplate.core.server.util.Duration;
import com.dotemplate.core.shared.properties.PropertySet;
import com.dotemplate.theme.server.ThemeApp;
import com.dotemplate.theme.server.cms.CMS;
import com.dotemplate.theme.server.cms.xhtml.XHTMLEngine;


public class HTMLGeneratorDelegate {

	private final static Log log = LogFactory.getLog ( HTMLGeneratorDelegate.class );
		

	
	public static void generate () throws AppException {
	    
		if ( log.isDebugEnabled() ){
			log.debug( "Generating html document for site "  ) ;
		}
		
		try {
			
			( ( XHTMLEngine ) ThemeApp.get().getExportEngine ( CMS.XHTML ) ).exportDoc() ;
		
		} catch ( Exception e ) {
		
			throw new AppException ( "Failed to generate theme HTML" , e ) ;
		
		}
		
		
	}

	@Duration
	public static void generate ( PropertySet set ) throws AppException {
		
		if ( log.isInfoEnabled() && App.isDevMode() ){
			
			System.out.println ( "\n\nhtml		>  " + set.getLongName() ) ;
		
		}		
				
		generate ( set, new VoidWriter() ) ;
		
	}

	
	public static void generate ( PropertySet set, Writer writer ) throws AppException {
		
		if ( log.isInfoEnabled() ){
			log.info ( "Generating html updates for set " + set.getName () );
		}

		
		if ( log.isInfoEnabled() && App.isDevMode() ){
			
			System.out.println ( "\n\nhtml		>  " + set.getLongName() ) ;
		
		}		
		
		
		VelocityContext context = new VelocityContext ( ThemeApp.getThemeContext() );

		context.put ( "s", set );

		String template = DesignUtils.getSymbol ( set.getParent () ).getPath () + "/html.vm";


		try {

			ThemeApp.getRenderEngine ().generate ( context, template, writer );

		} catch ( Exception e ) {

			throw new AppException ( "Failed to generate html for set '" + set.getName () + "'", e );
		}
		
	}

	
	
}
