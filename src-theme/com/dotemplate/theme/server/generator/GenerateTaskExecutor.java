package com.dotemplate.theme.server.generator;

import com.dotemplate.core.server.util.ConcurrentTask;
import com.dotemplate.core.server.util.ConcurrentTaskExecutor;
import com.dotemplate.core.server.util.ConcurrentTaskPool;



public class GenerateTaskExecutor  extends ConcurrentTaskExecutor<Void> {

	public GenerateTaskExecutor () {
		super ( 70 );
	}
	
	@Override
	protected ConcurrentTaskPool<Void> createPool () {
		
		return new ConcurrentTaskPool<Void>() {
			
			@Override
			protected ConcurrentTask<Void> createTask () throws Exception {
				return new GenerateTask() ;
			}
		};
	}
		

}
