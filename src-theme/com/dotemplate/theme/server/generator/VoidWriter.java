package com.dotemplate.theme.server.generator;

import java.io.IOException;
import java.io.Writer;

/**
 * A dummy Writer just to mock the Velocity Template.merge when
 * no output content is expected from the merge
 * 
 */
public class VoidWriter extends Writer {

	@Override
	public void close () throws IOException {
	}

	@Override
	public void flush () throws IOException {
	}

	@Override
	public void write ( char [] arg0, int arg1, int arg2 ) throws IOException {
	}
	

	public Writer append ( char arg0 ) throws IOException {
		return this ;
	}
	

	@Override
	public Writer append ( CharSequence arg0, int arg1, int arg2 )
			throws IOException {
		return this ;
	}
	
	@Override
	public Writer append ( CharSequence arg0 ) throws IOException {
		return this ;
	}

	@Override
	public void write ( char [] arg0 ) throws IOException {
	}

	@Override
	public void write ( int arg0 ) throws IOException {
	}
	
	
	@Override
	public void write ( String arg0 ) throws IOException {
	}
	
	
	@Override
	public void write ( String arg0, int arg1, int arg2 ) throws IOException {
	}
}
