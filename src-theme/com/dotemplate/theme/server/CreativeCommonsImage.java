package com.dotemplate.theme.server;

import com.dotemplate.core.shared.DesignResource;

public class CreativeCommonsImage implements DesignResource {

	private static final long serialVersionUID = -4955637156586475322L;
	
	public String name ;
	
	public String uri ;
	
	public String desc ;
	
	public String href ;

	
	public String getUri () {
		return uri;
	}

	public void setUri ( String uri ) {
		this.uri = uri;
	}

	public String getDesc () {
		return desc;
	}

	public void setDesc ( String desc ) {
		this.desc = desc;
	}

	public String getHref () {
		return href;
	}

	public void setHref ( String href ) {
		this.href = href;
	}

	public String getName () {
		return name;
	}

	public void setName ( String name ) {
		this.name = name;
	}

	@Override
	public String getThumbnailHtml () {
		return null;
	}

	@Override
	public boolean isPremium () {
		return false;
	}

	public String getFilter () {
		return null ;
	}

	@Override
	public String getTags() {
		return null;
	}
	
	
	
}
