package com.dotemplate.theme.server;

import java.util.ArrayList;

import com.dotemplate.core.server.DesignSession;
import com.dotemplate.core.server.frwk.AppException;
import com.dotemplate.core.server.frwk.AppRuntimeException;
import com.dotemplate.core.server.util.Path;
import com.dotemplate.core.server.util.UIDGenerator;
import com.dotemplate.core.shared.properties.Property;
import com.dotemplate.core.shared.properties.PropertySet;
import com.dotemplate.core.shared.properties.RefProperty;
import com.dotemplate.theme.server.generator.VoidWriter;
import com.dotemplate.theme.server.web.UserTheme;
import com.dotemplate.theme.shared.Theme;

public class ThemeUtils {

	
	public static void buildDependencies( ThemeContext themeContext ) {
		
		Theme theme  = themeContext.getDesign() ; 
		
		ArrayList< PropertySet > updatableSets = new ArrayList< PropertySet >() ;
		
		for ( PropertySet set : theme.getPropertySets() ){
			buildUpdatableSets( themeContext, set, updatableSets ) ;
		}
		
		for ( PropertySet set : updatableSets ){
			buildDependencies( themeContext, set, null ) ;
		}
		
	}
	

	private static void buildUpdatableSets( ThemeContext themeContext, PropertySet set, ArrayList< PropertySet > updatableSets ){
		
		if ( set == null ) {
			return ;
		}
		
		boolean editable = set.getEditable() != null && set.getEditable().booleanValue() ;
		
		if ( editable && set.selectable() ) {
			updatableSets.add( set ) ;
		}
		
		for ( Property p : set.getProperties() ){
			
			if ( p.isSet() ){
				
				buildUpdatableSets( themeContext, ( PropertySet ) p, updatableSets ) ;
			
			} else {
				
				if ( p.getType() == Property.REF ) {
					//System.out.println( "Ref " + ( ( RefProperty ) p ).getValue () ) ;
					buildUpdatableSets ( themeContext, 
							( PropertySet ) themeContext.get ( ( ( RefProperty ) p ).getValue () ), 
								updatableSets ) ;
				}
			}
		}
		
	}
	
	
	
	private static void buildDependencies( ThemeContext themeContext, PropertySet updatableSet, PropertySet set ) {
		
		PropertySet subset ;

		if ( set == null ) set = updatableSet ;
		
		boolean editable ;
		
		// condition on updatableSet.mutable() commented because it makes modifications on non clipped header not taken into account   
		if ( updatableSet.selectable() /* && updatableSet.mutable() */  ) {
			themeContext.getDependencies().add( updatableSet , updatableSet ) ;
		}
		
		
		for ( Property p : set.getProperties() ){
			
			editable = p.getEditable() != null && p.getEditable().booleanValue() ;
			
			if( !editable ) continue ;
			
			if ( p.isSet() ){
				
				subset = ( PropertySet ) p ;
				
				if ( subset.selectable()){
					continue ;
				}
					
				themeContext.getDependencies().add( p , updatableSet ) ;
				
				buildDependencies( themeContext, updatableSet, subset ) ;

				
			} else {
				
				themeContext.getDependencies().add( p , updatableSet ) ;
							
			}
		}
	}


	public static String getPermalink() {
		return getPermalink( ThemeApp.getDesignUid() ) ;
	}

	public static String getPermalink( String uid ) {
		return new StringBuffer( ThemeApp.getConfig().getBaseUrl() )
			.append( UserTheme.URL_MAPPING )
			.append( "?")
			.append( uid )
			.toString() ;
	}

	
	public static String getRowScripts( ThemeContext themeContext ) {
		
		StringBuffer sb = new StringBuffer() ; 
		
		for ( String s : themeContext.getRowScripts() ) {
			//sb.append( trimJavascript ( s ) ) ;
			sb.append( s ).append( "\n\n") ;
		}
		
		return sb.toString() ;
	
	}

	
	public static String trimJavascript( String js ) {
		return js.replaceAll("[\t\r]", "" ) ;
	}


	public static boolean isUserTheme( Theme theme ) {
		return theme.isPurchased() ;
	}
	
	
	public static Theme getModel( Theme theme ) {
		
		String model = theme.getParent() ;
		
		if ( model == null ) return theme ;

		Path modelPath = new Path ( ThemeApp.realPath( ThemeApp.getConfig ().getModelsPath () ) )
			.append( model ).append( "theme.xml" ) ;
		
			
		try {
			
			return ThemeApp.getThemeFactory().create ( modelPath.asFile()) ;
			
		} catch (AppException e) {

			throw new AppRuntimeException( "Failed to build parent model theme from " + model )  ;

		}
				
	}
	
	
	public static void applyColorSchemeToModel( Theme model, String scheme ) throws AppRuntimeException {
		
		model.setScheme( scheme ) ;
		
		ThemeContextFactory themeContextFactory = ( ThemeContextFactory ) ThemeApp.getSingleton ( 
				ThemeContextFactory.class ) ;
		
		ThemeContext themeContext = themeContextFactory.create ( model ) ;
		
		model.setUid( UIDGenerator.pickId() ) ;

		DesignSession.get().setDesignContext( themeContext ) ;
		
		//model.setParent( theme.getParent() ) ;
		model.setScheme( scheme ) ;

		
        String template  = new Path( "models" ).append( model.getName() )
        		.append( "colorize.vm" ).asString() ;
        
        if ( ! ThemeApp.getRenderEngine ().templateExists( template ) ) {
        	throw new AppRuntimeException( "Problem colorizing theme model " + model.getName() ) ;
        }
        
		try {
						
			ThemeApp.getRenderEngine ().generate (  themeContext, template, new VoidWriter() ) ;
		
		} catch ( Exception e ) {
			
			throw new AppRuntimeException ( "Failed to apply settings for set " + model.getName() , e ) ;
		
		}	
		
	}
	
	
	public static boolean hasFullFeatures() {
		ThemeContext ctx = ThemeApp.getThemeContext() ;
		return !ctx.isExportMode() || ( ctx.isExportMode() && ctx.getDesign().isPurchased() ) ;
	}

//	@Deprecated
//	public static CategoryContent getCategoryContent( Theme theme  ){
//		
//		String[] splitted  = StringUtils.split( theme.getContent(), "." ) ;
//		
//		return ThemeApp.getThemeManager().getThemeContentCategories().getCategoryContent(
//				splitted[ 0 ], splitted[ 1 ] );
//	
//		
//	}

	
}
