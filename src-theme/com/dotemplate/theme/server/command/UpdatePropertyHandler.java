package com.dotemplate.theme.server.command;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.frwk.CommandHandler;
import com.dotemplate.core.shared.frwk.Command;
import com.dotemplate.core.shared.frwk.WebException;
import com.dotemplate.core.shared.properties.Property;
import com.dotemplate.theme.server.ThemeApp;
import com.dotemplate.theme.shared.ThemeUpdate;
import com.dotemplate.theme.shared.command.UpdateProperty;
import com.dotemplate.theme.shared.command.UpdatePropertyResponse;



public class UpdatePropertyHandler extends CommandHandler< UpdatePropertyResponse > {

	
	private static Log log = LogFactory.getLog ( UpdatePropertyHandler.class );
	

	
	@Override
	public UpdatePropertyResponse handleAction ( Command< UpdatePropertyResponse > action )
							throws WebException {
		
		
		Property property = ( ( UpdateProperty  ) action ).getProperty () ; 

		
		if( log.isInfoEnabled () ){
			log.info ( "Update property " + property.getLongName () + " [uid=" + property.getUid () + "]" ) ;
		}
		
		
		UpdatePropertyResponse response = new UpdatePropertyResponse() ;
	
		try {
			
			ThemeUpdate refresh =  ( ThemeUpdate ) ThemeApp.getThemeManager ().updateProperty ( property ) ; 
			
			response.setClientRefresh ( refresh ) ;
			
			if ( log.isInfoEnabled () && ThemeApp.isDevMode() ){
			
				StringBuffer sb = new StringBuffer() ;
				
				sb.append ( "\n\nRefresh CSS\n" + refresh.getCss() ) ;
				
				sb.append ( "\n\nRefresh JS\n" + refresh.getJs () ) ;
				
				sb.append ( "\n\nRefresh HTML\n" ) ;
				
				
				for ( String key : refresh.getHtml ().keySet () ){
					sb.append( key ).append( ":\n" ).append ( refresh.getHtml ().get ( key ) ) ;
				}
				
				log.info ( sb.toString () ) ;

			}
			
			
		} catch ( Exception e ) {
			
			throw syserror ( "Sorry. Failed to Update property " + property.getLongName() , e ) ;
			
		}

		
		return response ;
		
		
	}
	
				

}
