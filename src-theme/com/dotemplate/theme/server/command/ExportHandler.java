package com.dotemplate.theme.server.command;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


import com.dotemplate.core.server.frwk.CommandHandler;
import com.dotemplate.core.shared.frwk.Command;
import com.dotemplate.core.shared.frwk.WebException;
import com.dotemplate.theme.server.ThemeApp;
import com.dotemplate.theme.shared.command.Export;
import com.dotemplate.theme.shared.command.ExportResponse;



public class ExportHandler extends CommandHandler< ExportResponse > {

	
	private static Log log = LogFactory.getLog ( ExportHandler.class );
	

	
	@Override
	public ExportResponse handleAction ( Command< ExportResponse > action )
							throws WebException {
		
		Export export =  ( Export  ) action ;
		
		String cmsType  = export.getCmsType () ; 

		if( log.isInfoEnabled () ){
			
			log.info ( "Export theme as '" + cmsType +  "' format " ) ;
		
		}
		
		
		ExportResponse response = new ExportResponse() ;
		
		try {
			
			ThemeApp.getThemeManager().export ( cmsType, export.isZip() ) ;
			
		} catch ( Exception e ) {
			
			throw syserror ( "Failed to export theme. Sorry.", e ) ;
		}

		return response ;
		
		
	}
	
				

}
