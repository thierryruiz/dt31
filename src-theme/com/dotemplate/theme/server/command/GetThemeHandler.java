package com.dotemplate.theme.server.command;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.frwk.CommandHandler;
import com.dotemplate.core.shared.frwk.Command;
import com.dotemplate.core.shared.frwk.WebException;
import com.dotemplate.theme.server.ThemeApp;
import com.dotemplate.theme.shared.Theme;
import com.dotemplate.theme.shared.command.GetThemeResponse;

public class GetThemeHandler extends CommandHandler<GetThemeResponse> {

	private final static Log log = LogFactory.getLog(GetThemeHandler.class);

	
	@Override
	public GetThemeResponse handleAction(Command<GetThemeResponse> action)
			throws WebException {

		if (log.isInfoEnabled()) {
			log.info( "Loading theme ..." );
		}

		Theme theme = ThemeApp.getTheme();

		/*
		if (theme == null) {

			// using GWT developer mode the site has to be created here

			try {

				theme = ThemeApp.getThemeManager()
						.createTheme( "_t-c-b-noMargins" );

				ThemeApp.setTheme(theme);

			} catch (Exception e) {
				e.printStackTrace();
			}
		}*/
		
		

		/*
		 * { // WP plugin test GetSite getSite = ( GetSite ) action ; String
		 * themeId = getSite.getUserThemeId () ;
		 * 
		 * try { Theme theme = Ease.getThemeMgr ().loadTheme ( themeId ); Site
		 * site = Ease.getSiteFactory ().createSite ( theme, UIDGenerator.pickId
		 * () ) ; EaseSession.get ().addSite ( site ) ;
		 * 
		 * } catch ( EaseException e ) { e.printStackTrace(); } }
		 */

		GetThemeResponse response = new GetThemeResponse();

		response.setTheme(theme);

		if (log.isDebugEnabled()) {
			log.debug("Theme loaded...");
		}


		return response;

	}

}
