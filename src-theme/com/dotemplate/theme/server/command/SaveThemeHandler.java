package com.dotemplate.theme.server.command;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.frwk.CommandHandler;
import com.dotemplate.core.shared.frwk.Command;
import com.dotemplate.core.shared.frwk.WebException;
import com.dotemplate.theme.server.ThemeApp;
import com.dotemplate.theme.shared.command.SaveThemeResponse;

public class SaveThemeHandler extends CommandHandler< SaveThemeResponse > {

	
	private final static Log log = LogFactory.getLog ( SaveThemeHandler.class );
	
	@Override
	public SaveThemeResponse handleAction ( Command< SaveThemeResponse > action ) throws WebException {
		
		if( log.isInfoEnabled() ){
			log.info("Saving theme ..." ) ;
		}
		
		try {
			
			ThemeApp.getThemeManager ().saveTheme() ;
			
		} catch ( Exception e ) {
			e.printStackTrace() ;
			log.error( e ) ;
			throw new WebException( "Failed to save theme sorry. " +
					"Please contact us.") ;
		
		}
		
		
		SaveThemeResponse response = new SaveThemeResponse() ;
		

		if( log.isDebugEnabled() ){
			log.debug( "Theme saved...") ;
		}
		
		return response ;
		
	}
	
	
}
