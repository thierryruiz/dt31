package com.dotemplate.theme.server;

import java.io.Serializable;

public class GoogleFont implements Serializable {

	private static final long serialVersionUID = -1720564510880449227L;
	
	private String title ;
	
	private String family ;
	
	private String weight ;
	
	private String style ;
	
	private String urlParams ;
	
	private String name ;
	
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	
	public String getTitle() {
		return title;
	}
	
	
	public String getUrlParams() {
		return urlParams;
	}

	public void setUrlParams(String url) {
		this.urlParams = url;
	}


	public String getFamily() {
		return family;
	}

	public void setFamily(String family) {
		this.family = family;
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	public String getStyle() {
		return style;
	}

	public void setStyle(String style) {
		this.style = style;
	}


	

	
	
}
