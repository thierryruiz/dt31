package com.dotemplate.theme.server;

import com.dotemplate.core.server.AppConfig;


public class ThemeAppConfig extends AppConfig {
	
	private String webContext ;
	
	private String jqueryUri ;
	
	
	public String getWebContext() {
		return webContext;
	}

	public void setWebContext(String webContext) {
		this.webContext = webContext;
	}
	
	
	public String getJqueryUri() {
		return jqueryUri;
	}
	
	
	public void setJqueryUri(String jqueryUri ) {
		this.jqueryUri = jqueryUri;
	}
	
	
}
