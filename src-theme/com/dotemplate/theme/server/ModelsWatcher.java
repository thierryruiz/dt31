package com.dotemplate.theme.server;

import java.io.IOException;
import java.nio.file.Paths;

import com.dotemplate.core.server.App;
import com.dotemplate.core.server.frwk.AppRuntimeException;
import com.dotemplate.core.server.watcher.DirWatcher;
import com.dotemplate.core.server.watcher.FileUpdateListener;


// Not used - Does not seems needed when model is updated  ?
public class ModelsWatcher implements FileUpdateListener {
	
	private DirWatcher watcher ;
	
	public ModelsWatcher() {
		
		try {
			
			watcher = new DirWatcher( Paths.get( App.realPath ( "WEB-INF/templates/models" )), true, this ) ;
			
			Thread th = new Thread(watcher, "ModelsWatcher");
	        th.start();

			
		} catch (IOException e) {
			throw new AppRuntimeException("Failed to create models watcher", e  ) ;
		}
	}
	
	

		
	@Override
	public void onFileUpdated(String path) {
		if ( ! path.endsWith("theme.xml") ) {
			return ;
		}
		
	}
	
	@Override
	public void onFileDeleted(String path) {
	}
	
	
	@Override
	public void onFileCreated(String path) {
		if ( ! path.endsWith("theme.xml") ) {
			return ;
		}
		
	}

	
	
	
	
	
	
	
	
	
	
	
	

}
