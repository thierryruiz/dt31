package com.dotemplate.theme.server;

import java.io.File;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.DesignMerger;
import com.dotemplate.core.server.frwk.AppException;
import com.dotemplate.core.server.util.Path;
import com.dotemplate.theme.shared.Theme;


public class ThemeFactory {

	private final static Log log = LogFactory.getLog ( ThemeFactory.class ) ;
	
	protected ThemeDescriptorReaderPool themeReaderPool ;
	
	public Theme create( File descriptor ) throws AppException {
		
		if( !descriptor.exists() ){
			
			throw new ThemeDescriptorNotFound( "Theme descriptor " + descriptor.getAbsolutePath() + " not found " ) ;
		
		}

		
		if ( themeReaderPool == null  ){		
			themeReaderPool = new ThemeDescriptorReaderPool () ;
		}
		
		
		Theme theme = null ;
		ThemeDescriptorReader themeReader = null ;
			
		try {
			
			themeReader =  ( ThemeDescriptorReader ) themeReaderPool.borrow() ;
						
			theme = themeReader.read ( descriptor  ) ;
			
			
		} catch ( Exception e ) {
			
			log.error( e ) ;

			throw new AppException( "Unable to generate theme from decriptor '" 
					+ descriptor.getAbsolutePath() , e  ) ;
			
		} finally {
			
			try {
				
				themeReaderPool.release( themeReader ) ;
			
			} catch ( Exception e ) {
				
				throw new AppException( "Failed to return ThemeReader instance to pool! " , e )  ;
			
			}
		}
		
		
		String model = theme.getParent() ;
		
		
		if ( model == null ) {
			return theme ;
		}
	
	
		Path modelPath = new Path ( ThemeApp.realPath( ThemeApp.getConfig ().getModelsPath () ) )
			.append( model ).append( "theme.xml" ) ;
		
		
		if ( ! modelPath.asFile().exists() ){
			throw new AppException ( "Unable to locate parent theme file '" + model + "' descriptor" ) ;
		}
		
		
		Theme parentTheme = create ( modelPath.asFile() ) ;
		
		parentTheme.setName( model ) ;
		
		if( ! model.startsWith("abstract") ){
			
			ThemeUtils.applyColorSchemeToModel( parentTheme, theme.getScheme() ) ;	
		}
		
		try {
			
			DesignMerger.merge( theme, parentTheme ) ;
			
		} catch ( Exception e ) {
			
			throw new AppException( "Unable to build theme " + descriptor.getAbsolutePath() + " from its parent", e ) ;
		
		}
		
		return theme ;
		
	}
	
	
	
	
	/*
	public Theme createTheme ( String name, String uid, String affiliateId ) throws AppException {

		if ( log.isInfoEnabled () ){
			log.info (  "Creating theme from descriptor '" + name +  "'..." ) ;
		}
	
		File descriptor = new File ( App.realPath(  App.getConfig ().getDesignsPath () + "/"
				+ name + ".xml" ) ) ;
						
		return doCreateTheme( descriptor, name, uid, affiliateId ) ;
		
	}
	
	
	public Theme createUserTheme ( String uid ) throws AppException {

		if ( log.isInfoEnabled () ){
			log.info (  "Creating user theme from uid '" + uid +  "'..." ) ;
		}

		PurchaseInfo purchaseInfo = PurchaseInfoHelper.load ( uid ) ;
				
		if ( purchaseInfo == null ) {
			return null ;
		}
		
		
		File descriptor = new File ( App.realPath( new Path ( App.getConfig ().getWorkRoot () ).append (
			uid ).append( "theme.xml" ).toString() ) ) ;
		
		
		Theme theme =  doCreateTheme( descriptor, uid, uid, purchaseInfo.getAffiliateId() ) ;

		ThemeApp.getThemeContext().setPurchaseInfo( purchaseInfo ) ;
		
		return theme ;
		
	}
	*/
	
	
	/*
	protected Theme doCreateTheme ( File descriptor, String name, String themeUid, String affiliateId ) throws AppException {
		
		try {
			
			Theme theme = create ( descriptor ) ; 
			
			theme.setUid( themeUid ) ;	
			theme.setName ( name ) ;
			
			Affiliate affiliate = ThemeApp.getAffiliateManager().find( affiliateId ) ; 
			
			ThemeContextFactory themeContextFactory = ( ThemeContextFactory ) ThemeApp.getSingleton ( 
					ThemeContextFactory.class ) ;
			
			
			ThemeContext themeContext = themeContextFactory.create ( theme, affiliate ) ;
			
			DesignSession session = DesignSession.get() ;
			
			session.addDesignContext ( themeContext ) ;
			session.setDesignContext ( themeContext ) ;
			
			CMS.set( themeContext, CMS.TYPE_XHTML ) ;
			
			Path outputPath = new Path ( App.realPath( ThemeApp.getConfig().getWorkRoot() ) ) ;
			
			outputPath.append( themeUid ) ; 
			
			File outputDir = outputPath.asFile();
			

			if ( log.isDebugEnabled () ){
				log.debug (  "Creating theme directory structure "
						+ outputDir.getAbsolutePath() + "..." ) ;
			}
			
			
			File cssDir 	= new File( outputDir.getAbsolutePath()  + File.separator + "css" ) ;
			File imageDir 	= new File( outputDir.getAbsolutePath()  + File.separator + "images" ) ;
			File jsDir 		= new File( outputDir.getAbsolutePath()  + File.separator + "js" ) ;
			
			
			FileUtils.forceMkdir ( outputDir )   ;
			FileUtils.forceMkdir ( cssDir )   ;
			FileUtils.forceMkdir ( imageDir )   ;
			FileUtils.forceMkdir ( jsDir )   ;
			

			ThemeApp.getThemeManager().generateDesign () ;
			
			return theme ;
			
			
		} catch ( Exception e ) {
			e.printStackTrace() ;
			log.error( e ) ;
			throw new AppException( "Unable to generate theme from decriptor '" 
					+ descriptor.getAbsolutePath() , e  ) ;
			
			
		} 

	}*/
		
	
	/*
	protected Theme createTheme ( File descriptor, String name, String themeUid ) throws AppException {
		
		if ( log.isInfoEnabled () ){
			
			log.info (  "Creating theme from descriptor " + descriptor.getAbsolutePath() ) ;
		
		}
		

		if( !descriptor.exists() ){
			throw new AppException(" Theme descriptor " + descriptor.getAbsolutePath() + " not found " ) ;
			//return null ;
		}

		
		if ( themeReaderPool == null  ){
			themeReaderPool = new ThemeReaderPool () ;
		}
				
		ThemeReader themeReader = null ;
		
		try {
			
			themeReader =  ( ThemeReader ) themeReaderPool.borrow() ;
			
			if ( ! descriptor.exists() ){
				
				return null ;
			
			}
			
			Theme theme = themeReader.read ( descriptor  ) ;
			
			theme.setUid( themeUid ) ;
			
			if ( name != null ){
				
				theme.setName ( name ) ;
			
			}
			
			
			ThemeContextFactory themeContextFactory = ( ThemeContextFactory ) ThemeApp.getSingleton ( 
					ThemeContextFactory.class ) ;
			
			ThemeContext themeContext = themeContextFactory.create ( theme ) ;
			
			DesignSession session = DesignSession.get() ;
			
			session.addDesignContext ( themeContext ) ;
			session.setDesignContext ( themeContext ) ;
			
			
			CMS.set( themeContext, CMS.TYPE_XHTML ) ;
			
			Path outputPath = new Path ( App.realPath( ThemeApp.getConfig().getWorkRoot() ) ) ;
			
			outputPath.append( themeUid ) ; 
			
			File outputDir = outputPath.asFile();
			

			if ( log.isDebugEnabled () ){
				log.debug (  "Creating theme directory structure "
						+ outputDir.getAbsolutePath() + "..." ) ;
			}
			
			
			File cssDir 	= new File( outputDir.getAbsolutePath()  + File.separator + "css" ) ;
			File imageDir 	= new File( outputDir.getAbsolutePath()  + File.separator + "images" ) ;
			File jsDir 		= new File( outputDir.getAbsolutePath()  + File.separator + "js" ) ;
			
			
			FileUtils.forceMkdir ( outputDir )   ;
			FileUtils.forceMkdir ( cssDir )   ;
			FileUtils.forceMkdir ( imageDir )   ;
			FileUtils.forceMkdir ( jsDir )   ;
			

			ThemeApp.getThemeManager().generateDesign () ;
			
			return theme ;
			
			
		} catch ( Exception e ) {
			e.printStackTrace();
			log.error( e ) ;
			throw new AppException( "Unable to generate theme from decriptor '" 
					+ descriptor.getAbsolutePath() , e  ) ;
			
			
		} finally {
			
			try {
				
				themeReaderPool.release( themeReader ) ;
			
			} catch (Exception e) {
				
				throw new AppException( "Failed to return ThemeReader instance to pool! " , e )  ;
			}
		}	
	} */
	
	
	
}
