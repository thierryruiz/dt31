package com.dotemplate.theme.server.web;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.App;
import com.dotemplate.core.server.DesignLogger;
import com.dotemplate.core.server.DesignSession;
import com.dotemplate.core.server.PurchaseInfo;
import com.dotemplate.core.server.PurchaseInfoHelper;
import com.dotemplate.core.server.frwk.AppException;
import com.dotemplate.core.server.util.UIDGenerator;
import com.dotemplate.core.server.web.BaseHttpServlet;
import com.dotemplate.theme.server.ThemeApp;
import com.dotemplate.theme.server.ThemeContext;
import com.dotemplate.theme.server.ThemePreprocessor;
import com.dotemplate.theme.server.ThemeUtils;
import com.dotemplate.theme.shared.Theme;

@SuppressWarnings("unused")
public class UserTheme extends BaseHttpServlet {

	public static String URL_MAPPING = "dt/u" ;
	
	private static final long serialVersionUID = 3765978745696149755L;
	
	private static Log log = LogFactory.getLog ( UserTheme.class );
	
	protected void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
			
		nocache( response ) ;
		response.setContentType( "text/html" ); 
	
		String baseUrl =  getBaseUrl( request )  ;
		
		ThemeApp.getConfig().setBaseUrl( baseUrl ) ;
	
		Enumeration<String> params  = request.getParameterNames() ;
		
		
		if ( !params.hasMoreElements() ){
			
			forward404( request, response ) ;
		
		}
		
		String themeUid = params.nextElement()  ;
		
		PurchaseInfo info  = null ;
		
		try {
			
			info = PurchaseInfoHelper.load( themeUid ) ;

		} catch ( AppException e ) {
			
			log.error( "Unable to load purchase info" , e ) ;
			forwardUnexpectedError( request, response ) ;
			return ;
		}
		
		String createUrl = new StringBuffer( ThemeApp.getConfig().getBaseUrl() )
			.append( CreateUserTheme.URL_MAPPING )
			.append( "?")
			.append( themeUid )
			.toString() ;
		
		
		request.setAttribute( "createUrl", createUrl ) ;
		request.setAttribute( "downloads", info.getDownloads() ) ;	
		
		// FIXME
		// request.setAttribute( "affiliateName", ... ) ;		
		// request.setAttribute( "affiliateUrl", ... ) ;
		
		//response.sendRedirect ( redirectUrl.toString() ) ;

		forwardTo( "user-theme.jsp", request, response ) ;
		
	}

}
