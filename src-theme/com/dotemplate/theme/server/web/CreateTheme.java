package com.dotemplate.theme.server.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.App;
import com.dotemplate.core.server.DesignLogger;
import com.dotemplate.core.server.DesignSession;
import com.dotemplate.core.server.util.UIDGenerator;
import com.dotemplate.core.server.web.BaseHttpServlet;
import com.dotemplate.theme.server.ThemeApp;
import com.dotemplate.theme.server.ThemeContext;
import com.dotemplate.theme.server.ThemeDescriptorNotFound;
import com.dotemplate.theme.shared.Theme;

@SuppressWarnings("unused")
public class CreateTheme extends BaseHttpServlet {

	private static final long serialVersionUID = 3765978745696149755L;

	private static Log log = LogFactory.getLog( CreateTheme.class );

	
	protected void doGet( HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		nocache( response );
		
		super.forwardTo( "create-proxy.jsp", request, response ) ;

	}

	
	protected void doPost( HttpServletRequest request,
			HttpServletResponse response ) throws ServletException, IOException {

		nocache( response );
		response.setContentType( "text/plain" );
		

		String baseUrl = getBaseUrl(request);

		ThemeApp.getConfig().setBaseUrl( baseUrl );

		String errorUrl = new StringBuffer().append(request.getScheme())
				.append("://").append(request.getServerName()).append(":")
				.append(request.getServerPort()).toString();

		
		String themeName = request.getParameter( "id" );

		if ( themeName == null ) {
			error( response, "Bad request! Sorry." ) ;
			return;
		}
		
		
		String affiliateId = request.getParameter( "affid" );
		
		if ( affiliateId == null ) {
			affiliateId = "dotemplate" ;
		}
		
		boolean randomize= request.getParameter( "randomize" ) != null ;

		
		if ( log.isInfoEnabled() ) {

			log.info("Creating theme from : " + themeName );

		}

		Theme theme;

		ensureDesignSession( request ) ;
		
		try {
	
			ThemeApp.getThemeManager().createTheme( themeName, UIDGenerator.pickId(), affiliateId, randomize );			


		}  catch ( ThemeDescriptorNotFound e ) {
			
			log.error ( e ) ;
			
			forward404( request, response ) ;
			
			return;
			
		} catch ( Exception e ) {

			error( response, SYSERROR ) ;
			
			return;
		}


		try {
			
			ThemeApp.getThemeManager().generateDesign() ;
			
		} catch (  Exception e ) {

			error( response, SYSERROR ) ;
			
			return;
		}
		
		
		
		try {
			
			response.setStatus( HttpServletResponse.SC_OK ) ;
			response.setHeader( "Content-Length", "OK".getBytes().length + "" ) ;
	        response.getWriter().println("OK");
	        response.getWriter().flush();
			
		} catch ( Exception e ) {
			
			log.error( e.getMessage() );
		
		} finally {
			
			response.getWriter().close();
			
		}
		
		
	}
	

	
	private void error( HttpServletResponse response, String msg ) throws IOException {
		log.warn( msg );
		msg = "NOK" ;
        response.setContentLength( msg.getBytes().length );
        PrintWriter out = response.getWriter();
        out.println(msg);
        out.flush();	
        out.close();
	}
	
	
	private void success( HttpServletResponse response, String msg ) throws IOException {

	}
	
	
	
	/*
	 
	 		if (request.getParameter("export") != null) {

			// create also export version in "000/download" for quick test

			try {

				ThemeApp.getThemeManager().export( "xhtml", false );

			} catch (Exception e) {
				error( response, SYSERROR ) ;
				return;
			}

		}
		

		
		if (request.getParameter("exportWP") != null) {

			// create also export version in "000/download" for quick test

			try {

				ThemeApp.getThemeManager().export( "wp", false);

			} catch (Exception e) {
				log.error( "Failed to export theme",  e ) ;
				error( response, SYSERROR ) ;
				return;
			}
		}

		/*
		 * StringBuffer redirectUrl = new StringBuffer( hostname ) .append (
		 * ThemeApp.getConfig().getWorkRoot() ) .append( "/" ) .append(
		 * theme.getUid() ) .append( "/index.html" ) ;
		 */

	 
	
}
