package com.dotemplate.theme.server.web;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.App;
import com.dotemplate.core.server.DesignLogger;
import com.dotemplate.core.server.DesignSession;
import com.dotemplate.core.server.PurchaseInfo;
import com.dotemplate.core.server.PurchaseInfoHelper;
import com.dotemplate.core.server.frwk.AppException;
import com.dotemplate.core.server.util.UIDGenerator;
import com.dotemplate.core.server.web.BaseHttpServlet;
import com.dotemplate.theme.server.ThemeApp;
import com.dotemplate.theme.server.ThemeContext;
import com.dotemplate.theme.server.cms.CMS;
import com.dotemplate.theme.server.cms.wp.WordpressEngine;
import com.dotemplate.theme.server.cms.xhtml.XHTMLEngine;
import com.dotemplate.theme.shared.Theme;

@SuppressWarnings("unused")
public class TestExport extends BaseHttpServlet {

	private static final long serialVersionUID = 3765978745696149755L;

	private static Log log = LogFactory.getLog( TestExport.class );

	
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String origin = request.getRemoteHost() ;
		
		nocache( response ) ;
	
		if ( ! "localhost".equals( origin ) && ! "127.0.0.1".equals( origin ) && !ThemeApp.isDevMode()){
			response.setStatus( HttpServletResponse.SC_FORBIDDEN ) ;
			return ;
		}
		
		nocache( response );
		response.setContentType( "text/plain" );
		
		String baseUrl = getBaseUrl(request);

		ThemeApp.getConfig().setBaseUrl( baseUrl );

		String errorUrl = new StringBuffer().append(request.getScheme())
				.append("://").append(request.getServerName()).append(":")
				.append(request.getServerPort()).toString();

		
		String themeName = request.getParameter( "id" );

		if ( themeName == null ) {
			log.error("Bad request! Sorry.") ;
			forwardUnexpectedError( request, response ) ;
			return;
		}
				
		if (log.isInfoEnabled()) {
			log.info("Test export theme : " + themeName );
		}

		ensureDesignSession( request ) ;
				
		String uid = UIDGenerator.pickId();
		
		try {

			ThemeApp.getThemeManager().createTheme( themeName, uid , "dotemplate", request.getParameter( "randomize" ) != null );
						
		} catch ( Exception e ) {
			e.printStackTrace() ;
			log.error(  SYSERROR, e ) ;
			forwardUnexpectedError( request, response ) ;
			return;
		}

		String export = request.getParameter( "export" ) ;
		String packparam = request.getParameter( "pack" ) ;
		boolean packed = "true".equals( packparam ) ? true : false ;
		

		
		// export = free|preview|purchased|wp
		// pack = true|false
		
		ThemeContext themeContext = ThemeApp.getThemeContext();	

		try {
			
			if ( export == null ) {
				
				response.sendError(HttpServletResponse.SC_BAD_REQUEST ) ;
				return ;
			
			}	
			
			
			if( "free".equals( export ) ){
				
				themeContext.put( ThemeContext.WATERMARK, false ) ;
				themeContext.getDesign().setPurchased( false ) ;
			
				
				if ( !packed ){					
					
					ThemeApp.getThemeManager().export( "xhtml", false );

					response.sendRedirect("http://127.0.0.1:8888" + request.getContextPath() + "/sites/" + uid + "/download/index.html" ) ;
					
				} else {
					
					ThemeApp.getThemeManager().export( "xhtml", true );
					
					File src = new File ( ThemeApp.realPath( "download\\doTemplate-" + uid + ".zip" ) ) ;
					File dest = new File ( ThemeApp.realPath("..\\") +  "wpdt\\adt\\dtthemes\\" + themeName + "\\" + themeName + ".zip" ) ;
					
					FileUtils.copyFile(src, dest) ;
					response.sendRedirect("http://127.0.0.1:8888" + request.getContextPath() + "/download/doTemplate-" + uid + ".zip" ) ;

				}
				
				return ;
				
			}

			
			if( "purchased".equals( export ) ){
				
				themeContext.put( ThemeContext.WATERMARK, false ) ;
				themeContext.getDesign().setPurchased( true ) ;
				
				ThemeApp.getThemeManager().generateDesign() ;

				
				if ( !packed ){					
					
					ThemeApp.getThemeManager().export( "xhtml", false );
					response.sendRedirect("http://127.0.0.1:8888" + request.getContextPath() + "/sites/" + uid + "/download/index.html" ) ;
					
				} else {
					
					ThemeApp.getThemeManager().export( "xhtml", true );
					response.sendRedirect("http://127.0.0.1:8888" + request.getContextPath() + "/download/doTemplate-" + uid + ".zip" ) ;

				}
				
			}
			
			
			if( "preview".equals( export ) ){
				
				themeContext.put( ThemeContext.WATERMARK, true ) ;
				themeContext.put( "EXPORT_FOR_PREVIEW", true ) ;
				themeContext.getDesign().setPurchased( true ) ;
				ThemeApp.getThemeManager().generateDesign() ;
				ThemeApp.getThemeManager().export( "xhtml", false );
				
				// copy generated to dtthemes
				
				
				
				File src = new File ( ThemeApp.get().getExportDirectoryRealPath() ) ;
				File dest = new File ( ThemeApp.realPath("..\\") +  "wpdt\\adt\\dtthemes\\" + themeName + "\\preview" ) ;
			
				if ( dest.exists() ){
					FileUtils.cleanDirectory( dest ) ;				
				}
				
				FileUtils.copyDirectory( src, dest ) ;
				
				response.sendRedirect("http://127.0.0.1:8888" + request.getContextPath() + "/sites/" + uid + "/download/index.html" ) ;
								
			}

			
			
			if( "wp".equals( export ) ){
				
				themeContext.put( ThemeContext.WATERMARK, false ) ;
				themeContext.getDesign().setPurchased( true ) ;
				ThemeApp.getThemeManager().generateDesign() ;
				ThemeApp.getThemeManager().export( "wp", false );
				
				( ( WordpressEngine ) ThemeApp.get().getExportEngine( "wp" )).copyToWordpress() ;
				
				
				response.sendRedirect("http://localhost/wptest" ) ;
				

								
			}
			
		} catch ( Exception e ) {
			e.printStackTrace() ;
			log.error(  SYSERROR ) ;
			forwardUnexpectedError( request, response ) ;
			return;
		
		}
		

	}
	
	
	
	
	
	
}
