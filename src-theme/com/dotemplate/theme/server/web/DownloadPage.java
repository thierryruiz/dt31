package com.dotemplate.theme.server.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dotemplate.core.server.web.BaseHttpServlet;
import com.dotemplate.theme.server.ThemeApp;

public class DownloadPage extends BaseHttpServlet {
	
	private static final long serialVersionUID = -2513758550307757475L;

	@Override
	@Deprecated // maybe used later to add social buttons
	protected void doGet( HttpServletRequest request, HttpServletResponse response )
			throws ServletException, IOException {
	
		
		if ( !checkDesignSession( request, response, true ) ){
			return ;
		}
		
		
		String downloadUrl = getBaseUrl( request ) + "download/doTemplate-" 
			+ ThemeApp.getDesignUid() + ".zip" ;
		
		request.setAttribute( "zip", downloadUrl ) ;
		
		forwardTo( "download.jsp", request, response ) ;
			
	}
	
	
	
}
