package com.dotemplate.theme.server.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.DesignSession;
import com.dotemplate.core.server.web.BaseHttpServlet;
import com.dotemplate.theme.server.ThemeApp;
import com.dotemplate.theme.shared.Theme;


@SuppressWarnings("unused")
public class EditTheme extends BaseHttpServlet {

	public static String URL_MAPPING = "dt/edit" ;
	
	private static final long serialVersionUID = -3201315930278755214L;

	private static Log log = LogFactory.getLog ( EditTheme.class );
	
	protected void doGet( HttpServletRequest request, HttpServletResponse response ) 
			throws ServletException, IOException {

		if( log.isDebugEnabled () ) {
			
			log.debug ( "Design theme request..." ) ;
		
		}
		
		if (  !checkDesignSession( request, response, true ) ) {
			return ;
		}
		
		nocache( response ) ;
		
		response.setContentType( "text/html" ); 
		
		HttpSession session = request.getSession ( false ) ;

		
		String siteUrl =  ThemeApp.get().getWorkUri() + "/index.html"  ;
		
		if ( log.isInfoEnabled() ){
			
			log.info( "Forward to url " + siteUrl ) ;
		
		}
		
		request.getRequestDispatcher ( "/" + siteUrl   ).forward ( request, response ) ;
		
			
	}

}
