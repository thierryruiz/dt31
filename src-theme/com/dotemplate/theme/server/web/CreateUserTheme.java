package com.dotemplate.theme.server.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.App;
import com.dotemplate.core.server.DesignLogger;
import com.dotemplate.core.server.DesignSession;
import com.dotemplate.core.server.PurchaseInfo;
import com.dotemplate.core.server.PurchaseInfoHelper;
import com.dotemplate.core.server.frwk.AppException;
import com.dotemplate.core.server.util.UIDGenerator;
import com.dotemplate.core.server.web.BaseHttpServlet;
import com.dotemplate.theme.server.ThemeApp;
import com.dotemplate.theme.server.ThemeContext;
import com.dotemplate.theme.server.ThemeDescriptorNotFound;
import com.dotemplate.theme.server.ThemePreprocessor;
import com.dotemplate.theme.server.ThemeUtils;
import com.dotemplate.theme.shared.Theme;

@SuppressWarnings("unused")
public class CreateUserTheme extends BaseHttpServlet {

	public static String URL_MAPPING = "dt/createUserTheme" ;
	
	private static final long serialVersionUID = 3765978745696149755L;
	
	private static Log log = LogFactory.getLog ( CreateUserTheme.class );
	
	
	protected void doGet( HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		nocache( response );
		
		super.forwardTo( "create-proxy.jsp", request, response ) ;

	}
	
	
	protected void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
			
		nocache( response ) ;
		response.setContentType( "text/html" ); 
	
		String baseUrl =  getBaseUrl( request )  ;
		
		ThemeApp.getConfig().setBaseUrl( baseUrl ) ;
	
		Enumeration<String> params  = request.getParameterNames() ;
		
		if ( !params.hasMoreElements() ){
			log.error( "Missing theme uid parameter" ) ;
			error( response, SYSERROR ) ;
			return ;
		
		}
		
		String themeUid = params.nextElement()  ;

		if( log.isInfoEnabled () ){
			
			log.info ( "Creating user theme " + themeUid ) ;
		
		}
		
		Theme theme ;
		
		ensureDesignSession( request ) ;
				
		try {
						
			ThemeApp.getThemeManager ().createUserTheme ( themeUid ) ;
			
			
		}  catch ( ThemeDescriptorNotFound e ) {
			
			log.error ( e ) ;
			
			forward404( request, response ) ;
			
			return;
			
		} catch ( Exception e ) {
			
			log.error( e ) ;
			error( response, SYSERROR ) ;
			return ;
		
		}
		
		
		try {
			
			ThemeApp.getThemeManager().generateDesign() ;
			
		} catch (  Exception e ) {

			error( response, SYSERROR ) ;
			
			return;
		}
		
		
		
		// check if valid purchase template and nb of downloads
		PurchaseInfo info  = null ;
		
		try {
			
			info = PurchaseInfoHelper.load( themeUid ) ;

		} catch ( AppException e ) {
			
			log.error( e ) ;
			error( response, SYSERROR ) ;
			return ;
		}
		
		if ( info.getDownloads() < 1 ){
			error( response, "Sorry ! You have no more download remaining." ) ;
			return ;
		}
		
		
		StringBuffer redirectUrl = new StringBuffer( baseUrl )
			.append( EditTheme.URL_MAPPING ) ; 
		
				
		
		if ( log.isInfoEnabled() ){
			log.info( "Redirecting visitor to url " + redirectUrl.toString() ) ;			
		}
		
		try {
			
			response.setStatus(HttpServletResponse.SC_OK) ;
			response.setHeader( "Content-Length", "OK".getBytes().length + "" ) ;
	        response.getWriter().println("OK");
	        response.getWriter().flush();
			
		} catch ( Exception e ) {
			
			log.error( e.getMessage() );
		
		} finally {
			
			response.getWriter().close();
			
		}
		
	}

	
	
	private void error( HttpServletResponse response, String msg ) throws IOException {
		log.warn( msg );
		msg = "NOK:" + msg ;
        response.setContentLength( msg.getBytes().length );
        PrintWriter out = response.getWriter();
        out.println(msg);
        out.flush();	
        out.close();
	}
	
}
