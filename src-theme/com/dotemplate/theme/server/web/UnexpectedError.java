package com.dotemplate.theme.server.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.web.BaseHttpServlet;

public class UnexpectedError extends BaseHttpServlet {

	private static final long serialVersionUID = 7633075868303174545L;
	
	
	@SuppressWarnings("unused")
	private static Log log = LogFactory.getLog( UnexpectedError.class );

	protected void doGet( HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		nocache( response );
		
		super.forwardTo( "error.jsp", request, response ) ;
		
	}

	
}
