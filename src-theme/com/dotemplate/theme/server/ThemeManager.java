package com.dotemplate.theme.server;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.App;
import com.dotemplate.core.server.Dependencies;
import com.dotemplate.core.server.DesignManager;
import com.dotemplate.core.server.DesignSession;
import com.dotemplate.core.server.DesignUtils;
import com.dotemplate.core.server.PurchaseInfo;
import com.dotemplate.core.server.PurchaseInfoHelper;
import com.dotemplate.core.server.affiliate.Affiliate;
import com.dotemplate.core.server.frwk.AppException;
import com.dotemplate.core.server.frwk.AppRuntimeException;
import com.dotemplate.core.server.util.Path;
import com.dotemplate.core.shared.DesignUpdate;
import com.dotemplate.core.shared.Scheme;
import com.dotemplate.core.shared.properties.Property;
import com.dotemplate.core.shared.properties.PropertySet;
import com.dotemplate.theme.server.cms.CMS;
import com.dotemplate.theme.server.generator.PropertySetGenerator;
import com.dotemplate.theme.server.generator.ThemeGenerator;
import com.dotemplate.theme.server.symbol.ColorSchemeProvider;
import com.dotemplate.theme.server.symbol.SlideshowImageProvider;
import com.dotemplate.theme.server.writer.ThemeXmlWriter;
import com.dotemplate.theme.shared.Theme;
import com.dotemplate.theme.shared.ThemeSymbolType;
import com.dotemplate.theme.shared.ThemeUpdate;



public class ThemeManager extends DesignManager< Theme > {

	private final static Log log = LogFactory.getLog ( ThemeManager.class );
	
	protected ThemeGenerator themeGenerator ;
	
	protected PropertySetGenerator setGenerator ;

	
	public ThemeManager() {
		
		add ( ThemeSymbolType.NAVBAR ) ;
		add ( ThemeSymbolType.HEADER ) ;
		add ( ThemeSymbolType.HERO ) ;		
		add ( ThemeSymbolType.BANNER ) ;
		add ( ThemeSymbolType.TAGLINE ) ;
		add ( ThemeSymbolType.BLOC ) ;
		add ( ThemeSymbolType.BLOCSET ) ;
		add ( ThemeSymbolType.SIDEBOX ) ;		

		add ( ThemeSymbolType.BG ) ;	
		add ( ThemeSymbolType.FILL ) ;
		add ( ThemeSymbolType.BORDER ) ;
				
		add ( ThemeSymbolType.DOC ) ;	
		add ( ThemeSymbolType.NAVIGATION ) ;
		add ( ThemeSymbolType.MENUSET ) ;

		add ( ThemeSymbolType.COMPONENT ) ;
		
		add ( ThemeSymbolType.TOP  ) ;
		add ( ThemeSymbolType.WRAPPER  ) ;	

		add ( ThemeSymbolType.CANVAS  ) ;

		add ( ThemeSymbolType.CONTAINER  ) ;

		add ( ThemeSymbolType.MAINBLOC  ) ;		
		add ( ThemeSymbolType.HMENU  ) ;
		add ( ThemeSymbolType.VMENU  ) ;

		add ( ThemeSymbolType.SIDEBAR  ) ;	
		add ( ThemeSymbolType.TYPO  ) ;
		add ( ThemeSymbolType.FONT  ) ;
		add ( ThemeSymbolType.FOOTER ) ;
		
		add ( ThemeSymbolType.CONTENT ) ;
		add ( ThemeSymbolType.PAGE ) ;
		
		add ( ThemeSymbolType.HEADING  ) ;
		add ( ThemeSymbolType.HEADINGFONT ) ;
		
		add ( ThemeSymbolType.BOTTOM  ) ;
		add ( ThemeSymbolType.ELEMENT ) ;
		add ( ThemeSymbolType.HMENUSTYLE ) ;
		add ( ThemeSymbolType.SLIDER ) ;
		
	}
	
	
	
	@Override
	public void init() {
		
		super.init();

		themeGenerator = ( ThemeGenerator ) App.getSingleton ( ThemeGenerator.class ) ;
		setGenerator = ( PropertySetGenerator ) App.getSingleton ( PropertySetGenerator.class ) ;
		
		Backlinks.load( new File ( App.realPath( "WEB-INF/conf/backlinks.properties" ) ) )  ;
		
		
	}
	
	
	
	@Override
	protected void loadResources() {
		super.loadResources();
	/*
		( ( SlideshowImageProvider ) App.getSingleton( 
				SlideshowImageProvider.class ) ).load() ;
	*/
	}
	

	
	public CreativeCommonsImage getSlideshowImage( String key ){
		return ( CreativeCommonsImage ) ( ( SlideshowImageProvider ) 
			App.getSingleton ( SlideshowImageProvider.class )).get ( key ) ;
	}
	
	
	public void createTheme( String name, String uid, String affiliateId, boolean randomize ) throws AppException {
		
		if ( log.isInfoEnabled () ){
			log.info (  "Creating theme from descriptor '" + name +  "'..." ) ;
		}
	
		File descriptor = new File ( App.realPath(  App.getConfig ().getDesignsPath () + "/"
				+ name + ".xml" ) ) ;
		
		doCreateTheme( descriptor, name, uid, affiliateId, randomize ) ;
		
	}

	
	public void createUserTheme( String uid ) throws AppException {
		
		if ( log.isInfoEnabled () ){
			log.info (  "Creating user theme from uid '" + uid +  "'..." ) ;
		}

		PurchaseInfo purchaseInfo = PurchaseInfoHelper.load ( uid ) ;
				
		if ( purchaseInfo == null ) {
			throw new AppException( "Cannot create user theme " + uid + ". No purchase info found" ) ;
		}
		
		File descriptor = new File ( App.realPath( new Path ( App.getConfig ().getWorkRoot () ).append (
			uid ).append( "theme.xml" ).toString() ) ) ;
		
		
		doCreateTheme( descriptor, uid, uid, purchaseInfo.getAffiliateId(), false ) ;
		
		ThemeApp.getThemeContext().setPurchaseInfo( purchaseInfo ) ;
		
	}
	
	
	
	@Override
	public void generateDesign() throws AppException  {
		
		if ( log.isDebugEnabled () ){
			log.debug ( "generate user theme..." ) ;
		}
		
		ThemeContext themeContext = ThemeApp.getThemeContext() ;
		
		Theme theme = themeContext.getDesign() ;
		
		if ( log.isInfoEnabled () ){
			log.info (  "Generate theme for edition '" + theme.getUid() +  "'..." ) ;
		}
		
		
		Path outputPath = new Path ( App.realPath( ThemeApp.getConfig().getWorkRoot() ) ) ;
		
		outputPath.append( theme.getUid() ) ; 
		
		File outputDir = outputPath.asFile();
		
		if ( log.isDebugEnabled () ){
			log.debug (  "Creating theme directory structure "
					+ outputDir.getAbsolutePath() + "..." ) ;
		}
		
		
		File cssDir 	= new File( outputDir.getAbsolutePath()  + File.separator + "css" ) ;
		File imageDir 	= new File( outputDir.getAbsolutePath()  + File.separator + "images" ) ;
		File jsDir 		= new File( outputDir.getAbsolutePath()  + File.separator + "js" ) ;
		
		try {
			
			FileUtils.forceMkdir ( outputDir )   ;
			FileUtils.forceMkdir ( cssDir )   ;
			FileUtils.forceMkdir ( imageDir )   ;
			FileUtils.forceMkdir ( jsDir )   ;

			
			CMS.set( themeContext, CMS.TYPE_XHTML ) ;
			
			themeGenerator.generate () ;
			
			
		} catch ( Exception e ){
			
			log.error( e ) ;

			throw new AppException( "Unable to generate  theme" , e  ) ;
		}
		
	}	
	
	
	protected void doCreateTheme ( File descriptor, String name, String uid, String affiliateId, boolean randomize ) throws AppException {
		
		try {
			
			Theme theme =  ThemeApp.getThemeFactory().create( descriptor ) ;
			
			theme.setUid( uid ) ;
			
			theme.setName( name ) ;
			
			ThemeContextFactory themeContextFactory = ( ThemeContextFactory ) ThemeApp.getSingleton ( 
					ThemeContextFactory.class ) ;
			
			ThemeContext themeContext = themeContextFactory.create ( theme, randomize ) ;
			
			Affiliate affiliate = ThemeApp.getAffiliateManager().find( affiliateId ) ; 
			
			themeContext.setAffiliate( affiliate ) ;
			
			DesignSession session = DesignSession.get() ;
			
			session.addDesignContext ( themeContext ) ;
			
			session.setDesignContext ( themeContext ) ;
			
			
		} catch ( ThemeDescriptorNotFound e ){
			
			throw e ;
			
		} catch ( Exception e ) {
			
			e.printStackTrace() ;
			
			log.error( e ) ;
			
			throw new AppException( "Unable to create  theme from decriptor '" 
					+ descriptor.getAbsolutePath() , e  ) ;
			
		} 

	}
	
	
	public void export (  String cmsId, boolean pack  ) throws AppException {
		
		if ( log.isDebugEnabled () ){
			log.debug ( "Creating theme download archive for cms " + cmsId  ) ;
		}
		
		PurchaseInfo purchaseInfo = ThemeApp.getThemeContext().getPurchaseInfo() ;
		
			
		if( purchaseInfo != null && purchaseInfo.getDownloads() == 0 ){
				
				throw new AppException ( "No more download left" ) ;
			
		}
		
		
		ThemeContext themeContext = ThemeApp.getThemeContext();
		
		
		try {
		
			// wrapp context for download
			ThemeContext cmsThemeContext = CMS.wrappThemeContextForCMS ( themeContext, cmsId ) ;
			
			ThemeApp.setThemeContext ( cmsThemeContext ) ;
			
			// regenerate
			ThemeApp.getThemeManager().generateDesign() ;
			
			ThemeApp.get().getExportEngine ( cmsId ).export () ;
			
			if ( pack ) {
				
				ThemeApp.getThemeArchiver ( cmsId ).createArchive () ;
			
			}
			
			if( purchaseInfo != null ){
				
				purchaseInfo.setDownloads( purchaseInfo.getDownloads() - 1 ) ;
				
				PurchaseInfoHelper.save() ;
			
			}
			
			
		} catch ( Exception e ) {
			
			throw new AppException ( "Unable to create download archive" , e ) ;
		
		} finally {
			
			// unwrap theme context after export
			ThemeApp.setThemeContext ( themeContext ) ;

		}

	}
	



	//@Duration
	@Override
	public DesignUpdate< Theme > updateProperty ( Property property ) throws Exception  {
		
		if ( log.isDebugEnabled () ){
			log.debug ( "Update theme property " + property.getLongName() + ", uid=" + property.getUid () ) ;
		}
		
		ThemeContext themeContext = ThemeApp.getThemeContext() ;
		
		Theme theme = themeContext.getDesign () ;
		
		Property current = DesignUtils.findPropertyByUid( theme, property.getUid() ) ;
		
		if ( current == null ){
			throw new AppException ( "Property to update '" + property.getUid() +
					"' (" + property.getLongName() + ")  not found in theme" ) ;
		}
		
		//DesignLogger.log (  property  ) ;
		
		// apply changes
		BeanUtils.copyProperties ( current, property ) ;
		
		//DesignLogger.log (  property  ) ;
		
		themeContext.updateRandom () ;
		
		themeContext.getCssImageCache().trackChanges () ;
		
		ThemeRefresh themeRefresh = themeContext.getThemeRefresh() ;
		
		themeRefresh.clear() ;

		Dependencies dependencies = ThemeApp.getThemeContext().getDependencies() ;		

		if ( current.isSet() ) {

			PropertySet set = ( PropertySet  ) current ;

			if ( set.mutable() ){
				
				ThemeUtils.buildDependencies( ThemeApp.getThemeContext() ) ;
			
			}
			
		}
		
		//dependencies.log( current ) ;		
		
		Collection < PropertySet > d = new ArrayList< PropertySet > ( dependencies.get( current.getUid() ) ) ;
		
		setGenerator.regenerate ( d )  ;
		
		ThemeUpdate themeUpdate = themeRefresh.getDesignUpdate () ;
	
		ArrayList< PropertySet > updated = new ArrayList< PropertySet >( d ) ; 
		
		themeUpdate.setUpdatedProperties( updated ) ;
		
		
		return themeUpdate  ;
		
	}
	
	
	public String changeColorScheme( String schemeName ) throws Exception {
		
		if ( log.isDebugEnabled () ){
			log.debug ( "Change color scheme..." ) ;
		}
			
		ThemeApp.getTheme().setScheme( schemeName ) ;
		
		return themeGenerator.generateOnColorSchemeChange() ;

	}
	
		
	public void generateStyleSheet (){	
    
		if ( log.isDebugEnabled() ){
            log.debug( "Generating CSS file... " ) ;
        }
		
		themeGenerator.generateStylesheet() ;
	}

	
	
	public void generateJavascript()  {
		
		if ( log.isDebugEnabled() ){
            log.debug( "Generating javascript files... " ) ;
        }
		
		themeGenerator.generateJavascript () ;
		
		
		if ( ThemeApp.getThemeContext().isExportMode() ){
		
			File downloadJsDir = new Path( ThemeApp.get().getExportDirectoryRealPath()).append ( "js" ).asFile () ;
			File jqueryFile = new Path ( App.realPath ( ThemeApp.getThemeAppConfig().getJqueryUri() ) ).asFile () ;

			File customjsFile = new Path ( App.realPath ( "js/custom.js" ) ).asFile () ;
			
			try {

				FileUtils.copyFileToDirectory ( jqueryFile, downloadJsDir ) ;
				FileUtils.copyFileToDirectory ( customjsFile, downloadJsDir ) ;
				
			} catch ( IOException e ) {
	
				throw new AppRuntimeException ( "Failed to export theme Javascript for download", e  ) ;
				
			}			

		}
		
	}
	

	public String getOnReadyJavascript () {
		
		return themeGenerator.generateOnReadyScript() ;
	
	}

	
	public void saveTheme () throws Exception  {
		
		if ( log.isDebugEnabled () ){
			log.debug ( "Saving theme descriptor" ) ;
		}
		
		saveTheme( new File( ThemeApp.get().getWorkRealPath () + File.separator + "theme.xml" ) ) ;
	}
	
		
	public void saveTheme ( File output ) throws Exception  {
		
		if ( log.isDebugEnabled () ){
			log.debug ( "Saving theme descriptor to file " + output ) ;
		}
		
		//new ThemeDescriptorWriter().write( ThemeApp.getTheme() , new FileOutputStream( output ) ) ;
	
		StringWriter writer = new StringWriter() ;
		new ThemeXmlWriter().write( writer ) ;
		
		FileUtils.writeStringToFile(output, writer.getBuffer().toString() ) ;
		
	}

	
		
	@Override
	public void onDesignPurchased ( Theme theme, String gateway, String transactionId, String payer) throws Exception {

		super.onDesignPurchased( theme, gateway, transactionId, payer ) ;
		
    	// why ? theme.setScheme( null ) ;
    	
		saveTheme() ;
    	
	}


	public Scheme getScheme() {
		return ( ( ColorSchemeProvider ) App.getSingleton( ColorSchemeProvider.class ) ).get( 
				ThemeApp.getTheme().getScheme() ) ;		
	}


	
	
}
