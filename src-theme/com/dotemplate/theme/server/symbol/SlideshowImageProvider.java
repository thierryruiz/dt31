package com.dotemplate.theme.server.symbol;

import java.io.File;

import org.apache.commons.lang.StringUtils;

import com.dotemplate.core.server.App;
import com.dotemplate.theme.server.CreativeCommonsImage;
import com.dotemplate.theme.server.SlideshowImage;



public class SlideshowImageProvider extends CreativeCommonsImageProvider {

	@Override
	public File getResourcePropertiesFile () {
		return new File ( App.realPath ( "WEB-INF/templates/_images/content/slideshow/slideshow.properties" ) );
	}

	
	@Override
	public  CreativeCommonsImage create ( String name, String[] tokens ) {
		
		SlideshowImage image = new SlideshowImage() ;
		
		image.setName ( name ) ;
		image.setDesc ( tokens[0] ) ;
		image.setHref ( tokens[1] ) ;

		
		String ext = StringUtils.substringAfterLast ( name, "." ) ;
		String uri = StringUtils.substringBeforeLast ( name, "." ).replace ( '.', '/' ) ;
		
		image.setUri ( "slideshow/" + uri  + "." + ext ) ;
		
		image.setPicasaUrl ( tokens[ 2 ] ) ; 
		return image ;
	}

}
