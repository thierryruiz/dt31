package com.dotemplate.theme.server.symbol;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


import com.dotemplate.core.server.DesignResourceProvider;
import com.dotemplate.core.server.frwk.AppRuntimeException;
import com.dotemplate.theme.server.CreativeCommonsImage;

public abstract class CreativeCommonsImageProvider extends DesignResourceProvider< CreativeCommonsImage > {

	private final static Log log = LogFactory.getLog ( CreativeCommonsImageProvider.class );
	
	Properties resourceProperties ;

	
	public void load () {
		
		if ( log.isDebugEnabled() ){
			log.info ( "Loading Creative commons images..." ) ;
		}
		
		resourceProperties = new Properties() ;
		
		try {
			resourceProperties.load (  new FileInputStream ( getResourcePropertiesFile() ) ) ;
		} catch ( Exception e ) {
			throw new AppRuntimeException( "Unable to load Creative Commons images...", e ) ;
		} 
		
		String[] tokens = {};
		
		for ( Entry<Object, Object> entry : resourceProperties.entrySet () ){
			
			tokens = StringUtils.split ( ( String ) entry.getValue (), ';' )  ;
			
			CreativeCommonsImage image = create ( ( String ) entry.getKey (), tokens ) ;
			
			resourcesMap.get( ALL_RESOURCES ).put ( image.getName (), image ) ;
		
		}
		
	}

	
	public abstract File getResourcePropertiesFile() ;
	
	public abstract CreativeCommonsImage create ( String name, String[] tokens  ) ;	
	
	
}
