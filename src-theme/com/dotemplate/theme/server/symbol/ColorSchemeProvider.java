package com.dotemplate.theme.server.symbol;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;

import com.dotemplate.core.server.DesignResourceProvider;
import com.dotemplate.core.server.frwk.AppRuntimeException;
import com.dotemplate.core.server.util.Path;
import com.dotemplate.core.shared.Scheme;
import com.dotemplate.theme.server.ThemeApp;
import com.dotemplate.theme.server.ThemeColorSchemeFactory;


public class ColorSchemeProvider extends DesignResourceProvider< Scheme > {
		
	protected ThemeColorSchemeFactory schemeFactory ;
	
	
	@Override
	public void load() {
		
		schemeFactory = new ThemeColorSchemeFactory() ;
		
		// Color schemes are located under WEB-INF/templates/_schemes directory
		
		// load schemes
		
		
		File schemesFile = new Path ( ThemeApp.realPath( "WEB-INF/templates/_schemes/schemes.csv" ) ).asFile() ;
		
		List< String > lines ;
		
		
		LinkedHashMap< String,  Scheme > schemes ;
		
		schemes = new LinkedHashMap< String,  Scheme >() ;
	
		
		try {
		
			lines = ( List< String > ) FileUtils.readLines( schemesFile ) ;
		
		} catch ( Exception e ) {
			throw new AppRuntimeException ( "Failed to load Schemes", e ) ;
		}
				
		Scheme scheme ; String name ;
		
		lines.remove( 0 ) ; // ignore first line
		int i = 0 ;
		for ( String line : lines  ) {
			
			name = StringUtils.substringBefore( line, "." ) ;
			
			scheme = schemeFactory.createScheme( name, StringUtils.substringAfter( line, ";" ) ) ;
			schemes.put( scheme.getName(), scheme ) ;
			scheme.setFree( i < 3 ) ; i++ ;
			
			
			computeThumbnailHtml( scheme ) ;
			
			resourcesMap.get( ALL_RESOURCES ).put( scheme.getName() , scheme ) ;
		
		}

	}
	
	
	
	
	private void computeThumbnailHtml ( Scheme scheme ){
		
        String[] colors = scheme.getColors() ;
        
        StringBuffer html = new StringBuffer ( "<div class='ez-scheme' title='" + scheme.getName () + "'>" )  ;
        
        
        for ( int i = 0 ; i < 4 /*i < colors.length */ ; i++  ){
        	
        	
            if ( i == 10 ) {
            	break ;
            }
        	
            html.append("<div title =\"" + colors[ i ] + 
            		"\" class=\"ez-scheme-color-panel\" style=\"background-color:" + colors[ i ]+ ";\"></div>" ) ;
        }
        
        html.append( "</div>" ) ;
        
        scheme.setThumbnailHtml( html.toString() ) ;
	
	}
	
	
	
	
	@Override
	public boolean isFree( String name ) {
		return true;
	}
	
	
}
