package com.dotemplate.theme.server.symbol;

import com.dotemplate.core.server.symbol.SymbolProvider;
import com.dotemplate.core.shared.properties.PropertySet;
import com.dotemplate.theme.server.cms.CMS;



public abstract class ThemeSymbolProvider extends SymbolProvider {
	
	
	protected void postCreateSymbol ( PropertySet symbol ){
		CMS.preloadSymbol ( symbol ) ;
	}

	
	/*
	public PropertySet createSymbolFromDescriptor ( File xmlFile ){
		
		PropertySet symbol ;
		
		SymbolReader symbolReader = new SymbolReader() ;
		
		
		if ( ! xmlFile.exists () ){
			throw new AppRuntimeException ( "Cannot create symbol  from file " + xmlFile.getAbsolutePath () ) ;
		}
		
		try {
			
			symbol = symbolReader.read ( xmlFile ) ;
			
		} catch ( Exception e ) {
			throw new AppRuntimeException( "Unable to load symbol from " +  xmlFile.getAbsolutePath(), e ) ;
		}
		
		if ( symbol == null ){
			throw new AppRuntimeException( "Null symbol loaded from " +  xmlFile.getAbsolutePath() ) ;
		}
		
		String name = getType().getFolder () + "-" + xmlFile.getParentFile ().getName () ;
		
		symbol.setName ( name ) ;
		symbol.setPath( "_symbols/" +  symbol.getName().replaceFirst ( "-", "/" ) ) ;
		symbol.setSymbolType( getType() ) ;

		
		CMS.preloadSymbol ( symbol ) ;
		
	
		
		try {
			
			DesignUtils.deepBuild( symbol ) ;
		
		} catch ( Exception e ) {
			
			throw new AppRuntimeException( "Failed to build " + symbol.getName() , e ) ;
		
		}
		
		prepare( symbol ) ;

		computeThumbnails( symbol ) ;
		computePreviewHtml( symbol ) ;
		
		// resourceMap.put (  symbol.getName (), symbol ) ;
		
		String[] tags = DesignUtils.getTags( symbol ) ;
		
		if ( tags != null ){
		
			
			LinkedHashMap< String, PropertySet> tagSymbols  ;
			
			for ( String tag : tags ){
				
				tagSymbols =  resourcesMap.get( tag ) ;
				
				if ( tagSymbols == null ) {
					
					tagSymbols = new LinkedHashMap< String, PropertySet>() ;
					
					resourcesMap.put( tag, tagSymbols ) ;
				
				}
				

				tagSymbols.put (  symbol.getName (), symbol ) ;
			
			}
			
		}
		
		
		resourcesMap.get( ALL_RESOURCES ).put (  symbol.getName (), symbol ) ; 
		
		
		if ( log.isDebugEnabled () ){
			log.debug (  "Symbol " + name + "loaded." ) ;
		}
		
		return symbol ;
	
	}
*/
	

	

}
