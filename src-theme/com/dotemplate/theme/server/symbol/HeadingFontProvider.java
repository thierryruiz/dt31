package com.dotemplate.theme.server.symbol;

import com.dotemplate.core.server.App;
import com.dotemplate.core.shared.SymbolType;
import com.dotemplate.core.shared.properties.PropertySet;
import com.dotemplate.theme.shared.ThemeSymbolType;



public class HeadingFontProvider extends ThemeSymbolProvider {
	
	
	private final static String abcd = "   AaBbCcDdEeFfGg" ;
	
	
	@Override
	protected SymbolType getType () {
		return ThemeSymbolType.HEADINGFONT ;
	}

	
	protected void computeThumbnails ( PropertySet font ){
		
		StringBuffer sb = new StringBuffer() ;		
		String title = ( String ) font.get (  "title" ) ;

		String ftype = ( String ) font.get( "ftype" ) ;
		
		if ( "google".equals( ftype ) ){
			
			String imageUrl = getThumbnailUrlPath( font ).asUrl () ;
			sb.append( "<div class=\"ez-headingfont\">" )		
			.append( "<img src=\"").append( imageUrl ).append ( "\"" ).append( 
					App.isDevMode() ?  " title=\"" + font.getName() + "\"" : "" ).append( "/></div>"  );
			
			font.setThumbnailHtml( sb.toString () ) ;
			font.setThumbnailUrl( imageUrl) ;		
			
		
		} else {
		
			sb.append("<div class=\"ez-headingfont\" style=\"" ) 
				.append( "font-family:" +	font.get( "family" ) + ";" ) 
				.append( "font-style:"  + 	font.get ( "style" )  + ";")	
				.append( "font-weight:" +	font.get ( "weight" ) + ";" )				
				.append( "line-height:" + 	font.get ("lineHeight" ) + ";" )
				.append( "text-transform:" + 	font.get ("textTransform" ) + ";" )				
				.append( "font-size:24px;color:#55555;padding:10px;\">" )
				.append( title )
				.append( abcd ).append("</div>") ;
		
				font.setThumbnailHtml( sb.toString() ) ;
		}
		
	}
	
	
	
	
}
