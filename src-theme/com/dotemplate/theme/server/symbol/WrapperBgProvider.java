package com.dotemplate.theme.server.symbol;

import com.dotemplate.core.shared.SymbolType;
import com.dotemplate.theme.shared.ThemeSymbolType;


public class WrapperBgProvider extends ThemeSymbolProvider {


	protected SymbolType getType () {
		return ThemeSymbolType.WRAPPERBG;
	}
		

	/*
	protected void computePreviewHtml ( ThemePropertySet symbol ) {
		String css = "images/symbols/" + symbol.getSymbolType ().getFolder () + "/" + symbol.getName() + "." + symbol.getThumbnailImageFormat () ;
		symbol.setPreviewHtml ( "<div style='width:400px;height:400px;background:url(" + css + ") repeat'></div>" ) ;
	}
	
	
	protected void computeThumbnailHtml ( ThemePropertySet symbol ){
		String css = "images/symbols/" + symbol.getSymbolType ().getFolder () + "/" + symbol.getName() + "." + symbol.getThumbnailImageFormat () ;
		symbol.setThumbnailHtml ( "<div style='width:80px;height:80px;background:url(" + css + ") repeat'></div>" ) ;
	}
	*/
	
	
}
