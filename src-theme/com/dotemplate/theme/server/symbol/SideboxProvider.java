package com.dotemplate.theme.server.symbol;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;

import com.dotemplate.core.shared.SymbolType;
import com.dotemplate.core.shared.properties.PropertySet;
import com.dotemplate.theme.shared.ThemeSymbolType;

public class SideboxProvider extends ThemeSymbolProvider {

	@Override
	protected SymbolType getType () {
		
		return ThemeSymbolType.SIDEBOX ;
	
	}
	
	
	protected void computeThumbnails ( PropertySet symbol ){
		String imageUrl = getThumbnailUrlPath( symbol ).asUrl () ;
		StringBuffer sb = new StringBuffer( "<img height='150px' src=\"").append( imageUrl ).append ( "\"/>"  ) ;
		symbol.setThumbnailHtml( sb.toString () ) ;
		symbol.setThumbnailUrl( imageUrl) ;		
	}

	
	
	protected void createThumbnail( PropertySet symbol ) throws Exception {
		
		String name = StringUtils.substringAfter( symbol.getName(), "-" ) ; 
		
		File srcFile = new File( "WEB-INF/templates/_symbols/sidebox/" + name + "/th.png" ) ;
		File destFile = new File( "images/symbols/sidebox/" + name + ".png" ) ;
				
		FileUtils.copyFile( srcFile, destFile ) ;
		
	}
	
	
	
	
}
