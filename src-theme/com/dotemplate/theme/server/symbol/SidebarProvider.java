package com.dotemplate.theme.server.symbol;

import com.dotemplate.core.shared.SymbolType;
import com.dotemplate.theme.shared.ThemeSymbolType;

public class SidebarProvider extends ThemeSymbolProvider {

	@Override
	protected SymbolType getType () {
		return ThemeSymbolType.SIDEBAR;
	}

}
