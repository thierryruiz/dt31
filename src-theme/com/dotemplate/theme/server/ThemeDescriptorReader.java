package com.dotemplate.theme.server;

import com.dotemplate.core.server.DesignDescriptorReader;
import com.dotemplate.theme.shared.Theme;

public class ThemeDescriptorReader extends DesignDescriptorReader< Theme > {
	
	@Override
	protected String getBetwitxtDescriptorName() {
		return "theme.betwixt" ;
	}
	
}
