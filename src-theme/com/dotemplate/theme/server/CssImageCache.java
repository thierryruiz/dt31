package com.dotemplate.theme.server;

import java.io.File;
import java.io.FileOutputStream;

import org.apache.commons.io.FileUtils;

import com.dotemplate.core.server.CachedImage.Format;
import com.dotemplate.core.server.ImageCache;
import com.dotemplate.core.server.svg.JPEGTranscoder;
import com.dotemplate.core.server.svg.PNGTranscoder;


public class CssImageCache extends ImageCache< CssImage > {

	private static final long serialVersionUID = 2457658480181936612L;

	
	public void export( File outputDir ) throws Exception {
		
		if ( outputDir.exists() ){
		
			FileUtils.cleanDirectory( outputDir ) ;
		
		} else {
			
			outputDir.mkdirs() ;
		
		}
		
		for ( CssImage image : values() ){
			
			/*
			ImageIO.write( image.getImage(), ( Format.PNG == image.getFormat() ) ? "png": "jpg" , new File ( 
					outputDir + File.separator + image.getId() ) );
			*/
			
			if (  Format.PNG == image.getFormat()  ){

				PNGTranscoder.writeImage( image.getImage(), new FileOutputStream( 
						 new File ( outputDir + File.separator + image.getId() ) ) )   ;
			
			} else {

				JPEGTranscoder.writeImage( image.getImage(), 0.95f, new FileOutputStream( 
						 new File ( outputDir + File.separator + image.getId() ) ) )   ;

			}
		
		}
	}
	
}
