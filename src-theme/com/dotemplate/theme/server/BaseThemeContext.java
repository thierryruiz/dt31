package com.dotemplate.theme.server;


import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.velocity.VelocityContext;

import com.dotemplate.core.server.CanvasImageCache;
import com.dotemplate.core.server.Dependencies;
import com.dotemplate.core.server.PurchaseInfo;
import com.dotemplate.core.server.affiliate.Affiliate;
import com.dotemplate.theme.shared.Theme;
import com.dotemplate.theme.server.css.Stylesheet;


public class BaseThemeContext extends VelocityContext implements ThemeContext {

	protected ThemePreprocessor preprocessor ;
	
	protected BindingsMap bindingsMap = new BindingsMap() ;
	
	protected ThemeRefresh themeRefresh ;
	
	protected Stylesheet stylesheet = new Stylesheet() ;

	protected HashMap< String, String > scripts = new HashMap< String, String > ();
	
	protected HashMap< String, String > rowScripts = new HashMap< String, String > ();
	
	protected HashMap< String, String > polyfills = new HashMap< String, String > ();

	protected  LinkedHashMap< String, GoogleFont > googleFonts = new  LinkedHashMap< String, GoogleFont >();
	
	protected CanvasImageCache canvasImageCache = new CanvasImageCache() ;
		
	protected CssImageCache cssImageCache = new CssImageCache() ;
	
	private Dependencies dependencies = new Dependencies() ;
	
	private PurchaseInfo purchaseInfo ;
	
	private Affiliate affiliate ;

	
	public void preprocess() {
		
		if ( preprocessor == null ){
			preprocessor = new ThemePreprocessor( this ) ;  
		}
		
		preprocessor.execute () ;
	
	}
	
	
	public void updateRandom (){
		put ( RANDOM ,  "?" +  RandomStringUtils.randomAlphanumeric ( 4 ) ) ;
	}
	

	public  Theme getDesign(){
    	return ( Theme) get ( THEME ) ;
    }
    	
	
	public synchronized ThemeRefresh getThemeRefresh () {
		
		if ( themeRefresh == null ) {
			themeRefresh = new ThemeRefresh() ;
		}
		
		return themeRefresh;
	}


	public BindingsMap getBindingsMap () {
		return bindingsMap;
	}


	public void addScript ( String url ) {
		if ( scripts.containsKey( url ) ) {
			return ;
		}
		
		scripts.put( url ,url ) ;
	}

	
	public Collection< String > getScripts() {
		return scripts.values() ;
	}
		
	
	public void addRowScript ( String id, String script ){		
		if( rowScripts.containsKey( id ) ) {
			return ;
		}
		
		rowScripts.put( id , script.trim() ) ;		
	}
	
	
	public Collection< String > getRowScripts() {
		return rowScripts.values() ;
	}


	public void clearRowScripts () {			
		rowScripts.clear() ;
	}

	
	@Override
	public boolean isExportMode () {
		Boolean b = ( Boolean ) get( EXPORT ) ;
		return b != null && b.booleanValue();
	}


	@Override
	public CanvasImageCache getCanvasImageCache() {
		return canvasImageCache ;
	}
	
	
	@Override
	public CssImageCache getCssImageCache() {
		return cssImageCache;
	}

	@Override
	public void setStylesheet( Stylesheet stylesheet ) {
		this.stylesheet = stylesheet;
	}
	
	
	@Override
	public Stylesheet getStylesheet() {
		return this.stylesheet ;
	}

	@Override
	public Dependencies getDependencies() {
		return dependencies;
	}

	@Override
	public PurchaseInfo getPurchaseInfo() {
		return purchaseInfo;
	}

	@Override
	public void setPurchaseInfo( PurchaseInfo purchaseInfo ) {
		this.purchaseInfo = purchaseInfo;
	}

	@Override
	public LinkedHashMap< String, GoogleFont > getGoogleFonts() {
		return googleFonts ;
	}
	
	public void setAffiliate(Affiliate affiliate) {
		this.affiliate = affiliate;
	}
	
	@Override
	public Affiliate getAffiliate() {
		return this.affiliate ;
	}


	@Override
	public Collection<String> getPolyfills() {
		return polyfills.values() ;
	}


	@Override
	public void addPolyfill(String url) {
		if( polyfills.containsKey( url ) ) {
			return ;
		}
		
		polyfills.put( url , url ) ;		
		
	}




	
	
	
}
