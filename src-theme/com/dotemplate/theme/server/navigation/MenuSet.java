package com.dotemplate.theme.server.navigation;

import java.util.ArrayList;
import java.util.Collection;

public class MenuSet implements HasItems {
	
	protected ArrayList< MenuItem > items ;
	
	@Override
	public Collection< MenuItem > getItems() {
		return items;
	}
	
	@Override
	public void addItem( MenuItem item ) {
		if( items == null ){
			items = new ArrayList< MenuItem >() ;			
		}
		items.add( item ) ;
	}
}
