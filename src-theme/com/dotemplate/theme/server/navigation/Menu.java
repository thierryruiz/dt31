package com.dotemplate.theme.server.navigation;

import java.util.ArrayList;
import java.util.Collection;

public class Menu extends MenuItem implements HasItems {

	protected ArrayList< MenuItem > items ;
	
	protected Menu() {
		items = new ArrayList< MenuItem >() ;
	}
	
	@Override
	public Collection< MenuItem > getItems() {
		return items;
	}
	
	@Override
	public void addItem( MenuItem item ) {
		items.add( item ) ;
	}
	
	
}
