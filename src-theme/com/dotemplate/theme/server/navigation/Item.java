package com.dotemplate.theme.server.navigation;

import com.dotemplate.core.shared.properties.PropertySet;

public class Item extends MenuItem {
	
	private String file ;
	
	public Item( PropertySet node ) {
		setLabel( ( String ) node.get( "label" ) ) ;
		file =  ( String ) node.get( "file" ) ;
	}
	
	public String getFile() {
		return file;
	}
		
}
