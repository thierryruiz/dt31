package com.dotemplate.theme.server.writer;

import java.io.StringWriter;

import com.dotemplate.core.server.DesignSession;
import com.dotemplate.core.server.frwk.AppRuntimeException;
import com.dotemplate.core.server.util.UIDGenerator;
import com.dotemplate.core.server.writer.Attribute;
import com.dotemplate.core.server.writer.PropertyDiff;
import com.dotemplate.core.server.writer.PropertySetComparator;
import com.dotemplate.core.server.writer.PropertySetDiff;
import com.dotemplate.core.shared.canvas.Graphic;
import com.dotemplate.core.shared.properties.Property;
import com.dotemplate.core.shared.properties.PropertySet;
import com.dotemplate.theme.server.ThemeApp;
import com.dotemplate.theme.server.ThemeContext;
import com.dotemplate.theme.server.ThemeContextFactory;
import com.dotemplate.theme.server.ThemeUtils;
import com.dotemplate.theme.server.generator.ThemeGenerator;
import com.dotemplate.theme.shared.Theme;


public class ThemeXmlWriter extends PropertySetComparator {
		
	public void write( StringWriter writer ) {
		
		Theme theme = ThemeApp.getTheme() ;
		
		writer.write( "<?xml version='1.0' ?>" ) ;
		writer.write( "\n<theme " ) ;

		writer.write( "extends=\"" ) ;
		writer.write( theme.getParent() ) ;
		writer.write( "\"" ) ;
		
		writer.write( " premium=\"" ) ;
		writer.write( "" + theme.isPremium() ) ;
		writer.write( "\"" ) ;
		
		writer.write( " purchased=\"" ) ;
		writer.write( "" + theme.isPurchased() ) ;
		writer.write( "\"" ) ;

		writer.write( " content=\"" ) ;
		writer.write( "" + theme.getContent() ) ;
		writer.write( "\"" ) ;

		
		writer.write( " scheme=\"" ) ;
		writer.write( "" + theme.getScheme() ) ;
		writer.write( "\">" ) ;
		
		PropertySet modelSet ; 
		
		PropertySetDiff setDiff ;
				
		Theme modelTheme = ThemeUtils.getModel( theme ) ;
	
		applyColorSchemeToModel( modelTheme ) ;
		
		for ( PropertySet themeSet : theme.getPropertySets() ){
			
			modelSet = modelTheme.getPropertySet( themeSet.getName() ) ;
			
			setDiff = compare( themeSet, modelSet ) ;
			
			write( setDiff, writer, "\t" ) ;
			
		}
		
		writer.write("\n</theme>\n" ) ;
		
	}

	
	private void write( PropertySetDiff setDiff, StringWriter writer, String indent ) {
		
		if ( setDiff.isEmpty() ){
			return ;
		}
		
		writer.write( "\n" );
		writer.write( indent );
		
		String tag = "set" ;
		
		switch ( setDiff.getType() ){
			case ( Property.SET ):
				tag = "set" ;
				break ;
			case ( Property.CANVAS ):
				tag = "canvas" ;
				break ;
			case ( Graphic.TEXT ):
				tag = "text" ;
				break ;
			case ( Graphic.BG ):
				tag = "background" ;
				break ;	
	
			default:
				return ;
		}

		
		writer.write( "<" );
		writer.write( tag );
		writeAttributes( setDiff, writer ) ;
		
		if ( setDiff.getProperties().isEmpty() ){
			
			writer.write( " />" );	
		
		} else {
			
			writer.write( ">" );
		
			for ( PropertyDiff diff : setDiff.getProperties() ){
				
				if ( diff.isSet() ){
					write ( ( PropertySetDiff ) diff, writer, indent + "\t" ) ;
				} else {
					write( diff, writer, indent + "\t" ) ;
				}
				
			}
			
			writer.write( "\n" );
			writer.write( indent + "</" );
			writer.write( tag );
			writer.write( ">") ;
		
		}
	}
	
	
	
	private void write( PropertyDiff diff, StringWriter writer, String indent ) {
		
		if ( diff.isEmpty() ){
			return ;
		}
		
		String tag = "" ;
		
		switch ( diff.getType() ){
		
			case ( Property.STRING ):
				tag = "string" ;
				break ;
			
			case ( Property.BOOLEAN ):
				tag = "boolean" ;
				break ;
			
			case ( Property.SIZE ):
				tag = "size" ;
				break ;

			case ( Property.COLOR ):
				tag = "color" ;
				break ;				
				
			case ( Property.PERCENT ):
				tag = "percent" ;
				break ;
			
			case ( Property.REF ):
				tag = "ref" ;
				break ;		
			
			case ( PropertySet.CANVAS ):
				tag = "canvas" ;
				break ;	
				
			case ( Graphic.IMAGE ):
				tag = "image" ;
				break ;
				
			case ( Graphic.BG ):
				tag = "background" ;
				break ;	

			case ( Graphic.TEXT ):
				tag = "text" ;
				break ;	
				
			default:
				break;
		}
		
		writer.write( "\n"+ indent );
		writer.write( "<" );
		writer.write( tag );
		writeAttributes( diff, writer ) ;
		writer.write( " />" );
			
	}
	
	
	
	private void writeAttributes( PropertyDiff diff, StringWriter writer ) {
		
		StringBuffer sb = new StringBuffer() ;
		
		sb.append( " name=\"" )
			.append( diff.getName() )
			.append( "\"" ) ;
		
	
		for ( Attribute attribute : diff.getAttributes() ){
			sb.append( " " )
				.append( attribute.getName() )
				.append( "=\"" )
				.append( attribute.getValue() )
				.append( "\"" ) ;
		}
		
		writer.write( sb.toString() ) ;
		
	}

	
	protected void applyColorSchemeToModel( Theme model ) throws AppRuntimeException {
		
		// need to apply the same color scheme on parent model to output only changed colors
		
		Theme theme = ThemeApp.getTheme() ;
		
		model.setScheme( theme.getScheme() ) ;
		
		ThemeContextFactory themeContextFactory = ( ThemeContextFactory ) ThemeApp.getSingleton ( 
				ThemeContextFactory.class ) ;
		
		
		ThemeContext currentThemeContext = ThemeApp.getThemeContext() ;
		ThemeContext parentThemeContext = themeContextFactory.create ( model  ) ;
		
		model.setUid( UIDGenerator.pickId() ) ;
	
		
		DesignSession.get().setDesignContext( parentThemeContext ) ;
		
		
		model.setParent( theme.getParent() ) ;
		model.setScheme( theme.getScheme() ) ;

		try {
		
			ThemeGenerator themeGenerator = ( ThemeGenerator ) ThemeApp.getSingleton ( ThemeGenerator.class ) ;
			
			themeGenerator.applyColorScheme() ;		
			
		} finally {
			
			DesignSession.get().setDesignContext( currentThemeContext ) ;
		
		}
		
		
	}

	
	

}
