package com.dotemplate.theme.server;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import com.dotemplate.core.shared.properties.PropertySet;


/**
 * 
 * This class represents the dependency of a set with 
 * all the others.
 * 
 * @author Thierry Ruiz
 *
 */
public class Bindings {

	@SuppressWarnings("unused")
	private PropertySet set ;
	
	private Map<String , PropertySet> bindings = new LinkedHashMap<String, PropertySet> () ;
	
	
	protected Bindings ( PropertySet set ){
		this.set = set ;
	}
	
	
	public void add ( PropertySet d ){
		if ( bindings.containsKey ( d.getUid () ) ) return ;
		bindings.put ( d.getUid (), d ) ;		
	}
	
	
	public Iterator<PropertySet> get(){
		return bindings.values ().iterator () ;
	}
	
	
	public Iterator<PropertySet> iterator(){
		return bindings.values ().iterator () ;
	}
	
	
	
}
