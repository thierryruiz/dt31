package com.dotemplate.theme.server;

import java.io.OutputStream;

import org.apache.commons.betwixt.io.BeanWriter;

import com.dotemplate.core.server.DesignBeanWriter;
import com.dotemplate.core.server.DesignDescriptorWriter;
import com.dotemplate.theme.shared.Theme;


public class ThemeDescriptorWriter extends DesignDescriptorWriter< Theme > {

	@Override
	protected BeanWriter getBeanWriter( OutputStream os ) {
		return new DesignBeanWriter( os, "theme.betwixt" ) ; 	
	}
	
}
