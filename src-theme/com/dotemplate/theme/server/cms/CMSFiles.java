package com.dotemplate.theme.server.cms;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


import com.dotemplate.core.server.frwk.AppRuntimeException;
import com.dotemplate.core.server.util.Path;
import com.dotemplate.theme.server.ThemeApp;

@Deprecated
public class CMSFiles {
	
	private static Log log = LogFactory.getLog( CMSFiles.class ) ;
	
	
	private Map<String, CMSFile> files = new HashMap<String, CMSFile>() ;
	
	
	public void addFile ( String fileName, String destDir ){
		CMSFile file = new CMSFile() ;
		file.fileName = fileName ;
		file.destDir = destDir ;
		
		files.put ( fileName , file ) ;
	
	}
	
	
	public void addContent ( String fileName, String contentId, String content ){
		
		try {
			files.get (  fileName ).contentMap.put (  contentId, content ) ;
		} catch ( Exception e ){
			throw new AppRuntimeException ( "Unable to add " + contentId + " to " + fileName, e ) ;
		}
	}
	
	
	
	public void export() {
		
		Path path ;
		
		for ( CMSFile file : files.values () ){
			
			path = new Path ( ThemeApp.get().getExportDirectoryRealPath () ) ;
			
			if ( file.destDir != null ){
				path.append( file.destDir );
 			}

			path.append( file.fileName );
			
			if ( log.isDebugEnabled () ){
				log.debug ( "Creating file "  + path.toString () ) ;
			}
			
			try {
				FileUtils.writeStringToFile ( path.asFile (), file.toString () ) ;
			} catch ( IOException e ) {
				throw new AppRuntimeException ( "Failed to export CMS files ", e );
			}
			
		}
	}
	


	private class CMSFile {
		
		private String fileName ;
		
		private String destDir ;
		
		private Map<String, String> contentMap = new LinkedHashMap<String,String>() ;
		
		@Override
		public String toString () {
			StringBuffer sb = new StringBuffer() ;
			
			for ( String content : contentMap.values () ){
				sb.append( content ) ;
			}
			
			return sb.toString ();
		}
		
	}
}