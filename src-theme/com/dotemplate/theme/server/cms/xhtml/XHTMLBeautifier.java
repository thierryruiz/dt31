package com.dotemplate.theme.server.cms.xhtml;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.io.Writer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.io.HTMLWriter;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.dotemplate.core.server.frwk.AppRuntimeException;
import com.dotemplate.core.server.util.Path;
import com.dotemplate.theme.server.ThemeApp;



public class XHTMLBeautifier {
	
	private static Log log = LogFactory.getLog( XHTMLBeautifier.class ) ;
	
	private  XHTMLEntityResolver entityResolver = new XHTMLEntityResolver() ;
	
	
	void writePretty ( StringBuffer xhtml, Path exportPath ) throws Exception {
        
    	// Use SAX and dom4j to create a well formated XHTML with indentation.
    	
    	final OutputFormat format = OutputFormat.createPrettyPrint ();
    	
    	
    	FileWriter fileWriter = null ;
    	XHTMLWriter writer = null ;
    	
        format.setXHTML ( true  ) ;
        format.setTrimText( true );
        format.setIndent( true );
        format.setPadText ( true ) ;
        format.setEncoding ( "UTF-8" ) ;
		format.setSuppressDeclaration ( true ) ;
		format.setExpandEmptyElements ( true ) ;
			
    	try {
    		
    		SAXReader reader = new SAXReader();
    		// read DTDs locally
    		reader.setEntityResolver ( entityResolver ) ;
    		reader.setValidation ( false ) ;
    		
    		Document document= reader.read( new StringReader( xhtml.toString () ) );
			
    		
			writer = new XHTMLWriter( fileWriter = new FileWriter( exportPath.toString() ) , format ) ;
			
			//writer.setEscapeText ( true ) ;
	        writer.setResolveEntityRefs ( false ) ;
		
			if ( log.isDebugEnabled () ){
				log.debug ( "Writing to " +  ThemeApp.get().getWorkRealPath() ) ;
			}
	        
			writer.write( document );

			
    	} catch ( Exception e ) {
    		log.error ( xhtml.toString (), e ) ;
    		System.out.println( xhtml.toString () ) ;
    		throw e ;
    	
    	} finally {
    		
    		try {
				fileWriter.flush () ;
				fileWriter.close () ;
				writer.close() ;
			} catch ( Exception e ) {
				log.error ( e ) ;
			}	
		}
    	
    }
	
	
    // Need a modified writer as the SAX parser used by dom4j insert a "xml:space" 
    // attribute on each node, which has to be removed.
    private class XHTMLWriter extends HTMLWriter {
    	
		private final static String XML_SPACE = "xml:space" ;
    	
    	public XHTMLWriter ( Writer writer, OutputFormat format ) {
			super ( writer, format );
    	}
    	
    	@Override
    	protected void writeAttributes ( org.dom4j.Element element ) throws IOException {
    		
    		for ( int i = 0, size = element.attributeCount(); i < size; i++ ) {
    			
                Attribute attribute = element.attribute( i );
                
                if ( XML_SPACE.equals ( attribute.getQualifiedName () ) ){
                	// do not write the xml:space attribute
                	continue ;
                }
                
                if( "shape".equals ( attribute.getQualifiedName () ) ){
                	// do not write the shape attribute
                	continue ;
                }
                
                

                writer.write(" ");
                writer.write(attribute.getQualifiedName());
                writer.write("=\"");
                writeEscapeAttributeEntities (attribute.getValue());
                writer.write("\"");
            }
    	}
    }
    
    
    //
    // Used by SAX parser:
    // resolves the DTD localy for better performances and because its no longer
    // available from w3c.org
    private class XHTMLEntityResolver implements EntityResolver {
    	    	
		public InputSource resolveEntity ( String publicId, String systemId ) throws SAXException,
								IOException {
			
			if ( publicId.indexOf ( "Transitional" ) > 1 )
				return new InputSource( new FileReader( ThemeApp.realPath ( 
						"WEB-INF/entities/xhtml1-transitional.dtd" ) ) ); ; 
			
			if ( publicId.indexOf ( "Latin" ) > 1 )
				return new InputSource( new FileReader( ThemeApp.realPath ( 
						"WEB-INF/entities/xhtml-lat1.ent" ) ) );
			
			if ( publicId.indexOf ( "Symbols" ) > 1 )
				return new InputSource( new FileReader( ThemeApp.realPath ( 
						"WEB-INF/entities/xhtml-symbol.ent" ) ) );
			
			
			if ( publicId.indexOf ( "Special" ) > 1 )
				return new InputSource( new FileReader( ThemeApp.realPath ( 
						"WEB-INF/entities/xhtml-special.ent" ) ) ); ;
			
			throw new AppRuntimeException ( "No entities matching " + publicId + 
					" in local repository WEB-INF/entities" ) ;
			
		}
    
    }
	
}
