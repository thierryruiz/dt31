package com.dotemplate.theme.server.cms.xhtml;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.context.Context;

import com.dotemplate.core.server.DesignUtils;
import com.dotemplate.core.server.RenderEngine;
import com.dotemplate.core.server.SetHierarchy;
import com.dotemplate.core.server.frwk.AppException;
import com.dotemplate.core.server.frwk.AppRuntimeException;
import com.dotemplate.core.server.util.Path;
import com.dotemplate.core.shared.properties.Property;
import com.dotemplate.core.shared.properties.PropertySet;
import com.dotemplate.theme.server.ThemeApp;
import com.dotemplate.theme.server.ThemeContext;
import com.dotemplate.theme.server.ThemeUtils;
import com.dotemplate.theme.server.cms.CMS;
import com.dotemplate.theme.server.cms.ExportEngine;


public class XHTMLEngine implements ExportEngine {

	private static Log log = LogFactory.getLog( XHTMLEngine.class ) ;
	
	protected VelocityEngine engine ;
	
	protected XHTMLBeautifier beautifier ; 

	protected XHTMLEngine() {
		setup() ;	
	}
	
	protected void setup () {
		
		if ( log.isDebugEnabled() ){
			log.debug( "Initializing XHTMLEngine" ) ;
		}
		
		Properties props = new Properties ();

		try {
			props.load ( new FileInputStream ( ThemeApp.realPath ( ThemeApp.getConfig().getRenderEngineConf() ) ) );
		} catch ( Exception e ) {
			log.fatal ( "View engine initialization failed ", e );
			return;
		}

		String vmPath = props.getProperty ( VelocityEngine.FILE_RESOURCE_LOADER_PATH );
		String[] paths = StringUtils.split ( vmPath, "," );

		StringBuffer absPath = new StringBuffer ();

		for ( int i = 0; i < paths.length; i++ ) {
			absPath.append ( ThemeApp.realPath ( paths[i] ) );
			if ( i < paths.length - 1 ) {
				absPath.append ( ", " );
			}
		}

		props.setProperty ( VelocityEngine.FILE_RESOURCE_LOADER_PATH, absPath.toString () );
		
		engine = new InnerEngine() ;
		
		try {
			
			engine.init ( props );
		
		} catch ( Exception e ) {
			
			log.fatal ( "View engine initialization failed.", e );
			return;
		
		}
	}

	
	
	protected void clearExportFolder() {
		
		File exportDir = new Path( ThemeApp.get().getExportDirectoryRealPath() ).asFile() ;
		
		if ( exportDir.exists() ){
			try {
				FileUtils.cleanDirectory( exportDir ) ;
			} catch ( Exception e ) {
				throw new AppRuntimeException( "Failed to export template ", e ) ;
			}
		}
	}

	
	protected  void exportStylesheet() {
		
		ThemeApp.getThemeManager().generateStyleSheet () ;
	
	}
	
	
	protected void exportJavascript() {
	
		ThemeApp.getThemeManager ().generateJavascript () ;
	
	}
	

	protected void exportCSSImages()  {
		
		Path exportPath = new Path ( 
				ThemeApp.get().getExportDirectoryRealPath() ).append( "images").append( "css" ) ;
		
		try {
			
			ThemeApp.getThemeContext().getCssImageCache().export ( exportPath.asFile() ) ;
		
		} catch ( Exception e ) {
			throw new AppRuntimeException("Failed to export CSS images ", e ) ;
		}
	}
	
	
	protected void exportReadme(){
		
    	// create README.html
    	RenderEngine renderer = ThemeApp.getRenderEngine() ;
    	
    	VelocityContext ctx = new VelocityContext() ;
    	
    	ctx.put( "permalink", ThemeUtils.getPermalink () ) ;
    	ctx.put( "maxDownloads", ThemeApp.getConfig().getMaxDownloads() ) ;
    	
    	
    	try {
	    	
    		renderer.generate( ctx, "README.txt.vm", 
	    			new FileWriter( new File( ThemeApp.get().getExportDirectoryRealPath() + "/README.txt" ) ) ) ;    	
    	
    	} catch ( Exception e ){
    		throw new AppRuntimeException("Failed to generate README.txt !", e ) ;
    	}
	}
	
	
	
	public void export() throws Exception {

		ThemeContext themeContext = ThemeApp.getThemeContext() ;

		CMS.set (  themeContext, CMS.TYPE_XHTML ) ;
		
		clearExportFolder() ;
		exportCSSImages() ;
		exportStylesheet() ;
		exportJavascript();
		exportDoc() ;
		
		if ( themeContext.isExportMode() ){
			exportReadme() ;
		}
		
	}
	
	
	
	public void exportDoc() throws Exception {
	
		ThemeContext themeContext = ThemeApp.getThemeContext() ;

		CMS.set (  themeContext, CMS.TYPE_XHTML ) ;
		
		boolean download  = themeContext.isExportMode() ;
		
		Path exportPath ; 
		
		Writer writer ;
		
		String vmTemplate, file, exportDir ; 
		
		PropertySet page ;
		
		PropertySet navigation  = ( PropertySet) themeContext.get( "navigation" ) ;
		
		PropertySet doc  = ( PropertySet) themeContext.get( "doc" ) ;
		
		
		// export full page index.html
		
		page = ( PropertySet ) navigation.getProperty( "home" ) ;
		
		file = "index.html" ;
		
		exportDir = download ? ThemeApp.get().getExportDirectoryRealPath() :
			ThemeApp.get().getWorkRealPath() ;
		
		exportPath = new Path ( exportDir ).append ( file ) ;
		
		writer = ( download ) ?  new StringWriter() : new FileWriter( exportPath.toString () ) ;
		
		vmTemplate =  themeContext.getDesign().getTemplate() + "/xhtml/document.html.vm" ;
		
		VelocityContext context = new VelocityContext ( themeContext  );
		
		context.put( "currentPage", page ) ;
		
		try {
			
			engine.mergeTemplate( vmTemplate ,"UTF-8", context, writer ) ;
			
			if ( download ){
				
				// for download export as a well formated document with indentation
				writePretty ( ( ( StringWriter ) writer ).getBuffer (), exportPath ) ;
			
			}

		} catch ( Exception e ) {
			
			log.error( e ) ;
			throw new AppException( "Failed to export to xhtml.", e ) ;
		
		} finally {

			try {
				writer.flush () ;
				writer.close () ;
			} catch ( Exception e ){
				
				log.error ( e.getMessage (), e ) ;
			
			} 
		}
		
		
		
		
		// Now export all pages fragment
		
		for ( Property p : navigation.getProperties() ){
			
			if ( !p.isSet() ){
				
				throw new AppRuntimeException(
						"Invalid 'navigation' set. It should contain only sub set properties. "
						+ " ' " + p.getLongName() + "' is not a set" ) ; 
			
			}
			
			page = ( PropertySet ) p  ;
			
			file = page.get( "file" ) + ".html" ;
			
			context = new VelocityContext ( themeContext );
			
			if ( "home".equals( page.getName() ) && download ){
				file = "index.html" ;
			}
			
						
			if( ! "home".equals( page.getName() ) && download && !ThemeApp.getTheme().isPurchased() ){
				continue ;
			}
			
			
			exportPath = download ? new Path (  ThemeApp.get().getExportDirectoryRealPath() ).append ( file )
					: new Path (  ThemeApp.get().getWorkRealPath() ).append( file ) ;
			
			
			writer = ( download ) ?  new StringWriter() : new FileWriter( exportPath.toString () ) ;
			
			
			if ( log.isDebugEnabled() ){
				log.debug( "Exporting to " + exportPath.toString () ) ;
			}
			
			context = new VelocityContext ( themeContext  );
			
			context.put( "currentPage", page ) ;
						
			SetHierarchy.create( doc ) ;
			
			context.put ( "s", doc ) ;
			
			vmTemplate = ( download ) ? themeContext.getDesign().getTemplate() + "/xhtml/document.html.vm" : 
				DesignUtils.getSymbol ( doc.getParent () ).getPath () + "/body.html.vm" ;
			
			try {
				
				engine.mergeTemplate( vmTemplate ,"UTF-8", context, writer ) ;
				
				if ( download ){
					
					writePretty ( ( ( StringWriter ) writer ).getBuffer (), exportPath ) ;
				
				}

			} catch ( Exception e ){
				
				log.error( e ) ;
				throw new AppException( "Failed to export to xhtml. " + page.getName() , e ) ;
			
			} finally {

				try {
					writer.flush () ;
					writer.close () ;
				} catch ( Exception e ){
					log.error ( e.getMessage (), e ) ;
					SetHierarchy.clear() ;
				}
				
			}
			
		}
		
		
		if ( log.isDebugEnabled() ){
			log.debug( "Export successful" ) ;
		}
		
	}
	

	
    private void writePretty ( StringBuffer xhtml, Path exportPath ) throws Exception {
        
    	if ( beautifier == null ){
    		beautifier = new XHTMLBeautifier() ;
    	}
    	
    	beautifier.writePretty( xhtml, exportPath ) ;
    	
    }	
   
    
    
    protected class InnerEngine extends VelocityEngine {
    	
    	@Override
    	public boolean mergeTemplate ( String templateName, Context context, Writer writer ) {
    		return super.mergeTemplate ( templateName, "UTF-8", context, writer );
    	}
    	
    }


	
}
