package com.dotemplate.theme.server.cms.xhtml;

import com.dotemplate.core.server.App;
import com.dotemplate.theme.server.cms.CMS;
import com.dotemplate.theme.server.cms.ExportEngine;
import com.dotemplate.theme.server.cms.ThemeArchiver;



public class DefaultCMS extends CMS {

	@Override
	protected String getCmsId () {
		return CMS.XHTML ;
	}

	@Override
	public ExportEngine getExportEngine () {
		return ( ExportEngine ) App.getSingleton ( XHTMLEngine.class );
	}
	
	@Override
	public ThemeArchiver getThemeArchiver () {
		return ( ThemeArchiver ) App.getSingleton ( XHTMLArchiver.class );
	}

	

}
