package com.dotemplate.theme.server.cms.wp;

import com.dotemplate.core.server.App;
import com.dotemplate.theme.server.cms.CMS;
import com.dotemplate.theme.server.cms.ExportEngine;
import com.dotemplate.theme.server.cms.ThemeArchiver;

public class Wordpress extends CMS {

	@Override
	protected String getCmsId () {
		return CMS.WP;
	}

	@Override
	public ExportEngine getExportEngine () {
		return ( ExportEngine ) App.getSingleton ( WordpressEngine.class ) ;
	}

	@Override
	public ThemeArchiver getThemeArchiver () {
		return ( ThemeArchiver )  App.getSingleton ( WordpressArchiver.class ) ;
	}

}
