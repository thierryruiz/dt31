package com.dotemplate.theme.server.cms.wp;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.theme.server.ExportThemeContext;
import com.dotemplate.theme.server.ThemeApp;
import com.dotemplate.theme.server.ThemeContext;

public class WordpressThemeContext extends ExportThemeContext {
	
	private static Log log = LogFactory.getLog( WordpressThemeContext.class ) ;
	
	public static final String DOWNLOAD_REAL_PATH = "downloadRealPath" ;

	// admin panel options
	protected WordpressAdminOptions adminOptions ;
	
	// all exported resources
	protected WordpressResources exportResources ;
	
	protected WordpressCustomMenus customMenus ;
	
	
	public WordpressThemeContext ( ThemeContext ctx ) {
		
		super( ctx ) ;
		put ( "_wp", new WordpressThemeHelper() ) ;
		put ( DOWNLOAD_REAL_PATH, ThemeApp.get().getExportDirectoryRealPath () ) ;
		
		adminOptions 		= new WordpressAdminOptions() ;
		exportResources 	= new WordpressResources() ;
		customMenus 		= new WordpressCustomMenus() ;
		
		
		if ( log.isDebugEnabled ()){
			log.debug ( "Wordpress theme context created" ) ;
		}
		
	}
	
	
	
	public WordpressAdminOptions getAdminOptions() {
		return adminOptions;
	}
	
	
	public WordpressResources getExportResources() {
		return exportResources;
	}
	
	public WordpressCustomMenus getCustomMenus() {
		return customMenus;
	}
	
	
	@Override
	public Object get ( String key ) {
		
		/*
		if ( ThemeApp.isDevMode () ){
			
			Object result = super.get( key ) ;
			
			if ( result == null ){
				
				log.error ( "WordpressThemeContext  '" + key + "' has return null object'" ) ;
			
			}
			
			return result ;
		}
		*/
		
		return super.get ( key );
	
		
	}
	
	
	
	@Override
	public void exportCMSResources() {
		exportResources.export() ;
	}

	
}
