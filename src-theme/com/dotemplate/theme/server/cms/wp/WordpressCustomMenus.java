package com.dotemplate.theme.server.cms.wp;

import java.util.ArrayList;

public class WordpressCustomMenus {
	
	private ArrayList< Menu > menus ;
	
	
	public void registerLocation( String name, String desc ){
		if( menus == null ){
			menus = new ArrayList< Menu >() ;
		}
		
		Menu menu = new Menu() ;
		menu.name = name ;
		menu.desc = desc ;
		menus.add( menu ) ;
	}
	
	
	
	public String asPHP() {
		StringBuffer sb = new StringBuffer() ;
		
		if ( menus != null ){
			for( Menu menu : menus ){
				sb.append( "\n\t\t'" ).append( menu.name ).append( "' => '" ).append(  menu.desc ) .append("'," ) ;
			}
		}
		
		return sb.toString() ;
		
	}
	


	
	
	private class Menu  {
		
		String name ;
		
		String desc ;
		
	}
	
}
