package com.dotemplate.theme.server.cms.wp;


public abstract class WordpressAdminOption {
	
	private String category ;
	
	private String id ;
	
	private String name ;
	
	private String desc ;
		
	private String value ;
	
	public abstract String getType() ;
	
	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	
	public StringBuffer asPHP(){
		
		StringBuffer sb = new StringBuffer()
			.append( "\t\"name\" => \"").append( name ).append("\",\n" )
			.append( "\t\"desc\" => \"").append( desc ).append("\",\n" )			
			.append( "\t\"id\" => \"").append( id ).append("\",\n" )
			.append( "\t\"type\" => \"").append( getType() ).append("\",\n" )			
			.append( "\t\"std\" => \"").append( value ).append("\",\n" ) ;			
			
		return sb ;
	}
	
	
}





