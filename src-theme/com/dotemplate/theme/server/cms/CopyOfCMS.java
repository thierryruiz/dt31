package com.dotemplate.theme.server.cms;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.App;
import com.dotemplate.core.shared.properties.PropertySet;
import com.dotemplate.theme.server.ExportThemeContext;
import com.dotemplate.theme.server.ThemeContext;
import com.dotemplate.theme.server.cms.blogger.Blogger;
import com.dotemplate.theme.server.cms.blogger.BloggerThemeContext;
import com.dotemplate.theme.server.cms.wp.Wordpress;
import com.dotemplate.theme.server.cms.wp.WordpressThemeContext;
import com.dotemplate.theme.server.cms.xhtml.DefaultCMS;

@Deprecated
public abstract class CopyOfCMS {

	private final static Log log = LogFactory.getLog ( CopyOfCMS.class );

	public final static int TYPE_XHTML = 0;

	public final static int TYPE_BLOGGER = 1;

	public final static int TYPE_WP = 2;
	
	public final static int TYPE_XPRS = 3;

	public final static String XHTML = "xhtml";

	public final static String BLOGGER = "blogger";
	
	public final static String XPRS = "xprs";

	public final static String WP = "wp";

	protected Map<String, String> htmlTemplateMap = new HashMap<String, String> ();

	protected Map<String, String> cssTemplateMap = new HashMap<String, String> ();

	protected Map<String, String> jsTemplateMap = new HashMap<String, String> ();
	
	protected Map<String, String> randomTemplateMap = new HashMap<String, String> ();
	

	protected abstract String getCmsId ();

	public abstract ExportEngine getExportEngine ();

	public abstract ThemeArchiver getThemeArchiver ();

	public static ExportEngine getExportEngine ( String cmsId ) {
		return get ( cmsId ).getExportEngine ();
	}
	
	
	public static ThemeContext wrappThemeContextForCMS( ThemeContext ctx, String cmsId  ){
		
		if ( log.isDebugEnabled () ){
			log.debug ( "Wrapping theme context for CMS " + cmsId  ) ;
		}
		
		
		if ( CopyOfCMS.XHTML.equals ( cmsId ) ) {
			
			return new ExportThemeContext( ctx ) ;
			
		} else if ( CopyOfCMS.BLOGGER.equals ( cmsId ) ) {
			
			return new BloggerThemeContext( ctx ) ;
		
		} else if ( CopyOfCMS.WP.equals ( cmsId ) ) {
			
			return new WordpressThemeContext( ctx ) ;
			
		} else if ( CopyOfCMS.XPRS.equals(cmsId ) ){
			return new ExportThemeContext( ctx ) ;
		} else {
		
			throw new IllegalArgumentException ( "Unknown CMS identifier'" + cmsId + "'" );
		}
	}
	

	public static ThemeArchiver getThemeArchiver ( String cmsId ) {
		return get ( cmsId ).getThemeArchiver ();
	}

	
	protected boolean templateExists ( String template ) {
		return App.getRenderEngine ().templateExists ( template );
	}

	
	public static void set ( ThemeContext ctx, String cmsId ) {

		if ( CopyOfCMS.XHTML.equals ( CopyOfCMS.XHTML ) ) {
			ctx.put ( XHTML, true );
			ctx.put ( ThemeContext.CMS, TYPE_XHTML );
		} else if ( CopyOfCMS.BLOGGER.equals ( cmsId ) ) {
			ctx.put ( BLOGGER, true );
			ctx.put ( ThemeContext.CMS, TYPE_BLOGGER );
		} else if ( CopyOfCMS.WP.equals ( cmsId ) ) {
			ctx.put ( WP, true );
			ctx.put ( ThemeContext.CMS, TYPE_WP );
		} else if ( CopyOfCMS.XPRS.equals ( cmsId ) ) {
			ctx.put ( XPRS, true );
			ctx.put ( ThemeContext.CMS, TYPE_XPRS );
		} else {
			throw new IllegalArgumentException ( "Unknown CMS identifier'" + cmsId + "'" );
		}
	}

	
	public static void set ( ThemeContext ctx, int type ) {

		switch ( type ) {
		case TYPE_XHTML :
			ctx.put ( XHTML, true );
			ctx.put ( ThemeContext.CMS, TYPE_XHTML );
			break;

		case TYPE_BLOGGER :
			ctx.put ( BLOGGER, true );
			ctx.put ( ThemeContext.CMS, TYPE_BLOGGER );
			break;

		case TYPE_WP :
			ctx.put ( WP, true );
			ctx.put ( ThemeContext.CMS, TYPE_WP );
			break;

		case TYPE_XPRS :
			ctx.put ( XPRS, true );
			ctx.put ( ThemeContext.CMS, TYPE_XPRS );
			break;
			
			
		default :
			throw new IllegalStateException ( "Unknown CMS type " + type );
		}

	}

	public static CopyOfCMS get ( ThemeContext themeContext ) {

		int type = ( Integer ) themeContext.get ( ThemeContext.CMS );

		switch ( type ) {
		case TYPE_XHTML :
			return ( CopyOfCMS ) App.getSingleton ( DefaultCMS.class );
		case TYPE_BLOGGER :
			return ( CopyOfCMS ) App.getSingleton ( Blogger.class );
		case TYPE_WP :
			return ( CopyOfCMS ) App.getSingleton ( Wordpress.class );
//		case TYPE_XPRS :
//			return ( CopyOfCMS ) App.getSingleton ( XPRS.class );	

		default :
			throw new IllegalStateException ( "No CMS defined in context (null "
									+ "or unknown CMS identifier set" );
		}
	}

	public String getCssTemplate ( PropertySet symbol ) {
		return cssTemplateMap.get ( symbol.getPath () );
	}

	public String getHtmlTemplate ( PropertySet symbol ) {
		return htmlTemplateMap.get ( symbol.getPath () );
	}

	public String getJsTemplate ( PropertySet symbol ) {
		return jsTemplateMap.get ( symbol.getPath () );
	}
	
	public String getRandomTemplate ( PropertySet symbol ) {
		return randomTemplateMap.get ( symbol.getPath () );
	}

	public static void preloadSymbol ( PropertySet symbol ) {

		( ( CopyOfCMS ) App.getSingleton ( DefaultCMS.class ) ).preloadSymbolTemplates ( symbol );
		( ( CopyOfCMS ) App.getSingleton ( Blogger.class ) ).preloadSymbolTemplates ( symbol );
		( ( CopyOfCMS ) App.getSingleton ( Wordpress.class ) ).preloadSymbolTemplates ( symbol );
		//( ( CopyOfCMS ) App.getSingleton ( XPRS.class ) ).preloadSymbolTemplates ( symbol );		

	}

	public static CopyOfCMS get ( String cmsId ) {

		if ( CopyOfCMS.XHTML.equals ( cmsId ) ) {
			return ( CopyOfCMS ) App.getSingleton ( DefaultCMS.class );
		} else

		if ( CopyOfCMS.BLOGGER.equals ( cmsId ) ) {
			return ( CopyOfCMS ) App.getSingleton ( Blogger.class );
		} else

		if ( CopyOfCMS.WP.equals ( cmsId ) ) {
			return ( CopyOfCMS ) App.getSingleton ( Wordpress.class );
		}

		else {
			new IllegalArgumentException ( "Unknown CMS identifier'" + cmsId + "'" );
		}

		return null;
	}

	protected void preloadSymbolTemplates ( PropertySet symbol ) {

		if ( log.isDebugEnabled () ) {
			log.debug ( "Preload symbols templates..." );
		}

		String vm = new StringBuffer ( symbol.getPath () ).append ( "/" ).append ( getCmsId () )
								.append ( "/css.vm" ).toString ();

		vm = templateExists ( vm ) ? vm : new StringBuffer ( symbol.getPath () )
								.append ( "/css.vm" ).toString ();

		if ( templateExists ( vm ) ) {
			cssTemplateMap.put ( symbol.getPath (), vm );
		}

		
		vm = new StringBuffer ( symbol.getPath () ).append ( "/" ).append ( getCmsId () ).append (
								"/html.vm" ).toString ();

		vm = templateExists ( vm ) ? vm : new StringBuffer ( symbol.getPath () ).append (
								"/html.vm" ).toString ();

		if ( templateExists ( vm ) ) {
			htmlTemplateMap.put ( symbol.getPath (), vm );
		}

		vm = new StringBuffer ( symbol.getPath () ).append ( "/" ).append ( getCmsId () ).append (
								"/js.vm" ).toString ();

		vm = templateExists ( vm ) ? vm : new StringBuffer ( symbol.getPath () ).append ( "/js.vm" )
								.toString ();

		if ( templateExists ( vm ) ) {
			jsTemplateMap.put ( symbol.getPath (), vm );
		}
		

		vm = new StringBuffer ( symbol.getPath () ).append ( "/" ).append ( getCmsId () ).append (
				"/random.vm" ).toString ();

		
		vm = templateExists ( vm ) ? vm : new StringBuffer ( symbol.getPath () ).append ( "/random.vm" )
				.toString ();

		if ( templateExists ( vm ) ) {
			randomTemplateMap.put ( symbol.getPath (), vm );
		}

	}
	

}
