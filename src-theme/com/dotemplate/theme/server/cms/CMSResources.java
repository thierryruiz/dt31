package com.dotemplate.theme.server.cms;

import java.io.File;
import java.io.StringWriter;
import java.util.HashMap;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.frwk.AppRuntimeException;
import com.dotemplate.core.server.util.Path;
import com.dotemplate.theme.server.ThemeApp;

public abstract class CMSResources < R extends CMSResource > {
	
	protected HashMap< String, R > resources = new HashMap< String, R >() ;
	
	private static Log log = LogFactory.getLog( CMSResources.class ) ;
	
	public void export(){
			
		Path path ;
		
		for ( R r  : resources.values() ){
			
			path = new Path ( ThemeApp.get().getExportDirectoryRealPath () ) ;
			
			path.append( r.getPath().asString() ) ;
			
			
			if ( log.isDebugEnabled () ){
				log.debug ( "Creating file "  + path.toString () ) ;
			}
			
			try {

				FileUtils.writeStringToFile ( path.asFile (), r.getContent().toString() ) ;
			
			} catch ( Exception e ) {
				
				throw new AppRuntimeException ( "Failed to export CMS resource '" + r.getName() + "'", e );
			}
			
		}
		
	}
	
	
		
	public R get ( String name ){
		
		R r = resources.get( name ) ;
		
		if ( r == null  ){
			throw new AppRuntimeException( "Resource '" + name + "' undefined" ) ;
		}
		
		return resources.get( name ) ;
	}
	
	public void set ( R resource ){
		resources.put( resource.getName(), resource ) ;
	}
	

	public void append( String resourceName, String filePath ){
		
		File src = new Path( ThemeApp.realPath ( filePath ) ).asFile () ;
		
		if ( ! src.exists () ){
			
			throw new AppRuntimeException( "Add content to file " + filePath + 
					" failed. File '" + src + "' does not exist." ) ;
		
		}
		
		String content = "" ;
		
		try {
			
			content = FileUtils.readFileToString ( src ) ;
			
		} catch ( Exception e ){
			
			throw new AppRuntimeException( "Add content to file " + filePath +
					" failed. Failed to read source file '" + src + "'.", e ) ;
		
		}
		
		
		get( resourceName ).appendContent( content ) ;
		
		
	}

	
	
	public void parse( String resourceName , String template  ){
		
		StringWriter writer = new StringWriter() ;
		
		try {
			
			ThemeApp.getRenderEngine().generate(  ThemeApp.getThemeContext(), template, writer ) ;
		
			get( resourceName ).appendContent( writer.getBuffer().toString() ) ;
			
			
		} catch ( Exception e ) {
			
			throw new AppRuntimeException( "Failed to parse '" + template + "'", e ) ;
		
		} 
		
		
		
	}
	
	
	
}
