package com.dotemplate.theme.server.cms.blogger;

import com.dotemplate.core.server.App;
import com.dotemplate.theme.server.cms.CMS;
import com.dotemplate.theme.server.cms.ExportEngine;
import com.dotemplate.theme.server.cms.ThemeArchiver;



public class Blogger extends CMS {


	@Override
	protected String getCmsId () {
		return CMS.BLOGGER ;
	}
	
	
	@Override
	public ExportEngine getExportEngine () {
		return ( ExportEngine ) App.getSingleton ( BloggerEngine.class ) ;
	}


	@Override
	public ThemeArchiver getThemeArchiver () {
		return ( ThemeArchiver ) App.getSingleton ( BloggerArchiver.class );
	}


	
	
	
}
