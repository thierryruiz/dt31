package com.dotemplate.theme.server.cms.blogger;

import java.io.File;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


import com.dotemplate.core.server.util.ConcurrentTask;
import com.google.gdata.data.media.MediaFileSource;
import com.google.gdata.data.photos.PhotoEntry;



public class PicasaUploadTask extends ConcurrentTask< PicasaUpload > {

	private static Log log = LogFactory.getLog( PicasaUploadTask.class ) ;
	
	private boolean uploaded = false ;
		
	private int retry = 0 ;
	
	
	
	@Override
	public PicasaUpload call () throws Exception {

		retry = 0 ;
		uploaded = false ;
		
		PicasaUpload context = ( PicasaUpload ) super.taskContext ;
		File image = context.getImage () ;
		
		// by default
		context.setUploadUrl ( image.getName () ) ;
		
		
		// Upload images ( tolerant with picasa error )
		while ( !uploaded ) {
			
			if ( retry > 3 ){
				log.warn ( "Picasa server unavailable while uploading image " + image.getName () + " after several retry !" ) ;
				break ;
			} 
			
			
			try {
				
				if ( log.isDebugEnabled () ){
					log.debug ( "Try to upload '" + image.getName () + "' to Picasa... retry= " + retry  ) ;
				}
				
				MediaFileSource myMedia = new MediaFileSource( image, "image/jpeg" ) ;
				PhotoEntry returnedPhoto = context.getPicasaService ().insert( 
										context.getUploadAlbum (), PhotoEntry.class, myMedia );
				
				context.setUploadUrl ( returnedPhoto.getMediaContents ().get ( 0 ).getUrl () ) ;
				
				uploaded = true ;
				
				log.info ( "Image '" + image.getName () + "' uploaded to Picasa..." ) ;
				
			
			} catch ( com.google.gdata.util.ServiceException e ) {
				log.warn ( "Picasa server unavailable while uploading image " + image.getName () + ". Retrying..." ) ;
				Thread.sleep ( 3000 ) ;
				retry++ ;	
			} catch ( Exception e ) {
				// silently return 
				log.warn( "Failed to upload image '" + image.getName () + "' in Picasa", e ) ;
				break ;
			}
			
		}
		
		
		return context ;
	}
	
	
	
	

}


