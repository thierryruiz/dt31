/*
 /*
 * 
 * Created on 1 mars 2007
 * 
 */
package com.dotemplate.theme.server;

import java.util.Collection;
import java.util.LinkedHashMap;

import com.dotemplate.core.server.DesignContext;
import com.dotemplate.theme.server.css.Stylesheet;
import com.dotemplate.theme.shared.Theme;



/**
 * 
 * 
 * @author Thierry Ruiz
 * 
 */
public interface ThemeContext extends DesignContext< Theme > {

	public final static String TYPE_XHTML = "xhtml" ;
	
	public final static String TYPE_BLOGGER = "blogger" ;
		
    public final static String THEME = "theme";
	
	public final static String JS = "JS" ;
	
	public final static String CSS_IMAGE_DIR = "cssImagesDir" ;
	
	public final static String VOID_LINK = 	"voidlink" ;
	
	public final static String BACKLINK = 	"backlink" ;
	
	public static final String CMS = "CMS";

	public final static String BASE_URL = "baseUrl" ;
	
	public final static String CONTENT = "_c" ;
		
	public final static String YEAR = "year" ;	
		
	public static final String PAYPAL_URL = "paypalUrl" ;
	
	public static final String PAYPAL_NOTIFY_URL = "paypalNotifyUrl" ;
		
	public static final String PAYPAL_BUTTON_ID = "paypalButtonId" ;

	
	ThemeRefresh getThemeRefresh() ;

    Collection< String > getRowScripts() ;
    
    Collection< String > getScripts() ;

    Collection< String > getPolyfills() ;
    
	void addRowScript ( String id, String js ) ;

	void addScript ( String url ) ;

	void addPolyfill ( String url ) ;
	
	void clearRowScripts () ;

	Stylesheet getStylesheet() ;
	
	void setStylesheet( Stylesheet stylesheet ) ;
	
	CssImageCache getCssImageCache() ;
	
	LinkedHashMap< String, GoogleFont > getGoogleFonts() ;
	
	

		
}

