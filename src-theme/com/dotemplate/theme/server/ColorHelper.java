package com.dotemplate.theme.server;

import com.dotemplate.core.server.ColorUtils;
import com.dotemplate.core.server.frwk.AppRuntimeException;
import com.dotemplate.core.shared.Scheme;
import com.dotemplate.core.shared.properties.ColorProperty;
import com.dotemplate.core.shared.properties.PropertySet;

public class ColorHelper {
	
	private ColorProperty colorPropertyToSet ;
	
	private String colorPropertyNameToSet ;
	
	public ColorHelper() {	
	}
	
	public ColorHelper setTo( PropertySet set ){
		this.colorPropertyToSet = (ColorProperty) set.getProperty("color") ;
		return this ; 
	}
	
	public ColorHelper setTo( String name ){
		this.colorPropertyNameToSet = name ;		
		return this ; 
	}
	
	public ColorHelper of( PropertySet set ){
		this.colorPropertyToSet = (ColorProperty) set.getProperty(colorPropertyNameToSet ) ;
		return this ;
	}
	
	public void oppositeOf( String color ){
		this.colorPropertyToSet.setValue( ColorUtils.getOpposite( color) );
	}
	
	public void dominant(){
		
		Scheme scheme = ThemeApp.getThemeManager().getScheme() ;
		
		if ( scheme == null ){
			throw new AppRuntimeException ( "Unknown scheme '" + ThemeApp.getTheme().getScheme() + "'" ) ;
		}
		
		String color = scheme.getColor( "dominant" ) ;
				
		this.colorPropertyToSet.setValue( color ) ;
	}

	

}
