package com.dotemplate.theme.server.css;

import java.util.LinkedHashMap;

import org.apache.commons.lang.StringUtils;


public class SimpleCSSParser {
	
	String doc ;
	
	public Stylesheet parseStyleSheet( String document ){
		
		//System.out.println( document  ) ;
		
		this.doc = document;
		
		Stylesheet stylesheet = new Stylesheet() ;
		
		doc = doc.replaceAll("(\\r|\\n|\\t)", "" );
		
		Media media ;
		Rule rule ;
		
		
		while ( doc.length() > 0 ){
			
			doc = StringUtils.strip( doc.trim() ) ;
			
			ignoreComments() ;
			
			media = parseMedia() ;

			if ( media != null ){
				stylesheet.add( media ) ;
			} else {
				rule = parseRule() ;
				stylesheet.add( rule ) ;
				//System.out.println( "\tRule parsed..." + rule.getSelector() + "\n\n" );
			}
			
			
		}
		
		
		return stylesheet ;
	}
	
	
	
	private void ignoreComments() {
		
		if ( doc.startsWith( "/*" ) ) {
			doc = StringUtils.substringAfter( doc, "*/" ) ;
		}
		
		if ( doc.startsWith( "//" ) ) {
			doc = StringUtils.substringAfter( doc, "\n" ) ;
		}
		
	}

	
	public Media parseMedia() {
		
		if( !doc.trim().startsWith("@media") ){ 
			//System.out.println( "Not a media content>" + doc ) ;
			return null ;
		}
		
		String selector = StringUtils.substringBefore( doc, "{" ) ;

		if ( selector == null || selector.trim().length() == 0 ) {
			throw new CSSParseException ( "Expected media selector at " + doc.substring( 0, 150 ) ) ;
		}

		doc = StringUtils.removeStart( doc, selector ).trim() ;
		
		selector = selector.trim() ;
		
		Media media = new Media() ;
		
		media.setSelector( selector ) ;
		media.setRules( parseMediaRules() ) ;
		
		
		
		return media ;
		
	}
	

	private LinkedHashMap< String, Rule > parseMediaRules() {
		
		String subdoc = StringUtils.substringBefore( doc, "}}" ) ;
 		subdoc = StringUtils.substringAfter( subdoc,"{" ) + "}" ;
		
 		//System.out.println( "\n\nparsing media content" + subdoc ) ;
 		
		SimpleCSSParser mediasheetParser = new SimpleCSSParser() ;
		
		Stylesheet mediasheet = mediasheetParser.parseStyleSheet( subdoc ) ;
		
		
		
		doc = StringUtils.substringAfter( doc, subdoc ).trim() ;
		doc = StringUtils.substringAfter(doc, "}" ) ;
		
		return mediasheet.getRules() ;
	}
	
	
	
	public Rule parseRule() {
		
		String selector = StringUtils.substringBefore( doc, "{" ) ;
		
		//System.out.println( "Parse selector " + selector ) ;
		
		if ( selector == null || selector.trim().length() == 0 ) {
			throw new CSSParseException ( "Expected CSS rule around " + doc.substring( 0, 20 ) + "..." ) ;
		}

		
		//System.out.println( ">>>>>" +  doc ) ;
		
		doc = StringUtils.removeStart( doc, selector ).trim() ;
		
		//System.out.println( "<<<<<<" +  doc ) ;
		
		selector = selector.trim() ;
		
		Rule rule = new Rule()  ;
		rule.setSelector( selector ) ;
		rule.setDeclarations( parseDeclarations() ) ;
		
		return rule ;
	}


	
	private LinkedHashMap< String, Declaration > parseDeclarations() {
		
		LinkedHashMap<String, Declaration> declarations = new LinkedHashMap<String, Declaration>() ;
		
		try {
		String s = StringUtils.substringBetween( doc, "{", "}" ) ;
		
		//System.out.println( "\tparse declarations " + s ) ;
		
		String[] tks = StringUtils.split( s , ";" ) ;
		
		Declaration d ;
		
		for ( int i = 0; i < tks.length ; i++ ){
			d =  parseDeclaration ( tks[ i ] );
			if ( d == null ) continue ;
			declarations.put( d.getProperty(), d ) ;
		}
		
		doc = StringUtils.substringAfter( doc, "}" ).trim() ;
		
		// System.out.println( "\t\tparse " + s ) ;
		} catch ( Exception e ){
			System.out.println( "Failed to parse CSS\n" + doc ) ;
			throw e ;
		}
		
		return declarations;
	}
	
	
	
	
	public Declaration parseDeclaration( String s ) {
		
		s = s.trim() ;
		
		// System.out.println( "\t\tparse " + s ) ;
		
		String[] tokens = StringUtils.split( s, ":" ) ;
		
		
		
		if ( tokens == null || tokens.length < 2 ) {
			// ignore 
			return null ;
		}
		
		String property = tokens[0].trim() ;
		
		String value = StringUtils.substringAfter(s, ":" ) ;
		if ( value == null ){
			// ignore 
			return null ;			
		}
		
		value = value.trim() ;
		value = StringUtils.remove( value, ";" ) ;
		
		
		Declaration d = new Declaration() ;
		d.setProperty(property) ;
		d.setValue( value ) ;
		
		int i = property.indexOf( "-" ) ;
		
		if ( i > 0 ) {
			
			// properties like background-image to become backgroundImage 
			
			tokens = StringUtils.split( property , "-" ) ;
			char c = Character.toUpperCase( tokens[1].charAt( 0 ) ) ;
			d.setJsProperty( tokens[0] + c + tokens[ 1 ].substring( 1 ) );
			
			
		} else {
			
			d.setJsProperty( property ) ;		
		
		}
		
		return d ;
		
		
	}
	
	
	
	/*
	public Declaration parseDeclaration( String s ) {
		
		s = s.trim() ;
		
		// System.out.println( "\t\tparse " + s ) ;
		
		String[] tokens = StringUtils.split( s, ":" ) ;
		
		if ( tokens.length != 2 ) {
			// ignore 
			return null ;
		}
		
		String value = tokens[ 1 ].trim() ;
		value = StringUtils.remove( value, ";" ) ;
		
		String property = tokens[0].trim() ;
		
		int i = property.indexOf( "-" ) ;
		
		Declaration d = new Declaration() ;
		
		d.setProperty( property ) ;
		d.setValue(value);
		
		if ( i > 0 ) {
			
			// properties like background-image to become backgroundImage 
			
			tokens = StringUtils.split( property , "-" ) ;
			char c = Character.toUpperCase( tokens[1].charAt( 0 ) ) ;
			d.setJsProperty( tokens[0] + c + tokens[ 1 ].substring( 1 ) );
			
			
		} else {
			
			d.setJsProperty( property ) ;		
		
		}
		
		return d ;
		
		
	}*/
}
