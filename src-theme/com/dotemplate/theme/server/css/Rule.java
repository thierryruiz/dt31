package com.dotemplate.theme.server.css;

import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedHashMap;



public class Rule implements Serializable {

	private static final long serialVersionUID = 7608165258780285967L;
	
	protected LinkedHashMap< String, Declaration> declarations ;
	
	protected String selector ;	
	
	public Rule() {
		declarations = new LinkedHashMap<String, Declaration>() ;
	}
	
	public void setSelector(String selector) {
		this.selector = selector;
	}
	

	public String getSelector() {
		return selector;
	}
	
	public void add ( Declaration d ){
		declarations.put( d.getProperty(), d ) ;
	}
	
	public LinkedHashMap<String, Declaration> getDeclarations() {
		return declarations;
	}

	public void setDeclarations(LinkedHashMap< String, Declaration > declarations) {
		this.declarations = declarations;
	}
	
	
	public void append( Collection< Declaration > ds ) {
		for ( Declaration d : ds ){
			declarations.put( d.getProperty(), d ) ;
		}
	}

	public boolean isEmpty() {
		return declarations.isEmpty() ;
	}
	
}
