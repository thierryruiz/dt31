package com.dotemplate.theme.server.css;

import java.io.OutputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.dotemplate.core.server.frwk.AppException;
import com.dotemplate.theme.server.ThemeApp;
import com.dotemplate.theme.server.ThemeContext;


public class CSSHelper {
	
	private final static String[] _colorProperties = {
		"color", 
		"background", 
		"background-image", 
		"background-color", 
		"border", 
		"border-color",
		"border",
		"border-left",
		"border-right",
		"border-bottom",
		"border-top",
		"border-left-color",
		"border-right-color",
		"border-bottom-color",
		"border-top-color",
	} ;
	
	
	private final static List< String > _colorPropertiesList =  Arrays.asList( _colorProperties )  ;  
	
	
	public static Iterator< Declaration > update( Rule oldRule,  Rule newRule ){
		
		ArrayList< Declaration > updated = new ArrayList< Declaration > () ;
		
		String property ;
		
		Declaration oldDeclaration ;
		
		HashMap<String, Declaration> oldDeclarations = oldRule.getDeclarations() ;
		
		for ( Declaration newDeclaration : newRule.getDeclarations().values() ){
			
			property =  newDeclaration.getProperty() ;
			
			oldDeclaration = oldDeclarations.get( property ) ;
			
			
			// absent
			if ( oldDeclaration == null  ) {
				
				oldDeclarations.put( property, newDeclaration ) ;
				updated.add( newDeclaration ) ;
				continue ;
			
			}  
			
			// present but equal
			if( newDeclaration.getValue().equals( oldDeclaration.getValue()) ) {
				continue ;
			} 
			
			
			// present but not equal
			oldDeclarations.put( property, newDeclaration ) ;
			updated.add( newDeclaration ) ;
			
		}
		
		return updated.iterator() ;
		
	}
	
	
	
	public static Stylesheet update ( Stylesheet sheet, Stylesheet newSheet ){
		
		// overrides a stylesheet and get the difference 
		
		Stylesheet diffSheet = new Stylesheet() ;
		
		Rule oldRule, diff ;
		
		Iterator< Declaration > diffDeclarations  ;
		
		LinkedHashMap< String, Rule > rules = sheet.getRules() ;
		
		for ( Rule newRule : newSheet.getRules().values() ){
			
			oldRule = rules.get( newRule.getSelector() ) ;
			
			if ( oldRule != null ){
				
				// old rule already exist add declarations if different
				
				diffDeclarations = update( oldRule,  newRule ) ;
				
				diff = new Rule () ;
				
				diff.setSelector( oldRule.getSelector() ) ;
				
				while ( diffDeclarations.hasNext() ){
				
					diff.add( diffDeclarations.next() ) ;
				
				}
				
				if ( ! diff.isEmpty() ){
					
					diffSheet.add( diff ) ;
				
				}
				
				
			} else {
				
				// rule not exist yet
				diffSheet.add( newRule ) ;
			
			}
		}
		
		return diffSheet ;
	
	}
	

	
	
	
	public static void print( Stylesheet sheet, OutputStream os, boolean pretty, boolean includeMediaQueries ) throws AppException {
		
		if ( pretty ){
			
			printPretty( sheet, os, includeMediaQueries ) ;
		
		} else {
		
			try {
				
				os.write( getStylesheetAsString( sheet, includeMediaQueries ).getBytes() ) ;
				
				
			} catch ( Exception e ) {
				
				throw new AppException ( "Failed to output stylesheet ", e  ) ;
				
			} finally {
				try {
					os.flush() ;
					os.close() ;
				} catch ( Exception e ){} 
			}

		}
		
	}
	
	
	
	
	private static void printPretty ( Stylesheet sheet, OutputStream os, boolean includeMediaQueries ) throws AppException {
		
		ThemeContext themeContext = ThemeApp.getThemeContext() ;
	
		StringWriter writer = new StringWriter() ;
		
		String vmTemplate ;
		
		StringBuffer sb = new StringBuffer() ;
		
		vmTemplate = themeContext.getDesign().getTemplate() + "/style-header.css.vm" ;
			
		try {
			
			ThemeApp.getRenderEngine().mergeTemplate( vmTemplate ,"UTF-8", themeContext, writer ) ;
			
			sb.append( writer.getBuffer().toString() ) ;
			

		} catch ( Exception e ) {

			throw new AppException ( "Failed to output stylesheet ", e  ) ;
		}
		
		
		for ( Rule rule : sheet.getRules().values() ){
			sb.append( rule.getSelector() ).append(" {\n") ;
			
			for ( Declaration declaration : rule.getDeclarations().values() ){
				sb.append('\t').append( realProperty ( declaration ) ).append( ':' ) ;
				sb.append( declaration.getValue() ).append( ';' ).append( '\n' ) ;
			}
			
			sb.append( "}\n\n" ) ;

		}
		
		
		if ( includeMediaQueries ){
			for( Media media : sheet.getMedias().values() ){
				
				sb.append( media.getSelector() ).append(" {\n") ;
				
				for ( Rule rule : media.getRules().values() ){
					
					sb.append( rule.getSelector() ).append(" {\n") ;
					
					for ( Declaration declaration : rule.getDeclarations().values() ){
						sb.append('\t').append( realProperty ( declaration ) ).append( ':' ) ;
						sb.append( declaration.getValue() ).append( ';' ).append( '\n' ) ;
					}
					
					sb.append( "}\n\n" ) ;
		
				}
				
				sb.append( "}\n\n" ) ;
			}		
		}
		
		try {
			
			os.write( sb.toString().getBytes() ) ;
		
		} catch ( Exception e ) {
			
			throw new AppException ( "Failed to output stylesheet ", e  ) ;
			
		} finally {
			try {
				os.flush() ;
				os.close() ;
			} catch ( Exception e ){} 
		}
		
		
	}
	
	
	public static String getStylesheetAsString( Stylesheet sheet ) {
		
		return CSSHelper.getStylesheetAsString( sheet, true ) ;
		
	}
	
	
	public static String getStylesheetAsString( Stylesheet sheet, boolean includeMediaQueries ) {
		
		StringBuffer sb = new StringBuffer() ;
		
		for ( Rule rule : sheet.getRules().values() ){
			sb.append( rule.getSelector() ).append("{") ;

			for ( Declaration declaration : rule.getDeclarations().values() ){
				sb.append( realProperty ( declaration ) ).append( ':' ) ;
				sb.append( declaration.getValue() ).append( ';' ) ;
			}
			
			sb.append( "}\n" ) ;
			
		}
		
		
		if ( includeMediaQueries ){

			for( Media media : sheet.getMedias().values() ){
				
				if ( media.isEmpty() ) continue ;
				
				sb.append( media.getSelector() ).append(" {\n") ;
				
				for ( Rule rule : media.getRules().values() ){
					
					sb.append( rule.getSelector() ).append("{") ;
					
					for ( Declaration declaration : rule.getDeclarations().values() ){
						sb.append( realProperty ( declaration ) ).append( ':' ) ;
						sb.append( declaration.getValue() ).append( ';' );
					}
					
					sb.append( "} " ) ;
		
				}
				
				sb.append( "}\n" ) ;
			}		
		
		}
		
		
		return sb.toString() ;
	
	}
	

	
	/**
	 * 
	 * @param rule
	 * @return true if Rule is
	 */
	public static String getColorStylesheetAsString ( Stylesheet sheet ) {
		
		StringBuffer sb = new StringBuffer() ;
		
		for ( Rule rule : sheet.getRules().values() ){ 
			
			String selector = null ;
			
			for ( Declaration declaration : rule.getDeclarations().values() ){
				
				if( isColorDeclaration( declaration ) ){
					if ( selector == null ){
						selector = rule.getSelector() ;
						sb.append( selector ).append(" {") ;
					}
					
					sb.append( realProperty ( declaration ) ).append( ':' ) ;
					sb.append( declaration.getValue() ).append( ';' ) ;
				}

			}
			
			if ( selector != null ){
				sb.append( "}\n" ) ;
			}
			
		}
		
		return sb.toString() ;
		
		
	}



	private static boolean isColorDeclaration( Declaration declaration ) {
		return _colorPropertiesList.contains( declaration.getProperty() ) ;
	}
	
	private static String realProperty ( Declaration d ){
		return StringUtils.substringBefore( d.getProperty(), "__" ) ;
	}
	
}
