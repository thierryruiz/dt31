package com.dotemplate.theme.server.css;


import com.dotemplate.core.server.util.ConcurrentTask;
import com.dotemplate.core.server.util.ConcurrentTaskExecutor;
import com.dotemplate.core.server.util.ConcurrentTaskPool;


public class ParseCssTaskExecutor extends ConcurrentTaskExecutor< Stylesheet > {

	public ParseCssTaskExecutor () {
		super ( 50 ) ;
	}

	
	@Override
	protected ConcurrentTaskPool<Stylesheet> createPool () {
		return new ConcurrentTaskPool< Stylesheet >( ) {
			protected ConcurrentTask<Stylesheet> createTask () throws Exception {
				return new ParseCSSTask() ;
			}
		} ;
	}

}
