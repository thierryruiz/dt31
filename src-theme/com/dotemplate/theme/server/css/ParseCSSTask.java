package com.dotemplate.theme.server.css;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.DesignSession;
import com.dotemplate.core.server.DesignTaskContext;
import com.dotemplate.core.server.util.ConcurrentTask;



public class ParseCSSTask extends ConcurrentTask< Stylesheet > {

	@SuppressWarnings("unused")
	private final static Log log = LogFactory.getLog ( ParseCSSTask.class );
	
	SimpleCSSParser cssParser ;
	
	public ParseCSSTask() {
		
		cssParser = new SimpleCSSParser()  ;
		
	}
	
	
	public Stylesheet call () throws Exception {
	
		ParseCSSTask.Context ctx = ( ParseCSSTask.Context ) super.taskContext ;

		DesignSession.set ( ctx.getDesignSession () ) ;
		
		Context context =  ( Context ) super.taskContext ;

		String css = context.getCss().trim() ;
		
		Stylesheet stylesheet ; 
		
		try {
			
			stylesheet = cssParser.parseStyleSheet( css ) ;
			
		} catch (Exception e) {
			
			e.printStackTrace();
		 
			throw new Exception ( "Failed to parse css \n" + css ) ;
		}
		
		return stylesheet  ;

	}
	
	
	
	static class Context extends DesignTaskContext {
		
		String css ;
		
		public String getCss() {
			return css;
		}
		
		public void setCss(String css) {
			this.css = css;
		}
		

		@Override
		public boolean isValid () {
			return true ;
		}
		
	}
	

	
}
