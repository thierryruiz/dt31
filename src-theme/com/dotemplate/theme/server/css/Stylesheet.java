package com.dotemplate.theme.server.css;

import java.io.Serializable;
import java.util.LinkedHashMap;


public class Stylesheet implements Serializable {

	private static final long serialVersionUID = -3988450201124058649L;

	protected LinkedHashMap< String, Rule > rules ;
	
	protected LinkedHashMap< String, Media > medias ;
	
	public Stylesheet() {
		rules = new LinkedHashMap< String, Rule >() ;
		medias = new LinkedHashMap< String, Media >() ;
	}
	
	public void clear() {
		rules.clear() ;
	}
	
	public boolean isEmpty() {
		return rules.isEmpty() ;
	}
	
	
	public LinkedHashMap< String, Rule > getRules() {
		return rules ;
	}

	
	public LinkedHashMap< String, Media > getMedias() {
		return medias ;
	}

	
	public void add( Rule rule ){
		
		Rule current = rules.get( rule.getSelector() ) ; 
		
		if ( current == null ){
			rules.put( rule.getSelector() , rule ) ;
			return ;
		}
		
		current.append ( rule.getDeclarations().values() ) ;
	
	}
	
	
	
	public void add( Media media ){
		
		Media current = medias.get( media.getSelector() ) ; 
		
		if ( current == null ){
			medias.put( media.getSelector() , media ) ;
			return ;
		}
		
		current.append ( media.getRules().values() ) ;
	
	}


}
