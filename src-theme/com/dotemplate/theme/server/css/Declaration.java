package com.dotemplate.theme.server.css;

import java.io.Serializable;


public class Declaration implements Serializable {
	
	private static final long serialVersionUID = 6862445009823535862L;

	private String property ;
	
	private String value ;

	private String jsProperty ;
	
	public Declaration() {
	}
	

	public String getProperty () {
		return property;
	}

	public void setProperty ( String property ) {
		this.property = property;
	}
	
	public String getValue () {
		return value;
	}

	public void setValue ( String value ) {
		this.value = value ;
	}

	
	public String getJsProperty() {
		return jsProperty;
	}

	public void setJsProperty(String jsProperty) {
		this.jsProperty = jsProperty;
	}
	
	
	
}
