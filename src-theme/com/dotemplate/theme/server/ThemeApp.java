package com.dotemplate.theme.server;




import java.io.File;

import com.dotemplate.core.server.App;
import com.dotemplate.core.server.AppConfig;
import com.dotemplate.core.server.RenderEngine;
import com.dotemplate.core.server.symbol.SymbolWatcher;
import com.dotemplate.theme.server.cms.CMS;
import com.dotemplate.theme.server.cms.ExportEngine;
import com.dotemplate.theme.server.cms.ThemeArchiver;
import com.dotemplate.theme.server.navigation.MenuSetFactory;
import com.dotemplate.theme.shared.Theme;


public abstract class ThemeApp extends App {

	public void init( AppConfig config ) {
		init (  System.getProperty ( "user.dir" ) + File.separator, config ) ;	
	}
	
	
	@Override
	public void init( String realPath, AppConfig config ) {
		
		
		super.init( realPath, config );
		
		renderEngine = ( RenderEngine ) getSingleton( config.getRenderEngine() ) ;
		
		designManager = ( ThemeManager ) getSingleton( config.getDesignManager() ) ;
		designManager.init() ;
		


		
		if ( App.isDevMode() ){
			new SymbolWatcher(); 
		}
				
	}
	
	public static ThemeAppConfig getThemeAppConfig() {
		return ( ThemeAppConfig ) getConfig() ;
	}
	
	
	public static ThemeApp get() {
		return ( ThemeApp ) _app ;
	}

	public static ThemeContext getThemeContext() {
		return ( ThemeContext )  getDesignContext();
	}
	
	public static void setThemeContext( ThemeContext ctx ) {
		setDesignContext( ctx );
	}
	
	public static ThemeManager getThemeManager() {
		return ( ThemeManager ) get().designManager ;
	}
	
	public ExportEngine getExportEngine( String cmsId ) {
		return CMS.getExportEngine ( cmsId ) ;
	}

	public static ThemeArchiver getThemeArchiver( String cmsId ) {
		return CMS.getThemeArchiver ( cmsId ) ;
	}

	
	public static void setTheme ( Theme theme ){
		setDesign( theme ) ;
	}
	
	public static Theme getTheme() {
		return ( Theme ) getDesign() ;
	}
	
	public static ThemeFactory getThemeFactory() {
		return ( ThemeFactory ) getSingleton( 
				getConfig().getDesignFactory() ) ;
	}
	
	public static MenuSetFactory getNavigationFactory() {
		return ( MenuSetFactory ) getSingleton( MenuSetFactory.class ) ;
	}
	
	
}
