package com.dotemplate.theme.server;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


public class ExportThemeContext extends ThemeContextWrapper {

	
	@SuppressWarnings("unused")
	private static Log log = LogFactory.getLog( ExportThemeContext.class ) ;
	
	
	public ExportThemeContext ( ThemeContext themeContext ) {
		
		super ( themeContext ) ;
		
		put ( EXPORT, true ) ;
		put ( RANDOM , "" ) ;
		put ( WORK_DIR, ""  ) ;
		put ( VOID_LINK, "" ) ;
		put ( CSS_IMAGE_DIR, "images/css" ) ;
	}

	@Override
	public boolean isExportMode() {
		return true ;
	}
	
	
	public void exportCMSResources() {
	}

	

}
