var casper = require('casper').create();

var site = casper.cli.get('site');

casper.options.viewportSize =  {
    width: 414,
    height: 736
}

casper.userAgent('Mozilla/5.0 (Macintosh; Intel Mac OS X)');

var wait_duration = 10000;

var url = 'http://demo.123clic.com/s/' + site ;

casper.echo( "Capturing screen at " + url ) ;

casper.start(url, function() {
    casper.echo("Page loaded");
});

casper.then(function() {
        casper.wait(wait_duration, function() {
                this.scrollToBottom();
                this.capture('./www/s/' + site + '/smallscreen.jpg',{
                        top:0,
                        left:0,
                        width:414,
                        height:736
                });
        });
});

casper.then(function() {
    this.echo("Exiting");
    this.exit();
});

casper.run();
