var express = require('express');
var bodyParser = require('body-parser');
var http = require('http');
var mime = require('mime');

var app = express();

var config = require("./config.json");

mime.lookup('/res/stripe_dynamic_css(*)');                  // => 'text/plain'
mime.lookup('.TXT');                      // => 'text/plain'
mime.lookup('htm');                       // => 'text/html'

mime.extension('text/html');                 // => 'html'
mime.extension('application/octet-stream');  // => 'bin'


//var router = express.Router();

//router.get('/', function(req, res, next) { 
//  res.render('templates', { templates: templates });
//});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use(function(req, res, next) {
	console.log( req.path ) ;
	if (/_css/.test(req.path)) {
		console.log( "utf-8" ) ;
	    res.charset = "utf-8";
	    res.setHeader('content-type', 'text/css');
	}
	next();
});

app.use(express.static(__dirname + '/www'));


//app.use('/templates', router);

/* serves main page 
app.get("/", function(req, res) {
	 res.sendFile(__dirname + '/index.html');
});
*/

//app.get('/', function(req,res){
//	res.render('index.html', {
//		layout:false,
//	});
//});

app.listen(config.port,function(){
	console.log("App Started on PORT " + config.port);
});

