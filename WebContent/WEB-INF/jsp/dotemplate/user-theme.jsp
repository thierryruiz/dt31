<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ page isELIgnored="false" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<body>

	<script type="text/javascript" language="javascript">
	
	var editorWin ;
	function openEditor () {
	    var w = screen.width - 100 ;
	    var h = screen.height - 40 ;
	    var left = 50 ; 
	    var top = 20 ;
	    var windowArgs = 'height=' + h + ',width=' + w + ',top=' + top + ',left=' + left + ',scrollbars=yes,resizable=yes,location=no';		    
	    editorWin = window.open( "${createUrl}", "editorWindow", windowArgs);
	    editorWin.focus() ;
	    setTimeout( function(){location.href = "http://www.dotemplate.com"}, 10000 );
	    return false ;
	};
	
	</script>


	<style>
	<!--
		.button {
			width:50%;
			margin:20px ;
			cursor:pointer;
		}
		
		a img {
			border:0;
		}
	
	-->
	</style>


	<div style="margin:0 auto">
		
		<c:if test="${downloads < 1}" >
			<p>Sorry you cannot edit your template anymore. You used all your download credits.</p> 
			<p>Contact us if you want to extend your download credits : mail[at]dotemplate.com </p>
		</c:if>
		
		<c:if test="${downloads > 0}" >

			<h3>Thank you for using doTemplate online template builder !</h3>
			
				<p>You have ${downloads} download(s) left.</p>
				
				<p style="margin-top:70px;width:100%;">
					<span class="button">
						<img src="images/dotemplate/edit-user-template.png" onclick="openEditor()" /> 
					</span>
				</p>
		</c:if>
		
	</div>

</body>