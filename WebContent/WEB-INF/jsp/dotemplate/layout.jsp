<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ page isELIgnored="false" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %> 


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

	<head>
	
		<title>Free online web templates builder</title>
		<base href="${base}" />
		<meta http-equiv="Content-Type" content="text/html; charset=UTF8" />
		<meta http-equiv="Cache-Control" content="no-cache"/>
	    <meta http-equiv="Expires" content="-1"/>
		<meta http-equiv="pragma" content="no_cache"/>
		<meta http-equiv="Content-Language" content="en-us" />			
		<link rel="stylesheet" type="text/css" href="style.css?v3.01" />
		
	</head>
	
	<body>
		<div id="wrapper">		
			<div id="centered">		
		 		<decorator:body />
			</div>	
		</div>
	</body>

</html>

