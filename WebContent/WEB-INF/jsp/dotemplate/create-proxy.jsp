<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ page isELIgnored="false" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<body>

	<script type="text/javascript" language="javascript" src="gwt/dtcaptcha/dtcaptcha.nocache.js?v3.01"></script>
	
	<img src="images/splash.jpg" />
	
	<br/>
	
	<p id="loader">
		<img src="images/ajax-loader.gif" />
	<p>
	
	<p id="error" style="display:none">
		Sorry. The server was unable to process your request.
		<br/>
		It may be a temporary problem. Please retry later or contact us at 
		<a href="mailto:mail@dotemplate.com">mail@dotemplate.com</a> to report us the issue. Thank you.
	</p>

</body>
