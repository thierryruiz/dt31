<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%
	String path = request.getContextPath();
	String basePath = "http://"+request.getServerName()+":"+request.getServerPort()+path+"/" ;
%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">


	<head>
		<title>doTemplate - Purchase template</title>
		<base href="<%=basePath%>" />	
		<meta http-equiv="Content-Type" content="text/html; charset=UTF8" />
		<meta http-equiv="Cache-Control" content="no-cache"/>
	    <meta http-equiv="Expires" content="-1"/>
		<meta http-equiv="pragma" content="no_cache"/>
		<meta http-equiv="Content-Language" content="en-us" />
		<link rel="stylesheet" type="text/css" href="style.css" />
	</head>


	<body>


		<div style="text-align:center;margin-top:30px">
	
			<a href='<%=request.getAttribute("zip")%>'>
				<img src="images/dotemplate/download.png" style="border:none" />
			</a>
			
			<div id="download" style="font-size:1.4em;font-weight:bold">
				<a href='<%=request.getAttribute("zip")%>'>Download your template</a>
			</div>
		
			<div style="margin-top:50px">		
				<p>
					If you like this tool, please <strong>DONATE</strong>.
				</p>
				<i>Current donation rate is <strong>only</strong> 1 donation for 2000+ download!</i>
			</div>	
			
			<div style="margin-top:20px;">
				<jsp:include page="/WEB-INF/jsp/dotemplate/donate.jsp" />
			</div>
	
		</div>
	</body>


</html>