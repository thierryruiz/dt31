<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ page isELIgnored="false" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<body>

	<style>
	<!--
		.button {
			width:50%;
			margin:20px ;
			cursor:pointer;
		}
		
		input {
			width:70%;
			padding:5px;
			border:1px solid #CCC;
			background-color:#EEE;
			text-align:center;
		}
		
		a img {
			border:0;
		}
	
	-->
	</style>

	<script type="text/javascript">
	    function select_all(obj) {
	        var text_val=eval(obj);
	        text_val.focus();
	        text_val.select();
	        if (!document.all) return; // IE only
	        r = text_val.createTextRange();
	        r.execCommand('copy');
	    }
	</script>

	<div style="width:80%;margin:0 auto">
		
		<h3>Thank you for your purchase !</h3>
		
			<p>You can edit, save and download your template at the following permanent URL:</p>
			
			<p>
				<input value="${permalink}" onclick="select_all(this)" name="url" type="text" /> 
			</p>
			
			<p>Please, bookmark this adress and keep it personal. <strong>Do not share it as you are limited to 5 downloads.</strong></p>
		
			<p>
				These information have been sent to your Paypal account email <strong>${payerEmail}</strong>.<br/> 
				<em>(If you haven't received this email after few minutes, verify your spam folder)</em>
			</p>
			
			<p style="margin-top:70px;width:100%;">
				<span class="button">
					<a href="${editUrl}">
						<img src="images/dotemplate/continue-editing.png"/> 
					</a>
				</span>	
			</p>
			
	</div>

</body>
