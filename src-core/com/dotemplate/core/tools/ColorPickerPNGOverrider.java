package com.dotemplate.core.tools;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import org.apache.commons.io.FileUtils;



/**
 * 
 * This class is called after GWT compilation to override de color picker PNG
 * genrated by GWT compiler which is not compressed. Replace this file by
 * the compressed one misc/colorpicker.png
 * 
 * 
 *
 */
public class ColorPickerPNGOverrider {

	public static void main ( String[] args ){
		
		System.out.println( "Lookup png in " + args[ 0 ] ) ;
		
		// locate the generated PNG in gwt/dotemplate
		String [] png = { "png"} ;
		
		File gwtDir =  new File ( args[ 0 ] ) ;
		
		Iterator<File> pngs = FileUtils.iterateFiles ( gwtDir, png, true ) ;
		
		// there is multiple png in this folder get the good one from its size
		while( pngs.hasNext () ){
			
			File file= pngs.next () ;
			
			// is this our file size=args [ 2 ] ?
			int size =  Integer.parseInt ( args[ 2 ] ) ;
			
			int fileLength =   ( int) file.length () / 1024 ;
			
			if ( fileLength - 10 < size  && size < fileLength + 10   ){
				
				// found > override
				try {
					
					System.out.println( "Found file " + file.getAbsolutePath() ) ;
					
					FileUtils.copyFile ( new File ( args[1] ) , file ) ;
				
				} catch ( IOException e ) {
					e.printStackTrace();
				}
		
			}
		}
	}
	
	
}
