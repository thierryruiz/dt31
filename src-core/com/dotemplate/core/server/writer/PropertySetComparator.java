package com.dotemplate.core.server.writer;




import org.apache.commons.beanutils.BeanUtils;

import com.dotemplate.core.server.DesignUtils;
import com.dotemplate.core.shared.properties.Property;
import com.dotemplate.core.shared.properties.PropertySet;


public class PropertySetComparator {
	
	
	public PropertySetDiff compare( PropertySet set1 , PropertySet set2 ){
	
		PropertySetDiff diff = new PropertySetDiff( set1.getName(), set1.getType() ) ;

		if ( set2 == null || set2.getParent() == null || !set1.getParent().equals( set2.getParent() ) ){
			set2 = DesignUtils.getSymbol( set1.getParent() ) ;
		}
	
		compareAttributes( set1, set2, diff ) ;
		
		PropertyDiff subDiff = null ;
		
		for ( Property p : set1.getProperties()  ){
			
			if ( p.isSet() ){
				
				subDiff = compare( ( PropertySet ) p , ( PropertySet ) set2.getProperty( p.getName() ) ) ;

			} else {
				subDiff  = compare ( p, set2.getProperty( p.getName() ) ) ;
			}
			
			if ( !subDiff.isEmpty() ){
				diff.add( subDiff ) ;
			}
			
		}
		
		return diff ;
	}
	
	
	
	public PropertyDiff compare( Property p1 , Property p2 ){

		PropertyDiff diff = new PropertyDiff( p1.getName(), p1.getType() ) ;
		
		compareAttributes( p1, p2, diff ) ;
		
		return diff ; 
	
	}
	
	
	
	public void compareAttributes( Property p1 , Property p2, PropertyDiff diff ){
		
		Object a1, a2 ;
		
		try {
			
			if ( p1.isComputed() ){
				return ;
			}
			
			if ( p2 == null ){ // for example new image/text
				
				for ( String da : p1.getDiffAttributes() ){
					
					a1 = BeanUtils.getSimpleProperty( p1, da ) ;
					
					if ( a1 == null ){
						continue ;
					}
					
					diff.add( new Attribute( da , a1.toString() ) ) ;
				
				}
				
				
				
			} else {
			
				
				for ( String da : p1.getDiffAttributes() ){
					
					a1 = BeanUtils.getSimpleProperty( p1, da ) ;
					a2 = BeanUtils.getSimpleProperty( p2, da ) ;
					
										
					if ( a1 == null && a2 == null ){
						continue ;
					}
					
					
					if ( a1 != null && a2 != null &&  a1.toString().equals ( a2.toString() ) ){
						continue ;
					}
					
					diff.add( new Attribute( da , a1.toString() ) ) ;
					
				}
			}
			
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		
		
	}
	

	public void compareAttributes( PropertySet set1 , PropertySet set2, PropertyDiff diff ){

		if( ( set1.getParent() == null && set2.getParent() == null ) 
				|| ( set1.getParent().equals ( set2.getParent() ) ) ){
			;
		} else {
			diff.add( new Attribute( "extends", set1.getParent() ) ) ;
		}
		
		compareAttributes ( ( Property ) set1, ( Property ) set2, diff  ) ;
		
	}



	
	
}
