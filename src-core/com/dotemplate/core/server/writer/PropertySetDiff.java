package com.dotemplate.core.server.writer;

import java.util.ArrayList;

public class PropertySetDiff extends PropertyDiff {

	private ArrayList< PropertyDiff > properties ;
	
	public PropertySetDiff( String name, int type ) {
		
		super( name, type );

		properties = new  ArrayList< PropertyDiff >() ;
		
	}

	
	public void add( PropertyDiff property ){
		properties.add( property ) ;
	}
	
	
	public ArrayList< PropertyDiff > getProperties() {
		return properties;
	}
	
	
	@Override
	public boolean isEmpty() {
		return super.isEmpty() && properties.isEmpty() ;
	}

	
	@Override
	public boolean isSet() {
		return true;
	}
	
}
