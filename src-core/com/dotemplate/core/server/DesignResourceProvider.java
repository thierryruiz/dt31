package com.dotemplate.core.server;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;


import com.dotemplate.core.shared.DesignResource;


public abstract class DesignResourceProvider< T extends DesignResource > {
	
	protected static String ALL_RESOURCES  = "all" ;	

	protected LinkedHashMap< String, LinkedHashMap< String, T > > resourcesMap ;
		
	public DesignResourceProvider() {	
		
		resourcesMap = new LinkedHashMap< String, LinkedHashMap< String, T > >() ;		
		resourcesMap.put( ALL_RESOURCES , new LinkedHashMap< String, T >() );
		
	}
	
	
	public abstract void load () ;
	
	
	protected void sort() {
	}

	
	
	public boolean isEmpty(){
		return resourcesMap.get( ALL_RESOURCES ).isEmpty() ;
		
	}
	
	public T get( String name ){
		return resourcesMap.get( ALL_RESOURCES ).get ( name );
	}
	
	public boolean isFree ( String name ){
		return !resourcesMap.get( ALL_RESOURCES ).get (  name ).isPremium() ;
	}

	
	public ArrayList< T > list() {
		return new ArrayList< T >   ( resourcesMap.get( ALL_RESOURCES ).values () ) ; 
	}

	
	public ArrayList< T > list( String tag ) {
		
		if ( tag != null ){
			if( resourcesMap.get( tag ) != null ){
				return new ArrayList< T > ( resourcesMap.get( tag ).values () )  ;
			} else {
				return new ArrayList< T >() ;
			}
		} else {
			
			return list() ;
		}
		
		
//		return tag != null ?  new ArrayList< T > ( resourcesMap.get( tag ).values () )  :
//			list() ;

	}

	
	public LinkedHashMap< String, LinkedHashMap< String, T > > getResourcesMap() {
		return resourcesMap;
	}

	
	public String[] getResourcesTags(){
		
		String[] tags = new String[ resourcesMap.size() ] ;
		
		ArrayList< String > tagList = new ArrayList<  String > ( resourcesMap.keySet() ) ;
		
		Collections.sort( tagList ) ;
		
		tagList.toArray( tags ) ;
		
		
		return tags ;
		
	}
	
	
	
	
}
