package com.dotemplate.core.server;

import java.io.File;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.affiliate.AffiliateManager;
import com.dotemplate.core.server.util.Path;
import com.dotemplate.core.server.util.Singletons;
import com.dotemplate.core.shared.Design;


public abstract class App {
	
	@SuppressWarnings("unused")
	private static Log log = LogFactory.getLog ( App.class );
	
	protected static App _app ;
	
	protected RenderEngine renderEngine ;
	
	protected AppConfig config ;
	
	protected String realPath ;

	protected DesignManager<? extends Design> designManager ;
	
	protected AffiliateManager affiliateManager ;
		
	
	public App(){
		_app = this ;
	}
	
	public static App get() {
		if ( _app == null ){
			throw new IllegalStateException( "Application not initialized" ) ;
		}
		return _app ;
	}
	
	
	public void init( AppConfig config ) {
		init ( System.getProperty ( "user.dir" ) + File.separator, config ) ;
	}
	
	
	public void init ( String realPath, AppConfig config ) {
		this.config = config ;
		this.realPath = realPath ;
	}

	
	public void init () {
		this.config = ( new AppConfigReader< AppConfig >( AppConfig.class ) ).read( getConfigFileName() ) ;	
	}

	
	protected abstract String getConfigFileName() ;
	

	public static String realPath ( String fileName ) {
		return ( _app.realPath + File.separator + fileName.trim () ).replace(
				"/", File.separator ) ;
	}
	
	
	public static AppConfig getConfig() {
		return _app.config;
	}
		
	
	public String getWorkUri () {
		return new Path ( getConfig ().getWorkRoot () ).append (
				getDesignUid() ).asUrl();
	}
	
	
	public String getWorkPath () {
		return new Path ( getConfig ().getWorkRoot () ).append (
				getDesignUid() ).toString();
	}

	public String getWorkPath ( String designUid ) {
		return new Path ( getConfig ().getWorkRoot () ).append (
				designUid ).toString();
	}

	
	public String getWorkRealPath () {
		return realPath ( get().getWorkPath () );
	}

	
	public String getWorkRealPath ( String designUid ) {
		return realPath ( get().getWorkPath ( designUid ) );
	}

	
	public String getExportDirectoryRealPath () {
		return realPath ( new Path ( getConfig ().getWorkRoot () ).append (
				getDesignUid () ).append( "download" ).toString () );
	}

	
	public String getUploadDirectoryRealPath ( String designUid ) {
		return realPath ( new Path ( get().getWorkPath ( designUid ) ).append( "upload" ).toString () );
	}

	
	public static void setDesignContext ( DesignContext<? extends Design> ctx ) {
		DesignSession.get().setDesignContext ( ctx ) ;
	}

	
	public static DesignContext<? extends Design> getDesignContext () {
		DesignContext<? extends Design> context =  DesignSession.get().getDesignContext () ;
		if ( context == null ){
			throw new IllegalStateException ( "No DesignContext object in session" ) ;
		}
		
		return context ;
	}
	
	
	public static void setDesign ( Design design ){
		DesignSession.setDesign ( design ) ;
	}
	
	public static Design setDesign ( String uid ){
		return DesignSession.setDesign ( uid ) ;
	}
	
	public static Design getDesign (){
		return DesignSession.getDesign () ;
	}

	public static String getDesignUid (){
		return DesignSession.getDesign ().getUid() ;
	}
	
	public void terminate() {
		//getStatisticsMgr ().saveStatistics () ;
	}

	public static Object getSingleton( Class<?> clazz ) {
		return Singletons.getSingleton( clazz );
	}
	
	public static Object getSingleton( String className ) {
		return Singletons.getSingleton( className );
	}
		
	public static boolean isDevMode () {
		return ( getConfig () != null  && getConfig ().isDevMode () ) ;
	}
	
	
	public static RenderEngine getRenderEngine() {
		return ( RenderEngine ) get().renderEngine ;
	}
	
	public static DesignManager<? extends Design> getDesignManager() {
		return get().designManager ;
	}
	
	public static AffiliateManager getAffiliateManager() {
		return ( AffiliateManager ) getSingleton( AffiliateManager.class );
	}
	
	

	
}
