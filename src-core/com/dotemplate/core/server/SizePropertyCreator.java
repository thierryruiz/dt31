package com.dotemplate.core.server;

import java.util.ArrayList;

import org.apache.commons.betwixt.io.read.BeanCreationChain;
import org.apache.commons.betwixt.io.read.ElementMapping;
import org.apache.commons.betwixt.io.read.ReadContext;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.frwk.AppException;
import com.dotemplate.core.shared.properties.SizeProperty;
import com.google.common.primitives.Ints;


public class SizePropertyCreator implements PropertyCreator<SizeProperty> {

	private final static Log log = LogFactory.getLog ( SizePropertyCreator.class ) ;
	
	
	public SizeProperty create( ElementMapping mapping, ReadContext context,
			BeanCreationChain chain ) {
		
		SizeProperty size = ( SizeProperty ) chain.create( mapping, context ); 
		
		// resolve range value
		// it can be like "1..10,1" or "1,2,3,4,5" 
		
		String range = mapping.getAttributes().getValue( "range" ) ;
		
		if ( range != null ){

			if ( log.isDebugEnabled() ){
				log.debug ( "Parsing size range="  + range  ) ;
			}
			
			try {

				int[] v = ( range.indexOf( ".." ) > 0 ) ? 
						getComputedValues( range ) :
							getDefinedValues( range ) ;
				
				size.setAllowedValues( v ) ;
				
				size.setMin( v[0] ) ;
				size.setMax( v[ v.length -1 ] ) ;
				
				
				

			} catch ( Exception e ) {
				
				log.error  ( e ) ;
				
				log.warn( "Unable to parse size range '" +
						range + "' of property " + mapping.getAttributes().getValue("name"), e ) ;
				
				
				size.setAllowedValues( new int[0] );
				
			}
		}
		
		return size;
	

	}
	
	public int[] getDefinedValues( String rangeAsString ){
		// like "1,2,3,4,5" 
		String[] tok = StringUtils.split( rangeAsString, ',' ) ;
		
		int [] values = new int[ tok.length ] ;
		
		for ( int i = 0 ; i < tok.length ; i++ ){
			log.debug( tok [ i ] ) ;
			values [ i ] = Integer.parseInt( tok [ i ] ) ;
		}
		return values;
	}
	
	
	
	public int[] getComputedValues(  String rangeAsString ) throws Exception{
		// like  be like "1..10,1" 
		
		String[] tok = StringUtils.split( rangeAsString, "," ) ;
		
		if ( tok.length != 2 ) {
			throw new AppException( "Not a valid range" ) ;
		}
		
		int step = Integer.parseInt( tok[ 1 ]) ;
		
		tok = StringUtils.split( tok[ 0 ], ".." ) ;
		
		int from = Integer.parseInt( tok[ 0 ]) ;
		int to = Integer.parseInt( tok[ 1 ]) ;
		
		ArrayList< Integer > valuesAsList = new ArrayList< Integer >() ;
		
		
		for ( int i = from ; i <= to  ; i = i + step  ){
			valuesAsList.add( i ) ;
		}
		
		return Ints.toArray( valuesAsList );
		
	}
	
	
	
	

}
