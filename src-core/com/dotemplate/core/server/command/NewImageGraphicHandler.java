package com.dotemplate.core.server.command;

import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.net.URL;

import javax.imageio.ImageIO;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.App;
import com.dotemplate.core.server.frwk.AppException;
import com.dotemplate.core.server.frwk.CommandHandler;
import com.dotemplate.core.server.util.Path;
import com.dotemplate.core.server.util.UIDGenerator;
import com.dotemplate.core.shared.Dimension;
import com.dotemplate.core.shared.canvas.ImageGraphic;
import com.dotemplate.core.shared.canvas.ImageResource;
import com.dotemplate.core.shared.command.NewGraphicResponse;
import com.dotemplate.core.shared.command.NewImageGraphic;
import com.dotemplate.core.shared.frwk.Command;
import com.dotemplate.core.shared.frwk.WebException;
import com.dotemplate.core.shared.properties.ImageProperty;


public class NewImageGraphicHandler extends CommandHandler< NewGraphicResponse<ImageGraphic> > {

	private static Log log = LogFactory.getLog ( NewImageGraphicHandler.class );

	
	@Override
	public NewGraphicResponse<ImageGraphic> handleAction ( Command< NewGraphicResponse<ImageGraphic> > a )
							throws WebException {
		
		NewGraphicResponse<ImageGraphic> response = new NewGraphicResponse< ImageGraphic >() ;

		NewImageGraphic action = ( NewImageGraphic ) a ; 
		
		ImageProperty property = new ImageProperty() ;

		property.setUid ( UIDGenerator.pickId () ) ;
		
		property.setEditable ( true ) ;
		property.setEnable ( true ) ;
		property.setFree ( true ) ;
		property.setOpacity ( ( float ) 1.0 ) ;
		property.setBlur ( 0  ) ;
		property.setName ( "image" + RandomStringUtils.randomNumeric ( 5 ) ) ;
		property.setLabel( "New Image" ) ;
		property.setLeft ( 0 ) ;
		property.setTop ( 0 ) ;
		property.setScale ( ( float ) 1.0 ) ;
		property.setUploadable(true) ;
		property.setVisible(true);
		
		property.setTopFading ( ( float ) 0 )  ;
		property.setBottomFading ( ( float ) 0 )  ;
		property.setRightFading ( ( float ) 0 ) ;
		property.setLeftFading ( ( float ) 0 ) ;
		
		String uploadFilename = action.getUploadFilename () ;
		
		String stockImageUrl = action.getStockImage () ;
		
		
		if ( uploadFilename != null ) {

			if ( log.isDebugEnabled () ){
				log.debug ( "Creating a new image property from upload image  " 
										+ uploadFilename ) ;
			}	
			
			Path uploadPath = new Path ( App.get().getWorkUri() ).append( "upload" ).append (  uploadFilename ) ;
			
			property.setValue (  uploadPath .asUrl() )  ;	
			
			Dimension d = getUploadedImageDimension( uploadFilename ) ;
			
			property.setWidth ( d.getWidth () ) ;
			property.setHeight ( d.getHeight () ) ;
			
		} else if ( stockImageUrl != null ){ 
			
			if ( log.isDebugEnabled () ){
				log.debug ( "Creating a new image property from stock image  " 
										+ stockImageUrl ) ;
			}	
		
			property.setValue (  stockImageUrl )  ;	
			
			Dimension d = getStockImageDimension( stockImageUrl ) ;
			
			property.setWidth ( d.getWidth () ) ;
			property.setHeight ( d.getHeight () ) ;
			
			
		} else {

			ImageResource themeImage = action.getImage () ;

			if ( log.isDebugEnabled () ){
				log.debug ( "Creating a new image property from theme image  " 
										+ themeImage.getPath () ) ;
			}	
			
			property.setValue ( themeImage.getPath () ) ;
			property.setWidth ( themeImage.getWidth () ) ;
			property.setHeight ( themeImage.getHeight () ) ;
			property.setFree( themeImage.isFreeResource() ) ;
			
		}
		
		try {
		
			App.getDesignManager ().renderGraphic( property ) ;
		
		} catch ( AppException e ) {
			
			throw syserror ( "Unable to create image..." ) ;
		
		}
		
		response.setGraphic ( property ) ; 
		
		return response ;
		
	}
	

	
	private Dimension getUploadedImageDimension( String fileName ) throws WebException {
		
		if ( log.isDebugEnabled () ) {
			log.debug ( "Get uploaded image  dimension '" +  fileName + "'" );
		}
		
		Path uploadPath = new Path ( App.get().getWorkUri() ).append( "upload" ).append (  fileName ) ;
		
		String path = App.realPath( uploadPath.toString() )  ;  
		
		BufferedImage image = null;
		
		try {
			image = ImageIO.read ( new FileInputStream ( path )  );
		} catch ( Exception e ) {
			log.warn ( "Cannot read image " + path , e ) ;
			throw new WebException( "This image cannot be read" ) ;
		}
		
		Dimension d = new Dimension ();
		d.setWidth ( image.getWidth () );
		d.setHeight ( image.getHeight () );
		
		return d ;
	
	}
	
	
	private Dimension getStockImageDimension( String url ) throws WebException {
		
		if ( log.isDebugEnabled () ) {
			log.debug ( "Get image  dimension from url '" +  url + "'" );
		}
		
		BufferedImage image = null;
		
		try {
			image = ImageIO.read ( new URL( url ) );
		} catch ( Exception e ) {
			log.error ( "Cannot read image " + url  , e ) ;
			System.out.println( url ) ;
			e.printStackTrace();
			throw new WebException( "This image cannot be read ") ;
		}
		
		Dimension d = new Dimension ();
		d.setWidth ( image.getWidth () );
		d.setHeight ( image.getHeight () );
		
		return d ;
	
	}
	

}
