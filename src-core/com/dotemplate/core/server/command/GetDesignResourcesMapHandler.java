package com.dotemplate.core.server.command;

import java.util.LinkedHashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.frwk.CommandHandler;
import com.dotemplate.core.shared.DesignResource;
import com.dotemplate.core.shared.command.GetDesignResourcesMapResponse;
import com.dotemplate.core.shared.frwk.Command;
import com.dotemplate.core.shared.frwk.WebException;


public abstract class GetDesignResourcesMapHandler< RESOURCE extends DesignResource >
		extends CommandHandler< GetDesignResourcesMapResponse< RESOURCE > > {

	
	@SuppressWarnings("unused")
	private static Log log = LogFactory.getLog( GetDesignResourcesMapHandler.class );

	
	@Override
	public GetDesignResourcesMapResponse< RESOURCE > handleAction(
			Command< GetDesignResourcesMapResponse< RESOURCE > > action )
			throws WebException {
		
		GetDesignResourcesMapResponse< RESOURCE > response = new GetDesignResourcesMapResponse< RESOURCE >();
		
		response.setResourcesMap( getDesignResourcesMap( action ) ) ;
		
		return response;
	
	}

	
	protected abstract LinkedHashMap< String, LinkedHashMap< String, RESOURCE > > getDesignResourcesMap( 
			Command< GetDesignResourcesMapResponse< RESOURCE > > action );

	
}
