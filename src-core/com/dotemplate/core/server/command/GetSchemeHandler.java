package com.dotemplate.core.server.command;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


import com.dotemplate.core.server.App;
import com.dotemplate.core.server.frwk.CommandHandler;
import com.dotemplate.core.shared.command.GetScheme;
import com.dotemplate.core.shared.command.GetSchemeResponse;
import com.dotemplate.core.shared.frwk.Command;
import com.dotemplate.core.shared.frwk.WebException;
import com.dotemplate.theme.server.symbol.ColorSchemeProvider;


public class GetSchemeHandler extends CommandHandler< GetSchemeResponse > {

	private static Log log = LogFactory.getLog ( GetSchemeHandler.class );

	
	@Override
	public GetSchemeResponse handleAction ( Command< GetSchemeResponse > command ) throws WebException {
				
	
		if ( log.isDebugEnabled () ) {
			
			log.debug ( "getting scheme  '" + ( ( GetScheme ) command ).getName() + "'"  ) ;
		
		}
		
		GetSchemeResponse response = new GetSchemeResponse() ;
		
		String schemeName = ( ( GetScheme ) command ).getName() ; 
		
		response.setScheme ( ( ( ColorSchemeProvider ) App.getSingleton( 
				ColorSchemeProvider.class )).get( schemeName ) ) ;
		
		return response ;
		
	}
	
	
}
