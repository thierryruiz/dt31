package com.dotemplate.core.server.command;


import java.util.LinkedHashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.App;
import com.dotemplate.core.shared.SymbolType;
import com.dotemplate.core.shared.command.GetDesignResourcesMapResponse;
import com.dotemplate.core.shared.command.GetSymbols;
import com.dotemplate.core.shared.frwk.Command;
import com.dotemplate.core.shared.properties.PropertySet;


public class GetSymbolsHandler extends GetDesignResourcesMapHandler< PropertySet > {

	
	private static Log log = LogFactory.getLog ( GetSymbolsHandler.class );

	
	@Override
	protected LinkedHashMap< String, LinkedHashMap< String, PropertySet > > getDesignResourcesMap(
			Command< GetDesignResourcesMapResponse <PropertySet > > action ) {
		
		GetSymbols getSymbols = ( GetSymbols ) action ;
		
		SymbolType type = getSymbols.getType () ;
		
		if ( log.isDebugEnabled () ) {
			log.debug ( "Get symbols  '" + type.getFolder () + " + filter:'+ " + getSymbols.getFilter() + "' "  ) ;
		}
		
		
		return App.getDesignManager ().getSymbols ( type, getSymbols.getFilter () );
	}
	
		
	
	
	
}
