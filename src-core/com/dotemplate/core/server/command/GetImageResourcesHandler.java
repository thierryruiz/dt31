package com.dotemplate.core.server.command;

import java.util.ArrayList;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.App;
import com.dotemplate.core.server.ImageResourceProvider;
import com.dotemplate.core.server.command.GetDesignResourcesHandler;
import com.dotemplate.core.server.command.GetImageResourcesHandler;
import com.dotemplate.core.shared.canvas.ImageResource;
import com.dotemplate.core.shared.command.GetDesignResourcesResponse;
import com.dotemplate.core.shared.command.GetImageResources;
import com.dotemplate.core.shared.frwk.Command;

public class GetImageResourcesHandler extends GetDesignResourcesHandler< ImageResource > {

	private static Log log = LogFactory.getLog ( GetImageResourcesHandler.class );

	
	@Override
	protected ArrayList< ImageResource > getDesignResources( Command< GetDesignResourcesResponse< ImageResource > > command ) {
		
		if ( log.isDebugEnabled () ){
			log.debug ( "Getting image resources..." ) ;
		}		
		
		GetImageResources getImageResources = ( GetImageResources ) command ;
		
		
		return ( ( ImageResourceProvider ) App.getSingleton ( ImageResourceProvider.class ) )
			.list( getImageResources.getTag() );
		
		
	}
					

}
