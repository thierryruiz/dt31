package com.dotemplate.core.server.command;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.App;
import com.dotemplate.core.server.frwk.CommandHandler;
import com.dotemplate.core.shared.SymbolType;
import com.dotemplate.core.shared.command.GetDesignResourcesMapResponse;
import com.dotemplate.core.shared.command.GetSymbols;
import com.dotemplate.core.shared.command.GetSymbolsResponse;
import com.dotemplate.core.shared.frwk.Command;
import com.dotemplate.core.shared.frwk.WebException;
import com.dotemplate.core.shared.properties.PropertySet;

@Deprecated
public class GetSymbolsHandlerDeprecated extends CommandHandler< GetDesignResourcesMapResponse< PropertySet > > {

	
	private static Log log = LogFactory.getLog ( GetSymbolsHandlerDeprecated.class );

	
	@Override
	public GetSymbolsResponse handleAction ( Command< GetDesignResourcesMapResponse< PropertySet > > action ) throws WebException {

		GetSymbolsResponse response = new GetSymbolsResponse() ;
		
		GetSymbols getSymbols = ( GetSymbols ) action ;
		
		SymbolType type = getSymbols.getType () ;
		
		if ( log.isDebugEnabled () ) {
			log.debug ( "Get symbols  '" + type.getFolder () + " + filter:'+ " + getSymbols.getFilter() + "' "  ) ;
		}
		
		response.setResourcesMap( App.getDesignManager ().getSymbols ( type, getSymbols.getFilter () ) ) ;
		
		
		/*
		ArrayList< PropertySet > symbols =  App.getDesignManager ().getSymbols ( type, getSymbols.getFilter () ) ;
		
		
		symbols = new ArrayList< PropertySet > ( Collections2.filter ( symbols, new Predicate< PropertySet >() {
			@Override
			public boolean apply ( PropertySet s ) {
				return ! StringUtils.contains ( s.getName (), "abstract" );
			}
		})) ;
		*/
	
		
		return response ;
		
	}
	
	
	
	
}
