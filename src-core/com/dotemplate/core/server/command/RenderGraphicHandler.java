package com.dotemplate.core.server.command;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.App;
import com.dotemplate.core.server.command.RenderGraphicHandler;
import com.dotemplate.core.server.frwk.CommandHandler;
import com.dotemplate.core.shared.canvas.Graphic;
import com.dotemplate.core.shared.command.RenderGraphic;
import com.dotemplate.core.shared.command.RenderGraphicResponse;
import com.dotemplate.core.shared.frwk.Command;
import com.dotemplate.core.shared.frwk.WebException;



public class RenderGraphicHandler extends CommandHandler< RenderGraphicResponse > {

	
	private static Log log = LogFactory.getLog ( RenderGraphicHandler.class );
	

	@Override
	public RenderGraphicResponse handleAction ( Command< RenderGraphicResponse > action )
							throws WebException {
		
		if( log.isDebugEnabled () ){
			log.debug ( "Rendering graphic..." ) ;
		}
		
		
		Graphic graphic = ( ( RenderGraphic ) action  ).getGraphic () ;
		
		RenderGraphicResponse response = new RenderGraphicResponse() ;
		
		try {
			
			App.getDesignManager ().renderGraphic ( graphic ) ;
			
			response.setGraphic ( graphic ) ;
					
		} catch ( Exception e ) {
			
			throw syserror ( "Failed to render image", e ) ;
		}
		
		return response ;
	
	}
	
				

}
