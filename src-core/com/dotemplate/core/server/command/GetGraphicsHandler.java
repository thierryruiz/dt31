package com.dotemplate.core.server.command;

import java.util.ArrayList;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.App;
import com.dotemplate.core.server.DesignUtils;
import com.dotemplate.core.server.frwk.CommandHandler;
import com.dotemplate.core.shared.canvas.Graphic;
import com.dotemplate.core.shared.command.GetGraphics;
import com.dotemplate.core.shared.command.GetGraphicsResponse;
import com.dotemplate.core.shared.frwk.Command;
import com.dotemplate.core.shared.frwk.WebException;
import com.dotemplate.core.shared.properties.PropertySet;



public class GetGraphicsHandler extends CommandHandler< GetGraphicsResponse > {

	private static Log log = LogFactory.getLog ( GetGraphicsHandler.class );
	
	
	@Override
	public GetGraphicsResponse handleAction ( Command< GetGraphicsResponse > action )
							throws WebException {
		
		if( log.isDebugEnabled () ){
			log.debug ( "Rendering graphics..." ) ;
		}
		
		
		GetGraphicsResponse response = new GetGraphicsResponse() ;
	
		ArrayList< Graphic > graphics ; 

		try {
			
			PropertySet set = ( PropertySet ) DesignUtils.findPropertyByUid ( 
					App.getDesign (), ( ( GetGraphics ) action ).getDrawableId () );
			
			graphics =  App.getDesignManager ().renderGraphics ( set ) ;
			
			//DesignLogger.log( set ) ;
			
			response.setGraphics ( graphics ) ;
		
			
		} catch ( Exception e ) {
			throw syserror ( "Failed to render graphics !" , e ) ;
		}
		
		
		if( log.isDebugEnabled () ){
			log.debug ( "Done rendering graphics..." ) ;
		}
		
		
		return response ;
	}
	
				

}
