package com.dotemplate.core.server.command;


import java.util.LinkedHashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.App;
import com.dotemplate.core.server.SVGFontProvider;
import com.dotemplate.core.shared.SVGFont;
import com.dotemplate.core.shared.command.GetDesignResourcesMapResponse;
import com.dotemplate.core.shared.frwk.Command;


public class GetSVGFontsHandler extends GetDesignResourcesMapHandler< SVGFont > {

	
	private static Log log = LogFactory.getLog ( GetSVGFontsHandler.class );


	@Override
	protected LinkedHashMap<String, LinkedHashMap< String, SVGFont > > getDesignResourcesMap(
			Command<GetDesignResourcesMapResponse<SVGFont>> action) {
		
		if ( log.isDebugEnabled () ) {
			log.debug ( "Get SVG fonts..." ) ;
		}
		
		return ( ( SVGFontProvider ) App.getSingleton ( SVGFontProvider.class ) ).getResourcesMap();
	
	}
	
	
	
	
}
