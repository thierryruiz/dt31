package com.dotemplate.core.server.symbol.logic;

public abstract class LogicalExpression extends BooleanExpression {

	protected BooleanExpression left ;
	
	protected BooleanExpression right ;


	public LogicalExpression ( BooleanExpression left, BooleanExpression right ) {
		this.left = left ;
		this.right = right ;
	}
	
	
	static LogicalExpression parseLogical ( String exp ){
		
		AndExpression andExp = AndExpression.parseAnd( exp ) ;
		if ( andExp != null ) return andExp ; 
		
		OrExpression orExp = OrExpression.parseOr( exp ) ;
		if ( orExp != null ) return orExp ; 
		
		return null;
		
	}
	
	

}
