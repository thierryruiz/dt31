package com.dotemplate.core.server.symbol.logic;


public class ValueExpression extends UnitaryExpression {
	
	private String value ;
	
	public ValueExpression ( String value ) {
		this.value = value ;
	}
	
	public String getValue () {
		return value;
	}
	
	static ValueExpression parseValue ( String exp ){
		return new ValueExpression( exp ) ;
	}

}
