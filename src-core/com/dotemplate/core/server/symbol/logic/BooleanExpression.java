package com.dotemplate.core.server.symbol.logic;

import com.dotemplate.core.shared.properties.PropertySet;


public abstract class BooleanExpression extends Expression {
	
	
	public static BooleanExpression parseBoolean( String exp ) {
		
		LogicalExpression logExp = LogicalExpression.parseLogical( exp ) ;
		if ( logExp != null ) return logExp ;
		
		
		// Expression exp 
		EqualityExpression eqExp = EqualityExpression.parseEquality ( exp ) ;
		if ( exp != null ) return eqExp ;
		
				
		return null ;

	}
	
	public abstract boolean eval ( PropertySet set ) ;
	

}
