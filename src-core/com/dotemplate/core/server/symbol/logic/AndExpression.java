package com.dotemplate.core.server.symbol.logic;

import org.apache.commons.lang.StringUtils;

import com.dotemplate.core.shared.properties.PropertySet;

public class AndExpression extends LogicalExpression {
	
	
	public AndExpression ( BooleanExpression left, BooleanExpression right ) {
		super( left, right ) ;
	}
	
	
	static AndExpression parseAnd( String expr ){
		
		String[ ] tokens = StringUtils.split ( expr, '&' ) ;
		
		if ( tokens == null ||  tokens.length != 2 ){
			return null ;
		}
		
		return new AndExpression ( 
				BooleanExpression.parseBoolean ( tokens[ 0 ].trim () ) ,
				BooleanExpression.parseBoolean ( tokens[ 1 ].trim ()))  ; 
	}
	
	
	
	@Override
	public boolean eval ( PropertySet set )  {
		return left.eval (  set ) && right.eval ( set ) ;
	}
	

}
