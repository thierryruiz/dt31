package com.dotemplate.core.server.symbol;

import java.io.File;
import java.nio.file.Paths;

import org.apache.commons.lang.StringUtils;

import com.dotemplate.core.server.App;
import com.dotemplate.core.server.DesignUtils;
import com.dotemplate.core.server.frwk.AppRuntimeException;
import com.dotemplate.core.server.watcher.DirWatcher;
import com.dotemplate.core.server.watcher.FileUpdateListener;
import com.dotemplate.core.shared.properties.PropertySet;

public class SymbolWatcher implements FileUpdateListener {
	
	private DirWatcher watcher ;
	
	public SymbolWatcher() {
		
		try {
			
			watcher = new DirWatcher( Paths.get( App.realPath ( "WEB-INF/templates/_symbols" )), true, this ) ;
			
			Thread th = new Thread(watcher, "SymbolWatcher");
	        th.start();

			
		} catch (Exception e) {
			throw new AppRuntimeException("Failed to create symbols watcher", e  ) ;
		}
	}
	
	
	@Override
	public void onFileUpdated(String path) {
		
		if ( ! path.endsWith("symbol.xml") ) {
			return ;
		}
		
		refreshSymbolProvider( path ) ;
						
	}
	
	@Override
	public void onFileDeleted(String path) {
	}
	
	
	@Override
	public void onFileCreated(String path) {
		if ( ! path.endsWith("symbol.xml") ) {
			return ;
		}
		
		refreshSymbolProvider( path ) ;
		
	}
	

	
	
	protected void refreshSymbolProvider( String path ){
		
		System.out.println("(Re)loading symbol " + path );
		
		String folder = StringUtils.substringBetween( path, "_symbols" + File.separator, File.separator ) ;
		
		SymbolProvider provider = DesignUtils.getSymbolProvider( folder ) ;
		
		PropertySet symbol = provider.createSymbolFromDescriptor( new File ( path ) ) ;
		
		System.out.println("Symbol " + symbol.getPath()  + " (re)loaded.");
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	

}
