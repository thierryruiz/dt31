package com.dotemplate.core.server.symbol;


import com.dotemplate.core.shared.SymbolType;


public class TextureProvider extends SymbolProvider {
	
	@Override
	protected SymbolType getType () {
		return SymbolType.TEXTURE;
	}


	
}
