package com.dotemplate.core.server.symbol;

import java.io.File;

import java.io.InputStream;

import org.apache.commons.betwixt.io.BeanReader;
import org.apache.commons.betwixt.io.read.BeanCreationList;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.xml.sax.InputSource;

import com.dotemplate.core.server.DesignCreator;
import com.dotemplate.core.server.frwk.AppException;
import com.dotemplate.core.shared.Design;
import com.dotemplate.core.shared.properties.PropertySet;



public class SymbolReader {

	private final static Log log = LogFactory.getLog ( SymbolReader.class ) ;
	
	
	public PropertySet read( File xmlFile ) throws AppException {
		
		if ( log.isDebugEnabled () ){
			try {
				log.debug (  "Reading symbol descriptor " + xmlFile.getName() ) ;
			} catch ( Exception e ) {
				e.printStackTrace();
			}
		}
		
		PropertySet symbol ;
		
		BeanReader beanReader = new BeanReader();
		BeanCreationList chain = BeanCreationList.createStandardChain();
		
	    chain.insertBeanCreator(1,  new DesignCreator() );
	    
		beanReader.getReadConfiguration().setBeanCreationChain( chain );

		beanReader.getXMLIntrospector().getConfiguration().setAttributesForPrimitives( true ) ;
		InputStream betwixt = Design.class.getClassLoader ().getResourceAsStream ( "theme.betwixt" ) ;
		
		
		try {
			beanReader.registerMultiMapping( new InputSource( betwixt ) ) ;
		} catch ( Exception e ) {
			log.error (  e ) ;
			throw new AppException( "Failed to initialize SymbolReader", e ) ;
		}		
				
		try {
			
			symbol =  ( PropertySet ) beanReader.parse( xmlFile ) ;
			
		} catch ( Exception e ) {
			log.error (  e ) ;
			throw new AppException( "Failed to read symbol from descriptor " + xmlFile.getName() , e ) ;
		}
		
		if ( log.isDebugEnabled () ){
			log.debug ( "Symbol read" ) ;
		}
		
		return symbol ;
		
	}
		
	
	
}
