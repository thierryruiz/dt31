package com.dotemplate.core.server.symbol;

import com.dotemplate.core.server.frwk.AppRuntimeException;
import com.dotemplate.core.server.symbol.logic.BooleanExpression;
import com.dotemplate.core.shared.properties.PropertySet;


public class SymbolFilter {

	private BooleanExpression expression ; 
	
	
	public SymbolFilter ( String exp ) {
		if( exp != null & exp.length() > 0 ){
			expression = BooleanExpression.parseBoolean ( exp ) ;
		
			if ( expression == null ){
				throw new AppRuntimeException( "Enable to parse criteria expression '" + exp + "'" ) ;
			}
		}		
	}
	
	
	public boolean match ( PropertySet set ) throws AppRuntimeException {
		if ( expression == null ) return true ;
		return expression.eval ( set ) ;
	}
	

	

}
