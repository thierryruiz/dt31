package com.dotemplate.core.server.symbol;

import java.awt.Rectangle;
import java.io.File;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.RandomUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.context.Context;

import com.dotemplate.core.server.App;
import com.dotemplate.core.server.DesignContext;
import com.dotemplate.core.server.DesignHelper;
import com.dotemplate.core.server.DesignResourceProvider;
import com.dotemplate.core.server.DesignUtils;
import com.dotemplate.core.server.RenderEngine;
import com.dotemplate.core.server.frwk.AppRuntimeException;
import com.dotemplate.core.server.random.RandomCategoryContexts;
import com.dotemplate.core.server.random.RandomContext;
import com.dotemplate.core.server.svg.PNGTranscoder;
import com.dotemplate.core.server.util.Path;
import com.dotemplate.core.shared.Design;
import com.dotemplate.core.shared.SymbolType;
import com.dotemplate.core.shared.properties.PropertySet;


public abstract class SymbolProvider extends DesignResourceProvider< PropertySet > {
	
	private final static Log log = LogFactory.getLog ( SymbolProvider.class );
		
	private final static String THUMBNAIL_SVG = "th.svg.vm" ;

	protected final int THUMBNAIL_WIDTH = 70 ;
	
	protected final int THUMBNAIL_HEIGHT = 50 ;	
	
	protected RandomCategoryContexts randomCategoryContexts ;
	
	protected HashMap< String, RandomContext > randomStyleContexts ;
	
	
	public void load () {
		
		if ( log.isDebugEnabled () ){
			
			log.debug ( "Loading symbols " + this.getClass ().getName() ) ;
		
		}
		
		String [] exts = { "xml" } ; 
		
		
		File symbolFolder = new File ( App.realPath ( App.getConfig().getVmTemplatesDir() + "/_symbols/" + 
				getType().getFolder () ) ) ;
		
		if ( ! symbolFolder.exists() ){
			log.warn( "Ignore loading symbol " +  symbolFolder.getAbsolutePath() + " does not exists" ) ;
			return ;
		}
		
		
		Iterator< ? > descriptors = FileUtils.iterateFiles (symbolFolder  , exts , true ) ;
		
		File desc ;

		if ( log.isDebugEnabled () ){
			log.debug (  "Loading Symbols " + getType().getFolder () ) ;
		}
		
		
		while ( descriptors.hasNext () ){
			
			desc = ( File ) descriptors.next () ;
			
			if ( desc.getAbsolutePath().indexOf( "deprecated" ) > -1 ) {
				continue ;
			}
			
			if ( log.isDebugEnabled () ){
				log.debug (  "Loading Symbol " + desc.getAbsolutePath() + "..." ) ;
			}
			
			createSymbolFromDescriptor ( desc ) ;
			
			//loaded = true ;
		
		}
				
		sort() ;
		
		if ( log.isDebugEnabled () ){
			log.debug ( "Symbols " + getType().getFolder () + " loaded."  ) ;
		}
		
	}
	
	
	
	public void loadRandomCategoryContent ( String category ) {
		
		if ( log.isDebugEnabled () ){
			log.debug ( "Loading symbol random category content " + this.getClass ().getName() ) ;
		}
		
		
		// list abstract/random subdirs 
		
		File randomFolder = new File ( App.realPath ( App.getConfig().getVmTemplatesDir() + "/_symbols/" + 
				getType().getFolder () + "/abstract/random" ) ) ;
		
		if ( ! randomFolder.exists() ){
			log.warn( "Ignore loading random category content " +  randomFolder.getAbsolutePath() + " does not exists" ) ;
			return ;
		}
		
		randomCategoryContexts = new RandomCategoryContexts() ;
		
		randomCategoryContexts.load(randomFolder, category);
		
	}
		
	
	
	public PropertySet createSymbolFromDescriptor ( File xmlFile ){
		
		PropertySet symbol ;
		
		SymbolReader symbolReader = new SymbolReader() ;
		
		
		if ( ! xmlFile.exists () ){
			throw new AppRuntimeException ( "Cannot create symbol  from file " + xmlFile.getAbsolutePath () ) ;
		}
		
		try {
			
			symbol = symbolReader.read ( xmlFile ) ;
			
		} catch ( Exception e ) {
			throw new AppRuntimeException( "Unable to load symbol from " +  xmlFile.getAbsolutePath(), e ) ;
		}
		
		if ( symbol == null ){
			throw new AppRuntimeException( "Null symbol loaded from " +  xmlFile.getAbsolutePath() ) ;
		}
		
		String name = getType().getFolder () + "-" + xmlFile.getParentFile ().getName () ;
		
		symbol.setName ( name ) ;
		symbol.setPath( "_symbols/" +  symbol.getName().replaceFirst ( "-", "/" ) ) ;
		symbol.setSymbolType( getType() ) ;
	
		
		postCreateSymbol( symbol ) ;
		
		
		try {
			
			DesignUtils.deepBuild( symbol ) ;
		
		} catch ( Exception e ) {
			
			throw new AppRuntimeException( "Failed to build " + symbol.getName() , e ) ;
		
		}
		
		
		prepare( symbol ) ;

		computeThumbnails( symbol ) ;
		
		computePreviewHtml( symbol ) ;
		
		String[] tags = DesignUtils.getTags ( symbol )  ;
		
		if ( tags != null ){
			
			LinkedHashMap< String, PropertySet> tagSymbols  ;
			
			for ( String tag : tags ){
				
				tagSymbols =  resourcesMap.get( tag ) ;
				
				if ( tagSymbols == null ) {
					
					tagSymbols = new LinkedHashMap< String, PropertySet>() ;
					
					resourcesMap.put( tag, tagSymbols ) ;
				
				}
				
				tagSymbols.put (  symbol.getName (), symbol ) ;
			
			}
			
		}
		
		
		
		
		
		
		resourcesMap.get( ALL_RESOURCES ).put (  symbol.getName (), symbol ) ; 
		

		if ( log.isDebugEnabled () ){
			
			log.debug (  "Symbol " + name + "loaded." ) ;
		
		}
		
		return symbol ;
	
	}
	
	
//	protected Context getRandomContext( String symbolName, String category ){
//		return randomContentContexts.get( symbolName  ).get( category ) ;
//	}
//	
//
//	protected void setRandomContext( String name, String category, Context context ){
//		randomContentContexts.put( name, context ) ;
//	}
//	
	
	protected void postCreateSymbol ( PropertySet symbol ){
	}
	
	
	protected void computeThumbnails ( PropertySet symbol ){
		String imageUrl = getThumbnailUrlPath( symbol ).asUrl () + "?" ;
		StringBuffer sb = new StringBuffer( "<img src=\"").append( imageUrl ).append ( "\"" ).append( 
				App.isDevMode() ?  " title=\"" + symbol.getName() + "\"" : "" ).append( "/>"  ) ;

		symbol.setThumbnailHtml( sb.toString () ) ;
		symbol.setThumbnailUrl( imageUrl) ;		

	}


	protected Path getThumbnailUrlPath( PropertySet symbol ){
		String folder = symbol.getSymbolType ().getFolder () ;
		return new Path( "images" ).append( "symbols").append( folder ).append( 
				StringUtils.substringAfter ( symbol.getName (), folder + "-" ) ).append2 ( ".png?" + 
		RandomUtils.nextInt(3)  ) ;	
	}
	

	public ArrayList< PropertySet > filter( String criteria ) {

		if ( log.isDebugEnabled () ){
			
			log.debug ( "Get theme resources matching criteria '" + 
					criteria == null ? "" : criteria + "'"  ) ;
		
		}

		ArrayList< PropertySet > result = list(); 		
		
		if ( criteria == null ) {
			return result ;
		}

		
		ArrayList< PropertySet > filtered = new ArrayList< PropertySet >() ;
		
		SymbolFilter filter = new SymbolFilter( criteria ) ;
		
		for ( PropertySet symbol : result ){

			if ( filter.match ( symbol ) ) {
				filtered.add (  symbol  ) ;
			}
		}
				
		return filtered ;
		
	}
	
	
	
	protected void prepare ( PropertySet symbol ) {
	}
	
	
	protected void computePreviewHtml ( PropertySet symbol ) {
	}
	
	
	protected void loadRandomCategoryContexts( String category ){	
	}
		
	
	public RandomContext getRandomCategoryContext ( PropertySet set , String category ) {
		return randomCategoryContexts.get( set, category  ) ;
	}
	
	
	public RandomContext getRandomStyle( PropertySet set ) {
		return null ;
	}

	
	protected abstract SymbolType getType() ;
	
	
	
	protected String getThumbnailVm( PropertySet symbol ){

		String vm = null ;

		Path path = new Path ( App.realPath ( App.getConfig().getVmTemplatesDir() ) )
				.append( symbol.getPath () ).append ( "th.svg.vm" ) ;
		
		
		if ( ! path.asFile ().exists () ){
			
			String parent =  symbol.getParent() ;
			
			if ( parent != null ) {
				
				vm = getThumbnailVm( DesignUtils.getSymbol( parent ) ) ;
			
			}
			
		} else {
			
			path = new Path ( symbol.getPath () ).append( THUMBNAIL_SVG ) ;
			
			return path.toString () ;
		
		}
		
		return vm ;
		
	}

	
	
	public boolean isAbstract ( PropertySet symbol ) {
		
		return StringUtils.contains ( symbol.getName (), "abstract" );
	
	}

	
	public void createThumbnails () throws Exception {

		for( PropertySet symbol : list() ){

			if ( isAbstract ( symbol ) ) continue ;
			
			createThumbnail( symbol ) ;
		
		}
		
	}
	

	public void createThumbnail ( String name ) throws Exception {
	
		createThumbnail( get( name ) ) ;
		
	}
	
	

	
	protected void createThumbnail( PropertySet symbol ) throws Exception {
		
		String vm ;
		
		Context context ;
		String output ;
		
		RenderEngine renderEngine = App.getRenderEngine () ;
		
		PropertySet set = newThumbnailPropertySet() ;
		
		set.setParent( symbol.getName() ) ;
		
		DesignUtils.deepBuild( set ) ;

		System.out.println( "Generate thumbnail " + symbol.getName()  ) ;
		
		vm = getThumbnailVm ( symbol ) ;
		
		if ( vm == null || ! renderEngine.templateExists ( vm ) ) ;
		
			
		StringWriter writer = new StringWriter() ;
	
		context = new VelocityContext() ;
		
		DesignHelper<Design> helper =  new DesignHelper<Design>() ;
		
		context.put (  DesignContext.HELPER, helper ) ;
		context.put (  "_", helper ) ;
		context.put (  "s", set ) ;
	
		preprocessThumbnailContext( set, context ) ;
		
		context.put (  "width", getThumbnailWidth( symbol ) ) ;
		context.put (  "height", getThumbnailHeight( symbol) ) ;

		renderEngine.generate ( context, vm, writer ) ;

		output = App.realPath ( getThumbnailUrlPath( symbol ).toString () ) ;
		
		System.out.println( "\n\n\n"  ) ;
		
		System.out.println( writer.getBuffer().toString() ) ;
		
		System.out.println( "Create thumbnail to "  + output.toString() ) ;
		
		
		PNGTranscoder.transcode ( writer.getBuffer ().toString (), 
								new Rectangle( getThumbnailWidth( symbol ), getThumbnailHeight( symbol ) ), 
									output.toString() ) ;
	
	}

	
	protected void preprocessThumbnailContext ( PropertySet symbol, Context context ){
	}
	
	
	protected PropertySet newThumbnailPropertySet() {
		return new PropertySet() ;
	}
	
	
	protected int getThumbnailWidth( PropertySet symbol ) {
		return THUMBNAIL_WIDTH;
	}
	
	protected int getThumbnailHeight( PropertySet symbol ) {
		return THUMBNAIL_HEIGHT;
	}
	
	
	protected Comparator< PropertySet > sortComparator = new Comparator< PropertySet >() {
		@Override
		public int compare( PropertySet symbol1, PropertySet symbol2 ) {

			try {

				if ( symbol1.getSortIndex() == null ) {
					
					return -1 ;
				
				}
				
				if ( symbol2.getSortIndex() == null ) {
					
					return 1 ;
				
				}
				
				return ( symbol1.getSortIndex() < symbol2.getSortIndex() ) ? -1 : 1    ;
				
			} catch ( Exception e ) {
				
				e.printStackTrace();
				System.out.println( symbol1.getPath() + " " +  symbol2.getPath() ) ;
				
				return -1 ;

			}
		
		}
	};




	
	

}
