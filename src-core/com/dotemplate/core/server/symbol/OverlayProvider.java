package com.dotemplate.core.server.symbol;

import java.awt.Dimension;
import java.io.File;

import org.apache.commons.io.FileUtils;
import org.apache.velocity.context.Context;

import com.dotemplate.core.server.App;
import com.dotemplate.core.server.DesignUtils;
import com.dotemplate.core.server.svg.ImageUtils;
import com.dotemplate.core.server.util.Path;
import com.dotemplate.core.shared.SymbolType;
import com.dotemplate.core.shared.properties.PropertySet;



public class OverlayProvider extends SymbolProvider {

	@Override
	protected SymbolType getType () {
		return SymbolType.OVERLAY ;
	}
	
	
	@Override
	protected void preprocessThumbnailContext( PropertySet symbol,
			Context context ) {
	
		// compute thumbnail scale
		
		Path imagePath = DesignUtils.getSymbolRealPath( symbol ).append( "overlay.png" ) ;
		
		Dimension d = ImageUtils.getImageSize( imagePath.asFile() ) ;

		context.put( "scale", new Double( ( double ) getThumbnailWidth( symbol ) / d.getWidth() ) ) ;	
		
	}
	
	
	@Override
	protected void createThumbnail(PropertySet symbol) throws Exception {
		
		if ( symbol.getName().endsWith( "nooverlay" ) ) {
			
			FileUtils.copyFile( new File( App.realPath( "WEB-INF/templates/" + symbol.getPath() + "/empty.png" ) ) , new File(
					App.realPath ( getThumbnailUrlPath( symbol ).toString () ) ) ) ;
			
			return ;
		
		}
		
		super.createThumbnail( symbol );
	
	}
	
	
	/*
	@Override
	protected int getThumbnailHeight( PropertySet symbol ) {
		int width = ( Integer ) symbol.get( "width" )  ; 
		int height = ( Integer ) symbol.get( "height" )  ; 
		return ( int ) ( ( double ) ( ( double ) ( height ) / ( double ) ( width ) ) *  getThumbnailWidth( symbol ) )   ;
	}*/
	
	
	@Override
	protected int getThumbnailWidth( PropertySet symbol ) {
		return 150 ;
	}
	
	protected int getThumbnailHeight( PropertySet symbol ) {
		return 60;
	}
	
}
