package com.dotemplate.core.server.symbol;


import com.dotemplate.core.shared.SymbolType;
import com.dotemplate.core.shared.properties.PropertySet;


public class PatternProvider extends SymbolProvider {
	
	
	@Override
	protected SymbolType getType () {
		return SymbolType.PATTERN;
	}

	
	@Override
	protected int getThumbnailHeight(PropertySet symbol) {
		return 100 ;
	}
	
	@Override
	protected int getThumbnailWidth(PropertySet symbol) {
		return 100 ;
	}

	
	@Override
	protected void sort() {
	}
	
}
