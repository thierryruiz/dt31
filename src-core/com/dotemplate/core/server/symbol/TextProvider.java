package com.dotemplate.core.server.symbol;

import java.util.Collections;

import com.dotemplate.core.shared.SymbolType;
import com.dotemplate.core.shared.properties.PropertySet;
import com.dotemplate.core.shared.properties.TextProperty;



public class TextProvider extends SymbolProvider {

	
	@Override
	protected SymbolType getType () {
		
		return SymbolType.TEXT ;
	
	}
	
	
	@Override
	protected void sort() {
		Collections.sort( list(), sortComparator ) ;
	}
	
	
	protected int getThumbnailWidth( PropertySet symbol ) {
		return 420;
	}
	
	protected int getThumbnailHeight( PropertySet symbol ) {
		return 110;
	}
	
	
	@Override
	protected PropertySet newThumbnailPropertySet() {
		TextProperty text = new TextProperty() ;
		text.setTop( 10 ) ;
		text.setLeft( 10 ) ;

		return text ;
	}
	
	
	
}
