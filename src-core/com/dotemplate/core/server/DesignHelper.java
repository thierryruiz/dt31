/*
/*
 * 
 * Created on 28 juin 2007
 * 
 */
package com.dotemplate.core.server;

import java.awt.Dimension;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.RandomUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.velocity.context.Context;

import com.dotemplate.core.server.frwk.AppException;
import com.dotemplate.core.server.frwk.AppRuntimeException;
import com.dotemplate.core.server.random.RandomContext;
import com.dotemplate.core.server.svg.SVGDimensionHelper;
import com.dotemplate.core.server.svg.Slices;
import com.dotemplate.core.server.util.Path;
import com.dotemplate.core.shared.Design;
import com.dotemplate.core.shared.SVGFont;
import com.dotemplate.core.shared.canvas.Graphic;
import com.dotemplate.core.shared.properties.ImageProperty;
import com.dotemplate.core.shared.properties.Property;
import com.dotemplate.core.shared.properties.PropertySet;
import com.dotemplate.core.shared.properties.TextProperty;
import com.dotemplate.theme.server.ThemeApp;

/**
 * 
 * 
 * @author Thierry Ruiz
 * 
 */
public class DesignHelper< D extends Design > {

	private static Log log = LogFactory.getLog ( DesignHelper.class );

	private SVGDimensionHelper dimensionHelper = new SVGDimensionHelper ();

	public String darker ( String colorCode, int scale ) {
		return ColorUtils.darker( colorCode, scale ) ;
	}
	
	public String brighter ( String colorCode, int scale ) {
		return ColorUtils.brighter( colorCode, scale ) ;
	}
	
	public String oppositeSoftColor( String color ){
		return ColorUtils.getOppositeSoft( color ) ;
	}
	
	
	public String shade( String color, int percent ){
		return ColorUtils.shade( color, percent  ) ;
	}
	
	public String oppositeColor( String color ){
		return ColorUtils.getOpposite( color ) ;
	}
	
	public int percentWidth ( int width, int percent ) {
		return ( int ) ( width * percent / 100 );
	}

	public String escapeXML ( String s ) {
		return StringEscapeUtils.escapeXml ( s );
	}

	
	public boolean isRemoteImage( ImageProperty image ){
		return image.getValue().startsWith("http") ;
	}

	
	public String id () {
		return "" + RandomUtils.nextInt ( 999999 );
	}

	public double div ( int n, int m ) throws ParseException {
		double value = ( double ) ( ( double ) n ) / ( ( double ) m );
		return ( double ) Math.round( value * 100 ) / 100.0d ;
	}

	public int halfSize ( int size ) throws ParseException {
		return ( int ) div( size, 2 ) ;
	}
	
	public int thirdSize ( int size ) throws ParseException {
		return ( int ) div( size, 3 ) ;
	}

	public int quarterSize ( int size ) throws ParseException {
		return ( int ) div( size, 4 ) ;
	}
	
	
	public int ratio ( int v1, int v2, int v3 ) {
		double ratio = ( double ) v2 / ( double ) v3;
		return ( int ) Math.round ( v1 * ratio );
	}

	public SVGFont getSVGFont ( String family ) {
		return ( ( SVGFontProvider ) App.getSingleton ( SVGFontProvider.class ) ).get ( family );
	}

	
	public int computeBaseline ( TextProperty text ) {
		try {

			// compute the text baseline top coordinate for this text property
			SVGFont font = ( ( SVGFontProvider ) App.getSingleton ( SVGFontProvider.class ) )
									.get ( text.getFontFamily () );

			if ( font == null ) {
				throw new AppRuntimeException ( "Unknown font '" + text.getFontFamily ()
										+ "' used by text '" + text.getName () + "'" );
			}

			return ( int ) ( font.getAscent () * text.getFontSize () / 100 );

		} catch ( Exception e ) {
			log.error ( e );
			throw new AppRuntimeException ( "Failed to compute text baseline for text ", e );
		}
	}

	
	public void computeSize ( TextProperty text ) {
		dimensionHelper.computeDimensions ( text );
	}

	public Dimension getTextSize ( TextProperty text ) {
		return dimensionHelper.getDimensions ( text );
	}

	
	public void computeSize ( ImageProperty image ) {
		dimensionHelper.computeDimensions ( image );
	}

	public String realPath ( String path ) {
		return App.realPath ( path ).replace ( '/', File.separatorChar );
	}

	public String symbolRealPath ( PropertySet symbol ) {
		return App.realPath ( 
				new StringBuffer( "WEB-INF/templates/" )
					.append( symbol.getPath() ).toString() ) ;
	}
	
	
	public void log ( String msg, Object o ) {
		if ( log.isDebugEnabled () ) {
			if ( o != null ) {
				msg = msg + " " + o.toString ();
			}

			log.debug ( msg );
		}
	}

	public void log ( String msg ) {
		if ( log.isDebugEnabled () ) {
			log.debug ( msg );
		}
	}

	public void log ( Object o ) {
		if ( log.isDebugEnabled () ) {
			log.debug ( o.toString () );
		}
	}

	public void sysout ( String msg ) {
		System.out.println ( msg );
	}
	
	
	public void sysout ( Object o ) {
		System.out.println ( "[sysout] " + (o != null ? o : "null")  );
	}


	public void sysout ( String msg, Object o ) {
		System.out.println ( "[sysout] " + msg + " " + (o != null ? o : "null") );
	}
	
	
	public void sysout ( String s1, String s2 ) {
		System.out.println ( "[sysout] "  + (s1 != null ? s1 : "null") + " " + (s2 != null ? s2 : "null ") );
	}
	
	public void sysout ( String s1, String s2, String s3 ) {
		System.out.println ( "[sysout] "  + (s1 != null ? s1 : "null") + " " + (s2 != null ? s2 : "null ") + " " + (s3 != null ? s3 : "null ") );
	}

	public void sysout ( PropertySet set ) {
		DesignLogger.log ( set );
	}


	public PropertySet getSymbol ( String symbolName ) {
		return DesignUtils.getSymbol ( symbolName );
	}

	
	public Object getSymbolSettings ( PropertySet symbol ) {

		String vm = new StringBuffer ( symbol.getPath () ).append ( "/" ).append ( "settings.vm" )
								.toString ();

		if ( App.getRenderEngine ().templateExists ( vm ) ) {
			return vm;
		}

		return new Boolean ( false );

	}

	public Object get ( String name ) {
		// TODO is really usefull $name is supposed to make it ? 
		return App.getDesignContext ().get ( name );
	
	}

	
	public String bannerBrush ( String brushName ) {
		return App.realPath ( new StringBuffer ( "WEB-INF/templates/_images/brushes/" ).append (
								brushName ).append ( ".png" ).toString ().replace ( '/',
								File.separatorChar ) );
	}

	// include a file from its uri relative to webapp root directory
	public String include ( String fileUri ) {

		try {
			return FileUtils.readFileToString ( new File ( App.realPath ( fileUri ) ) );
		} catch ( IOException e ) {
			throw new AppRuntimeException ( "Failed to include file " + fileUri, e );
		}

	}

	// copy a file from its uri relative to webapp root directory
	public void copy ( String fileUri ) {
		File dest = ( App.getDesignContext ().isExportMode () ) ? new Path ( App.get()
								.getExportDirectoryRealPath () ).asFile () : new File ( App.get()
								.getWorkRealPath () );

		try {
			FileUtils.copyFileToDirectory ( new File ( App.realPath ( fileUri ) ), dest );
		} catch ( IOException e ) {
			throw new AppRuntimeException ( "Failed to copy file " + fileUri, e );
		}	
	}

	public boolean templateExists ( String tpl ) {
		return App.getRenderEngine ().templateExists ( tpl );
	}

	
	public void generate ( String template, String output ) {
		try {
			App.getRenderEngine ().generate ( App.getDesignContext (), template,
									new FileWriter ( output ) );
		} catch ( Exception e ) {
			throw new AppRuntimeException ( "Failed to generate theme", e );
		}
	}

		
	public Collection<Graphic> getGraphics ( PropertySet drawable ) {

		ArrayList<Graphic> graphics = new ArrayList<Graphic> ();

		int type;
		
		for ( Property p : drawable.getProperties () ) {

			type = p.getType ();

			if ( Graphic.IMAGE == type || Graphic.TEXT == type || Graphic.BG == type ) {

				graphics.add ( ( Graphic ) p );

			}
		}

		return graphics;
	}

	
	

	public String [] patternDimension ( String patternName ) {
		try {
			return StringUtils.substringBetween ( patternName, "-", "." ).split ( "x" );
		} catch ( Exception e ) {

			throw new AppRuntimeException ( "Enable get pattern dimension from String "
									+ patternName, e );
		}
	}

	
	public void missingContextObject( String key ){
		throw new AppRuntimeException("Missing '" +key + "' in context. Please ensure this key is set." ) ;
	}
	
	
	/*
	 * public String parentPath( ThemePropertySet set ){
	 * 
	 * ThemePropertySet symbol = SymbolProvider.getSymbol ( set );
	 * 
	 * if ( symbol == null ) { throw new EaseRuntimeException (
	 * "No symbol define for set " + set.getName () ); }
	 * 
	 * return SymbolProvider.getSymbol ( symbol ).getPath () ;
	 * 
	 * }
	 */

	/*
	 * public String getSuperVm ( ThemePropertySet set, String vm ) {
	 * 
	 * if( "html.vm".equals ( vm ) ){ System.out.println( set.getPath () ) ; }
	 * 
	 * 
	 * ThemePropertySet symbol = SymbolProvider.getSymbol ( set );
	 * 
	 * if ( symbol == null ) { throw new EaseRuntimeException (
	 * "No symbol define for set " + set.getName () ); }
	 * 
	 * ThemePropertySet supr = SymbolProvider.getSymbol ( symbol );
	 * 
	 * if ( supr == null ) { throw new EaseRuntimeException (
	 * "No parent symbol define for symbol " + symbol.getPath () ); }
	 * 
	 * String superPath = new StringBuffer ( supr.getPath () ).append ( '/'
	 * ).append ( vm ).toString ();
	 * 
	 * log.debug ( set.getName () + " : Use template " + superPath ) ;
	 * 
	 * 
	 * 
	 * return superPath ;
	 * 
	 * }
	 */


	
	public void setProperty ( PropertySet set, String name, Property property ) throws AppException {
		set.putProperty( name, DesignUtils.cloneProperty( property ) );
	}

	
	public Object superSettings ( PropertySet set ) throws AppException {
		PropertySet symbol = SetHierarchy.next ( set );
		return getSymbolSettings ( symbol );
	}

	public Object superDraw ( PropertySet set ) throws AppException {

		PropertySet symbol = SetHierarchy.next ( set );

		String vm = new StringBuffer ( symbol.getPath () ).append ( "/" ).append ( "svgf.vm" )
								.toString ();

		if ( App.getRenderEngine ().templateExists ( vm ) ) {
			return vm;
		}

		return new Boolean ( false );
	}
	

	public Object superSvg ( PropertySet set ) throws AppException {

		PropertySet symbol = SetHierarchy.next ( set );

		String vm = new StringBuffer ( symbol.getPath () ).append ( "/" ).append ( "svg.vm" )
								.toString ();

		if ( App.getRenderEngine ().templateExists ( vm ) ) {
			return vm;
		}

		return new Boolean ( false );
	}

	
	public void createHierarchy ( PropertySet set ) {
		
		if ( set == null ){
			throw new AppRuntimeException( "Cannot create hierarchy. PropertySet is null !" )  ;
		}
		
		try {
			SetHierarchy.create ( set );
		} catch (Exception e) {
			log.error( e ) ;
			throw new AppRuntimeException( e )  ;
		}
	}

	
	
	public PropertySet nextAncestor ( PropertySet set ) throws AppException {
		return SetHierarchy.next ( set );
	}

	
	
	public String getConfig( String name ){
		
		try {
			return ( String ) BeanUtils.getProperty( App.getConfig(), name ) ;
		} catch (Exception e) {
			
			throw new AppRuntimeException("Undefine config setting '"+ name + "" );
		}
		
	}
	

	public void addSlice( Slices slices, String name, int left, int top, int width, int height, double quality  ) {
		slices.add( name, left, top, width, height, quality ) ;
	}
	
	
	public void addSlicePng( Slices slices, String name, int left, int top, int width, int height, double quality  ) {
		slices.addPng( name, left, top, width, height, quality ) ;
	}

	
	
	public void createDependency ( PropertySet set, String attributes, PropertySet dep ){
		
		// create a dependency generate rule of property set update
		
		if ( set == null ) return ;
		
		Dependencies deps = App.getDesignContext().getDependencies() ;
		
		//PropertyDependencies d ;
		
		boolean editable ;
		
		if ( attributes.length() == 0 ){
			
			deps.add( set, dep ) ;
			
			/*
			d = deps.getPropertyDependencies( set ) ;
			
			if ( d == null ){
				d = deps.add( set, null ) ;
			}
			
			d.add( dep ) ;
			*/
			
		} else {	
			
			attributes = attributes.trim() ;
			
			if ( "*".equals( attributes ) ) {
			
				deps.add( set, dep ) ;
				
				for ( Property p : set.getProperties() ){
					
					if ( p.isSet() ) {
						createDependency( ( PropertySet ) p, "*", dep  ) ;
					}
					
					editable = set.getEditable() != null && set.getEditable().booleanValue() ;
					
					if ( ! editable ) continue ;
					
					deps.add( p , dep ) ;
					
					/*
					d = deps.getPropertyDependencies( p ) ;		
					
					if ( d == null ){
						d = deps.add( p, null ) ;
					}
					
					d.add(dep) ;
					*/
				
				}
			} else {
				
				Property p ;
				
				for ( String s : attributes.split(",") ){
					
					p = ( Property ) set.getProperty( s.trim() ) ;

					if ( p == null ){
						continue ;
						
						/*
						throw new AppRuntimeException( 
								"Null or undefined child property " + s.trim() + "' on '" + 
									set.getLongName() + "'" ) ;
						*/
						
						
					}
					
					if ( p.isSet() ) continue ;
					
					editable = set.getEditable() != null && set.getEditable().booleanValue() ;
					
					if ( ! editable ) continue ;
					
					/*
					d = deps.getPropertyDependencies( p ) ;		
					
					
					d = deps.getPropertyDependencies( p ) ;		
					
					if ( d == null ){
						
						d = deps.add( p, null ) ;
					
					}
					
					d.add( dep ) ;
					*/
					
					deps.add( p , dep )  ;
				
				}
				
			}
			
		}
		
	}
	
	
	public boolean isPurchased( ){
		return App.getDesignContext ().getDesign().isPurchased() ;
	}
	
	
	
	
	public Context randomMap( PropertySet set ){

		
//		DesignUtils.getSymbolProvider(set) 
//		
//		String vm = new StringBuffer ( set.getPath () ).append ( "/" ).append("random").append ( ".vm" )
//								.toString ();
//
//		if ( App.getRenderEngine ().templateExists ( vm ) ) {
//			return vm;
//		}

		
		
		
		
		return null ;
	}
	
	
	
	public RandomContext getRandomContent( PropertySet set ){		
		return DesignUtils.getSymbolProvider(set).getRandomCategoryContext(
				set, ThemeApp.getTheme().getContent() ) ;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	/*
	public PropertyDependencies onUpdate( Property property ){
		
		Dependencies deps = App.getDesignContext().getDependencies() ;
		
		PropertyDependencies d = deps.getPropertyDependencies( property ) ;
		
		if ( d == null ){
			d = deps.add( property, null ) ;
		}
		
		return d ;
		
	}

	

	public PropertyDependenciesList onUpdate( Property parent, String[] attributes ){
		
		Dependencies deps = App.getDesignContext().getDependencies() ;
		
		PropertyDependencies d ;

		PropertyDependenciesList list = new PropertyDependenciesList() ;
		
		Property property ;
		
		for ( String attribute : attributes ){
			try {
				
				property = ( Property ) PropertyUtils.getProperty( parent  , attribute ) ;
				
			} catch ( Exception e )  {
				throw new AppRuntimeException( 
						"Null or undefined child property " + attribute + "' on '" + 
							parent.getLongName() + "'", e ) ;
			}			

			
			d = deps.getPropertyDependencies( property ) ;		
			
			if ( d == null ){
				d = deps.add( property, null ) ;
			}
			
			list.add( d ) ;
				
		}
		
		return list ;
		
	}*/
	
	
	
	
	
	
	
}
