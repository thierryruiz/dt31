package com.dotemplate.core.server;

import java.awt.Color;

public class ColorUtils {
    
	public static String toHexString( Color color ) {
        return "#"+( "" +Integer.toHexString( color.getRGB() ) ).substring( 2 ).toUpperCase();        
    }
	
	public static Color fromHexString( String hexa ) {
        return Color.decode( hexa  ) ;
    }
	

	public static boolean isDark( Color c ) {
		//return c.getRed() + c.getGreen() + c.getBlue() < 3 * 180;
	
		double yiq = ((c.getRed()*299)+(c.getGreen()*587)+(c.getBlue()*114)) / 1000 ;
	
		return ( yiq >= 128 ) ? false : true ;
				
	/*	
	 * from http://24ways.org/2010/calculating-color-contrast/		
	 int r = parseInt( hexcolor.substr(0,2),16);
			var g = parseInt(hexcolor.substr(2,2),16);
			var b = parseInt(hexcolor.substr(4,2),16);
			var yiq = ((r*299)+(g*587)+(b*114))/1000;
			return (yiq >= 128) ? 'black' : 'white';
	*/
	
	}
	
	public static Color getOpposite( Color c  ) {
		return isDark(c) ? Color.WHITE : Color.BLACK;
	}

	public static String getOpposite( String hexa ){
		return toHexString( getOpposite( fromHexString( hexa ) ) ) ;
	}
	
	
	public static String getOppositeSoft( String hexa ){
		return toHexString( getOppositeSoft( fromHexString( hexa ) ) ) ;
	}
	
	public static Color getOppositeSoft( Color c ){
		return getOpposite( c , 30 ) ;
	}
	
	public static Color getOpposite( Color c, int distandeToOpposite ){

		Color opposite = ColorUtils.getOpposite( c )  ;
		
		HSLColor oppositeHSL = getHSLColor ( opposite ) ;
		
		if ( oppositeHSL.getLuminance() == 0  ){
			return oppositeHSL.adjustLuminance( distandeToOpposite ) ; 
		}
		
		return ColorUtils.isDark( opposite ) ? 
				oppositeHSL.adjustTone( distandeToOpposite ) : oppositeHSL.adjustShade( 20 ) ;

	}

	
	
	
	public static HSLColor getHSLColor( String hexa ) {
		return new HSLColor( Color.decode( hexa ) );
	}
	
	public static HSLColor getHSLColor( Color color ) {
		return new HSLColor( color );
	}

	
	public static String shade( String color, int scale ) {
		// color variation to a darker or brighter color
		return isDark( fromHexString( color ) ) ? brighter( color, scale ) :
			darker( color, scale ) ;
		
		/*
		HSLColor hsl = getHSLColor(  color ) ;

		boolean isDark = isDark( fromHexString( color ) ) ;
		
		return ( isDark ) ? toHexString ( hsl.adjustTone( percent ) ) : 
				toHexString ( hsl.adjustShade( percent ) )  ;
		*/
	}
	
	
	public static String darker ( String colorCode, int scale ) {

		Color color = Color.decode ( colorCode );

		int r, g, b;

		r = color.getRed ();
		g = color.getGreen ();
		b = color.getBlue ();

		r = ( r - scale > 255 ) ? 255 : r - scale;
		g = ( g - scale > 255 ) ? 255 : g - scale;
		b = ( b - scale > 255 ) ? 255 : b - scale;

		r = ( r < 0 ) ? 0 : r;
		g = ( g < 0 ) ? 0 : g;
		b = ( b < 0 ) ? 0 : b;

		Color result = new Color ( r, g, b );

		return "#" + padHex ( Integer.toHexString ( result.getRed () ) )
								+ padHex ( Integer.toHexString ( result.getGreen () ) )
								+ padHex ( Integer.toHexString ( result.getBlue () ) );
	}

	
	
	public static String brighter ( String colorCode, int scale ) {

		// scale = Math.abs( scale ) ;
		Color color = Color.decode ( colorCode );

		int r, g, b;

		r = color.getRed ();
		g = color.getGreen ();
		b = color.getBlue ();

		r = ( r + scale > 255 ) ? 255 : r + scale;
		g = ( g + scale > 255 ) ? 255 : g + scale;
		b = ( b + scale > 255 ) ? 255 : b + scale;

		r = ( r < 0 ) ? 0 : r;
		g = ( g < 0 ) ? 0 : g;
		b = ( b < 0 ) ? 0 : b;

		Color result = new Color ( r, g, b );

		return "#" + padHex ( Integer.toHexString ( result.getRed () ) )
								+ padHex ( Integer.toHexString ( result.getGreen () ) )
								+ padHex ( Integer.toHexString ( result.getBlue () ) );
	}
	
	
	private static String padHex ( String s ) {
		return ( s.length () == 1 ) ? "0" + s : s;
	}
	
	
}
