package com.dotemplate.core.server;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;


import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


import com.dotemplate.core.shared.properties.Property;
import com.dotemplate.core.shared.properties.PropertySet;
import com.dotemplate.theme.server.ThemeApp;



public class Dependencies {

	private static Log log = LogFactory.getLog( Dependencies.class );
	
	Map< String, PropertyDependencies > map ;
	
	
	public Dependencies() {
		map = new HashMap< String, PropertyDependencies >();
	}


	public PropertyDependencies add( Property prop , PropertySet set ){
		
		PropertyDependencies dep = map.get( prop.getUid() ) ;
		
		
		if (  dep == null ){
			map.put( prop.getUid(), dep = new PropertyDependencies() ) ;
			dep.property = prop ;
			dep.sets = new ArrayList < PropertySet >() ;
		
		}

		
		if ( set != null ){
			
			if ( !dep.sets.contains(set) ){
				
				if ( ThemeApp.isDevMode() && log.isDebugEnabled() ){
					
					System.out.println( prop.getLongName() + "  >  " + set.getLongName() ) ;
				
				}
				
				dep.sets.add( set ) ;
				
			}
		}
		
		return dep ;
		
	}
	
	
	public void log( Property property ) {
		
		System.out.println ( "\n\nProperty  '" + property.getLongName()  + "' dependencies:") ;
		Collection < PropertySet > sets = get( property.getUid() ) ;
		for ( PropertySet set : sets ) {
			System.out.println ( "\t" + set.getLongName() ) ;
		}
	
	}
	
	
	
	@SuppressWarnings("unchecked")
	public Collection < PropertySet > get( String propertyUid ) {
		
		PropertyDependencies dep = map.get( propertyUid ) ;
		
		if ( dep == null ) {
			return Collections.EMPTY_LIST ;
		}
	
		return dep.sets ;
		
	}
	
	
	
	public PropertyDependencies getPropertyDependencies( Property property ) {
		return map.get( property.getUid() ) ;
	}
	
	
	public class PropertyDependencies {
		
		private Property property ;
		
		private ArrayList< PropertySet > sets  ;

		
		public void add( PropertySet set ){
			
			if ( ! sets.contains( set ) ){
				
				if ( ThemeApp.isDevMode() && log.isInfoEnabled() ){
					System.out.println( "onUpdate( " + property.getLongName() + " ) update ( " + set.getLongName() + " )" ) ;
				}	
				sets.add( set ) ;
			}
		}
		
		
	}
	

}
