package com.dotemplate.core.server;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.util.Properties;

import com.dotemplate.core.server.frwk.AppException;
import com.dotemplate.core.shared.Design;


public class PurchaseInfoHelper {
	
	
	public static PurchaseInfo load( String designUid ) throws AppException {
		
		PurchaseInfo info = new PurchaseInfo() ;
		
		Properties props = new Properties() ;
		
		File purchaseDesc = new File (  App.get().getWorkRealPath( designUid ) + "/purchase.properties" ) ;
		
		if ( ! purchaseDesc.exists() ){
			return null ;
		}
		
		try {

			props.load( new FileInputStream( purchaseDesc ) )  ;
			
			info.setDownloads( Integer.parseInt( props.getProperty( "downloads" ) ) ) ;
			info.setGateway( props.getProperty( "gateway" ) ) ;
			info.setTransactionId( props.getProperty( "transactionId" ) ) ;
			info.setPayer( props.getProperty( "payer" ) ) ;
			info.setAffiliateId( props.getProperty( "affiliateId" ) ) ;
			
		} catch ( Exception e ){
			
			throw new AppException( "Failed to read purchase info from " + purchaseDesc.getAbsolutePath(), e ) ;
			
		}
		
		return info ;
		
	}

	
	
	
	public static void save()  throws AppException {
		
		Design design = App.getDesign() ;
		
		if ( ! design.isPurchased() ) return ;
		
		PurchaseInfo info = App.getDesignContext().getPurchaseInfo() ; 
		
		if ( info == null ) {
			throw new AppException( "No PurchaseInfo in DesignContext" ) ;
		}
		
    	// create theme properties
    	Properties props = new Properties() ;
    	props.setProperty( "downloads", "" + info.getDownloads() ) ;
    	props.setProperty( "gateway", info.getGateway() );
    	props.setProperty( "payer", info.getPayer() );
    	props.setProperty( "transactionId", info.getTransactionId() );
    	props.setProperty( "affiliateId", info.getAffiliateId() ) ;
    	
    	
    	try {
    		
    		props.store( new FileWriter( new File( App.get().getWorkRealPath() + "/purchase.properties" ) ), "" ) ;
    		
    	} catch ( Exception e ){
    		
    		throw new AppException("Failed to write purchase properties !", e ) ;
    	
    	}

		
		
		
	}
	
	
	
	

}
