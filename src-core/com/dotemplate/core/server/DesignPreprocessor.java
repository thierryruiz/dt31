package com.dotemplate.core.server;

import java.util.Iterator;
import java.util.Stack;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.util.UIDGenerator;
import com.dotemplate.core.shared.Design;
import com.dotemplate.core.shared.DesignTraverser;
import com.dotemplate.core.shared.properties.Property;
import com.dotemplate.core.shared.properties.PropertySet;


public class DesignPreprocessor<D extends Design > implements DesignTraverser {
	
	
	@SuppressWarnings("unused")
	private static Log log = LogFactory.getLog( DesignPreprocessor.class );
	
	private static final long serialVersionUID = 2644951018797603138L;
	
	protected DesignContext< D > designContext ;
	
	private NameStack nameStack ;
	
	
	public DesignPreprocessor ( DesignContext<D> context ) {
		
		this.designContext = context ;
		
		nameStack = new NameStack() ;
		
	}

	
	public void execute () {
				
		Design theme = designContext.getDesign () ;
		
		nameStack.clear() ;
		
		theme.execute( this ) ;	
	
		for ( PropertySet set : theme.getPropertySetMap ().values () ) {
			
			designContext.put( set.getName(), set ) ;
		
		}
		
	}
	
	
	public boolean startSet ( PropertySet set ) {				
		// set property uid if not set 		
		
		if ( set.getUid () == null ){
			set.setUid (  UIDGenerator.pickId () ) ;
		}
		
		nameStack.push( set.getName() ) ;
		
		set.setLongName( nameStack.toString() ) ;
		
		
		return true ;
	}


	public boolean startProperty ( Property property ) {

		// set property uid if not set 		
		if ( property.getUid () == null ){
			property.setUid (  UIDGenerator.pickId () ) ;
		}
		
		nameStack.push( property.getName() ) ;
		
		property.setLongName( nameStack.toString() ) ;
		
		return true ;
	}
	
	

	@Override
	public boolean endSet(PropertySet set) {
		nameStack.pop();
		return true;
	}

	@Override
	public boolean endProperty(Property property) {
		nameStack.pop();
		return true;
	}



	private class NameStack extends Stack<String>{
		
		private static final long serialVersionUID = 1L;

		
		@Override
		public synchronized String toString() {
			
			StringBuffer sb = new StringBuffer() ;
			
			if ( isEmpty() ) return "" ;
			
			Iterator< String > names = super.iterator() ;
			 
			sb.append( names.next() ) ;
			
			while ( names.hasNext() ){
				sb.append( '.' ).append( names.next() ) ;
			}
			
			return sb.toString();
		}
		
		
	}

	
	
	

	
}
