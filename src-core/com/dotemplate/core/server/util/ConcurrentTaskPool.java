package com.dotemplate.core.server.util;

import org.apache.commons.pool.PoolableObjectFactory;



public abstract class ConcurrentTaskPool<V> extends Pool< ConcurrentTask< V > > {

	
	public ConcurrentTaskPool () {
		super() ;
		innerPool.setMaxActive(-1) ;
	}

	
	
	@SuppressWarnings("unchecked")
	public ConcurrentTask< V > borrow() throws Exception {	
		return ( com.dotemplate.core.server.util.ConcurrentTask< V > ) innerPool.borrowObject() ;
	}
	
	
	public void release ( ConcurrentTask< V > task) throws Exception {
		task.applyContext ( null ) ;
		innerPool.returnObject( task ) ;
	}
	
	
	
	@Override
	protected PoolableObjectFactory createFactory() {
		return new PoolableObjectFactory () {
			public void activateObject( Object o ) throws Exception {
			}

			public void destroyObject( Object o ) throws Exception {
			}

			public Object makeObject() throws Exception {
				return createTask ();
			}

			public void passivateObject( Object o ) throws Exception {
			}

			public boolean validateObject( Object o ) {
				return true;
			}
		} ;

	}
	
	
	protected abstract ConcurrentTask< V > createTask () throws Exception ;
	
	
	

}
