/**
 * Copyright Thierry Ruiz ( Athesanet ) 2004 - 2008
 * All rights reserved.
 *
 * Created on 20 mars 08 : 11:19:04
 */
package com.dotemplate.core.server.util;

import java.io.File;


/**
 * 
 * A util class to build file system path. Atomatically appends
 * the system File separator 
 * 
 * @author Thierry Ruiz
 *
 */
public class Path {
	
	protected StringBuffer buffer = new StringBuffer() ;
	
	public Path(){
	}
	
	public Path( String s ){
		buffer.append ( s ) ;
	}
	
	
	public Path append( String s ) {
		buffer.append (  File.separator ) ;
		buffer.append ( s ) ;
		
		return this ;
		
	}

	
	public Path append2 ( String s ) {
		buffer.append ( s ) ;
		return this ;
	}

	
	public StringBuffer getBuffer() {
		return buffer ;
	}
	
	public String toString  ( ) {
		return buffer.toString () ;
	}
	
	
	public File asFile () {
		return new File ( buffer.toString () ) ;
	}
	
	
	public String asUrl(){
		return  buffer.toString ().replace( File.separatorChar, '/' ) ;
	}
	
	public String asString ( ){
		return buffer.toString() ;
	}
	
	
	

	

}
