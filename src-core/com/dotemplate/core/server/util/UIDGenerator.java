/**
 * Copyright Thierry Ruiz - Athesanet 2004-2008. All rights reserved.
 * Created on 3 mars 08 - 14:27:46
 *
 */
package com.dotemplate.core.server.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;


import org.apache.commons.lang.RandomStringUtils;


/**
 * A unique ID generator based on current date and a random int.
 *
 * @author Thierry Ruiz
 *
 */
public class UIDGenerator {
	
	private static SimpleDateFormat format = new SimpleDateFormat( "yyMMddHHmm" ) ;
	
	private static Calendar calendar = new GregorianCalendar () ;
	
	public static String pickId(){ 
		return pickId( 8 ) ;
	}
	

	public static String pickId( int n ){ 
		return new StringBuffer( format.format ( calendar.getTime () ) )
				.append( RandomStringUtils.randomAlphanumeric( n ) )
				.toString () ;
	}
	
	
	
}
