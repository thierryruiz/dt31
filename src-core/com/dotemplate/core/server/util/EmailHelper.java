package com.dotemplate.core.server.util;



import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.reloading.FileChangedReloadingStrategy;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.mail.HtmlEmail;

import com.dotemplate.core.server.App;
import com.dotemplate.core.server.frwk.AppException;
import com.dotemplate.core.server.frwk.AppRuntimeException;



/**
 * 
 * doTemplate Gmail account password ...popa shhh!!!...
 * darooze Hotmail account for test
 * 
 * 
 * @author Admin
 *
 */
public class EmailHelper {

	private final static Log log = LogFactory.getLog ( EmailHelper.class );
	
	private PropertiesConfiguration config ; 
	
	
	private EmailHelper() {
		
		log.info ( "Creating mail service...." ) ;
		
		try {
			config = new PropertiesConfiguration( App.realPath ( "WEB-INF/mail.properties" ) ) ;
			config.setReloadingStrategy( new FileChangedReloadingStrategy() );

		} catch ( ConfigurationException e ) {
			throw new AppRuntimeException ( "Unable to load mail service configuration", e ) ;	
		}
	}
	
	
	public void sendDownloadUrlEmail ( String dest, String key ) throws AppException {
		
		log.info ( "Sending download url to '" + dest + "'..." );

		HtmlEmail email = new HtmlEmail ();
		
		try {

			email.setHostName ( config.getString ( "smtpHost" ) ) ;
			email.setAuthentication ( config.getString ( "smtpAccount.dotemplate" ), config.getString( "smtpKey" ) ) ;
			email.setSSL ( true ) ;
			email.setSslSmtpPort ( config.getString ( "smtpPort" ) ) ;
			
			
			/*
			Object[] arguments = { Ease.getConfiguration().getHostname (), key };
			String msg = MessageFormat.format (
				config.getString ( "activationMsg.dotemplate" ), arguments );
			*/

			
			email.addTo ( dest, dest ) ;
			email.setSubject ( "Your template download instructions" ) ;
			email.setFrom ( App.getConfig().getAdminEmail () , "doTemplate" ) ;
			
			String link = "http://" + App.getConfig().getBaseUrl () + "/dt/preloadTheme.do?key=" + key ;
			
			
			// set the html message
			email.setHtmlMsg("<html>Dear user.<br/><br/>Thanks for your payment. Click the following link " +
					" to edit and download your template.<br/><br/>" +
					"<a href=\"" + link+ "\">" + link + "</a></html>");

			if ( log.isDebugEnabled() ){
				log.debug("Download link is " + link ) ;
			}
			
			email.setBounceAddress ( "thierry.ruiz@gmail.com" );
			email.send ();
			
		} catch ( Exception e ) {
			
			throw new AppException ( "Unable to send download url email to '" + dest + "'", e ) ;
		
		}
		
		log.trace ( "download url sent to '" + dest + "'" );
		
		
	}
	

	

}
