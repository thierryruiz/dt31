package com.dotemplate.core.server.util;


import org.apache.commons.pool.PoolableObjectFactory;
import org.apache.commons.pool.impl.GenericObjectPool;

public abstract class Pool<T extends Object> {

	protected GenericObjectPool innerPool ;
		
	protected PoolableObjectFactory factory ;
	
	public Pool(){
		innerPool = new GenericObjectPool ( factory = 
				createFactory() ) ;
	}
	
	@SuppressWarnings("unchecked")
	public T borrow() throws Exception {
		return ( T ) innerPool.borrowObject() ;
	}
	
	public void release ( T i ) throws Exception {
		innerPool.returnObject( i ) ;	
	}
	
	protected abstract PoolableObjectFactory createFactory() ;
	
	
}
