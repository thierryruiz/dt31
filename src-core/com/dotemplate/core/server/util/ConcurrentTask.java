package com.dotemplate.core.server.util;


import java.util.concurrent.Callable;


public abstract class ConcurrentTask< V > implements Callable< V > {
	
	protected ConcurrentTaskContext taskContext ;

	public ConcurrentTaskContext getTaskContext () {
		return taskContext;
	}

	public void applyContext ( ConcurrentTaskContext taskContext ) {
		this.taskContext = taskContext;
	}
	
	
	
}
