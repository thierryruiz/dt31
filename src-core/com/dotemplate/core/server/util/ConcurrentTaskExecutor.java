package com.dotemplate.core.server.util;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import com.dotemplate.core.server.frwk.AppException;


public abstract class ConcurrentTaskExecutor< V > {

	private ConcurrentTaskPool< V > pool ;
	
	private ExecutorService executor ;
		
	protected abstract ConcurrentTaskPool< V > createPool() ;
	
	
	public ConcurrentTaskExecutor( int nbOfThreads ){
		pool = createPool() ;
		executor = Executors.newFixedThreadPool( nbOfThreads  ) ;
	}
	
	
	public List< Future< V > > execute( Collection< ConcurrentTaskContext > contexts  ) throws Exception {
		
		ArrayList < ConcurrentTask < V > > tasks = new ArrayList< ConcurrentTask < V > >() ;
		
		try {
			
			ConcurrentTask< V > task ;
			
			for ( ConcurrentTaskContext context : contexts ){
				
				if ( ! context.isValid () ) continue ;
				
				tasks.add ( task = pool.borrow () ) ;
				
				task.applyContext ( context ) ;
			
			}
			
			List<Future<V>> results = executor.invokeAll ( tasks ) ;
			
			for ( Future<V> future : results ){
				future.get () ; // will rethrow exception raised by a task
			}
			
			return results ;
			
		} catch ( Exception e) {
			
			throw new AppException ( "Error while executing concurrent task...", e ) ;		
		
		}
		
		
		finally {
			for ( ConcurrentTask < V > task : tasks ){
				pool.release ( task ) ;
			}
		}
	}

}
