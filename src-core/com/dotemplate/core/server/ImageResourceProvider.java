package com.dotemplate.core.server;

import java.awt.Dimension;
import java.io.File;
import java.util.Iterator;
import java.util.LinkedHashMap;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.svg.ImageUtils;
import com.dotemplate.core.server.util.Path;
import com.dotemplate.core.shared.canvas.ImageResource;

public class ImageResourceProvider extends DesignResourceProvider< ImageResource >{

	private final static Log log = LogFactory.getLog ( ImageResourceProvider.class );
	
	
	private final static String CANVAS_IMG_DIR = 
		new Path( "WEB-INF").append( "templates" ).append( "_images"  ).append( "canvas" ).asString() ;
		
	
	
	@Override
	public void load() {

		if ( log.isDebugEnabled() ){
			log.debug ( "Loading images..."  ) ;
		}
		
		String [] exts = { "png", "jpg" } ; 
		
		File file ;
		File parent ;
		
		String folder ;
		boolean isFree ;
		ImageResource imageResource ;
		Path imagePath ;
		Path imageThumbnailPath ;
		Dimension dimension ;
		
		if ( log.isDebugEnabled() ){
			log.debug ( "Loading images from " +  CANVAS_IMG_DIR  ) ;
		}
		
		// load images 
		Iterator< ? > files = FileUtils.iterateFiles ( new File ( App.realPath ( CANVAS_IMG_DIR ) ) , exts , true ) ;
		
		
		while ( files.hasNext () ){
			
			file = ( File ) files.next () ;
			parent = file.getParentFile () ;
			imageResource = new ImageResource() ;
			
			isFree =  parent.getName ().equals ( "free" ) ;
			
			folder = ( isFree ) ? "free" : "premium" ;
			
			imageResource.setFree ( isFree ) ;
			
			imageResource.setName ( StringUtils.substringBeforeLast ( file.getName (), "." ) ) ;
			
			imagePath = new Path( CANVAS_IMG_DIR )
				.append( folder )
				.append( file.getName() ) ;
			
			// compute image thumbnail html
			imageThumbnailPath = new Path( "images" )
					.append( "canvas" )
					.append ( folder )
					.append( imageResource.getName () )
					.append2( ".jpg" ) ;
		
			imageResource.setPath( imagePath.asUrl() ) ;
			imageResource.setThumbnailHtml( "<img class='imageResource' src=\"" + imageThumbnailPath.asUrl() + "\" />" ) ;
	
				
			// compute image tags
			// parse "foo-bar-01.png"
			String s = StringUtils.substringBeforeLast( file.getName (), "-" ) ;

			// "foo-bar" > [foo, bar]
			String[] tags = StringUtils.split( s, '-' ) ;
			
			// tag list reprepensation common to all DesignResource
			imageResource.setTags( s.replace( '-', ',' ) ) ; 
			
			
			LinkedHashMap< String, ImageResource > tagImagesMap  ;
			
			for ( String tag : tags ){
				
				tagImagesMap =  resourcesMap.get( tag ) ;
				
				if ( tagImagesMap == null ) {				
				
					tagImagesMap = new LinkedHashMap< String, ImageResource >() ;
					
					resourcesMap.put( tag, tagImagesMap ) ;
					
				}
				
				tagImagesMap.put (  imageResource.getName (), imageResource ) ;
				
			}

			
			if ( ! imageResource.getTags().contains( "logo" ) ){
				resourcesMap.get( ALL_RESOURCES ).put( imageResource.getName (), imageResource ) ;
			}
			
			dimension = ImageUtils.getImageSize( file.getAbsolutePath () )  ;
			
			imageResource.setWidth ( ( int ) dimension.getWidth () ) ;
			imageResource.setHeight ( ( int ) dimension.getHeight () ) ;
		
		}
		
		
	}
	
	/*
	@Override
	protected void sort() {	
		
		LinkedHashMap< String, ImageResource > allImages =  resourcesMap.get( ALL_RESOURCES  ) ;
		
		ArrayList< String > tagList = new ArrayList<  String > ( resourcesMap.keySet() ) ;
		
		Collections.sort( tagList ) ;

		LinkedHashMap< String, LinkedHashMap< String, ImageResource > > sortedResourcesMap = 
				new LinkedHashMap< String, LinkedHashMap< String, ImageResource > >() ;
		
		sortedResourcesMap.put( ALL_RESOURCES, allImages ) ; 
		
		for ( String tag : tagList ){
			
			if ( ALL_RESOURCES.equals( tag ) ) continue ;
			
			sortedResourcesMap.put( tag, resourcesMap.get( tag ) ) ;
		}
		
		resourcesMap = sortedResourcesMap ;
			
		
	}*/
	
	




}
