package com.dotemplate.core.server;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.frwk.AppException;
import com.dotemplate.core.server.svg.SVGFonts;
import com.dotemplate.core.server.symbol.SymbolProvider;
import com.dotemplate.core.server.util.Duration;
import com.dotemplate.core.shared.Design;
import com.dotemplate.core.shared.DesignUpdate;
import com.dotemplate.core.shared.SymbolType;
import com.dotemplate.core.shared.canvas.Graphic;
import com.dotemplate.core.shared.properties.Property;
import com.dotemplate.core.shared.properties.PropertySet;
import com.dotemplate.theme.server.symbol.ColorSchemeProvider;


public abstract class DesignManager< D extends Design > {

	private final static Log log = LogFactory.getLog ( DesignManager.class );

	protected Map< String, SymbolType > symbolTypes ;
	
	public abstract DesignUpdate< D > updateProperty ( Property property ) throws Exception  ;
	
	public abstract void generateDesign() throws AppException ;
	
	
	protected DesignManager() {
		
		symbolTypes = new HashMap< String, SymbolType >() ;
		
		add ( SymbolType.PATTERN ) ;
		add ( SymbolType.TEXTURE ) ;
		add ( SymbolType.OVERLAY ) ;	
		add ( SymbolType.TEXT ) ;
		
	}
	
	
	public void init()  {

		if ( log.isDebugEnabled () ) {
			log.debug ( "Initializing..." );
		}

		loadFonts() ;
		loadResources() ;

		
	}

	
	protected void loadResources() {
		
		if ( log.isDebugEnabled () ) {
			log.debug ( "Loading design resources..." );
		}

		( ( ImageResourceProvider ) App.getSingleton( ImageResourceProvider.class ) ).load() ;
		( ( SVGFontProvider ) App.getSingleton( SVGFontProvider.class ) ).load() ;
		( ( ColorSchemeProvider ) App.getSingleton( ColorSchemeProvider.class ) ).load() ;
		
		
		loadSymbols() ;
		
		
	
	}
	
	
	protected void loadSymbols() {
		
		if ( log.isDebugEnabled () ) {
			log.debug ( "Loading symbols..." );
		}
		
		for ( SymbolType type : symbolTypes.values() ){
			SymbolProvider sp = DesignUtils.getSymbolProvider( type ) ;
			sp.load() ;
			
			// // FIXME harcoded catgory
			sp.loadRandomCategoryContent("generic");
		}

	}
	
	
	protected void add ( SymbolType type  ){
		symbolTypes.put ( type.getFolder(), type  ) ;
	}
	
	
	public Iterator< SymbolType > symbolTypes() {
		return symbolTypes.values ().iterator () ;
	}
	
	public SymbolType getSymbolType ( String folder ){
		return symbolTypes.get( folder ) ;
	}
	
	private void loadFonts () {		
		SVGFonts.load () ;
	}

	/*
	public boolean isValidate() {
		return validate;
	}
	
	public void setValidate( boolean validate ) {
		this.validate = validate;
	}*/
	
	
	
	
	
	
	@Duration
	public ArrayList< Graphic > renderGraphics ( PropertySet set ) throws AppException {

		// ensures tmp folder is created
		File output = new File (  App.get().getWorkRealPath () + "/tmp" ) ;
		
		if ( !output.exists () ){
			try {
				FileUtils.forceMkdir ( output ) ;
			} catch ( IOException e ) {
				throw new AppException ( "Unable to create tmp folder", e ) ;
			}
		}
		
		
		// renders the banner garphics
		return ( ( CanvasRenderer ) App.getSingleton ( 
				CanvasRenderer.class ) ).renderGraphics ( set ) ;
	
		
	}
	
	
	@Duration
	public void renderGraphic ( Graphic graphic ) throws AppException {
		( ( CanvasRenderer ) App.getSingleton ( CanvasRenderer.class ) )
			.renderGraphic ( graphic ) ;
	}

	
	/*
	public ArrayList < Scheme > getColorSchemes(){
		
		if ( log.isDebugEnabled () ) {
			log.debug ( "Get color schemes..." ) ;
		}
		
		return ( ( ColorSchemeProvider ) App.getSingleton( 
				ColorSchemeProvider.class ) ).list() ;
	
	}
	

	public Scheme getColorScheme( String name ){
		
		if ( log.isDebugEnabled () ) {
			log.debug ( "Get color scheme "  + name  ) ;
		}
		
		return ( ( ColorSchemeProvider ) App.getSingleton( 
				ColorSchemeProvider.class ) ).get( name ) ;
	}

	
	public Scheme getScheme ( String name ) {		
		return ( ( ColorSchemeProvider ) App.getSingleton( 
				ColorSchemeProvider.class ) ).get( name ) ;
	}


	public void setColorScheme ( String name ) throws AppException {

		Scheme scheme = ( ( ColorSchemeProvider ) App.getSingleton( 
								ColorSchemeProvider.class ) ).get( name ) ;
		
		if ( scheme == null ){
			throw new AppException ( "Unknown scheme " + scheme )  ;
		}

		App.getDesignContext().getDesign ().setScheme ( name ) ;
		
	}*/

	
	
	public LinkedHashMap< String, LinkedHashMap< String ,PropertySet > > getSymbols ( 
			SymbolType type, String criteria ) {
	
		SymbolProvider sp = DesignUtils.getSymbolProvider( type ) ;
		
		if ( criteria != null ){
			
			// TODO criteria
			return sp.getResourcesMap() ;
			//return sp.filter ( criteria  ) ;
		
		} else {
			
			return sp.getResourcesMap() ;
		
		}
		
		
	}


	
	public void onDesignPurchased ( D design, String gateway, String transactionId, String payer ) throws Exception {

		if ( log.isDebugEnabled () ){
			
			log.debug ( "Unlock design " + design.getUid() ) ;
		
		}
				
		design.setPurchased( true ) ;
				
		App.getDesignContext().put ( DesignContext.WATERMARK, false ) ;
		
    	generateDesign () ;
    	
    	PurchaseInfo purchaseInfo = new PurchaseInfo() ;
    	
    	purchaseInfo.setDownloads( App.getConfig().getMaxDownloads() ) ;
    	purchaseInfo.setGateway( gateway ) ;
    	purchaseInfo.setPayer( payer ) ;
    	purchaseInfo.setTransactionId( transactionId );
    	
    	App.getDesignContext().setPurchaseInfo( purchaseInfo ) ;
    	
    	PurchaseInfoHelper.save() ;
    	
	}



	
}
