package com.dotemplate.core.server;

import java.util.Iterator;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.dotemplate.core.shared.Design;
import com.dotemplate.core.shared.canvas.Graphic;
import com.dotemplate.core.shared.properties.BackgroundImageProperty;
import com.dotemplate.core.shared.properties.BackgroundProperty;
import com.dotemplate.core.shared.properties.ColorProperty;
import com.dotemplate.core.shared.properties.PercentProperty;
import com.dotemplate.core.shared.properties.Property;
import com.dotemplate.core.shared.properties.PropertySet;
import com.dotemplate.core.shared.properties.RefProperty;
import com.dotemplate.core.shared.properties.SizeProperty;
import com.dotemplate.core.shared.properties.StringProperty;
import com.dotemplate.core.shared.properties.TextProperty;


public class DesignLogger {

	private static DesignLogger logger = new DesignLogger() ;
	
	
	public static void log( Design theme ){
		logger.doLog( theme ) ;
	}
	
	public static void log( PropertySet set ){
		logger.doLog( set ) ;
	}
	
	public static void log( Property property ){
		logger.doLog( property ) ;
	}

	
	private void doLog ( Design theme ){
		for ( Iterator<PropertySet> it = theme.getPropertySets().iterator() ;
			it.hasNext() ; ) {
			doLog ( it.next () ) ;
		}
	}
	
	
	private void doLog ( PropertySet set ){
		CustomStringBuilder sb = new CustomStringBuilder( set, 0 ) ;
		doLog( set, sb ) ;
		System.out.println( sb.toString () ) ;
	}
	
	
	private void doLog( Property property ) {
		CustomStringBuilder sb = new CustomStringBuilder( property, 0 ) ;
		doLog( property, sb ) ;
		System.out.println( sb.toString () ) ;
	}	
	
	
	private void doLog ( Property p, CustomStringBuilder tsb ) {
		
		tsb.append( "\n" );
		tsb.append( p.getLongName() ) ;
		tsb.append( "--------------------------------------------------------------------" ) ;
		tsb.append( "uid", p.getUid () ) ;
		tsb.append( "className", p.getClass().getName() ) ;
		
		
		switch ( p.getType() ){
			case Property.COLOR : 
				append ( ( ColorProperty ) p, tsb ) ;
				break ;

			case Property.SIZE : 
				append ( ( SizeProperty ) p, tsb ) ;
				break ;
				
			case Property.PERCENT :
				append ( ( PercentProperty ) p, tsb ) ;
				break ;	
				
			case Graphic.TEXT : 
				append ( ( TextProperty ) p, tsb ) ;
				break ;

			case Graphic.BG : 
				append ( ( BackgroundProperty ) p, tsb ) ;
				break ;
				
				
			case Property.STRING :
				append ( ( StringProperty ) p, tsb ) ;
				break ;
				
			case Property.BGIMG :
				append ( ( BackgroundImageProperty ) p, tsb ) ;
				break ;				
				
			case Property.REF :
				append ( ( RefProperty ) p, tsb ) ;
				break ;

				
				
			default: break ;
		}
		
		tsb.append( "label", p.getLabel() ) ;
		
	
		if ( p.isSet() ){
			append ( ( PropertySet ) p , tsb  ) ;
		} 
	} 
	

	private void append ( PropertySet set, CustomStringBuilder tsb ) {
		
		tsb.append ( "parent" , set.getParent() ) ;
		tsb.append ( "path" , set.getPath() ) ;
		tsb.append ( "styleId" , set.getStyleId() ) ;
		tsb.append ( "selector" , set.getSelectors() ) ;
		tsb.append ( "editable" , set.getEditable() ) ;
		tsb.append ( "enable" , set.getEnable() ) ;
		tsb.append ( "free" , set.getFree() ) ;
		tsb.append ( "icon" , set.getIcon() ) ;
		
		
		for ( Iterator<Property> it = set.getProperties().iterator(); 
			it.hasNext() ; ){
			Property p = it.next() ; 
			CustomStringBuilder tsb2 = new CustomStringBuilder( p, tsb.getIndent() + 1 ) ;
			doLog( p, tsb2 ) ;
			tsb.append( tsb2.toString() ) ;
		} 
	}

	
	private void append ( SizeProperty p, CustomStringBuilder tsb ) {
		tsb.append ( "value", p.getValue() ) ;
		tsb.append ( "editable" , p.getEditable() ) ;
		tsb.append ( "enable" , p.getEnable() ) ;
	}
	
	private void append ( PercentProperty p, CustomStringBuilder tsb ) {
		tsb.append ( "value", p.getValue() ) ;
		tsb.append ( "editable" , p.getEditable() ) ;
		tsb.append ( "enable" , p.getEnable() ) ;
	}

	
	private void append ( BackgroundImageProperty p, CustomStringBuilder tsb ) {
		tsb.append ( "value", p.getValue() ) ;

	}

	
	private void append ( StringProperty p, CustomStringBuilder tsb ) {
		tsb.append ( "value", p.getValue() ) ;
	}
	
	private void append ( ColorProperty p, CustomStringBuilder tsb ) {
		tsb.append ( "value", p.getValue() ) ;
		tsb.append ( "editable" , p.getEditable() ) ;
		tsb.append ( "enable" , p.getEnable() ) ;
	}
	
	private void append ( TextProperty p, CustomStringBuilder tsb ) {
		tsb.append ( "value", p.getValue() ) ;
		tsb.append ( "font", p.getFontFamily() ) ;		
	}
	
	
	private void append ( BackgroundProperty p, CustomStringBuilder tsb ) {
		tsb.append ( "width", p.getWidth () ) ;
		tsb.append ( "height", p.getHeight () ) ;		
	}
	
	
	
	private class CustomStringBuilder extends ToStringBuilder {
		
		private int indent = 0 ;
		
		public CustomStringBuilder ( Object obj, int indent ) {
			super(obj) ;
			
			setDefaultStyle ( ToStringStyle.MULTI_LINE_STYLE ) ;
			
			this.indent = indent ;
			StringBuffer sb = getStringBuffer () ;
			sb.setLength ( 0 ) ;
		
			
			for ( int i = 0 ; i < indent ; i++  ) {
				sb.append ("\t") ;
			}
			
			
			//sb.append ( obj.toString () + "\n"  );
			
		}
		
		
		
		public ToStringBuilder append ( String fieldName, Object obj ) {
			StringBuffer sb = new StringBuffer() ;
			for ( int i = 0 ; i < indent ; i++  ) {
				sb.append ("\t") ;
			}
			
			return super.append ( sb.append ( fieldName ).toString (), obj );
		}
		
		
		public ToStringBuilder append ( String  s ) {
			StringBuffer sb = new StringBuffer() ;
			
			for ( int i = 0 ; i < indent ; i++  ) {
				sb.append ("\t") ;
			}
			
			return super.append ( sb.append ( s ) );
		}
		
		
		public int getIndent () {
			return indent;
		}
		
		
	}
	
	
}
