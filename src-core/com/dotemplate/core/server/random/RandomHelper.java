package com.dotemplate.core.server.random;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.context.Context;

import com.dotemplate.core.server.App;
import com.dotemplate.core.server.DesignUtils;
import com.dotemplate.core.server.RenderEngine;
import com.dotemplate.core.server.frwk.AppRuntimeException;
import com.dotemplate.core.server.symbol.SymbolProvider;
import com.dotemplate.core.server.util.Path;
import com.dotemplate.core.shared.properties.PropertySet;
import com.dotemplate.theme.server.generator.VoidWriter;


public class RandomHelper {
	
	
	public void map( PropertySet set ){
		
		SymbolProvider provider = DesignUtils.getSymbolProvider( set ) ;
		
		PropertySet symbol = DesignUtils.getSymbol( set ) ;
		
		String vm = null ;
		
	}
	
	
	public void randomdMap( String folder, RandomContext symbolRandomContent ) {
		
		RenderEngine renderEngine = App.getRenderEngine () ;
				
		Path mapPath = new Path ( App.realPath ( "WEB-INF" ) )
			.append( "templates").append( "_symbols" )
			.append( folder ).append ( "random" ).append( "generic.map" ) ;
		

		String vm = mapPath.asString() ;

		if ( vm == null || ! renderEngine.templateExists ( vm ) ) ;
					
		VoidWriter writer = new VoidWriter() ;
	
		Context context = new VelocityContext() ;
		
		try {
			renderEngine.generate ( context, vm, writer ) ;
		} catch (Exception e) {
			throw new AppRuntimeException ( "Failed to load random data for symbol " + folder , e ) ;
		}

		
	}
	

}
