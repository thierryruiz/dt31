package com.dotemplate.core.server.random;

import java.util.HashMap;


public class DesignRandomContent {
	
	private HashMap< String, RandomContext > innerMap ;
	
	public DesignRandomContent(){
		innerMap = new 	HashMap< String, RandomContext >() ;
	}
	
	void set( String uid, RandomContext content ){
		innerMap.put(uid, content ) ;
	}
	
}
