package com.dotemplate.core.server.random;

import java.util.HashMap;


public class RandomContext {
	
	HashMap< String, Object > innerContext ;
	
	public RandomContext() {
		innerContext = new HashMap< String, Object >() ;
	}
	
	public Object get( String key ){
		return innerContext.get(key) ;
	}
	
	public void set( String key, Object value ){
		innerContext.put(key, value) ;
	}
	
}


