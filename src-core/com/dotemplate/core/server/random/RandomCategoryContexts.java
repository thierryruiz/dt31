package com.dotemplate.core.server.random;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.RandomUtils;

import com.dotemplate.core.server.frwk.AppRuntimeException;
import com.dotemplate.core.shared.properties.PropertySet;


public class RandomCategoryContexts {
	
	private HashMap< String, HashMap< String, ArrayList< RandomContext > > > map ;
		
	public void load( File randomFolder,  String category ){
		
		File[] mapDirs = randomFolder.listFiles( (FileFilter) DirectoryFileFilter.DIRECTORY ) ;
		
		String [] exts = { "content" } ; 
		
		Collection< File > mapFiles ;
		
		for ( File mapDir : mapDirs ) {
			
			mapFiles = FileUtils.listFiles( mapDir, exts, true) ;
			
			for ( File mapFile : mapFiles ){
				if ( mapFile.getName().startsWith(category + '.' ) ) {
					loadRandomCategoryContentMapFile( mapFile, category, mapDir.getName() ) ;
				}
			}
		}
	}
	
	
	protected void loadRandomCategoryContentMapFile ( File mapFile, String category, String randomId ) {
	
		if( map == null ){
			map = new HashMap< String, HashMap< String, ArrayList< RandomContext > > >() ;
		}
		
		HashMap< String, ArrayList< RandomContext> > categoryContexts = map.get(category) ;
		
		if( categoryContexts == null ){
			
			map.put( randomId , categoryContexts = new HashMap< String, ArrayList< RandomContext > >() ) ;
	
			ArrayList< RandomContext > randomContentList ; 
			
			categoryContexts.put( category, randomContentList = new ArrayList< RandomContext >() ) ;
			
			try {
				
				List< String > lines = FileUtils.readLines( mapFile ) ;
				
				RandomContext context = null  ;

				for ( String line : lines ){
					
					if ( line.length() ==  0 ){
						
						context = null ;
						
		
					} else {
						
						if ( context == null ){
							context = new RandomContext() ;
							randomContentList.add( context ) ;
						}
						
						context.set( StringUtils.substringBefore(line, "=" ).trim() , 
								StringUtils.substringAfter(line, "=" ).trim() ) ;
						
					}
						
				}
							
			} catch (IOException e) {
				throw new AppRuntimeException( "Failed to read category content file " + mapFile.getAbsolutePath() , e ) ;
			}
			
		}
		
	}


	
	public RandomContext get( PropertySet set, String category ) {
		
		String index = ( String ) set.get( "randomId" ) ;
		
		if ( index == null ){
			throw new AppRuntimeException("No randomId defined in property set " + set.getLongName() ) ;
		}
		
		HashMap< String, ArrayList< RandomContext > > categoryContexts = map.get( index ) ;
		
		
		if ( categoryContexts == null ){
			throw new AppRuntimeException("no random content " + index + " defined in property set '" + set.getLongName() + "' for category '" + category + "'" ) ;
		}
		
		ArrayList< RandomContext > contexts = categoryContexts.get(category) ;
		
		if ( contexts == null ){
			throw new AppRuntimeException("no random content defined in property set '" + set.getLongName() + "' for category '" + category + "'"   ) ;

		} 
		
		
		return contexts.get( RandomUtils.nextInt( contexts.size() ) ) ;
		
	}
	
	
}
