package com.dotemplate.core.server;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Future;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.frwk.AppException;
import com.dotemplate.core.server.util.ConcurrentTaskContext;
import com.dotemplate.core.shared.canvas.Graphic;
import com.dotemplate.core.shared.properties.Property;
import com.dotemplate.core.shared.properties.PropertySet;


/**
 * 
 * 
 * @author Thierry Ruiz
 *
 */
public class CanvasRenderer {

	private static Log log = LogFactory.getLog( CanvasRenderer.class ) ;
	
	private static RenderGraphicTaskExecutor renderer = new RenderGraphicTaskExecutor() ;
	
	
	public ArrayList<Graphic> renderGraphics( PropertySet set ) throws AppException {
		
		if ( log.isDebugEnabled () ) {
			log.debug ( "Rendering graphics..." );
		}
		
		ArrayList<Graphic> results = new ArrayList<Graphic>() ;
		
		if (  set.getType () != Property.CANVAS ) {
			throw new AppException ( "PropertySet '" + set.getName() + " is not a canvas." ) ;
		}

		
		Property property ;
		
		
		List<ConcurrentTaskContext> taskContexts = new ArrayList<ConcurrentTaskContext>() ;

		
		for ( Iterator<Property> it = set.getProperties ().iterator () ; it.hasNext () ; ){

			property = it.next () ;
			
			if ( property.getEnable() != null && !property.getEnable().booleanValue() ){
				continue ;
			}
			
		
			switch ( property.getType () ){
			
				case Graphic.BG:
				case Graphic.TEXT :
				case Graphic.IMAGE :
					
					Graphic graphic  = ( Graphic ) property ;
					
					RenderGraphicTask.Context context = new RenderGraphicTask.Context() ;
					
					context.setGraphic ( graphic ) ;
					
					taskContexts.add( context ) ;
						
					results.add ( graphic ) ;
					break ;
					
				default:
					continue ;	
			}
		
		}
		
		
		try {
			
			renderer.execute ( taskContexts ) ;
			
		} catch ( Exception e ) {
		
			throw new AppException ( "Cannot render banner ", e ) ;
		}
		
		return results ;
		
	}
	
	
	public Void  renderGraphic( Graphic graphic ) throws AppException {
		
		if ( log.isDebugEnabled () ) {
			log.debug ( "Rendering graphic " + graphic.getName () );
		}
	
		
		List<ConcurrentTaskContext> taskContexts = new ArrayList<ConcurrentTaskContext>() ;
		
		if ( ! graphic.isEmpty () ) {
			
			RenderGraphicTask.Context context = new RenderGraphicTask.Context() ;
			
			context.setGraphic ( graphic ) ;
			
			taskContexts.add( context ) ;
		
		}
		
		try {
			
			List< Future< Void > > result = renderer.execute ( taskContexts ) ;
			
			// this is the path of the file generated 
			return result.get ( 0 ).get () ;
		
		
		} catch ( Exception e ) {
		
			throw new AppException ( "Cannot render graphic " + graphic.getName () , e ) ;
		
		}
		
		
	}
	
	
	
}
