package com.dotemplate.core.server;

import java.io.Serializable;

public class PurchaseInfo implements Serializable {

	private static final long serialVersionUID = 5935330150853651991L;

	private String gateway ;

	private int downloads ;
	
	private String transactionId; 
	
	private String payer ;
	
	private String affiliateId = "";
	
	
	// TODO
	@SuppressWarnings("unused")
	private String date ;	
	
	public String getGateway() {
		return gateway;
	}

	public void setGateway(String gateway) {
		this.gateway = gateway;
	}

	public int getDownloads() {
		return downloads;
	}

	public void setDownloads(int downloads) {
		this.downloads = downloads;
	}

	
	public void setPayer(String payer) {
		this.payer = payer;
	}
	
	public String getPayer() {
		return payer;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	
	
	public void setAffiliateId(String affiliateId) {
		this.affiliateId = affiliateId;
	}
	
	public String getAffiliateId() {
		return affiliateId;
	}
	
	
	
}
