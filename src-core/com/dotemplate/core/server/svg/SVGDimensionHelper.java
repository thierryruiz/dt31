package com.dotemplate.core.server.svg;

import java.awt.Dimension;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

import org.apache.batik.bridge.BridgeContext;
import org.apache.batik.bridge.DocumentLoader;
import org.apache.batik.bridge.GVTBuilder;
import org.apache.batik.bridge.UserAgent;
import org.apache.batik.bridge.UserAgentAdapter;
import org.apache.batik.dom.svg.SAXSVGDocumentFactory;
import org.apache.batik.util.XMLResourceDescriptor;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.context.Context;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.svg.SVGLocatable;
import org.w3c.dom.svg.SVGRect;


import com.dotemplate.core.server.App;
import com.dotemplate.core.server.frwk.AppRuntimeException;
import com.dotemplate.core.shared.properties.ImageProperty;
import com.dotemplate.core.shared.properties.TextProperty;



public class SVGDimensionHelper {
	
	private static Log log = LogFactory.getLog ( SVGDimensionHelper.class );
	
   	private String parser ; 
	
	private SAXSVGDocumentFactory factory  ;  
	
	private BridgeContext bridgeContext ;
	
	private GVTBuilder builder ;
	
	public SVGDimensionHelper (){	
		
        if ( log.isDebugEnabled() ){
        	log.debug( "new SVGDimensionHelper instance..." ) ;
        }
		
		parser = XMLResourceDescriptor.getXMLParserClassName ();
		factory = new SAXSVGDocumentFactory ( parser );
		UserAgent userAgent = new UserAgentAdapter ();
		bridgeContext = new BridgeContext ( userAgent, new DocumentLoader ( userAgent ) );
		bridgeContext.setDynamicState ( BridgeContext.DYNAMIC );
		builder = new GVTBuilder ();
	}
	
	
	public Dimension getDimensions( TextProperty text ){
		
        if ( log.isDebugEnabled() ){
        	log.debug( "Get TextProperty dimensions for " + text.getName () ) ;
        }
        
        
        try {
			StringWriter writer = new StringWriter() ;
			Context context = new VelocityContext() ;
			context.put ( "text", text ) ;
			context.put( "textValue", StringEscapeUtils.escapeXml ( text.getValue() ) ) ;
			
			try {
				App.getRenderEngine ().generate ( context, "text.svg.vm", writer ) ;
			} catch ( Exception e ) {
				throw new AppRuntimeException ( "Unable to set TextProperty dimensions...", e );
			}
			
			
			if ( log.isInfoEnabled () ){
				log.info ( writer.getBuffer ().toString() ) ;
			}
			
			
			Document doc = null;
			try {
				doc = factory.createDocument ( null, new StringReader( writer.getBuffer ().toString () ) );
			} catch ( IOException e ) {
				throw new AppRuntimeException ( "Unable to set TextProperty dimensions...", e );
			}
			
			builder.build ( bridgeContext, doc );
			
			// get TextProperty bounds
			Element el = doc.getElementById ( "element" ) ;
			SVGRect rect = ( ( SVGLocatable ) el ).getBBox() ;
			
			int width =  ( int ) rect.getWidth () +  text.getPaddingLeft () + text.getPaddingRight () ;
			int height = ( int ) ( rect.getHeight () ) ;
			height = ( int ) ( ( float ) height * 1.33  ) +  text.getPaddingTop () + text.getPaddingBottom () ;
			            
			
			return new Dimension( width, height ) ;
						
			
		} catch ( Exception e ) {
			e.printStackTrace();
			throw new RuntimeException( e ) ;
		}	
	}
	
	
	public void computeDimensions( TextProperty text ){
		
        if ( log.isDebugEnabled() ){
        	log.debug( "Compute TextProperty dimensions for " + text.getName () ) ;
        }
        
        
        try {
        	
        	Dimension d = getDimensions( text ) ;      
			text.setWidth ( new Integer ( ( int ) Math.rint( d.getWidth())) ) ;
			text.setHeight ( new Integer ( ( int ) Math.rint( d.getHeight()) ) ) ;
			
			
		} catch ( Exception e ) {
			e.printStackTrace();
			throw new RuntimeException( e ) ;
		}	
	}
	
	
	
	public void computeDimensions( ImageProperty image ){
		
		if ( log.isDebugEnabled() ){
        	log.debug( "Compute ImageProperty dimensions for " + image.getName () + ", " + image.getValue() ) ;
        }
		
		Dimension d = ImageUtils.getImageSize ( App.realPath ( image.getValue()  ) ) ;    		
		image.setWidth ( ( int ) d.getWidth () ) ;
		image.setHeight ( ( int ) d.getHeight () ) ; 
	}
	

}
