package com.dotemplate.core.server.svg;

import org.apache.batik.transcoder.ErrorHandler;
import org.apache.batik.transcoder.TranscoderException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class TranscoderErrorHandler implements ErrorHandler {
	
	private static Log log = LogFactory.getLog( TranscoderErrorHandler.class ) ;
	
	

	public void error ( TranscoderException e ) throws TranscoderException {
		log( e ) ;
		throw e ;
	}


	public void fatalError ( TranscoderException e ) throws TranscoderException {
		log( e ) ;
		throw e ;
	}


	public void warning ( TranscoderException e ) throws TranscoderException {
		log( e ) ;
		throw e ;
	}

    private void log ( TranscoderException e ){
    	log.error("/n/n------ BATIK TRANSCODER ERROR---------------------------------------" ) ;
    	log.error ( e ) ;
    }

	
}
