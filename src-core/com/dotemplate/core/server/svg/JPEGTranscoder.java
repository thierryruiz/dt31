package com.dotemplate.core.server.svg;

import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

import org.apache.batik.bridge.BridgeContext;
import org.apache.batik.transcoder.SVGAbstractTranscoder;
import org.apache.batik.transcoder.TranscoderInput;
import org.apache.batik.transcoder.TranscoderOutput;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.pool.PoolableObjectFactory;

import com.dotemplate.core.server.App;
import com.dotemplate.core.server.frwk.AppException;
import com.dotemplate.core.server.util.Pool;



public class JPEGTranscoder extends org.apache.batik.transcoder.image.JPEGTranscoder {
	
	private static Log log = LogFactory.getLog( JPEGTranscoder.class );

	protected static JPEGTranscoderPool pool = new JPEGTranscoderPool() ;
	
	private BufferedImage bufferedImage ;
	
	
	protected JPEGTranscoder () {
		super() ;
	    setErrorHandler( new TranscoderErrorHandler () ) ;
	    addTranscodingHint( SVGAbstractTranscoder.KEY_XML_PARSER_VALIDATING, Boolean.FALSE ) ;
	}
	
	
	@Override
	protected BridgeContext createBridgeContext () {
		return new BridgeContextWrapper( userAgent );
	}
		
	protected BridgeContext createBridgeContext(String svgVersion) {
		return new BridgeContextWrapper(userAgent);
    }

	@Override
	public BufferedImage createImage ( int width, int height ) {
		return bufferedImage = super.createImage ( width, height );
	}
	
	
	public  static void writeImage ( BufferedImage image, OutputStream os ) throws Exception {
		
		JPEGTranscoder transcoder = null ;
		TranscoderOutput transcoderOutput = new TranscoderOutput();
		
		try {
			
			transcoder = pool.borrow();
			transcoderOutput.setOutputStream ( os ) ;
			transcoder.writeImage ( image, transcoderOutput ) ;
		
		} catch ( Exception e ) {
		
			throw new AppException( "Unable to get a new instance from JPEGTranscoder pool", e )  ;
		
		}  finally {
			
			try {
				pool.release( transcoder ) ;
	    	} catch ( Exception e ){
	    		log.error ( "Failed to return png transcoder to its pool", e ) ;
	    		transcoder = null ; // hopefully will be garbage collected
	    	}
	    	
	    }
	}
	
	
	public  static void writeImage ( BufferedImage image, float quality, OutputStream os ) throws Exception {
		
		JPEGTranscoder transcoder = null ;
		TranscoderOutput transcoderOutput = new TranscoderOutput();
		
		try {
			
			transcoder = pool.borrow();
			transcoderOutput.setOutputStream ( os ) ;
			transcoder.addTranscodingHint( JPEGTranscoder.KEY_QUALITY, new Float( quality ) );
			
			transcoder.writeImage ( image, transcoderOutput ) ;
		
		} catch ( Exception e ) {
		
			throw new AppException( "Unable to get a new instance from JPEGTranscoder pool", e )  ;
		
		}  finally {
			
			try {
				pool.release( transcoder ) ;
	    	} catch ( Exception e ){
	    		log.error ( "Failed to return jpeg transcoder to its pool", e ) ;
	    		transcoder = null ; // hopefully will be garbage collected
	    	}
	    	
	    }
	}
	
	
	
	public static void transcode ( String svg, Rectangle r, float quality, String output ) 
		throws Exception {
		
		if ( log.isInfoEnabled () ){
			log.info( "Rendering SVG to JPG '" + output + "' (" + r.getWidth() + "," + r.getHeight() + ")" ) ;
		}
		
        if ( log.isDebugEnabled() && App.isDevMode () ){
        	log.info( "\n" + svg  + "\n\n\n") ;
            FileUtils.writeStringToFile ( new File ( output +".svg"  ), svg ) ; // so I can see extension in windows 
        }
		
        File outputFile  = new File ( output ) ;
        
        if ( outputFile.exists () ) FileUtils.forceDelete (  outputFile ) ;
        
		
		transcode ( svg, r , quality, new FileOutputStream ( output ) ) ;
	
	}
	
	
	
	public static BufferedImage transcode ( String svg, Rectangle r, float quality, OutputStream os ) throws Exception {
		

		if ( log.isDebugEnabled() ){
        	log.debug( "Rendering SVG to JPG " ) ;
        }
        
		
		svg = svg.trim () ;
		
	    TranscoderOutput transcoderOutput = new TranscoderOutput();
	    
	    TranscoderInput input = new TranscoderInput();
		
        JPEGTranscoder transcoder ;
        
		try {
			transcoder = pool.borrow();
		} catch ( Exception e ) {
			throw new AppException( 
					"Unable to get a new instance from JPEGTranscoder pool", e )  ;
		}
        
        transcoder.addTranscodingHint( SVGAbstractTranscoder.KEY_WIDTH, new Float( r
                .getWidth()));

        transcoder.addTranscodingHint( SVGAbstractTranscoder.KEY_HEIGHT, new Float( r
                .getHeight()));

        transcoder.addTranscodingHint( SVGAbstractTranscoder.KEY_AOI, r );
        
        transcoder.addTranscodingHint( JPEGTranscoder.KEY_QUALITY, new Float( quality ) );
        
        input.setInputStream( new ByteArrayInputStream ( svg.getBytes() ) );
        
        transcoderOutput.setOutputStream( os );

        try {
        	transcoder.transcode( input, transcoderOutput );
        	
         	return transcoder.getBufferedImage () ;
        	
        } catch ( Exception e ){
			throw new AppException( "Unable to transcode SVG (below) to jpg.\n\n" + svg, e )  ;
        } finally {
        	try {
        		pool.release( transcoder ) ;
        	} catch ( Exception e ){
        		log.error ( "Failed to return jpeg transcoder to its pool", e ) ;
        		transcoder = null ; // hopefully will be garbage collected
        	}
        	
            try {
                os.flush();
                os.close();
            } catch ( Exception e ){
                log.error ( "Unable to close output stream after jpg trancode", e ) ;
            } ;
            
        }
	}
		

	private static class JPEGTranscoderPool extends Pool<JPEGTranscoder> {

		public JPEGTranscoderPool() {
			super() ;
			innerPool.setMaxActive(-1) ;
		}
		
		
		@Override
		protected PoolableObjectFactory createFactory() {
			
			return new PoolableObjectFactory () {
				public void activateObject( Object o ) throws Exception {
				}

				public void destroyObject( Object o ) throws Exception {
				}

				public Object makeObject() throws Exception {
					return new JPEGTranscoder() ;
				}

				public void passivateObject( Object o ) throws Exception {
					JPEGTranscoder t = ( JPEGTranscoder ) o ;
					t.getTranscodingHints().clear() ;
				}

				public boolean validateObject( Object o ) {
					return true;
				}
			} ;
		} 
	};
	

	public BufferedImage getBufferedImage () {
		return bufferedImage;
	}
	
	
	
}
