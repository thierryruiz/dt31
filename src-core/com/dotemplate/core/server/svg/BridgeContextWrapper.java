package com.dotemplate.core.server.svg;

import java.util.Map;

import org.apache.batik.bridge.BridgeContext;
import org.apache.batik.bridge.UserAgent;


public class BridgeContextWrapper extends BridgeContext {
	
	public BridgeContextWrapper ( UserAgent userAgent ) {
		super( userAgent );
	}
	
	public Map< ?,? > getFontFamilyMap () {		
		return SVGFonts.getFontFamilies () ;		
	}
	
}
