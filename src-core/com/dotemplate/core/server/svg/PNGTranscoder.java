package com.dotemplate.core.server.svg;

import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

import org.apache.batik.bridge.BridgeContext;
import org.apache.batik.transcoder.SVGAbstractTranscoder;
import org.apache.batik.transcoder.TranscoderException;
import org.apache.batik.transcoder.TranscoderInput;
import org.apache.batik.transcoder.TranscoderOutput;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.math.RandomUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.pool.PoolableObjectFactory;


import com.dotemplate.core.server.App;
import com.dotemplate.core.server.frwk.AppException;
import com.dotemplate.core.server.util.Pool;


public class PNGTranscoder extends org.apache.batik.transcoder.image.PNGTranscoder {
	
	private static Log log = LogFactory.getLog( PNGTranscoder.class );
	
	private static PNGTranscoderPool pool = new PNGTranscoderPool() ;
	
	private BufferedImage bufferedImage ;
	
	private String currentSvg = "" ;	
	
	
	private PNGTranscoder () {
		
		super() ;
	
		setErrorHandler( new TranscoderErrorHandler (){
	    	@Override
	    	public void fatalError( TranscoderException e )
	    			throws TranscoderException {
	    		log.error( e ) ;
	    		super.fatalError(e);
	    	}
	    	
	    	@Override
	    	public void error( TranscoderException e) throws TranscoderException {
	    		log.error( currentSvg ) ;
	    		log.error( e ) ;
	    		super.error(e);
	    	}
	    	
	    }) ;
	    		
	    addTranscodingHint( SVGAbstractTranscoder.KEY_XML_PARSER_VALIDATING, Boolean.FALSE ) ;
	      
	}
	
	
	
	@Override
	public BufferedImage createImage ( int width, int height ) {
		return bufferedImage = super.createImage ( width, height );
	}
	
	
	public  static void writeImage ( BufferedImage image, OutputStream os ) throws Exception {
		
		PNGTranscoder transcoder = null ;
		TranscoderOutput transcoderOutput = new TranscoderOutput();
		
		try {
			
			transcoder = pool.borrow();
			transcoderOutput.setOutputStream ( os ) ;
			transcoder.writeImage ( image, transcoderOutput ) ;
		
		} catch ( Exception e ) {
		
			throw new AppException( "Unable to get a new instance from PNGTranscoder pool", e )  ;
		
		}  finally {
			
			try {
				pool.release( transcoder ) ;
	    	} catch ( Exception e ){
	    		log.error ( "Failed to return png transcoder to its pool", e ) ;
	    		transcoder = null ; // hopefully will be garbage collected
	    	}
	    	
	    }
	}

	public static BufferedImage transcode ( String svg, Rectangle r, OutputStream os ) throws Exception {
		
		svg = svg.trim () ;

					
		if ( log.isDebugEnabled () ){
			log.debug( "Rendering SVG to PNG " ) ;
		}
		
		//log.warn ( " \n\n\n" + svg ) ;  
		 
		
	    TranscoderOutput transcoderOutput = new TranscoderOutput();
	    TranscoderInput input = new TranscoderInput();
	    
	    PNGTranscoder transcoder ;
	    
	    
		try {
			
			transcoder = pool.borrow();
		
		} catch ( Exception e ) {
			
			throw new AppException( "Unable to get a new instance from PNGTranscoder pool"  , e )  ;
		}

		
		
        transcoder.addTranscodingHint( 
        		SVGAbstractTranscoder.KEY_WIDTH, new Float( r.getWidth() ) );

        transcoder.addTranscodingHint( 
        		SVGAbstractTranscoder.KEY_HEIGHT, new Float(r.getHeight()));

        transcoder.addTranscodingHint( SVGAbstractTranscoder.KEY_AOI, r );
       
        transcoder.setCurrentSvg( svg ) ;
        
        input.setInputStream( new ByteArrayInputStream ( svg.getBytes() ) );
        
        transcoderOutput.setOutputStream( os );
        
        if ( log.isDebugEnabled() ){
            log.debug( "Rasterizing SVG to PNG ") ;
        }

        try {
        	
        	transcoder.transcode( input, transcoderOutput );
        	
        	return transcoder.getBufferedImage () ;
        	
        	
        } catch ( Exception e ){
        
        	File errorLog = new File ( "svg-error." + RandomUtils.nextLong() ) ;
        	FileUtils.writeStringToFile( errorLog  , svg ) ;
        	
        	throw new AppException( "Unable to transcode SVG to png. See svg in " + errorLog.getAbsolutePath() , e )  ;
        
        
        } finally {
        	try {
        		pool.release( transcoder ) ;
        	} catch ( Exception e ){
        		log.error ( "Failed to return png transcoder to its pool", e ) ;
        		transcoder = null ; // hopefully will be garbage collected
        	}
        	
            try {
                os.flush();
                os.close();
            } catch ( Exception e ){
                log.error ( "Unable to close output stream after png trancode", e ) ;
            } ;
            
        }
	}
	
	
	public static void transcode ( String svg, Rectangle r, String output ) 
		throws Exception {
				
		if ( log.isDebugEnabled () ){
			log.debug( "Rendering SVG to PNG '" + output + "' (" + r.getWidth() + "," + r.getHeight() + ")" ) ;
		}
		
		
        if ( log.isDebugEnabled() && App.isDevMode () ){
        	log.debug( "\n" + svg  + "\n\n\n") ;
            FileUtils.writeStringToFile ( new File ( output +".svg.svg"  ), svg ) ; // so I can see extension in windows 
        }
        
		
        File outputFile  = new File ( output ) ;
        
        if ( outputFile.exists () ) FileUtils.forceDelete (  outputFile ) ;
        
        transcode ( svg, r, new FileOutputStream ( output ) ) ;
    
	}
	

	protected BridgeContext createBridgeContext () {
		return new BridgeContextWrapper( userAgent );
	}

	
	protected BridgeContext createBridgeContext(String svgVersion) {
		return new BridgeContextWrapper(userAgent);
    }
	
	
	public BufferedImage getBufferedImage () {
		return bufferedImage;
	}
	
	
	private static class PNGTranscoderPool extends Pool<PNGTranscoder> {

		public PNGTranscoderPool() {
			super() ;
			innerPool.setMaxActive(-1) ;
		}
		
		@Override
		protected PoolableObjectFactory createFactory() {
			return new PoolableObjectFactory () {
				public void activateObject( Object o ) throws Exception {
				}

				public void destroyObject( Object o ) throws Exception {
				}

				public Object makeObject() throws Exception {
					return new PNGTranscoder() ;
				}

				public void passivateObject( Object o ) throws Exception {
					PNGTranscoder t = ( PNGTranscoder ) o ;
					t.getTranscodingHints().clear() ;
				}

				public boolean validateObject( Object o ) {
					return true;
				}
			} ;
		}
	}
	
	public void setCurrentSvg(String currentSvg) {
		this.currentSvg = currentSvg;
	}
	
	public String getCurrentSvg() {
		return currentSvg;
	}

}
