
package com.dotemplate.core.server.svg;

import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.StringWriter;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.context.Context;

import com.dotemplate.core.server.App;
import com.dotemplate.core.server.frwk.AppRuntimeException;

/**
 * 
 * @author Thierry Ruiz
 *
 */
public class ImageUtils {

	
    private static Log log = LogFactory.getLog( ImageUtils.class );

    public static Dimension getImageSize ( String imagePath ){
    	
        if ( log.isDebugEnabled() ){
        	log.debug( "Get image size '" + imagePath + "'...") ;
        }
    	
        BufferedImage image = null;

        try {
            image = ImageIO.read( new FileInputStream ( imagePath ) ) ;
            return new Dimension( image.getWidth(), image.getHeight() ) ;
        } catch (IOException e) {
            throw new RuntimeException ( "Failed to read the image " + imagePath + " from input stream. ", e ) ;
        }
        
    }
    
    public static Dimension getImageSize ( File file ){
    	
        if ( log.isDebugEnabled() ){
        	log.debug( "Get image size " + file.getAbsolutePath() + "'...") ;
        }
    	
        BufferedImage image = null;

        try {
            image = ImageIO.read( new FileInputStream ( file ) ) ;
            return new Dimension( image.getWidth(), image.getHeight() ) ;
        } catch (IOException e) {
            throw new RuntimeException ( "Failed to read the image from input stream.", e ) ;
        }
    }
    
    
    
	public static void exportContentImage( String path ){
		
        if ( log.isDebugEnabled() ){
        	log.debug( "Create image " + path + "'...") ;
        }
    
        path = App.realPath( path ) ;
        
		File destFile = new File ( App.get().getExportDirectoryRealPath() 
				+ "/images/" + 
				path.substring( path.lastIndexOf( File.separator ) ) )   ;
		
		if ( destFile.exists() ) return ;
		
		File srcFile = new File ( path ) ;
		
		try {
			FileUtils.copyFile( srcFile, destFile ) ;
			
		} catch ( IOException e ) {
			throw new AppRuntimeException ( "Unable to create images directory ", e ) ;
		}
	}

    
    /*
	public static void copyContentImage( String path ){
		
        if ( log.isDebugEnabled() ){
        	log.debug( "Create image " + path + "'...") ;
        }
    
        path = App.realPath( "categories/" + path ) ;
        //path = App.realPath( "WEB-INF/templates/_images/content/" + path ) ;
    	
        boolean download = App.getDesignContext().isExportMode() ;
        
		File destFile = new File ( ( download ? App.get().getExportDirectoryRealPath() : App.get().getWorkRealPath () )  + "/images/" + 
				path.substring( path.lastIndexOf( File.separator ) ) ) ;
		
		if ( destFile.exists() ) return ;
		
		File srcFile = new File ( path ) ;
		
		try {
			FileUtils.copyFile( srcFile, destFile ) ;
			
		} catch ( IOException e ) {
			throw new AppRuntimeException ( "Unable to create images directory ", e ) ;
		}
	}*/
	

	
	public static String clipImage( String path, int rx ){

        if ( log.isDebugEnabled() ){
        	log.debug( "Clip image " + path + "'...") ;
        }
        
        boolean export = App.getDesignContext().isExportMode() ;
        
        path = App.realPath( "categories/" + path ) ;

        
        
        
        String url = export ?
        		"images" + path.substring( path.lastIndexOf( File.separator ) ) :
        		App.get().getWorkUri() + File.separator + "images" + 
        			path.substring( path.lastIndexOf( File.separator ) ) ;
        
        
        url = StringUtils.replace( url, ".jpg", ".png" ) ;
        
        
		Dimension d = getImageSize( path ) ;
		
		
		StringWriter writer = new StringWriter() ;
		
		Context context = new VelocityContext( App.getDesignContext() ) ;
		
		context.put( "dimension", d ) ;
		context.put( "image", "" + path ) ;
		context.put( "rx", "" + rx ) ;
		
		
		try {
			
			App.getRenderEngine ().generate ( context, "clipImage.svg.vm", writer ) ;
		
		} catch ( Exception e ) {
			
			throw new AppRuntimeException ( "Cannot render clip image", e ) ;
		
		}

		path = StringUtils.replace( path, ".jpg", ".png" ) ;

		
		String output = export ? 
				App.get().getExportDirectoryRealPath () + "/images/" + 
					path.substring( path.lastIndexOf( File.separator ) ) 
				
				:App.get().getWorkRealPath () + "/images/" + 
					path.substring( path.lastIndexOf( File.separator ) )  ;
				
		try {
			
			PNGTranscoder.transcode( writer.getBuffer().toString(), 
					new Rectangle( d ), output ) ;
			
		} catch (Exception e) {
			throw new AppRuntimeException ( "Cannot render clip image to PNG", e ) ;
		}
		
		
		return  url.replace( '\\', '/' ) ;

	}	
	
		
	public static BufferedImage read ( File file ){		
        try {
            return ImageIO.read( new FileInputStream ( file ) ) ;

        } catch (IOException e) {
            throw new AppRuntimeException ( "Failed to read the image from input stream.", e ) ;
        }
	}
	
}
