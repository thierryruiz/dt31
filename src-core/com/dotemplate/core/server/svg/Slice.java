package com.dotemplate.core.server.svg;

import java.awt.Rectangle;

public class Slice extends Rectangle {
	
	private static final long serialVersionUID = 1535069396909985254L;
	
	private boolean png = false ;
	
	float quality = ( float ) 0.9 ;
	
	private String name ;
	
	
	public void setQuality ( float quality ) {
		this.quality = quality;
	}
	
	public float getQuality () {
		return quality;
	}

	public String getName () {
		return name;
	}

	public void setName ( String name ) {
		this.name = name;
	}

	public boolean isPng () {
		return png;
	}

	public void setPng ( boolean png ) {
		this.png = png;
	} 
	

}
