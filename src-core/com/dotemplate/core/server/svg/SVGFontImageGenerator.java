/**
 * Copyright Thierry Ruiz ( Athesanet ) 2004 - 2008
 * All rights reserved.
 *
 * Created on 17 avr. 08 : 18:42:45
 */




package com.dotemplate.core.server.svg;




import java.awt.Dimension;
import java.awt.Rectangle;
import java.io.ByteArrayInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Properties;

import org.apache.batik.transcoder.ErrorHandler;
import org.apache.batik.transcoder.TranscoderException;
import org.apache.batik.transcoder.TranscoderInput;
import org.apache.batik.transcoder.TranscoderOutput;
import org.apache.batik.transcoder.image.JPEGTranscoder;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.context.Context;

import com.dotemplate.core.server.App;
import com.dotemplate.core.server.frwk.AppException;



/**
 * @author Thierry Ruiz
 * 
 */
public class SVGFontImageGenerator extends VelocityEngine {

	private final static String[] _freeFonts = { "Arial", "ArialNarrow", "Century", "Chumbly", "Franklin", "Garamond", "Impact", 
		"Planet", "Trebuchet", "Verdana" };


	public static void main ( String[] args ) {

		SVGFontImageGenerator generator = new SVGFontImageGenerator ();
		
		Properties properties = new Properties() ;
		properties.setProperty ( VelocityEngine.FILE_RESOURCE_LOADER_PATH, "WEB-INF/templates" ) ;

		System.out.println ( "Working dir :" +  System.getProperty ( "user.dir" ) ) ;
		
		try {
			generator.init ( properties ) ;
		} catch ( Exception e1 ) {
			e1.printStackTrace();
		}
		
		
		
		for ( int i = 0; i < _freeFonts.length; i++ ) {
			try {
				generator.generate ( _freeFonts [i], true );
			} catch ( Exception e ) {
				System.out.println ( "Unable to generate image for font " + _freeFonts[i] );
				e.printStackTrace ();
				return;
			}
		}

	}


	private SVGFontImageGenerator () {

	}


	private void generate ( String fontFamily, boolean free ) throws Exception {

		String vmTemplate = "svgfont.svg.vm";

		Context context = new VelocityContext ();
		context.put ( "fontFamily", fontFamily );

		StringWriter writer = null;

		// render the layout with nested view
		try {
			// writer = new FileWriter( "WEB-INF/templates/_fonts/" + fontFamily
			// + ".svg" ) ;
			writer = new StringWriter ();
			Template vlTemplate = getTemplate ( vmTemplate );
			vlTemplate.merge ( context, writer );
						
			// render image
			renderAsJpg ( writer.getBuffer ().toString (), App.realPath( "images/svgfonts/free/" + fontFamily + ".jpg" )  ) ;
			
			
		} catch ( Exception e ) {
			e.printStackTrace ();
			throw new Exception ( "Cannot render template '" + vmTemplate + "'.", e );
		} finally {
			try {
				writer.flush ();
				writer.close ();
			} catch ( Exception e ) {
				e.printStackTrace ();
			}
		}

	}


	public void renderAsJpg ( String svg, String out ) throws IOException, TranscoderException, AppException {

		FileOutputStream fos = null;

		try {

			JPEGTranscoder transcoder = new JPEGTranscoder ();
			transcoder.setErrorHandler ( new ErrorHandler () {

				public void error ( TranscoderException e ) throws TranscoderException {
					e.printStackTrace ();

					throw e;
				}


				public void fatalError ( TranscoderException e ) throws TranscoderException {
					e.printStackTrace ();

					throw e;
				}


				public void warning ( TranscoderException e ) throws TranscoderException {
					e.printStackTrace ();

					throw e;
				}

			} );

			Dimension d = new Dimension( 300, 60 ) ;
			
			TranscoderOutput output = new TranscoderOutput ();
			TranscoderInput input = new TranscoderInput ();

			transcoder.addTranscodingHint(JPEGTranscoder.KEY_WIDTH, new
					Float( d.getWidth() ));

			transcoder.addTranscodingHint(JPEGTranscoder.KEY_HEIGHT, new
			 Float( d.getHeight() ) );

			Rectangle rectangle = new Rectangle( d ) ;
			 transcoder.addTranscodingHint(JPEGTranscoder.KEY_AOI, rectangle);

			transcoder.addTranscodingHint ( JPEGTranscoder.KEY_XML_PARSER_VALIDATING, Boolean.FALSE );
			transcoder.addTranscodingHint ( JPEGTranscoder.KEY_QUALITY, new Float ( 0.8 ) );

			fos = new FileOutputStream ( out );
			input.setInputStream ( new ByteArrayInputStream ( svg.getBytes () ) );
			output.setOutputStream ( fos );

			transcoder.transcode ( input, output );

		} catch ( Exception e ) {
			e.printStackTrace () ;
		} finally {
			try {
				fos.flush ();
				fos.close ();
			} catch ( Exception e ) {
				e.printStackTrace () ;
			}
		}

	}

}
