package com.dotemplate.core.server.svg;

import java.awt.Dimension;
import java.io.IOException;
import java.io.StringReader;

import org.apache.batik.bridge.BridgeContext;
import org.apache.batik.bridge.DocumentLoader;
import org.apache.batik.bridge.GVTBuilder;
import org.apache.batik.bridge.UserAgent;
import org.apache.batik.bridge.UserAgentAdapter;
import org.apache.batik.dom.svg.SAXSVGDocumentFactory;
import org.apache.batik.util.XMLResourceDescriptor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.svg.SVGLocatable;
import org.w3c.dom.svg.SVGRect;

import com.dotemplate.core.server.frwk.AppRuntimeException;



public class SvgHelper {
	
	private final static Log log = LogFactory.getLog ( SvgHelper.class );
	
	private String parser ; 
	
	private SAXSVGDocumentFactory factory  ;  
	
	private BridgeContext bridgeContext ;
	
	private GVTBuilder builder ;
	
	
	public SvgHelper (){	
		parser = XMLResourceDescriptor.getXMLParserClassName ();
		factory = new SAXSVGDocumentFactory ( parser );
		UserAgent userAgent = new UserAgentAdapter ();
		bridgeContext = new BridgeContext ( userAgent, new DocumentLoader ( userAgent ) );
		bridgeContext.setDynamicState ( BridgeContext.DYNAMIC );
		builder = new GVTBuilder ();
	}
	
	
	public Dimension computeSVGElementSize( String svgDoc, String id ){
		
		
        if ( log.isDebugEnabled() ){
        	log.debug( "Compute SVG element bounds" ) ;
        	log.debug ( "/n" + svgDoc ) ;
        }
        
        try {
				
			Document doc = null;
			try {
				doc = factory.createDocument ( null, new StringReader( svgDoc ) );
			} catch ( IOException e ) {
				throw new AppRuntimeException ( "Unable to set TextProperty dimensions...", e );
			}
			
			builder.build ( bridgeContext, doc );
			
			// get bounds
			Element el = doc.getElementById ( id ) ;
			SVGRect rect = ( ( SVGLocatable ) el ).getBBox() ;
			

			/*
			SVGOMTextElement textEl = ( SVGOMTextElement ) doc.getElementById ( "title1" ) ;
			
			SVGTextElementBridge bridgeCtx =  ( SVGTextElementBridge ) textEl.getSVGContext () ;

			TextNode textNode =  ( TextNode ) bridgeContext.getGraphicsNode( textEl ) ;
			
			List<?> list =  ( List<?> ) textNode.getAttributedCharacterIterator ().getAttribute ( 
									org.apache.batik.gvt.text.GVTAttributedCharacterIterator.TextAttribute.GVT_FONT_FAMILIES) ;
			
			GVTFont gvtFont ;
		
			
			GVTFontFamily font = ( GVTFontFamily ) list.get ( 0 ) ;
			
			GVTFontFace fontFace = font.getFontFace () ;
			
			
			
			float ascent = fontFace.getAscent () ;
			float descent = fontFace.getDescent () ;
			float units = fontFace.getUnitsPerEm () ;
			*/
			
			if ( rect == null ){
				return new Dimension ( 10, 10 ) ;
			}
			
			return new Dimension( ( int ) rect.getWidth (), ( int ) rect.getHeight () ) ;
			
			
		} catch ( Exception e ) {
			throw new RuntimeException( e ) ;
		}
		
		
	}
	
	/*
	public Dimension computeSVGTextSize( String svgDoc, String id ){
		
        if ( log.isDebugEnabled() ){
        	log.debug( "Compute SVG element bounds" ) ;
        	log.debug ( "/n" + svgDoc ) ;
        }
        
        try {
				
			Document doc = null;
			try {
				doc = factory.createDocument ( null, new StringReader( svgDoc ) );
			} catch ( IOException e ) {
				throw new AppRuntimeException ( "Unable to set TextProperty dimensions...", e );
			}
			
			builder.build ( bridgeContext, doc );
			
			// get TextProperty bounds
			Element el = doc.getElementById ( id ) ;
			//TextNode text =  ( TextNode ) bridgeContext.getGraphicsNode( el ) ;
			ShapeNode rectNode =  ( ShapeNode ) bridgeContext.getGraphicsNode( el ) ;
			
			//@SuppressWarnings("unused")
			//AttributedCharacterIterator aci =  text.getAttributedCharacterIterator () ;
			
			
			//List<?> list =  ( List<?> ) text.getAttributedCharacterIterator ().getAttribute ( 
			//						org.apache.batik.gvt.text.GVTAttributedCharacterIterator.TextAttribute.GVT_FONT_FAMILIES) ;
			
			//GVTFont gvtFont ;
			
			
			
			//GVTFontFamily font = ( GVTFontFamily ) list.get ( 0 ) ;
			
			//GVTFontFace fontFace = font.getFontFace () ;
			
			//float ascent = fontFace.getAscent () ;
			//float descent = fontFace.getDescent () ;
			

			// SVGRect rect = ( ( SVGLocatable ) text ).getBBox() ;
			SVGRect rect = ( ( SVGLocatable ) rectNode ).getBBox() ;
			
			
			return new Dimension( ( int ) rect.getWidth (), ( int ) rect.getHeight () ) ;
			
			
		} catch ( Exception e ) {
			throw new AppRuntimeException( e ) ;
		}
		
		
	}*/
	
	
		
	
}
