
package com.dotemplate.core.server;

import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.net.URL;

import javax.imageio.ImageIO;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.velocity.VelocityContext;

import com.dotemplate.core.server.frwk.AppException;
import com.dotemplate.core.server.svg.PNGTranscoder;
import com.dotemplate.core.server.svg.SvgHelper;
import com.dotemplate.core.server.svg.VoidOutputStream;
import com.dotemplate.core.server.util.ConcurrentTask;
import com.dotemplate.core.server.web.GetCanvasImage;
import com.dotemplate.core.shared.canvas.Graphic;
import com.dotemplate.core.shared.properties.BackgroundProperty;
import com.dotemplate.core.shared.properties.ImageProperty;
import com.dotemplate.core.shared.properties.TextProperty;
import com.dotemplate.theme.server.ThemeApp;
import com.dotemplate.theme.server.ThemeAppConfig;


public class RenderGraphicTask extends ConcurrentTask< Void > {

	private final static Log log = LogFactory.getLog ( RenderGraphicTask.class ) ;

	private SvgHelper svgHelper ;
	
	
	/* (non-Javadoc)
	 * @see java.util.concurrent.Callable#call()
	 */
	@Override
	public Void call () throws Exception {

		Context ctx =  ( Context ) super.getTaskContext () ;
		
		DesignSession.set( ctx.getDesignSession() ) ;
		
		Graphic graphic = ctx.getGraphic () ;

		Dimension dimension = null ;
		
		StringWriter writer = new StringWriter();

		// String output = null ;
		
		// String imageName ;
		
		try {
			
			switch ( graphic.getType() ){
			
					case Graphic.BG :
						dimension = renderBackground ( ( BackgroundProperty ) graphic, writer ) ; 
						break ;
					
					case Graphic.TEXT	:
						dimension = renderText ( ( TextProperty ) graphic, writer ) ; 
						break ;	
		
					case Graphic.IMAGE	:
						dimension = renderImage( ( ImageProperty ) graphic, writer ) ; 
						break ;	
			
					default: throw new AppException ( "Unknown graphic type " + graphic.getName() ) ;
						
			}
			
			BufferedImage image = PNGTranscoder.transcode (
					writer.getBuffer ().toString (), 
						new Rectangle ( dimension ), 
							new VoidOutputStream() ) ;
				
			
			CanvasImage cachedImage = new CanvasImage( graphic.getName(), image ) ;
			
			App.getDesignContext().getCanvasImageCache().setImage( cachedImage ) ;
			
			String webContext = ( ( ThemeAppConfig ) ThemeApp.getConfig() ).getWebContext() ;

			graphic.setWorkingUri ( webContext + GetCanvasImage.URL_PREFIX + 
					cachedImage.getId() + "&" + RandomStringUtils.randomAlphanumeric( 6 ) ) ;
			
			
			/*
			imageName =  new StringBuffer( graphic.getName ()  )
				.append( "." )
				.append( RandomStringUtils.randomNumeric ( 4 ) )
				.append( ".png" ).toString () ;
			
			
			
			output = new StringBuffer( App.getWorkRealPath () )
				.append ( File.separator )
				.append( "tmp")
				.append( File.separator )
				.append( imageName ).toString () ;
			

			PNGTranscoder.transcode ( writer.getBuffer ().toString (), new Rectangle ( dimension ), output ) ;
			
			String workingUri = graphic.getWorkingUri () ;
			
			if ( workingUri != null ){
				// remove previous working file 				
				FileUtils.deleteQuietly ( new File ( App.realPath ( workingUri ) )  ) ;
			} 
			
			graphic.setWorkingUri ( new WorkPath().append ( "tmp" ).append ( imageName ).asUrl () );
			*/
					
			
		} finally {
			DesignSession.clear() ;
		}
		
		
		return null ;
			
	}

	
	protected Dimension renderBackground( BackgroundProperty background, Writer writer ) throws Exception {
		
		if ( log.isDebugEnabled () ){
			log.debug ( "Rendering background graphic "  + background.getName () ) ;
		}
		
		
		VelocityContext context = new VelocityContext( App.getDesignContext() ) ;
		
		context.put ( "background", background ) ;
		
		App.getRenderEngine ().generate ( context, "background.svg.vm", writer ) ;
		
	
		return new Dimension ( background.getWidth (), background.getHeight () ) ;
	
	}
	
	

	protected Dimension renderImage( ImageProperty image, Writer writer ) throws Exception {

		if ( log.isDebugEnabled () ){
			log.debug ( "Renderind image graphic "  + image.getName () ) ;
		}		
		
		BufferedImage bf ;
		
		
		if ( image.getValue ().startsWith( "http"  ) ){
		
			try {
				
				bf = ImageIO.read ( new URL ( image.getValue() ) ) ;
				
			} catch ( IOException e ) {
				
				throw new AppException ( "Failed to render Graphic " + image.getName () , e ) ;
			}

		} else {

			
			String path = App.realPath ( image.getValue () ).replace( '/' , File.separatorChar ) ;
			
			try {
				
				bf = ImageIO.read ( new FileInputStream ( path  ) ) ;
				
			} catch ( IOException e ) {
				
				throw new AppException ( "Failed to render Graphic " + image.getName () , e ) ;
			}
		
		}
		
   		image.setWidth ( ( int ) bf.getWidth () ) ;
		image.setHeight ( ( int ) bf.getHeight () ) ; 
		
		
		VelocityContext context = new VelocityContext( App.getDesignContext() ) ;
	
		context.put ( "image", image ) ;
		
		App.getRenderEngine ().generate ( context, "image.svg.vm", writer ) ;
		
		int renderedWidth = Math.round (  image.getWidth ()  * image.getScale() ) ;
		int renderedHeight = Math.round  ( image.getHeight ()  * image.getScale() ) ;
		
	
		image.setRenderedWidth ( renderedWidth ) ;
		image.setRenderedHeight ( renderedHeight ) ;
		
		
		return new Dimension ( renderedWidth, renderedHeight ) ;
		
	}

	
	
	
	protected Dimension renderText( TextProperty text, Writer writer ) throws Exception {

		if ( log.isDebugEnabled () ){
			log.debug ( "Renderind text graphic "  + text.getName () ) ;
		}
		
		VelocityContext context = new VelocityContext( App.getDesignContext() ) ;
		
		context.put ( "text", text ) ;
		
		App.getRenderEngine ().generate ( context, "text.svg.vm" , writer ) ;
		
		
		if ( svgHelper == null ){
			svgHelper = new SvgHelper() ;
		}
		
		Dimension d = svgHelper.computeSVGElementSize ( 
			( ( StringWriter ) writer ) .getBuffer ().toString (), "element" ) ;
		
		
		text.setWidth( ( int ) d.getWidth()  ) ;
		text.setHeight( ( int ) d.getHeight()  ) ;
		
		d = new Dimension ( ( int ) d.getWidth() + 2*TextProperty.PADDING, ( int )  d.getHeight() + 2* TextProperty.PADDING  ) ;
		
		
		// compute graphic dimension 
		//SVGFont font = ( ( SVGFontProvider ) App.getSingleton ( 
		//						SVGFontProvider.class ) ).get ( text.getFontFamily () ) ;
		
		/*int height = ( int ) ( ( font.getAscent() + font.getDescent() )  * text.getFontSize () / 100 ) ;
		
		d.setSize( d.getWidth(), height ) ;
		
		text.setWidth( ( int ) d.getWidth() ) ;
		text.setHeight( height ) ;
		*/
		
		text.setRenderedWidth ( ( int ) d.getWidth() ) ;
		text.setRenderedHeight ( ( int ) d.getHeight () ) ;
		
		
		return d ;
		
	}
	
	
	
	static class Context extends DesignTaskContext {
		
		private Graphic graphic ;
		
		
		public Graphic getGraphic () {
			return graphic;
		}
		
		
		public void setGraphic ( Graphic graphic ) {
			this.graphic = graphic;
		}


		public boolean isValid () {
			return ! graphic.isEmpty () ;
		}
	
	} ;
	
}
