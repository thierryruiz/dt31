package com.dotemplate.core.server;


public class AppConfig {
	
	private String vmTemplatesDir ;
	
	private String renderEngine ;
	
	private String  renderEngineConf ;
	
	private String designManager ;
	
	private String designFactory ;
	
	private String workRoot;

	private String designsPath;
	
	private String modelsPath;
		
	private String designImagesPath;

	private String uploadDirectory;

	private String schemesPath;

	private String baseUrl ; // computed
	
	private boolean devMode;
	
	private boolean superDevMode;
	
	private boolean testMode;
	
	private String gwtUri ;
	
	private String adminEmail ;
	
	private int maxDownloads ;
	
	
	public String getAdminEmail() {
		return adminEmail;
	}

	public void setAdminEmail(String adminEmail) {
		this.adminEmail = adminEmail;
	}

	public String getWorkRoot() {
		return workRoot;
	}

	public void setWorkRoot(String workRoot) {
		this.workRoot = workRoot;
	}

	public String getDesignsPath() {
		return designsPath;
	}
	
	public void setDesignsPath(String designsPath) {
		this.designsPath = designsPath;
	}

	public String getModelsPath() {
		return modelsPath;
	}

	public void setModelsPath(String modelsPath) {
		this.modelsPath = modelsPath;
	}
	
	public String getDesignImagesPath() {
		return designImagesPath;
	}

	public void setDesignImagesPath(String designImagesPath) {
		this.designImagesPath = designImagesPath;
	}

	public String getUploadDirectory() {
		return uploadDirectory;
	}

	public void setUploadDirectory(String uploadDirectory) {
		this.uploadDirectory = uploadDirectory;
	}

	public String getSchemesPath() {
		return schemesPath;
	}

	public void setSchemesPath(String schemesPath) {
		this.schemesPath = schemesPath;
	}

	public boolean isDevMode() {
		return devMode;
	}

	public void setDevMode(boolean devMode) {
		this.devMode = devMode;
	}

	public boolean isTestMode() {
		return testMode;
	}

	public void setTestMode(boolean testMode) {
		this.testMode = testMode;
	}
	
	public boolean isSuperDevMode() {
		return superDevMode;
	}
	
	public void setSuperDevMode(boolean superDevMode) {
		this.superDevMode = superDevMode;
	}


	public String getGwtUri() {
		return gwtUri;
	}
	
	public void setGwtUri(String gwtUri) {
		this.gwtUri = gwtUri;
	}

	public String getDesignManager() {
		return designManager;
	}

	public void setDesignManager(String designManager) {
		this.designManager = designManager;
	}

	public String getDesignFactory() {
		return designFactory;
	}

	public void setDesignFactory(String designFactory) {
		this.designFactory = designFactory;
	}

	public String getBaseUrl() {
		return baseUrl;
	}

	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}
	
	public int getMaxDownloads() {
		return maxDownloads;
	}

	public void setMaxDownloads(int maxDownloads) {
		this.maxDownloads = maxDownloads;
	}

	public void setRenderEngine(String renderEngine) {
		this.renderEngine = renderEngine;
	}
	
	public String getRenderEngine() {
		return renderEngine;
	}
	
	public void setRenderEngineConf(String renderEngineConf) {
		this.renderEngineConf = renderEngineConf;
	}
	
	public String getRenderEngineConf() {
		return renderEngineConf;
	}
	
	public String getVmTemplatesDir() {
		return vmTemplatesDir;
	}
	
	public void setVmTemplatesDir(String vmTemplatesDir) {
		this.vmTemplatesDir = vmTemplatesDir;
	}
	
}
