package com.dotemplate.core.server.watcher;

public interface FileUpdateListener {
	
	public void onFileUpdated( String path ) ;
	
	public void onFileCreated( String path ) ;
	
	public void onFileDeleted( String path ) ;
	

}
