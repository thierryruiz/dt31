package com.dotemplate.core.server;

import java.beans.IntrospectionException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Stack;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.betwixt.io.BeanWriter;
import org.apache.commons.betwixt.io.WriteContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;

import com.dotemplate.core.server.frwk.AppRuntimeException;
import com.dotemplate.core.shared.Design;
import com.dotemplate.core.shared.properties.Property;
import com.dotemplate.core.shared.properties.PropertySet;


public class DesignBeanWriter extends BeanWriter {

	
	@SuppressWarnings("unused")
	private final static Log log = LogFactory.getLog( DesignBeanWriter.class );

	private Stack < Object > beanStack = new Stack<Object>();
	
	private Stack < PropertySet > setStack = new Stack< PropertySet >();
	
	private Stack < Attributes > attributesStack = new Stack< Attributes >() ;
	
	private boolean writeCurrentProperty = true ;
	
	protected Design design ;
	
	
	public DesignBeanWriter( OutputStream os, String betwixtResource ) {

		super( os );

		setWriteEmptyElements( false ) ;
		
		getBindingConfiguration().setMapIDs( false );
		
		getXMLIntrospector().getConfiguration().setAttributesForPrimitives( true );

		enablePrettyPrint();

		InputStream in = Design.class.getClassLoader().getResourceAsStream( betwixtResource );

		try {

			getXMLIntrospector().register( new InputSource( in ) );

		} catch ( Exception e ) {

			throw new AppRuntimeException( "Failed while initializing DesignBeanWriter", e );
		}
		
	
	}

	
	
	@Override
	protected void pushBean( Object object ) {
		beanStack.push( object );
		super.pushBean( object );
	}
	
	
	@Override
	protected Object popBean() {
		beanStack.pop();
		Object object = super.popBean();
		return object;
	}


	protected synchronized PropertySet peekSet() {
		return setStack.peek();
	}

	protected synchronized PropertySet popSet() {
		return setStack.pop();
	}

	protected synchronized void pushSet(PropertySet set) {
		setStack.push(set);
	}
	

	protected synchronized void pushAttributes(Attributes attributes) {
		attributesStack.push( attributes );
	}

	protected synchronized Attributes peekAttributes() {
		return attributesStack.peek();
	}
	
	protected synchronized Attributes popAttributes() {
		return attributesStack.pop();
	}

	@Override
	public void write( Object bean ) throws IOException, SAXException,
			IntrospectionException {
		
		this.design = ( Design ) bean ;
		
		super.write(bean);
	}
	
	
	
	@Override
	protected void startElement( WriteContext context, String uri,
			String localName, String qualifiedName, Attributes attributes )
			throws IOException, SAXException {

		Object bean = beanStack.peek();

		PropertySet set;
		Property property; 
		Property symbolProperty;
		
	
		if ( isSetElement( qualifiedName ) ){
			
			set = ( PropertySet ) bean;

			PropertySet symbol = DesignUtils.getSymbol( set );

			pushSet( set );
			
			
			attributes = getDiffAttributes( set, symbol, attributes ) ;

			
		} else if ( isPropertyElement( qualifiedName ) ) {

			writeCurrentProperty = true ;
			
			property = ( Property ) bean ;
			
			set = peekSet();

			PropertySet symbol = DesignUtils.getSymbol( set );

			symbolProperty = symbol.getProperty( property.getName() );
			
			if ( symbolProperty != null ) {
						
				attributes = getDiffAttributes( property, symbolProperty, attributes ) ;
				
				if ( attributes.getLength() == 1 ){
					writeCurrentProperty = false ;
					return ;
				}
			}

		}  		
				

		super.startElement( context, uri, localName, qualifiedName, attributes );

		
	}

	
	
	@Override
	protected void endElement( WriteContext context, String uri,
			String localName, String qualifiedName) throws IOException, SAXException  {
		
			
		if ( isSetElement( qualifiedName ) ) {
			popSet();			
		} else if ( isPropertyElement( qualifiedName ) ){
			if ( !writeCurrentProperty ){
				return ;
			}
		}
	
		super.endElement( context, uri, localName, qualifiedName);
	}


	
	
	protected Attributes getDiffAttributes( Property p1, Property p2, Attributes attributes ) throws RuntimeException {
		
		ArrayList<String> unmodified = new ArrayList< String >() ;
		
		Object o1 , o2 ;
		
		String attribute ;
		
		
		for ( int i = 0 ; i < attributes.getLength() ; i++ ){
			
			attribute = attributes.getQName( i ) ;
			
			if ( "extends".equals ( attribute ) ){
				continue ;
			}
			
			if ( "name".equals ( attribute ) ){
				continue ;
			}	
			
			
			try {

				o1 = PropertyUtils.getSimpleProperty( p1, attribute ) ;
				o2 = PropertyUtils.getSimpleProperty( p2, attribute ) ;

				if ( o1 == null && o2 == null ){
					
					unmodified.add( attribute ) ;
				
				} else if ( o1!= null && o2 != null && 
						isUnmodified( o1, o2, PropertyUtils.getPropertyDescriptor( 
								p1, attribute ).getPropertyType() ) ){
					
					unmodified.add( attribute ) ;
				
				}
				
			} catch ( Exception e ) {
				
				throw new AppRuntimeException( "Failed to export : Error while getting diff attributes for attribute " + attribute 
						+ " " + p1.getLongName()  , e ) ;
			
			} 
			
		}
		
		
		
		ModifiedAttributes modified = new ModifiedAttributes( attributes ) ;
		
		for ( String a : unmodified ){
			
			modified.removeAttribute( a ) ;
		
		}
		
		return modified ;
		
	}
	
	
	
	protected boolean isUnmodified ( Object o1, Object o2, Class< ? > type ){
		
		if ( type == Boolean.class ) {
			
			return ( ( Boolean ) o1 ).booleanValue() ==  ( ( Boolean ) o2 ).booleanValue() ;
		
		} else if ( type == String.class ) {
		
			return ( ( String ) o1 ).equals( ( String ) o2 ) ;
		
		} else {
			
			return false ;
		
		}
		
	}
	
	
	private class ModifiedAttributes extends AttributesImpl {

		public ModifiedAttributes( Attributes attributes ) {
			super(attributes);
		}

		
		public void removeAttribute( String name ) {
			int length = getLength();
			int index = -1;
			for (int i = 0; i < length; i++) {
				if (getQName(i).equals(name)) {
					index = i;
					break;
				}
			}
			if (index > -1) {
				super.removeAttribute(index);
			}
		}
		
	}

	
	protected boolean isSetElement( String qualifiedName ) {
		return 
			qualifiedName.equals( "set" ) || 
			qualifiedName.equals( "canvas" ) || 
			qualifiedName.equals( "background" ) || 
			qualifiedName.equals( "text" ) ;
	}
	
	
	protected boolean isPropertyElement ( String qualifiedName ) {
		return 
			qualifiedName.equals( "size" ) ||
			qualifiedName.equals( "color" ) ||
			qualifiedName.equals( "string" ) ||
			qualifiedName.equals( "boolean" ) ||
			qualifiedName.equals( "percent" ) || 
			qualifiedName.equals( "ref" )||
			qualifiedName.equals( "bgimg" );		
	}


}
