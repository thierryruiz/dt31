/**
 * Copyright Thierry Ruiz ( Athesanet ) 2004 - 2008
 * All rights reserved.
 *
 * Created on 4 sept. 08 : 11:13:34
 */
package com.dotemplate.core.server;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.MDC;

import com.dotemplate.core.server.frwk.AppRuntimeException;
import com.dotemplate.core.shared.Design;



/**
 * 
 * A unique object storing all design session related data. This object is
 * the unique object attached to HTTP Session. It is stored in
 * the thread local to be accessible from all thread processing.
 * 
 * @author Thierry Ruiz
 *
 */
public class DesignSession implements HttpSessionBindingListener {
	
	@SuppressWarnings("unused")
	private final static Log log = LogFactory.getLog ( DesignSession.class );
	
	private DesignContext<? extends Design> designContext ;
	
	private Map<String, DesignContext <? extends Design > > contexts = 
		new HashMap<String, DesignContext< ? extends Design > >() ;
	
	//private UserActivity userActivity = new UserActivity();
	
	private boolean editing ;
	
	private static ThreadLocal< DesignSession > threadLocal = new InheritableThreadLocal< DesignSession >() ;
	
	
	public DesignContext<? extends Design> getDesignContext () {
		return designContext;
	}
	
	
	public boolean hasDesignContext( String uid ){
		return contexts.get( uid ) != null ;
	}
	
	
	public DesignContext<? extends Design> setDesignContext ( String uid ) {
		this.designContext = contexts.get( uid ) ;		
		MDC.put ( "design", uid ) ;
		MDC.put ( "theme", designContext.getDesign ().getName () ) ;
		return this.designContext ;
	}
	
	
	public void setDesignContext ( DesignContext<? extends Design> designContext ) {
		this.designContext = designContext;
		MDC.put ( "design", designContext.getDesign().getUid() ) ;
		MDC.put ( "theme", designContext.getDesign ().getName () ) ;
	}
	
	
	public void addDesignContext ( DesignContext<? extends Design> designContext ){
		contexts.put( designContext.getDesign().getUid() , designContext ) ;
	}
	
	public boolean isEditing () {
		return editing;
	}

	public void setEditing ( boolean editing ) {
		this.editing = editing;
	}

	
    public static void set( DesignSession session ) {
    	threadLocal.set( session );
    }
    

	public static DesignSession get() {
    	
    	DesignSession result =  ( DesignSession ) threadLocal.get();
 
    	if ( result == null ){
    		throw new AppRuntimeException( "No DesignSession in threadLocal" ) ;
    	}
    
    	return result ;
    }
	
	
	public static Design getDesign() {
		return get().getDesignContext().getDesign() ;
	}
	
	
	public static void setDesign( Design design ) {
		get().setDesignContext ( design.getUid() ) ;
	}

	public static Design setDesign( String uid ) {
		return get().setDesignContext ( uid ).getDesign() ;
	}	
	

	public static void clear () {
		threadLocal.remove ();
	}

	/*
	public UserActivity getUserActivity () {
		return userActivity;
	}

	public void setUserActivity ( UserActivity userActivity ) {
		this.userActivity = userActivity;
	}
	*/
	
	
	public Map< String, DesignContext < ? extends Design > > getContexts(){
		return this.contexts ;
	}



	@Override
	public void valueBound( HttpSessionBindingEvent event ) {
	}


	@Override
	public void valueUnbound(HttpSessionBindingEvent event ) {
		for( DesignContext< ? > context : getContexts().values() ){
			new UploadDirectoryCleaner( context ).clean() ;
		}
	}
	
	
		
}
