package com.dotemplate.core.server;

import java.io.File;
import java.io.InputStream;

import org.apache.commons.betwixt.io.BeanReader;
import org.apache.commons.betwixt.io.read.BeanCreationList;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.xml.sax.InputSource;

import com.dotemplate.core.server.frwk.AppException;
import com.dotemplate.core.server.frwk.AppRuntimeException;
import com.dotemplate.core.shared.Design;


public abstract class DesignDescriptorReader< D extends Design > {
	
	private final static Log log = LogFactory.getLog ( DesignDescriptorReader.class ) ;
	
	private BeanReader beanReader ;
	
	protected DesignDescriptorReader(){
		
		beanReader = new BeanReader() ;
		
		BeanCreationList chain = BeanCreationList.createStandardChain();
		
	    chain.insertBeanCreator( 1, new DesignCreator( ) );
	    
	    beanReader.getReadConfiguration().setBeanCreationChain( chain ) ;

		beanReader.getXMLIntrospector().getConfiguration().setAttributesForPrimitives( true ) ;
		InputStream betwixt = Design.class.getClassLoader ().getResourceAsStream ( 
				getBetwitxtDescriptorName() ) ;
		
		try {
			
			beanReader.registerMultiMapping( new InputSource( betwixt ) ) ;
			
		} catch ( Exception e ) {
			
			log.error (  e ) ;
			throw new AppRuntimeException( "Failed to create a new reader", e ) ;
		
		}
		
		
	}
	
	
	
	@SuppressWarnings( "unchecked" )
	public D read( File file ) throws AppException {
		
		if ( log.isDebugEnabled () ){
			log.debug (  "Reading design " + file.getPath() + "..." ) ;
		}
		
		D design ;
		
		try {
			
			design =  ( D ) beanReader.parse( file ) ;
			
			/* Seems not useful ( recurrent ) 
	    	for ( PropertySet set : design.getPropertySets() ) {
				
	    		DesignUtils.deepBuild( set ) ;
				
			}
			*/
			
	    	
		} catch ( Exception e ) {
			
			throw new AppException( "Failed to read theme ", e ) ;
		
		}


		if ( log.isDebugEnabled () ){
			
			DesignLogger.log ( design ) ;
		
		}
		
		if ( log.isDebugEnabled () ){
			log.debug (  "Done reading design " + file.getPath() + "..." ) ;
		}
		
		
		return design ;		
	
	}
	
	

	

	public void reset() {
		beanReader.clear() ;
	}
	
	
	protected abstract String getBetwitxtDescriptorName () ;
	
	
		
}





