/*
 * Created on Aug 26, 2003
 *
 */
package com.dotemplate.core.server.frwk;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;



public class AppException extends Exception {

	private static final long serialVersionUID = 3623360516035115073L;	
	
	private Log log = LogFactory.getLog ( AppException.class );
	
	public AppException(String msg, Exception e) {
		super( msg, e ) ;
		log.error ( e ) ;
		log.error( msg ) ;
		if ( e.getCause () != null ){
			log.error ( e.getCause () ) ;
		}	
	}

	public AppException(String msg ) {
		super( msg ) ;
		log.error( msg ) ;
	}
        
}
