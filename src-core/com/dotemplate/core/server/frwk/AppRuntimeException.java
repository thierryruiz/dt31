/*
 /*
 * Copyright 2005-2006 Athesanet. All rights reserved.
 * 
 * Created on 30 sept. 2006
 * 
 */
package com.dotemplate.core.server.frwk;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


public class AppRuntimeException extends RuntimeException {

	private static final long serialVersionUID = 6218257906907372737L;

	private Log log = LogFactory.getLog ( AppRuntimeException.class );

	
	public AppRuntimeException() {
		super ();
	}

	public AppRuntimeException( String message ) {
		super ( message );
		log.error ( message );
	}

	public AppRuntimeException( String message, Throwable cause ) {
		super ( message, cause );
		cause.printStackTrace () ;
		log.error( message ) ;
		log.error ( cause );
	}

	public AppRuntimeException(Throwable cause) {
		super ( cause );
		cause.printStackTrace () ;
		log.error ( cause );
	}
	
}
