package com.dotemplate.core.server.frwk;

import org.apache.commons.lang.ClassUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.App;
import com.dotemplate.core.shared.frwk.Command;
import com.dotemplate.core.shared.frwk.Response;
import com.dotemplate.core.shared.frwk.WebException;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;


public class AbstractRemoteService extends RemoteServiceServlet  {

	private static final long serialVersionUID = -2234178223551535360L;
	
	private final static Log log = LogFactory.getLog ( AbstractRemoteService.class );
	
	
	protected WebException syserror ( String msg ) {
		log.error ( msg ) ;
		return new WebException( "Oups! Looks like a bug, sorry. Please report to us." ) ;
	}

	
	protected WebException syserror ( Exception e ) {
		processError( e ) ;
		return new WebException( "Oups! Looks like a bug, sorry. Please report to us.", e ) ;
	}
	
	
	protected WebException syserror ( String msg, Exception e  ) {
		processError( e ) ;
		return new WebException(  msg, e ) ;
	}
	
	
	protected WebException userError ( String msg ) {
		log.error( msg ) ;
		return new  WebException( msg, WebException.USER ) ;
	}
	
	
	protected void processError ( Throwable t ){
		log.error ( t ) ;
		t.printStackTrace () ;
		Throwable cause = t.getCause () ;
		if ( cause != null ){
			log.error ( cause ) ;
			cause.printStackTrace () ;
		}
	}
	
	
	@SuppressWarnings("unchecked")
	protected <T extends Response> CommandHandler< T > processCommandHandler( Command< T > command ) {

		
		// Delegates action processing to the underlying CommandHandler. As a naming 
		// convention CommandHandlers are located in subpackage "server.actions" and named 
		// <command name>Handler. 
		
		
		String packageName = ClassUtils.getPackageName( command.getClass() ) ;
		
		packageName = StringUtils.replace( packageName, "shared", "server" ) ;
		
		String handlerClassName = new StringBuffer()
			.append( packageName ).append(".")
			.append( command.getClass ().getSimpleName () )
			.append( "Handler" ).toString () ;
		
		CommandHandler<T> handler = ( CommandHandler<  T> )	App.getSingleton ( handlerClassName ) ;
				
		return handler ;
		
	}
	
	
}
