package com.dotemplate.core.server.frwk;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.shared.frwk.Command;
import com.dotemplate.core.shared.frwk.Response;
import com.dotemplate.core.shared.frwk.WebException;


public abstract class CommandHandler<T extends Response> {

	private final static Log log = LogFactory.getLog ( CommandHandler.class );
	
	public abstract T handleAction ( Command< T > action ) throws WebException ;

	
	protected WebException syserror ( String msg ) {
		log.error ( msg ) ;
		return new WebException( "Unexpected error ! Sorry." ) ;
	}

	
	protected WebException syserror ( Exception e ) {
		processError( e ) ;
		return new WebException( "Unexpected error", e ) ;
	}
	
	
	protected WebException syserror ( String msg, Exception e  ) {
		processError( e ) ;
		return new WebException(  msg, e ) ;
	}
	
	
	protected WebException userError ( String msg ) {
		log.error( msg ) ;
		return new  WebException( msg, WebException.USER ) ;
	}
	
	protected void processError ( Throwable t ){
		log.error ( t ) ;
		t.printStackTrace () ;
		Throwable cause = t.getCause () ;
		if ( cause != null ){
			log.error ( cause ) ;
			cause.printStackTrace () ;
		}
	}

}
