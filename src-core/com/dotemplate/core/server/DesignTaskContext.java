package com.dotemplate.core.server;

import com.dotemplate.core.server.util.ConcurrentTaskContext;


public class DesignTaskContext implements ConcurrentTaskContext {

	// This task context hold the EaseSession inherited from parent
	// thread. This is required as task executor use a thread pool, 
	// even using a InheritableThreadLocal to store EaseSession,
	// child thread inherits the correct EaseSession the first time only 
	// (at thread creation). As thread is reused and not created EaseSession 
	// must be set explicitly using the task context.
	
	
	private DesignSession session ;
	
	
	public DesignTaskContext () {
		// called in parent thread to set easeSession
		session = DesignSession.get () ;
	}
	
	
	public DesignSession getDesignSession () {
		return session;
	}


	@Override
	public boolean isValid () {
		return true ;
	}

	
}
