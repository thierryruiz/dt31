package com.dotemplate.core.server;

import com.dotemplate.core.shared.Design;

public abstract class DesignContextFactory < D extends Design > {
	
	public abstract DesignContext< D > create ( D design ) ;
	
	public abstract DesignContext< D > create ( D design, boolean random ) ;
	
	
}
