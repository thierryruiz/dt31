package com.dotemplate.core.server;

import java.lang.reflect.InvocationTargetException;
import java.util.Iterator;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.frwk.AppRuntimeException;
import com.dotemplate.core.shared.Design;
import com.dotemplate.core.shared.properties.Property;
import com.dotemplate.core.shared.properties.PropertyMap;
import com.dotemplate.core.shared.properties.PropertySet;


public class DesignMerger {

	private static Log log = LogFactory.getLog( PropertyUtils.class );
	
	private static BeanUtilsCustom beanUtils = new BeanUtilsCustom() ;
	
	
	public static void merge ( Design dest, Design src ) throws Exception {
		
		beanUtils.copyProperties( dest, src ) ;
				
		PropertySet srcSet, destSet ;

		
		for ( Iterator<PropertySet> it = src.getPropertySets().iterator() ;
			it.hasNext() ; ) {
			
			srcSet = it.next() ;			
		
			DesignUtils.deepBuild( srcSet ) ;
			
			destSet = dest.getPropertySet( srcSet.getName() ) ;
			
			if ( destSet == null ){
				
				dest.addPropertySet( srcSet ) ;
			
			} else {
			
				DesignUtils.deepBuild ( destSet ) ;
				
				merge ( destSet, srcSet ) ;
				
			}
		}
		
		
		for ( Iterator<PropertySet> it = dest.getPropertySets().iterator() ;
			it.hasNext() ; ) {

			destSet = it.next() ;
			
			srcSet = src.getPropertySet( destSet.getName() ) ;
			
			if ( srcSet != null ) {
				//already done
				continue ;
			}
			
			DesignUtils.deepBuild( destSet ) ;
			
		}
		
	}

	
	
	public static void merge( Property dest, Property src ) throws Exception {
		
		if ( log.isDebugEnabled () ){
			log.debug ( "Merging property '" + src.getName () + "' to '" 
									+ src.getName () + "'"  ) ;
		}
		
		if ( ! dest.isSet() ) {
			
			beanUtils.copyProperties( dest, src ) ;
			return ;
		
		}
		
		PropertySet destSet = ( PropertySet ) dest ;
		PropertySet srcSet = ( PropertySet ) src ;
				
		
		if ( log.isDebugEnabled () ){
			log.debug ( destSet.getParent() + " " + srcSet.getName() ) ;
		}
		
		
		if( destSet.getParent() == null || destSet.getParent().equals( srcSet.getName() ) ){
			
			destSet.setPropertyMap( merge ( destSet.getPropertyMap(), srcSet.getPropertyMap() ) ) ;
			
			beanUtils.copyProperties( dest, src ) ;
		
		}

		// usefull ? 
		if ( destSet.getLabel() == null ) destSet.setLabel ( srcSet.getLabel() ) ;
		if ( destSet.getSelectors() == null ) destSet.setSelectors( srcSet.getSelectors() ) ;
		if ( destSet.getStyleId() == null ) destSet.setStyleId( srcSet.getStyleId() ) ;
		if ( destSet.getStyleClass() == null ) destSet.setStyleClass( srcSet.getStyleClass() ) ;
		if ( destSet.getGroup() == null ) destSet.setGroup( srcSet.getGroup() ) ;
		if ( destSet.getIcon() == null ) destSet.setIcon( srcSet.getIcon() ) ;
		
	}
	
	
	
	public static PropertyMap merge ( PropertyMap dest, PropertyMap src ) throws Exception {
		
		PropertyMap map = new PropertyMap() ;
		
		String key ;
		
		// copy src map
		for ( Iterator<String> it = src.keySet().iterator() ; it.hasNext() ; ){
			key = it.next() ;
			map.addProperty( ( Property ) beanUtils.cloneBean ( src.get( key ) ) ) ;
		}	
	
		Property destP , srcP ;
		
		
		for ( Iterator<String> it = dest.keySet().iterator() ; it.hasNext() ; ){
			key = it.next() ;
			
			destP = dest.get( key ) ;
			
			if ( destP.isSet () ){
				
				DesignUtils.deepBuild ( ( PropertySet ) destP ) ;
			
			}
			
			
			if ( map.containsKey( key ) ){
				
				srcP = map.get (  key ) ;
				
				if ( srcP.isSet () ){
					
					DesignUtils.deepBuild ( ( PropertySet ) srcP ) ;
				
				}
			
				merge ( destP, srcP ) ;
			
			} 
			
			map.addProperty ( destP ) ;	
			
		}	
		
		
		return map ;
		
	}
	

	
	
	private static class BeanUtilsCustom extends BeanUtilsBean {
	
	
		@Override
		public void copyProperty( Object dest, String name, Object value )
				throws IllegalAccessException, InvocationTargetException {
					
			if ( value == null ) return ;
			
			// ignore property map 
			if ( "propertyMap".equals( name ) ) {	
				return ;
			}
			
			try {
				
				Object overrider = PropertyUtils.getSimpleProperty( dest , name ) ;
							
				if ( overrider != null ) return ;
	
				if ( log.isDebugEnabled() ){
					log.debug( "Override property " + name + " with " + value ) ;
				}
				
				super.copyProperty( dest, name, value ) ;	
										
			} catch ( NoSuchMethodException e ) {
				
				throw ( new AppRuntimeException ( e ) ) ;
			
			}	
	
		}

	
		private void copyProperties( Property dest, Property src )
				throws IllegalAccessException, InvocationTargetException {
			
			// preserve uid 
			String uid = dest.getUid() ;
			super.copyProperties( dest, src );
			dest.setUid(uid) ;
			
		}
	}
	
	
}
