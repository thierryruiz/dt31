package com.dotemplate.core.server.apis.picasa;


import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.frwk.AppRuntimeException;
import com.google.gdata.client.Query;
import com.google.gdata.client.photos.PicasawebService;
import com.google.gdata.data.photos.AlbumFeed;
import com.google.gdata.data.photos.PhotoEntry;
import com.google.gdata.util.AuthenticationException;



/* 
 * 
 * This class is only used in dev mode to deploy Blogger css 
 * images in Picasa to test the template  generated.
 *
 */
class Picasa {

	private static Log log = LogFactory.getLog( Picasa.class ) ;
	
	private PicasawebService picasa ;
	
	
	public Picasa() {
		connect() ;
	}
	
	
	
	private synchronized void connect() {
		
		log.info ( "Initializing Picasa connection..." ) ;
		
		picasa = new PicasawebService( "dotemplate" );
		
		try {
			
			picasa.setUserCredentials( "dotemplate@gmail.com", "saysayboomak" );
			
		} catch ( AuthenticationException e ) {
			
			throw new AppRuntimeException( "Failed to login in Picasa", e ) ;
		
		}
		
	}
	
	
	
	public List< PhotoEntry > searchPhotos ( String search ) throws Exception {
		
		URL baseSearchUrl = new URL("https://picasaweb.google.com/data/feed/api/all");

		Query searchQuery = new Query( baseSearchUrl );
		
		searchQuery.setStringCustomParameter( "kind", "photo" );
		searchQuery.setMaxResults( 10 );
		searchQuery.setFullTextQuery( search );
  
		AlbumFeed searchResultsFeed = picasa.query( searchQuery, AlbumFeed.class);

		ArrayList< PhotoEntry > results = new ArrayList< PhotoEntry >() ;
			
		for ( PhotoEntry photo : searchResultsFeed.getPhotoEntries() ) {
			
			results.add( photo ) ;
			
		    System.out.println( photo.getMediaContents().get(0).getUrl() );
		
		}
		
		
		return results ;
		
		
	}

	


	
	
	
	public static void main ( String[] args ) {
		
		Picasa picasa = new Picasa() ;
		
		
		try {
			picasa.searchPhotos( "skyline" ) ;
		} catch (Exception e) {

			e.printStackTrace();
		}

	}
	
	
	
}
