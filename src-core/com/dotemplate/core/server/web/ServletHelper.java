package com.dotemplate.core.server.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class ServletHelper {

	
	protected static void nochache ( HttpServletResponse response ){
		response.setHeader( "Cache-Control", "no-cache" ); 
		response.setHeader( "Pragma", "no-cache" ); 
		response.setHeader( "Expires", "-1" ); 
	}
	
	
	protected static String getBaseUrl( HttpServletRequest request ) {
		return new StringBuffer()
			.append( request.getScheme() ).append("://" )
			.append( request.getServerName() ).append( ":" )
			.append( request.getServerPort() )
			.append( request.getContextPath() )
			.append("/").toString() ;
	}
	
	
}
