package com.dotemplate.core.server.web;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.App;
import com.dotemplate.core.server.DesignLogger;
import com.dotemplate.core.server.util.UIDGenerator;
import com.dotemplate.core.shared.frwk.WebException;




@SuppressWarnings("unused")
public abstract class BaseHttpServlet extends HttpServlet {

	private static final long serialVersionUID = -7928193897612788765L;
	
	private static Log log = LogFactory.getLog ( BaseHttpServlet.class );

	protected final static String SYSERROR = "Oups an error occured! Sorry. Please contact us." ;

	
	@Override
	protected void service( HttpServletRequest request, HttpServletResponse response )
			throws ServletException, IOException {
		request.setAttribute( "base", getBaseUrl( request ) ) ;
		super.service(request, response);
	}
	
	
	/*
	protected void setDesignSession( HttpServletRequest request  ) {
		DesignSessionHelper.initDesignSession( ( HttpServletRequest ) request ) ;
	}*/
	
	
	
	protected void forwardTo( String page, HttpServletRequest request, HttpServletResponse response ) 
			throws ServletException, IOException {

			String destination = "/WEB-INF/jsp/dotemplate/" + page ;
			
			getServletContext().getRequestDispatcher( destination ).forward(
					request, response );	
	}
	
	
	protected void forwardUnexpectedError( HttpServletRequest request, HttpServletResponse response ) 
			throws ServletException, IOException {

			String destination = "/WEB-INF/jsp/dotemplate/error.jsp" ;
			
			getServletContext().getRequestDispatcher( destination ).forward(
					request, response );
	}
	
	
	
	protected boolean checkDesignSession( HttpServletRequest request, HttpServletResponse response, boolean forwardToErrorPage )
			throws ServletException, IOException {

		
		if ( ! DesignSessionHelper.checkDesignSession (  request ) ){
			
			log.info ( "Session expired or invalid session !"  );
			
			if( forwardToErrorPage ){
				
				forwardSessionTimeout( request, response ) ;			
			
			}
			
			return false ;
		}
		
		return true ;
		
	}
	
	
	protected void ensureDesignSession( HttpServletRequest request ) {
		DesignSessionHelper.ensureDesignSession( request ) ;
	}
	
	
	protected void forward404( HttpServletRequest request, HttpServletResponse response ) 
			throws ServletException, IOException {
		
		forwardTo( "404.jsp", request, response ) ;
	
	}
	

	protected void forwardSessionTimeout( HttpServletRequest request, HttpServletResponse response ) 
			throws ServletException, IOException {
		
		forwardTo( "timeout.jsp", request, response ) ;
	
	}

	
	
	protected String getBaseUrl( HttpServletRequest request ) {
		return ServletHelper.getBaseUrl(request) ;
	}
	

	public  void log( HttpServletRequest req ) {

    	if ( log.isDebugEnabled() ){
        	StringBuffer sb = new StringBuffer ()
                    .append("\n\n\n\n=========  INCOMING REQUEST =========================================================\n" )
                    .append( "\trequest url : ")
                    .append( req.getRequestURL () )                    
                    .append( "\n")
                    
                    .append( "\trequest method : ")
                    .append( req.getMethod () )
                    .append( "\n")

                    .append( "\trequest path info : ")
                    .append( req.getPathInfo () )  
                    .append( "\n")
                    .append( "\trequest uri : ")
                    .append( req.getRequestURI () )
                    .append( "\n")
                    .append( "\trequest query : ")
                    .append( req.getQueryString() )
                    .append( "\n")                    
                    .append( "\trequested sessionId : ")          
                    .append( req.getRequestedSessionId () )
        	 		.append( "\n") ;
                    
            	String key ;
	            for ( Enumeration<String> e = req.getParameterNames ()  ; e.hasMoreElements () ;) {
	            	sb.append ( "\t\t").append( key =  e.nextElement () ).append("=").append( 
	            			req.getParameter ( key ) ).append("\n") ;
	            }
	            
	            log.debug ( sb.toString()  ) ;     
	    	  
        } 
	}


	protected void nocache( HttpServletResponse response ) {
		// dynamic content so avoid client side caching
		response.setHeader( "Cache-Control", "no-cache" ); 
		response.setHeader( "Pragma", "no-cache" ); 
		response.setHeader( "Expires", "-1" ); 
	}

	
}
