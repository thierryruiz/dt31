package com.dotemplate.core.server.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.App;
import com.dotemplate.core.server.CachedImage;
import com.dotemplate.core.server.frwk.AppRuntimeException;
import com.dotemplate.core.server.svg.PNGTranscoder;



public class GetCanvasImage extends BaseHttpServlet {

	private static final long serialVersionUID = 1L;

	public static final String URL_PREFIX = "/dt/canvasImg?src=" ;
       
	private static Log log = LogFactory.getLog ( GetCanvasImage.class );
	
	
	protected void doGet( HttpServletRequest request, HttpServletResponse response ) 
		throws ServletException, IOException {
		
		
		if ( !checkDesignSession( request, response, false ) ){
			response.setStatus( HttpServletResponse.SC_NOT_MODIFIED ) ;
		}
		
		String img = request.getParameter( "src" ) ;
		
		if ( img == null ) {
			response.setStatus( HttpServletResponse.SC_BAD_REQUEST ) ;
			return ;
		}
		
		if ( log.isDebugEnabled () ) {
			log.debug (  "Get image " + img  ) ;
		}
		
		response.setContentType( "image/png" );
		
		
		// try to get image from cache 
		CachedImage cachedImage = App.getDesignContext ().getCanvasImageCache ().getImage( img ) ;

		if ( cachedImage == null ){
			
			throw new AppRuntimeException( "No image '" + img + "' found in cache" ) ;
			
		}
		
		
		if ( cachedImage.isChanged() ){
			
			if ( log.isErrorEnabled() ){
				log.debug( "Get image " + img + " from cache (changed)" ) ;
			}
			
			ServletOutputStream os = response.getOutputStream () ;
			
			os.flush () ;
			
			try {
				
				PNGTranscoder.writeImage ( cachedImage.getImage(), os ) ;
				
			} catch (Exception e) {
				log.error( "Error while reloading image " + img ) ;
			}
		} else {
			
			if ( log.isErrorEnabled() ){
				log.debug( "Get image " + img + " (no change)" ) ;
			}
			
			response.setStatus( HttpServletResponse.SC_NOT_MODIFIED ) ;
			
		}
	}

}
