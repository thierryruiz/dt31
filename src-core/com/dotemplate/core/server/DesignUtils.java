/*
/*
 * 
 * Created on 6 mars 2007
 * 
 */
package com.dotemplate.core.server;


import java.io.File;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.frwk.AppRuntimeException;
import com.dotemplate.core.server.symbol.SymbolProvider;
import com.dotemplate.core.server.util.Path;
import com.dotemplate.core.server.util.UIDGenerator;
import com.dotemplate.core.shared.Design;
import com.dotemplate.core.shared.DesignResource;
import com.dotemplate.core.shared.SymbolType;
import com.dotemplate.core.shared.properties.BackgroundImageProperty;
import com.dotemplate.core.shared.properties.BooleanProperty;
import com.dotemplate.core.shared.properties.ColorProperty;
import com.dotemplate.core.shared.properties.PercentProperty;
import com.dotemplate.core.shared.properties.Property;
import com.dotemplate.core.shared.properties.PropertyMap;
import com.dotemplate.core.shared.properties.PropertySet;
import com.dotemplate.core.shared.properties.RefProperty;
import com.dotemplate.core.shared.properties.SizeProperty;
import com.dotemplate.core.shared.properties.StringProperty;




/**
 * 
 * 
 * @author Thierry Ruiz
 * 
 */
public class DesignUtils {

    private static Log log = LogFactory.getLog( DesignUtils.class );
    
    
	/**
	 * 
	 * Initialize a set with all it's symbol references  
	 * 
	 * 
	 * @param set the set to initialize
	 * @return 
	 * @throws Exception
	 */
	public static void deepBuild ( PropertySet set ) {
		
 		if ( set.isBuilt () ) return  ;
		
        if ( log.isDebugEnabled() ){
        	
        	log.debug( "Deep build of '" + set.getName () + "' " +
        			"[ uid=" + set.getUid () + "," + set.toString () + " ]" ) ;
        
        }
		

		// recursively build a symbol
		Property p ;
		
		String parent = set.getParent() ;
		
		if ( parent != null ){
			
			if ( log.isDebugEnabled () ){
				
				log.debug ( "Set has a parent symbol " + parent ) ;
			
			}
			
			PropertySet symbol =  cloneSet( getSymbol ( parent ) ) ;
			
			
			if ( symbol == null ){
				
				 throw new AppRuntimeException ( "No symbol found matching name '" + parent + "'." ) ;
			
			}
			
			
			if ( log.isDebugEnabled () ){
				log.debug ( "Merge set '" + set.getName () + "' with values from parent set '" 
										+ symbol.getName () + "'"  ) ;
			}
			
			try {
				
				DesignMerger.merge( set, symbol ) ;
			
			} catch ( Exception e) {

				throw new AppRuntimeException ( "Failed to build set " + set.getLongName() +
						" from symbol " + symbol.getPath() , e ) ;
			
			}
			
		}
		
		
		for ( Iterator<Property> it = set.getProperties().iterator() ; it.hasNext() ; ){
			
			p = it.next();
			
			if ( ! p.isSet() ) continue ;
			
			deepBuild( ( PropertySet ) p ) ;
						
		}
		
		
		set.setBuilt ( true ) ;
		
	}
	

	public static Collection< PropertySet > getAllSets( Design design ){
		
		Map< String, PropertySet > sets = new LinkedHashMap < String, PropertySet >() ;
		
		for ( PropertySet set : design.getPropertySets() ){
			
			addSubSets( set, sets ) ;
		
		}
		
		return sets.values() ;
		
	}
	
	
	private static void addSubSets ( PropertySet set,  Map< String, PropertySet > map ){
		
		// add set itself
		map.put ( set.getUid(), set ) ;
		
		for ( Property property : set.getPropertyMap ().values() ){
			if ( property.isSet() ){
				addSubSets( ( PropertySet ) property , map ) ;
			}
		}
		
	}
	
	
	
	public static Property findPropertyByUid ( Design design , String uid ){
		
	    if ( log.isDebugEnabled() ){
	    	log.debug( "Find property " + uid  ) ;
	    }
	   
		Property property ;
		
		for ( PropertySet set : design.getPropertySets () ){
	
			if ( set.getUid().equals( uid ) ) return set ;
			
			property = findPropertyByUid( set, uid ) ;
			
			if ( property != null ) return property ;
			
		}
		
		return null;
	}

	
	
	public static Property findPropertyByUid ( PropertySet set, String uid ){
		
	    if ( log.isDebugEnabled() ){
	    	log.debug( "Find property " + uid + " in " + set.getName () ) ;
	    }
	    	
	    Property result ;
	    
		for ( Property property : set.getProperties () ){
			
			if ( property.getUid().equals( uid ) ) return property ;
			
			if ( property.isSet () ) {
				result =  findPropertyByUid ( ( PropertySet ) property, uid ) ;
				if ( result != null ) {
					return result ;
				}
			} 
		}
		
		return null;
	
	}

		
	public static PropertySet findSetByFullName( DesignContext< ? extends Design > context, String fullName ){
		try {
			return findSetByFullName( ( PropertySet ) context.get( StringUtils.substringBefore( fullName, "." ) ), 
					StringUtils.substringAfter( fullName, "." ) ) ;
		} catch ( Exception e ) {
			throw new AppRuntimeException( "Unable to lookup set '" + fullName + "'" ) ;
		}
	}


	
	public static PropertySet findSetByFullName( PropertySet set, String path ) {
		if ( path.length() == 0 ) return set ;
		return findSetByFullName( ( PropertySet ) set.get( StringUtils.substringBefore( 
				path, "." ) ) , StringUtils.substringAfter( path, "." ) )  ;
	}
	
	
	
	public static boolean isPropertyEquals( PropertySet set,  String name, String value ) {
			
		Property property = set.getPropertyMap ().get (  name  ) ;
	
		if ( property == null ){
			return false ;
		}

		switch ( property.getType () ){
		
			case Property.SIZE :
				return ( (  SizeProperty ) property ).getValue ()  == Integer.parseInt ( value );

			case Property.PERCENT :
				return ( (  PercentProperty ) property ).getValue () == Float.parseFloat ( value ) ;
		
			case Property.COLOR :
				return ( (  ColorProperty ) property ).getValue().equals ( value );
				
			case Property.STRING :
				return ( (  StringProperty ) property ).getValue().equals ( value ) ;
				
			case Property.BGIMG :
				return ( (  BackgroundImageProperty ) property ).getValue().equals ( value ) ;
				

			case Property.REF:
				return ( (  RefProperty ) property ).getValue().equals ( value ) ;
				
			case Property.BOOLEAN : 
				return  ( ( BooleanProperty ) property ) .isValue() == Boolean.parseBoolean ( value ) ;	
				
			
			default : return false  ;
		}
		
	}
	
	
	public static Property cloneProperty( Property property )  {
		
		try {
			if ( property == null ){
				return null ;
			}
	
			if ( property.isSet() ){
				return cloneSet( ( PropertySet ) property ) ;
			}
			
			Property clone = ( Property ) BeanUtils.cloneBean( property ) ;
			
			clone.setUid( UIDGenerator.pickId() ) ;
			
			return clone ;
		
		} catch ( Exception e ) {
				
				throw new AppRuntimeException ( "Failed to clone Property '" + property.getLongName() + "'" ) ;
			
		}
		
	}
	
	public static PropertySet cloneSet( PropertySet set )  {
		
		try {

			PropertySet clone = ( PropertySet ) BeanUtils.cloneBean( set ) ;
			
			clone.setUid( UIDGenerator.pickId() ) ;
			
			clone.setPropertyMap( new PropertyMap() ) ;
			
			Property cloneP ;
			
			for ( Property p : set.getProperties() ){
				
				if ( p.isSet() ) {
					
					clone.addProperty( cloneSet ( ( PropertySet ) p ) ) ;
				
				} else {
					
					cloneP = ( Property ) BeanUtils.cloneBean( p ) ;
					
					cloneP.setUid( UIDGenerator.pickId() ) ;
					
					clone.addProperty(  cloneP ) ;
					
				}
				
			}
		
			return clone ;
		
		} catch ( Exception e ) {
			
			throw new AppRuntimeException ( "Failed to clone PropertySet '" + set.getLongName() + "'" ) ;
		
		}
		

	}
	

	public static Path getSymbolRealPath( PropertySet symbol ){
		return new Path ( App.realPath ( "WEB-INF" ) )
			.append( "templates").
				append( symbol.getPath () );		
	}
	
	
	public static SymbolType getSymbolType( String type ){
		return App.getDesignManager().getSymbolType( type ) ;
	}
	
	
	public static SymbolProvider getSymbolProvider( SymbolType type ){
		return ( SymbolProvider ) App.getSingleton ( type.getProvider () ) ;	
	}

	
	public static SymbolProvider getSymbolProvider( PropertySet set ){
		
		String parent = set.getParent() ;
		
		String type = parent.substring ( 0, parent.indexOf ( "-" ) ) ;
	
		try {
			
			return getSymbolProvider( type ) ;
		
		} catch ( Exception e ) {
			
			throw new AppRuntimeException( "Failed to get symbol provider of set " + set.getLongName() , e ) ;
		
		}
		
	}
	
	
	public static SymbolProvider getSymbolProvider ( String folder ){
		return  getSymbolProvider( App.getDesignManager().getSymbolType( folder ) );
	}
	
	
	public static PropertySet getSymbol( PropertySet set ){
		String parent = set.getParent () ;
		if ( parent == null ) return null ;
		return getSymbol( parent ) ;
	}
	
	
	public static PropertySet getSymbol( String name ){
		
		if ( log.isDebugEnabled () ){
			log.debug ( "Get symbol " + name ) ;
		}
		
		String type = name.substring ( 0, name.indexOf ( "-" ) ) ;
		
		SymbolProvider provider;
		
		try {
			
			provider = getSymbolProvider( type ) ;
		
		} catch ( Exception e ) {
			
			throw new AppRuntimeException( "Failed to get provider for symbol type " + type , e ) ;
		
		}
		
		PropertySet symbol = provider.get ( name ) ;
		
		if ( symbol == null ){
			
			if ( log.isDebugEnabled () ){
				
				log.debug (  "Symbol '" + name + "' not created, attempt to create it from descriptor"  ) ;
				
			}
			
			// create it from descriptor
			symbol = provider.createSymbolFromDescriptor (  new File ( 
					App.realPath ( App.getConfig().getVmTemplatesDir() + "/_symbols/" + StringUtils.replaceOnce( 
							name, "-", "/" ) + "/symbol.xml" ) ) ) ;
			
		}

		if ( ! symbol.isBuilt () ){
			
			try {
				
				DesignUtils.deepBuild( symbol ) ;
			
			} catch ( Exception e ) {
				
				throw new AppRuntimeException( "Failed to build symbol " + symbol.getName() , e ) ;
			
			}
		
		}
		

		//DesignLogger.log( symbol ) ;
		
		
		return symbol ;
		
	}
	
	
	public static PropertySet createFromSymbol( String name ) {
		return DesignUtils.cloneSet ( getSymbol( name ) ) ;
	}


	public static String[] getTags( DesignResource resource ) {
		return StringUtils.split( resource.getTags(), ',');
	}
		
	

}


