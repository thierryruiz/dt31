package com.dotemplate.core.server;

import java.io.OutputStream;

import org.apache.commons.betwixt.io.BeanWriter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.frwk.AppException;
import com.dotemplate.core.shared.Design;


public abstract class DesignDescriptorWriter< D extends Design > {

	private final static Log log = LogFactory.getLog ( DesignDescriptorWriter.class ) ;
	
	
	public void write ( D design, OutputStream os ) throws AppException {
		
		if( log.isDebugEnabled () ){
			
			log.debug (  "Writing desgn bean..." ) ;
		
		}
	
		try {

			os.write( "<?xml version='1.0' ?>\n".getBytes () );
			
			BeanWriter beanWriter = getBeanWriter( os ) ; 			
			
			beanWriter.write( design );
			
			
		} catch ( Exception e ) {
			
			e.printStackTrace();
			
			log.error (  e ) ;
			
			throw new AppException ( "Failed to to write Theme", e  ) ;
		
		} finally {
			
			try {
				os.close();
			} catch ( Exception e ) {
				log.error (  e ) ;
			}
		}
		
		if ( log.isDebugEnabled () ){
			log.debug (  "Design descriptor output done" ) ;
		}
		
	}
	
	
	protected abstract BeanWriter getBeanWriter( OutputStream os ) ;
	
	
}
