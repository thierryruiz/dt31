/*
 * Created on 19 oct. 2005
 *
 */
package com.dotemplate.core.server;


import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.Writer;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.velocity.Template;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.context.Context;

import com.dotemplate.core.server.App;
import com.dotemplate.core.server.frwk.AppRuntimeException;



/**
 * 
 * 
 * @author Thierry Ruiz
 * 
 */
public class RenderEngine extends VelocityEngine {

	private static Log log = LogFactory.getLog ( RenderEngine.class );
	
	
	protected RenderEngine() {
		
		// initializing engine. Absolute path must be set for each vm load path
		Properties props = new Properties ();

		try {
			props.load ( new FileInputStream ( App.realPath ( App.getConfig().getRenderEngineConf() ) ) );
		} catch ( Exception e ) {
			log.fatal ( "RenderEngine initialization failed ", e );
			throw new AppRuntimeException( e ) ;
		}

		String vmPath = props.getProperty ( VelocityEngine.FILE_RESOURCE_LOADER_PATH );
		props.setProperty ( VelocityEngine.FILE_RESOURCE_LOADER_PATH, App.realPath ( vmPath ) );

		
		if ( App.isDevMode() ){
			// disable VM cache in dev mode
			props.setProperty ( VelocityEngine.FILE_RESOURCE_LOADER_CACHE, "false" );
		}
		
		try {
			
			init ( props );
			
		} catch ( Exception e ) {
			
			log.fatal ( "RenderEngine initialization failed.", e );
		
		}
		
		
	}
	
	
	
	protected RenderEngine( Properties props  ) {
		
		try {
			
			init ( props );
			
		} catch ( Exception e ) {
			
			log.fatal ( "RenderEngine initialization failed.", e );
		
		}
	
	}
	
	
	
	public void generate( Context context, Template template, Writer writer ) throws Exception {

		if ( log.isDebugEnabled () ) {
			log.debug ( "Merging template '" + template.getName () + "'..." );
		}

		// render the layout with nested view
		try {
			
			template.merge ( context, writer );
		
		} catch ( Exception e ) {
		
			log.error ( e ) ;
			throw new AppRuntimeException ( "Cannot render VM template '"
					+ template.getName () + "'.", e );
		
		} finally {
            try {
            	writer.flush() ;
            	writer.close() ;
            } catch ( Exception e) { 
            	log.warn ( e ) ;
            }  
        }
	}

	
	public void generate( Context context, String vm, Writer writer ) throws Exception {
		generate ( context, getTemplate( vm ), writer ) ;
	} 


	public void generate( Context context, String vm, String out ) throws Exception {
		generate ( context, getTemplate( vm ), new FileWriter ( out ) ) ;
	} 

	
	public boolean templateExists ( String template ){
		return super.resourceExists ( template ) ;
	}
	
	
}
