/*
/*
 * 
 * Created on 5 mars 2007
 * 
 */
package com.dotemplate.core.shared;

import java.io.Serializable;
import java.util.LinkedHashMap;

import com.google.gwt.user.client.rpc.IsSerializable;


/**
 * 
 * @author Thierry Ruiz
 * 
 */
public class Scheme implements IsSerializable, Serializable, DesignResource {
	
	private static final long serialVersionUID = 5173000815866471810L;
	
	private String thumbnailHtml ;
	
	private boolean free ;
	
	private String name ;
	
	private String previewHtml ; // not used
	
	private String[] colors ;
		
	private LinkedHashMap< String, String > colorMap ;
	
	public Scheme() {
		colorMap = new LinkedHashMap< String, String >() ;
	}
	
	public String[] getColors () {
		return colors;
	}

	public void setColors ( String[] colors ) {
		this.colors = colors;
	}

	public String getThumbnailHtml() {
		return thumbnailHtml;
	}

	public void setThumbnailHtml(String thumbnailHtml) {
		this.thumbnailHtml = thumbnailHtml;
	}

	public boolean isPremium() {
		return !free;
	}

	public void setFree( boolean free ) {
		this.free = free;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPreviewHtml () {
		return previewHtml;
	}

	public void setPreviewHtml ( String previewHtml ) {
		this.previewHtml = previewHtml;
	}

	@Override
	public String getFilter () {
		return null;
	}
	
    public void setColor( String type, String value ){
    	colorMap.put( type , value ) ;
    }
    
    public String getColor( String type ){
    	return colorMap.get( type ) ;
    }
    
    @Override
    public String getTags() {
    	return null;
    }
    

 	
}


