package com.dotemplate.core.shared;

import java.io.Serializable;

import com.google.gwt.user.client.rpc.IsSerializable;

public interface DesignUpdate<D extends Design> extends Serializable, IsSerializable {
	
}
