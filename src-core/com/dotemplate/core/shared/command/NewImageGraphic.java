package com.dotemplate.core.shared.command;

import com.dotemplate.core.shared.command.NewGraphicResponse;
import com.dotemplate.core.shared.canvas.ImageGraphic;
import com.dotemplate.core.shared.canvas.ImageResource;
import com.dotemplate.theme.shared.command.ThemeCommand;



public class NewImageGraphic extends ThemeCommand< NewGraphicResponse<ImageGraphic> > {

	private static final long serialVersionUID = 6034879612018139431L;
	
	private ImageResource image ;
	
	private String uploadFilename ;
	
	private String stockImage ;
	
	
	public void setImage ( ImageResource image ) {
		this.image = image;
	}
	
	public ImageResource getImage () {
		return image;
	}

	public String getUploadFilename () {
		return uploadFilename;
	}

	public void setUploadFilename ( String uploadFilename ) {
		this.uploadFilename = uploadFilename;
	}

	public void setStockImage ( String url ) {
		this.stockImage = url ;
	}
	
	public String getStockImage() {
		return stockImage;
	}
	
	
}
