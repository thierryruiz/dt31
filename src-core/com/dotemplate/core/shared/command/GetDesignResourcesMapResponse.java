package com.dotemplate.core.shared.command;

import java.util.LinkedHashMap;

import com.dotemplate.core.shared.DesignResource;
import com.dotemplate.core.shared.frwk.Response;



public class GetDesignResourcesMapResponse< R extends DesignResource > implements Response {

	private static final long serialVersionUID = -6641181311904189709L;
		
	protected LinkedHashMap< String, LinkedHashMap< String, R > > resourcesMap ;

		
	public LinkedHashMap< String, LinkedHashMap< String, R > > getResourcesMap() {
		return resourcesMap;
	}

	public void setResourcesMap(
			LinkedHashMap<String, LinkedHashMap< String, R > > resourcesMap ) {
		this.resourcesMap = resourcesMap;
	}
		
	
}
