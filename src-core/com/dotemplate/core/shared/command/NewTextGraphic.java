package com.dotemplate.core.shared.command;

import com.dotemplate.core.shared.command.NewGraphicResponse;
import com.dotemplate.core.shared.canvas.TextGraphic;
import com.dotemplate.theme.shared.command.ThemeCommand;


public class NewTextGraphic extends ThemeCommand< NewGraphicResponse<TextGraphic> > {
	
	private static final long serialVersionUID = 7139838271965485881L;
		
	private TextGraphic text ;
	
	public void setText ( TextGraphic text ) {
		this.text = text;
	}
	
	public TextGraphic getText () {
		return text;
	}
	
	
}
