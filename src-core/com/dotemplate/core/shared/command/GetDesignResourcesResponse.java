package com.dotemplate.core.shared.command;

import java.util.ArrayList;

// import java.util.LinkedHashMap;

import com.dotemplate.core.shared.DesignResource;
import com.dotemplate.core.shared.frwk.Response;

public class GetDesignResourcesResponse< R extends DesignResource > implements Response {

	private static final long serialVersionUID = 6211492182096738552L;

	private ArrayList< R > resources ;
	
	private boolean moreResults = false ;  
	
	
	public ArrayList< R > getResources () {
		return resources;
	}

	public void setResources ( ArrayList< R > resources ) {
		this.resources = resources;
	}

	
	public boolean hasMoreResults() {
		return moreResults ;
	}

	
	public void setHasMoreResults ( boolean more ) {
		this.moreResults = more;
	}
	
	
	
}
