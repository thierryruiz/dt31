package com.dotemplate.core.shared.command;


public class GetScheme extends DesignCommand< GetSchemeResponse > {

	private static final long serialVersionUID = -6948340973782017540L;
	
	private String name ;
	
	public String getName() {
		return name;
	}
	
	public void setName( String name ) {
		this.name = name;
	}
	
	
}
