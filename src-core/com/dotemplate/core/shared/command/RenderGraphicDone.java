package com.dotemplate.core.shared.command;

import com.dotemplate.core.client.frwk.CommandCallback;
import com.dotemplate.core.shared.command.RenderGraphicResponse;
import com.dotemplate.core.shared.canvas.Graphic;




public abstract class RenderGraphicDone extends CommandCallback< RenderGraphicResponse > {
		
	Graphic graphic ;
	
	public RenderGraphicDone ( Graphic graphic ) {
		this.graphic = graphic ;
	}


}
