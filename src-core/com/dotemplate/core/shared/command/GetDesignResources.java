package com.dotemplate.core.shared.command;

import com.dotemplate.core.shared.DesignResource;
import com.dotemplate.core.shared.command.GetDesignResourcesResponse;


public class GetDesignResources< T extends DesignResource > extends DesignCommand< GetDesignResourcesResponse< T > > {

	private static final long serialVersionUID = 2168466817375207138L;
	
	private int index ;

	private int pagingLength = -1 ;
	
	private String filter ;
	
	private String tag ;
	
	public int getIndex () {
		return index;
	}

	public void setIndex ( int index ) {
		this.index = index;
	}

	public int getPagingLength () {
		return pagingLength;
	}

	public void setPagingLength ( int pagingLength ) {
		this.pagingLength = pagingLength;
	}

	public String getFilter () {
		return filter;
	}

	public void setFilter ( String filter ) {
		this.filter = filter;
	}

	public String getTag() {
		return tag;
	}

	public void setTag( String tag ) {
		this.tag = tag;
	}
	
	
}
