package com.dotemplate.core.shared.command;


import com.dotemplate.core.shared.DesignResource;
import com.dotemplate.core.shared.frwk.Response;


public class GetDesignResourcesTagsResponse< R extends DesignResource > implements Response {

	private static final long serialVersionUID = -7618293819679896733L;
	
	protected String[] resourcesTags ;
	
	public String[] getResourcesTags() {
		return resourcesTags;
	}
	
	public void setResourcesTags( String[] resourcesTags) {
		this.resourcesTags = resourcesTags;
	}
	
	
	
}
