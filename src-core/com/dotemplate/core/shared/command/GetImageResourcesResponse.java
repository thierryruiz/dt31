package com.dotemplate.core.shared.command;


import com.dotemplate.core.shared.command.GetDesignResourcesResponse;
import com.dotemplate.core.shared.canvas.ImageResource;


public class GetImageResourcesResponse extends GetDesignResourcesResponse< ImageResource > {

	private static final long serialVersionUID = -3295479668468688300L;
	
		
}
