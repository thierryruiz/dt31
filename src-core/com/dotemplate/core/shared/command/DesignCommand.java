package com.dotemplate.core.shared.command;


import com.dotemplate.core.shared.frwk.Command;
import com.dotemplate.core.shared.frwk.Response;


public abstract class DesignCommand< RESPONSE extends Response> implements Command< RESPONSE > {

	private static final long serialVersionUID = 7737014379930721490L;
	
	protected String designUid ;
		
	public String getDesignUid () {
		return designUid ;
	}
	
	public void setDesignUid( String designUid ) {
		this.designUid = designUid;
	}
	
	public  boolean isDesignUidRequired() {
		return true ;
	}
	
	
}
