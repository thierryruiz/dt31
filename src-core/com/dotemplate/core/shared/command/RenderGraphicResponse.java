package com.dotemplate.core.shared.command;

import com.dotemplate.core.shared.canvas.Graphic;
import com.dotemplate.core.shared.frwk.Response;



public class RenderGraphicResponse implements Response {

	private static final long serialVersionUID = 8582737476139701798L;
	
	private Graphic graphic ;

	
	public Graphic getGraphic () {
		return graphic;
	}

	public void setGraphic ( Graphic graphic ) {
		this.graphic = graphic;
	}

	
}
