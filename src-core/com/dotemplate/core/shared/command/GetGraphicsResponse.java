package com.dotemplate.core.shared.command;

import java.util.ArrayList;

import com.dotemplate.core.shared.canvas.Graphic;
import com.dotemplate.core.shared.frwk.Response;


public class GetGraphicsResponse implements Response {

	private static final long serialVersionUID = -2626440184448584571L;
	
	private ArrayList< Graphic > graphics ;
	
	
	public void setGraphics ( ArrayList<Graphic> graphics ) {
		this.graphics = graphics;
	}
	
	
	public ArrayList<Graphic> getGraphics () {
		return graphics;
	}
	
	
}
