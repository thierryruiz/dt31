package com.dotemplate.core.shared.command;

import com.dotemplate.core.shared.frwk.Response;
import com.dotemplate.core.shared.properties.Property;

public class GetPropertyResponse implements Response {

	private static final long serialVersionUID = -3301109851013589935L;
		
	Property property  ;
	
	public Property getProperty() {
		return property;
	}
	
	public void setProperty(Property property) {
		this.property = property;
	}
	
	
	
}
