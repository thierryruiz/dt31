package com.dotemplate.core.shared;

import java.io.Serializable;

import com.google.gwt.user.client.rpc.IsSerializable;

public class DesignResourceType  implements IsSerializable, Serializable {
	
	private static final long serialVersionUID = 3852172595442028926L;
	
	protected String provider ;
	
	public DesignResourceType() {
	}

	public DesignResourceType( String provider ){
		this.provider = provider ;
	}
	
	public String getProvider() {
		return provider;
	}
	
	public void setProvider( String provider ) {
		this.provider = provider;
	}
	

}
