package com.dotemplate.core.shared;

import java.io.Serializable;

import com.dotemplate.core.shared.properties.Property;
import com.dotemplate.core.shared.properties.PropertySet;
import com.google.gwt.user.client.rpc.IsSerializable;




public interface DesignTraverser extends IsSerializable, Serializable {

	boolean  startSet ( PropertySet set ) ;
	
	boolean  endSet ( PropertySet set ) ;
	
	boolean  startProperty ( Property property ) ;
	
	boolean  endProperty ( Property property ) ;
	
	
}
