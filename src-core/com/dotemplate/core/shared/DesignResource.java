package com.dotemplate.core.shared;

import java.io.Serializable;

import com.google.gwt.user.client.rpc.IsSerializable;


public interface DesignResource extends IsSerializable, Serializable {
	
	public boolean isPremium() ;
	
	public String getName() ;
	
	public String getThumbnailHtml() ;
	
	public String getFilter() ;
	
	public String getTags() ;
	
	
	
}
