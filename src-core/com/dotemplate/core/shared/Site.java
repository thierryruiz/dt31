
package com.dotemplate.core.shared;

public interface Site {

	public String getUid() ;
		
	public String getThemeUid() ;
	
	public String getType() ;
	
}
