package com.dotemplate.core.shared;

import java.io.Serializable;

import com.google.gwt.user.client.rpc.IsSerializable;

public class SVGFont implements IsSerializable, Serializable, DesignResource {
	
	private static final long serialVersionUID = -1139553250000762339L;

	private String family ;

	private boolean free ;
	
	private int ascent ;
	
	private int descent ;
	
	private String thumbnailHtml ;
	
	private String previewHtml ;
	
	public String getFamily () {
		return family;
	}

	public void setFamily ( String family ) {
		this.family = family;
	}
	
	public boolean isFreeResource () {
		return free ;
	}
	
	
	public void setFree ( boolean free ) {
		this.free = free;
	}
	
	public String getName () {
		return family ;
	}
	
	
	public String getThumbnailHtml() {
		return thumbnailHtml;
	}

	public void setThumbnailHtml(String thumbnailHtml) {
		this.thumbnailHtml = thumbnailHtml;
	}

	
	public String getPreviewHtml () {
		return previewHtml;
	}

	public void setPreviewHtml ( String previewHtml ) {
		this.previewHtml = previewHtml;
	}

	public boolean isPremium () {
		return !free;
	}

	public int getAscent () {
		return ascent;
	}

	public void setAscent ( int ascent ) {
		this.ascent = ascent;
	}

	public int getDescent () {
		return descent;
	}

	public void setDescent ( int descent ) {
		this.descent = descent;
	}

	public String getFilter () {
		return null;
	}
	
	@Override
	public String getTags() {
		return null;
	}
	
	
}
