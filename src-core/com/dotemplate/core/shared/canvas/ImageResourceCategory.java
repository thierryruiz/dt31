package com.dotemplate.core.shared.canvas;

import java.io.Serializable;
import java.util.ArrayList;

import com.google.gwt.user.client.rpc.IsSerializable;


public class ImageResourceCategory implements IsSerializable, Serializable {

	private static final long serialVersionUID = 179970617900544840L;

	private ArrayList< ImageResource > images = new ArrayList<ImageResource>() ;
	
	private String label ; 
			
	public String getLabel() {
		return label;
	}

	public void setLabel( String label ) {
		this.label = label;
	}
	
	public ArrayList< ImageResource > getImages() {
		return images;
	}

	public void addImage( ImageResource image ) {
		this.images.add ( image  ) ;
	}

	public int getImagesCount(){
		return images.size () ;
	}
	
	
}
