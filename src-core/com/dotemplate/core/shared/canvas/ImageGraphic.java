package com.dotemplate.core.shared.canvas;


public interface ImageGraphic extends Graphic {
	
	Float getOpacity() ;
	
	Integer getBlur() ;
	
	void setBlur ( Integer blur ) ;
	
	void setOpacity( Float opacity ) ;
	
	void setTopFading ( Float fadding ) ;
	
	Float getTopFading () ;
	
	void setLeftFading ( Float fadding ) ;
	
	Float getLeftFading () ;
	
	void setRightFading ( Float fadding ) ;
	
	Float getRightFading () ;
	
	void setBottomFading ( Float fadding ) ;
	
	Float getBottomFading () ;

	void setScale ( Float scale ) ;
	
	Float getScale () ;
	
	boolean isUploaded() ;
	
	
}

