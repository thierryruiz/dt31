package com.dotemplate.core.shared.canvas;


public interface TextGraphic extends Graphic {
	
	void setText ( String text ) ;
	
	String getText() ;
	
	String getFontFamily() ;
	
	void setFontFamily( String family ) ;
	
	Integer getFontSize () ;
	
	void setFontSize ( Integer fontSize ) ;

	
}
