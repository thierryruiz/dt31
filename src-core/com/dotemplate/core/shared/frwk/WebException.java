/*
 /*
 * 
 * Created on 20 juin 2007
 * 
 */

package com.dotemplate.core.shared.frwk;


import com.google.gwt.user.client.rpc.IsSerializable;


/**
 *  
 * @author Thierry Ruiz
 * 
 */
public class WebException extends Exception implements IsSerializable {

	private static final long serialVersionUID = -3304651103956716711L;

	public final static int USER = 10 ;
	
	public final static int SYSTEM = 11 ;
	
	public final static int SESSION_EXPIRED = 12 ;
	
	private int code = SYSTEM ;
	
	// Important Exception.message seems 
	// lost during serialization !  
	private String msg = "" ; 
		
	public WebException ( ) {
	}

	public WebException ( String msg ) {
		super ( msg );
		this.msg = msg ;
	}

	public WebException ( Throwable cause ) {
		this ( cause.getMessage () ) ;
	}

	public WebException ( String msg, Throwable t ) {
		this ( msg );
	}

	public WebException ( String message, int code ) {
		this ( message );
		this.code = code;
	}

	public WebException ( int code ) {
		this.code = code;
	}

	public WebException ( Throwable exception, int code ) {
		this ( exception );
		this.code = code;
	}

	public int getCode () {
		return code ;
	}

	public String getMessage () {
		return msg ;
	}
	
	
	
}
