package com.dotemplate.core.shared;



public class SymbolType extends DesignResourceType {
	
	private static final long serialVersionUID = 3150118796606596209L;
	
	protected String folder ;
		
	public SymbolType () {
	}
	
	public SymbolType( String folder, String provider ) {
		super( provider ) ;
		this.folder = folder ;
		this.provider = provider ;
	}
	
	public String getFolder() {
		return folder ;
	}
		
	
	public final static SymbolType PATTERN = new SymbolType( "pattern", "com.dotemplate.core.server.symbol.PatternProvider" ) ;

	public final static SymbolType TEXTURE = new SymbolType( "texture", "com.dotemplate.core.server.symbol.TextureProvider" ) ;
	
	public final static SymbolType OVERLAY = new SymbolType( "overlay", "com.dotemplate.core.server.symbol.OverlayProvider" ) ;

	public final static SymbolType TEXT = new SymbolType( "text", "com.dotemplate.core.server.symbol.TextProvider" ) ; 	

	
	
}
