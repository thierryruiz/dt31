package com.dotemplate.core.shared.properties;


public class BackgroundImageProperty extends Property {
	
	private static final long serialVersionUID = 1410480331522695661L;
	
	private String value ;
	
	public int getType () {
		return BGIMG ;
	}
	

	public String getValue() {
		return value;
	}


	public void setValue(String value) {
		this.value = value;
	}


	@Override
	public String[] getDiffAttributes() {
		return new String[] { "value", "label", "enable", "free", "editable" } ;
	}
	
	
}
