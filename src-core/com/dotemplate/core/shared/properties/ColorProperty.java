package com.dotemplate.core.shared.properties;



public class ColorProperty extends Property {

	private static final long serialVersionUID = 8738655825732349090L;

	public final static String REGEXP = "^#[0-9A-Fa-f]{6}$" ;
		
    private String regexp;

    private String value ;
    
   
	public ColorProperty() {
		setRegexp ( REGEXP ) ;
	}
	
	public String getValue () {
		return value;
	}

	/*
    public void setLabel( String label ) {
        this.label = label ;
    }*/
	
	public void setValue ( String value ) {
		this.value = value ;
	}

	public String getRegexp() {
		return regexp;
	}

	public void setRegexp( String regexp ) {
		this.regexp = regexp;
	}
		
	public int getType() {
		return COLOR ;
	}

	@Override
	public String[] getDiffAttributes() {
		return new String[] { "value", "label", "enable", "free", "editable" } ;
	}
	
	

}
