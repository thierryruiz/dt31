package com.dotemplate.core.shared.properties;

import java.io.Serializable;
import java.util.Collection;

import com.dotemplate.core.shared.DesignResource;
import com.dotemplate.core.shared.DesignTraverser;
import com.dotemplate.core.shared.SymbolType;
import com.google.gwt.user.client.rpc.IsSerializable;


public class PropertySet extends Property implements IsSerializable, Serializable, DesignResource  {

	private static final long serialVersionUID = 5440423205620035554L;
				
	private Boolean mutable ;
	
	private PropertyMap propertyMap = new PropertyMap ();
	
	private String parent ;
	
	private String ref ;
	
	private String path ;
	
	private String thumbnailHtml ;
	
	private String thumbnailUrl ;
	
	private String swappButton ;
	
	private SymbolType symbolType ;
	
	private String filter = "" ;
	
	private String styleClass ;
	
	private String styleId ;
	
	private String selectors;  
	
	private String highlight ; 
	
	private boolean built = false ;
	
	private String group ;
	
	private Boolean changeLayout ;
	
	private String tags ;
	
	private Integer sortIndex ;
	

	public String getSelectors () {
		return selectors;
	}

	public void setSelectors ( String selectors ) {
		this.selectors = selectors;
	}

	public String getStyleClass () {
		return styleClass;
	}
	
	public void setStyleClass ( String styleClass ) {
		this.styleClass = styleClass;
	}
	
	public void addStyleClass ( String c ) {
		this.styleClass = styleClass + " " + c ;
	}
	
	
	public Boolean getMutable() {
		return mutable;
	}

	public void setMutable ( Boolean mutable ) {
		this.mutable = mutable;
	}

	public void setMutable ( boolean mutable ) {
		this.mutable = mutable;
	}
	
	
	public String getStyleId () {
		return styleId;
	}

	public void setStyleId ( String styleId ) {
		this.styleId = styleId;
	}

	public int getType () {
		return SET ;
	}
	
	
	protected void ensureName(Property property) {
		if( property.getName() == null ){
			property.setName( getName() + "-p"+ propertyMap.values().size()  ) ;
		}
	}
	
	
	public void addProperty ( Property property ) {
		ensureName( property ) ;
		propertyMap.addProperty ( property );
	}
	
	


	public void addProperty ( String name,  Property property ) {
		ensureName( property ) ;
		propertyMap.addProperty ( name, property );
	}

	public void putProperty ( String name,  Property property ) {
		ensureName( property ) ;
		propertyMap.addProperty ( name, property );
	}
	
	public Collection<Property> getProperties () {
		return propertyMap.values ();
	}

	public void setProperties ( PropertyMap properties ) {
		this.propertyMap = properties;
	}
	
	
	public void setPropertyMap ( PropertyMap propetyMap ) {
		this.propertyMap = propetyMap;
	}
	
	public PropertyMap getPropertyMap () {
		return propertyMap;
	}

	public Property getProperty ( String name ) {
		return propertyMap.get ( name );
	}

	
	public String getParent () {
		return parent;
	}
	
	public void setParent ( String parent ) {
		this.parent = parent;
	}
	
	public String getRef() {
		return ref;
	}
	
	public void setRef(String ref) {
		this.ref = ref;
	}
	
	
	public String getPath () {
		return path;
	}

	public void setPath ( String path ) {
		this.path = path;
	}


	public String getThumbnailHtml () {
		return thumbnailHtml;
	}

	public void setThumbnailHtml ( String thumbnailHtml ) {
		this.thumbnailHtml = thumbnailHtml;
	}


	public String getThumbnailUrl () {
		return thumbnailUrl;
	}

	public void setThumbnailUrl ( String thumbnailUrl ) {
		this.thumbnailUrl = thumbnailUrl;
	}
	
	public String getHighlight () {
		return highlight;
	}
	
	public void setHighlight ( String highlight ) {
		this.highlight = highlight;
	}
	
	public boolean isSet() {
		return true ;
	}
	
	public boolean isBuilt () {
		return built;
	}
	
	public void setBuilt ( boolean built ) {
		this.built = built;
	}
	
	
	public SymbolType getSymbolType () {
		return symbolType;
	}

	
	public void setSymbolType ( SymbolType symbolType ) {
		this.symbolType = symbolType;
	}

	
	public boolean isPremium () {
		return ! ( free == null || ( free != null && free ) ) ;
	}
	
	public String getFilter () {
		return filter;
	}

	public void setFilter ( String filter ) {
		this.filter = filter;
	}
	
	public String getSwappButton () {
		return swappButton;
	}
	
	public void setSwappButton ( String swappButton ) {
		this.swappButton = swappButton;
	}
	
	public String getGroup() {
		return label ;
		//return group;
	}
	
	public void setGroup( String group ) {
		this.group = group;
	}
	
	
	public Boolean getChangeLayout() {
		return changeLayout;
	}

	public void setChangeLayout(Boolean changeLayout) {
		this.changeLayout = changeLayout;
	}
	
	
	public void setChangeLayout(boolean changeLayout) {
		this.changeLayout = changeLayout;
	}
	

	public boolean selectable (){
		return getSelectors () != null /*|| getStyleClass() != null*/ ;
	}
	
	public String getTags() {
		return tags;
	}

	
	public void setTags( String tags ) {
		this.tags = tags;
	}
	
	
	public Integer getSortIndex() {
		return sortIndex;
	}
	
	
	public void setSortIndex(Integer sortIndex) {
		this.sortIndex = sortIndex;
	}

	
	public boolean execute ( DesignTraverser traversal ) {		
		
		boolean result = traversal.startSet ( this ) ;
		
		if ( result ){
			
			result = propertyMap.execute ( traversal );
		
		} else {
			
			return false ;
		
		}
		
		return traversal.endSet ( this ) ;
		
		
		
		/*
		if ( traversal.startSet ( this ) ) {
			return propertyMap.execute ( traversal );
		} else {
			return false;
		}*/
		
		
		
	}
	
	
	
	
	// called by Velocity engine
	public Object get ( String name ) {
		
		Property property = propertyMap.get (  name  ) ;
		
		if ( property == null || ( property.getEnable() != null  
				&& !property.getEnable().booleanValue() ) ){
			
			return null ;
		
		}
		
		switch ( property.getType () ){
			case Property.SIZE :
				return ( (  SizeProperty ) property ).getValue ()  ;

			case Property.PERCENT :
				return ( (  PercentProperty ) property ).getValue ()  ;
		
			case Property.COLOR :
				return ( (  ColorProperty ) property ).getValue() ;
				
			case Property.STRING :
				return ( (  StringProperty ) property ).getValue() ;

			case Property.REF:
				return ( (  RefProperty ) property ).getValue() ;
								
			case Property.BOOLEAN : 
				return  ( ( BooleanProperty ) property ).isValue()  ;	
				
			case Property.BGIMG :
				return ( (  BackgroundImageProperty ) property ).getValue() ;				
				
			
			default : return property ;
		}	
	}
	
	

	public void set( String name, Object value ) {
		
		Property property = propertyMap.get (  name  ) ;
		
		if ( property == null ){
			return ;
			//throw new RuntimeException  ( "NO PROPERTY " + name + " in set " + this.getName() + " (parent=" + parent + ")"  ) ;
		}

		
		switch ( property.getType () ){
		
			case Property.SIZE :
				( (  SizeProperty ) property ).setValue ( ( Integer  ) value ) ; 
				property.setComputed( true );
				return ;

			case Property.PERCENT :
				( (  PercentProperty ) property ).setValue ( ( Float  ) value ) ; 
				property.setComputed( true );
				return ;
	
			case Property.COLOR :
				( (  ColorProperty ) property ).setValue( ( String ) value )  ; 
				property.setComputed( true );
				return ;
				
			case Property.STRING :
				( (  StringProperty ) property ).setValue( ( String ) value )  ; 
				property.setComputed( true );
				return ;

			case Property.REF:
				( (  RefProperty ) property ).setValue( ( String ) value )  ; 
				property.setComputed( true );
				return ;
							
			case Property.BOOLEAN : 
				( ( BooleanProperty ) property ) .setValue ( ( Boolean ) value )  ;	
				property.setComputed( true );
				return ;
				
			case Property.BGIMG :
				( (  BackgroundImageProperty ) property ).setValue( ( String ) value )  ; 
				property.setComputed( true );
				return ;				

			case Property.SET : 
				( ( PropertySet ) property ) .putProperty( name, ( Property ) value )  ; 
				( ( Property ) value ).setComputed( true ) ;  
				return ;
	
				
			default : return ;
		}
	}

	
	public boolean mutable() {
		return mutable != null && mutable ;
	}
	
	
	@Override
	public String[] getDiffAttributes() {
		return new String[] { "label", "enable", "free", "editable", "styleClass", "styleId",  "selectors" } ;
	}
		
	
}
