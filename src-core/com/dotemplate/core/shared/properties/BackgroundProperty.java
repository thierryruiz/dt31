package com.dotemplate.core.shared.properties;

import com.dotemplate.core.shared.canvas.BackgroundGraphic;
import com.dotemplate.core.shared.canvas.Graphic;


public class BackgroundProperty extends PropertySet implements BackgroundGraphic {
	
	private static final long serialVersionUID = 1L;

	private Integer width ;
		
	private Integer height ;
	
	private Integer top = 0 ;
	
	private Integer left = 0 ; 

	private Boolean visible ;
	
	private String workingUri ;
		
	
	public int getRenderedWidth () {
		return width ;
	}

	public int getRenderedHeight () {
		return height;
	}

	
	public String getWorkingUri () {
		return workingUri;
	}

	public void setWorkingUri ( String workingUri ) {
		this.workingUri = workingUri;
	}
	
	
	public Integer getWidth() {
		return width;
	}

	public void setWidth(Integer width) {
		this.width = width;
	}

	public Integer getHeight() {
		return height;
	}

	public void setHeight(Integer height) {
		this.height = height;
	}

	
	public boolean isResizeable() {
		return false;
	}

	public boolean isMoveable() {
		return false;
	}
	
	public Integer getTop () {
		return top;
	}

	public void setTop ( Integer top ) {
		this.top = top;
	}

	public Integer getLeft () {
		return left;
	}

	public void setLeft ( Integer left ) {
		this.left = left;
	}

	public int getType() {
		return Graphic.BG;
	}

	public void setVisible(Boolean visible) {
		this.visible = visible ;
	}
	
	public void setVisible(boolean visible) {
		this.visible = visible ;
	}
	
	
	public Boolean getVisible() {
		return visible;
	}
	
	public boolean isEmpty () {
		return false;
	}

	@Override
	public boolean isLocked () {
		return true ;
	}
	
	
	public boolean isBackground() {
		return true ;
	}
	
	@Override
	public String[] getDiffAttributes() {
		return new String[] { "label", "enable", "free", "editable", 
				"top", "left","width", "height", "visible"} ;
	}

	
}
