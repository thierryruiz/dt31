/**
 * Copyright Thierry Ruiz - Athesanet 2004-2008. All rights reserved.
 * Created on 28 f�vr. 08:22:00:01
 *
 */
package com.dotemplate.core.shared.properties;



/**
 *  
 *
 * @author Thierry Ruiz
 *
 */
public class BooleanProperty extends Property {
	
	private static final long serialVersionUID = 5980263353807946663L;

	private boolean value ;
	
	public boolean isValue () {
		return value;
	}

	public void setValue ( boolean value ) {
		this.value = value;
	}


	public int getType() {
		return BOOLEAN;
	}
	
	
	@Override
	public String[] getDiffAttributes() {
		return new String[] { "value", "label", "enable", "free", "editable" } ;
	}
	
}
