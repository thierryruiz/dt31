package com.dotemplate.core.shared.properties;

import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.Set;

import com.dotemplate.core.shared.DesignTraverser;
import com.google.gwt.user.client.rpc.IsSerializable;


public class PropertyMap implements IsSerializable, Serializable {

	private static final long serialVersionUID = 3820740038432780808L;

	private LinkedHashMap< String, Property > innerMap = new  LinkedHashMap<String, Property >() ;
	
	
	public void addProperty( Property prop ){	
		innerMap.put ( prop.getName (), prop ) ;	
	}
	
	
	public void addProperty( String name, Property prop ){		
		innerMap.put ( name, prop ) ;	
	}
	
	
	public Property remove ( Property prop ){
		return innerMap.remove ( prop.getName () ) ;
	}

	
	public void clear() {
		innerMap.clear () ;
	}
	
	
	public void overridesWith ( Collection<Property> properties ){		
		for ( Iterator<Property> it = properties.iterator (); it.hasNext () ; ){
			addProperty ( it.next () ) ;
		}
	}
	
	
	public boolean execute ( DesignTraverser traversal ) {
		
		for ( Iterator<Property> it = innerMap.values().iterator (); it.hasNext () ; ){
			if ( ! it.next ().execute( traversal ) ) return false ;
		}
		
		return true ;
		
	}
	
	public Collection<Property> values() {
		return innerMap.values () ;
	}
	
	
	public Property get( String name ){
		return innerMap.get( name ) ;
	}

	
	public Set<String> keySet() {
		return innerMap.keySet();
	}
	
	
	public Set< Entry < String, Property > > entrySet() {
		return innerMap.entrySet() ;
	}
	
	public boolean containsKey( Object name ){
		return innerMap.containsKey( name ) ;
	}
	
	
	
}
