package com.dotemplate.core.shared.properties;



public class SizeProperty extends Property {
	
	private static final long serialVersionUID = 1410480331522695661L;
	
	private Integer value ;
	
	private String regexp = "^[0-9]{1,4}$" ;

	private String range ;
	
	private Integer min ;
	
	private Integer max ;
	
	private int[] allowedValues ;
	
	
	public Integer getValue () {
		return value;
	}

	public void setValue ( Integer value ) {
		this.value = value;
	}

	public int getType() {
		return SIZE ;
	}
	
	public String getRegexp () {
		return regexp;
	}
	
	public void setRegexp ( String regexp ) {
		this.regexp = regexp;
	}

	public String getRange() {
		return range;
	}

	public void setRange(String range) {
		this.range = range;
	}

	public Integer getMin() {
		return min;
	}

	public void setMin(Integer min) {
		this.min = min;
	}

	public Integer getMax() {
		return max;
	}

	public void setMax(Integer max) {
		this.max = max;
	}
	
	public int[] getAllowedValues() {
		return allowedValues;
	}
	
	public void setAllowedValues( int[] allowedValues ) {
		this.allowedValues = allowedValues;
	}
	
	@Override
	public String[] getDiffAttributes() {
		return new String[] { "value", "label", "range", "enable", "free", "editable" } ;
	}
		
	
	
}
