/*
 /*
 * 
 * Created on 1 mars 2007
 * 
 */
package com.dotemplate.core.shared.properties ;

import java.io.Serializable;


import com.dotemplate.core.shared.DesignTraverser;
import com.google.gwt.user.client.rpc.IsSerializable;


/**
 * 
 * 
 *
 * @author Thierry Ruiz
 *
 */
public abstract class Property implements IsSerializable, Serializable  {
		
	private static final long serialVersionUID = 2527986709780560343L;

	public final static int SIZE = 0;

    public final static int BOOLEAN = 1;

    public final static int COLOR = 2;

    public static final int STRING = 4;
				
	public final static int SET = 10 ;
			
	public final static int PERCENT = 15 ;
	
	public final static int REF = 20 ;
	
	public final static int CANVAS = 25 ;
	
	public final static int BGIMG = 30 ;
	
    private String name;
    
    private String longName ;
    
    private String uid ;
    
    protected String label;
    
    protected String icon ;
    
    private String key ;
        
    private Boolean editable ;
    
    private Boolean enable ;
    
    protected Boolean free ;
    
    private boolean computed ;
    
    public abstract int getType();
    
    public String getUid () {
		return uid;
	}
    
    public void setUid ( String uid ) {
		this.uid = uid;
	}
    
    public String getName() {
        return this.name;
    }

    public void setName( String name ) {
        this.name = name;
    }

    public String getLabel() {
        return this.label;
    }

    public void setLabel( String label ) {
        this.label = label ;
    }
    
	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getKey() {
		return key;
	}

	public void setKey( String key ) {
		this.key = key;
	}
	
	public Boolean getEditable () {
		return editable;
	}
		
	public void setEditable ( Boolean editable ) {
		this.editable = editable;
	}

	public void setEditable ( boolean editable ) {
		this.editable = editable;
	}
	
	public Boolean getEnable () {
		return enable;
	}

	public void setEnable ( Boolean enable ) {
		this.enable = enable;
	}

	public void setEnable ( boolean enable ) {
		this.enable = enable;
	}
	
	public Boolean getFree () {
		return free;
	}

	public void setFree ( Boolean free ) {
		this.free = free;
	}

	public void setFree ( boolean free ) {
		this.free = free;
	}

	public String getLongName() {
		return longName;
	}

	public void setLongName( String longName ) {
		this.longName = longName;
	}

	public boolean execute ( DesignTraverser traversal ) {
		return traversal.startProperty ( this ) && 
			traversal.endProperty( this );
	}
	
	public boolean isComputed() {
		return computed;
	}
	
	public void setComputed(boolean computed) {
		this.computed = computed;
	}
	
	
	public boolean isSet() {
		return false ;
	}
	
	
	@Override
	public boolean equals ( Object obj ) {
		return ( ( Property ) obj ).getUid ().equals ( uid )  ;
	}
	
	@Override
	public int hashCode() {
		return super.hashCode();
	}
	
	public abstract String[] getDiffAttributes() ; 

	
}


