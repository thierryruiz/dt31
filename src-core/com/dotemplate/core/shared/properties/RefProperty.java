package com.dotemplate.core.shared.properties;



/**
 * 
 * A reference to an object in ThemeContext. 
 * Name and value are equal and are the key of the object.
 * 
 * @author T.Ruiz
 *
 */
public class RefProperty extends StringProperty {

	private static final long serialVersionUID = -3474680081329930263L;

	
	public RefProperty () {
		setFree( new Boolean ( true ) ) ;
		setEnable ( new Boolean ( true ) ) ;
		setEditable ( new Boolean ( false ) ) ;
	}
	
	
	@Override
	public int getType () {
		return REF;
	}
	
	@Override
	public String getValue () {
		return ( super.getValue() != null )  ? super.getValue() : getName()    ;
	}
	
	@Override
	public String[] getDiffAttributes() {
		return new String[] { "value" } ;
	}
	
}
