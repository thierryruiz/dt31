package com.dotemplate.core.shared;


import com.dotemplate.core.shared.frwk.Command;
import com.dotemplate.core.shared.frwk.Response;
import com.dotemplate.core.shared.frwk.WebException;
import com.google.gwt.user.client.rpc.RemoteService;

public interface EditService extends RemoteService {
	
	<T extends Response> T execute ( Command<T> action ) throws WebException ;

}
