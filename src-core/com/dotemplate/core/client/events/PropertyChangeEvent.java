package com.dotemplate.core.client.events;

import com.dotemplate.core.client.editors.PropertyEditor;
import com.dotemplate.core.client.frwk.Event;
import com.dotemplate.core.shared.properties.Property;
import com.dotemplate.core.shared.properties.PropertyMap;
import com.dotemplate.core.shared.properties.PropertySet;

public class PropertyChangeEvent extends Event< PropertyChangeEventHandler > {
			
	private Property property ;

	
	public PropertyChangeEvent( Property property ) {
		this.property = property ;
	}

	
	public static  Type<PropertyChangeEventHandler > TYPE = 
		new Type< PropertyChangeEventHandler >();
	

	@Override
	protected void dispatch( PropertyChangeEventHandler handler ) {
		handler.onPropertyChanged( this ) ;
	}

	
	@Override
	public com.google.gwt.event.shared.GwtEvent.Type< PropertyChangeEventHandler > getAssociatedType() {
		return TYPE ;
	}

	
	@SuppressWarnings("unchecked")
	public PropertyEditor<? extends Property > getEditor() {
		return ( PropertyEditor<? extends Property> ) getSource() ;
	}

	
	public Property getProperty() {
		return property;
	}
	
	
	public PropertyMap getMutablePropertyDeletedProperties(){
		return null ;
	}
	
	
	public PropertySet getSymbol() {
		return null ;
	}
	
	
}
