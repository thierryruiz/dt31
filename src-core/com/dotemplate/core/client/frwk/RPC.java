package com.dotemplate.core.client.frwk;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.dotemplate.core.client.Loader;
import com.dotemplate.core.client.canvas.presenter.CanvasEditor;
import com.dotemplate.core.shared.frwk.Command;
import com.dotemplate.core.shared.frwk.Response;
import com.dotemplate.core.shared.frwk.WebException;
import com.google.gwt.user.client.rpc.AsyncCallback;


public class RPC< T extends Response > {
	
	protected static Logger logger = Logger.getLogger( CanvasEditor.class.getName() );
	
	private Command<T> action ;
	
	private int retryCount = 0 ;
	
	private final static int MAX_RETRY = 2 ;
	
	private RemoteServiceAsync service ;
	
	
	public RPC ( RemoteServiceAsync service ) {
		this.service = service ;
	}

	
	public void execute ( Command<  T> action , CommandCallback< T > callback, String loadMsg ){
		execute( action, callback, loadMsg, true ) ;
	}

	
	
	public void execute ( Command<T> action , CommandCallback<T> callback ){
		execute( action, callback, null, true ) ;
	}
	
	
	
	public void execute ( Command<T> action , CommandCallback<T> callback, String loadMsg, boolean showAjaxLoader ){
		
		this.action = action ;
		
		Utils.preprocess ( service, "dt/rpc" ) ;
		
		logger.log ( Level.INFO,  "RPC call " + action.getClass ().getName () ) ;
		
		Loader.show ( loadMsg, showAjaxLoader ) ;
		
		service.execute( action, new ErrorProofCallback( callback ) ) ;
	
		
	}
	
	
	
	class ErrorProofCallback implements AsyncCallback< T >{
		
		CommandCallback<T> innerCallback ;
		

		public ErrorProofCallback ( CommandCallback< T > callback ) {
			innerCallback = callback ;
		}

		@Override
		public void onFailure ( Throwable oops ) {
			
			WebException we = null ;
			
			try {
				
				we = ( WebException ) oops ;
				
				Loader.hide() ;
				
				if ( WebException.SESSION_EXPIRED == we.getCode() ){
					
					Utils.alert( "Sorry, your editing session has expired due to inactivity." ) ;
					innerCallback.onFailure( oops ) ;
					return ;
				
				}
				
				Utils.alert( oops.getMessage () ) ;
				innerCallback.onFailure( oops ) ;
				
				
			} catch ( ClassCastException e ){
				
				if ( retryCount == MAX_RETRY ) {
					
					Loader.hide () ;
					Utils.alert( "Oops, something is wrong on the server sorry. Please retry or contact us." ) ;
					innerCallback.onFailure( oops ) ;
				
				} else {
					
					retry() ;
				
				}			
			} 
			
		}
		
		
		public void retry ( ){
			retryCount ++ ;
			execute( action, innerCallback ) ;
		}
		
		
		
		public void onSuccess( T response ) {
			Loader.hide () ;
			innerCallback.onSuccess( response ) ;		
		}
	
	}

	
	
}
