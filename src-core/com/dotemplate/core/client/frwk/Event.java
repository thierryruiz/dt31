package com.dotemplate.core.client.frwk;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

public abstract class Event< H extends EventHandler > extends GwtEvent< H > {

}
