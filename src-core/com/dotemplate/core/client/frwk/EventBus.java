package com.dotemplate.core.client.frwk;

import com.google.gwt.event.shared.HandlerManager;


public class EventBus extends HandlerManager {

	private static EventBus _instance = new EventBus() ;
	
	public static EventBus get() {
		return _instance ;
	}
	
	public EventBus () {
		super( null ) ;
	}
	
	
}
