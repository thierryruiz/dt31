package com.dotemplate.core.client.frwk;

import com.google.gwt.user.client.ui.IsWidget;


public interface View extends IsWidget {

	String getId() ;
	
}
