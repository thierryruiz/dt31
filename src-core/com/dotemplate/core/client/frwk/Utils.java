/*
 /*
 * 
 * Created on 10 d�c. 2007
 * 
 */
package com.dotemplate.core.client.frwk;


import org.gwtbootstrap3.extras.bootbox.client.Bootbox;

import com.dotemplate.core.client.resources.CSSInjector;
import com.dotemplate.core.client.resources.JavascriptInjector;
import com.extjs.gxt.ui.client.GXT;
import com.extjs.gxt.ui.client.core.DomQuery;
import com.extjs.gxt.ui.client.widget.MessageBox;
import com.google.gwt.dom.client.NodeList;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.resources.client.TextResource;
import com.google.gwt.user.client.DOM;
import com.google.gwt.dom.client.Element;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.CellPanel;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HasAlignment;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;




/**
 * 
 * 
 * @author Thierry Ruiz
 * 
 */
public class Utils {

	
	public static void useScript( TextResource js ){
		JavascriptInjector.inject( js ) ;
	}
	
	
	public static void useStylesheet( TextResource css ){
		CSSInjector.inject( css );
	}
	
	
	public static void alert( String msg ){
		Bootbox.alert(msg);
	}
	
	public static Image spacer( int width, int height ) {
		Image spacer = new Image ( "images/spacer.gif" );
		spacer.setPixelSize ( width, height );
		Element el = spacer.getElement () ;
		DOM.setStyleAttribute ( el, "margin", "0" );
		DOM.setStyleAttribute ( el, "padding", "0" );
		DOM.setStyleAttribute ( el, "border", "none" );		
		return spacer;
		
	}

	
	public static boolean eventInWidget( Event event, Widget widget ) {
		return inRectangle ( DOM.eventGetClientX ( event ), DOM
				.eventGetClientY ( event ), widget.getAbsoluteLeft (), widget
				.getAbsoluteTop (), widget.getOffsetWidth (), widget
				.getOffsetHeight () );
	}
	
	
	public static boolean mouseInElement( MouseOverEvent event, Element element ) {
		return inRectangle ( event.getClientX(), event.getClientY(), element.getAbsoluteLeft (), element
				.getAbsoluteTop (), element.getOffsetWidth (), element
				.getOffsetHeight () );
	}
	
	
	public static boolean eventInBounds( Event event, int x1, int y1,
			int x2, int y2  ) {
		int eX, eY ;
		eX = DOM.eventGetClientX ( event ) ;
		eY = DOM.eventGetClientY ( event ) ;
	
		return eX > x1 && eX < x2 && eY > y1
			&& eY < y2 ;
	}

	
	public static boolean inRectangle( int absX, int absY, int left, int top,
			int width, int height ) {
		return absX > left && absX < left + width && absY > top
				&& absY < top + height;
	}
	
	
	/** For the dt-wp plugin 
	public static void preprocess( RemoteServiceAsync service, String entryPoint ){
		( ( ServiceDefTarget ) service ).setServiceEntryPoint (		
			 "/wordpress-dt/dt-proxy.php" ) ;
	}
	*/
	
	
	public static void preprocess( RemoteServiceAsync service, String entryPoint ){
		( ( ServiceDefTarget ) service ).setServiceEntryPoint ( entryPoint  ) ;
	}
	
	
	@Deprecated // looks like it's not working properly
	public static native void refrehStyleSheet( String random )/*-{ 
    	var i, a, main;
  		for(i=0; (a = $doc.getElementsByTagName("link")[i]); i++) {
    		if(a.getAttribute("rel").indexOf("style") != -1 && a.getAttribute("title")) {
      			if(a.getAttribute("title") == "theme" ) a.href = a.href + random ;
    		}
  		}
	}-*/;
	
	

	
	public static NodeList< com.google.gwt.user.client.Element > selectDOM( String query ) {
		return DomQuery.select( query ) ;
	}
	
	
	
	public static native boolean isOldIE()/*-{ 
      var ua = navigator.userAgent.toLowerCase();
      if ( (ua.indexOf("msie 6.0") != -1) || (ua.indexOf("msie 5.5") != -1) || (ua.indexOf("msie 5.5") != -1)  ) {
      	return true ;
  	  } else return false ;
	}-*/;
	
	
	public static boolean isIE() {
		return GXT.isIE ;
	}

	
	
	public static void inline ( Widget w ) {
		if ( isIE() ){
			DOM.setStyleAttribute ( w.getElement (), "display", "inline" ) ;
		} else {
			DOM.setStyleAttribute ( w.getElement (), "display", "inline-block" ) ;
		}
	}
	
	
	
	/**
     * Evaluate scripts dynamically
     *
     * @param element a new HTML(text).getElement()
     */
    public static native void evalScript(String js) /*-{
         $wnd.eval(js);
    }-*/;


	public static void setId( Widget w, String id) {
		DOM.setElementAttribute( w.getElement(), "id", id ) ;
	}

	public static void marginLeft ( Widget w, int margin ) {
		DOM.setStyleAttribute ( w.getElement (), "marginLeft", margin + "px" ) ;
	}
	
	public static void marginTop ( Widget w, int margin ) {
		DOM.setStyleAttribute ( w.getElement (), "marginTop", margin + "px" ) ;
	}
	
	public static void marginRight( Widget w, int margin ) {
		DOM.setStyleAttribute ( w.getElement (), "marginRight", margin + "px" ) ;
	}
	
	public static void margins( Widget w, String margins ) {
		DOM.setStyleAttribute ( w.getElement (), "margin", margins ) ;
	}

	public static void center( Widget w ) {
		DOM.setStyleAttribute ( w.getElement (), "margin", "0 auto" ) ;
	}
	
	public static void style( Widget w, String name, String value ) {
		DOM.setStyleAttribute ( w.getElement (), name, value ) ;
	}	
	
	
	public static void marginTop( IsWidget w , int margin ){
		DOM.setStyleAttribute( w.asWidget().getElement(), "marginTop", margin + "px" ) ;
	}
	
	public static void marginLeft( IsWidget w , int margin ){
		DOM.setStyleAttribute( w.asWidget().getElement(), "marginLeft", margin + "px" ) ;
	}

	public static void marginRight( IsWidget w , int margin ){
		DOM.setStyleAttribute( w.asWidget().getElement(), "marginRight", margin + "px" ) ;
	}

	
	public static void margins( IsWidget w , int top, int right, int bottom, int left ){
		DOM.setStyleAttribute( w.asWidget().getElement(), "margin", 
				top + "px " +  right + "px " + bottom + "px " + left + "px ") ;
	}
	
	public static void margins( IsWidget w , int margin ){
		DOM.setStyleAttribute( w.asWidget().getElement(), "margin", margin + "px" ) ;
	}
	

	public static void style( IsWidget w , String name,  String value ){
		DOM.setStyleAttribute( w.asWidget().getElement(), name, value ) ;
	}	
	
	public static void alignLeft ( HasAlignment widget ) {
		widget.setHorizontalAlignment( HasAlignment.ALIGN_LEFT ) ;
	}
	
	public static void alignRight ( HasAlignment widget ) {
		widget.setHorizontalAlignment( HasAlignment.ALIGN_RIGHT ) ;
	}
	
	public static void alignCenter ( HasAlignment widget ) {
		widget.setHorizontalAlignment( HasAlignment.ALIGN_CENTER ) ;
	}
	
	public static void alignLeft ( CellPanel table, Widget w ) {
		table.setCellHorizontalAlignment( w, HasAlignment.ALIGN_LEFT ) ;
	}
	
	public static void alignRight ( CellPanel table, Widget w ) {
		table.setCellHorizontalAlignment( w, HasAlignment.ALIGN_RIGHT ) ;
	}
	
	public static void alignCenter ( CellPanel table, Widget w ) {
		table.setCellHorizontalAlignment( w, HasAlignment.ALIGN_CENTER ) ;
	}

	public static void alignMiddle ( CellPanel table, Widget w ) {
		table.setCellVerticalAlignment( w, HasAlignment.ALIGN_MIDDLE ) ;
	}	
	
	public static void alignTop ( CellPanel table, Widget w ) {
		table.setCellVerticalAlignment( w, HasAlignment.ALIGN_TOP ) ;
	}	
	
	public static void alignBottom ( CellPanel table, Widget w ) {
		table.setCellVerticalAlignment( w, HasAlignment.ALIGN_BOTTOM ) ;
	}
	
	
	public static void floatLeft( IsWidget w ){
		style( w, "cssFloat", "left" ) ;
		style( w, "styleFloat", "left" ) ;  // IE 
	}
	

	public static void floatRight( IsWidget w ){
		style( w, "cssFloat", "right" ) ;
		style( w, "styleFloat", "right" ) ; // IE
	}
	
	public static void clearBoth( IsWidget w ){
		style( w, "clear", "both" ) ;
	}
	
	public static void alignLeft (  FlexTable table, int row, int col  ) {
		table.getFlexCellFormatter().setHorizontalAlignment( row, col, HasAlignment.ALIGN_LEFT ) ;
	}
	
	public static void alignRight (  FlexTable table, int row, int col  ) {
		table.getFlexCellFormatter().setHorizontalAlignment( row, col, HasAlignment.ALIGN_RIGHT ) ;
	}
	
	public static void alignCenter (   FlexTable table, int row, int col  ) {
		table.getFlexCellFormatter().setHorizontalAlignment( row, col, HasAlignment.ALIGN_CENTER) ;
	}
	
	public static void alignMiddle ( FlexTable table, int row, int col ) {
		table.getFlexCellFormatter().setVerticalAlignment ( row, col, HasAlignment.ALIGN_MIDDLE ) ;
	}	
	
	public static void alignTop ( FlexTable table, int row, int col ) {
		table.getFlexCellFormatter().setVerticalAlignment( row, col, HasAlignment.ALIGN_TOP ) ;
	}	
	
	public static void alignBottom ( FlexTable table, int row, int col ) {
		table.getFlexCellFormatter().setVerticalAlignment( row, col, HasAlignment.ALIGN_BOTTOM ) ;
	}	
    

	public static final native String[] split( String s, String separator ) /*-{
		return s.split(separator);
	}-*/;

	
	
	public static final native void log( String s) /*-{
		console.log ( s ) ;
	}-*/;



	

	
}
