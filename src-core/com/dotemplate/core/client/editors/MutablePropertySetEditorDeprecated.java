package com.dotemplate.core.client.editors;

import org.gwtbootstrap3.client.ui.Button;
import org.gwtbootstrap3.client.ui.constants.ButtonType;

import com.dotemplate.core.client.Client;
import com.dotemplate.core.client.events.PropertyChangeEventHandler;
import com.dotemplate.core.client.frwk.CommandCallback;
import com.dotemplate.core.client.widgets.resource.DesignResourceChangeHandler;
import com.dotemplate.core.client.widgets.resource.DesignResourceProvider;
import com.dotemplate.core.shared.command.GetDesignResourcesMap;
import com.dotemplate.core.shared.command.GetSymbol;
import com.dotemplate.core.shared.command.GetSymbolResponse;
import com.dotemplate.core.shared.command.GetSymbols;
import com.dotemplate.core.shared.properties.PropertyMap;
import com.dotemplate.core.shared.properties.PropertySet;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Widget;

@Deprecated
public class MutablePropertySetEditorDeprecated extends PropertySetEditor 
	implements DesignResourceChangeHandler< PropertySet > {
	
	protected SymbolSelector symbolSelector ;
	
	protected PropertyEditorControl changeControl ;

	public MutablePropertySetEditorDeprecated( PropertySet set) {	
		super( set );
	}
	
	protected void chooseSymbol() {
		
		if ( symbolSelector == null ){
			
			DesignResourceProvider< PropertySet > symbolProvider = new DesignResourceProvider< PropertySet >() {
				
				@Override
				protected GetDesignResourcesMap< PropertySet > getDesignResourcesMapCommand() {
					
					GetSymbols getSymbols = new GetSymbols() ;
					
					getSymbols.setType ( property.getSymbolType () ) ;
					getSymbols.setFilter ( property.getFilter () ) ;
					
					return getSymbols ;
				
				}
			};
			
			
			symbolSelector = new SymbolSelector( symbolProvider ) ;
			
			symbolSelector.setHeading( property.getLabel() ) ;
		
		
		}
		
		symbolSelector.open( property, this ) ;
	
	}
	
	
	@Override
	public HandlerRegistration addHandler(PropertyChangeEventHandler h) {
		return super.addHandler(h);
	}
	
	
	@Override
	public void onValueChange( ValueChangeEvent< PropertySet > event ) {
		
		// load symbol from server 
		GetSymbol getSymbol = new GetSymbol() ;
		
		getSymbol.setDesignUid( Client.get().getDesign().getUid() ) ;
		
		getSymbol.setName( event.getValue().getName() ) ;
		
		Client.get().getDesignService().execute( getSymbol , new CommandCallback< GetSymbolResponse >()  {
					
			public void onSuccess( GetSymbolResponse response ) {
				
				PropertySet symbol = response.getSymbol() ;
				
				property.setParent ( symbol.getName () ) ;
				property.setPath ( symbol.getPath () ) ;
				property.setThumbnailHtml ( symbol.getThumbnailHtml () ) ;
				property.setThumbnailUrl( symbol.getThumbnailUrl() ) ;
				property.setFilter ( symbol.getFilter () ) ;
				property.setIcon( symbol.getIcon () ) ;
				
				PropertyMap deletedProperties = property.getPropertyMap() ;
				
				property.setPropertyMap ( symbol.getPropertyMap () ) ;
				
				setChangeControlIcon() ;
						
				support.fireSymbolChange( property, symbol, deletedProperties ) ; 
				
			};
		
		}) ;
	}
	
	
	
	@Override
	protected void createWidget() {

		String heading =  property.selectable() ? property.getLabel() : null ;

		control = new PropertySetEditorControl( heading ) ;

		// FIXME NG
		changeControl = new PropertyEditorControl(){
			
			@Override
			protected Widget createControl() {
				
				Button chooseButton = new Button("Choose...") ;
				
				chooseButton.setType( ButtonType.PRIMARY );
				
				chooseButton.addStyleName("dt-mutable-property-button");
				
				chooseButton.addClickHandler( new ClickHandler() {	
					@Override
					public void onClick( ClickEvent e ) {
						chooseSymbol() ;
					}
				} ) ;
				
				return chooseButton;
			}
			
			
			@Override
			public void disable() {
				// TODO Auto-generated method stub				
			}
			
			@Override
			public void enable() {
				// TODO Auto-generated method stub
			}


			@Override
			protected Widget createIcon() {
				// TODO Auto-generated method stub
				return null;
			}
			
		};
		
		
		changeControl.getControlContainer().setWidget( changeControl.createControl() ) ;
		
		setChangeControlIcon() ;
		
		changeControl.setId( editorId ) ;
		
		String lbl = property.getSwappButton() ;
		
		if ( lbl == null ){
			lbl = property.getLabel() ;
			if ( lbl == null ) lbl = "" ;
		}
		
		changeControl.getLabel().setText( lbl ) ;

		
		control.add( changeControl.asWidget() ) ;
		
	}
	

	public PropertyEditorControl getChangeControl() {
		return changeControl;
	}
	
	
	
	@Override
	public Widget asWidget() {
		
		if( control == null ) {
			
			createWidget() ;
		
		}
		
		return control.asWidget() ;
		
	}
	
		
	protected void setChangeControlIcon(){
		
		// FIXME
		/*changeControl.getIcon().setUrl( "images/spacer.gif" ) ;
		
		
		Style style = changeControl.getIcon().getElement().getStyle() ;
		
		if ( property.getIcon() != null ){
			style.setBackgroundImage("url(client/icons/themetoolbar/" + property.getIcon() + "?" + Random.nextInt()+")") ;
		} else {
			style.setBackgroundImage("url(" + ( ( PropertySet ) property ).getThumbnailUrl () + "?" + Random.nextInt() + ")" ) ;
		}*/
	}

	
}
