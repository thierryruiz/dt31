package com.dotemplate.core.client.editors;

import com.dotemplate.core.client.events.PropertyChangeEventHandler;
import com.dotemplate.core.client.widgets.StringInputDialog;
import com.dotemplate.core.client.widgets.StringInputDialog.Callback;
import com.dotemplate.core.shared.properties.StringProperty;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Widget;


public class StringPropertyEditor implements PropertyEditor<  StringProperty >, Callback {

	private StringProperty property ;
	
	private static StringInputDialog inputDialog ;
	
	private EditorSupport support ;
	
	protected StringPropertyEditorControl control ;
	
	protected PropertySetEditor parentEditor ;
	
	protected String editorId ;


	public StringPropertyEditor( StringProperty property ) {

		this.property = property ;
		
		this.support = new EditorSupport( this ) ;
		
		createWidget();
			
	}
	
	
	protected void setValue( String value ) {		
		property.setValue( value ) ;
		support.firePropertyChange( property ) ; 		
	}

	
	
	@Override
	public HandlerRegistration addHandler( PropertyChangeEventHandler h ) {
		return support.addHandler( h );
	}



	@Override
	public void removeHandler(PropertyChangeEventHandler h) {
		support.removeHandler( h ) ;
	}



	@Override
	public void fireEvent( GwtEvent<?> e ) {
		support.fireEvent( e ) ; 
	}


	@Override
	public StringProperty getProperty() {
		return property ;
	}

	
	@Override
	public void setProperty(StringProperty property) {
		this.property = property ;
	}
	
	
	
	@Override
	public Widget asWidget() {
		if ( control == null ) {
			createWidget() ;
		}
		
		return control;
	}

	
	
	public PropertySetEditor getParentEditor() {
		return parentEditor;
	}


	public void setParentEditor( PropertySetEditor parentEditor ) {
		this.parentEditor = parentEditor;
	}
	
	
	protected void createWidget() {
		
		control = new StringPropertyEditorControl( property.getIcon() ) ;
				
		control.setId( editorId ) ;
		
		// FIXME
		//control.getIcon().setUrl( "client/icons/themetoolbar/" + property.getIcon() ) ;
		
		doLabel();
		
		control.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				openInputDialog() ;
			}
		});
	}
	
	
	protected void doLabel(){
		String value = property.getValue() ;
		control.getLabel().setText( ( value == null || value.length() == 0 ) ? "Text" : value ) ;
	}
	
	
	protected void openInputDialog() {
		
		if ( inputDialog == null ){
			inputDialog = new StringInputDialog( ) ;
			inputDialog.setHeading( property.getLabel().toLowerCase() ) ;
			inputDialog.setLabel( "Enter " + property.getLabel().toLowerCase()  ) ;
		}
		
		inputDialog.open( property.getValue(),  this ) ;		
		
	}


	@Override
	public void remove() {
		control.asWidget().removeFromParent() ;
		
	}


	public String getEditorId() {
		return editorId;
	}


	public void setEditorId(String editorId) {
		this.editorId = editorId;
	}


	@Override
	public void enable() {
		control.enable();
	}


	@Override
	public void disable() {
		control.disable();
	}


	@Override
	public boolean isEnabled() {
		// FIXME NG
		return true ;
	}


	@Override
	public void onOk( String value ) {
		setValue( value ) ;
		doLabel() ;

	}
	
	
}
