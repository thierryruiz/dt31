package com.dotemplate.core.client.editors;

import com.dotemplate.core.client.canvas.BlurPanel;
import com.dotemplate.core.client.canvas.FadingPanel;
import com.dotemplate.core.client.canvas.ImageLayer;
import com.dotemplate.core.client.canvas.Layer;
import com.dotemplate.core.client.canvas.OpacityPanel;
import com.dotemplate.core.client.canvas.event.LayerEvent;
import com.dotemplate.core.shared.canvas.ImageGraphic;
import com.dotemplate.core.client.canvas.presenter.GraphicEditor;
import com.dotemplate.core.shared.properties.ImageProperty;
import com.dotemplate.core.shared.properties.Property;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Widget;



@Deprecated
public class ImageEditor extends GraphicEditor< ImageGraphic >  {

	private HorizontalPanel wrapper ;
	
	private OpacityPanel opacityPanel ;

	private FadingPanel fadingPanel ;
	
	private BlurPanel blurPanel ;


	
	public ImageEditor ( ImageGraphic image ) {
		
		super( image, null ) ;

		
		wrapper = new HorizontalPanel() ;
		wrapper.setStyleName( "ez-graphic-editor" ) ;
		
		wrapper.setSpacing ( 0 ) ;
			
		opacityPanel = new OpacityPanel() ;
		opacityPanel.setOpacity ( image.getOpacity() ) ;
		
		wrapper.add ( opacityPanel ) ;
		wrapper.setCellVerticalAlignment ( opacityPanel, HorizontalPanel.ALIGN_MIDDLE ) ;
		
		fadingPanel = new FadingPanel( 
				image.getTopFading(),
				image.getRightFading(),
				image.getBottomFading(), 
				image.getLeftFading())  ;

		
		wrapper.add (  fadingPanel ) ;
		wrapper.setCellVerticalAlignment ( fadingPanel, HorizontalPanel.ALIGN_MIDDLE ) ;
		
		
		blurPanel = new BlurPanel() ;
		blurPanel.setBlurDeviation( image.getBlur() ) ;
		wrapper.add ( blurPanel ) ;
		wrapper.setCellVerticalAlignment ( blurPanel, HorizontalPanel.ALIGN_MIDDLE ) ;
		
		
		
		opacityPanel.addChangeHandler( new ChangeHandler() {
			@Override
			public void onChange( ChangeEvent e ) {
				onChangeOpacity() ;
			}
		}) ;
		
		
		blurPanel.addChangeHandler( new ChangeHandler() {
			@Override
			public void onChange( ChangeEvent e ) {
				onChangeBlur() ;
			}
		}) ;
		
		
		fadingPanel.addChangeHandler( new ChangeHandler() {
			@Override
			public void onChange( ChangeEvent e ) {
				graphic.setTopFading( fadingPanel.getTopFading() ) ;
				graphic.setRightFading( fadingPanel.getRightFading() ) ;
				graphic.setBottomFading( fadingPanel.getBottomFading() ) ;
				graphic.setLeftFading( fadingPanel.getLeftFading() ) ;
				preview() ;
			}
		}) ;
		
		
	}


	
	private void onChangeOpacity() {
		graphic.setOpacity( opacityPanel.getOpacity() ) ;
		preview() ;
	}
	
	
	private void onChangeBlur() {
		graphic.setBlur( blurPanel.getBlurDeviation() ) ;
		preview() ;
	}
	
	
	@Override
	protected Layer< ImageGraphic > createLayer() {
		return new ImageLayer() ;
	}


	@Override
	public void onLayerResized( LayerEvent event) {		
		double scale = ( double ) layer.getWidth() / ( double ) graphic.getWidth () ;			
		graphic.setScale ( ( float ) scale )  ;
		preview() ;
	}


	@Override
	public void onLayerSelected( LayerEvent event) {
		
		fadingPanel.setTopFading( graphic.getTopFading() ) ;
		fadingPanel.setLeftFading( graphic.getLeftFading() ) ;
		fadingPanel.setRightFading( graphic.getRightFading() ) ;
		fadingPanel.setBottomFading( graphic.getBottomFading() ) ;
		
		opacityPanel.setOpacity(graphic.getOpacity()) ;
		blurPanel.setBlurDeviation(graphic.getBlur()) ;
		
	}
	
	
	@Override
	public ImageProperty getProperty() {
		return ( ImageProperty ) graphic ;
	}

	@Override
	public void setProperty(Property property) {
		this.graphic = ( ImageProperty ) property ;
	}
	

	@Override
	public Widget asWidget() {
		return wrapper ;
	}


	@Override
	public void setParentEditor( PropertySetEditor parentEditor ) {
		// TODO Auto-generated method stub
	}


	@Override
	public PropertySetEditor getParentEditor() {
		return null;
	}



	@Override
	public void bind() {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void onLayerRenamed(LayerEvent event) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void onLayerDeleted(LayerEvent event) {
		// TODO Auto-generated method stub
		
	}
	
	
	
}
