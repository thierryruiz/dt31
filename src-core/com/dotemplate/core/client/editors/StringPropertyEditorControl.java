package com.dotemplate.core.client.editors;

import org.gwtbootstrap3.client.ui.Button;
import org.gwtbootstrap3.client.ui.constants.ButtonType;

import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Widget;


public class StringPropertyEditorControl extends PropertyEditorControl implements HasClickHandlers {
	
	Button editButton ;
	
	String icon ;
	
	public StringPropertyEditorControl( String icon ) {
		
		super() ;
		
		this.icon = icon ;
		
		getIconContainer().setWidget( createIcon() ) ;
		getControlContainer().setWidget( createControl() ) ;
				
	}
	
	
	
	@Override
	protected Widget createControl() {
		
		editButton = new Button( "Edit...") ;
		
		editButton.setType( ButtonType.PRIMARY );
		
		return editButton ;
	
	}


	public void enable() {
	}


	public boolean isEnabled() {
		return false;
	}


	
	public void disable() {
	}

	public void setId(String editorId) {
		super.getElement().setId(editorId);
		
	}

	@Override
	public HandlerRegistration addClickHandler(ClickHandler handler) {
		return editButton.addClickHandler(handler);
	}

	@Override
	protected Widget createIcon() {
		HTML faIcon = new HTML() ;
		if ( icon != null ){
			String[] c =  icon.split("\\s+") ;
			if ( c != null && c.length > 0 ){
				for ( String t : c){
					
					faIcon.addStyleName( t );
				}
			}
		}
		return faIcon;
	}


}
