package com.dotemplate.core.client.editors;

public interface HasParentEditor {
	
	void setParentEditor( PropertySetEditor parentEditor ) ;
	
	PropertySetEditor getParentEditor() ;

	
}
