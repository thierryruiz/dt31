package com.dotemplate.core.client.editors;

import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Widget;


public abstract class PropertyEditorControl extends PropertyEditorLayout {
		
	protected String icon;
	
	
	@Deprecated
	public PropertyEditorControl( EditorControlStyle style ) {
	}
	
	
	public PropertyEditorControl() {
	}


	@Deprecated
	public void setIcon( String icon ){
	}	
	
	
	
	@Deprecated
	public void setIcon( String icon, boolean border ){		
	}
	
	
	@Deprecated
	public void setText( String text ) {
		label.setText( text );
	}
	
	protected abstract Widget createControl() ;
	
	public void setTip( String tip ){
	}
	
	public void setId( String id ){	
	}
	
	public abstract void enable() ;
		
	public abstract void disable();
		
	protected Widget createIcon() {
		HTML glyphIcon = new HTML() ;
		if ( icon != null ){
			String[] c =  icon.split("\\s+") ;
			if ( c != null && c.length > 0 ){
				for ( String t : c){
					
					glyphIcon.addStyleName( t );
				}
			}
		}
		
		return glyphIcon;
	}
	
		
}
