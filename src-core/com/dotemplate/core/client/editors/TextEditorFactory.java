package com.dotemplate.core.client.editors;


@Deprecated
public class TextEditorFactory extends CanvasEditorFactory {
	
	private static TextEditorFactory instance  ;
	
	
	public static TextEditorFactory get() {
		
		if ( instance == null ) {
			instance = new TextEditorFactory() ;
		}
		
		return instance ;
	}
	
	
	
}
