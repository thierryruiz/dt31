package com.dotemplate.core.client.editors;


import java.util.LinkedHashMap;

import com.dotemplate.core.client.widgets.resource.DesignResourceChangeHandler;
import com.dotemplate.core.client.widgets.resource.DesignResourceDialog;
import com.dotemplate.core.client.widgets.resource.DesignResourceList;
import com.dotemplate.core.client.widgets.resource.DesignResourceLoadAdapter;
import com.dotemplate.core.client.widgets.resource.DesignResourceProvider;
import com.dotemplate.core.client.widgets.resource.DesignResourceScrollSelector;
import com.dotemplate.core.client.widgets.resource.DesignResourceSelector;
import com.dotemplate.core.client.widgets.resource.DesignResourceThumbnail;
import com.dotemplate.core.shared.properties.PropertySet;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;


public class SymbolSelector extends DesignResourceDialog< PropertySet > {
	

	public SymbolSelector( DesignResourceProvider< PropertySet > resourceProvider ) {
		
		super( resourceProvider, 640, 500 ) ;

	}
	
	
	@Override
	protected DesignResourceSelector < PropertySet > createResourceSelector() {	
		return new SymbolScrollSelector() ;
	}
	

	public void open( final PropertySet selected, DesignResourceChangeHandler< PropertySet > callback ) {
		
		this.callback = callback ;

		if ( !resourceSelector.isLoaded() ){
			
			resourceProvider.addLoadHandler( new DesignResourceLoadAdapter<PropertySet>(){
				@Override
				public void onDesignResourcesLoad(
						LinkedHashMap<String, LinkedHashMap<String, PropertySet > > tags ) {
					
					( ( SymbolScrollSelector ) resourceSelector ) .setResources( tags ) ;
					
					resourceSelector.setValue( selected ) ;
					
					open() ;
					
				}
			});
			
			resourceProvider.loadResourcesMap() ;
			
			
		} else {
			
			resourceSelector.setValue( selected ) ;
			
			open() ;
			
		}
				
		
	}
	

	protected final class SymbolScrollSelector extends DesignResourceScrollSelector< PropertySet > {

		
		@Override
		protected DesignResourceList<PropertySet> createResourceList() {
			return new SymbolList() ;
		}
		
		
		@Override
		protected boolean isResourceAvailable(PropertySet resource) {
			return !resource.getName().contains( "abstract" ) ;
		}

		@Override
		public HandlerRegistration addValueChangeHandler(
				ValueChangeHandler<PropertySet> handler) {
			return handlerManager.addHandler( ValueChangeEvent.getType(), handler );
		}
				
	}
	
	
	private final class SymbolList extends DesignResourceList< PropertySet > {
		
		@Override
		protected DesignResourceThumbnail<PropertySet> getThumbnail(
				PropertySet set ) {
			
			return thumbnails.get( set.getParent() );
		
		}
		
	}


}