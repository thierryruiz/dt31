/*
/*
 * 
 * Created on 2 mars 2007
 * 
 */
package com.dotemplate.core.client.editors;


import com.dotemplate.core.client.Client;
import com.dotemplate.core.client.events.PropertyChangeEventHandler;
import com.dotemplate.core.client.frwk.CommandCallback;
import com.dotemplate.core.client.frwk.RPC;
import com.dotemplate.core.shared.Scheme;
import com.dotemplate.core.shared.command.GetProperty;
import com.dotemplate.core.shared.command.GetPropertyResponse;
import com.dotemplate.core.shared.properties.ColorProperty;
import com.dotemplate.theme.client.ThemeClient;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Widget;


/**
 * 
 * @author Thierry Ruiz
 * 
 */
public class ColorPropertyEditor implements PropertyEditor< ColorProperty >, ValueChangeHandler<Scheme> {
	
	protected ColorProperty property ;
	
	protected EditorSupport support ;

	protected PropertySetEditor parentEditor ;
	
	protected ColorPropertyEditorControl control ;
	
	protected String editorId ;
	
	public ColorPropertyEditor ( ColorProperty property ) {
		
		this.property = property ; 
		
		this.support = new EditorSupport( this ) ;
		
		
		
	}
	

//	protected void openColorPicker() {
//		
//		ColorPickerDialog colorPicker = Client.get().getColorPickerDialog() ;
//				
//		colorPicker.setSelectedColor( property.getValue() ) ;
//		colorPicker.setScheme( Client.get().getColorScheme() ) ;
//		colorPicker.setUserScheme( Client.get().getDesign().getUserScheme() ) ;
//		
//		colorPicker.open( this ) ;
//		
//	}
//
//	
//	
//	@Override
//	public void onColorSelected( String color ) {
//		
//		property.setValue( color ) ;
//		
//		control.setValue( color ) ;
//		
//		support.firePropertyChange( property ) ;
//	
//		Client.get().addUserColor( color ) ;
//	
//	}

	
	
	
	@Override
	public HandlerRegistration addHandler(PropertyChangeEventHandler h) {
		return support.addHandler( h );
	}



	@Override
	public void removeHandler( PropertyChangeEventHandler h ) {
		support.removeHandler( h ) ;
	}



	@Override
	public void fireEvent( GwtEvent<?> e ) {
		support.fireEvent( e ) ; 
	}


	@Override
	public ColorProperty getProperty() {
		return property ;
	}
	
	
	@Override
	public void setProperty(ColorProperty property) {
		this.property = property ;
	}
	

	@Override
	public Widget asWidget() {
		
		if ( control == null ) {
			createWidget() ; 
		}
		
		return control.asWidget() ;
	}

	
	public PropertySetEditor getParentEditor() {
		return parentEditor;	
	}


	public void setParentEditor( PropertySetEditor parentEditor ) {	
		this.parentEditor = parentEditor;
	}
	
	
	protected void createWidget() {
		
		control = new ColorPropertyEditorControl( property.getValue() ) ;
		
		control.getLabel().setText( property.getLabel() );
		
		control.addValueChangeHandler(new ValueChangeHandler<String>() {
			@Override
			public void onValueChange(ValueChangeEvent<String> event) {
				
				String color = event.getValue() ;
				property.setValue( color) ;				
				support.firePropertyChange( property ) ;
				Client.get().addUserColor( color ) ;
			
			}
		});
		
		control.setId( editorId ) ;
		
		ThemeClient.get().getEditor().addValueChangeHandler(this) ;
		
	}


	@Override
	public void remove() {
		control.asWidget().removeFromParent() ;
	}


	public String getEditorId() {
		return editorId;
	}


	public void setEditorId(String editorId) {
		this.editorId = editorId;
	}


	@Override
	public void enable() {
		control.enable();
	}


	@Override
	public void disable() {
		control.disable();
	}


	@Override
	public boolean isEnabled() {
		return control.isEnabled();
	}

	
	@Override
	public void onValueChange(ValueChangeEvent<Scheme> event) {
		
		
		// property value has chenge server side
		GetProperty getProperty = new GetProperty() ;
		
		getProperty.setDesignUid( Client.get().getDesign().getUid() ) ;
		
		getProperty.setUid( property.getUid() ) ;
	
		( new RPC< GetPropertyResponse >( Client.get().getDesignService() ) ).execute ( 
				getProperty, new CommandCallback< GetPropertyResponse >() {
				
			@Override
			public void onSuccess ( GetPropertyResponse response ) {
				
				property.setValue( ( ( ColorProperty ) response.getProperty() ).getValue() )  ;
				
				if ( control == null ) {
					createWidget() ;
				}
				
				control.setValue( property.getValue() ) ;

			}
			
		}) ;
	}
		
	
}
