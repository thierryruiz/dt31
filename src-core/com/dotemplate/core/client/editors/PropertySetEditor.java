package com.dotemplate.core.client.editors;

import com.dotemplate.core.client.events.PropertyChangeEventHandler;
import com.dotemplate.core.shared.properties.PropertySet;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Widget;


public class PropertySetEditor implements PropertyEditor< PropertySet >  {

	protected PropertySet property ;
	
	protected EditorSupport support ;
	
	protected PropertySetEditor parentEditor ;
	
	protected PropertySetEditorControl control ;
	
	protected String editorId ;
	
	
	public PropertySetEditor( PropertySet property ) {
		
		this.property = property ;
		
		this.support = new EditorSupport( this ) ;
		
	}

	
	public PropertySetEditorControl getControl() {
		
		if( control == null ) {
			createWidget() ;
		}
		
		return control;
	
	}
	
	
	@Override
	public PropertySet getProperty() {
		return property ;
	}
	
	
	@Override
	public void setProperty( PropertySet property ) {
		this.property = property ;
	}

	
	
	@Override
	public Widget asWidget() {
		
		if( control == null ) {
			createWidget() ;
		}
		
		return control.asWidget() ;
		
	}
	
	protected void createWidget() {
		control = new PropertySetEditorControl( 
				property.selectable() ? property.getLabel() : null ) ;
		
		control.setId( property.getUid() ) ;
		
	}
	
	
	
	@Override
	public HandlerRegistration addHandler( PropertyChangeEventHandler h ) {
		return support.addHandler( h );
	}


	@Override
	public void removeHandler( PropertyChangeEventHandler h ) {
		support.removeHandler( h ) ;
	}



	@Override
	public void fireEvent( GwtEvent<?> e ) {
		support.fireEvent( e ) ; 
	}


	public PropertySetEditor getParentEditor() {
		return parentEditor;
	}


	public void setParentEditor( PropertySetEditor parentEditor ) {
		this.parentEditor = parentEditor;
	}


	@Override
	public void remove() {
		control.asWidget().removeFromParent() ;
	}


	public String getEditorId() {
		return editorId;
	}

	
	public void setEditorId( String editorId ) {
		this.editorId = editorId;
	}
	
	
	public void select(){
		control.select();
	}


	public void unselect(){
		control.unselect();
	}

	
	@Override
	public void enable() {
		// FIXME NG
		//control.enable() ;
		
	}


	@Override
	public void disable() {
		// FIXME NG
		//control.disable() ;
	}
	
	
	@Override
	public boolean isEnabled() {
		// FIXME NG
		// return control.isEnabled() ;
		return true ;
	}
	
}
