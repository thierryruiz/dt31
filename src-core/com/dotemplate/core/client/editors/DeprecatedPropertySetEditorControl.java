package com.dotemplate.core.client.editors;


import com.extjs.gxt.ui.client.fx.FxConfig;
import com.extjs.gxt.ui.client.widget.Component;
import com.extjs.gxt.ui.client.widget.Header;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.button.ButtonGroup;
import com.extjs.gxt.ui.client.widget.layout.TableLayout;

import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;

@Deprecated
public class DeprecatedPropertySetEditorControl implements IsWidget {

	protected LayoutContainer layout ;

	protected ButtonGroup extButtons ;
	
	protected Header header ;
	
	
	public DeprecatedPropertySetEditorControl( String heading ) {
		
		if ( heading == null ){
			
			layout = new LayoutContainer( new TableLayout( 10 ) ) ;
		
		} else {
			
			extButtons = new ButtonGroup( 10 ) ;
			if ( heading == null || heading.length() == 0  ){
				heading = " " ;
			}
			
			extButtons.setHeading( heading ) ;
			header = extButtons.getHeader() ;
			layout = extButtons ;
		
		}

	}
	
	public void setId( String id ) {
		layout.setId( id ) ;
	}

	
	public void add ( Widget widget ) {
		layout.add( widget ) ;
	}
	
	public void insert ( Widget widget, int index ) {
		layout.insert( widget, index ) ;
	}
	
	
	public int indexOf( Widget w ){
		return layout.indexOf( ( Component ) w ) ;
	}
	
	
	@Override
	public Widget asWidget() {
		return layout ;
	}	
	
	
	public void layout() {
		layout.layout( true ) ;
	}
	
	
	public void select() {
		
		if ( extButtons == null ) return ;
		
		header.addStyleName("ez-set-select-bold" ) ;
		header.addStyleName("ez-set-select" ) ;
		
		header.el().blink(new FxConfig(){
			@Override
			public int getDuration() {
				return 300 ;
			}
		}) ;
		
	}

	
	public void unselect() {
		if ( extButtons == null ) return ;
		header.removeStyleName("ez-set-select-bold" ) ;
		header.removeStyleName("ez-set-select" ) ;
	}
	
	
	public void clear() {
		layout.removeAll() ;
	}

	public void remove( Widget widget ) {
		layout.remove( widget ) ;	
	}
	
	
	public void enable() {
		layout.enable() ;
	}
	
	
	public void disable() {
		layout.disable() ;
	}
	
	public boolean isEnabled() {
		return layout.isEnabled() ;
	}
	
}
