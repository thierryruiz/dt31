package com.dotemplate.core.client.editors;

import com.dotemplate.core.client.events.PropertyChangeEventHandler;
import com.dotemplate.core.client.widgets.NumberInputDialog;
import com.dotemplate.core.client.widgets.NumberInputDialog.Callback;
import com.dotemplate.core.shared.properties.PercentProperty;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Widget;


public class PercentPropertyEditor implements PropertyEditor<  PercentProperty >, Callback {

	private PercentProperty property ;
		
	private static NumberInputDialog percentDialog ;
	
	private EditorSupport support ;
	
	protected PropertyEditorControl control ;
	
	protected PropertySetEditor parentEditor ;
	
	protected String editorId ;
	
	
	public PercentPropertyEditor( PercentProperty property ) {

		this.property = property ;
		
		this.support = new EditorSupport( this ) ;
		
		createWidget();
		
		
	}
	

	protected void onCustomPercentSelected() {
		
		if ( percentDialog == null ){
			percentDialog = new NumberInputDialog( ) ;
		}
		
		percentDialog.setHeading( "Custom " +  property.getLabel().toLowerCase() ) ;
		percentDialog.setLabel( "Enter custom " + property.getLabel().toLowerCase() + " ( % )"  ) ;
		percentDialog.open( ( int ) ( property.getValue() * 100 ) , 0, 100, this ) ;
	
	}
	
	
	
	protected void setValue( int value ) {		
		property.setValue( ( float ) value / 100 ) ;
		support.firePropertyChange( property ) ;		
	}
	

	
	@Override
	public HandlerRegistration addHandler( PropertyChangeEventHandler h ) {
		return support.addHandler( h );
	}



	@Override
	public void removeHandler( PropertyChangeEventHandler h ) {
		support.removeHandler( h ) ;
	}



	@Override
	public void fireEvent( GwtEvent<?> e ) {
		support.fireEvent( e ) ; 
	}


	@Override
	public PercentProperty getProperty() {
		return property ;
	}

	
	@Override
	public void setProperty( PercentProperty property ) {
		this.property = property ;
	}
	
	
	
	@Override
	public Widget asWidget() {
		if ( control == null ) {
			createWidget() ;
		}
		
		return control.asWidget() ;
	}

	
	
	public PropertySetEditor getParentEditor() {
		return parentEditor;
	}


	public void setParentEditor( PropertySetEditor parentEditor ) {
		this.parentEditor = parentEditor;
	}
	
	
	
	protected void createWidget() {
	
		final PercentEditorControl dropdown = 
				new PercentEditorControl( ( new Double (property.getValue() * 100) ).intValue(), property.getIcon() ) ;
		
		control = dropdown ;
		
		dropdown.getLabel().setText( property.getLabel() );
		
		dropdown.setId( editorId ) ;
		
		dropdown.addValueChangeHandler( new ValueChangeHandler<Integer>() {
			@Override
			public void onValueChange( ValueChangeEvent<Integer> event ) {
				
				int value = event.getValue() ;
				
				if ( PercentEditorControl.CUSTOM_VALUE == value ){
					
					onCustomPercentSelected() ;
				} else {
					
					setValue( value ) ; 
				
				}
			}
		}) ;
		
	
		
		/*
    	final Menu menu = new Menu();
    	
    	PercentMenuItem item ;
    	
    	boolean custom = true ;
    	
    	for ( int i = 0 ; i <= 100 ; i = i + 10  ){
    		
    		menu.add( item = new PercentMenuItem( "" + i  + " %", i ) );  
    		
    		item.addSelectionListener( menuSelectionListener ) ;

    		if ( i == ( int ) ( property.getValue() * 100 ) ){
    			item.setSelected( true ) ;
    			custom = false ;
    		}
    	}

    	menu.add( item = new PercentMenuItem( "Custom value...", CUSTOM_VALUE ) )  ;  
    	
    	if ( custom ){
    		item.setSelected( true ) ;
    	}
    	
    	item.addSelectionListener( menuSelectionListener ) ;
    	
    	( ( Button ) control.asWidget() ).setMenu ( menu ) ;
    	*/
	}
	


	@Override
	public void remove() {
		control.asWidget().removeFromParent() ;
		
	}


	public String getEditorId() {
		return editorId;
	}


	public void setEditorId(String editorId) {
		this.editorId = editorId;
	}


	@Override
	public void enable() {
		control.enable();
	}


	@Override
	public void disable() {
		control.disable();
	}


	@Override
	public boolean isEnabled() {
		// FIXME NG
		return true ; // control.isEnabled();
	}


	@Override
	public void onOk( int value ) {
		setValue( value ) ;
	}
		
	
	
}
