package com.dotemplate.core.client.editors;


import org.gwtbootstrap3.client.ui.gwt.FlowPanel;

import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;


public class PropertySetEditorControl implements IsWidget {
	
	FlowPanel layout ;
	
	
	public PropertySetEditorControl( String heading ) {
		
		layout = new FlowPanel() ;
		
		layout.setStyleName( "dt-property-set-editor-control" );
		
		/* TODO delete
		if ( heading != null && heading.length() > 0 ){
			
			Strong title = new Strong() ;
			
			title.setText( heading );
			
			layout.add( title );
		
		}*/
		
	}
	
	public void setId( String id ) {
	}

	
	public void add ( Widget widget ) {
		layout.add( widget ) ;
	}
	
	
	public void insert ( Widget widget, int index ) {
		layout.insert( widget, index ) ;
	}
	
	
	public int indexOf( Widget w ){
		return DOM.getChildIndex(layout.getElement(), w.getElement() )  ;
	}
	
	
	@Override
	public Widget asWidget() {
		return layout ;
	}	
	
	
	
	public void select() {
		layout.addStyleName( "active" );
	}

	
	public void unselect() {
		layout.removeStyleName( "active" );	}
	
	
	public void clear() {
		// FIXME NG
		//layout.removeAll() ;
	}

	public void remove( Widget widget ) {
		// FIXME NG
		//layout.remove( widget ) ;	
	}
	
	
	public void enable() {
		// FIXME NG
		//layout.enable() ;
	}
	
	
	public void disable() {
		// FIXME NG
		//layout.disable() ;
	}
	
	public boolean isEnabled() {
		// FIXME NG
		//return layout.isEnabled() ;
		return true ;
	}
	
}
