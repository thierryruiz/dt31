package com.dotemplate.core.client.editors;


import org.gwtbootstrap3.client.ui.Button;

import com.dotemplate.core.client.Client;
import com.dotemplate.core.shared.properties.CanvasProperty;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;


public class CanvasPropertyEditor extends PropertySetEditor {
		
	public CanvasPropertyEditor( CanvasProperty property  ) {	
		
		super( property );
				
	}


	@Override
	protected void createWidget() {

		control = new PropertySetEditorControl( property.getLabel() ) ;
		
		Button button = new Button("Design " + property.getLabel().toLowerCase() ) ;
		
		button.setMarginLeft( 20 );
		
		button.addClickHandler( new ClickHandler() {
			@Override
			public void onClick( ClickEvent e ) {
				Client.get().editCanvas( ( CanvasProperty ) property ) ;
			}
		}) ;
		
		control.add( button );
			
	}
	
	

}
