package com.dotemplate.core.client.editors;

import com.dotemplate.core.client.apis.StockImage;
import com.dotemplate.core.client.apis.StockImageDetails;
import com.dotemplate.core.client.apis.unsplash.UnsplashImageAPI;
import com.dotemplate.core.client.canvas.event.SelectStockImageHandler;
import com.dotemplate.core.client.canvas.presenter.StockImageSelector;
import com.dotemplate.core.client.canvas.ui.StockImageSearchPanel;
import com.dotemplate.core.client.canvas.view.StockImageSearchView;
import com.dotemplate.core.client.events.PropertyChangeEventHandler;
import com.dotemplate.core.client.widgets.StringInputDialog.Callback;
import com.dotemplate.core.shared.properties.BackgroundImageProperty;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Widget;


public class BackgroundImagePropertyEditor implements PropertyEditor<  BackgroundImageProperty >, Callback {

	private BackgroundImageProperty property ;
		
	private EditorSupport support ;
	
	protected BackgroundImagePropertyEditorControl control ;
	
	protected PropertySetEditor parentEditor ;
	
	protected String editorId ;

	protected BackgroundImageEditorDialog editorDialog ;
	
	protected StockImageSelector backgroundImageSelector ;


	public BackgroundImagePropertyEditor( BackgroundImageProperty property ) {

		this.property = property ;
		
		this.support = new EditorSupport( this ) ;
		
		createWidget();
			
	}
	
	
	protected void setValue( String value ) {		
		property.setValue( value ) ;
		support.firePropertyChange( property ) ; 		
	}

	
	
	@Override
	public HandlerRegistration addHandler( PropertyChangeEventHandler h ) {
		return support.addHandler( h );
	}



	@Override
	public void removeHandler(PropertyChangeEventHandler h) {
		support.removeHandler( h ) ;
	}



	@Override
	public void fireEvent( GwtEvent<?> e ) {
		support.fireEvent( e ) ; 
	}


	@Override
	public BackgroundImageProperty getProperty() {
		return property ;
	}

	
	@Override
	public void setProperty(BackgroundImageProperty property) {
		this.property = property ;
	}
	
	
	
	@Override
	public Widget asWidget() {
		if ( control == null ) {
			createWidget() ;
		}
		
		return control;
	}

	
	
	public PropertySetEditor getParentEditor() {
		return parentEditor;
	}


	public void setParentEditor( PropertySetEditor parentEditor ) {
		this.parentEditor = parentEditor;
	}
	
	
	protected void createWidget() {
		
		control = new BackgroundImagePropertyEditorControl() ;
				
		control.setId( editorId ) ;
		
		control.getLabel().setText( property.getLabel() ) ;
		
		control.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				openInputDialog() ;
			}
		});
	}
	
	
	protected void openInputDialog() {
		
		if ( editorDialog == null ){
			
			editorDialog = new BackgroundImageEditorDialog() ;
			
			StockImageSearchView searchImagePanel = new StockImageSearchPanel() ;
			
			searchImagePanel.setSearchPlaceHolder( "Search Background Image..." );
			
			backgroundImageSelector = new StockImageSelector( searchImagePanel, new UnsplashImageAPI() ) ;

			editorDialog.setFlickImageView(searchImagePanel);
			
			backgroundImageSelector.addHandler( new SelectStockImageHandler() {
				@Override
				public void onStockImageSelected( StockImage image, StockImageDetails imageDetails) {
					
					applyImage ( imageDetails ) ;
					
					editorDialog.hide() ;
					
				}
			});
				
		}
		
		editorDialog.show();
		
	}


	protected void applyImage(StockImageDetails imageDetails) {
		property.setValue( imageDetails.getUrl() ) ;
		support.firePropertyChange( property ) ;
	}


	@Override
	public void remove() {
		control.asWidget().removeFromParent() ;
		
	}


	public String getEditorId() {
		return editorId;
	}


	public void setEditorId(String editorId) {
		this.editorId = editorId;
	}


	@Override
	public void enable() {
		control.enable();
	}


	@Override
	public void disable() {
		control.disable();
	}


	@Override
	public boolean isEnabled() {
		// FIXME NG
		return true ;
	}


	@Override
	public void onOk( String value ) {
		setValue( value ) ;
	}
	
	
}
