package com.dotemplate.core.client.editors;

import com.dotemplate.core.client.events.PropertyChangeEventHandler;
import com.dotemplate.core.shared.properties.BooleanProperty;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Widget;


public class BooleanPropertyEditor implements PropertyEditor<  BooleanProperty > {

	private BooleanProperty property ;

	
	private EditorSupport support ;
	
	protected BooleanPropertyEditorControl control ;
	
	protected PropertySetEditor parentEditor ;
	
	protected String editorId ;


	public BooleanPropertyEditor( BooleanProperty property ) {

		this.property = property ;
		
		this.support = new EditorSupport( this ) ;
		
		createWidget();
			
	}
	
	
	protected void setValue( Boolean value ) {		
		property.setValue( value ) ;
		support.firePropertyChange( property ) ; 		
	}

	
	
	@Override
	public HandlerRegistration addHandler( PropertyChangeEventHandler h ) {
		return support.addHandler( h );
	}



	@Override
	public void removeHandler(PropertyChangeEventHandler h) {
		support.removeHandler( h ) ;
	}



	@Override
	public void fireEvent( GwtEvent<?> e ) {
		support.fireEvent( e ) ; 
	}


	@Override
	public BooleanProperty getProperty() {
		return property ;
	}

	
	@Override
	public void setProperty(BooleanProperty property) {
		this.property = property ;
	}
	
	
	
	@Override
	public Widget asWidget() {
		if ( control == null ) {
			createWidget() ;
		}
		
		return control;
	}

	
	
	public PropertySetEditor getParentEditor() {
		return parentEditor;
	}


	public void setParentEditor( PropertySetEditor parentEditor ) {
		this.parentEditor = parentEditor;
	}
	
	
	protected void createWidget() {
		
		control = new BooleanPropertyEditorControl( property.isValue(), property.getIcon() ) ;
				
		control.setId( editorId ) ;
		
		control.getLabel().setText( property.getLabel() ) ;
			
		control.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
			@Override
			public void onValueChange(ValueChangeEvent<Boolean> event) {
				setValue( event.getValue() ) ;
			}
		}) ;
		
	}
	

	@Override
	public void remove() {
		control.asWidget().removeFromParent() ;
		
	}


	public String getEditorId() {
		return editorId;
	}


	public void setEditorId(String editorId) {
		this.editorId = editorId;
	}


	@Override
	public void enable() {
		control.enable();
	}


	@Override
	public void disable() {
		control.disable();
	}


	@Override
	public boolean isEnabled() {
		// FIXME NG
		return true ;
	}


	
}
