package com.dotemplate.core.client.editors;

import java.util.LinkedHashMap;

import com.dotemplate.core.client.events.PropertyChangeEvent;
import com.dotemplate.core.client.events.PropertyChangeEventHandler;
import com.dotemplate.core.client.frwk.Utils;
import com.dotemplate.core.shared.DesignTraverser;
import com.dotemplate.core.shared.canvas.Graphic;
import com.dotemplate.core.shared.properties.Property;
import com.dotemplate.core.shared.properties.PropertyMap;
import com.dotemplate.core.shared.properties.PropertySet;
import com.google.gwt.user.client.ui.HorizontalPanel;

@Deprecated
public abstract class MutableGraphicEditor < GRAPHIC extends Graphic > extends GraphicEditor< GRAPHIC >
	implements PropertyChangeEventHandler {

	protected HorizontalPanel subEditorsPanel ;
	
	protected MutablePropertySetEditor mutableEditor ;
	
	protected PropertyEditorMap subEditors ;
	
	protected DesignTraverser editorCreator ;
	
	public MutableGraphicEditor( GRAPHIC graphic ) {
		
		super( graphic );

		subEditorsPanel = new HorizontalPanel() ;

		Utils.marginTop( subEditorsPanel, 30 ) ;

		subEditors = new PropertyEditorMap() ;
		
		editorCreator = new DesignTraverser() {
			
			private static final long serialVersionUID = 1L;

			
			@Override
			public boolean startProperty( Property property ) {	
				return true;
			}
			
			
			@Override
			public boolean startSet( PropertySet set ) {
				
				boolean editable = set.getEditable () != null && set.getEditable () ;
	
				boolean enable = set.getEnable() != null &&  set.getEnable () ;
				
				if ( ! enable || !editable ) return true ;
								
				LinkedHashMap< String,  PropertyEditor < ? > > childEditors = 
					new LinkedHashMap< String, PropertyEditor< ? > > () ;
				
				for ( Property pp : ( ( PropertySet ) set ).getProperties() ){
	
					createEditors( pp, childEditors ) ;
					
				}
				
				return true;
			}


			@Override
			public boolean endSet(PropertySet set) {
				return true;
			}


			@Override
			public boolean endProperty(Property property) {
				return true;
			}
			
		};
	
		
	}
	
	
	protected abstract EditorFactory getEditorFactory() ;
	
	
	
	protected void createEditors ( Property p, LinkedHashMap< String,  
			PropertyEditor < ? extends Property > > childEditors ){
		
		boolean editable = p.getEditable () != null && p.getEditable () ;

		boolean enable = p.getEnable() != null &&  p.getEnable () ;

		if ( !editable || ! enable  ) return ;
		
		PropertyEditor < ? extends Property > e = getEditorFactory().createPropertyEditor( p ) ;
		
		if ( e == null ) return ;
		
		subEditors.put( p.getUid(), e ) ;
		
		e.addHandler( this ) ;
		
		childEditors.put( p.getUid(), e )   ;

		if ( p.isSet() ){
		
			for ( Property pp : ( ( PropertySet ) p ).getProperties() ){
				
				createEditors( pp, childEditors ) ;
				
			}
		}
		
	}
	
	
	@Override
	public void onPropertyChanged( PropertyChangeEvent e ) {
		
		Property p = e.getEditor().getProperty() ;
		
		if ( p.isSet() && ( ( PropertySet ) p ).mutable() ) {
			
			onMutablePropertyChanged ( e , ( MutablePropertySetEditor ) e.getEditor() ) ;
			
		}
		
		preview() ;
		
	}
	
	
	protected void onMutablePropertyChanged( PropertyChangeEvent e, MutablePropertySetEditor editor  ){
		
		PropertyMap deletedProperties = e.getMutablePropertyDeletedProperties() ;
		
		if ( deletedProperties != null  ){
			
			removeEditors( deletedProperties ) ;
		
		}
		
		LinkedHashMap< String,  PropertyEditor < ? > > childEditors = new LinkedHashMap< String, PropertyEditor< ? > > () ;
		
		for ( Property p : editor.getProperty().getProperties() ){
						
			if ( p.isSet() && ( ( PropertySet ) p ).selectable()  ){
				
				( ( PropertySet ) p ).execute( editorCreator ) ;
			
			} else {
				
				createEditors( p, childEditors ) ;
			
			}
			
		}
				
		
		for ( PropertyEditor< ? > childEditor : childEditors.values() ){
			
			subEditorsPanel.add ( childEditor.asWidget() ) ;
			
		}

	}
	
	
	
	
	protected void removeEditors( PropertyMap properties ){
		
		PropertyEditor<?> editor ;
		
		for ( Property p : properties.values() ){
			
			editor =  subEditors.remove( p.getUid() ) ;
			
			if ( editor != null ){
				editor.remove() ;
				editor.removeHandler( this ) ;
				editor = null ;	
			}
			
			
			
			if ( p.isSet() ){
				
				removeEditors( ( ( PropertySet ) p ).getPropertyMap() ) ;
			
			}
			
		}
		
	}



	
	
}
