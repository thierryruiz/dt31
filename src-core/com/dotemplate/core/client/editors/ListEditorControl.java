package com.dotemplate.core.client.editors;


import com.extjs.gxt.ui.client.event.MenuEvent;
import com.extjs.gxt.ui.client.widget.menu.Menu;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.HasChangeHandlers;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Widget;


@Deprecated
public class ListEditorControl<T extends Object> extends PropertyEditorControl implements HasChangeHandlers {

	protected HandlerManager handlerManager ;
	
	protected Menu menu ;
	
	protected SelectionListener<MenuEvent> itemListener ;
	
	protected T selected ;
	
	
	@SuppressWarnings("unchecked")
	public ListEditorControl( EditorControlStyle style ) {
		
		super( style ) ;
		
		handlerManager = new HandlerManager( this ) ;
	
		//extButton.setMenu( menu = new Menu() ) ;
	   	menu.setMaxHeight( 200 ) ;
		
		itemListener = new SelectionListener< MenuEvent >() {
			public void componentSelected(MenuEvent event ) {
				selected = ( ( MenuItem<T> )  event.getItem() ).getModel().getValue() ;
				handlerManager.fireEvent( new ChangeEvent() {});
			};
		};
		
	}

	
	@Override
	public HandlerRegistration addChangeHandler( ChangeHandler h ){ 
		return handlerManager.addHandler( ChangeEvent.getType(), h) ;
	}
	
	
	public void add ( Item< T > item ){		
		MenuItem<T> mi ;
		menu.add( mi = new MenuItem< T >( item ) ) ;
		mi.addSelectionListener( itemListener ) ;
	}
	
	
	
	public T getSelected() {
		return selected;
	}
	
	
	
	public interface Item<T> {
		
		String getLabel() ;
		
		T getValue() ;
		
	}
	
	
	@SuppressWarnings("hiding")
	private class MenuItem< T > extends com.extjs.gxt.ui.client.widget.menu.MenuItem {
		
		private Item< T > model ;
		
		
		public MenuItem( Item< T> item ) {
			this.model = item ;
			setText( model.getLabel() ) ;
		}
		
		@SuppressWarnings("unchecked")
		public Item<T> getModel() {
			return model;
		}
		
	}


	@Override
	protected Widget createControl() {
		// FIXME NG Auto-generated method stub
		return null;
	}


	@Override
	public void enable() {
		// FIXME NG Auto-generated method stub
		
	}


	@Override
	public void disable() {
		// FIXME NG Auto-generated method stub
		
	}


	@Override
	protected Widget createIcon() {
		// TODO Auto-generated method stub
		return null;
	}


	
}
