package com.dotemplate.core.client.editors;

import com.dotemplate.core.client.events.HasPropertyChangeEventHandlers;
import com.dotemplate.core.shared.properties.Property;
import com.google.gwt.user.client.ui.IsWidget;


public interface PropertyEditor< PROPERTY extends Property > extends IsWidget, HasPropertyChangeEventHandlers {
	
	PROPERTY getProperty() ;
	
	void setProperty( PROPERTY property ) ;
	
	void remove() ;
	
	void setParentEditor( PropertySetEditor parentEditor ) ;
	
	PropertySetEditor getParentEditor() ;
	
	void setEditorId( String id ) ;
	
	String getEditorId() ;
	
	void enable() ;
	
	void disable() ;
	
	boolean isEnabled() ;
	
	
}
