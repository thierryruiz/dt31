package com.dotemplate.core.client.editors;

import com.dotemplate.core.client.widgets.ListBox;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Widget;



public class PercentEditorControl extends PropertyEditorControl
	implements HasValueChangeHandlers< Integer > {

	
	protected static int CUSTOM_VALUE = -9999 ;
	
	PercentListBox listbox ;
	
	int range[] ;
	
	int current ;
	
	boolean custom = false ;
	
	
	public PercentEditorControl( int  current, String icon ) {
 
		this.icon = icon ;
		
		this.range = new int[]{ 0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100 } ;
		
		this.current = current ;
		
		getIconContainer().setWidget( createIcon() ) ;
		getControlContainer().add( createControl() ) ;
		
	}
	
	
	@Override
	protected Widget createControl() {
		
		listbox = new PercentListBox()  ;
		
		boolean custom = true ;
		
		for ( int v : range ){
    		
			
			listbox.add( v ) ;
			
			if ( v == current ){
				custom = false ; 
			}
			
		}
		
		
		
		
		listbox.add( CUSTOM_VALUE )  ;
		
		
		listbox.addClickHandler( new ClickHandler() {
			@Override
			public void onClick( ClickEvent event ) {
				ValueChangeEvent.fire( PercentEditorControl.this, ( Integer ) event.getSource()  );
			}
		}) ;
		
		
		if ( !custom ){
			listbox.setSelected( this.current );
		} else {
			listbox.setSelected ( CUSTOM_VALUE ) ;
		}
		
		return listbox ;
	
	}
	
	protected int getSelectedValue() {
		return listbox.getSelected()  ;
	}
	
	
	@Override
	protected void onLoad() {
		super.onLoad();
		
		listbox.refresh();
	}

	@Override
	public void enable() {
		// FIXME NG Auto-generated method stub
	}


	@Override
	public void disable() {
		// FIXME NG Auto-generated method stub
	}
	
	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<Integer> handler) {
		return super.addHandler( handler, ValueChangeEvent.getType() );
	}
	
	
	class PercentListBox extends ListBox < Integer > {

		
		public PercentListBox() {
			addStyleName( "dt-size-listBox" ) ;
		}
		
		
		@Override
		protected String dataAsString( Integer data ) {
			return ( CUSTOM_VALUE == data ) ? "Custom..." : data + "%" ;
		}
		
		@Override
		protected String uuid(Integer data) {
			return "" + data ;
		}
		
	}


}
