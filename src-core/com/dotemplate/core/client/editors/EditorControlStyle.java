package com.dotemplate.core.client.editors;


@Deprecated
public class EditorControlStyle {
	
	protected enum Layout { ICON_TOP, ICON_RIGHT } ; 

	protected enum Scale { SMALL, MEDIUM, LARGE } ; 
	
	protected int iconWidth = 30 ;
	
	protected int iconHeight = 30 ;
	
	protected int buttonWidth = 0 ;
	
	protected int buttonHeight = 0 ;
	
	protected Layout layout ;
	
	protected Scale scale ;
	
	public int getIconWidth() {
		return iconWidth;
	}

	public void setIconWidth(int iconWidth) {
		this.iconWidth = iconWidth;
	}

	public int getIconHeight() {
		return iconHeight;
	}

	public void setIconHeight(int iconHeight) {
		this.iconHeight = iconHeight;
	}

	public Layout getLayout() {
		return layout;
	}

	public void setLayout(Layout layout) {
		this.layout = layout;
	}

	public Scale getScale() {
		return scale;
	}

	public void setScale(Scale scale) {
		this.scale = scale;
	}
	
	
	public int getButtonWidth() {
		return buttonWidth;
	}

	public void setButtonWidth(int buttonWidth) {
		this.buttonWidth = buttonWidth;
	}

	public int getButtonHeight() {
		return buttonHeight;
	}

	public void setButtonHeight(int buttonHeight) {
		this.buttonHeight = buttonHeight;
	}


	public boolean labelWordWrap() {
		return true ;
	}
	
	
	public void addStyleClass( String styleClass ){
		
	}
	
	
	public final static EditorControlStyle TEXT_EDITOR_STYLE = new EditorControlStyle(){
		
		public int getIconHeight() {
			return 16;
		};
		
		public int getIconWidth() {
			return 16;
		};
		
		public Layout getLayout() {
			return Layout.ICON_RIGHT ;
		};
		
		public Scale getScale() {
			return Scale.MEDIUM ;
		};
		
		public boolean labelWordWrap() {
			return false ;
		}
		
	};
	
	
	public final static EditorControlStyle BACKGROUND_EDITOR_STYLE = new EditorControlStyle(){
		
		public int getIconHeight() {
			return 30;
		};
		
		public int getIconWidth() {
			return 30;
		};
		
		public Layout getLayout() {
			return Layout.ICON_RIGHT ;
		};
		
		public Scale getScale() {
			return Scale.LARGE ;
		};
		
		public int getButtonWidth() {
			return 180 ;
		};
		
		public int getButtonHeight() {
			return 42 ;
		};
		
		public boolean labelWordWrap() {
			return false ;
		};
		
	};
	
	
	
	public final static EditorControlStyle TOOLBAR_STYLE = new EditorControlStyle(){
		
		public int getIconHeight() {
			return 32;
		};
		
		public int getIconWidth() {
			return 32;
		};
		
		public Layout getLayout() {
			return Layout.ICON_TOP ;
		};
		
		public Scale getScale() {
			return Scale.LARGE ;
		};
		
	} ;

	

}
