package com.dotemplate.core.client.editors;



import com.dotemplate.core.client.canvas.Layer;
import com.dotemplate.core.client.canvas.TextLayer;
import com.dotemplate.core.client.canvas.event.LayerEvent;
import com.dotemplate.core.client.events.PropertyChangeEvent;
import com.dotemplate.core.client.frwk.Utils;
import com.dotemplate.core.client.widgets.TextBox;
import com.dotemplate.core.client.widgets.resource.DesignResourceChangeHandler;
import com.dotemplate.core.shared.SVGFont;
import com.dotemplate.core.shared.canvas.TextGraphic;
import com.dotemplate.core.shared.properties.Property;
import com.dotemplate.core.shared.properties.PropertySet;
import com.dotemplate.core.shared.properties.TextProperty;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

@Deprecated
public class TextEditor extends MutableGraphicEditor < TextGraphic >  {

	protected FlowPanel wrapper ;
	
	protected TextBox input ;
	
	protected PropertyEditorControl previewTextControl ;
	
	protected ListEditorControl< Integer > fontSizeControl ;
	
	protected PropertyEditorControl fontFamilyControl ;
	
	
	public TextEditor ( TextGraphic text ) {
		
		super( text ) ;
		
		( ( PropertySet ) text ) .execute( editorCreator ) ;
		
	
		wrapper = new FlowPanel() ;
		wrapper.setStyleName( "ez-graphic-editor" ) ;
		
		DOM.setStyleAttribute ( wrapper.getElement (), "textAlign", "left" ) ;
		
		wrapper.add( createTextPanel() )  ;
		wrapper.add( createTextSettingsPanel() )  ;
	
		Utils.marginTop( subEditorsPanel, 30 ) ;
		
		wrapper.add( subEditorsPanel ) ;
		
		
		for ( PropertyEditor< ? extends Property > e : subEditors.values() ){
			subEditorsPanel.add( e.asWidget() ) ;
		}
		
		
	}
	
	
	@Override
	protected Layer< TextGraphic > createLayer() {
		return new TextLayer() ;
	}

	
	
	private Panel createTextPanel() {
		
		VerticalPanel vp = new VerticalPanel() ;
		
		vp.setHorizontalAlignment ( VerticalPanel.ALIGN_LEFT );
		
		Label label = new Label( "Your text" ) ;
		label.setStyleName( "ez-canvas-text-label" ) ;
		
			
		vp.add (  label ) ;
		
		// text input		
		input = new TextBox() ;
		input.setStyleName( "ez-canvas-text-input" ) ;
        input.setMaxLength( 60 ) ;
        input.setWidth( 350 ) ;
        input.setText ( graphic.getText() ) ;
        
        input.addChangeHandler (new ChangeHandler() {
			@Override
			public void onChange( ChangeEvent e ) {
				graphic.setText( input.getText() ) ;
			}
		});
        
		input.addKeyDownHandler(new KeyDownHandler(){ 
			public void onKeyDown( KeyDownEvent event) {
				if ( KeyCodes.KEY_ENTER == event.getNativeKeyCode() ){
					onChangeText() ;
				}
			}
		}) ; 
        
        
	   	HorizontalPanel hp = new HorizontalPanel() ;
	   	hp.setVerticalAlignment ( VerticalPanel.ALIGN_MIDDLE );

    	hp.add ( input ) ;
        hp.setCellWidth( input, "380px" ) ;

	   	previewTextControl = new PropertyEditorControl(){

			@Override
			protected Widget createControl() {
				// FIXME  Auto-generated method stub
				return new Button("fix me") ;
			}

			@Override
			public void enable() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void disable() {
				// TODO Auto-generated method stub
				
			}

			@Override
			protected Widget createIcon() {
				// TODO Auto-generated method stub
				return null;
			}
	   		
	   	};
	   	previewTextControl.setText( "Preview text" ) ;
	   	previewTextControl.setIcon( "client/icons/CanvasTextEditor/preview.png" ) ;
	   	
	   	
	   	/* FIXME NG 
	   	previewTextControl.addClickHandler( new ClickHandler( ) {
			@Override
			public void onClick(ClickEvent e) {
				onChangeText() ;
			}
		}) ;
		*/
	   	
	   	hp.add ( previewTextControl  ) ;
	   	hp.setCellWidth( previewTextControl, "130px" ) ;
        
        
        vp.add( hp ) ;
        
        return vp ;

        
	}
	
	private HorizontalPanel createTextSettingsPanel() {
		
	   	HorizontalPanel hp = new HorizontalPanel() ;
	   	hp.setVerticalAlignment ( VerticalPanel.ALIGN_MIDDLE );
	   	
	   	fontSizeControl = new ListEditorControl< Integer >( EditorControlStyle.TEXT_EDITOR_STYLE ) ;
	   	fontSizeControl.setText( "Text Size..." ) ;
	   	fontSizeControl.setIcon( "client/icons/CanvasTextEditor/font-size.png" ) ;
	   	
        for ( int i = 10 ; i < 101 ; i++  ){
        	fontSizeControl.add( new FontSize( i ) ) ;
        }             
	   	
        fontSizeControl.addChangeHandler( new ChangeHandler() {
			@Override
			public void onChange( ChangeEvent e) {
				graphic.setFontSize ( fontSizeControl.getSelected () ) ;
				preview() ;
			}
	   	});
	   
	   	
	   	final DesignResourceChangeHandler< SVGFont > selectFontFamilyHdlr = new DesignResourceChangeHandler< SVGFont >() {
	   		
	   		@Override
	   		public void onValueChange(ValueChangeEvent<SVGFont> event) {
	   			//getProperty().setFontFamily( selectedFont.getFamily() ) ; 
				preview() ;	   			
	   		}
	   		
	   	};
	   	
	   	/* FIXME NG
	   	fontFamilyControl.addClickHandler( new ClickHandler () {
	   		@Override
	   		public void onClick( ClickEvent e ) {
	   			SVGFontSelector.show( selectFontFamilyHdlr, getProperty().getFontFamily() ) ;
	   		}
	   	}) ;
	   	*/
	   	
	   	
	   	fontFamilyControl = new PropertyEditorControl(){

			@Override
			protected Widget createControl() {
				// TODO Auto-generated method stub
				return new Button( "Fixe me ");
			}

			@Override
			public void enable() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void disable() {
				// TODO Auto-generated method stub
				
			}

			@Override
			protected Widget createIcon() {
				// TODO Auto-generated method stub
				return null;
			}
	   		
	   	};
	   	fontFamilyControl.setText( "Choose Font..." ) ;
	   	fontFamilyControl.setIcon( "client/icons/CanvasTextEditor/font-family.png" ) ;
	   	

	   	
 
        hp.add( fontSizeControl ) ;
        hp.add( fontFamilyControl ) ;
        
	   	
	   	// create text style editor
	   	mutableEditor = ( MutablePropertySetEditor ) 
	   		TextEditorFactory.get().createPropertyEditor( ( TextProperty ) getGraphic() ) ;
	   	
	   	
		mutableEditor.addHandler( this ) ;
		
		
		Widget widget = mutableEditor.asWidget() ;
		
		hp.add( widget ) ;
		hp.setCellHorizontalAlignment( widget, HorizontalPanel.ALIGN_LEFT ) ;
		//hp.setCellWidth( widget, "150px" ) ;
	   	
	   	Utils.marginTop( hp, 30 ) ;
	   	
	   	return hp ;
	   	
	   
	}

	
	@Override
	public void onPropertyChanged(PropertyChangeEvent e) {
		super.onPropertyChanged(e);
	}

	
	
	@Override
	protected void onMutablePropertyChanged( PropertyChangeEvent e,
			MutablePropertySetEditor editor ) {
		
		TextGraphic symbol = ( TextProperty ) e.getSymbol() ;
		
		//graphic.setFontSize( symbol.getFontSize() ) ;
		
		graphic.setFontFamily( symbol.getFontFamily() ) ;
		
		super.onMutablePropertyChanged( e, editor );
		
	}
	
	
	
	private void onChangeText() {
		
		String text = input.getText() ;
		
		if ( text == null || text.length() == 0 ){
			Utils.alert ( "Please, enter a valid text." ) ;
			input.selectAll() ;
			input.setFocus( true ) ;
			return ;
		}
		
		graphic.setText ( text ) ;
		fireChange() ;
		
		preview() ;
		
	}
	

	@Override
	public TextProperty getProperty() {
		return ( TextProperty )  graphic ;
	}

	
	public void setProperty( Property graphic ){
		this.graphic = ( TextProperty ) graphic ;
	}
	

	@Override
	public Widget asWidget() {
		return wrapper ;
	}

	
	class FontSize implements ListEditorControl.Item< Integer > {

		Integer value ;
		
		public FontSize( Integer i ) {
			this.value = i ;
		}
		
		@Override
		public String getLabel() {
			return value + " px" ;
		}

		@Override
		public Integer getValue() {
			return value ;
		}
		
	}


	@Override
	public void setParentEditor( PropertySetEditor parentEditor ) {
	}


	@Override
	public PropertySetEditor getParentEditor() {
		return null;
	}


	@Override
	protected EditorFactory getEditorFactory() {
		return TextEditorFactory.get() ;
	}


	@Override
	public void onLayerRenamed(LayerEvent event) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onLayerDeleted(LayerEvent event) {
		// TODO Auto-generated method stub
		
	}



	
}
