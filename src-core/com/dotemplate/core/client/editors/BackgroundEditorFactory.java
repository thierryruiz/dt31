package com.dotemplate.core.client.editors;


@Deprecated
public class BackgroundEditorFactory extends CanvasEditorFactory {
	
	private static BackgroundEditorFactory instance  ;
	
	
	public static BackgroundEditorFactory get() {
		
		if ( instance == null ) {
			instance = new BackgroundEditorFactory() ;
		}
		
		return instance ;
	}
	
	
	
}
