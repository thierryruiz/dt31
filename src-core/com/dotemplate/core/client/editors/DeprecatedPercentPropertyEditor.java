package com.dotemplate.core.client.editors;

import com.dotemplate.core.client.events.PropertyChangeEventHandler;
import com.dotemplate.core.client.widgets.NumberInputDialog;
import com.dotemplate.core.client.widgets.NumberInputDialog.Callback;
import com.dotemplate.core.shared.properties.PercentProperty;
import com.extjs.gxt.ui.client.event.MenuEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.widget.Component;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.menu.Menu;
import com.extjs.gxt.ui.client.widget.menu.MenuItem;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Widget;


public class DeprecatedPercentPropertyEditor implements PropertyEditor<  PercentProperty >, Callback {

	private PercentProperty property ;
		
	private static NumberInputDialog percentDialog ;
	
	private EditorSupport support ;
	
	protected PropertyEditorControl control ;
	
	
	protected PropertySetEditor parentEditor ;
	
	protected String editorId ;
	
	private static int CUSTOM_VALUE = -9000 ;
	
	
	public DeprecatedPercentPropertyEditor( PercentProperty property) {

		this.property = property ;
		
		this.support = new EditorSupport( this ) ;
		
		
	}
	
	
	protected SelectionListener< MenuEvent > menuSelectionListener = 
			new SelectionListener<MenuEvent>() {
		
		@Override
		public void componentSelected( MenuEvent event ) {
			
			onMenuSelected( ( PercentMenuItem )  event.getItem() ) ; 
		}
		
	};	
	
	
	
	protected void onMenuSelected( PercentMenuItem item ) {
		
		for ( Component menuitem : item.getParentMenu().getItems() ){
			(( PercentMenuItem ) menuitem ).setSelected( false ) ;
		}
		
		item.setSelected( true ) ;
		
		if ( item.isCustom() ){
			
			if ( percentDialog == null ){
				percentDialog = new NumberInputDialog() ;
			}
			
			percentDialog.setHeading( "Custom " +  property.getLabel().toLowerCase() ) ;
			percentDialog.setLabel( "Enter custom " + property.getLabel().toLowerCase() + " ( % )"  ) ;
			percentDialog.open( ( int ) ( property.getValue() * 100 ) , 0, 100, this ) ;
						
	
		} else {
			
			property.setValue( ( float ) item.value / 100 ) ;
			support.firePropertyChange( property ) ;
			
		}
		
	}
	

	
	@Override
	public HandlerRegistration addHandler( PropertyChangeEventHandler h ) {
		return support.addHandler( h );
	}



	@Override
	public void removeHandler( PropertyChangeEventHandler h ) {
		support.removeHandler( h ) ;
	}



	@Override
	public void fireEvent( GwtEvent<?> e ) {
		support.fireEvent( e ) ; 
	}


	@Override
	public PercentProperty getProperty() {
		return property ;
	}

	
	@Override
	public void setProperty( PercentProperty property ) {
		this.property = property ;
	}
	
	
	
	@Override
	public Widget asWidget() {
		if ( control == null ) {
			createWidget() ;
		}
		
		return control.asWidget() ;
	}

	
	
	public PropertySetEditor getParentEditor() {
		return parentEditor;
	}


	public void setParentEditor( PropertySetEditor parentEditor ) {
		this.parentEditor = parentEditor;
	}
	
	
	
	protected void createWidget() {		
		control = new PercentEditorControl(0, null) ;
		control.setText( property.getLabel() ) ;
		control.setIcon( "client/icons/themetoolbar/" + property.getIcon() ) ;
		control.setId( editorId ) ;
		
		// TODO move GXT menu outside editor class 
    	final Menu menu = new Menu();
    	
    	PercentMenuItem item ;
    	
    	boolean custom = true ;
    	
    	for ( int i = 0 ; i <= 100 ; i = i + 10  ){
    		
    		menu.add( item = new PercentMenuItem( "" + i  + " %", i ) );  
    		
    		item.addSelectionListener( menuSelectionListener ) ;

    		if ( i == ( int ) ( property.getValue() * 100 ) ){
    			item.setSelected( true ) ;
    			custom = false ;
    		}
    	}

    	menu.add( item = new PercentMenuItem( "Custom value...", CUSTOM_VALUE ) )  ;  
    	
    	if ( custom ){
    		item.setSelected( true ) ;
    	}
    	
    	item.addSelectionListener( menuSelectionListener ) ;
    	
    	( ( Button ) control.asWidget() ).setMenu ( menu ) ;
    	
	}
	


	@Override
	public void remove() {
		control.asWidget().removeFromParent() ;
		
	}


	public String getEditorId() {
		return editorId;
	}


	public void setEditorId(String editorId) {
		this.editorId = editorId;
	}


	@Override
	public void enable() {
		control.enable();
	}


	@Override
	public void disable() {
		control.disable();
	}


	@Override
	public boolean isEnabled() {
		// FIXME NG
		return true ; // control.isEnabled();
	}


	@Override
	public void onOk( int value ) {
		property.setValue( ( float ) value / ( float ) 100  ) ;
		support.firePropertyChange( property ) ;				
	}
	
	
	private class PercentMenuItem extends MenuItem {
		
		private float value ;
		
		public PercentMenuItem( String text,  float value ) {
			setText(text) ;
			this.value = value;
		}
		
		
		boolean isCustom() {
			return CUSTOM_VALUE == value ;
		}
		
		void setSelected( boolean selected ) {
			this.setStyleAttribute( "fontWeight", 
					selected ? "bold" : "normal" ) ;
		}
	
	}
	
	
	
}
