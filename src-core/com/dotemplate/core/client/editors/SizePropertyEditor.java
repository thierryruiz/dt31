package com.dotemplate.core.client.editors;

import com.dotemplate.core.client.events.PropertyChangeEventHandler;
import com.dotemplate.core.client.widgets.NumberInputDialog;
import com.dotemplate.core.client.widgets.NumberInputDialog.Callback;
import com.dotemplate.core.shared.properties.SizeProperty;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Widget;


public class SizePropertyEditor implements PropertyEditor<  SizeProperty >, Callback {

	private SizeProperty property ;
	
	private static NumberInputDialog customSizeDialog ;
	
	private EditorSupport support ;
	
	protected SizePropertyEditorControl control ;
	
	protected PropertySetEditor parentEditor ;
	
	protected String editorId ;


	public SizePropertyEditor( SizeProperty property ) {

		this.property = property ;
		
		this.support = new EditorSupport( this ) ;
		
		createWidget();
			
	}
	
	protected void onCustomSizeSelected() {
		
		if ( customSizeDialog == null ){
			customSizeDialog = new NumberInputDialog( ) ;
		}
		
		customSizeDialog.setHeading( "Custom " +  property.getLabel().toLowerCase() ) ;
		customSizeDialog.setLabel( "Enter custom " + property.getLabel().toLowerCase() + " (px)"  ) ;
		customSizeDialog.open( property.getValue(), property.getMin(), property.getMax(), this ) ;
	
	}
	
	
	protected void setValue( int value ) {		
		property.setValue( value ) ;
		support.firePropertyChange( property ) ; 		
	}

	
	
	@Override
	public HandlerRegistration addHandler( PropertyChangeEventHandler h ) {
		return support.addHandler( h );
	}



	@Override
	public void removeHandler(PropertyChangeEventHandler h) {
		support.removeHandler( h ) ;
	}



	@Override
	public void fireEvent( GwtEvent<?> e ) {
		support.fireEvent( e ) ; 
	}


	@Override
	public SizeProperty getProperty() {
		return property ;
	}

	
	@Override
	public void setProperty(SizeProperty property) {
		this.property = property ;
	}
	
	
	
	@Override
	public Widget asWidget() {
		if ( control == null ) {
			createWidget() ;
		}
		
		return control;
	}


	
	public PropertySetEditor getParentEditor() {
		return parentEditor;
	}


	public void setParentEditor( PropertySetEditor parentEditor ) {
		this.parentEditor = parentEditor;
	}
	
	
	protected void createWidget() {
		
		int[] range = property.getAllowedValues() ;
		
		final SizePropertyEditorControl sizeSelect = 
				new SizePropertyEditorControl( range, property.getValue(), property.getIcon() ) ;
		
		control = sizeSelect ;
		
		sizeSelect.getLabel().setText( property.getLabel() );
		
		// FIXME
		//sizeSelect.getIcon().setUrl( "client/icons/themetoolbar/" + property.getIcon() ) ;
		
		sizeSelect.setId( editorId ) ;
		
		
		sizeSelect.addValueChangeHandler( new ValueChangeHandler<Integer>() {
			@Override
			public void onValueChange( ValueChangeEvent<Integer> event ) {
				
				int value = event.getValue() ; 
				
				if ( SizePropertyEditorControl.CUSTOM_VALUE == value ){
				
					onCustomSizeSelected() ;
				
				} else {
					
					setValue( value ) ; 
				}
				
			}
		}) ;
	}
	
	
	@Override
	public void remove() {
		control.asWidget().removeFromParent() ;
		
	}


	public String getEditorId() {
		return editorId;
	}


	public void setEditorId(String editorId) {
		this.editorId = editorId;
	}


	@Override
	public void enable() {
		control.enable();
	}


	@Override
	public void disable() {
		control.disable();
	}


	@Override
	public boolean isEnabled() {
		// FIXME NG
		return true ;
	}


	@Override
	public void onOk( int value ) {
		setValue( value ) ; 				
	}
	
	
}
