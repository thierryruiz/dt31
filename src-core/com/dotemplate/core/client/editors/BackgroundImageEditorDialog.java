package com.dotemplate.core.client.editors;

import org.gwtbootstrap3.client.shared.event.ModalHideEvent;
import org.gwtbootstrap3.client.shared.event.ModalHideHandler;
import org.gwtbootstrap3.client.shared.event.ModalShowEvent;
import org.gwtbootstrap3.client.shared.event.ModalShowHandler;
import org.gwtbootstrap3.client.ui.Modal;

import com.dotemplate.core.client.canvas.view.StockImageSearchView;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Overflow;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;


public class BackgroundImageEditorDialog extends Composite  {

	private static BackgroundImageEditorDialogUiBinder uiBinder = GWT
			.create( BackgroundImageEditorDialogUiBinder.class );
	
	
	interface BackgroundImageEditorDialogUiBinder extends
			UiBinder<Widget, BackgroundImageEditorDialog> {
	}


	@UiField
	Modal modal ;
	
	
	@UiField 
	FlowPanel stockImagePlaceHolder ;
	
	
	StockImageSearchView stockImageSearchView ;
	
	
	public BackgroundImageEditorDialog( ) {
		
		initWidget( uiBinder.createAndBindUi(this) );
		
		// bootstrap fix
		modal.addHideHandler(new ModalHideHandler() {
			@Override
			public void onHide(ModalHideEvent evt) {
				RootPanel.getBodyElement().getStyle().setOverflow(Overflow.AUTO);
				
			}
		}) ;
		
		modal.addShowHandler(new ModalShowHandler() {
			
			@Override
			public void onShow(ModalShowEvent evt) {
				RootPanel.getBodyElement().getStyle().setOverflow(Overflow.AUTO);
				
			}
		});
		
	}

	
	public void setFlickImageView( StockImageSearchView view ) {
		stockImagePlaceHolder.add ( stockImageSearchView = view ) ;
	}
	
	
	public void hide() {
		modal.hide();	
	}

	public String getId() {
		return "backgroundImageEditorDialog" ;
	}
	

	public void show() {
		modal.show();
	}









	

}
