package com.dotemplate.core.client.editors;

import org.gwtbootstrap3.client.ui.html.Span;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

public class PropertyEditorLayout extends Composite {

	private static PropertyEditorLayoutUiBinder uiBinder = GWT
			.create(PropertyEditorLayoutUiBinder.class);

	interface PropertyEditorLayoutUiBinder extends
			UiBinder<Widget, PropertyEditorLayout> {
	}

	public PropertyEditorLayout() {
		initWidget(uiBinder.createAndBindUi(this));
	}
		
	@UiField
	SimplePanel controlContainer ;

	
	@UiField
	SimplePanel iconContainer ;
	
	@UiField
	Span label ;
	

	public Span getLabel() {
		return label;
	}
	
	public SimplePanel getIconContainer() {
		return iconContainer;
	}
	
	public SimplePanel getControlContainer() {
		return controlContainer;
	}
	
}


