package com.dotemplate.core.client.editors;

import com.dotemplate.core.client.widgets.ListBox;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Widget;


public class SizePropertyEditorControl extends PropertyEditorControl 
	implements HasValueChangeHandlers< Integer > {
	
	public static int CUSTOM_VALUE = -9999 ;
	
	int[] range ;
	
	SizeListBox listbox ;
	
	int current ;
	
	boolean custom = false ;
		
	public SizePropertyEditorControl( int[] range, int current, String icon ) {
		
		this.range = range ;
		this.current = current ;
		this.icon = icon ;
		
		getIconContainer().setWidget( createIcon() ) ;
		getControlContainer().add( createControl() ) ;
		
	}
	
	
	
	@Override
	protected Widget createControl() {
		
		listbox = new SizeListBox()  ;
		
		for ( int v : range ){
    		
			listbox.add( v ) ;
			
		}
		
		
		listbox.add( CUSTOM_VALUE )  ;
		
		listbox.addClickHandler( new ClickHandler() {
			@Override
			public void onClick( ClickEvent event ) {
				ValueChangeEvent.fire( SizePropertyEditorControl.this, ( Integer ) event.getSource() );
			}
		}) ;
		
		
		listbox.setSelected( current );
		
		return listbox ;
	
	}
	
	
	
	@Override
	protected void onLoad() {
		
		super.onLoad();
		
		listbox.refresh();
		
	}

	
	
	@Override
	public void enable() {
		// FIXME NG Auto-generated method stub
	}


	@Override
	public void disable() {
		// FIXME NG Auto-generated method stub
	}
	

	protected int getSelectedValue() {
		return listbox.getSelected() ;
	}
	
	

	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<Integer> handler) {
		return super.addHandler( handler, ValueChangeEvent.getType() );
	}

	
	class SizeListBox extends ListBox < Integer > {

		public SizeListBox() {
			addStyleName( "dt-size-listBox" ) ;
		}
		
		
		@Override
		protected String dataAsString( Integer data ) {
			return ( CUSTOM_VALUE == data ) ? "Custom..." : data + "px" ;
		}
		
		@Override
		protected String uuid(Integer data) {
			return "" + data ;
		}
		
	}
	
}
