package com.dotemplate.core.client.editors;

import org.gwtbootstrap3.extras.toggleswitch.client.ui.ToggleSwitch;
import org.gwtbootstrap3.extras.toggleswitch.client.ui.base.constants.SizeType;

import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Widget;


public class BooleanPropertyEditorControl extends PropertyEditorControl implements  HasValueChangeHandlers<Boolean> {
	
	ToggleSwitch toggle ;
	
	private boolean initialState = false ;
	
	public BooleanPropertyEditorControl(  boolean state, String icon ) {
		
		this.icon = icon ;
		
		initialState = state ;
		
		getIconContainer().setWidget( createIcon() ) ;
		getControlContainer().setWidget( createControl() ) ;
				
	}
	
	
	
	@Override
	protected Widget createControl() {
		
		toggle = new ToggleSwitch() ;
		toggle.setSize(SizeType.MINI);

		toggle.setOnText("Yes");
		toggle.setOffText("No");
		
		toggle.setValue(initialState, false );
		return toggle ;
	
	}


	public void enable() {
	}


	public boolean isEnabled() {
		return false;
	}


	public void disable() {
	}

	public void setId(String editorId) {
		super.getElement().setId(editorId);
	}


	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<Boolean> handler) {		
		return toggle.addValueChangeHandler(handler) ;
	}


}
