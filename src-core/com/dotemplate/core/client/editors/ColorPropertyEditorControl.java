package com.dotemplate.core.client.editors;

import com.dotemplate.core.client.widgets.colorpicker2.ColorPicker;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;


public class ColorPropertyEditorControl extends PropertyEditorControl implements HasValue< String > {

	private ColorPicker colorpicker ;
	
	private Image iconImage ;
	
	
	public ColorPropertyEditorControl( final String color ) {
		
		super() ;
				
		getIconContainer().setWidget( createIcon() ) ;
		getControlContainer().setWidget( createControl() ) ;
		
		setValue( color ) ;
		
	}
	
	
	
	@Override
	protected Widget createIcon() {
		iconImage = new Image() ;
		iconImage.setUrl( "images/spacer.gif" )  ;
		iconImage.addStyleName("dt-property-editor-color-icon");
		return iconImage ;
	}

	
	
	@Override
	protected Widget createControl() {

		colorpicker = new ColorPicker() ;
		
		colorpicker.addStyleName( "dt-color-picker-input" );

		colorpicker.addValueChangeHandler( new ValueChangeHandler<String>() {
			@Override
			public void onValueChange(ValueChangeEvent<String> event) {
				iconImage.getElement().getStyle().setBackgroundColor( event.getValue() );
			}
		}) ;
		
		return colorpicker;
	}


	public void enable() {
			
	}


	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return false;
	}


	
	public void disable() {
		// TODO Auto-generated method stub
	}

	public void setId(String editorId) {
		super.getElement().setId(editorId);
		
	}

	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<String> handler) {
		return colorpicker.addValueChangeHandler(handler);
	}

	@Override
	public String getValue() {
		return colorpicker.getValue();
	}

	@Override
	public void setValue(String value) {
		iconImage.getElement().getStyle().setBackgroundColor( value );
		colorpicker.setValue(value);
	}

	@Override
	public void setValue(String value, boolean fireEvents) {
		iconImage.getElement().getStyle().setBackgroundColor( value );
		colorpicker.setValue(value, fireEvents);		
	}

}
