package com.dotemplate.core.client.editors;


import com.dotemplate.core.shared.properties.BackgroundImageProperty;
import com.dotemplate.core.shared.properties.BooleanProperty;
import com.dotemplate.core.shared.properties.CanvasProperty;
import com.dotemplate.core.shared.properties.ColorProperty;
import com.dotemplate.core.shared.properties.PercentProperty;
import com.dotemplate.core.shared.properties.Property;
import com.dotemplate.core.shared.properties.PropertySet;
import com.dotemplate.core.shared.properties.SizeProperty;
import com.dotemplate.core.shared.properties.StringProperty;



public abstract class EditorFactory  {

	
	public PropertyEditor< ? extends Property > createPropertyEditor( Property property ) {
		
		
		/*
		boolean editable = property.getEditable () != null &&  property.getEditable () ;
		
		boolean enable = property.getEnable() != null &&  property.getEnable () ;
	
		
		if ( !editable || ! enable ) {
			
			Log.debug ( "Property '" + property.getName () + "' ignored because not editable or enabled." ) ;
			
			return null ;
		}*/
		
		
		//Log.debug ( "Creating property editor for property '" + property.getType() + "'" + property.getLongName () + "'." ) ;
		
		
		switch ( property.getType () ){
			
			case Property.SET :
				
				PropertySet set = ( PropertySet ) property ;

				if ( set.mutable() ) {
					return new MutablePropertySetEditor( set ) ;
				
				} else {
					return new PropertySetEditor( set ) ;				
				}
				

			case ( Property.CANVAS ) :
				return new CanvasPropertyEditor( ( CanvasProperty ) property  ) ;
			
			
			case ( Property.COLOR ) :
				return new ColorPropertyEditor( ( ColorProperty ) property ) ;
			
			
			case ( Property.SIZE ) :
				return new SizePropertyEditor( ( SizeProperty ) property ) ;
			
	
			case ( Property.PERCENT ) :
				return new PercentPropertyEditor( ( PercentProperty ) property ) ;	

			
			case ( Property.STRING ) :
				return new StringPropertyEditor( ( StringProperty ) property ) ;	

			
			case ( Property.BGIMG ) :
				return new BackgroundImagePropertyEditor( ( BackgroundImageProperty ) property ) ;	

			
			case ( Property.BOOLEAN ) :
				return new BooleanPropertyEditor( ( BooleanProperty ) property ) ;	

			
			
			default: return null ;
		
		}
		
	}
	
	

}
