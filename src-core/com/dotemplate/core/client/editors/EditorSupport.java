package com.dotemplate.core.client.editors;

import com.dotemplate.core.client.events.HasPropertyChangeEventHandlers;
import com.dotemplate.core.client.events.PropertyChangeEvent;
import com.dotemplate.core.client.events.PropertyChangeEventHandler;
import com.dotemplate.core.shared.properties.Property;
import com.dotemplate.core.shared.properties.PropertyMap;
import com.dotemplate.core.shared.properties.PropertySet;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.event.shared.HandlerRegistration;



/**
 * 
 * Creates the editors view
 * 
 * @author Thierry Ruiz
 *
 */
public class EditorSupport implements HasPropertyChangeEventHandlers {
	
	protected HandlerManager handlerManager;
	
	protected  PropertyEditor< ? extends Property > editor ;
	
	public EditorSupport( PropertyEditor< ? extends Property > editor ) {
		
		this.editor = editor ;
		
		handlerManager = new HandlerManager( editor ) ;		
	}
	
	
	@Override
	public HandlerRegistration addHandler( PropertyChangeEventHandler h ) {
		return handlerManager.addHandler( PropertyChangeEvent.TYPE, h  ) ;
	}
	
	
	
	@Override
	public void fireEvent( GwtEvent<?> e ) {
		handlerManager.fireEvent( e ) ;
	}
	

	public void firePropertyChange( Property property ) {
		handlerManager.fireEvent( new PropertyChangeEvent( property ) {
			@Override
			public Object getSource () {
				return editor ;
			}
		}) ;
	}

	
	@Override
	public void removeHandler( PropertyChangeEventHandler h ) {
		handlerManager.removeHandler( PropertyChangeEvent.TYPE, h ) ;
	}

	
	public void fireSymbolChange( 
			PropertySet set,
			final PropertySet symbol,
			final PropertyMap deletedProperties ) {
		

		
		handlerManager.fireEvent( new PropertyChangeEvent( set ) {
			
			@Override
			public Object getSource () {
				return editor ;
			}
			
			@Override
			public PropertyMap getMutablePropertyDeletedProperties() {
				return deletedProperties ;
			}
			
			@Override
			public PropertySet getSymbol() {
				return symbol ;
			}
		}) ;
		
	}
	
	
	
	
	
	


}
