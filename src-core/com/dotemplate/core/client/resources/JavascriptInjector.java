// -------------------------------------------------------------------------
// Copyright (c) 2006-2014 GEMALTO group. All Rights Reserved.
//
// This software is the confidential and proprietary information of
// GEMALTO.
//
// Project name: Sensorlogic Admin Portal
//
// Platform : Java virtual machine
// Language : JAVA 6.0
//
// Original author: mingli
//
// -------------------------------------------------------------------------
// GEMALTO MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
// THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
// TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
// PARTICULAR PURPOSE, OR NON-INFRINGEMENT. GEMALTO SHALL NOT BE
// LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING,
// MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
//
// THIS SOFTWARE IS NOT DESIGNED OR INTENDED FOR USE OR RESALE AS ON-LINE
// CONTROL EQUIPMENT IN HAZARDOUS ENVIRONMENTS REQUIRING FAIL-SAFE
// PERFORMANCE, SUCH AS IN THE OPERATION OF NUCLEAR FACILITIES, AIRCRAFT
// NAVIGATION OR COMMUNICATION SYSTEMS, AIR TRAFFIC CONTROL, DIRECT LIFE
// SUPPORT MACHINES, OR WEAPONS SYSTEMS, IN WHICH THE FAILURE OF THE
// SOFTWARE COULD LEAD DIRECTLY TO DEATH, PERSONAL INJURY, OR SEVERE
// PHYSICAL OR ENVIRONMENTAL DAMAGE ("HIGH RISK ACTIVITIES"). GEMALTO
// SPECIFICALLY DISCLAIMS ANY EXPRESS OR IMPLIED WARRANTY OF FITNESS FOR
// HIGH RISK ACTIVITIES.
// -------------------------------------------------------------------------

package com.dotemplate.core.client.resources;


import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.HeadElement;
import com.google.gwt.dom.client.ScriptElement;
import com.google.gwt.resources.client.TextResource;

public class JavascriptInjector {

    private static Logger logger = Logger.getLogger ("");

    private static HeadElement head;

    private static ArrayList< String > injectedScripts = new ArrayList < String >()  ;   
    
    public static void inject ( TextResource js  ) {
    	
    	if( injectedScripts.contains( js.getName() ) ){
    		return ;
    	}


        ScriptElement script = Document.get ().createScriptElement ();
        script.setAttribute ("language", "javascript");
        script.setText ( js.getText() );
        ensureHead().appendChild (script);
        injectedScripts.add( js.getName() ) ;
        
    }

    
    public static void inject ( String url  ) {
    	
    	if( injectedScripts.contains( url ) ){
    		return ;
    	}

 
        ScriptElement script = Document.get ().createScriptElement ();
        script.setAttribute ("language", "javascript");
        script.setSrc( url );
        ensureHead().appendChild (script);
        injectedScripts.add( url ) ;
    
    }
    
    
    
    protected static HeadElement ensureHead(){
        if (head == null) {
            Element element = Document.get ().getElementsByTagName ("head").getItem (0);
            if (element == null) {
                if (logger.isLoggable (Level.SEVERE)) {
                    logger.log (Level.SEVERE, "Javascript Injection failed, Head element required.");
                    return null;
                }
            }
            
            head = HeadElement.as (element);
        
        }
        
        return head ;
        
    }
    
    
    
}
