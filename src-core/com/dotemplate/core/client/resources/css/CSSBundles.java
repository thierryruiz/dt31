package com.dotemplate.core.client.resources.css;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.TextResource;

public interface CSSBundles extends ClientBundle {
    
    public static CSSBundles INSTANCE = GWT.create (CSSBundles.class);
    
    @Source( "core-editor.css" )
    TextResource getCoreEditorCss();
    
    @Source( "farbtastic.css")
    TextResource getColorPickerCss();
    
    @Source( "jquery-ui-1.12.0.min.css")
    TextResource getJQueryUiCss();
    
}
