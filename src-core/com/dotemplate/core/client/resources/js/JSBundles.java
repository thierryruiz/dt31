package com.dotemplate.core.client.resources.js;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.TextResource;

public interface JSBundles extends ClientBundle {
    
    public static JSBundles INSTANCE = GWT.create (JSBundles.class);
    
    @Source( "farbtastic.js")
    TextResource getColorPickerJs();
    
    @Source( "jquery-ui-1.12.0.min.js") //custom
    TextResource getJQueryUiJs();
    
    
}