package com.dotemplate.core.client.apis.flickr;

import java.util.ArrayList;

import com.dotemplate.core.client.apis.StockImage;
import com.dotemplate.core.client.apis.StockImageAPI;
import com.dotemplate.core.client.apis.StockImageDetails;
import com.google.gwt.core.client.Callback;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;


public class FlickImageAPI implements StockImageAPI {
	
	final static String _apikey = "4ef2fe2affcdd6e13218f5ddd0e2500d" ;
	
	// saysayboomak@yahoo.fr ym@...5o
	
	
	@Override
	public void search( String search, int page, int pageSize, final Callback<  ArrayList< StockImage >,  String > callback ) {
		
		Callback< JsArray < JavaScriptObject >, String  > jscallback = new Callback< JsArray < JavaScriptObject >, String  >(){

			@Override
			public void onFailure(String reason) {
				callback.onFailure(reason);
			}

			@Override
			public void onSuccess( JsArray<JavaScriptObject> jsresult) {
				
				ArrayList< StockImage > images = new ArrayList< StockImage >() ;
				
				for ( int i = 0 ; i < jsresult.length() ; i++ ){
					images.add( new FlickrImage( jsresult.get( i ) ) ) ;
				}
				
				callback.onSuccess( images );
			}
		};
			
		nativeSearch( search, page, pageSize, _apikey, jscallback ) ;
		
	}
	
	



	private native void nativeSearch( String search, int page, int pageSize,  String apikey, 
			Callback< JsArray < JavaScriptObject >, String  > jscallback ) /*-{		
		
		var url = "https://api.flickr.com/services/rest/?method=flickr.photos.search" + 
			"&api_key=" + apikey + 
			"&text=" + search + 
			"&sort=relevance" +  
			"&group_id=79664037@N00" + 
			"&page=" + ( page + 1 ) + 
			"&per_page=" + pageSize ;
		
	
		$wnd.jQuery.getJSON( url + "&format=json&jsoncallback=?", function( data ){
			
			var images = [] ;
    		
    		$wnd.jQuery.each( data.photos.photo, function( i, item ){
    			
    			console.log ( JSON.stringify( item ) ) ;
    			
	    		images.push( item ) ;

        		if ( i == pageSize - 1 ) {
        			return false;
        		}

    			
    		});
    		
    		jscallback.@com.google.gwt.core.client.Callback::onSuccess(Ljava/lang/Object;)(images);
    		
		});
			
		
	}-*/;


	
	private native void nativeGetDetails( JavaScriptObject stockImage,  String apikey, 
			Callback< JavaScriptObject, String  > jscallback ) /*-{		
		
		var url = "https://api.flickr.com/services/rest/?method=flickr.photos.getSizes" + 
			"&api_key=" + apikey + 
			"&photo_id=" + stockImage.id ; 
	
	
		var result = {} ;
		
		result.uid = stockImage.id ;
		
		$wnd.jQuery.getJSON( url + "&format=json&jsoncallback=?", function( data ){
			

			for (var i = 0 ; i < data.sizes.size.length ; i++) {
			 	
			 	var size = data.sizes.size[ i ] ;
			 	
			 	if ( size.label === "Large" ) {
					result.url = size.source;
				}

				if ( size.label === "Medium 800" ) {
					result.url = size.source;
				}
				
				if ( size.label === "Medium 640" ) {
					result.url =  size.source;
				}
				
				if ( size.label === "Medium" ) {
					result.url =  size.source;
				}
				
			}
			
			
    		console.log ( JSON.stringify( result ) ) ;
    		jscallback.@com.google.gwt.core.client.Callback::onSuccess(Ljava/lang/Object;)(result);
    		
		});
			
		
	}-*/;


	@Override
	public void getImageDetails ( StockImage stockImage,
			final Callback< StockImageDetails, String> callback ) {
		
		
		Callback< JavaScriptObject , String  > jscallback = new Callback< JavaScriptObject, String  >(){

			
			@Override
			public void onFailure(String reason) {
				callback.onFailure(reason);
			}

			
			@Override
			public void onSuccess( JavaScriptObject jsresult ) {
				callback.onSuccess( FlickrImageDetails.create( jsresult ) );
			}
		};
			
		
		nativeGetDetails( ( ( FlickrImage ) stockImage).getNativeImage(), _apikey, jscallback ) ;
		
	}




	
	
	
	
	
		
	

	
	
}
