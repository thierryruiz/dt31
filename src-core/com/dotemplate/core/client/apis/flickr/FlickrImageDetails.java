// -------------------------------------------------------------------------
// Copyright (c) 2006-2013 GEMALTO group. All Rights Reserved.
//
// This software is the confidential and proprietary information of GEMALTO.
//
// Project name: SensorLogic Admin Portal
//
// Platform : Java virtual machine
// Language : JAVA 6.0
//
// Author : Thierry Ruiz
//
// -------------------------------------------------------------------------
// GEMALTO MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
// THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
// TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
// PARTICULAR PURPOSE, OR NON-INFRINGEMENT. GEMALTO SHALL NOT BE
// LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING,
// MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
//
// THIS SOFTWARE IS NOT DESIGNED OR INTENDED FOR USE OR RESALE AS ON-LINE
// CONTROL EQUIPMENT IN HAZARDOUS ENVIRONMENTS REQUIRING FAIL-SAFE
// PERFORMANCE, SUCH AS IN THE OPERATION OF NUCLEAR FACILITIES, AIRCRAFT
// NAVIGATION OR COMMUNICATION SYSTEMS, AIR TRAFFIC CONTROL, DIRECT LIFE
// SUPPORT MACHINES, OR WEAPONS SYSTEMS, IN WHICH THE FAILURE OF THE
// SOFTWARE COULD LEAD DIRECTLY TO DEATH, PERSONAL INJURY, OR SEVERE
// PHYSICAL OR ENVIRONMENTAL DAMAGE ("HIGH RISK ACTIVITIES"). GEMALTO
// SPECIFICALLY DISCLAIMS ANY EXPRESS OR IMPLIED WARRANTY OF FITNESS FOR
// HIGH RISK ACTIVITIES.
// -------------------------------------------------------------------------

package com.dotemplate.core.client.apis.flickr ;


import com.dotemplate.core.client.apis.StockImageDetails;
import com.google.gwt.core.client.JavaScriptObject;

public class FlickrImageDetails extends JavaScriptObject implements StockImageDetails {
	
	protected FlickrImageDetails(){} ;
	
	
	public static final native StockImageDetails create( JavaScriptObject jsImage ) /*-{
		return jsImage ;
	}-*/;
	
	
	@Override
	public final native String getUid() /*-{
		return this.id ;
	}-*/;
	
	@Override
	public final native String getUrl() /*-{
		return this.url ;
	}-*/;


	@Override
	public final native String getCredits() /*-{
		return "TODO" ;
	}-*/;
	
		
	
}
