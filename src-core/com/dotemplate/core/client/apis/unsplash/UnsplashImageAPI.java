package com.dotemplate.core.client.apis.unsplash;

import java.util.ArrayList;

import com.dotemplate.core.client.apis.StockImage;
import com.dotemplate.core.client.apis.StockImageAPI;
import com.dotemplate.core.client.apis.StockImageDetails;
import com.google.gwt.core.client.Callback;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;

public class UnsplashImageAPI implements StockImageAPI {

	final static String _apikey = "17a13d0f214d8cf2d64f7dea66cff823ad793e3c1e4a0f69278dd855e8dcdd47" ;

	// usay...h
	
	
	@Override
	public void search(String search, int page, int pageSize,
			final Callback< ArrayList < StockImage >, String> callback) {
		
		Callback< JsArray < JavaScriptObject >, String  > jscallback = new Callback< JsArray < JavaScriptObject >, String  >(){

			@Override
			public void onFailure(String reason) {
				callback.onFailure(reason);
			}

			@Override
			public void onSuccess( JsArray<JavaScriptObject> jsresult) {
				
				ArrayList< StockImage > images = new ArrayList< StockImage >() ;
				
				for ( int i = 0 ; i < jsresult.length() ; i++ ){
					images.add( new UnsplashImage( jsresult.get( i ) ) ) ;
				}
				
				callback.onSuccess( images );
			}
		};
			
		nativeSearch( search, page, pageSize, _apikey, jscallback ) ;
		
	}

	private native void nativeSearch( String search, int page, int pageSize,  String apikey, 
			Callback< JsArray < JavaScriptObject >, String  > jscallback ) /*-{		
		
		var url = "https://api.unsplash.com/photos/search?page=1&query=office&client_id=17a13d0f214d8cf2d64f7dea66cff823ad793e3c1e4a0f69278dd855e8dcdd47" + 
			"&client_id=" + apikey + 
			"&query=" + search + 
			"&page=" + ( page + 1 ) + 
			"&per_page=" + pageSize ;
		
	
		$wnd.jQuery.getJSON( url , function( result ){
			
			var images = [] ;
    		
    		$wnd.jQuery.each( result, function( i, image ){
    			
    			console.log ( JSON.stringify( image ) ) ;
    			
	    		images.push( image ) ;

        		if ( i == pageSize - 1 ) {
        			return false;
        		}

    			
    		});
    		
    		jscallback.@com.google.gwt.core.client.Callback::onSuccess(Ljava/lang/Object;)(images);
    		
		});
			
		
	}-*/;
	
	
	
	@Override
	public void getImageDetails(StockImage image,
			Callback<StockImageDetails, String> callback) {
		
		callback.onSuccess( new UnsplashImageDetails( ( UnsplashImage ) image ));

	}

}
