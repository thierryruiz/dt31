package com.dotemplate.core.client.apis;


public interface StockImageDetails {
	
	String getUid() ;
	
	String getUrl() ;
		
	String getCredits() ;
	
}
