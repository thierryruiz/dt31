package com.dotemplate.core.client.apis;

import java.util.ArrayList;

import com.google.gwt.core.client.Callback;


public interface StockImageAPI {
	
	public void search( String search, int page, int pageSize, Callback<  ArrayList< StockImage >, String > callback ) ;
	
	public void getImageDetails( StockImage image, Callback< StockImageDetails , String > callback ) ;
	
	
}
