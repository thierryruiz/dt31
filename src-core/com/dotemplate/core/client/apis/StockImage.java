package com.dotemplate.core.client.apis;


public interface StockImage {
	
	String getUid() ;
	
	String getThumbnailUrl() ;
	
}
