package com.dotemplate.core.client.widgets;

import com.dotemplate.core.client.Client;
import com.dotemplate.core.client.frwk.Utils;
import com.dotemplate.core.client.widgets.colorpicker.ColorPickerDialog;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasChangeHandlers;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.CellPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

@Deprecated
public class ColorButton implements IsWidget, HasChangeHandlers, ColorPickerDialog.Handler {
	
	public enum Layout { VERTICAL, HORIZONTAL };
	
	protected CellPanel wrapper ;
	
	protected Image colorControl ;
	
	protected String color ;
	
	protected HandlerManager handlerManager ;

	
	public ColorButton( String color, String lbl, Layout layout ) {
		
		this.color = color ;
		
		this.handlerManager = new HandlerManager( this ) ;
		
		if ( Layout.VERTICAL == layout ){
			wrapper = new VerticalPanel () ;
		} else {
			wrapper = new HorizontalPanel() ; 
		}
	
		Label label = new Label( lbl ) ;
		
		wrapper.add( label ) ;
		
		Image colorControl = Utils.spacer( 20 , 20 ) ;
		DOM.setStyleAttribute( colorControl.getElement() , "backgroundColor", color ) ;
		DOM.setStyleAttribute( colorControl.getElement() , "border", "1px solid #333" ) ;
		
		wrapper.add( colorControl ) ;
		
		bind() ;
		
	}
	
	
	protected  void bind() {
		colorControl.addClickHandler( new ClickHandler( ) {
			@Override
			public void onClick( ClickEvent e ) {
				
				//ColorPickerDialog colorPicker = Client.get().getColorPickerDialog() ;
				
				//colorPicker.setSelectedColor( color ) ;
				
				//colorPicker.open( ColorButton.this ) ;			
			}
		}) ;
	}


	@Override
	public Widget asWidget() {
		return wrapper ;
	}

	
	public String getColor() {
		return color;
	}


	@Override
	public void fireEvent( GwtEvent<?> event ) {
		handlerManager.fireEvent( event ) ;
	}


	@Override
	public HandlerRegistration addChangeHandler( ChangeHandler handler  ) {
		return handlerManager.addHandler( ChangeEvent.getType(), handler);
	}


	@Override
	public void onColorSelected( String color ) {
		this.color = color ;
		fireEvent( new ChangeEvent() {
			@Override
			public Object getSource() {
				return ColorButton.this ;
			}
		}) ;
	}

	
}
