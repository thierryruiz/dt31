package com.dotemplate.core.client.widgets;

import org.gwtbootstrap3.client.ui.Button;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DeckPanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;

public class SideMenu extends Composite {

	private static SideMenuUiBinder uiBinder = GWT
			.create(SideMenuUiBinder.class);

	interface SideMenuUiBinder extends UiBinder<Widget, SideMenu> {
	}
	
	@UiField
	protected FlowPanel top ;

	@UiField
	protected DeckPanel controlsContainer ;

	
	@UiField
	Button toggler ;

	private boolean opened = true ;
	
	public SideMenu() {
		initWidget(uiBinder.createAndBindUi(this));
		toggler.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				toggle();
			}
		});
	}
	
		
	public void toggle(){
		nativeToggle( this.getElement(), opened, 270 ) ;
		opened = !opened ;
	}
	
	
	
    private native void nativeToggle( final Element e, boolean isOpened, int width ) /*-{
		if ( isOpened ) {
			$wnd.jQuery(e).animate({
					right:-width
			},{
				duration:100
			}	);
    	} else {
			$wnd.jQuery(e).animate({
					right:0
			},{
				duration:100
			});
		}	
	}-*/;

}
