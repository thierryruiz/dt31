package com.dotemplate.core.client.widgets;

import com.google.gwt.core.client.JavaScriptObject;

public class UploadFile extends JavaScriptObject {

	protected UploadFile() {

	}

	public final native int getSize() /*-{

		return this.fileSize != null ? this.fileSize : this.size;

	}-*/;

	public final native String getName() /*-{

		return this.fileName != null ? this.fileName : this.name;

	}-*/;

	public final native String getType() /*-{

		return this.type;

	}-*/;

}
