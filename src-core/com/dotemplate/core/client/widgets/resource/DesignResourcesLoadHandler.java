package com.dotemplate.core.client.widgets.resource;


import java.util.ArrayList;
import java.util.LinkedHashMap;

import com.dotemplate.core.shared.DesignResource;


public interface DesignResourcesLoadHandler< RESOURCE extends DesignResource > {

	void onDesignResourcesLoad( ArrayList< RESOURCE > items ) ;
	
	void onDesignResourcesLoad( ArrayList< RESOURCE > items, boolean hasMore ) ;
			
	void onDesignResourcesLoad( LinkedHashMap< String, LinkedHashMap< String , RESOURCE > > tags ) ;
	
	void onDesignResourcesLoad( String[] tags ) ;

}
