package com.dotemplate.core.client.widgets.resource;

import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.Set;

import com.dotemplate.core.shared.DesignResource;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.Widget;


public abstract class DesignResourceScrollSelector< RESOURCE extends DesignResource > 
	extends DesignResourceSelector< RESOURCE > {
	
	protected ScrollPanel wrapper ;
	
	public DesignResourceScrollSelector( ) {
				
		wrapper = new ScrollPanel() ;	

		wrapper.add( list.asWidget() ) ;
				
	}
	
	
	public void setResources( LinkedHashMap< String, LinkedHashMap< String, RESOURCE > > tags ) {
		
		list.clearList () ;
		
		DesignResourceThumbnail< RESOURCE > el ;
		
		if ( tags.size() == 1 ){ // only one tag > all
			
			for ( RESOURCE resource : tags.get( "all" ).values() ) {
				
				if ( ! isResourceAvailable( resource ) ) {
					continue ;
				}
				
				el = list.add ( resource );
				
				el.addClickHandler( this )  ;
				
				el.asWidget().getElement().setId( resource.getName() );
				
			}
			
		} else { // multiple tags > list organized by tags
			
			Set< Entry < String, LinkedHashMap< String, RESOURCE > > > entries = tags.entrySet() ; 
			
			for ( Entry < String, LinkedHashMap< String, RESOURCE > > entry : entries ){
			
				// do not display 'all' tag resources
				if ( "all".equals( entry.getKey() )  ) continue ;
				
				list.addTagSeparator( entry.getKey() ) ;
				
				for ( RESOURCE resource : entry.getValue().values() ) {
					
					if ( !isResourceAvailable( resource ) ) continue ;
					
					el = list.add ( resource  ) ;
					
					el.addClickHandler( this )  ;
					
					el.asWidget().getElement().setId( resource.getName() );

				}
				
			}
			
		}
	
	}
	
	
	@Override
	public Widget asWidget() {
		return wrapper ;
	}
	
	
}
