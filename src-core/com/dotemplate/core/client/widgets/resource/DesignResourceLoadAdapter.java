package com.dotemplate.core.client.widgets.resource;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import com.dotemplate.core.shared.DesignResource;

public class DesignResourceLoadAdapter< RESOURCE extends DesignResource > implements
		DesignResourcesLoadHandler<RESOURCE> {

	@Override
	public void onDesignResourcesLoad(ArrayList<RESOURCE> items) {
	}

	@Override
	public void onDesignResourcesLoad(ArrayList<RESOURCE> items, boolean hasMore) {
	}

	@Override
	public void onDesignResourcesLoad(
			LinkedHashMap<String, LinkedHashMap<String, RESOURCE>> tags) {		
	}

	@Override
	public void onDesignResourcesLoad(String[] tags) {
	}

}
