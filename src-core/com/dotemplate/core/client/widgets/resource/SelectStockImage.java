package com.dotemplate.core.client.widgets.resource;

import com.dotemplate.core.client.apis.StockImage;
import com.dotemplate.core.client.apis.StockImageDetails;
import com.dotemplate.core.client.canvas.event.SelectStockImageHandler;
import com.dotemplate.core.client.frwk.Event;


public class SelectStockImage extends Event< SelectStockImageHandler > {
	
	public static final Type < SelectStockImageHandler> TYPE = new Type< SelectStockImageHandler >();
	
	protected StockImage image ;
	
	protected StockImageDetails imageDetails ;

	
	public SelectStockImage( StockImage image , StockImageDetails imageDetails ) {
		this.image = image ;
		this.imageDetails =imageDetails ;
	}
	
	
	public StockImage getImage() {
		return image;
	}
	
	public StockImageDetails getImageDetails() {
		return imageDetails;
	}
	
	
	@Override
	protected void dispatch( SelectStockImageHandler handler ) {
		handler.onStockImageSelected(image, imageDetails);
	}

	
	@Override
	public Type< SelectStockImageHandler > getAssociatedType() {
		return TYPE ;
	}
	
}
