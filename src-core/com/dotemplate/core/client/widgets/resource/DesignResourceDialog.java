package com.dotemplate.core.client.widgets.resource;


import com.dotemplate.core.client.widgets.Window;
import com.dotemplate.core.shared.DesignResource;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.shared.HandlerRegistration;


public abstract class DesignResourceDialog< RESOURCE extends DesignResource > extends Window {
	
	protected DesignResourceSelector< RESOURCE > resourceSelector ;
	
	protected DesignResourceProvider< RESOURCE > resourceProvider ;
	
	protected HandlerRegistration callbackRegistration ;
	
	protected DesignResourceChangeHandler < RESOURCE > callback ;
	
	public DesignResourceDialog ( DesignResourceProvider< RESOURCE > resourceProvider, int width, int height ) {
		
		setSize ( width, height ) ;
		
		body.getElement().getStyle().setPadding( 0, Unit.PX );
		
		footer.getElement().getStyle().setMarginTop( 0, Unit.PX );
		
		this.resourceProvider = resourceProvider ;
		
		resourceSelector = createResourceSelector() ;
		
		resourceSelector.addValueChangeHandler( new DesignResourceChangeHandler<RESOURCE>() {

			@Override
			public void onValueChange(ValueChangeEvent<RESOURCE> event) {
				
				callback.onValueChange( event );
				
				close() ;
			}
		} ) ;

		add( resourceSelector.asWidget() ) ;
		
	}
	
	
	protected abstract DesignResourceSelector < RESOURCE > createResourceSelector() ;
		
	public abstract void open( RESOURCE selected, DesignResourceChangeHandler< RESOURCE > callback ) ;


}
