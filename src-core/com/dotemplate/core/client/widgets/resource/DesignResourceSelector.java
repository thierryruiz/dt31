package com.dotemplate.core.client.widgets.resource;


import com.dotemplate.core.client.Client;
import com.dotemplate.core.shared.DesignResource;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.IsWidget;


public abstract class DesignResourceSelector< RESOURCE extends DesignResource > implements
	 HasValue< RESOURCE >, IsWidget, ClickHandler {
	
	protected HandlerManager handlerManager ;
	
	protected DesignResourceList< RESOURCE > list  ;
	
	protected RESOURCE selected ;
	
	//protected DesignResourceProvider< RESOURCE > provider ;
	
	
	public DesignResourceSelector( ) {
		
		//this.provider = provider ;
		
		this.handlerManager = new HandlerManager( this ) ;
		
		list = createResourceList() ;
		
	}
	
	
	protected DesignResourceList< RESOURCE > createResourceList() {
		return new DesignResourceList< RESOURCE >() ;
	}
	
	
	@Override
	public void setValue( RESOURCE r ) {
		this.selected = r;
		select() ;
	}
	
	@Override
	public void setValue( RESOURCE r, boolean fireEvent ) {
		
		setValue( r ) ;
		
		if ( fireEvent ){
			ValueChangeEvent.fire(this, r);
		}
	}
	
	@Override
	public RESOURCE getValue() {
		return selected;
	}
	
	
	protected void select() {
		if ( selected != null ){
			list.select( selected ) ; 
		}
	}


	@Override
	public void onClick( ClickEvent e ) {
		
		@SuppressWarnings("unchecked")
		DesignResourceThumbnail< RESOURCE > el = ( DesignResourceThumbnail< RESOURCE > ) e.getSource() ;
		
		if ( el.isLocked() ){
			
			Client.get().invitePurchase() ;
			
			
		} else {
			
			ValueChangeEvent.fire(this, el.getResource());
		
		}
		
	}
	
	
	@Override
	public void fireEvent(GwtEvent<?> event ) {
		handlerManager.fireEvent( event ) ;
	}

	
	protected boolean isResourceAvailable( RESOURCE resource ){
		return true ;
	}
	
	
	public boolean isLoaded(){
		return !list.isEmpty() ;
	}
	
	
	public void scrollToEnd(){
		list.scrollToEnd();
	}
	

}
