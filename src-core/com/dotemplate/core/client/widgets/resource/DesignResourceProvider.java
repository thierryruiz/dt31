package com.dotemplate.core.client.widgets.resource;

import java.util.ArrayList;

import com.dotemplate.core.client.Client;
import com.dotemplate.core.client.frwk.CommandCallback;
import com.dotemplate.core.client.frwk.RPC;
import com.dotemplate.core.shared.DesignResource;
import com.dotemplate.core.shared.command.GetDesignResources;
import com.dotemplate.core.shared.command.GetDesignResourcesMap;
import com.dotemplate.core.shared.command.GetDesignResourcesMapResponse;
import com.dotemplate.core.shared.command.GetDesignResourcesResponse;
import com.dotemplate.core.shared.command.GetDesignResourcesTags;
import com.dotemplate.core.shared.command.GetDesignResourcesTagsResponse;


public abstract class DesignResourceProvider< R extends DesignResource > {
	
	protected String tag ;
	
	protected ArrayList< DesignResourcesLoadHandler< R > > loadHandlers =
			new ArrayList< DesignResourcesLoadHandler< R > >() ;
	
	
	public String getTag() {
		return tag;
	}
	
	public void setTag(String tag) {
		this.tag = tag;
	}
	
	
	public void addLoadHandler( DesignResourcesLoadHandler<R> loadHandler ) {
		this.loadHandlers.add( loadHandler ) ;
	}
	
	
	public void loadResources () {
		
		GetDesignResources< R > action = getDesignResourcesCommand() ;
		
		action.setTag( tag ) ;
		
		action.setDesignUid( Client.get().getDesign().getUid() ) ;
			
		( new RPC< GetDesignResourcesResponse< R > >( Client.get().getDesignService() ) ).execute ( 
				action,
				new CommandCallback< GetDesignResourcesResponse< R > >(){
			
					
			public void onSuccess ( GetDesignResourcesResponse< R > response ) {
				for ( DesignResourcesLoadHandler<R> loadHandler : loadHandlers ){
					loadHandler.onDesignResourcesLoad( response.getResources() ) ;
				}
			}
		
		}) ;
		
	}
		
	
	public void loadResourcesMap () {
		
		GetDesignResourcesMap< R > action = getDesignResourcesMapCommand() ;
		
		action.setDesignUid( Client.get().getDesign().getUid() ) ;
			
		( new RPC< GetDesignResourcesMapResponse< R > >( Client.get().getDesignService() ) ).execute ( 
				action,
				new CommandCallback< GetDesignResourcesMapResponse< R > >(){
			
			public void onSuccess ( GetDesignResourcesMapResponse< R > response ) {

				for ( DesignResourcesLoadHandler<R> loadHandler : loadHandlers ){
					loadHandler.onDesignResourcesLoad( response.getResourcesMap() ) ;
				}
				
			}
		
		}) ;
		
	}
	
	
	public void loadResourcesTags () {
		
		GetDesignResourcesTags< R > action = getDesignResourcesTagsCommand() ;
		
		action.setDesignUid( Client.get().getDesign().getUid() ) ;
		
	
		( new RPC< GetDesignResourcesTagsResponse< R > >( Client.get().getDesignService() ) ).execute ( 
				action,
				new CommandCallback< GetDesignResourcesTagsResponse< R > >(){
			
			public void onSuccess ( GetDesignResourcesTagsResponse< R > response ) {
				
				for ( DesignResourcesLoadHandler<R> loadHandler : loadHandlers ){
					loadHandler.onDesignResourcesLoad( response.getResourcesTags() ) ;
				}
				
			}		
		}) ;
		
	}
	
	
	protected GetDesignResources< R > getDesignResourcesCommand() {
		throw new RuntimeException( "Operation not supported" ) ;
	}
	
	protected GetDesignResourcesMap< R > getDesignResourcesMapCommand() {
		throw new RuntimeException( "Operation not supported" ) ;
	}
	
	protected GetDesignResourcesTags< R > getDesignResourcesTagsCommand() {
		return null ;
	}
	
	
}
