package com.dotemplate.core.client.widgets.resource;

import java.util.ArrayList;

import org.gwtbootstrap3.client.ui.Button;
import org.gwtbootstrap3.client.ui.constants.ButtonType;

import com.dotemplate.core.shared.DesignResource;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.DockPanel;
import com.google.gwt.user.client.ui.Widget;


public abstract class DesignResourcePageSelector< RESOURCE extends DesignResource > 
	extends DesignResourceSelector< RESOURCE > {
	
	protected DockPanel layout ;
	
	protected Button loadMoreButton ;
	
	protected DesignResourcePageAsyncProvider< RESOURCE > provider ;
	
	
	public DesignResourcePageSelector( DesignResourcePageAsyncProvider< RESOURCE > provider ) {
		
		super( ) ;
		
		this.provider = provider ; 
		
		layout = new DockPanel() ;
	
		loadMoreButton = new Button( loadMoreButtonLabel() , new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				loadMore() ;
			}
		} ) ;
		
		
		loadMoreButton.setType( ButtonType.PRIMARY );
		
		layout.add( list.asWidget(), DockPanel.CENTER ) ;	
		
		layout.add( loadMoreButton, DockPanel.SOUTH ) ;	
		
		layout.setCellHorizontalAlignment( loadMoreButton, DockPanel.ALIGN_CENTER ) ;
				
		layout.setSpacing( 0 ) ;
		
		layout.setCellWidth( list.asWidget(), "80%" ) ;

		
		provider.addLoadHandler(  new DesignResourceLoadAdapter<RESOURCE>(){
			@Override
			public void onDesignResourcesLoad( ArrayList< RESOURCE > resources, boolean hasMore) {
				
				setResources( resources, hasMore ) ;
				
				if ( selected != null ){
					select() ;
				} else {
					scrollToEnd();
				}
			}
		});
		
		
		


	}	
	

	protected void loadMore() {
		( ( DesignResourcePageAsyncProvider< RESOURCE > ) provider ).next() ;	
	}


	protected void setResources( ArrayList< RESOURCE > resources, boolean hasMore ) {
		
		DesignResourceThumbnail< RESOURCE > el ;
		
		for ( RESOURCE resource : resources  ) {
			
			el = list.add ( resource  );
			
			el.addClickHandler( this )  ;
			
			el.asWidget().getElement().setAttribute("id", resource.getName() ) ;
		
		}

		
		if( hasMore ){
			enableNext() ;
		} else {
			disableNext() ;
		}
	
		
	}
	
	

	@Override
	public Widget asWidget() {
		return layout ;
	}

	
	protected void enableNext () {
		loadMoreButton.setVisible(true);
	}
	
	protected void disableNext () {
		loadMoreButton.setVisible ( false ) ;
	}
	
	protected String loadMoreButtonLabel() {
		return "Load More" ;
	}
	

	
}
