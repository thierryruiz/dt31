package com.dotemplate.core.client.widgets.resource;

import com.dotemplate.core.client.Client;
import com.dotemplate.core.client.frwk.CommandCallback;
import com.dotemplate.core.client.frwk.RPC;
import com.dotemplate.core.shared.DesignResource;
import com.dotemplate.core.shared.command.GetDesignResources;
import com.dotemplate.core.shared.command.GetDesignResourcesResponse;


public abstract class DesignResourcePageAsyncProvider< RESOURCE extends DesignResource > 
	extends DesignResourceProvider< RESOURCE > {
	
	private int paging = 10 ;
	
	protected int index ;
	
	protected boolean hasMore = false ;
	
	public void setPaging( int paging ) {
		this.paging = paging;
	}
	
	
	public int getPaging() {
		return paging;
	}
	
	
	public int getIndex() {
		return index;
	}
	
	
	public void reset(){
		index = 0 ;
	}
	
	
	
	
	
	public void next() {
		if( !hasMore ) return ;
		index = index + paging ;
		loadResources() ;
	}
	
	
	public void previous() {
		if ( index == 0 ) return ;
		index = index - paging ;
		loadResources() ;
	}
	
	
	@Override
	public void loadResources () {
		
		GetDesignResources< RESOURCE > action = getDesignResourcesCommand() ;
		
		action.setDesignUid( Client.get().getDesign().getUid() ) ;
		
		action.setIndex ( getIndex() ) ;
		
		action.setPagingLength ( getPaging() ) ;
		
		( new RPC< GetDesignResourcesResponse< RESOURCE > >( Client.get().getDesignService() ) ).execute ( 
				action,
				new CommandCallback< GetDesignResourcesResponse< RESOURCE > >(){
			
			public void onSuccess ( GetDesignResourcesResponse< RESOURCE > response ) {
				
				for ( DesignResourcesLoadHandler<RESOURCE> loadHandler : loadHandlers ){
					
					loadHandler.onDesignResourcesLoad( response.getResources (), response.hasMoreResults () ) ;
				}
				
				hasMore = response.hasMoreResults () ;	
			}
			
		}) ;
		
	}
	
	
}
