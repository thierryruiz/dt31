package com.dotemplate.core.client.widgets.resource;

import java.util.HashMap;

import com.dotemplate.core.shared.DesignResource;
import com.google.gwt.dom.client.Element;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;


public class DesignResourceList< RESOURCE extends DesignResource > implements IsWidget {
	
	protected FlowPanel root;

	protected HashMap< String, DesignResourceThumbnail< RESOURCE > > thumbnails ;

	public DesignResourceList () {
		root = new FlowPanel ();
		root.setStyleName( "ez-chooser" ) ;
		thumbnails = new HashMap< String, DesignResourceThumbnail<  RESOURCE > >() ;
	}
	

	protected void addTagSeparator( String tag ){
		HTML hdr = new HTML ( "<h2>" + tag + "</h2>" ) ;
		hdr.addStyleName( "pannel-heading" );
		root.add (  hdr ) ;
	}

	
	protected DesignResourceThumbnail< RESOURCE > add(  RESOURCE resource ){
		
		
		DesignResourceThumbnail< RESOURCE > el = new DesignResourceThumbnail< RESOURCE > ( resource ) ;
		root.add ( el ) ;
		thumbnails.put( resource.getName() , el ) ;
		return el ;
	}

	
	protected boolean isEmpty() {
		return root.getWidgetCount () == 0 ;
	}
	
	public void clearList () {
		root.clear ();
	}
	
	
	@Override
	public Widget asWidget() {
		return root;
	}
	

	public void select( RESOURCE selected  ){
		
		if ( selected == null  ) {
			return ;
		}
		
		for ( DesignResourceThumbnail< RESOURCE > th : thumbnails.values() ){
			th.setSelected(false) ;
		}
		
		
		DesignResourceThumbnail< RESOURCE > thumbnail = getThumbnail( selected ) ;
		
		if ( thumbnail != null ){
			thumbnail.setSelected( true ) ;
			
			// unlock a premium design resource if used in free design 
			thumbnail.setLocked( false ) ;
		}
		
	}
	
	// overrides by symbol selector
	protected DesignResourceThumbnail< RESOURCE > getThumbnail( RESOURCE resource ){
		return  thumbnails.get( resource.getName() ) ;
	}

	
	public void scrollToEnd(){
		nativeScrollToEnd( root.asWidget().getElement() ) ;
	}
	
	
	
	private static final native void nativeScrollToEnd( Element e ) /*-{
		 $wnd.jQuery(e).stop().animate({
		  	scrollTop: $wnd.jQuery(e)[0].scrollHeight
		}, 800);
	}-*/;


	
	
}
