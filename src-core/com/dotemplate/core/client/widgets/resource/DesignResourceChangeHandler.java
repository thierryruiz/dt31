package com.dotemplate.core.client.widgets.resource;


import com.dotemplate.core.shared.DesignResource;
import com.google.gwt.event.logical.shared.ValueChangeHandler;

public interface DesignResourceChangeHandler< R extends DesignResource > extends ValueChangeHandler< R > {

}
