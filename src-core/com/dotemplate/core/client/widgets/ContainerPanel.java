package com.dotemplate.core.client.widgets;

import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;

public class ContainerPanel implements IsWidget {

	private ContentPanel innerPanel ;
	
	public ContainerPanel() {
		innerPanel = new ContentPanel() ;
	}
	
	
	public ContainerPanel( String heading ) {
		this() ;
		innerPanel.setHeaderVisible( true ) ;
		setHeading( heading ) ;
	}
		
	
	public void setHeading( String heading ) {
		innerPanel.setHeading( heading  ) ;
	}
	
	public void clear() {
		innerPanel.removeAll() ;
	}
	
	
	
	@Override
	public Widget asWidget() {
		return innerPanel ;
	}

	
	
	public void setWidget( Widget widget ) {
		innerPanel.add( widget ) ;
	}
	
	
	
	
}
