package com.dotemplate.core.client.widgets;

import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.SimplePanel;

@Deprecated
public class Button extends SimplePanel implements HasClickHandlers {
	
	private com.extjs.gxt.ui.client.widget.button.Button innerBtn ;
	
	
	public Button ( String text, final ClickHandler handler ) {
		
		innerBtn = new com.extjs.gxt.ui.client.widget.button.Button( text ) ;
		
		innerBtn.addSelectionListener( new SelectionListener<ButtonEvent>() {  
			public void componentSelected( ButtonEvent ev ) {			
				fireEvent( new ClickEvent(){
		    		  public Object getSource() {
			    			return Button.this;
			    		}  
				}) ;
		      }
		});
		
		setWidget ( innerBtn ) ;

		if ( handler != null ){
			addClickHandler( handler ) ;
		}
	}

	
	public Button ( String text ) {
		this( text, null ) ;
	}
	
	
	public Object getInnerButton(){
		return innerBtn ;
	}
	
	
	public void setWidth ( int width ){
		super.setWidth ( width + "px" ) ;
		innerBtn.setWidth ( width ) ;
	}

	
	public void setHeight ( int height ){
		super.setHeight ( height + "px" ) ;
		innerBtn.setHeight ( height )  ;
	}
	

	@Override
	public HandlerRegistration addClickHandler ( ClickHandler handler ) {
		return addHandler ( handler, ClickEvent.getType () );
	}
	
	
	public void setText ( String text ){
		innerBtn.setText ( text ) ;
	}
	
}
