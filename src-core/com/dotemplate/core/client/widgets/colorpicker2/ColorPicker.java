package com.dotemplate.core.client.widgets.colorpicker2;

import org.gwtbootstrap3.client.ui.TextBox;

import com.dotemplate.core.client.frwk.Utils;
import com.dotemplate.core.client.resources.css.CSSBundles;
import com.dotemplate.core.client.resources.js.JSBundles;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.dom.client.FocusHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.Widget;


public class ColorPicker extends Composite implements HasValue<String> {

	
	static {
		Utils.useStylesheet( CSSBundles.INSTANCE.getColorPickerCss() );
		Utils.useScript( JSBundles.INSTANCE.getColorPickerJs() ) ;
	}
	
	private static ColorPickerUiBinder uiBinder = GWT
			.create(ColorPickerUiBinder.class);

	interface ColorPickerUiBinder extends UiBinder<Widget, ColorPicker> {
	}
	
	static ColorPickerPopup popup = ColorPickerPopup.get() ;

	
	@UiField
	TextBox textbox;

	String previousColor ;
	
	
	public ColorPicker() {

		initWidget(uiBinder.createAndBindUi(this));
		
		textbox.addFocusHandler(new FocusHandler() {
			
			@Override
			public void onFocus(FocusEvent event) {
				
				previousColor = textbox.getPlaceholder() ;
				
				popup.show( ColorPicker.this, getValue(),
						textbox.getAbsoluteLeft() - 250, 
						textbox.getAbsoluteTop()  );
			}
		});
		
	}

	
	TextBox getTextbox() {
		return textbox;
	}
	
	
	void setTextBoxColor( String color ){
		textbox.setPlaceholder( color );
		textbox.setValue( color );
//		textbox.getElement().getStyle().setBackgroundColor(color);
//		textbox.getElement().getStyle().setColor( invert( color ) );
	}
	
	
	void cancelColor(){
		setTextBoxColor(previousColor);
	}

		
	private static String invert( String hexcolor ) {
		
		return ( nativeInvert( hexcolor.substring( 1 )  ) ?  "#ffffff" : "#000000" ) ;
	}
	
	
    private static native boolean nativeInvert( String hexcolor ) /*-{

		var r = parseInt(hexcolor.substr(0,2),16)/255 ;
		var g = parseInt(hexcolor.substr(2,2),16)/255;
		var b = parseInt(hexcolor.substr(4,2),16)/255;
		
		return (r * 0.3 + g * .59 + b * .11) <= 0.6;
	
	}-*/;


	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<String> handler) {
		return addHandler( handler, ValueChangeEvent.getType() ) ;
	
	}

	@Override
	public String getValue() {
		return getTextbox().getValue() ;
	}


	@Override
	public void setValue(String color) {
		setTextBoxColor( color )  ;
	}


	@Override
	public void setValue( String color, boolean fireEvents) {
		
		setTextBoxColor( color )  ;
		
		if ( fireEvents ){
			ValueChangeEvent.fire(this, color );
		}
		
	}
    
    
}
