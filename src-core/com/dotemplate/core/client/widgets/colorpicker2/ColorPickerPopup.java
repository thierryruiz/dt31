package com.dotemplate.core.client.widgets.colorpicker2;

import org.gwtbootstrap3.client.ui.Anchor;
import org.gwtbootstrap3.client.ui.Button;
import org.gwtbootstrap3.client.ui.Column;
import org.gwtbootstrap3.client.ui.TextBox;

import com.dotemplate.core.client.Client;
import com.dotemplate.core.client.frwk.Utils;
import com.dotemplate.core.shared.Scheme;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.dom.client.Style;
import com.google.gwt.dom.client.Style.BorderStyle;
import com.google.gwt.dom.client.Style.Cursor;
import com.google.gwt.dom.client.Style.Display;
import com.google.gwt.dom.client.Style.Float;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;

public class ColorPickerPopup extends Composite implements ValueChangeHandler<Scheme> {

	private static ColorPickerPopupUiBinder uiBinder = GWT
			.create(ColorPickerPopupUiBinder.class);

	interface ColorPickerPopupUiBinder extends
			UiBinder<Widget, ColorPickerPopup> {
	}
	
	@UiField
	PopupPanel popup ;
	
	@UiField
	TextBox colorInput ;
	
	@UiField
	Column colorWheelContainer ;

	@UiField
	Button applyButton ;
	
	@UiField
	Button cancelButton ;	

	ColorPicker picker ;
	
	@UiField
	FlowPanel colorSchemeContainer;
	
	@UiField
	FlowPanel greyScaleContainer;
	
	@UiField
	Anchor changeSchemeAnchor ;
	
	@UiField
	FlowPanel userSchemeContainer;
	
	JavaScriptObject colorPicker ;
	
	static ColorPickerPopup INSTANCE = new ColorPickerPopup() ;
	
	Scheme scheme ;
	
	public static ColorPickerPopup get() {
		return INSTANCE;
	}
	
	private ColorPickerPopup() {
		
		initWidget( uiBinder.createAndBindUi( this ) );
		
		String id = DOM.createUniqueId() ;
		
		colorWheelContainer.getElement().setId( id ) ;
		
		RootPanel.get().add( popup ) ;
		
		colorPicker = createColorPicker ( id ) ;
		
		colorInput.setId( DOM.createUniqueId() );
		
		
		linkTo( colorPicker,  colorInput.getElement().getId() );
		
		cancelButton.addClickHandler( new ClickHandler() {			
			@Override
			public void onClick(ClickEvent event) {
				cancel() ;
			}
		} );
		
		applyButton.addClickHandler( new ClickHandler() {			
			@Override
			public void onClick(ClickEvent event) {
				apply() ;
			}
		} );
		
		
        addColor( "#000000", greyScaleContainer );
        addColor( "#101010", greyScaleContainer );
        addColor( "#202020", greyScaleContainer );
        addColor( "#303030", greyScaleContainer );
        addColor( "#404040", greyScaleContainer );
        addColor( "#505050", greyScaleContainer );
        addColor( "#606060", greyScaleContainer );
        addColor( "#707070", greyScaleContainer );
        addColor( "#808080", greyScaleContainer );
        addColor( "#909090", greyScaleContainer );
        addColor( "#A0A0A0", greyScaleContainer );
        addColor( "#B0B0B0", greyScaleContainer );
        addColor( "#C0C0C0", greyScaleContainer );
        addColor( "#D0D0D0", greyScaleContainer );
        addColor( "#E0E0E0", greyScaleContainer );
        addColor( "#EFEFEF", greyScaleContainer );
        addColor( "#F8F8F8", greyScaleContainer );        
        addColor( "#FFFFFF", greyScaleContainer );
        
        changeSchemeAnchor.addClickHandler(new ClickHandler() {
        	@Override
        	public void onClick(ClickEvent event) {
        		hide() ;
        	}
        });
        
        
	}


	protected void apply() {
		hide() ;
		picker.setValue( colorInput.getValue(), true );
		
	}
	
	
	protected void cancel() {
		hide() ;
		picker.cancelColor();
	}
	
	
	public HasClickHandlers getChangeSchemeLink() {
		return changeSchemeAnchor ;
	}

	
	public void hide() {
		popup.hide();
		popup.getElement().getStyle().setDisplay(Display.NONE); // popup fix	
	}
	

	public void show( ColorPicker picker, String color, int left, int top  ){
		
		this.picker = picker ;
		
		setInputColor( color ) ;
				
		popup.setVisible( true );
		
		popup.setPopupPosition( left, top );
				
		initUserScheme( Client.get().getDesign().getUserScheme() ) ;
		
		initColorScheme() ;
		
		popup.show();
				
		popup.getElement().getStyle().setDisplay(Display.BLOCK); // popup fix	
		
	}


	void setInputColor( String color ){
		colorInput.setPlaceholder( color );
		colorInput.setValue( color );
		colorInput.getElement().getStyle().setBackgroundColor(color);
		colorInput.getElement().getStyle().setColor( invert( color ) );
		ValueChangeEvent.fire(colorInput,color );
	}
	
	
	private static String invert( String hexcolor ) {
		
		return ( nativeInvert( hexcolor.substring( 1 )  ) ?  "#ffffff" : "#000000" ) ;
	}
	
    private static native boolean nativeInvert( String hexcolor ) /*-{

		var r = parseInt(hexcolor.substr(0,2),16)/255 ;
		var g = parseInt(hexcolor.substr(2,2),16)/255;
		var b = parseInt(hexcolor.substr(4,2),16)/255;
		
		return (r * 0.3 + g * .59 + b * .11) <= 0.6;
	
	}-*/;
	
    
    
    
	private void initColorScheme() {

		colorSchemeContainer.clear() ;
		
        String[] colors = scheme.getColors() ;
	    
        for ( int i = 0 ; i < colors.length ; i++ ) {
            addColor( colors [ i ], colorSchemeContainer ) ;
        }
	
	}

    
	
	private void initUserScheme ( String userScheme ) {
		
		if ( userScheme == null || userScheme.length() == 0  ) {
			return ; 
		}

		userSchemeContainer.clear() ;
		userSchemeContainer.setVisible( true ) ;
		userSchemeContainer.setVisible( true ) ;
		
	    String[] colors = userScheme.split( "," ) ;        
        
	    for ( int i = 0 ; i < colors.length ; i++ ) {
            addColor( colors [ i ], userSchemeContainer ) ;
        }
	    
	}

	
    private native JavaScriptObject createColorPicker( String containerId ) /*-{
    	return new $wnd.jQuery.farbtastic( '#' + containerId ) ;
	}-*/;

	
    private native void linkTo( JavaScriptObject colorPicker, String inputId ) /*-{
		colorPicker.linkTo( '#' + inputId );
	}-*/;

    private native void updateValue( JavaScriptObject colorPicker, String color ) /*-{
		colorPicker.updateValue( color )  ;
	}-*/;
    
    
	private void addColor( final String color, FlowPanel panel ){
		
        if ( color == null ) return ;
        
        final Image btn = Utils.spacer(15, 15) ;
        
        Style style = btn.getElement().getStyle() ;
        
        style.setBackgroundColor(color);
        style.setFloat(Float.LEFT);
        style.setMargin(1, Unit.PX);
        style.setCursor ( Cursor.POINTER ) ;
        style.setBorderColor("");
        style.setBorderStyle(BorderStyle.SOLID);
        style.setBorderWidth(1, Unit.PX);
        
        panel.add( btn ) ;
                
        btn.addClickHandler( new ClickHandler () {

            public void onClick( ClickEvent e ) {
            	setInputColor( color ) ;
            }
        
        }) ;
        
	}


	@Override
	public void onValueChange(ValueChangeEvent<Scheme> event) {
		scheme = event.getValue() ;
	}
	

}
