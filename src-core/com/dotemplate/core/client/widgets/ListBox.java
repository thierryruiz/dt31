package com.dotemplate.core.client.widgets;


import java.util.LinkedHashMap;

import org.gwtbootstrap3.extras.select.client.ui.Option;
import org.gwtbootstrap3.extras.select.client.ui.Select;

import com.dotemplate.core.client.frwk.Utils;
import com.google.gwt.dom.client.Element;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerRegistration;


// Support click handler to fire change event even when the same item is selected
public abstract class ListBox< T > extends Select implements HasClickHandlers {

	private static final char[] CHARS 
		= "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".toCharArray(); 
	
	private LinkedHashMap< String,  T > items = new LinkedHashMap< String, T >() ;
	
	
	public T getSelected(){
		return items.get( getSelectElement().getValue() ) ;
	}
	
	
	public void setSelected( T data ){
		setValue( uuid ( data ) ) ;
	}
	
	
	public void setSelected( final T data, boolean fireEvent ){
		
		setSelected ( data ) ;
		
		if ( fireEvent ){
						
			fireEvent( new ClickEvent(){
				@Override
				public Object getSource() {
					return data ;
				}
			});
		}
		
	}
	
		
	
	
	@Override
	protected void onLoad() {
		
		super.onLoad();
		
		listenItemClick ( getElement() ) ;		
	
	}
	
	
	@Override
	public void refresh() {
	
		super.refresh();

		listenItemClick ( getElement() ) ;		
	
	}
	
	
	
	
	public void add( T data ){
		
		ListBoxItem< T > item = new ListBoxItem< T >( dataAsString( data ), data ) ;
		
		item.setValue( uuid( data ) );
		
		items.put( item.getValue(), data ) ;
		
		add (  item  ) ;
		
	}
	

	protected abstract String dataAsString( T data ) ;
	
	protected abstract String uuid( T data ) ;
	
	
	class ListBoxItem< I > extends Option {
		
		private I data ;
		
		protected ListBoxItem() {
		}
		
		public ListBoxItem( String text,  I data ){
			this.data = data ;
			setText( text );
		}
		
		public I getData() {
			return data;
		}
				
	}
		

	private native void listenItemClick(Element e) /*-{
		var that = this ;
		$wnd.jQuery(e).next().find( "li" ).each( function( index ){
			$wnd.jQuery( this ).click( function(){
				that.@com.dotemplate.core.client.widgets.ListBox::onOptionClick(*)( index );
			}) ;
		});
	}-*/;


	public void onOptionClick( int index ) {
	
		String uuid = getSelectElement().getOptions().getItem( index ).getValue() ;

		final T item =  items.get( uuid ) ;
		
		Utils.log( dataAsString( item ) ) ;
		
		fireEvent( new ClickEvent(){
			@Override
			public Object getSource() {
				return item ;
			}
		});
	
	}
	
	
	
	@Override
	public HandlerRegistration addClickHandler(ClickHandler handler) {
		return this.addHandler(handler, ClickEvent.getType());
	}
	
	
	
}
