package com.dotemplate.core.client.widgets;


import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;



public class Tip extends Window {
	
	private HorizontalPanel root ;
	
	private Label tipMsg ;
		
	public Tip( String htmlMsg, String img ) {
		
		super() ;
		setSize( 460, 170 ) ;
		setHeading ( "Tip" ) ;

		root = new HorizontalPanel() ;
		root.setSpacing ( 10 ) ;
		
		if ( img != null ){
			root.add ( new Image( img /*"images/gwt/theme/customize.jpg"*/ ) ) ;
		}
		
		root.add( tipMsg = new HTML( htmlMsg ) ) ;
		
		tipMsg.setStyleName ( "ez-tip" ) ;
		
		root.setCellHorizontalAlignment ( tipMsg, VerticalPanel.ALIGN_CENTER ) ;
		root.setCellVerticalAlignment ( tipMsg, VerticalPanel.ALIGN_MIDDLE ) ;

		add (  root );
		
		show() ;
		
	}
	
	
	

	
	private void show () {
		openCenter () ;
		
		( new Timer() {
		 	public void run () {
		 		close () ;
		 	}	
		}).schedule ( 8000 ) ;
	}
	
}
