package com.dotemplate.core.client.widgets;

import org.gwtbootstrap3.client.ui.AnchorListItem;

public class DropDownMenuItem< T > extends AnchorListItem {
	
	T value ;
	
	
	@SuppressWarnings("unused")
	private DropDownMenuItem() {
		// TODO Auto-generated constructor stub
	}
	
	
	public DropDownMenuItem( String text, T value ) {
		
		super( text ) ;
		
		this.value = value ;
	
	}
	
	
	public T getValue() {
		return value;
	}
	

}
