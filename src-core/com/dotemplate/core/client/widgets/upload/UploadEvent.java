package com.dotemplate.core.client.widgets.upload;

import com.dotemplate.core.client.frwk.Event;




public class UploadEvent extends Event< UploadEventHandler > {
	
	public static final Type<UploadEventHandler> TYPE = new Type<UploadEventHandler>();
	
	private boolean success;

	private String fileName;

	public UploadEvent( boolean success ) {
		this.success = success;
	}

	public boolean isSuccess() {
		return success;
	}

	public boolean isFailed() {
		return !success;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName( String fileName ) {
		this.fileName = fileName;
	}

	@Override
	protected void dispatch( UploadEventHandler h ) {
		h.onUpload(UploadEvent.this);
	}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<UploadEventHandler> getAssociatedType() {
		return TYPE;
	}
	
	
}
