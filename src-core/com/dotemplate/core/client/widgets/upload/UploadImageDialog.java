package com.dotemplate.core.client.widgets.upload;


import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.FileUpload;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Hidden;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.FormPanel.SubmitCompleteEvent;
import com.dotemplate.core.client.frwk.Utils;
import com.dotemplate.core.client.widgets.Window;
import com.dotemplate.core.client.widgets.upload.UploadEventHandler;


public class UploadImageDialog extends Window implements HasUploadEventHandler {
	
	private static Logger logger = Logger.getLogger( UploadImageDialog.class.getName() );
	
	private HandlerManager handlerManager ;

	private VerticalPanel root;

	FileInput fileUpload;
	
	Hidden imageName ;
	
	private FormPanel form = new FormPanel ();

	private HTML uploadTip ;
	
	private String uploadId ;
	
	protected String uploadErrorMsg = "Ooops ! File upload error." ;
	
	
	public UploadImageDialog () {
		
		this.handlerManager = new HandlerManager( this ) ;
				
		setHeading ( "Upload image" );
		setSize ( 450, 220 );

		form = new FormPanel ();
		form.setEncoding ( FormPanel.ENCODING_MULTIPART );
		form.setMethod ( FormPanel.METHOD_POST );
		form.setStyleName( "ez-upload-dialog" ) ;
		
		
		root = new VerticalPanel ();
		
		root.setWidth( "100%" ) ;
		root.setSpacing ( 20 );
		
		Label label ;
		
		root.add ( label = new Label ( "Choose an image from your computer" ) );
		

		fileUpload = new FileInput ();
		fileUpload.setName ( "upload" );

		root.add ( fileUpload );
		
		uploadTip = new HTML() ;
		
		imageName = new Hidden() ;
		imageName.setName ( "imageName" ) ;
		
		root.add ( imageName ) ;
		root.add ( uploadTip ) ;

		Utils.marginTop( label, 70 ) ;
		Utils.marginTop( fileUpload, 10 ) ;
		
		root.setCellHorizontalAlignment( label , VerticalPanel.ALIGN_CENTER ) ;
		root.setCellHorizontalAlignment( fileUpload , VerticalPanel.ALIGN_CENTER ) ;
		root.setCellHorizontalAlignment( uploadTip , VerticalPanel.ALIGN_CENTER ) ;
		
		form.setWidget ( root );

		add ( form );

		bind() ;
		
	}

	
	protected void bind() {
		
		fileUpload.addOnChangeHandler ( new ChangeHandler () {
			
			public void onChange ( ChangeEvent e ) {				
				
				String fileName = fileUpload.getFilename ();
				
				if ( fileName == null || fileName.length () == 0 ) {
					Utils.alert ( "Please choose an image file." );
					return;
				}
				
				String filename = fileUpload.getFilename() ;
				String extension = filename.substring ( filename.lastIndexOf ( '.' ) + 1 ).toLowerCase () ;

				imageName.setValue ( uploadId + "." +  extension  ) ;		
				
				form.submit ();
			
			}
		} );
		
		
		
		form.addSubmitCompleteHandler ( new FormPanel.SubmitCompleteHandler () {
			
			public void onSubmitComplete ( SubmitCompleteEvent event ) {
				
				String result = event.getResults ();
				
				logger.log ( Level.FINE, "Upload results:" + result );

				if ( result.indexOf ( "SESSION_TIMEOUT" ) > 0 ) {
					
					Utils.alert( "Sorry, your editing session has expired due to inactivity" ) ;
					return ;
					
				} else if ( result.indexOf ( "UPLOAD_KO" ) > 0 ) {
					
					Utils.alert ( uploadErrorMsg ) ;
   					return;
					
				} else {
   					
					close ();
   	  
   					UploadEvent uploaded = new UploadEvent( true ) ;
   					uploaded.setFileName ( imageName.getValue() ) ;
   					handlerManager.fireEvent ( uploaded ) ;

				}
				
			}
		} );
		
	}
	
	
	@Override
	public void openCenter() {
		super.openCenter();
		form.reset() ;
	}
	
	private class FileInput extends FileUpload {

		public void addOnChangeHandler ( final ChangeHandler handler ) {
			addDomHandler ( new ChangeHandler () {
				public void onChange ( ChangeEvent event ) {
					handler.onChange ( null );
				}
			}, ChangeEvent.getType () );
		}

	}
	
	public void setUploadId( String uploadId ) {
		this.uploadId = uploadId;
	}	

	
	public void setUploadTip ( String html ) {
		uploadTip.setHTML ( html ) ;
	}
	
	
	public void setUploadUrl ( String url ) {
		form.setAction (  url ) ;
	}

	public void setUploadErrorMsg(String uploadErrorMsg) {
		this.uploadErrorMsg = uploadErrorMsg;
	}

	
	
	@Override
	public HandlerRegistration addUploadEventHandler( UploadEventHandler handler ) {
		return handlerManager.addHandler ( UploadEvent.TYPE , handler );
	}


}
