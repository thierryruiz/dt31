package com.dotemplate.core.client.widgets;

import org.gwtbootstrap3.client.shared.event.ModalHideEvent;
import org.gwtbootstrap3.client.shared.event.ModalHideHandler;
import org.gwtbootstrap3.client.shared.event.ModalShowEvent;
import org.gwtbootstrap3.client.shared.event.ModalShowHandler;
import org.gwtbootstrap3.client.ui.Button;
import org.gwtbootstrap3.client.ui.Container;
import org.gwtbootstrap3.client.ui.Modal;
import org.gwtbootstrap3.client.ui.ModalBody;
import org.gwtbootstrap3.client.ui.ModalFooter;
import org.gwtbootstrap3.client.ui.ModalHeader;
import org.gwtbootstrap3.client.ui.constants.ButtonType;

import com.dotemplate.core.client.frwk.Utils;
import com.dotemplate.core.client.resources.css.CSSBundles;
import com.dotemplate.core.client.resources.js.JSBundles;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Style.Overflow;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window.ScrollEvent;
import com.google.gwt.user.client.Window.ScrollHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;


public class Window extends Composite  {
	
	static {
		Utils.useScript( JSBundles.INSTANCE.getJQueryUiJs() ) ;
		Utils.useStylesheet( CSSBundles.INSTANCE.getJQueryUiCss() ) ;
	}
	
	
	private static WindowUiBinder uiBinder = GWT
			.create(WindowUiBinder.class);

	interface WindowUiBinder extends UiBinder< Widget, Window > {
	}
	

	@UiField 
	Modal modal ;
	
	@UiField
	protected ModalBody body ; 
	
	@UiField 
	protected ModalHeader header ;

	@UiField 
	protected ModalFooter footer ;

	@UiField
	protected Container content ; 
	
	public Window() {
		
		initWidget(uiBinder.createAndBindUi(this));
		
		// bootstrap fix
		modal.addHideHandler(new ModalHideHandler() {
			@Override
			public void onHide(ModalHideEvent evt) {
				RootPanel.getBodyElement().getStyle().setOverflow(Overflow.AUTO);
				
			}
		}) ;
		
		modal.addShowHandler(new ModalShowHandler() {
			
			@Override
			public void onShow(ModalShowEvent evt) {
				RootPanel.getBodyElement().getStyle().setOverflow(Overflow.AUTO);
				
			}
		});
	}
	
	public void setMaximizable ( boolean maximizable ){
		//innerWnd.setMaximizable ( maximizable ) ;
	}
	
	public void setSize ( int width, int height ){
		modal.setWidth(width + "px");
		content.setHeight(height + "px");
		
	}
	
	public void setHeading( String heading ){
		header.setTitle( heading ) ;
	}
	
	
	protected void setDraggabe(){
		nativeDraggable(modal.getElement());
	}
			
	
	public void add( Widget widget ){
		content.add( widget ) ;
	}

	
	public Button addOkButton() {
		
		Button button = new Button ( "Ok" ) ;
		button.setType(ButtonType.PRIMARY );
		
		footer.add( button ) ;
	
		return button ;
	}
	
	
	public Button addOkButton( final String text ) {
		Button button = new Button ( text ) ;
		
		button.setType(ButtonType.PRIMARY );
		
		footer.add( button ) ;
	
		return button ;
	
	}
	
	
	public Button addCancelButton(){
		Button button = new Button ( "Cancel" ) ;
		button.setType( ButtonType.DANGER );
		
		footer.add( button ) ;
	
		return button ;
		
	}
	
	public void setId( String id ) {
		modal.getElement().setId( id ) ;
	}
	
	
	public void setFixedTopPosition( final int top ) {
		com.google.gwt.user.client.Window.addWindowScrollHandler(new ScrollHandler() {
			@Override
			public void onWindowScroll( ScrollEvent event ) {
				//innerWnd.setPagePosition( getLeft(), event.getScrollTop() + top ) ;
			}
		});
	}
	
	
	public void open(){
		modal.show() ;
	}	

	
	@Deprecated
	public void openCenter(){
		open() ;
	}	
	
			
	public void close() {
		modal.hide() ;
	}
	

	
	public void moveTo( int left, int top, boolean animate ){
	}
	
	
   private native void nativeDraggable( final Element e ) /*-{
		$wnd.jQuery(e).draggable({
    		handle: ".modal-header"
		}); 
	}-*/;
    
	
}
