package com.dotemplate.core.client.widgets;

import com.extjs.gxt.ui.client.event.DragEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.SliderEvent;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.HasChangeHandlers;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.SimplePanel;


@Deprecated
public class Slider extends SimplePanel implements HasChangeHandlers {

	private MySlider innerSlider ;
	

	public Slider ( double start, double end, int width ) {
		
		innerSlider = new MySlider() ; 
		innerSlider.setWidth ( width ) ;
		innerSlider.setMinValue ( ( int ) start ) ;
		innerSlider.setMaxValue ( ( int ) end ) ;
		
		innerSlider.setIncrement( 1 );  
	  
		innerSlider.setClickToChange( false );  
		
		setWidget ( innerSlider ) ;
		
		innerSlider.addListener ( Events.Change, new Listener<SliderEvent>() {
			public void handleEvent ( SliderEvent be ) {
				Slider.this.fireEvent (  new ChangeEvent(){
					public Object getSource () {
						return this ;
					}
				}) ;
			}
		}) ;		
	}
	
	
	public void disable() {
		innerSlider.disable () ;
	}
	
	public void enable() {
		innerSlider.enable () ;
	}
	
	
	public void setVertical( boolean vertical ){
		innerSlider.setVertical ( true ) ;
	}
	
	public Slider ( double start, double end ) {
		this( start, end, 100 ) ;
	}
	
	
	public void setValue( double value ) {
		try {
			innerSlider.setValue ( ( int ) value ) ; 
		} catch ( Exception ex ){ /* catch error on slider tip display */ }
		
	}
	
	
	public double getValue() {
		return ( double ) innerSlider.getValue ();
	}
	
	
	
	class MySlider extends com.extjs.gxt.ui.client.widget.Slider {

		public void setValue( int value ) {
			setValue( value, true );
		}
		
		
		protected void onDragEnd(DragEvent de) {
			super.onDragEnd ( de ) ;
			Slider.this.fireEvent ( new ChangeEvent(){
				public Object getSource () {
					return Slider.this ;
				}
			}) ;
		}
		
	}


	@Override
	public HandlerRegistration addChangeHandler ( final ChangeHandler handler ) {
		return super.addHandler ( handler, ChangeEvent.getType () );
	}
	
	
}
