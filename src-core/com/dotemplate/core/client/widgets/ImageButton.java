package com.dotemplate.core.client.widgets;


import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;

import com.extjs.gxt.ui.client.widget.button.Button;


public class ImageButton implements IsWidget, HasClickHandlers {

	private Button extButton ;
	
	public enum Scale { SMALL, MEDIUM, LARGE } ;
	
	public enum Layout { ICON_TOP, ICON_RIGHT } ;
	
	public ImageButton( String label, String icon, Scale scale, Layout layout ) {
	}
	
	public void breakLabel(){
		extButton.setText( "<span>" + extButton.getText().replaceFirst( 
				" ", "<br/>") + "</span>" ) ; 
	}
	
	
	@Override
	public void fireEvent( GwtEvent< ? > event ) {
		// TODO Auto-generated method stub	
	}

	@Override
	public HandlerRegistration addClickHandler(ClickHandler arg0) {
		return null;
	}

	@Override
	public Widget asWidget() {
		return extButton ;
	}


}
