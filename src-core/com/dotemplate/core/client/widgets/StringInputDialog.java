package com.dotemplate.core.client.widgets;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;



public class StringInputDialog extends Window {

	protected Label label ;
	
	protected TextBox input ; 
				
	protected Callback callback ;
	
	
	public StringInputDialog() {
		setSize( 400, 100 );
		FlowPanel root = new FlowPanel() ;
		
		root.add( label = new Label() ) ;
		root.add( input = new TextBox() ) ;
		
		input.setWidth( "350px" ) ;
		
        input.addKeyPressHandler ( new KeyPressHandler() {
        	@Override
        	public void onKeyPress ( KeyPressEvent keyEvt ) {   
        		if ( keyEvt.getNativeEvent().getKeyCode() == KeyCodes.KEY_ENTER ) {
            		
        			apply() ;
        			keyEvt.stopPropagation () ;
        		}
        		
        	}
        });
		
		add( root ) ;
		
		addOkButton().addClickHandler( new ClickHandler() {
			@Override
			public void onClick( ClickEvent e ) {
				apply() ;
			}
		}) ;
		
		addCancelButton().addClickHandler( new ClickHandler() {
			@Override
			public void onClick( ClickEvent e ) {
				close() ;
			}
		}) ;
	
	}
	
	
	protected void apply() {
		if ( ! validate() ) {
			//Utils.alert( "Please enter a valid text" ) ;
		} else {
			

			close() ;

			callback.onOk( getValue() ) ;
			
		}
	}
	
	
	protected boolean validate() {		
		return true ;
	}

	
	

	public void open( String text, Callback callback ) {
		this.callback = callback ;
		input.setText( text ) ;
		openCenter() ;
	}

	
	public void setLabel( String lbl ){
		label.setText( lbl ) ;
	}
	
	
	public String getValue() {
		return input.getText() ;
	}
	
	
	public interface Callback {
		void onOk ( String value ) ; 
	}
	
	
	
}
