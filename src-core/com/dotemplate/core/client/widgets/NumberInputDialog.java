package com.dotemplate.core.client.widgets;

import com.dotemplate.core.client.frwk.Utils;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;



public class NumberInputDialog extends Window {

	protected Label label ;
	
	protected TextBox input ; 
			
	protected  int min ;
	
	protected int max ;
	
	protected Callback callback ;
	
	
	public NumberInputDialog() {
		setSize( 300, 100 );
		FlowPanel root = new FlowPanel() ;
		
		root.add( label = new Label() ) ;
		root.add( input = new TextBox() ) ;
		
		input.setWidth( "150px" ) ;
		
        input.addKeyPressHandler ( new KeyPressHandler() {
        	@Override
        	public void onKeyPress ( KeyPressEvent keyEvt ) {   
        		if ( keyEvt.getNativeEvent().getKeyCode() == KeyCodes.KEY_ENTER ) {
            		
        			apply() ;
        			keyEvt.stopPropagation () ;
        		}
        		
        	}
        });
		
		add( root ) ;
		
		addOkButton().addClickHandler( new ClickHandler() {
			@Override
			public void onClick( ClickEvent e ) {
				apply() ;
			}
		}) ;
		
		addCancelButton().addClickHandler( new ClickHandler() {
			@Override
			public void onClick( ClickEvent e ) {
				close() ;
			}
		}) ;
	
	}
	
	
	protected void apply() {
		if ( ! validate() ) {
			Utils.alert( "Please enter a valid size between " 
					+ min + " and " + max + "." ) ;
		} else {
			
			close() ;
			callback.onOk( getValue() ) ;
		}
	}
	
	
	protected boolean validate() {
		int value = -1 ;
		try {
			value = Integer.parseInt( input.getText() ) ;
		} catch ( Exception e ){
			return false ;
		}
		
		if ( value < min || value > max ) return false ;
		
		return true ;
	}

	
	

	public void open( Integer value, int min, int max, Callback callback ) {
		this.callback = callback ;
		input.setText( "" + value ) ;
		this.min = min ;
		this.max = max ;
		openCenter() ;
	}

	
	public void setLabel( String lbl ){
		label.setText( lbl ) ;
	}
	
	
	public int getValue() {
		return Integer.parseInt( input.getText() ) ;
	}
	
	
	public interface Callback {
		void onOk ( int value ) ; 
	}
	
	
	
}
