package com.dotemplate.core.client.widgets;

import com.extjs.gxt.ui.client.core.El;
import com.extjs.gxt.ui.client.fx.FxConfig;
import com.extjs.gxt.ui.client.widget.ModalPanel;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.google.gwt.user.client.Window.ScrollEvent;
import com.google.gwt.user.client.Window.ScrollHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;



@Deprecated
public class DeprecatedWindow extends Composite  {
	
	protected ExtWindow innerWnd ;
	
	protected  ModalPanel modalPanel ;
	
	public DeprecatedWindow() {
		
		innerWnd = new ExtWindow() ;
		
		innerWnd.setLayout ( new FitLayout () ) ;
		innerWnd.setPosition ( 0 , 0 ) ;
		innerWnd.setDraggable( true ) ;
		innerWnd.setClosable( true ) ;
		innerWnd.setMinWidth ( 80 ) ;
		add ( innerWnd );
		
	}
	
	
	
	/*
	public void setModalBlur(){
		innerWnd.modalPanel().addStyleName( "x-modal2" ) ;
	}
	*/
	
	
	public void setMaximizable ( boolean maximizable ){
		innerWnd.setMaximizable ( maximizable ) ;
	}
	
	public void setSize ( int width, int height ){
		innerWnd.setSize(width, height) ;
	}
	
	public void setHeading( String heading ){
		innerWnd.setHeading (  heading ) ;
	}
	
	public void setModal( boolean modal ){
		innerWnd.setModal( modal ) ;
	}
	
	public void setDraggable( boolean draggable ){
		innerWnd.setDraggable( draggable ) ;
	}
	
	public void setPosition( int left, int top  ){
		innerWnd.setPosition( left, top ) ;
	}
	
	public void setConstrain ( boolean constrain ){
		innerWnd.setConstrain( constrain ) ;
	}
	
	public void setClosable ( boolean closable ){
		innerWnd.setClosable( closable ) ;
	}
	
	public void setResizable ( boolean resizable ){
		innerWnd.setResizable( resizable ) ;
	}

	public void removeAll (){
		innerWnd.removeAll() ;
	}

	public void setCollapsible ( boolean collapsible ){
		innerWnd.setCollapsible( collapsible ) ;
	}

	public void setPadding( int padding ) {
		innerWnd.setBodyStyle ("padding:" + padding + "px" ) ;
	}
	
	
	public void add( Widget widget ){
		innerWnd.add( widget ) ;
	}

	public Button addOkButton() {
		
		Button button = new Button ( "Ok" ) ;
		
		innerWnd.addButton( ( com.extjs.gxt.ui.client.widget.button.Button ) 
				button.getInnerButton() ) ;
	
		return button ;
	}
	
	
	public Button addOkButton( final String text ) {
		Button button = new Button ( text ) ;
		
		innerWnd.addButton( ( com.extjs.gxt.ui.client.widget.button.Button ) 
				button.getInnerButton() ) ;
		
		return button ;
	
	}
	
	
	public Button addCancelButton(){
		Button button = new Button ( "Cancel" ) ;
		
		innerWnd.addButton( ( com.extjs.gxt.ui.client.widget.button.Button ) 
				button.getInnerButton() ) ;
		
		return button ;
		
	}
	
	public void setId( String id ) {
		innerWnd.getElement().setId( id ) ;
	}
	
	
	public void setFixedTopPosition( final int top ) {
		com.google.gwt.user.client.Window.addWindowScrollHandler(new ScrollHandler() {
			@Override
			public void onWindowScroll( ScrollEvent event ) {
				innerWnd.setPagePosition( getLeft(), event.getScrollTop() + top ) ;
			}
		});
	}
	
	
	public void addButton( Button button ){
		innerWnd.addButton( ( com.extjs.gxt.ui.client.widget.button.Button ) 
				button.getInnerButton() ) ;
	}
	
	
	public void open(){
		innerWnd.show() ;
	}	

	
	public void openCenter(){
		innerWnd.show() ;
		innerWnd.center() ;	
		innerWnd.layout( true ) ;
	}	
	
			
	public void close() {
		innerWnd.hide() ;
	}
	

	public int getLeft () {
		return innerWnd.getAbsoluteLeft ();
	}

	public int getTop () {
		return innerWnd.getAbsoluteTop ();
	}

	
	public void moveTo( int left, int top, boolean animate ){
		if( animate ){
			innerWnd.el ().setXY ( left, top, FxConfig.NONE  ) ;
		} else {
			innerWnd.el().setXY ( left, top ) ;
		}
	}
	
	
	public class ExtWindow extends com.extjs.gxt.ui.client.widget.Window {
		public ModalPanel modalPanel() {
			return getModalPanel() ;
		}
		
		public El getShadowEl(){
			return layer ;
		}
	}
	
	
}
