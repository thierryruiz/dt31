package com.dotemplate.core.client.widgets;



public class PercentSlider extends Slider {

	public PercentSlider () {
		super ( 0.0 , 100.0 );
	}

	public PercentSlider ( int width ) {
		super ( 0.0 , 100.0, width  );
	}
	
	public float getFloatValue () {
		return ( float ) getValue() / 100 ; 
	}
	
	public void setFloatValue ( float value ) {
		setValue( value * 100 ) ; 
	}
	
	
}
