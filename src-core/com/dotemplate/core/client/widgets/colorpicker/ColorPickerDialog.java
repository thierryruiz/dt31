/*
/*
 * 
 * Created on 12 juil. 2007
 * 
 */
package com.dotemplate.core.client.widgets.colorpicker;

import org.gwtbootstrap3.client.ui.Button;

import com.dotemplate.core.client.frwk.Utils;
import com.dotemplate.core.client.widgets.Window;
import com.dotemplate.core.shared.Scheme;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;


/**
 * 
 * 
 * @author Thierry Ruiz
 * 
 */
@Deprecated
public class ColorPickerDialog extends Window  {
	        
    private VerticalPanel root ;
    
    private FlowPanel schemePanel ;
    
    private Label userSchemeLabel ;
    
    private FlowPanel userSchemePanel ;
        
    private FlowPanel grayScalePanel ;
    
    private final static int WIDTH = 450 ;
    
    private final static int HEIGHT = 500 ;
    
    private ColorPicker colorPicker ;
 
    private Handler handler ;
    
    
    public ColorPickerDialog (){
		setSize( WIDTH + 60, HEIGHT ) ;
		setHeading ( "Choose color" ) ;
		build() ;
    }
    
    
	public void setSelectedColor( String hex ) {
        try {
		colorPicker.setHex( hex.substring(1) ) ;
		} catch (Exception e) {
		}
    }
    
	
	private void addColor( final String color, FlowPanel panel ){
		
        if ( color == null ) return ;
        
        final Image btn = Utils.spacer(15, 15) ;
        
        DOM.setStyleAttribute ( btn.getElement(), "backgroundColor", color );
        DOM.setStyleAttribute ( btn.getElement(), "float", "left" );
        DOM.setStyleAttribute ( btn.getElement(), "margin", "1px" );
        DOM.setStyleAttribute ( btn.getElement(), "cursor", "pointer" );
        DOM.setStyleAttribute ( btn.getElement(), "border", "1px solid #000000" );
        
        panel.add( btn ) ;
                
        btn.addClickHandler( new ClickHandler () {

            public void onClick( ClickEvent e ) {
                setSelectedColor ( color ) ;
            }
        
        }) ;
        
	}
	
	
	public void open ( Handler handler ) {
	
		this.handler = handler ;
		
		openCenter ();		

	}
	
	
	protected void build() {
		
		
		root = new VerticalPanel() ;
        root.setStyleName( "ez-color-picker" ) ;
		
        schemePanel = new FlowPanel () ;
        schemePanel.setWidth( "100%" ) ;
        
        grayScalePanel = new FlowPanel () ;
        grayScalePanel.setWidth( WIDTH - 80  + "px" ) ;
        
        addColor( "#000000", grayScalePanel );
        addColor( "#101010", grayScalePanel );
        addColor( "#202020", grayScalePanel );
        addColor( "#303030", grayScalePanel );
        addColor( "#404040", grayScalePanel );
        addColor( "#505050", grayScalePanel );
        addColor( "#606060", grayScalePanel );
        addColor( "#707070", grayScalePanel );
        addColor( "#808080", grayScalePanel );
        addColor( "#909090", grayScalePanel );
        addColor( "#A0A0A0", grayScalePanel );
        addColor( "#B0B0B0", grayScalePanel );
        addColor( "#C0C0C0", grayScalePanel );
        addColor( "#D0D0D0", grayScalePanel );
        addColor( "#E0E0E0", grayScalePanel );
        addColor( "#EFEFEF", grayScalePanel );
        addColor( "#F8F8F8", grayScalePanel );        
        addColor( "#FFFFFF", grayScalePanel );
        
        
        root.add( new Label( "Color scheme" ) ) ;
        root.add( schemePanel ) ;
        
        Label grayScaleLabel = new Label( "Gray scale" ) ;
        Utils.marginTop( grayScaleLabel, 10) ;
        
        root.add (  grayScaleLabel ) ;
        root.add (  grayScalePanel ) ;
        
        
        userSchemeLabel = new Label( "Your colors" ) ;
        userSchemePanel = new FlowPanel() ;
        Utils.marginTop( userSchemeLabel, 10) ;
        
        userSchemeLabel.setVisible( false ) ;
        userSchemePanel.setVisible( false ) ;
        
        root.add( userSchemeLabel ) ;
        root.add( userSchemePanel ) ;
        
        
        
        root.add( colorPicker = new ColorPicker () ) ;
        Utils.marginTop( colorPicker, 10) ;
        
        
        Button okButton = addOkButton () ;
                
        addCancelButton ().addClickHandler( new ClickHandler() {
			@Override
			public void onClick(ClickEvent e) {
				ColorPickerDialog.this.close() ;
				
			}
		}) ;
        
        
        okButton.addClickHandler( new ClickHandler() {
			@Override
			public void onClick( ClickEvent e ) {
				ColorPickerDialog.this.close() ;
				handler.onColorSelected(  "#"  + colorPicker.getHexColor()  ) ;
			}
		}) ;
                
		add (  root ) ;
		
	}
	

	
	public void setScheme ( Scheme scheme ) {
		
		schemePanel.clear() ;
	    String[] colors = scheme.getColors();        
        for ( int i = 0 ; i < colors.length ; i++ ) {
            addColor( colors [ i ], schemePanel ) ;
        }
	
	}
	

	
	public void setUserScheme ( String userScheme ) {
		
		if ( userScheme == null || userScheme.length() == 0  ) {
			return ; 
		}

		userSchemePanel.clear() ;
		userSchemeLabel.setVisible( true ) ;
		userSchemePanel.setVisible( true ) ;
		
	    String[] colors = userScheme.split( "," ) ;        
        
	    for ( int i = 0 ; i < colors.length ; i++ ) {
            addColor( colors [ i ], userSchemePanel ) ;
        }
	    
	    
	    
	}


	public interface Handler {
		void onColorSelected( String color )  ;
	}

	

	
}


