package com.dotemplate.core.client.widgets.colorpicker;

import java.util.LinkedHashMap;

import com.dotemplate.core.client.widgets.resource.DesignResourceChangeHandler;
import com.dotemplate.core.client.widgets.resource.DesignResourceDialog;
import com.dotemplate.core.client.widgets.resource.DesignResourceLoadAdapter;
import com.dotemplate.core.client.widgets.resource.DesignResourceProvider;
import com.dotemplate.core.client.widgets.resource.DesignResourceScrollSelector;
import com.dotemplate.core.client.widgets.resource.DesignResourceSelector;
import com.dotemplate.core.shared.Scheme;
import com.dotemplate.core.shared.command.GetAllSchemes;
import com.dotemplate.core.shared.command.GetDesignResourcesMap;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;


public class SchemeSelector extends DesignResourceDialog< Scheme > {

	private static SchemeSelector instance ;
	
	
	public SchemeSelector() {
		
		super( new DesignResourceProvider< Scheme >() {

			@Override
			protected GetDesignResourcesMap<Scheme> getDesignResourcesMapCommand() {
				return new GetAllSchemes() ;
			}
			
		
		}, 480, 420 ) ;
		
		setHeading( "Choose color scheme" ) ;
	
	}
	
	
	public static void show( DesignResourceChangeHandler< Scheme > callback, Scheme scheme  ){
		
		if ( instance == null ){
			instance = new SchemeSelector() ;
		}
		
		instance.open( scheme, callback ) ;
		
	}
	
	
	public void open( final Scheme scheme , final DesignResourceChangeHandler< Scheme > callback ) {
		
		this.callback = callback ;
		
		if ( !resourceSelector.isLoaded() ){
			
			resourceProvider.addLoadHandler( new DesignResourceLoadAdapter<Scheme>(){
				@Override
				public void onDesignResourcesLoad(
						LinkedHashMap<String, LinkedHashMap<String, Scheme > > tags) {
					
					
					( ( DesignResourceScrollSelector< Scheme > ) resourceSelector ) .setResources( tags ) ;
					
					resourceSelector.setValue( scheme ) ;
					
					open() ;	
					
					
				}
			});
			
			resourceProvider.loadResourcesMap() ;
			
			
		} else {
			
			resourceSelector.setValue( scheme ) ;
			
			open() ;	
			
		}
	}
	
	
	
	
	@Override
	protected DesignResourceSelector < Scheme > createResourceSelector() {
		
		return new DesignResourceScrollSelector< Scheme >() {
			
			@Override
			public HandlerRegistration addValueChangeHandler(
					ValueChangeHandler<Scheme> handler) {
				return handlerManager.addHandler( ValueChangeEvent.getType(), handler );
			}
			
		};
	}



	

}
