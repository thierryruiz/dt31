package com.dotemplate.core.client.canvas;


import com.dotemplate.core.client.canvas.event.HasLayerEventHandlers;
import com.dotemplate.core.client.canvas.event.LayerEvent;
import com.dotemplate.core.client.canvas.event.LayerEventHandler;
import com.dotemplate.core.client.canvas.presenter.GraphicEditor;
import com.dotemplate.core.client.frwk.Utils;
import com.dotemplate.core.client.resources.css.CSSBundles;
import com.dotemplate.core.client.resources.js.JSBundles;
import com.dotemplate.core.shared.canvas.Graphic;
import com.google.gwt.dom.client.Element;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.event.dom.client.LoadEvent;
import com.google.gwt.event.dom.client.LoadHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;


public abstract class Layer< GRAPHIC extends Graphic > implements IsWidget, HasLayerEventHandlers {
	
	public static int BORDER_WIDTH = 1 ;
	
	static {
		Utils.useScript( JSBundles.INSTANCE.getJQueryUiJs() ) ;
		Utils.useStylesheet( CSSBundles.INSTANCE.getJQueryUiCss() ) ;
	}
	
	protected Image img ;
	
	protected String imgUrl ;
		
	protected boolean selected ;

	protected FocusPanel imgContainer ;

	protected HandlerManager handlerManager ;
	
	protected GraphicEditor< GRAPHIC > editor ;
	
	//protected LayerButton < GRAPHIC > selector ;
	
	
	public Layer () {

		handlerManager = new HandlerManager( this ) ;
		
		selected = false ;

		img = new Image () ;

		imgContainer = new FocusPanel() ;
		
		imgContainer.addKeyDownHandler( new KeyDownHandler() {
			
			@Override
			public void onKeyDown(KeyDownEvent event) {
				if(event.getNativeKeyCode() == KeyCodes.KEY_DELETE) {  
					fireDeleted();
				}
			}
		} ) ;
		
		
		imgContainer.setStyleName ( "ez-layer" ) ;
		
		// image must be wrapped in a div 
		// in order to be draggable
		imgContainer.add ( img );
				
		img.addClickHandler ( new ClickHandler () {
			public void onClick ( ClickEvent arg0 ) {
				select() ;
			}
		} );
		
		
		img.addLoadHandler ( new LoadHandler(){
			@Override
			public void onLoad ( LoadEvent arg0 ) {
				makeDraggable () ;
				makeResizeable () ;	
			}
		}) ;
				
	}
	
	
	public int getLeft() {
		
		Widget parentPanel = imgContainer.getParent() ;
		
		if ( parentPanel == null ) return 0 ;
		
		return ( !selected ) ? imgContainer.getAbsoluteLeft () - parentPanel.getAbsoluteLeft () :   	
			imgContainer.getAbsoluteLeft () - parentPanel.getAbsoluteLeft () + BORDER_WIDTH ;	

	}

	
	public int getTop() {		
				
		Widget parentPanel = imgContainer.getParent() ;
		
		if ( parentPanel == null ) return 0 ;

		return ( !selected ) ? imgContainer.getAbsoluteTop () - parentPanel.getAbsoluteTop ()  :   	
			imgContainer.getAbsoluteTop () - parentPanel.getAbsoluteTop () + BORDER_WIDTH ; 
	}
	
	
	public int getWidth() {
		return img.getWidth() ;
	}
	

	public int getHeight () {
		return img.getHeight() ;
	}
	

	public void setImageUrl ( String url ) {
		this.imgUrl = url ;
		img.setUrl ( url + "?" + System.currentTimeMillis () ) ;
	}
	
	public GraphicEditor< GRAPHIC > getEditor() {
		return editor;
	}

	public void setEditor( GraphicEditor<GRAPHIC> editor ) {
		this.editor = editor;
	}
	
	
	public GRAPHIC getGraphic () {
		return editor.getGraphic() ;
	}
	
	
	public abstract boolean isMoveable () ;
	
	
	public void makeDraggable() {
		// image must be wrapped in a div to be draggable
		
		nativeDraggable( imgContainer.getElement() );
		
		/*
		draggable = new Draggable(  ui ) ;
		draggable.setUpdateZIndex ( false ) ;
		
		draggable.addDragListener ( new DragListener () {

			@Override
			public void dragStart ( DragEvent de ) {
				select() ;
			}
			
			public void dragEnd( com.extjs.gxt.ui.client.event.DragEvent de ) {				
				fireMoved() ;
			};
		}) ;
		
		draggable.setUseProxy( false ); 
		*/
	}
	
	
	
	
	
	protected void  makeResizeable() {
		
		nativeResizable( img.getElement() );
		
		/*
		resizable = new Resizable ( ui ) ;
		
		resizable.addResizeListener ( new ResizeListener ( ) {	
			
			public void resizeEnd ( ResizeEvent re ) {
				super.resizeEnd ( re );
				
				img.setWidth ( Layer.this.getWidth() + "px" );
				img.setHeight ( Layer.this.getHeight()  +  "px" ) ;
				
				fireResized() ;
				
			}
			
			
			@Override
			public void resizeStart ( ResizeEvent re ) {
				super.resizeStart ( re );
				select() ;
			}
			
		}) ;
		
		resizable.setPreserveRatio ( true ) ;
		*/
	}
	
	
    private native void nativeDraggable( final Element e ) /*-{
    	var _this = this;
		$wnd.jQuery(e).draggable({
			start:function(e, ui) {
				_this.@com.dotemplate.core.client.canvas.Layer::select()();
			},	
      		stop: function(e, ui) {
      			_this.@com.dotemplate.core.client.canvas.Layer::fireMoved()();
      		}
    	}) ;
	}-*/;
    
    
    private native void nativeResizable( final Element e ) /*-{
		
		var _this = this;
		
		$wnd.jQuery(e).resizable({
      		
      		aspectRatio: true,
      		handles: "n, e, s, w",
      		
      		start:function(e, ui) {
				_this.@com.dotemplate.core.client.canvas.Layer::select()();
			},
      		stop: function(e, ui) {
      			_this.@com.dotemplate.core.client.canvas.Layer::fireResized()();
      		}
    	}) ;
	}-*/;
	
    
	
	@Override
	public Widget asWidget() {
		return imgContainer;
	}
	
	
	public void refresh ( String url, int rWidth, int rHeight ) {
		setImageUrl( url );
	}
	
	
	public void select() {
		
		if ( selected ) {
			return ;
		}
	
		selected = true ;
		
		imgContainer.setFocus(true);
		imgContainer.addStyleName ( "selected" ) ;
		
		fireSelected() ;
		
	}
	
	
	public void unselect(){
		
		if( !selected ) {
			return ;
		}
		
		selected = false ;
		imgContainer.setFocus(false);
		imgContainer.removeStyleName ( "selected" ) ;
		
		
	}
	
	
	public abstract String getLabel() ;
	
	
	public HandlerRegistration addLayerEventHandler ( LayerEventHandler h ) {
		return handlerManager.addHandler ( LayerEvent.TYPE, h );
	}
	
	@Override
	public void fireEvent ( GwtEvent<?> e ) {
		handlerManager.fireEvent ( e  ) ;
	}
	
	public void fireMoved () {
		fireEvent ( new LayerEvent( this, LayerEvent.EventType.MOVED ) ) ;
	}

	public void fireResized () {
		fireEvent ( new LayerEvent( this, LayerEvent.EventType.RESIZED ) ) ;
	}

	public void fireSelected () {
		fireEvent ( new LayerEvent( this, LayerEvent.EventType.SELECTED ) ) ;
	}
	
	public void fireRenamed () {
		fireEvent ( new LayerEvent( this, LayerEvent.EventType.RENAMED ) ) ;
	}
	
	public void fireDeleted () {
		fireEvent ( new LayerEvent( this, LayerEvent.EventType.DELETED ) ) ;
	}
	

}
