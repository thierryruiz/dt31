package com.dotemplate.core.client.canvas;



import com.dotemplate.core.client.frwk.Utils;
import com.dotemplate.core.client.widgets.PercentSlider;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasChangeHandlers;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.DockPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ToggleButton;
import com.google.gwt.user.client.ui.VerticalPanel;

@Deprecated
public class FadingPanel extends VerticalPanel implements HasChangeHandlers {

	private DockPanel dock ;
	
	private ToggleButton topButton, rightButton, bottomButton, leftButton ; 
	
	private PercentSlider slider ;
	
	private ToggleButton active ;
	
	private float topFading = ( float ) 1.0 ;
	
	private float rightFading = ( float ) 1.0 ;
	
	private float bottomFading = ( float ) 1.0 ;
	
	private float leftFading = ( float ) 1.0 ;
	
	
	public FadingPanel ( float topFading , float rightFading, float bottomFading, float leftFading ) {
	
		this.topFading = topFading ;
		this.rightFading = rightFading ;
		this.bottomFading = bottomFading ;
		this.leftFading = leftFading ;
		
		setWidth(  "250px" ) ;
		
		topButton = new ToggleButton() ;
		topButton.setStyleName ( "ez-drawboard-fadeTopBtn" ) ;

		rightButton = new ToggleButton() ;
		rightButton.setStyleName ( "ez-drawboard-fadeRightBtn" ) ;

		bottomButton = new ToggleButton() ;
		bottomButton.setStyleName ( "ez-drawboard-fadeBottomBtn" ) ;

		leftButton = new ToggleButton() ;
		leftButton.setStyleName ( "ez-drawboard-fadeLeftBtn" ) ;

		
		dock = new DockPanel() ;
		dock.setPixelSize ( 120, 140 ) ;
		
		dock.setVerticalAlignment ( DockPanel.ALIGN_MIDDLE ) ;
		dock.setHorizontalAlignment ( DockPanel.ALIGN_CENTER ) ;

		
		slider = new PercentSlider( 130 ) ;
		
		
		dock.add ( leftButton, DockPanel.WEST  ) ;
		dock.add ( rightButton, DockPanel.EAST ) ;
		dock.add ( topButton, DockPanel.NORTH ) ;
		dock.add ( bottomButton, DockPanel.SOUTH ) ;
		dock.add ( new Image ( "client/icons/FadingPanel/fading.jpg"  ), DockPanel.CENTER ) ;
		
		dock.setVerticalAlignment ( DockPanel.ALIGN_MIDDLE ) ;

		add ( dock ) ;
		setCellHorizontalAlignment ( dock, VerticalPanel.ALIGN_CENTER ) ;

		
		Label title = new Label( "Image fading" ) ;
		Utils.marginTop(title, 20 ) ;
		add( title ) ;
		setCellHorizontalAlignment ( title, VerticalPanel.ALIGN_CENTER ) ;
		
		add ( slider ) ;
		setCellHorizontalAlignment ( slider, VerticalPanel.ALIGN_CENTER ) ;
		
		
		slider.addChangeHandler ( new ChangeHandler(){
			
			@Override
			public void onChange ( ChangeEvent event ) {
				
				float value = slider.getFloatValue () ;
				
				if ( active == topButton )		{ 	FadingPanel.this.topFading =  value  ; 	} else 
				if ( active == rightButton )	{ 	FadingPanel.this.rightFading = value  ;	} else 
				if ( active == bottomButton )	{	FadingPanel.this.bottomFading =  value  ; } else
				if ( active == leftButton )		{	FadingPanel.this.leftFading = value  ; 	} else { 
					noDirectionSelectedError() ;
					return ; 
				}
				
				
				fireEvent ( new ChangeEvent(){
					@Override
					public Object getSource () {
						return FadingPanel.this ;
					}
				}) ;
				
			}

		});
		
		
		
		topButton.addClickHandler ( new ClickHandler () {
			@Override
			public void onClick ( ClickEvent e ) {
				slider.setFloatValue ( getTopFading () ) ;
				select( topButton ) ;
			}
		}) ;
	
		rightButton.addClickHandler ( new ClickHandler () {
			@Override
			public void onClick ( ClickEvent e ) {
				slider.setFloatValue ( getRightFading () ) ;
				select( rightButton ) ;
			}
		}) ;
	
		bottomButton.addClickHandler ( new ClickHandler () {
			@Override
			public void onClick ( ClickEvent e ) {
				slider.setFloatValue ( getBottomFading () ) ;
				select( bottomButton ) ;
			}
		}) ;
		
		
		leftButton.addClickHandler ( new ClickHandler () {
			@Override
			public void onClick ( ClickEvent e ) {
				slider.setFloatValue ( getLeftFading () ) ;
				select( leftButton ) ;
			
			}
		}) ;
	}



	private void noDirectionSelectedError() {
		Utils.alert( "Please select the fading direction, using arrow buttons." ) ;
		slider.setFloatValue( 0 ) ;
	}	
	
	
	protected void select ( ToggleButton toggle ) {
		if ( active != null ){
			active.setDown ( false ) ;
		}
		
		active = toggle ;
		active.setDown ( true ) ;
		slider.enable () ;
		
	}




	public float getTopFading () {
		return topFading;
	}

	public void setTopFading ( float topFading ) {
		this.topFading = topFading;
	}


	public float getRightFading () {
		return rightFading;
	}


	public void setRightFading ( float rightFading ) {
		this.rightFading = rightFading;
	}


	public float getBottomFading () {
		return bottomFading;
	}

	public void setBottomFading ( float bottomFading ) {
		this.bottomFading = bottomFading;
	}


	public float getLeftFading () {
		return leftFading;
	}


	public void setLeftFading ( float leftFading ) {
		this.leftFading = leftFading;
	}


	@Override
	public HandlerRegistration addChangeHandler ( ChangeHandler h ) {
		return super.addHandler ( h, ChangeEvent.getType () );
	}
	
}
