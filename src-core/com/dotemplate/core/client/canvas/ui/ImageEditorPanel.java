package com.dotemplate.core.client.canvas.ui;

import org.gwtbootstrap3.extras.slider.client.ui.Slider;
import org.gwtbootstrap3.extras.slider.client.ui.base.event.SlideStopEvent;
import org.gwtbootstrap3.extras.slider.client.ui.base.event.SlideStopHandler;

import com.dotemplate.core.client.canvas.view.ImageEditorView;
import com.dotemplate.core.shared.canvas.ImageGraphic;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.Widget;

public class ImageEditorPanel extends Composite implements ImageEditorView {

	private static ImageEditorPanelUiBinder uiBinder = GWT
			.create(ImageEditorPanelUiBinder.class);

	interface ImageEditorPanelUiBinder extends UiBinder<Widget, ImageEditorPanel> {
	}
	
	@UiField
	Slider opacitySlider ;
	
	@UiField
	Slider leftFadingSlider ;
	
	@UiField
	Slider rightFadingSlider ;
	
	@UiField
	Slider topFadingSlider ;
	
	@UiField
	Slider bottomFadingSlider ;
	
	@UiField
	Slider blurSlider ;
	
	
	SliderWrapper opacitySliderWrapper ;
	
	SliderWrapper leftFadingSliderWrapper ;
	
	SliderWrapper rightFadingSliderWrapper ;
	
	SliderWrapper topFadingSliderWrapper ;
	
	SliderWrapper bottomFadingSliderWrapper ;
	
	SliderWrapper blurSliderWrapper ;
	
	
	private ImageGraphic image ;

	public ImageEditorPanel( ImageGraphic image ) {
		
		this.image = image ;
		
		initWidget(uiBinder.createAndBindUi(this));
		
		opacitySliderWrapper = new SliderWrapper( opacitySlider ) ;
		
		leftFadingSliderWrapper = new SliderWrapper( leftFadingSlider ) ;
		
		rightFadingSliderWrapper = new SliderWrapper( rightFadingSlider ) ;
		
		topFadingSliderWrapper = new SliderWrapper( topFadingSlider ) ;
		
		bottomFadingSliderWrapper = new SliderWrapper( bottomFadingSlider ) ;
	
		blurSliderWrapper = new SliderWrapper( blurSlider ) ;
		
		
	}


	@Override
	public String getId() {
		return "imageEditor_" + image.getName() ;
	}


	@Override
	public HasValue<Double> getOpacityControl() {
		return opacitySliderWrapper ;
	}
	
	
	@Override
	public HasValue<Double> getLeftFadingControl() {
		return leftFadingSliderWrapper;
	}


	@Override
	public HasValue<Double> getRightFadingControl() {
		return rightFadingSliderWrapper;
	}


	@Override
	public HasValue<Double> getTopFadingControl() {
		return topFadingSliderWrapper;
	}


	@Override
	public HasValue<Double> getBottomFadingControl() {
		return bottomFadingSliderWrapper;
	}


	@Override
	public HasValue<Double> getBlurControl() {
		return blurSliderWrapper;
	}
	
	
	// Need wrapper because Slider class throws ValueChangeEvent even during sliding 
	// and not only on slide stop 
	class SliderWrapper implements HasValue< Double >{
		
		private Slider slider ;
		
		private HandlerManager handlerManager = new HandlerManager( this );
		
		SliderWrapper ( final Slider slider ){
			
			this.slider = slider ;
			
			this.slider.addSlideStopHandler(new SlideStopHandler<Double>() {
				@Override
				public void onSlideStop( SlideStopEvent<Double> event) {
					ValueChangeEvent.fire( SliderWrapper.this, SliderWrapper.this.getValue() )   ;
				}
			});
			
		
		}

		@Override
		public HandlerRegistration addValueChangeHandler(
				ValueChangeHandler<Double> handler) {
			return handlerManager.addHandler( ValueChangeEvent.getType(), handler );
		}

		@Override
		public void fireEvent(GwtEvent<?> event) {
			handlerManager.fireEvent(event);
		}

		@Override
		public Double getValue() {
			return slider.getValue();
		}

		@Override
		public void setValue(Double value) {
			slider.setValue(value);
			
		}

		@Override
		public void setValue(Double value, boolean fireEvents) {
			slider.setValue(value, fireEvents);
		}
		
	}








	
	

}
