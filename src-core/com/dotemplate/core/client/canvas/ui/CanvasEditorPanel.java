package com.dotemplate.core.client.canvas.ui;

import java.util.HashMap;

import org.gwtbootstrap3.client.shared.event.ShowEvent;
import org.gwtbootstrap3.client.shared.event.ShowHandler;
import org.gwtbootstrap3.client.ui.AnchorListItem;
import org.gwtbootstrap3.client.ui.Button;
import org.gwtbootstrap3.client.ui.PanelGroup;

import com.dotemplate.core.client.canvas.Layer;
import com.dotemplate.core.client.canvas.LayerButton;
import com.dotemplate.core.client.canvas.presenter.GraphicEditor;
import com.dotemplate.core.client.canvas.view.CanvasEditorView;
import com.dotemplate.core.shared.canvas.Graphic;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public class CanvasEditorPanel extends Composite implements CanvasEditorView, ShowHandler {

	private static CanvasEditorPanelUIBinder uiBinder = GWT
			.create(CanvasEditorPanelUIBinder.class);

	interface CanvasEditorPanelUIBinder extends UiBinder<Widget, CanvasEditorPanel> {
	}

	@UiField
	AbsolutePanel previewAbsolutePanel ;
		
	@UiField
	PanelGroup controlPanelGroup ;
	
	@UiField
	AnchorListItem addImageAnchor ;

	@UiField
	AnchorListItem addFlickImageAnchor ;

	@UiField
	AnchorListItem uploadImageAnchor ;
	
	@UiField
	AnchorListItem addLogoAnchor ;
	
	@UiField
	Button addTextButton ;
	
	@UiField
	Button deleteButton ;

	@UiField
	Button moveBackwardButton ;
	
	@UiField
	Button moveForwardButton ;
	
	
	protected HashMap< Layer<? extends Graphic> , LayerControlPanel > controlPanels ;
	
	
	public CanvasEditorPanel() {
		initWidget( uiBinder.createAndBindUi( this ) );
		controlPanels = new HashMap< Layer<? extends Graphic> , LayerControlPanel >() ;
	}

	@Override
	public String getId() {
		return "canvasEditorDialog" ;
	}

		
	@Override
	public void setCanvasSize(int width, int height) {
		previewAbsolutePanel.setPixelSize( width, height);
		
	}

	@Override
	public void clear() {
		previewAbsolutePanel.clear() ;
		controlPanelGroup.clear();
		controlPanels.clear() ;
	}

	
	@Override
	public void addLayer( Layer<? extends Graphic> layer, int left, int top) {
		previewAbsolutePanel.add( layer.asWidget(), left - Layer.BORDER_WIDTH, top - Layer.BORDER_WIDTH);
	}
	
	
	@Override
	@Deprecated
	public void addLayerButton( LayerButton<? extends Graphic > layerButton ){
	}
	
	
	

	@Override
	public void setEditor( GraphicEditor<? extends Graphic> currentEditor ) {
		controlPanels.get( currentEditor.getLayer() ).expand();
	}

	@Override
	public void addLayerEditor( GraphicEditor<? extends Graphic> editor ) {
		
		LayerControlPanel lcp = new LayerControlPanel( editor.getLayer() ) ;
		lcp.addShowHandler(this);
		lcp.getBody().add( editor.asWidget() );
		controlPanelGroup.add( lcp ) ;
		controlPanels.put(editor.getLayer(), lcp) ;
			
	}

	
	@Override
	public void onShow(ShowEvent showEvent) {
		collapseAll() ;
	}
	
	
	private void collapseAll() {
		for (LayerControlPanel lcp : controlPanels.values() ){
			lcp.collapse() ;
		}
	}

	@Override
	public HasClickHandlers getAddGalleryImageControl() {
		return addImageAnchor;
	}

	@Override
	public HasClickHandlers getAddFlickrImageControl() {
		return addFlickImageAnchor;
	}


	@Override
	public HasClickHandlers getAddLogoControl() {
		return addLogoAnchor;
	}

	@Override
	public HasClickHandlers getAddTextControl() {
		return addTextButton ;
	}

	@Override
	public HasClickHandlers getUploadImageControl() {
		return uploadImageAnchor ;
	}

	@Override
	public HasClickHandlers getDeleteControl() {
		return deleteButton;
	}

	@Override
	public HasClickHandlers getMoveForwardControl() {
		return moveForwardButton ;
	}

	@Override
	public HasClickHandlers getMoveBackwardControl() {
		return moveBackwardButton;
	}

	@Override
	public void enableDeleteControl(boolean enabled) {
		deleteButton.setEnabled(enabled);
		
	}

	@Override
	public void enableMoveBackwardControl(boolean enabled) {
		moveBackwardButton.setEnabled(enabled);
		
	}

	@Override
	public void enableMoveForwardControl(boolean enabled) {
		moveForwardButton.setEnabled(enabled);
	}

	@Override
	public void removeLayerEditor(GraphicEditor<? extends Graphic> editor) {
		(controlPanels.remove(editor.getLayer())).removeFromParent();
	}

	@Override
	public int indexOfLayer(Layer<? extends Graphic> layer) {
		return previewAbsolutePanel.getWidgetIndex( layer );
	}

	@Override
	public void removeLayer(Layer<? extends Graphic> layer) {
		layer.asWidget().removeFromParent();
	}

	@Override
	public void insertLayer(Layer<? extends Graphic> layer, int i, int left, int top) {
		previewAbsolutePanel.insert(layer.asWidget(), left, top, i) ;
	}

	@Override
	public boolean isLayerOnFront(Layer<? extends Graphic> layer) {
		return indexOfLayer( layer )  == previewAbsolutePanel.getWidgetCount() -1  ;		
	}

	@Override
	public void show() {
	}

	@Override
	public void hide() {
	}



}
