package com.dotemplate.core.client.canvas.ui;

import org.gwtbootstrap3.client.shared.event.ModalHideEvent;
import org.gwtbootstrap3.client.shared.event.ModalHideHandler;
import org.gwtbootstrap3.client.shared.event.ModalShowEvent;
import org.gwtbootstrap3.client.shared.event.ModalShowHandler;
import org.gwtbootstrap3.client.ui.Button;
import org.gwtbootstrap3.client.ui.Container;
import org.gwtbootstrap3.client.ui.Modal;
import org.gwtbootstrap3.client.ui.ModalHeader;

import com.dotemplate.core.client.canvas.Layer;
import com.dotemplate.core.client.canvas.LayerButton;
import com.dotemplate.core.client.canvas.presenter.GraphicEditor;
import com.dotemplate.core.client.canvas.view.CanvasEditorView;
import com.dotemplate.core.shared.canvas.Graphic;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Overflow;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;

public class CanvasEditorDialog extends Composite implements CanvasEditorView {

	private static CanvasEditorDialogUiBinder uiBinder = GWT
			.create(CanvasEditorDialogUiBinder.class);

	
	interface CanvasEditorDialogUiBinder extends UiBinder<Widget, CanvasEditorDialog> {
	}

	
	@UiField
	ModalHeader header ;
	
	@UiField
	Modal modal ;
	
	
	@UiField
	Container content ;
	
	
	@UiField
	Button applyButton ;
	
	@UiField
	Button cancelButton ;
	
	CanvasEditorPanel canvasEditorUI ;
	
	
	public CanvasEditorDialog() {	
		
		initWidget( uiBinder.createAndBindUi( this ) );
		
		canvasEditorUI = new CanvasEditorPanel() ;
		
		content.add( canvasEditorUI );
		
		// bootstrap fix
		modal.addHideHandler(new ModalHideHandler() {
			@Override
			public void onHide(ModalHideEvent evt) {
				RootPanel.getBodyElement().getStyle().setOverflow(Overflow.AUTO);
				
			}
		}) ;
		
		modal.addShowHandler(new ModalShowHandler() {
			
			@Override
			public void onShow(ModalShowEvent evt) {
				RootPanel.getBodyElement().getStyle().setOverflow(Overflow.AUTO);
				
			}
		});
		
		applyButton.addClickHandler(new ClickHandler(){

			@Override
			public void onClick(ClickEvent event) {
				modal.hide();				
			}
			
		});
		
		
		cancelButton.addClickHandler(new ClickHandler(){

			@Override
			public void onClick(ClickEvent event) {
				clear();
				modal.hide();
			}
			
		});
		

	}
	
	
	public CanvasEditorPanel getCanvasEditorUI() {
		return canvasEditorUI;
	}
	
	
	@Override
	public void show (){
		modal.show(); 
	}
	
	@Override
	public void hide() {
		modal.hide();
	}
	
	
	public HasClickHandlers getApplyControl() {
		return applyButton ; 
	}


	@Override
	public String getId() {
		return "canvasEditorDialog";
	}


	@Override
	public void setCanvasSize(int width, int height) {
		canvasEditorUI.setCanvasSize(width, height);
	}


	@Override
	public void clear() {
		canvasEditorUI.clear();
	}


	@Override
	public void addLayer(Layer<? extends Graphic> layer, int left, int top) {
		canvasEditorUI.addLayer(layer, left, top);
		
	}


	@Override
	public void addLayerButton(LayerButton<? extends Graphic> layerButton) {
		// TODO Auto-generated method stub
	}


	@Override
	public void addLayerEditor(GraphicEditor<? extends Graphic> editor) {
		canvasEditorUI.addLayerEditor(editor);
		
	}


	@Override
	public void setEditor(GraphicEditor<? extends Graphic> currentEditor) {
		canvasEditorUI.setEditor(currentEditor);
		
	}


	@Override
	public HasClickHandlers getAddGalleryImageControl() {
		return canvasEditorUI.getAddGalleryImageControl();
	}
	
	@Override
	public HasClickHandlers getAddFlickrImageControl() {
		return canvasEditorUI.getAddFlickrImageControl();
	}


	@Override
	public HasClickHandlers getUploadImageControl() {
		return canvasEditorUI.getUploadImageControl();
	}


	@Override
	public HasClickHandlers getAddLogoControl() {
		return canvasEditorUI.getAddLogoControl();
	}


	@Override
	public HasClickHandlers getAddTextControl() {
		return canvasEditorUI.getAddTextControl();
	}


	@Override
	public HasClickHandlers getDeleteControl() {
		return canvasEditorUI.getDeleteControl();
	}


	@Override
	public HasClickHandlers getMoveForwardControl() {
		return canvasEditorUI.getMoveForwardControl();
	}


	@Override
	public HasClickHandlers getMoveBackwardControl() {
		return canvasEditorUI.getMoveBackwardControl();
	}


	@Override
	public void enableDeleteControl(boolean enable) {
		canvasEditorUI.enableDeleteControl(enable);
		
	}


	@Override
	public void enableMoveBackwardControl(boolean enable) {
		canvasEditorUI.enableMoveBackwardControl(enable);
	}


	@Override
	public void enableMoveForwardControl(boolean enable) {
		canvasEditorUI.enableMoveForwardControl(enable);
	}


	@Override
	public void removeLayerEditor(GraphicEditor<? extends Graphic> currentEditor) {
		canvasEditorUI.removeLayerEditor(currentEditor);
	}


	@Override
	public int indexOfLayer(Layer<? extends Graphic> layer) {
		return canvasEditorUI.indexOfLayer(layer);
	}


	@Override
	public void removeLayer(Layer<? extends Graphic> layer) {
		canvasEditorUI.removeLayer(layer);
	}


	@Override
	public void insertLayer(Layer<? extends Graphic> layer, int i, int left, int top) {
		canvasEditorUI.insertLayer(layer, i, left, top);
		
	}


	@Override
	public boolean isLayerOnFront(Layer<? extends Graphic> layer) {
		return canvasEditorUI.isLayerOnFront(layer);
	}



	
}
