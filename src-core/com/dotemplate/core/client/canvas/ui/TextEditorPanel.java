package com.dotemplate.core.client.canvas.ui;

import java.util.List;

import org.gwtbootstrap3.client.ui.TextBox;
import org.gwtbootstrap3.client.ui.form.validator.Validator;

import com.dotemplate.core.client.canvas.view.TextEditorView;
import com.dotemplate.core.client.frwk.Utils;
import com.dotemplate.core.shared.canvas.TextGraphic;
import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.Editor;
import com.google.gwt.editor.client.EditorError;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

public class TextEditorPanel extends Composite implements TextEditorView {

	private static TextEditorPanelUiBinder uiBinder = GWT
			.create(TextEditorPanelUiBinder.class);

	private TextGraphic text ;

	private FontSizeEditorControl fontSizeControl;
	
	private SVGFontEditorControl svgFontControl ;
	
	
	@UiField
	SimplePanel subEditorsContainer;
	
	@UiField
	SimplePanel textStyleEditorsContainer ;
	
	
	@UiField
	FlowPanel fontSettingsContainer ;
	
	@UiField
	TextBox textInput ;
	
	
	interface TextEditorPanelUiBinder extends UiBinder<Widget, TextEditorPanel> {
	}
	
	public TextEditorPanel( TextGraphic text ) {
		
		this.text = text ;
		
		initWidget(uiBinder.createAndBindUi( this ) );

		fontSizeControl = new FontSizeEditorControl() ;	   	
	   	
		svgFontControl = new SVGFontEditorControl() ;
		
	   	fontSettingsContainer.add( fontSizeControl );	   	
	   	
	   	fontSettingsContainer.add( svgFontControl );	   	
		
	   	
	   	textInput.addChangeHandler (new ChangeHandler() {
			@Override
			public void onChange( ChangeEvent e ) {
				e.getNativeEvent().stopPropagation();
				
			}
		});
        
	   	textInput.addKeyDownHandler(new KeyDownHandler(){ 
			public void onKeyDown( KeyDownEvent event) {
				if ( KeyCodes.KEY_ENTER == event.getNativeKeyCode() ){
					ValueChangeEvent.fire( textInput, textInput.getValue() );
				}
			}
		}) ;
	}
	

	@Override
	public String getId() {
		return "textEditor_" + text.getName()  ;
	}

	
	@Override
	public void setSubEditorViews(Widget subEditors) {
		subEditorsContainer.setWidget( subEditors );
	}

	@Override
	public HasValue<Integer> getFontSizeSelector() {
		return fontSizeControl;
	}


	@Override
	public HasValue<String> getTextInput() {
		return textInput;
	}


	@Override
	public HasClickHandlers getFontFamilyControl() {
		return svgFontControl;
	}

	@Override
	public void setStyleEditorView(Widget styleEditor) {
		textStyleEditorsContainer.setWidget( styleEditor );
	}


	@Override
	public boolean validateTextInput() {

		// FIXME validation, on blur...,
		String text = textInput.getValue() ;
		
		if ( text == null || text.length() == 0 ){
			
			Utils.alert ( "Please, enter a valid text." ) ;
			
			textInput.selectAll() ;
			textInput.setFocus( true ) ;
			return false ;
		}
		
		return true;
	}


}
