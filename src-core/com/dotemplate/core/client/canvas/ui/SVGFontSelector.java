package com.dotemplate.core.client.canvas.ui;

import java.util.LinkedHashMap;

import com.dotemplate.core.client.widgets.resource.DesignResourceChangeHandler;
import com.dotemplate.core.client.widgets.resource.DesignResourceDialog;
import com.dotemplate.core.client.widgets.resource.DesignResourceLoadAdapter;
import com.dotemplate.core.client.widgets.resource.DesignResourceProvider;
import com.dotemplate.core.client.widgets.resource.DesignResourceScrollSelector;
import com.dotemplate.core.client.widgets.resource.DesignResourceSelector;
import com.dotemplate.core.shared.SVGFont;
import com.dotemplate.core.shared.command.GetDesignResourcesMap;
import com.dotemplate.core.shared.command.GetSVGFonts;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;


public class SVGFontSelector extends DesignResourceDialog < SVGFont >  {

	private static SVGFontSelector instance ;
	
	private static LinkedHashMap < String, SVGFont > fontsMap ;
	
	public SVGFontSelector() {
		
		super( new DesignResourceProvider< SVGFont >() {
			
			@Override
			protected GetDesignResourcesMap< SVGFont > getDesignResourcesMapCommand() {
				return new GetSVGFonts() ;
			}
		
		}, 600, 500 ) ;
		
		setHeading( "Choose font" ) ;
		
	}
	
		
	public static void show( DesignResourceChangeHandler< SVGFont > callback, String selectedFamily  ){
		
		if ( instance == null ){
			instance = new SVGFontSelector() ;
		}
		
		instance.open( selectedFamily, callback ) ;
		
	}
	
	
	
	public void open( final String fontFamily , final DesignResourceChangeHandler< SVGFont > callback ) {
		
		if ( !resourceSelector.isLoaded() ){
			
			resourceProvider.addLoadHandler( new DesignResourceLoadAdapter<SVGFont>(){
				@Override
				public void onDesignResourcesLoad(
						LinkedHashMap<String, LinkedHashMap<String, SVGFont > > tags) {
					
					
					( ( DesignResourceScrollSelector< SVGFont > ) resourceSelector ) .setResources( tags ) ;
					
					fontsMap = tags.values().iterator().next() ;
										
					open( fontsMap.get( fontFamily ), callback  ) ;
					
					
				}
			});
			
			resourceProvider.loadResourcesMap() ;
			
			
		} else {
			
			open( fontsMap.get( fontFamily ), callback  ) ;
			
		}
	}
	
	
	
	
	public void open( final SVGFont selected, DesignResourceChangeHandler< SVGFont > callback ) {
		
		this.callback = callback ;

		resourceSelector.setValue( selected ) ;
		
		open() ;				
		
	}
	

	
	

	@Override
	protected DesignResourceSelector < SVGFont > createResourceSelector() {
		
		DesignResourceSelector < SVGFont > selector = new DesignResourceScrollSelector< SVGFont >() {
			
			
			@Override
			public HandlerRegistration addValueChangeHandler(
					ValueChangeHandler<SVGFont> handler) {
				return handlerManager.addHandler( ValueChangeEvent.getType(), handler );
			}
						
		};
		
		selector.asWidget().addStyleName( "ez-svgfont-selector" ) ;
		
		return selector ;
	
	}
	

	
}
