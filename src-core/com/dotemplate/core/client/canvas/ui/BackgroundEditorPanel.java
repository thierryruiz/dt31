package com.dotemplate.core.client.canvas.ui;

import com.dotemplate.core.client.canvas.view.BackgroundEditorView;
import com.dotemplate.core.shared.canvas.BackgroundGraphic;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public class BackgroundEditorPanel extends Composite implements BackgroundEditorView {

	private static BackgroundEditorPanelUiBinder uiBinder = GWT
			.create(BackgroundEditorPanelUiBinder.class);

	private BackgroundGraphic background ;
	
	
	interface BackgroundEditorPanelUiBinder extends
			UiBinder<Widget, BackgroundEditorPanel> {
	}

	public BackgroundEditorPanel( BackgroundGraphic background ) {
		this.background = background ;
		initWidget(uiBinder.createAndBindUi(this));
	}

	@Override
	public String getId() {
		return "background_" + background.getName() ;
	}

}
