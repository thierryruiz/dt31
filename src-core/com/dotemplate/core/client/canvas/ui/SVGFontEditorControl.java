package com.dotemplate.core.client.canvas.ui;

import org.gwtbootstrap3.client.ui.Button;
import org.gwtbootstrap3.client.ui.constants.ButtonType;

import com.dotemplate.core.client.editors.PropertyEditorControl;

import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Widget;


public class SVGFontEditorControl extends PropertyEditorControl implements HasClickHandlers  {

	protected Button chooseButton ;
	

	public SVGFontEditorControl() {
		
	   	getLabel().setText( "Choose Font..." ) ;
	   	
	   	// FIXME
	   	//getIcon().setUrl("client/icons/CanvasTextEditor/font-family.png" ) ;
	   	
	   	getControlContainer().setWidget( createControl() ) ;	   	
	   	
	   	
	}
	

	
	@Override
	protected Widget createControl() {
		
		chooseButton = new Button("Choose...") ;
		chooseButton.setType( ButtonType.PRIMARY );
		chooseButton.addStyleName("dt-mutable-property-button");
		return chooseButton;
	}
	
	
	
	
	@Override
	public void disable() {
		// TODO Auto-generated method stub				
	}
	
	@Override
	public void enable() {
		// TODO Auto-generated method stub
	}



	@Override
	public HandlerRegistration addClickHandler(ClickHandler handler) {
		return chooseButton.addClickHandler(handler);
	}



	@Override
	protected Widget createIcon() {
		// TODO Auto-generated method stub
		return null;
	}


}
