package com.dotemplate.core.client.canvas.ui;

import java.util.ArrayList;

import com.dotemplate.core.client.canvas.presenter.ImageResourcePagingAsyncProvider;
import com.dotemplate.core.client.canvas.view.ImageGalleryView;
import com.dotemplate.core.client.widgets.ListBox;
import com.dotemplate.core.shared.canvas.ImageResource;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;



public class ImageGallery extends Composite implements ImageGalleryView {

	@UiField
	FlowPanel imageSelectorPlaceHolder ;
	
	@UiField
	FlowPanel tagSelectorPlaceHolder ;
	
	protected TagListBox tagSelector ;
	
	protected ImageGalleryPanel imageGalleryPanel ;
	
	private static ImageGalleryUiBinder uiBinder = GWT
			.create(ImageGalleryUiBinder.class);

	
	interface ImageGalleryUiBinder extends UiBinder<Widget, ImageGallery> {
	}

	
	public ImageGallery( ImageResourcePagingAsyncProvider provider ) {
		
		initWidget( uiBinder.createAndBindUi( this ) );
	
		imageGalleryPanel = new ImageGalleryPanel( provider ) ;
	
		imageSelectorPlaceHolder.add( imageGalleryPanel ) ;

		tagSelector = new TagListBox() ;
		
		tagSelectorPlaceHolder.add( tagSelector );
		
		
	}

	
	
	@Override
	public String getId() {
		return "imageGallery" ;
	}

	
	@Override
	public void selectTag( final String tag ){
		
		Scheduler.get().scheduleDeferred(new ScheduledCommand() {
			@Override
			public void execute() {
				tagSelector.setSelected( tag );
			}
		});

	}

	
	@Override
	public void setTags( String[] tags ) {
		
		for ( int i = 0 ; i < tags.length ; i++  ){
			tagSelector.add( tags[ i ] );
		}
		
		tagSelector.refresh();
				
	}
	
	
	public void setUnlockedImages(ArrayList<String> unlockedImages) {
		imageGalleryPanel.setUnlockedImages(unlockedImages);
	}
	
	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<ImageResource> handler) {
		return imageGalleryPanel.addValueChangeHandler( handler );
	}
	
		
	@Override
	public void clearResults() {
		imageGalleryPanel.clearResults() ;		
	}
	
	
	@Override
	public HandlerRegistration addSelectTagHandler(ChangeHandler handler) {
		return tagSelector.addChangeHandler( handler );
	}



	@Override
	public String getSelectedTag() {
		return tagSelector.getSelected() ;
	}
	
	
	class TagListBox extends ListBox < String > {
		
		@Override
		protected String dataAsString( String data ) {
			return data ;
		}
		
		@Override
		protected String uuid(String data) {
			return data ;
		}
		
	}


	
	@Override
	public ImageResource getValue() {
		return imageGalleryPanel.getValue();
	}



	@Override
	public void setValue(ImageResource value) {
		imageGalleryPanel.setValue(value);
	}



	@Override
	public void setValue(ImageResource value, boolean fireEvents) {
		imageGalleryPanel.setValue(value, fireEvents );
	}
	
	
}
