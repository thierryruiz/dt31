package com.dotemplate.core.client.canvas.ui;



import java.util.ArrayList;

import com.dotemplate.core.client.canvas.presenter.ImageResourcePagingAsyncProvider;
import com.dotemplate.core.client.widgets.resource.DesignResourcePageSelector;
import com.dotemplate.core.shared.canvas.ImageResource;
import com.google.gwt.dom.client.Style.Overflow;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;


public class ImageGalleryPanel extends DesignResourcePageSelector< ImageResource >  {
		
		
	protected ArrayList< String  > unlockedImages = new ArrayList< String >() ;
	
	
	public ImageGalleryPanel ( ImageResourcePagingAsyncProvider provider ) {
		
		super ( provider ) ;		
				
		layout.setHeight("600px");
		
		list.asWidget().setHeight("475px");
		
		list.asWidget().getElement().getStyle().setOverflowY(Overflow.AUTO );
			
	}

	
	
	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<ImageResource> handler) {
		return handlerManager.addHandler( ValueChangeEvent.getType(), handler );
	}




	
	@Override
	protected void setResources( ArrayList< ImageResource > resources,
			boolean hasMore) {
		
		for( ImageResource ir : resources ){
			for ( String img : unlockedImages ){
				if ( ir.isPremium() && ir.getPath().equals( img ) ){
					ir.setFree( true ) ;
				}
			}
		}
		
		super.setResources(resources, hasMore);

	}
	
	public void clearResults() {
		list.clearList(); 		
	}

	
	
	protected void setUnlockedImages( ArrayList< String > unlockedImages ) {
		this.unlockedImages = unlockedImages;
	}

	
	@Override
	protected String loadMoreButtonLabel() {
		return "More Images" ;
	}


	

	
	
}
