package com.dotemplate.core.client.canvas.ui;

import com.dotemplate.core.client.editors.PropertyEditorControl;
import com.dotemplate.core.client.widgets.ListBox;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.Widget;


public class FontSizeEditorControl extends PropertyEditorControl 
	implements HasValue< Integer > {

	int[] range ;
	
	FontSizeListBox listbox ;
		
	boolean custom = false ;
	
	public FontSizeEditorControl( ) {
		
		this.range = new int[ 90 ] ;
		
		for ( int i = 0 ; i < 91 ; i++  ){
			this.range[ i ] = i+10 ;
	    }             
		
		
		
		// FIXME
	   	//getIcon().setUrl( "client/icons/CanvasTextEditor/font-size.png" ) ;
	   	
		getLabel().setText( "Text Size" ) ;
	   	
		getControlContainer().add( createControl() ) ;
		
	}
	
	
	
	@Override
	protected Widget createControl() {
		
		listbox = new FontSizeListBox()  ;
		
		for ( int v : range ){
    		
			listbox.add( v ) ;
			
		}
		
		listbox.addClickHandler( new ClickHandler() {
			@Override
			public void onClick( ClickEvent event ) {
				ValueChangeEvent.fire( FontSizeEditorControl.this, 
						( Integer ) event.getSource() );
			}
		}) ;
		
		
		return listbox ;
	
	}
	
	
	
	@Override
	protected void onLoad() {
		
		super.onLoad();
		
		listbox.refresh();
		
	}

	
	@Override
	public void enable() {
		// FIXME NG Auto-generated method stub
	}


	@Override
	public void disable() {
		// FIXME NG Auto-generated method stub
	}
	

	@Override
	public Integer getValue() {
		return listbox.getSelected();
	}


	@Override
	public void setValue(Integer value) {
		listbox.setSelected(value);
	}


	@Override
	public void setValue(Integer value, boolean fireEvents) {
		listbox.setSelected(value);
		ValueChangeEvent.fire( FontSizeEditorControl.this, value );
	}

	
	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<Integer> handler) {
		return super.addHandler( handler, ValueChangeEvent.getType() );
	}


	
	class FontSizeListBox extends ListBox < Integer > {

		public FontSizeListBox() {
			addStyleName( "dt-size-listBox" ) ;
		}
		
		
		@Override
		protected String dataAsString( Integer data ) {
			return data + "px" ;
		}
		
		@Override
		protected String uuid(Integer data) {
			return "" + data ;
		}
		
	}



	@Override
	protected Widget createIcon() {
		// TODO Auto-generated method stub
		return null;
	}

	
}
