package com.dotemplate.core.client.canvas.ui;

import org.gwtbootstrap3.client.shared.event.ShowHandler;
import org.gwtbootstrap3.client.ui.Button;
import org.gwtbootstrap3.client.ui.Collapse;
import org.gwtbootstrap3.client.ui.Heading;
import org.gwtbootstrap3.client.ui.PanelBody;
import org.gwtbootstrap3.client.ui.PanelHeader;

import com.dotemplate.core.client.canvas.Layer;
import com.dotemplate.core.client.canvas.event.LayerEvent;
import com.dotemplate.core.client.canvas.event.LayerEvent.EventType;
import com.dotemplate.core.client.widgets.StringInputDialog;
import com.dotemplate.core.client.widgets.StringInputDialog.Callback;
import com.dotemplate.core.shared.canvas.Graphic;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;


public class LayerControlPanel extends Composite  {

	private static LayerControlPanelUiBinder uiBinder = GWT
			.create(LayerControlPanelUiBinder.class);

	interface LayerControlPanelUiBinder extends UiBinder<Widget, LayerControlPanel> {
	}
	
	
	@UiField
	PanelHeader panelHeader;

	@UiField
	Heading heading;

	@UiField
	PanelBody body;

	@UiField
	Collapse collapse;
	
	@UiField
	Button renameButton ;
		
	@UiField
	Button deleteButton ;
	
	StringInputDialog renameDialog ;
	
	Layer<? extends Graphic> layer ; 

	
	public LayerControlPanel( final Layer<?> layer ) {
		
		this.layer = layer ;
		
		initWidget( uiBinder.createAndBindUi(this));
		
		panelHeader.setDataTargetWidget(collapse);
		panelHeader.setDataParent("#accordion");
		
		heading.setText( layer.getLabel() );
		
		renameButton.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				renameButton.getElement().blur();
				event.getNativeEvent().stopPropagation();
				
				openRenameDialog() ;
				
			}
		});
		
		
		deleteButton.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				deleteButton.getElement().blur();
				event.getNativeEvent().stopPropagation();
				
				layer.select();
				
				layer.fireDeleted();
			}
		});

		
	}
	

	public void addShowHandler( ShowHandler  showHandler ){
		collapse.addShowHandler(showHandler) ;
	}
	

	
	public void setId(String id) {
		this.getElement().setId(id);
	}
	
	
	
	public PanelBody getBody() {
		return body;
	}
	
	
	public void collapse() {
		if ( collapse.isShown() ){
			collapse.hide() ;
		}
	}
	
	
	public void expand() {
		if ( collapse.isHidden() ){
			collapse.show() ;
		}
	}
	
	
	
	protected void openRenameDialog(){
		
		if ( renameDialog == null ){
			renameDialog = new StringInputDialog( ) ;
			renameDialog.setHeading( "Rename Layer" ) ;
			renameDialog.setLabel( "Enter layer name"  ) ;
		}
		
		renameDialog.open( layer.getLabel(), renameDialogCallback ) ;		
	
	}

	
//	@Override
//	public HandlerRegistration addLayerEventHandler(LayerEventHandler h) {
//		return addHandler( h, LayerEvent.TYPE );
//	}
	
	
	Callback renameDialogCallback = new Callback(){
		public void onOk(String value) {
			
			heading.setText( value ) ;
			layer.getGraphic().setLabel( value );
			
			fireEvent( new LayerEvent( layer, EventType.RENAMED ) );
			
		}
		
	};
	
	
//	HasValue< String > layerName = new HasValue<String>() {
//		
//		private String value ;
//		
//		@Override
//		public HandlerRegistration addValueChangeHandler(
//				ValueChangeHandler<String> handler) {		
//			return LayerControlPanel.this.addHandler( handler , ValueChangeEvent.getType());
//		}
//
//		@Override
//		public void fireEvent(GwtEvent<?> event) {
//			LayerControlPanel.this.fireEvent(event);			
//		}
//
//		@Override
//		public String getValue() {
//			return value ;
//		}
//
//		@Override
//		public void setValue(String value) {
//			this.value = value ;
//			heading.setText(value);
//		}
//
//		@Override
//		public void setValue(String value, boolean fireEvents) {
//			this.value = value ;
//			heading.setText( value ) ;
//			if( fireEvents ){
//				ValueChangeEvent.fire( this, value);
//			}
//		}
//	};





}
