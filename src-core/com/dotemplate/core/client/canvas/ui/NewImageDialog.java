package com.dotemplate.core.client.canvas.ui;

import java.util.ArrayList;

import org.gwtbootstrap3.client.shared.event.ModalHideEvent;
import org.gwtbootstrap3.client.shared.event.ModalHideHandler;
import org.gwtbootstrap3.client.shared.event.ModalShowEvent;
import org.gwtbootstrap3.client.shared.event.ModalShowHandler;
import org.gwtbootstrap3.client.ui.Modal;
import org.gwtbootstrap3.client.ui.TabListItem;

import com.dotemplate.core.client.canvas.view.ImageGalleryView;
import com.dotemplate.core.client.canvas.view.NewImageView;
import com.dotemplate.core.client.canvas.view.StockImageSearchView;
import com.dotemplate.core.shared.canvas.ImageResource;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Overflow;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;


public class NewImageDialog extends Composite implements NewImageView {

	private static NewImageDialogUiBinder uiBinder = GWT
			.create( NewImageDialogUiBinder.class );
	
	
	interface NewImageDialogUiBinder extends
			UiBinder<Widget, NewImageDialog> {
	}


	@UiField
	Modal modal ;
	
	@UiField 
	FlowPanel imageGalleryPlaceHolder ;
	
	@UiField 
	FlowPanel flickrPlaceHolder ;
	
	
	@UiField 
	TabListItem flickrTabListItem ;
	
	@UiField 
	TabListItem imageGalleryTabListItem ;

	
	
	ImageGalleryView galleryView ;
	
	StockImageSearchView flickSearchView ;
	
	
	public NewImageDialog( ) {
		
		initWidget( uiBinder.createAndBindUi(this) );
		
		// bootstrap fix
		modal.addHideHandler(new ModalHideHandler() {
			@Override
			public void onHide(ModalHideEvent evt) {
				RootPanel.getBodyElement().getStyle().setOverflow(Overflow.AUTO);
				
			}
		}) ;
		
		modal.addShowHandler(new ModalShowHandler() {
			
			@Override
			public void onShow(ModalShowEvent evt) {
				RootPanel.getBodyElement().getStyle().setOverflow(Overflow.AUTO);
				
			}
		});
		
	}

	
	@Override
	public void setImageGalleryView( ImageGalleryView view  ) {
		imageGalleryPlaceHolder.add( galleryView = view );
	}

	@Override
	public void setFlickImageView( StockImageSearchView view ) {
		flickrPlaceHolder.add ( flickSearchView = view ) ;
	}
	

	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<ImageResource> handler) {
		return galleryView.addValueChangeHandler( handler );
	}


	public void setUnlockedImages( ArrayList< String > unlockedImages ) {
		galleryView.setUnlockedImages( unlockedImages ) ;
	}

	
	@Override
	public void hide() {
		modal.hide();	
	}

	@Override
	public String getId() {
		return "newImageDialog" ;
	}
	

	@Override
	public void show(InitialState state) {
		
		modal.show();
		
		if( state == InitialState.FLICKR_IMAGE ){
			flickrTabListItem.showTab(true);
		}
		
		if( state == InitialState.GALLERY_IMAGE ){
			imageGalleryTabListItem.showTab(true);
		}
		
		
		if( state == InitialState.LOGO ){
			imageGalleryTabListItem.showTab(true);
		}
		
	}









	

}
