package com.dotemplate.core.client.canvas.ui;

import java.util.ArrayList;

import org.gwtbootstrap3.client.ui.Button;
import org.gwtbootstrap3.client.ui.TextBox;

import com.dotemplate.core.client.apis.StockImage;
import com.dotemplate.core.client.canvas.event.SelectStockImageHandler;
import com.dotemplate.core.client.canvas.view.StockImageSearchView;
import com.dotemplate.core.client.widgets.resource.SelectStockImage;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;


public class StockImageSearchPanel extends Composite implements StockImageSearchView {

	private static StockImageSearchPanelUiBinder uiBinder = GWT
			.create(StockImageSearchPanelUiBinder.class);
	
	
	interface StockImageSearchPanelUiBinder extends
		UiBinder<Widget, StockImageSearchPanel> {
	}

	
	@UiField
	TextBox searchTextBox;
	
	@UiField
	FlowPanel imageContainer ;
	
	@UiField
	Button moreImageButton ;
	
	
	SearchInputChangeHandler searchInputChangeHandler ;

	
	public StockImageSearchPanel() {
		
		searchInputChangeHandler = new SearchInputChangeHandler() ;
		
		initWidget(uiBinder.createAndBindUi(this));
		
		searchTextBox.addKeyDownHandler(new KeyDownHandler() {
			
			  public void onKeyDown( KeyDownEvent event ) {
				  
				  if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
					  
					  searchInputChangeHandler.fireEvent(  new ValueChangeEvent<String>(
								searchTextBox.getText()){} );

					  DOM.eventCancelBubble( DOM.eventGetCurrentEvent(), true );
					  
			      }
			  }
		}) ;
		
		
	}


	@Override
	public String getId() {
		return "searchImagePanel" ;
	}

	@Override
	public HasValueChangeHandlers< String > getSearchInput() {
		return searchInputChangeHandler;
	}

	@Override
	public String getSearchSring() {
		return searchTextBox.getText() ;
	}

	
	@Override
	public void setPageImages( ArrayList<StockImage> images ) {

		for ( StockImage apiImage : images ){
			
			SimplePanel wpr = new SimplePanel() ;
			
			wpr.setStyleName( "ez-chooser-item" ) ;
			
			Image image = new Image() ;
			
			image.setHeight("120px");
			image.setUrl( apiImage.getThumbnailUrl() );
			wpr.setWidget(image);
			
			imageContainer.add(  wpr );
			
			final StockImage stockImage = apiImage ; 
			
			image.addClickHandler( new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					fireEvent( new SelectStockImage( stockImage, null ) );
				}
			} ) ;
						
		}
		
		scrollToEnd(imageContainer.getElement());
		
	}

	

	class SearchInputChangeHandler implements HasValueChangeHandlers<String > {

		HandlerManager handlerManager = new HandlerManager( null ) ;
		
	
		@Override
		public void fireEvent(GwtEvent<?> event) {
			handlerManager.fireEvent(event);
		}

		@Override
		public HandlerRegistration addValueChangeHandler(
				ValueChangeHandler<String> handler) {
			return handlerManager.addHandler(ValueChangeEvent.getType(), handler );
		}
		
	}

	@Override
	public void setSearchPlaceHolder(String placeholder) {
		searchTextBox.setPlaceholder(placeholder);
	}

	@Override
	public HasClickHandlers getMoreImageControl() {
		return moreImageButton  ;
	}

	@Override
	public void showMoreImageControl(boolean show) {
		moreImageButton.setVisible(show);
	}

	
	@Override
	public void clearImages() {
		imageContainer.clear();
	}

	@Override
	public HandlerRegistration addHandler(SelectStockImageHandler h) {
		return this.addHandler( h, SelectStockImage.TYPE );
	}	
	
	private static final native void scrollToEnd( Element e ) /*-{
		 $wnd.jQuery(e).stop().animate({
		  	scrollTop: $wnd.jQuery(e)[0].scrollHeight
		}, 800);
	}-*/;
	
}
