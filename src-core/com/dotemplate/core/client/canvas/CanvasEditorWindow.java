package com.dotemplate.core.client.canvas;

import java.util.ArrayList;

import com.dotemplate.core.client.widgets.DeprecatedWindow;
import com.dotemplate.core.shared.properties.CanvasProperty;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;

@Deprecated
public class CanvasEditorWindow extends DeprecatedWindow implements ResizeHandler {
		
	private CanvasEditor canvasEditor ;
	
	
	public CanvasEditorWindow () {

		innerWnd.setStyleName( "ez-drawboard" ) ;
		setModal ( true ) ;
		setCollapsible ( false ) ;
		setClosable ( true ) ;
		//setResizable(false) ;
		
		innerWnd.setWidth( "1017px" ) ;
		
		
		int winHeight = CanvasEditorToolbar.HEIGHT + CanvasEditor.PREVIEW_PANEL_HEIGHT + CanvasEditor.LAYERS_PANEL_HEIGHT  + 50 ; 
		
		innerWnd.setHeight( winHeight +  "px" ) ;		
		
		canvasEditor = new CanvasEditor() ;
		
		com.google.gwt.user.client.Window.addResizeHandler ( this ) ;
				
		add( canvasEditor.asWidget() ) ;
		
		
		canvasEditor.getCancelButton().addClickHandler( new ClickHandler( ) {
			@Override
			public void onClick(ClickEvent arg0) {
				canvasEditor.cancel() ;
				close() ;
			}
		});		

		canvasEditor.getApplyButton().addClickHandler( new ClickHandler( ) {
			@Override
			public void onClick(ClickEvent arg0) {
				
				close() ;
			}
		});		

				

	}
	
	
	
	public void edit( CanvasProperty canvas ) {
		
		setHeading ( canvas.getLabel() ) ;
		
		openCenter() ;
		
		canvasEditor.edit ( canvas ) ;
		
		
	}
	
	
	@Override
	public void onResize ( ResizeEvent e ) {
		innerWnd.center() ;
	}



	public void setUnlockedImages(ArrayList< String > unlockedImages) {
		canvasEditor.setUnlockedImages( unlockedImages ) ;
	}


	
}
