package com.dotemplate.core.client.canvas.presenter;

import com.dotemplate.core.client.canvas.ui.ImageGallery;
import com.dotemplate.core.client.canvas.view.ImageGalleryView;
import com.dotemplate.core.client.frwk.AbstractPresenter;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;


public class ImageGallerySelector extends AbstractPresenter< ImageGalleryView > {
	
	ImageResourcePagingAsyncProvider provider ;
	
	public ImageGallerySelector( ImageResourcePagingAsyncProvider galleryProvider ) {
		
		super ( new ImageGallery( galleryProvider ) ) ;
		
		provider = galleryProvider ;
		
		view.addSelectTagHandler( new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent e) {
				filter ( view.getSelectedTag() );
			}
		} ) ;
		
		
	}



	@Override
	public void bind() {
	}

	
	
	public void filter( String tag ) {
		
		view.clearResults();
		
		view.selectTag( tag )  ;
		
		provider.setTag( tag ) ;
		provider.reset() ;
	
		provider.loadResources() ;
		
	}
	
	




}
