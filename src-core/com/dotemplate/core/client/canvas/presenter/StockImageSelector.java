package com.dotemplate.core.client.canvas.presenter;

import java.util.ArrayList;

import com.dotemplate.core.client.apis.StockImage;
import com.dotemplate.core.client.apis.StockImageAPI;
import com.dotemplate.core.client.apis.StockImageDetails;
import com.dotemplate.core.client.canvas.event.HasSelectStockImageHandlers;
import com.dotemplate.core.client.canvas.event.SelectStockImageHandler;
import com.dotemplate.core.client.canvas.view.StockImageSearchView;
import com.dotemplate.core.client.frwk.AbstractPresenter;
import com.dotemplate.core.client.frwk.Utils;
import com.dotemplate.core.client.widgets.resource.SelectStockImage;
import com.google.gwt.core.client.Callback;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.event.shared.HandlerRegistration;

public class StockImageSelector extends AbstractPresenter< StockImageSearchView >
	implements SelectStockImageHandler, HasSelectStockImageHandlers {
	
	protected HandlerManager handlerManager = new HandlerManager(this) ;

	protected StockImageAPI api ;

	int page = 0 ;
	

	public StockImageSelector( StockImageSearchView view, StockImageAPI api ) {
		
		super( view ) ;
		
		this.api = api ;
	}
	
	
	@Override
	public void bind() {
		
		view.getSearchInput().addValueChangeHandler(new ValueChangeHandler<String>() {
			
			@Override
			public void onValueChange( ValueChangeEvent<String> event ) {
				page = 0 ;
				view.clearImages();
				view.showMoreImageControl( false );
				searchAndDisplayImages() ;
				
			}
		}) ;
		
		
		view.getMoreImageControl().addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				page++;
				searchAndDisplayImages() ;
			}
		});
		
		view.addHandler( this ) ;
		
		
	}
	

	protected void searchAndDisplayImages() {
		
		String search = view.getSearchSring() ;
		
		final int pageSize = 25 ;
		
		api.search( search, page, pageSize, new Callback< ArrayList<StockImage>, String>() {
			
			@Override
			public void onSuccess( ArrayList<StockImage> results ) {			
				view.setPageImages( results );
				
				Utils.log( "" + results.size() ) ;
				view.showMoreImageControl( pageSize == results.size() );
			}
			
			@Override
			public void onFailure(String reason) {
				Utils.alert( "Sorry. Search image error.");				
			}
		} ) ;

	}


	@Override
	public void onStockImageSelected( final StockImage image, StockImageDetails details ) {
		
		api.getImageDetails( image, new Callback< StockImageDetails , String>() {
			
			@Override
			public void onSuccess( StockImageDetails result ) {
				fireEvent( new SelectStockImage(image, result ) );
			}
			
			@Override
			public void onFailure(String reason) {
				Utils.alert( "Sorry. Search image error.");				
			}
		} ) ;
		
		
	}


	@Override
	public void fireEvent(GwtEvent<?> event) {
		handlerManager.fireEvent( event );
	}


	@Override
	public HandlerRegistration addHandler(SelectStockImageHandler h) {
		return handlerManager.addHandler(SelectStockImage.TYPE, h);
	}

}
