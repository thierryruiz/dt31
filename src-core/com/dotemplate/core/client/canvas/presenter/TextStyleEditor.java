package com.dotemplate.core.client.canvas.presenter;

import com.dotemplate.core.client.editors.MutablePropertySetEditor;
import com.dotemplate.core.client.widgets.resource.DesignResourceProvider;
import com.dotemplate.core.shared.command.GetDesignResourcesMap;
import com.dotemplate.core.shared.command.GetSymbols;
import com.dotemplate.core.shared.properties.PropertySet;


public class TextStyleEditor extends MutablePropertySetEditor {
	
	public TextStyleEditor( PropertySet set ) {
		super( set);	
	}
	
	
	@Override
	protected void createWidget() {
		
		super.createWidget();
		
		mutableControl.getLabel().setText( "Text styles..." ) ;
	
	}
	

	protected void chooseSymbol() {
		
		if ( symbolSelector == null ){
			
			DesignResourceProvider< PropertySet > symbolProvider = new DesignResourceProvider< PropertySet >() {
				@Override
				protected GetDesignResourcesMap< PropertySet > getDesignResourcesMapCommand() {
					
					GetSymbols getSymbols = new GetSymbols() ;
					
					getSymbols.setType ( property.getSymbolType () ) ;
					getSymbols.setFilter ( property.getFilter () ) ;
					
					return getSymbols ;
				
				}
			};
			
			symbolSelector = new TextStyleSelector( symbolProvider ) ;
		
		}
		
		symbolSelector.open( property, this ) ;
	
	}
	
	

}
