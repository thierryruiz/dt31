package com.dotemplate.core.client.canvas.presenter;


import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.dotemplate.core.client.Client;
import com.dotemplate.core.client.apis.StockImage;
import com.dotemplate.core.client.apis.StockImageDetails;
import com.dotemplate.core.client.canvas.Layer;
import com.dotemplate.core.client.canvas.event.LayerEvent;
import com.dotemplate.core.client.canvas.event.LayerEventHandler;
import com.dotemplate.core.client.canvas.event.SelectStockImageHandler;
import com.dotemplate.core.client.canvas.ui.BackgroundEditorPanel;
import com.dotemplate.core.client.canvas.ui.ImageEditorPanel;
import com.dotemplate.core.client.canvas.ui.NewImageDialog;
import com.dotemplate.core.client.canvas.ui.TextEditorPanel;
import com.dotemplate.core.client.canvas.view.CanvasEditorView;
import com.dotemplate.core.client.canvas.view.NewImageView.InitialState;
import com.dotemplate.core.client.frwk.AbstractPresenter;
import com.dotemplate.core.client.frwk.CommandCallback;
import com.dotemplate.core.client.frwk.RPC;
import com.dotemplate.core.client.frwk.Utils;
import com.dotemplate.core.client.widgets.resource.DesignResourceChangeHandler;
import com.dotemplate.core.client.widgets.upload.UploadEvent;
import com.dotemplate.core.client.widgets.upload.UploadEventHandler;
import com.dotemplate.core.client.widgets.upload.UploadImageDialog;
import com.dotemplate.core.shared.canvas.BackgroundGraphic;
import com.dotemplate.core.shared.canvas.Graphic;
import com.dotemplate.core.shared.canvas.ImageGraphic;
import com.dotemplate.core.shared.canvas.ImageResource;
import com.dotemplate.core.shared.canvas.TextGraphic;
import com.dotemplate.core.shared.command.GetGraphics;
import com.dotemplate.core.shared.command.GetGraphicsResponse;
import com.dotemplate.core.shared.command.NewGraphicDone;
import com.dotemplate.core.shared.command.NewGraphicResponse;
import com.dotemplate.core.shared.command.NewImageGraphic;
import com.dotemplate.core.shared.command.NewTextGraphic;
import com.dotemplate.core.shared.properties.CanvasProperty;
import com.dotemplate.core.shared.properties.Property;
import com.dotemplate.core.shared.properties.PropertySet;
import com.dotemplate.core.shared.properties.TextProperty;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;



public class CanvasEditor extends AbstractPresenter< CanvasEditorView >
	implements LayerEventHandler {
	
	protected static Logger logger = Logger.getLogger( CanvasEditor.class.getName() );

	public CanvasEditor( CanvasEditorView view ) {
		super(view);
	}

	protected PropertySet canvasProperty ;
		
	protected ArrayList< Graphic > graphics ;
	
	protected int canvasWidth = 0 ;
	
	protected int canvasHeight = 0 ;
	
	protected GraphicEditor<? extends Graphic> currentEditor ;
	
	protected NewImageSelector newImageSelector ;	
	
	protected UploadImageDialog addUploadImageDialog ;
	
	protected ArrayList< String  > unlockedImages ; 
	
	protected BackgroundGraphic bgGraphic;
	
	
	
	@Override
	public void bind() {
		
		view.getAddTextControl().addClickHandler( new ClickHandler() {
			@Override
			public void onClick( ClickEvent e ) {
				newText() ;
			}
		}) ;

		
		view.getAddGalleryImageControl().addClickHandler( new ClickHandler() {
			@Override
			public void onClick( ClickEvent e ) {
				newGalleryImage() ;
			}
		}) ;
		
		
		view.getAddFlickrImageControl().addClickHandler( new ClickHandler() {
			@Override
			public void onClick( ClickEvent e ) {
				newFlickrImage() ;
			}
		}) ;
		
		view.getAddLogoControl().addClickHandler( new ClickHandler() {
			@Override
			public void onClick( ClickEvent e ) {
				newGalleryLogo() ;
			}
		}) ;
		
		
		view.getUploadImageControl().addClickHandler( new ClickHandler() {
			@Override
			public void onClick( ClickEvent e ) {
				newUploadImage() ;
			}
		}) ;
		
		
		view.getDeleteControl().addClickHandler(new ClickHandler() {
			@Override
			public void onClick( ClickEvent e ) {
				deleteGraphic() ;
			}
		}) ;
		
		
		view.getMoveBackwardControl().addClickHandler(new ClickHandler() {
			@Override
			public void onClick( ClickEvent e ) {
				moveSelectedLayerBackward() ;
			}
		}) ;
		
		
		view.getMoveForwardControl().addClickHandler( new ClickHandler() {
			@Override
			public void onClick( ClickEvent e ) {
				moveSelectedLayerForward() ;
			}
		}) ;
		
	}
	
	
	public void clear() {
		view.clear();
	}
	
	
	
	
	public void edit( final CanvasProperty set ) {
		
		this.canvasProperty = set ;
		
		canvasWidth = ( Integer ) canvasProperty.get( "width" ) ;
		canvasHeight = ( Integer ) canvasProperty.get( "height" )  ;

		GetGraphics getGraphics = new GetGraphics() ;
		
		getGraphics.setDesignUid( Client.get().getDesign().getUid() ) ;
		getGraphics.setDrawableId ( set.getUid () ) ;
		
		logger.log( Level.INFO, "Set canvas size " + canvasWidth + ", " + canvasHeight  );
	
		view.setCanvasSize ( canvasWidth, canvasHeight ) ;	
	
		logger.log( Level.INFO, "Loading graphics..." ) ;
		
		
		new RPC< GetGraphicsResponse >( Client.get().getDesignService() ).execute ( 
			getGraphics, new  CommandCallback< GetGraphicsResponse >() {

				@Override
				public void onSuccess ( GetGraphicsResponse response ) {
					initGraphics( response.getGraphics() ) ;
				}
		} ) ;
	
	}
	

	
	protected void initGraphics (  ArrayList<Graphic> graphics ){
		
		view.clear() ;
		
		this.graphics = graphics ; 
		
		Layer<? extends Graphic> firstLayer = null ;
			
		for ( Graphic graphic : graphics ){

			if ( !graphic.getVisible().booleanValue() ){
				continue;
			}
			
			if ( firstLayer == null ){
				
				firstLayer = addGraphicEditor( graphic ).getLayer() ;

				
			} else {
				
				addGraphicEditor( graphic ) ;
			}
			
		}
		
		if ( firstLayer != null ){
			firstLayer.select() ;
		}
		
	}
	
	public void saveGraphics (){
		
		for ( Graphic graphic : graphics ){
			canvasProperty.getPropertyMap().remove( ( Property ) graphic ) ;
			canvasProperty.addProperty( ( Property ) graphic ) ;
		}
		
		Client.get().updateProperty( canvasProperty ) ;
		
	}
	
	@SuppressWarnings( "unchecked" )
	protected GraphicEditor<? extends Graphic> addGraphicEditor ( Graphic graphic ){
		
		
		final GraphicEditor<? extends Graphic> gEditor ;
				
		switch ( graphic.getType () ) {
		
			case Graphic.IMAGE :
				gEditor= new ImageEditor( ( ImageGraphic ) graphic, 
						new ImageEditorPanel( ( ImageGraphic ) graphic ) ) ;
				
				break;
			
			case Graphic.TEXT :
				gEditor = new TextEditor( ( TextGraphic ) graphic, 
						new TextEditorPanel( ( TextGraphic ) graphic ) ) ;
				break;
	
			case Graphic.BG :
				gEditor = new BackgroundEditor( ( BackgroundGraphic ) graphic, 
						new BackgroundEditorPanel( ( BackgroundGraphic ) graphic) ) ;	
				break;				
				
			default :
				return null ;
		}
		
		
		view.addLayerEditor( gEditor );
		
		
		Layer<?> layer = gEditor.getLayer() ;
		
		layer.addLayerEventHandler( this ) ;
		
		
		int left =  graphic.getLeft () ;
		int top = graphic.getTop() ;
		
		
		if( Graphic.TEXT == graphic.getType () ){
			left = left - TextProperty.PADDING ;
			top = top - TextProperty.PADDING ;
		} 
		
		view.addLayer ( layer, left, top ) ;
				
		return gEditor ;
		
	}



	@Override
	public void onLayerSelected( LayerEvent  event) {
		Layer< ? extends Graphic  > selectedLayer ;
		
		Graphic graphic ;
		
		if ( currentEditor != null ){
			
			selectedLayer = currentEditor.getLayer();
			
			selectedLayer.unselect() ;
			
			graphic = selectedLayer.getGraphic() ;

			
			/* FIXME NG
			if (  graphic.getType() != Graphic.BG ) {
				
				int left =  graphic.getLeft () ;
				int top = graphic.getTop() ;
				
				if( Graphic.TEXT == graphic.getType () ){
					left = left - TextProperty.PADDING ;
					top = top - TextProperty.PADDING ;
				} 
				
				( ( AbsoluteLayout ) preview.getLayout() ).setPosition( 
						( Component ) selectedLayer.asWidget(), left , top  ) ;
			}*/
			
		}
		
		
		selectedLayer = event.getLayer() ;
		
		currentEditor = selectedLayer.getEditor() ;
		
		view.setEditor ( currentEditor ) ;
				
		view.enableDeleteControl( ! currentEditor.getGraphic().isLocked() ) ;
		
		view.enableMoveBackwardControl( ! isLayerOnBack( selectedLayer ) && selectedLayer.isMoveable() ) ;
		
		view.enableMoveForwardControl( ! view.isLayerOnFront( selectedLayer ) && selectedLayer.isMoveable() ) ;
		
		graphic = selectedLayer.getGraphic() ;
		
		
		
		/*
		if (  graphic.getType() != Graphic.BG ) {
			
			int left =  graphic.getLeft () ;
			int top = graphic.getTop() ;
			
			if( Graphic.TEXT == graphic.getType () ){
				left = left - TextProperty.PADDING ;
				top = top - TextProperty.PADDING ;
			} 
			
			( ( AbsoluteLayout ) preview.getLayout() ).setPosition( 
					( Component ) selectedLayer.asWidget(), left -2 , top - 2 ) ;
		}
		*/
		
	}



	@Override
	public void onLayerMoved(LayerEvent event) {
		
		int left = event.getLayer().getLeft() ;
		int top = event.getLayer().getTop() ;

		Graphic graphic = currentEditor.getGraphic() ;

		if ( Graphic.TEXT == graphic.getType() ){
			left = left + TextProperty.PADDING ;
			top = top + TextProperty.PADDING ;
		}
		
		graphic.setLeft( left ) ;		
		graphic.setTop( top ) ; 
	}



	@Override
	public void onLayerResized(LayerEvent event) {
		// nothing
	}
	
	
	@Override
	public void onLayerRenamed(LayerEvent event) {
		// nothing
	}


	@Override
	public void onLayerDeleted(LayerEvent event) {
		deleteGraphic() ;
	}
	
	
	protected void deleteGraphic( ){
		
		if ( currentEditor == null ) {
			Utils.alert( "Please, select the graphic to delete." ) ;
			return ;
		}
		
		view.removeLayerEditor( currentEditor ) ;
		view.removeLayer( currentEditor.getLayer() );
		
		graphics.remove( currentEditor.getGraphic() ) ;
		
		canvasProperty.getPropertyMap ().remove ( ( Property ) currentEditor.getGraphic() ) ;
		
		( ( Property ) currentEditor.getGraphic() ).setEnable( new Boolean ( false ) ) ;
 		
		view.enableDeleteControl( false ) ;
		view.enableMoveBackwardControl( false ) ;
		view.enableMoveForwardControl( false ) ;
		
		currentEditor.remove();
		currentEditor = null ;
		
	}
	
	
	protected void newGalleryImage () {
		
		ensureNewImageSelector();
		
		newImageSelector.show ( InitialState.GALLERY_IMAGE ) ;
		
	}
	
	protected void newFlickrImage () {
		
		ensureNewImageSelector();
		
		newImageSelector.show ( InitialState.FLICKR_IMAGE ) ;
		
	}
	
	
	
	
	protected void newGalleryLogo () {
		
		ensureNewImageSelector();
		
		newImageSelector.show ( InitialState.LOGO ) ;
		
		
	}
	
	
	protected void ensureNewImageSelector() {
		
		if ( newImageSelector == null ){
			
			newImageSelector = new NewImageSelector( new NewImageDialog() ) ;

			newImageSelector.getView().addValueChangeHandler( new DesignResourceChangeHandler< ImageResource >() {
				
				@Override
				public void onValueChange(ValueChangeEvent<ImageResource> event) {
					
					createNewImageProperty ( event.getValue() ) ;
					
					newImageSelector.hide() ;
				}
				
				
			});
			
			
			newImageSelector.addHandler( new SelectStockImageHandler() {
				@Override
				public void onStockImageSelected( StockImage image, StockImageDetails imageDetails) {
					
					createNewImageProperty ( imageDetails ) ;
					
					newImageSelector.hide() ;
					
				}
			});
			
			
			
		}
		
		newImageSelector.setUnlockedImages( unlockedImages ) ;
		
	}
	
	
	
	protected void newUploadImage() {
		
		final String uploadId = "" + System.currentTimeMillis () ;
		
		if ( addUploadImageDialog  == null ){
			
			addUploadImageDialog = new UploadImageDialog() ;
			
			addUploadImageDialog.setUploadUrl ( "dt/uploadImage" ) ;
			addUploadImageDialog.setUploadTip ( "<p style='font-size:0.9em'>GIF, JPEG or PNG image file<br/>400 kb max</p>" ) ;
			addUploadImageDialog.setUploadErrorMsg( UPLOAD_ERROR_MSG ) ;
			
			addUploadImageDialog.addUploadEventHandler( new UploadEventHandler() {
				@Override
				public void onUpload( UploadEvent uploadEvent) {
	
					if ( uploadEvent.isFailed() ) return ;
					
					createNewImageProperty( uploadEvent.getFileName () ) ;
					
				}
			}) ;
			
		}
		
		addUploadImageDialog.setUploadId( uploadId ) ;
		
		addUploadImageDialog.openCenter() ;
	
	
	}

	
	public void setUnlockedImages( ArrayList<String> unlockedImages ) {
		this.unlockedImages = unlockedImages;
	}

	
	protected void createNewImageProperty ( String uploadFilename ) {
		
		NewImageGraphic action = new NewImageGraphic () ;
		action.setDesignUid( Client.get().getDesign().getUid() ) ;
		action.setUploadFilename ( uploadFilename ) ;
		
		new RPC< NewGraphicResponse< ImageGraphic > > ( Client.get().getDesignService () ) .execute (
				action, new NewGraphicDone< ImageGraphic >(){ 
					
					public void onSuccess ( NewGraphicResponse< ImageGraphic > response ) {
						
						ImageGraphic newImage = response.getGraphic() ;
						
						graphics.add( newImage ) ;
						
						addGraphicEditor ( newImage ).getLayer ().select() ;
					
					}
				}
		) ;
		
	}
	
	
	protected void createNewImageProperty ( StockImageDetails stockImage ) {
	
		NewImageGraphic action = new NewImageGraphic () ;
		
		action.setDesignUid( Client.get().getDesign().getUid() ) ;
		

		action.setStockImage( stockImage.getUrl() ) ;
		
		
		new RPC< NewGraphicResponse< ImageGraphic > > ( Client.get().getDesignService () ) .execute (
				action, new NewGraphicDone< ImageGraphic >(){ 
					
					public void onSuccess ( NewGraphicResponse< ImageGraphic > response ) {
						
						ImageGraphic newImage = response.getGraphic() ;
						
						graphics.add( newImage ) ;
						
						addGraphicEditor ( newImage ).getLayer ().select() ;
					
					}
				}
		) ;
		
	}
	
	
	protected void createNewImageProperty ( ImageResource image ) {
		
		
		NewImageGraphic action = new NewImageGraphic () ;
		action.setDesignUid( Client.get().getDesign().getUid() ) ;
		
		action.setImage ( image ) ;
		
		new RPC< NewGraphicResponse< ImageGraphic > > ( Client.get().getDesignService () ) .execute (
				action, new NewGraphicDone<ImageGraphic>(){ 
					
					public void onSuccess ( NewGraphicResponse<ImageGraphic> response ) {
						
						ImageGraphic newImage = response.getGraphic() ;
						
						graphics.add( newImage ) ;
						
						addGraphicEditor ( newImage ).getLayer ().select() ;
					
					}
				}
		) ;
		
	}
	

	
	protected void newText () {
		
		
		NewTextGraphic action = new NewTextGraphic();
		action.setDesignUid( Client.get().getDesign().getUid() ) ;
		
		
		new RPC< NewGraphicResponse< TextGraphic > > ( Client.get().getDesignService() ) .execute (
				action, new NewGraphicDone< TextGraphic >(){ 
					public void onSuccess ( NewGraphicResponse<TextGraphic> response ) {
						
						TextGraphic newText = response.getGraphic() ;
						
						graphics.add( newText ) ;
						
						addGraphicEditor ( newText ).getLayer ().select() ;
						
					}
				}
		) ;
	}
	
	
	
	protected boolean hasBackground () {
		return bgGraphic != null ; 
	}
	
	protected Layer<? extends Graphic> getSelectedLayer() {
		return ( ( currentEditor == null ) ? null : currentEditor.getLayer() ) ;
	}
	
	
	protected < GRAPHIC extends Graphic >  boolean isLayerOnBack( Layer< GRAPHIC > layer ) {
		int zindex = view.indexOfLayer ( layer ) ;
		return hasBackground() && zindex == 1  
			|| ! hasBackground() && zindex == 0 ;
	}
	
	
	protected void moveSelectedLayerForward () {
		
		Layer<? extends Graphic> layer = getSelectedLayer () ;
		
		if( layer == null ) return ;
		
		Graphic graphic = currentEditor.getGraphic() ;
				
		if ( ! graphic.isMoveable () || view.isLayerOnFront ( layer ) ) {
			return ;
		}
		
		int i = view.indexOfLayer ( layer ) ;
		
		graphics.remove( graphic ) ;
		
		graphics.add( i + 1 , graphic ) ;
		
		
		view.insertLayer ( layer, i + 1 , graphic.getLeft () - 2, graphic.getTop () - 2 ) ;
		
		view.enableMoveBackwardControl( ! isLayerOnBack( layer ) && layer.isMoveable() ) ;
		
		view.enableMoveForwardControl( ! view.isLayerOnFront( layer ) && layer.isMoveable() ) ;
		
	}

	
	
	protected void moveSelectedLayerBackward () {

		Layer<? extends Graphic> layer = getSelectedLayer () ;
		
		if( layer == null ) return ;
		
		Graphic graphic = currentEditor.getGraphic() ;
		
		if ( ! graphic.isMoveable () || isLayerOnBack ( layer ) ) {
			return ;
		}
		
		int i = view.indexOfLayer ( layer ) ;
		
		graphics.remove( graphic ) ;
		
		graphics.add( i - 1 , graphic ) ;
		
		view.insertLayer (  layer, i - 1, graphic.getLeft () - 2  , graphic.getTop () - 2 ) ;
		
		view.enableMoveBackwardControl( ! isLayerOnBack( layer ) && layer.isMoveable() ) ;
		
		view.enableMoveForwardControl( ! view.isLayerOnFront( layer ) && layer.isMoveable() ) ;
			
	
	}

	
	public void cancel() {
		canvasProperty = null ;
		view.clear() ;
		currentEditor = null ;

	}
	
	
	private final static String UPLOAD_ERROR_MSG = 
			"Upload error ! Please check your image file.\n" +
			"We support only GIF, JPEG and PNG images ( 400 kb max )." ;



	
	

		

}
