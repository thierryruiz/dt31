package com.dotemplate.core.client.canvas.presenter;

import java.util.LinkedHashMap;

import com.dotemplate.core.client.canvas.BackgroundLayer;
import com.dotemplate.core.client.canvas.Layer;
import com.dotemplate.core.client.canvas.view.BackgroundEditorView;
import com.dotemplate.core.client.editors.EditorControlStyle;
import com.dotemplate.core.client.editors.EditorFactory;
import com.dotemplate.core.client.editors.MutablePropertySetEditor;
import com.dotemplate.core.client.editors.PropertyEditor;
import com.dotemplate.core.client.editors.PropertyEditorMap;
import com.dotemplate.core.client.editors.PropertySetEditor;
import com.dotemplate.core.client.events.PropertyChangeEvent;
import com.dotemplate.core.client.events.PropertyChangeEventHandler;
import com.dotemplate.core.client.frwk.Utils;
import com.dotemplate.core.shared.DesignTraverser;
import com.dotemplate.core.shared.canvas.BackgroundGraphic;
import com.dotemplate.core.shared.properties.BackgroundProperty;
import com.dotemplate.core.shared.properties.Property;
import com.dotemplate.core.shared.properties.PropertyMap;
import com.dotemplate.core.shared.properties.PropertySet;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;



public class BackgroundEditor extends GraphicEditor< BackgroundGraphic > implements PropertyChangeEventHandler {
	
	protected FlowPanel wrapper ;
	
	protected FlowPanel subEditorsPanel ;
	
	protected DesignTraverser editorCreator ;
	
	PropertyEditorMap allEditors ;
	
	public BackgroundEditor ( BackgroundGraphic bg, BackgroundEditorView view ) {
		
		super( bg, view ) ;
		
		
		wrapper = new FlowPanel() ;
		wrapper.setStyleName( "ez-graphic-editor" ) ;
		
		
		subEditorsPanel = new FlowPanel(){
			
			@Override
			public void add( Widget w ) {
				// Need a bloody wrapper
				SimplePanel wpr = new SimplePanel() ;
				wpr.setWidget( w ) ;
				Utils.floatLeft( wpr ) ;
				Utils.margins( wpr, 0, 20, 20, 0 ) ;
				
				wpr.setWidth( EditorControlStyle.BACKGROUND_EDITOR_STYLE.getButtonWidth() + "px" ) ;
				super.add( wpr ) ;
			};
			
			@Override
			public void insert( Widget w, int beforeIndex ) {
				// Need a bloody wrapper
				SimplePanel wpr = new SimplePanel() ;
				wpr.setWidget( w ) ;
				Utils.floatLeft( wpr ) ;
				Utils.margins( wpr, 0, 20, 20, 0 ) ;
				
				wpr.setWidth( EditorControlStyle.BACKGROUND_EDITOR_STYLE.getButtonWidth() + "px" ) ;
				super.insert( wpr, beforeIndex ) ;

			};
			
			
			@Override
			public boolean remove( Widget w ) {
				return super.remove( w.getParent() );
			}
		
		};

		
		wrapper.add( subEditorsPanel ) ;

		allEditors = new PropertyEditorMap() ;
		
		PropertySetEditor editor = ( PropertySetEditor ) BackgroundEditorFactory.get().createPropertyEditor( 
				( ( PropertySet ) bg ) ) ; 
			
		editor.addHandler( BackgroundEditor.this ) ;
		
		LinkedHashMap< String,  PropertyEditor < ? > > childEditors = new LinkedHashMap< String, PropertyEditor< ? > > () ;
		
		for ( Property pp : ( ( PropertySet ) bg ).getProperties() ){

			createEditors( pp, childEditors ) ;
			
		}
		
		buildView( editor, childEditors ) ;					
		
		
	}
	
	
	
	protected void buildView( PropertySetEditor rootEditor,
			LinkedHashMap<String, PropertyEditor< ? > > childEditors ) {
			
		//subEditorsPanel.add( rootEditor ) ;
		
		
		for ( PropertyEditor< ? > editor : childEditors.values() ){
			
			editor.setParentEditor( rootEditor ) ;
			
			subEditorsPanel.add( editor.asWidget() ) ;

		}
	}

	public void createEditors ( Property p, LinkedHashMap< String,  
			PropertyEditor < ? extends Property > > childEditors ){
		
		boolean editable = p.getEditable () != null && p.getEditable () ;

		boolean enable = p.getEnable() != null &&  p.getEnable () ;
	
		EditorFactory factory = BackgroundEditorFactory.get() ; 
		
		if ( !editable || ! enable  ) return ;
		
		PropertyEditor < ? extends Property > e = factory.createPropertyEditor( p ) ;

		if ( e == null ) return ;
		
		e.addHandler( this ) ;
		
		childEditors.put( p.getUid(), e )  ;
		
		allEditors.put(  p.getUid(), e ) ;
		
		
		if ( p.isSet() ){
		
			for ( Property pp : ( ( PropertySet ) p ).getProperties() ){
				
				createEditors( pp, childEditors ) ;
				
			}
		}
	}
	
	

	@Override
	protected Layer< BackgroundGraphic > createLayer() {
		return new BackgroundLayer() ;
	}


	@Override
	public BackgroundProperty getProperty() {
		return ( BackgroundProperty )  graphic ;
	}
	
	@Override
	public void setProperty(Property property) {
		this.graphic = ( BackgroundProperty ) property ;
	}


	@Override
	public Widget asWidget() {
		return wrapper;
	}


	@Override
	public void setParentEditor(PropertySetEditor parentEditor) {
	}


	@Override
	public PropertySetEditor getParentEditor() {
		return null;
	}

	
	
	@Override
	public void onPropertyChanged( PropertyChangeEvent e ) {
		
		Property p = e.getEditor().getProperty() ;
		
		if ( p.isSet() && ( ( PropertySet ) p ).mutable() ) {
			
			onMutablePropertyChanged ( e , ( MutablePropertySetEditor ) e.getEditor() ) ;
		}
				
		preview() ;
	}
	
	
	
	protected void onMutablePropertyChanged( PropertyChangeEvent e, MutablePropertySetEditor mutableEditor  ){
		
		PropertyMap deletedProperties = e.getMutablePropertyDeletedProperties() ;
		
		if ( deletedProperties != null  ){
			
			removeEditors( deletedProperties ) ;
		
		}
		
		
		LinkedHashMap< String,  PropertyEditor < ? > > childEditors = 
			new LinkedHashMap< String, PropertyEditor< ? > > () ;
		
		
		for ( Property p : mutableEditor.getProperty().getProperties() ){
			createEditors( p, childEditors ) ;
		}
		
		
		PropertySetEditor rootEditor = mutableEditor.getParentEditor() ;
		
		
		//int index = 2  ;
		
		for ( PropertyEditor< ? > childEditor : childEditors.values() ){
			
			childEditor.setParentEditor( rootEditor ) ;
			subEditorsPanel.add( childEditor.asWidget() ) ;
			
			//subEditorsPanel.insert( childEditor.asWidget(), index ) ;
			//index++ ;
		}

	}
	
	
	
	protected void removeEditors( PropertyMap properties ){
		
		PropertyEditor< ? > editor ;
		
		for ( Property p : properties.values() ){
			
			editor =  allEditors.remove( p.getUid() ) ;
						
			if ( editor != null ){
				
				subEditorsPanel.remove( editor.asWidget() ) ;
				
				editor.remove() ;
				editor.removeHandler( this ) ;
				
				editor = null ;	
			}
			
			
			if ( p.isSet() ){
				
				removeEditors( ( ( PropertySet ) p ).getPropertyMap() ) ;
			
			}
			
		}		
	}



	@Override
	public void bind() {
	}




	
	
}
