package com.dotemplate.core.client.canvas.presenter;

import java.util.ArrayList;
import java.util.logging.Logger;

import com.dotemplate.core.client.apis.flickr.FlickImageAPI;
import com.dotemplate.core.client.canvas.event.SelectStockImageHandler;
import com.dotemplate.core.client.canvas.ui.StockImageSearchPanel;
import com.dotemplate.core.client.canvas.view.NewImageView;
import com.dotemplate.core.client.canvas.view.StockImageSearchView;
import com.dotemplate.core.client.canvas.view.NewImageView.InitialState;
import com.dotemplate.core.client.frwk.AbstractPresenter;
import com.dotemplate.core.client.widgets.resource.DesignResourceLoadAdapter;
import com.dotemplate.core.shared.canvas.ImageResource;

public class NewImageSelector extends AbstractPresenter< NewImageView > {

	protected static Logger logger = Logger.getLogger( NewImageSelector.class.getName() );
	
	protected ImageGallerySelector imageGallerySelector ;
	
	protected StockImageSelector flickImageSelector ;

	protected ImageResourcePagingAsyncProvider galleryImageProvider ;
	
	protected boolean imageTagsLoaded = false ;
	
	
	public NewImageSelector( NewImageView view ) {
		
		super( view ) ;
		
		
		// image gallery
		galleryImageProvider =  new ImageResourcePagingAsyncProvider();
		
		galleryImageProvider.setPaging( 30 );
	
		imageGallerySelector = new ImageGallerySelector( galleryImageProvider ) ;

		view.setImageGalleryView( imageGallerySelector.getView() ) ; 
		
		
		// Flickr gallery
		StockImageSearchView flickrSearchPanel = new StockImageSearchPanel() ;
		
		flickrSearchPanel.setSearchPlaceHolder( "Search Flickr Image..." );
		
		flickImageSelector = new StockImageSelector( flickrSearchPanel, new FlickImageAPI() ) ;
		
		view.setFlickImageView( flickrSearchPanel );
		
	}
	
	
	@Override
	public void bind() {
		
	}
	
	
	public void show ( final NewImageView.InitialState state ) {
		
		if ( !imageTagsLoaded ){
			
			galleryImageProvider.addLoadHandler( new DesignResourceLoadAdapter< ImageResource >(){
				@Override
				public void onDesignResourcesLoad( String[] tags ) {
					
					imageGallerySelector.getView().setTags( tags ); 
							
					view.show( state ) ;
					
					if( state == InitialState.LOGO ){
						imageGallerySelector.filter("logo");			
					} else {
						imageGallerySelector.filter("all");
					}
					
					imageTagsLoaded = true ;
				
				}
			});
			
			galleryImageProvider.loadResourcesTags();
			
		} else {
			
			view.show( state ) ;
			
			if( state == InitialState.LOGO ){
				
				imageGallerySelector.filter("logo");			
			
			} else {
				imageGallerySelector.filter("all");
			}

		}
				
	}

	
	public void hide () {
		view.hide();
	}

	public void setUnlockedImages(ArrayList<String> unlockedImages) {		
	}


	public void addHandler( SelectStockImageHandler h ) {
		flickImageSelector.addHandler( h ) ;		
	}
	
	


	

}
