package com.dotemplate.core.client.canvas.presenter;

import com.dotemplate.core.client.canvas.ImageLayer;
import com.dotemplate.core.client.canvas.Layer;
import com.dotemplate.core.client.canvas.event.LayerEvent;
import com.dotemplate.core.client.canvas.view.ImageEditorView;
import com.dotemplate.core.client.editors.PropertySetEditor;
import com.dotemplate.core.shared.canvas.ImageGraphic;
import com.dotemplate.core.shared.properties.ImageProperty;
import com.dotemplate.core.shared.properties.Property;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;


public class ImageEditor extends GraphicEditor< ImageGraphic >  {

	public ImageEditor ( ImageGraphic image, ImageEditorView view  ) {
		
		super( image, view ) ;
		
	}


	@Override
	public void bind() {
		
		ImageEditorView imageEditorView = ( ImageEditorView )  view ;
	
		imageEditorView.getOpacityControl().addValueChangeHandler(new ValueChangeHandler<Double>() {
			@Override
			public void onValueChange(ValueChangeEvent<Double> event) {
				graphic.setOpacity( new Double ( event.getValue() / 100d ).floatValue() ) ;
				preview() ;
			}
		}) ;
		
		imageEditorView.getLeftFadingControl().addValueChangeHandler(new ValueChangeHandler<Double>() {
			@Override
			public void onValueChange(ValueChangeEvent<Double> event) {
				graphic.setLeftFading( new Double ( event.getValue() / 100d ).floatValue() ) ;
				preview() ;
			}
		}) ;
		
		imageEditorView.getRightFadingControl().addValueChangeHandler(new ValueChangeHandler<Double>() {
			@Override
			public void onValueChange(ValueChangeEvent<Double> event) {
				graphic.setRightFading( new Double ( event.getValue() / 100d ).floatValue() ) ;
				preview() ;
			}
		}) ;
		
		imageEditorView.getTopFadingControl().addValueChangeHandler(new ValueChangeHandler<Double>() {
			@Override
			public void onValueChange(ValueChangeEvent<Double> event) {
				graphic.setTopFading( new Double ( event.getValue() / 100d ).floatValue() ) ;
				preview() ;
			}
		}) ;

		imageEditorView.getBottomFadingControl().addValueChangeHandler(new ValueChangeHandler<Double>() {
			@Override
			public void onValueChange(ValueChangeEvent<Double> event) {
				graphic.setBottomFading( new Double ( event.getValue() / 100d ).floatValue() ) ;
				preview() ;
			}
		}) ;

		imageEditorView.getBlurControl().addValueChangeHandler(new ValueChangeHandler<Double>() {
			@Override
			public void onValueChange(ValueChangeEvent< Double > event) {
				graphic.setBlur( new Double ( event.getValue() ).intValue() ) ;
				preview() ;
			}
		}) ;
		
	}

	
	
	@Override
	protected Layer< ImageGraphic > createLayer() {
		return new ImageLayer() ;
	}


	@Override
	public void onLayerResized( LayerEvent event) {		
		double scale = ( double ) layer.getWidth() / ( double ) graphic.getWidth () ;			
		graphic.setScale ( ( float ) scale )  ;
		preview() ;
	}

	

	@Override
	public void onLayerSelected( LayerEvent event) {
		
		ImageEditorView imageEditorView = ( ImageEditorView )  view ;
		
		imageEditorView.getOpacityControl().setValue( graphic.getOpacity() * 100d , false );
		
		imageEditorView.getLeftFadingControl().setValue( graphic.getLeftFading() * 100d , false );
				
		imageEditorView.getRightFadingControl().setValue( graphic.getRightFading() * 100d , false );

		imageEditorView.getTopFadingControl().setValue( graphic.getTopFading() * 100d , false );

		imageEditorView.getBottomFadingControl().setValue( graphic.getBottomFading() * 100d , false );

		imageEditorView.getBlurControl().setValue( graphic.getBlur() * 100d , false );

	}
	
	
	@Override
	public ImageProperty getProperty() {
		return ( ImageProperty ) graphic ;
	}

	@Override
	public void setProperty(Property property) {
		this.graphic = ( ImageProperty ) property ;
	}


	@Override
	public void setParentEditor( PropertySetEditor parentEditor ) {
	}


	@Override
	public PropertySetEditor getParentEditor() {
		return null;
	}




	
	
	
}
