package com.dotemplate.core.client.canvas.presenter;

import com.dotemplate.core.client.editors.SymbolSelector;
import com.dotemplate.core.client.widgets.resource.DesignResourceProvider;
import com.dotemplate.core.shared.properties.PropertySet;


public class TextStyleSelector extends SymbolSelector {

	public TextStyleSelector( DesignResourceProvider< PropertySet > resourceProvider ) {

		super(  resourceProvider  );
		
		setHeading( "Choose Text Style" );
	
	}

	
}
