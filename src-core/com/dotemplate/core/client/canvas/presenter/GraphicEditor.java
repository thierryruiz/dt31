package com.dotemplate.core.client.canvas.presenter;


import com.dotemplate.core.client.Client;
import com.dotemplate.core.client.canvas.Layer;
import com.dotemplate.core.client.canvas.event.LayerEvent;
import com.dotemplate.core.client.canvas.event.LayerEventHandler;
import com.dotemplate.core.client.canvas.view.GraphicEditorView;
import com.dotemplate.core.client.editors.PropertyEditor;
import com.dotemplate.core.client.events.PropertyChangeEvent;
import com.dotemplate.core.client.events.PropertyChangeEventHandler;
import com.dotemplate.core.client.frwk.AbstractPresenter;
import com.dotemplate.core.client.frwk.RPC;
import com.dotemplate.core.shared.canvas.Graphic;
import com.dotemplate.core.shared.command.RenderGraphic;
import com.dotemplate.core.shared.command.RenderGraphicDone;
import com.dotemplate.core.shared.command.RenderGraphicResponse;
import com.dotemplate.core.shared.properties.Property;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;



public abstract class GraphicEditor < GRAPHIC extends Graphic > extends AbstractPresenter< GraphicEditorView< GRAPHIC > > implements 
	PropertyEditor< Property >, LayerEventHandler, IsWidget {
	
	protected Layer< GRAPHIC > layer ;
	
	protected GRAPHIC graphic ;
	
	protected HandlerManager handlerManager;
	
	protected String editorId ;

	protected abstract Layer< GRAPHIC > createLayer() ;
	
	
	public GraphicEditor( GRAPHIC graphic, GraphicEditorView< GRAPHIC > view ) {
		super( view ) ;
		this.graphic = graphic ;
		layer = createLayer() ;
		layer.setImageUrl ( graphic.getWorkingUri () ) ;
		layer.addLayerEventHandler ( this ) ;
		layer.setEditor( this ) ;
		
		handlerManager = new HandlerManager( this ) ;
	
	}
	
	
	public Layer< GRAPHIC > getLayer() {
		return layer;
	}
	
	
	public GRAPHIC getGraphic() {
		return graphic ;
	}

	
	
	@Override
	public void onLayerMoved ( LayerEvent event ) {
	}

	
	
	@Override
	public void onLayerResized( LayerEvent event ) {
		// no behavior
	}


	@Override
	public void onLayerSelected( LayerEvent event ) {
		// no behavior
	}
	
	
	@Override
	public void onLayerRenamed(LayerEvent event) {
	}



	@Override
	public void onLayerDeleted(LayerEvent event) {
	}	
	
	
	@Override
	public Widget asWidget() {
		return view.asWidget() ;
	}

	
	protected void preview () {
		
		RenderGraphic renderGraphic = new RenderGraphic ();
		renderGraphic.setDesignUid( Client.get().getDesign().getUid() ) ; 
		
		renderGraphic.setGraphic ( graphic );

		new RPC< RenderGraphicResponse > ( Client.get().getDesignService() ).execute (

		renderGraphic, new RenderGraphicDone ( graphic ) {
			@Override
			public void onSuccess ( RenderGraphicResponse response ) {

				Graphic rendered = response.getGraphic ();

				graphic.setWorkingUri ( rendered.getWorkingUri () );

				layer.refresh ( 
						rendered.getWorkingUri (),
						rendered.getRenderedWidth (),
						rendered.getRenderedHeight () );

			}
		} );
	}
	
	
	
	@Override
	public HandlerRegistration addHandler( PropertyChangeEventHandler h ) {
		return handlerManager.addHandler( PropertyChangeEvent.TYPE, h  ) ;
	}
	
	
	
	@Override
	public void removeHandler(PropertyChangeEventHandler h) {
		handlerManager.removeHandler( PropertyChangeEvent.TYPE, h ) ;
		
	}
	
	public void fireChange() {
		handlerManager.fireEvent( new ChangeEvent() {
			@Override
			public Object getSource () {
				return GraphicEditor.this;
			}
		}) ;
	}
		
	@Override
	public void fireEvent( GwtEvent<?> e ) {
		handlerManager.fireEvent( e ) ;
	}


	public String getEditorId() {
		return editorId;
	}


	public void setEditorId(String editorId) {
		this.editorId = editorId;
	}
	
	@Override
	public void enable() {
	}
	
	
	@Override
	public void disable() {
	}
	
	
	@Override
	public boolean isEnabled() {
		return true;
	}
	
	@Override
	public void remove() {
		asWidget().removeFromParent() ;
	}
	
	

	
	
}
