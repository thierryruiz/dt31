package com.dotemplate.core.client.canvas.presenter;

import com.dotemplate.core.client.editors.ColorPropertyEditor;
import com.dotemplate.core.client.editors.EditorFactory;
import com.dotemplate.core.client.editors.MutablePropertySetEditor;
import com.dotemplate.core.client.editors.PercentPropertyEditor;
import com.dotemplate.core.client.editors.PropertyEditor;
import com.dotemplate.core.client.editors.PropertySetEditor;
import com.dotemplate.core.client.editors.SizePropertyEditor;
import com.dotemplate.core.client.frwk.Utils;
import com.dotemplate.core.shared.canvas.Graphic;
import com.dotemplate.core.shared.properties.ColorProperty;
import com.dotemplate.core.shared.properties.PercentProperty;
import com.dotemplate.core.shared.properties.Property;
import com.dotemplate.core.shared.properties.PropertySet;
import com.dotemplate.core.shared.properties.SizeProperty;
import com.dotemplate.core.shared.properties.TextProperty;


public abstract class CanvasEditorFactory extends EditorFactory {
		
	@Override
	public PropertyEditor<? extends Property> createPropertyEditor(
			Property property) {
		
		PropertyEditor<? extends Property> editor = super.createPropertyEditor( property );
	
		
		if ( editor != null ) {
			return editor ;
		}
		
		Utils.log( "CanvasEditorFactory.createPropertyEditor()....")  ;
		
		
		switch ( property.getType () ){
		
			case Property.SET :
			case Graphic.BG :
				PropertySet set = ( PropertySet ) property ;
				
				if ( set.mutable() ) {
					
					return new MutablePropertySetEditor( set) ;
				
				} else {
					return new PropertySetEditor( set ) ;				
				}

			case Graphic.TEXT :
				return new TextStyleEditor( ( TextProperty ) property ) ;
	
			case ( Property.COLOR ) :
				return new ColorPropertyEditor( ( ColorProperty ) property ) ;
			
			
			case ( Property.SIZE ) :
				return new SizePropertyEditor( ( SizeProperty ) property ) ;	
			
			case ( Property.PERCENT ) :
				return new PercentPropertyEditor( ( PercentProperty ) property ) ;	
			
			
			default: return null ;
		
		}
		
	
	}
	
	
	
}
