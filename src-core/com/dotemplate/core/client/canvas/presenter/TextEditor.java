package com.dotemplate.core.client.canvas.presenter;



import com.dotemplate.core.client.canvas.Layer;
import com.dotemplate.core.client.canvas.TextLayer;
import com.dotemplate.core.client.canvas.ui.FontSizeEditorControl;
import com.dotemplate.core.client.canvas.ui.SVGFontSelector;
import com.dotemplate.core.client.canvas.view.TextEditorView;
import com.dotemplate.core.client.editors.EditorFactory;
import com.dotemplate.core.client.editors.MutablePropertySetEditor;
import com.dotemplate.core.client.editors.PropertyEditor;
import com.dotemplate.core.client.editors.PropertyEditorControl;
import com.dotemplate.core.client.editors.PropertySetEditor;
import com.dotemplate.core.client.events.PropertyChangeEvent;
import com.dotemplate.core.client.widgets.resource.DesignResourceChangeHandler;
import com.dotemplate.core.shared.SVGFont;
import com.dotemplate.core.shared.canvas.TextGraphic;
import com.dotemplate.core.shared.properties.Property;
import com.dotemplate.core.shared.properties.PropertySet;
import com.dotemplate.core.shared.properties.TextProperty;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.Widget;


public class TextEditor extends MutableGraphicEditor < TextGraphic > implements DesignResourceChangeHandler< SVGFont >   {
	
	protected PropertyEditorControl previewTextControl ;
	
	protected FontSizeEditorControl fontSizeControl ;
	
	protected PropertyEditorControl fontFamilyControl ;
	
	protected TextEditorView textEditorView;
	
	
	public TextEditor ( TextGraphic text, TextEditorView view  ) {
			
		super( text, view  ) ;
		
		textEditorView = view ;
		
		( ( PropertySet ) text ) .execute( editorCreator ) ;
				
		view.getTextInput().setValue(graphic.getText());
		
		view.getFontSizeSelector().setValue( graphic.getFontSize() );
		
		
	   	// create text style editor
	   	mutableEditor = ( MutablePropertySetEditor ) 
	   		TextEditorFactory.get().createPropertyEditor( ( TextProperty ) getGraphic() ) ;
	   	
		mutableEditor.addHandler( this ) ;
		
		view.setStyleEditorView( mutableEditor.asWidget() ) ;
	
		view.setSubEditorViews( subEditorsPanel ) ;
		
		for ( PropertyEditor< ? extends Property > e : subEditors.values() ){
			subEditorsPanel.add( e.asWidget() ) ;
		}
		
	}
	
	
	@Override
	protected Layer< TextGraphic > createLayer() {
		return new TextLayer() ;
	}
		
	
	@Override
	public void onValueChange( ValueChangeEvent<SVGFont> event) {
		getProperty().setFontFamily( event.getValue().getFamily() ) ; 
		fireChange() ;
		preview() ;
	}

	
	@Override
	public void onPropertyChanged(PropertyChangeEvent e) {
		super.onPropertyChanged(e);
	}

	
	
	@Override
	protected void onMutablePropertyChanged( PropertyChangeEvent e,
			MutablePropertySetEditor editor ) {
		
		TextGraphic symbol = ( TextProperty ) e.getSymbol() ;
		
		graphic.setFontSize( symbol.getFontSize() ) ;
		
		graphic.setFontFamily( symbol.getFontFamily() ) ;
		
		super.onMutablePropertyChanged( e, editor );
		
	}
	
	
	
	private void onChangeText() {
		
		if ( ! textEditorView.validateTextInput() ) {
			return ;
		}
		
		graphic.setText ( textEditorView.getTextInput().getValue() ) ;
		
		fireChange() ;
		
		preview() ;
		
	}
	

	@Override
	public TextProperty getProperty() {
		return ( TextProperty )  graphic ;
	}

	
	public void setProperty( Property graphic ){
		this.graphic = ( TextProperty ) graphic ;
	}
	

	@Override
	public Widget asWidget() {
		return view.asWidget() ;
	}

	
	@Override
	public void setParentEditor( PropertySetEditor parentEditor ) {
	}


	@Override
	public PropertySetEditor getParentEditor() {
		return null;
	}


	@Override
	protected EditorFactory getEditorFactory() {
		return TextEditorFactory.get() ;
	}


	@Override
	public void bind() {
		
		TextEditorView textEditorView = ( TextEditorView ) view ;
		
		textEditorView.getTextInput().addValueChangeHandler(
			new ValueChangeHandler<String>() {
				@Override
				public void onValueChange(ValueChangeEvent<String> event) {
					onChangeText() ;
				}
			}
		) ;
		
		
		textEditorView.getFontSizeSelector().addValueChangeHandler(
			new ValueChangeHandler<Integer>() {
				@Override
				public void onValueChange(ValueChangeEvent<Integer> event) {
					graphic.setFontSize ( event.getValue() ) ;
					fireChange() ;
					preview() ;	
				}

			}
		) ;
		
		
		textEditorView.getFontFamilyControl().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				SVGFontSelector.show( TextEditor.this, getProperty().getFontFamily() ) ;
			}
		});
		
		
	}





	
}
