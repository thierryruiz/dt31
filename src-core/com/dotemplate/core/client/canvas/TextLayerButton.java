package com.dotemplate.core.client.canvas;

import com.dotemplate.core.client.events.PropertyChangeEvent;

import com.dotemplate.core.shared.canvas.TextGraphic;


public class TextLayerButton extends LayerButton< TextGraphic > {

	public TextLayerButton( Layer< TextGraphic > layer) {
		super( layer );
	}

	
	@Override
	public void onPropertyChanged( PropertyChangeEvent e ) {
		setButtonText() ;
	}




}
