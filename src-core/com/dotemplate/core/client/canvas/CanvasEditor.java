package com.dotemplate.core.client.canvas;


import java.util.ArrayList;

import com.dotemplate.core.client.Client;
import com.dotemplate.core.client.canvas.event.LayerEvent;
import com.dotemplate.core.client.canvas.event.LayerEventHandler;
import com.dotemplate.core.client.canvas.presenter.GraphicEditor;
import com.dotemplate.core.client.canvas.presenter.ImageEditor;
import com.dotemplate.core.client.frwk.CommandCallback;
import com.dotemplate.core.client.frwk.RPC;
import com.dotemplate.core.client.frwk.Utils;
import com.dotemplate.core.client.widgets.upload.UploadEvent;
import com.dotemplate.core.client.widgets.upload.UploadEventHandler;
import com.dotemplate.core.client.widgets.upload.UploadImageDialog;
import com.dotemplate.core.shared.canvas.BackgroundGraphic;
import com.dotemplate.core.shared.canvas.Graphic;
import com.dotemplate.core.shared.canvas.ImageGraphic;
import com.dotemplate.core.shared.canvas.ImageResource;
import com.dotemplate.core.shared.canvas.TextGraphic;
import com.dotemplate.core.shared.command.GetGraphics;
import com.dotemplate.core.shared.command.GetGraphicsResponse;
import com.dotemplate.core.shared.command.NewGraphicDone;
import com.dotemplate.core.shared.command.NewGraphicResponse;
import com.dotemplate.core.shared.command.NewImageGraphic;
import com.dotemplate.core.shared.command.NewTextGraphic;
import com.dotemplate.core.shared.properties.Property;
import com.dotemplate.core.shared.properties.PropertySet;
import com.dotemplate.core.shared.properties.TextProperty;
import com.extjs.gxt.ui.client.Style.Scroll;
import com.extjs.gxt.ui.client.widget.Component;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.layout.AbsoluteData;
import com.extjs.gxt.ui.client.widget.layout.AbsoluteLayout;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.DockPanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;

@Deprecated
public class CanvasEditor implements IsWidget, LayerEventHandler {
	
	static int PREVIEW_PANEL_HEIGHT = 500 ;
	
	static int LAYERS_PANEL_HEIGHT = 260 ;
	
	protected DockPanel wrapper ;
	
	protected FlowPanel layout ;
	
	// protected FlowPanel previewContainer ;
	
	protected ContentPanel preview ;

	protected boolean oldIE ;
					
	protected CanvasEditorToolbar toolbar ;
	
	protected EditPanel editPanel ;
	
	protected ContentPanel previewScrollPanel;
	
	protected FlowPanel settingsPanel ;
	
	protected LayerSelector layerSelector ;
	
//	protected AddGalleryImageDialog addGalleryImageDialog ;
	
	protected UploadImageDialog addUploadImageDialog ;
	
	protected GraphicEditor<? extends Graphic> currentEditor ;
	
	protected PropertySet canvasProperty ;
	
	protected BackgroundGraphic bgGraphic ;
	
	protected ArrayList< Graphic > graphics ;
	
	protected int canvasWidth = 0 ;
	
	protected int canvasHeight = 0 ;
	
	protected ArrayList< String  > unlockedImages ; 
	
	private final static String OLD_IE_WARNING = 
		"<h3>Cannot edit banner</h3>." +
		"You are using an old version on Internet Explorer which does not support image transparency." +
		"Please use Firefox or Google Chrome for a better and faster template editing experience." ;

	
	private final static String UPLOAD_ERROR_MSG = 
		"Upload error ! Please check your image file.\n" +
		"We support only GIF, JPEG and PNG images ( 400 kb max )." ;
	

	
	protected CanvasEditor() {
	
		layout = new FlowPanel() ;		
		layout.setWidth( "100%" ) ;
		layout.add( toolbar = new CanvasEditorToolbar() ) ;
		
		
		wrapper = new DockPanel() ;
		wrapper.setWidth( "100%" ) ;
		wrapper.addStyleName("ez-canvas-editor-dock") ;
		
		
		oldIE =  Utils.isOldIE () ;
		
		
		if ( oldIE ){
			wrapper.add( new HTML( OLD_IE_WARNING ), DockPanel.CENTER ) ;			
			return ;
		}

		
		HorizontalPanel previewWrapper = new HorizontalPanel();
		previewWrapper.setWidth( "100%" ) ;
		previewWrapper.setHeight( PREVIEW_PANEL_HEIGHT + "px" ) ;
		
		
		previewScrollPanel = new ContentPanel() ;
		previewScrollPanel.setScrollMode( Scroll.AUTO ) ;
		previewScrollPanel.setBorders(false) ;
		previewScrollPanel.setAutoWidth( true ) ;
		
		previewScrollPanel.setLayout( new FitLayout() ) ;
		previewScrollPanel.setHeaderVisible(false) ;
		previewScrollPanel.setCollapsible ( false );
		previewScrollPanel.setHeight( PREVIEW_PANEL_HEIGHT  + "px" ) ;
		previewScrollPanel.setWidth( "100%" ) ;
		previewScrollPanel.setStyleName( "ez-drawboard-scroll" ) ;
		previewScrollPanel.add( previewWrapper ) ;
		
		//previewContainer.add( previewScrollPanel ) ;
		
		
		preview = new ContentPanel() ;
		preview.setLayout ( new AbsoluteLayout() ) ;
		preview.setHeaderVisible(false) ;
		preview.setCollapsible ( false );
		preview.setStyleName( "ez-drawboard-preview" ) ;
		
		
		previewWrapper.add( preview ) ;
		previewWrapper.setCellHorizontalAlignment( preview, HasHorizontalAlignment.ALIGN_CENTER ) ;
		previewWrapper.setCellVerticalAlignment( preview, HasVerticalAlignment.ALIGN_MIDDLE ) ;
		

		layerSelector = new LayerSelector() ;
		
			
		editPanel = new EditPanel() ;
		
		
		wrapper.add( previewScrollPanel, DockPanel.NORTH ) ;
		wrapper.add( layerSelector, DockPanel.WEST ) ;
		wrapper.add( editPanel, DockPanel.CENTER ) ;
		
		wrapper.setCellHorizontalAlignment( previewScrollPanel, HasHorizontalAlignment.ALIGN_CENTER ) ;
		
		wrapper.setCellWidth( layerSelector, "220px" ) ;
		wrapper.setCellWidth( editPanel, "auto" ) ;
				
		layout.add( wrapper ) ;

		
		bind() ;

	
	}
	

	
	@Override
	public Widget asWidget() {
		return layout ;
	}
	
	
	
	private void bind() {

		toolbar.getAddTextButton().addClickHandler( new ClickHandler() {
			@Override
			public void onClick( ClickEvent e ) {
				newText() ;
			}
		}) ;

		
		toolbar.getGalleryImageMenuItem().addClickHandler( new ClickHandler() {
			@Override
			public void onClick( ClickEvent e ) {
				newGalleryImage() ;
			}
		}) ;
		
		toolbar.getGalleryLogoMenuItem().addClickHandler( new ClickHandler() {
			@Override
			public void onClick( ClickEvent e ) {
				newGalleryLogo() ;
			}
		}) ;
		
		

		toolbar.getUploadImageMenuItem().addClickHandler( new ClickHandler() {
			@Override
			public void onClick( ClickEvent e ) {
				newUploadImage() ;
			}
		}) ;
		
		
		
		toolbar.getDeleteButton().addClickHandler(new ClickHandler() {
			@Override
			public void onClick( ClickEvent e ) {
				deleteGraphic() ;
			}
		}) ;
		
		
		toolbar.getBringBackButton().addClickHandler(new ClickHandler() {
			@Override
			public void onClick( ClickEvent e ) {
				selectedLayerBack() ;
			}
		}) ;
		
		
		toolbar.getBringFrontButton().addClickHandler( new ClickHandler() {
			@Override
			public void onClick( ClickEvent e ) {
				selectedLayerFront() ;
			}
		}) ;
		
		
		toolbar.getApplyButton().addClickHandler( new ClickHandler() {
			@Override
			public void onClick( ClickEvent e ) {
				saveGraphics() ;
			}
		}) ;
	
	}

	
	protected HasClickHandlers getApplyButton (){
		return toolbar.getApplyButton() ;
	}

	
	protected HasClickHandlers getCancelButton (){
		return toolbar.getCancelButton() ;
	}
	

	public void setUnlockedImages( ArrayList<String> unlockedImages ) {
		this.unlockedImages = unlockedImages;
	}
	
	public void edit( final PropertySet set ) {
		
		/*
		Element canvas = Utils.selectDOM ( set.getSelectors () ).getItem ( 0 ) ;
		
		int width = canvas.getOffsetWidth() ;
		int height = canvas.getOffsetHeight() ;
		
		
		if ( this.canvasProperty != null 
				&& set.getUid().equals( canvasProperty.getUid() ) 
				&& canvasWidth == width
				&& canvasHeight == height 
		) {
		
			return ;
		}*/
		
		this.canvasProperty = set ;
		
		canvasWidth = ( Integer ) canvasProperty.get( "width" ) ;
		canvasHeight = ( Integer ) canvasProperty.get( "height" )  ;

		
		GetGraphics getGraphics = new GetGraphics() ;
		
		getGraphics.setDesignUid( Client.get().getDesign().getUid() ) ;
		getGraphics.setDrawableId ( set.getUid () ) ;
	
		//layout.setWidth ( canvasWidth + "px" ) ;
		
		preview.setPixelSize ( canvasWidth, canvasHeight ) ;	
	
		
		//layout.getFlexCellFormatter().setWidth ( 0, 0,  canvasWidth +  "px" ) ;
		//layout.getFlexCellFormatter().setHeight ( 0, 0, canvasHeight + "px" ) ;


		
		
		new RPC< GetGraphicsResponse >( Client.get().getDesignService() ).execute ( 
			getGraphics, new  CommandCallback< GetGraphicsResponse >() {

				@Override
				public void onSuccess ( GetGraphicsResponse response ) {
					initGraphics( response.getGraphics() ) ;
				}
		} ) ;
	
	}
	
	
	
	protected void initGraphics (  ArrayList<Graphic> graphics ){
		
		clear() ;
		
		this.graphics = graphics ; 
		
		Layer<? extends Graphic> firstLayer = null ;
			
		for ( Graphic graphic : graphics ){

			if ( !graphic.getVisible().booleanValue() ){
				continue;
			}
			
			if ( firstLayer == null ){
				
				firstLayer = addGraphicEditor( graphic ).getLayer() ;

				
			} else {
				
				addGraphicEditor( graphic ) ;
			}
			
		}
		
		if ( firstLayer != null ){
			firstLayer.select() ;
		}
		
	}
	
	
	
	@SuppressWarnings( "unchecked" )
	protected GraphicEditor<? extends Graphic> addGraphicEditor ( Graphic graphic ){
		
		final GraphicEditor<? extends Graphic> gEditor ;
		final LayerButton<? extends Graphic> layerButton ;
		
		
		
		switch ( graphic.getType () ) {
			
			
			case Graphic.IMAGE :
				gEditor= new ImageEditor( ( ImageGraphic ) graphic, null ) ;
				layerButton = new ImageLayerButton( ( Layer< ImageGraphic > ) gEditor.getLayer() ) ;
				
				
				break;
			/*
			case Graphic.TEXT :
				gEditor = new TextEditor( ( TextGraphic ) graphic ) ;
				layerButton = new TextLayerButton( ( Layer< TextGraphic > ) gEditor.getLayer() ) ;
				break;
	
			case Graphic.BG :
				gEditor = new BackgroundEditor( ( BackgroundGraphic ) graphic ) ;	
				layerButton = new BackgroundLayerButton( ( Layer< BackgroundGraphic > ) gEditor.getLayer() ) ;
				break;				
			*/	
			default :
				layerButton = null ;
				return null ;
		}
		
		
		layerSelector.add( layerButton ) ;
		gEditor.addHandler( layerButton ) ;
		
		layerButton.addClickHandler( new ClickHandler() {
			@Override
			public void onClick( ClickEvent event ) {
				gEditor.getLayer().select() ;				
			}
		} ) ;
		
		
		( ( Layer<Graphic > ) gEditor.getLayer() ).addLayerEventHandler( this ) ;

		
		int left =  graphic.getLeft () ;
		int top = graphic.getTop() ;
		
		if( Graphic.TEXT == graphic.getType () ){
			left = left - TextProperty.PADDING ;
			top = top - TextProperty.PADDING ;
		} 
		
		preview.add( ( ContentPanel ) gEditor.getLayer().asWidget () , 
				new AbsoluteData( left, top ) ) ;  
		
		
		preview.layout () ;
		
		return gEditor ;
		
	}
	
	
	/*
	protected void editGraphic ( GraphicEditor<? extends Graphic > editor ){
		
		if ( currentEditor != null ){
			currentEditor.getLayer().unselect() ;
		}
		
		Log.debug( "Edit layer " + editor.getLayer().getLabel() );		
		currentEditor = editor ;
		editPanel.show( currentEditor.asWidget() ) ;
		
	}*/
	

	protected void saveGraphics (){
		
		for ( Graphic graphic : graphics ){
			canvasProperty.getPropertyMap().remove( ( Property ) graphic ) ;
			canvasProperty.addProperty( ( Property ) graphic ) ;
		}
		
		
		Client.get().updateProperty( canvasProperty ) ;
		
	}
	
	
	
	
	private void clear () {
		if ( oldIE ) return ;
		preview.removeAll () ;
		editPanel.clear() ;
		layerSelector.clear() ;
	}
	
	
	
	@Override
	public void onLayerSelected( LayerEvent event ) {
		
		Layer< ? extends Graphic  > selectedLayer ;
		
		Graphic graphic ;
		
		
		if ( currentEditor != null ){
			
			selectedLayer = currentEditor.getLayer();
			selectedLayer.unselect() ;
			
			graphic = selectedLayer.getGraphic() ;
		
			if (  graphic.getType() != Graphic.BG ) {
				
				int left =  graphic.getLeft () ;
				int top = graphic.getTop() ;
				
				if( Graphic.TEXT == graphic.getType () ){
					left = left - TextProperty.PADDING ;
					top = top - TextProperty.PADDING ;
				} 
				
				
				( ( AbsoluteLayout ) preview.getLayout() ).setPosition( 
						( Component ) selectedLayer.asWidget(), left , top  ) ;
			}
			
		}
		
		
		selectedLayer = event.getLayer() ;
		
		currentEditor = selectedLayer.getEditor() ;
		
		editPanel.show( currentEditor.asWidget()) ;
		
		toolbar.enableDelete( ! currentEditor.getGraphic().isLocked() ) ;
		
		toolbar.enableBringBack( ! isLayerOnBack( selectedLayer ) && selectedLayer.isMoveable() ) ;
		
		toolbar.enableBringFront( ! isLayerOnFront( selectedLayer ) && selectedLayer.isMoveable() ) ;
		
		graphic = selectedLayer.getGraphic() ;
		
		
		if (  graphic.getType() != Graphic.BG ) {
			
			int left =  graphic.getLeft () ;
			int top = graphic.getTop() ;
			
			if( Graphic.TEXT == graphic.getType () ){
				left = left - TextProperty.PADDING ;
				top = top - TextProperty.PADDING ;
			} 
			
			( ( AbsoluteLayout ) preview.getLayout() ).setPosition( 
					( Component ) selectedLayer.asWidget(), left -2 , top - 2 ) ;
		}

		
		preview.layout( true ) ;
	
		
		
	}
	
	
	@Override
	public void onLayerMoved( LayerEvent event ) {
		
		int left = event.getLayer().getLeft() ;
		int top = event.getLayer().getTop() ;

		
		Graphic graphic = currentEditor.getGraphic() ;

		if ( Graphic.TEXT == graphic.getType() ){
			left = left + TextProperty.PADDING ;
			top = top + TextProperty.PADDING ;
		}
		
		graphic.setLeft( left ) ;		
		graphic.setTop( top ) ; 

	}


	@Override
	public void onLayerResized( LayerEvent event ) {
	}

	
	
	

	protected void ensureAddGalleryImageDialog() {
		
//		if ( addGalleryImageDialog == null ){
//			
//			addGalleryImageDialog = new AddGalleryImageDialog() ;
//			
////			addGalleryImageDialog.addHandler( new DesignResourceChangeHandler< ImageResource >() {
////				
////				@Override
////				public void onValueChange(ValueChangeEvent<ImageResource> event) {
////					createNewImageProperty ( event.getValue() ) ;
////					
////					addGalleryImageDialog.close() ;
////					
////				}
////			});
////			
//		}
		
//		addGalleryImageDialog.setUnlockedImages( unlockedImages ) ;
		
	}
	
	
	
	protected void newGalleryImage () {
		
		ensureAddGalleryImageDialog() ;
		
//		addGalleryImageDialog.openCenter() ;
//		addGalleryImageDialog.selectTag("all") ;
	}
	
	
	protected void newGalleryLogo () {
		
		ensureAddGalleryImageDialog() ;
		
//		addGalleryImageDialog.openCenter() ;
//		addGalleryImageDialog.selectTag("logo") ;
		
		
	}
	
	
	
	
	
	
	protected void newUploadImage() {
		
		final String uploadId = "" + System.currentTimeMillis () ;
		
		if ( addUploadImageDialog  == null ){
			
			addUploadImageDialog = new UploadImageDialog() ;
			addUploadImageDialog.setUploadUrl ( "dt/uploadImage" ) ;
			addUploadImageDialog.setUploadTip ( "<p style='font-size:0.9em'>GIF, JPEG or PNG image file<br/>400 kb max</p>" ) ;
			addUploadImageDialog.setUploadErrorMsg( UPLOAD_ERROR_MSG ) ;
			
			addUploadImageDialog.addUploadEventHandler( new UploadEventHandler() {
				@Override
				public void onUpload( UploadEvent uploadEvent) {
	
					if ( uploadEvent.isFailed() ) return ;
					
					createNewImageProperty( uploadEvent.getFileName () ) ;
					
				}
			}) ;
			
		}
		
		addUploadImageDialog.setUploadId( uploadId ) ;
		
		
		addUploadImageDialog.openCenter() ;
	}
	
	
	
	protected void createNewImageProperty ( String uploadFilename ) {
		
		
		NewImageGraphic action = new NewImageGraphic () ;
		action.setDesignUid( Client.get().getDesign().getUid() ) ;
		action.setUploadFilename ( uploadFilename ) ;
		
		new RPC< NewGraphicResponse< ImageGraphic > > ( Client.get().getDesignService () ) .execute (
				action, new NewGraphicDone< ImageGraphic >(){ 
					
					public void onSuccess ( NewGraphicResponse< ImageGraphic > response ) {
						
						ImageGraphic newImage = response.getGraphic() ;
						
						graphics.add( newImage ) ;
						
						addGraphicEditor ( newImage ).getLayer ().select() ;
					
					}
				}
		) ;
		
	}
	
	
	protected void createNewImageProperty ( ImageResource image ) {
		
		
		NewImageGraphic action = new NewImageGraphic () ;
		action.setDesignUid( Client.get().getDesign().getUid() ) ;
		
		action.setImage ( image ) ;
		
		new RPC< NewGraphicResponse< ImageGraphic > > ( Client.get().getDesignService () ) .execute (
				action, new NewGraphicDone<ImageGraphic>(){ 
					
					public void onSuccess ( NewGraphicResponse<ImageGraphic> response ) {
						
						ImageGraphic newImage = response.getGraphic() ;
						
						graphics.add( newImage ) ;
						
						addGraphicEditor ( newImage ).getLayer ().select() ;
					
					}
				}
		) ;
		
	}
	

	
	protected void newText () {
		
		
		NewTextGraphic action = new NewTextGraphic();
		action.setDesignUid( Client.get().getDesign().getUid() ) ;
		
		
		new RPC< NewGraphicResponse< TextGraphic > > ( Client.get().getDesignService() ) .execute (
				action, new NewGraphicDone< TextGraphic >(){ 
					public void onSuccess ( NewGraphicResponse<TextGraphic> response ) {
						
						TextGraphic newText = response.getGraphic() ;
						
						graphics.add( newText ) ;
						
						addGraphicEditor ( newText ).getLayer ().select() ;
						
					}
				}
		) ;
	}
	
	
	protected void deleteGraphic( ){
		
		if ( currentEditor == null ) {
			Utils.alert( "Please, select the element to delete." ) ;
			return ;
		}
		
		
		//layerSelector.remove( currentEditor.getLayer().getSelector() ) ;
		preview.remove( currentEditor.getLayer().asWidget() ) ;
		
		//editPanelHeading.setHTML("") ;
		
		
		preview.layout( true ) ;
		
		//graphics.remove( currentEditor.getGraphic() ) ;
		
		//canvasProperty.getPropertyMap ().remove ( ( Property ) currentEditor.getGraphic() ) ;
		( ( Property ) currentEditor.getGraphic() ).setEnable( new Boolean ( false ) ) ;
 		
		toolbar.enableDelete( false ) ;
		toolbar.enableBringBack( false ) ;
		toolbar.enableBringFront( false ) ;
		
		currentEditor.remove();
		currentEditor = null ;
		
	}
	
	protected boolean hasBackground () {
		return bgGraphic != null ; 
	}
	
	protected Layer<? extends Graphic> getSelectedLayer() {
		return ( ( currentEditor == null ) ? null : currentEditor.getLayer() ) ;
	}
	
	protected < GRAPHIC extends Graphic > boolean isLayerOnFront( Layer< GRAPHIC > lyr ) {
		return  preview.indexOf ( ( Component ) lyr.asWidget() )  == preview.getItemCount() -1  ;		
	
	}
	
	protected < GRAPHIC extends Graphic >  boolean isLayerOnBack( Layer< GRAPHIC > lyr ) {
		int zindex = preview.indexOf ( ( Component ) lyr.asWidget() ) ;
		return hasBackground() && zindex == 1  
			|| ! hasBackground() && zindex == 0 ;
	}
	
	
	protected void selectedLayerFront () {
		
		Layer<? extends Graphic> lyr = getSelectedLayer () ;
		
		if( lyr == null ) return ;
		
		Graphic graphic = currentEditor.getGraphic() ;
		
		
		if ( ! graphic.isMoveable () || isLayerOnFront ( lyr ) ) return ;
		
		int i = preview.indexOf ( ( Component ) lyr.asWidget() ) ;
		
		graphics.remove( graphic ) ;
		
		graphics.add( i + 1 , graphic ) ;
		
		
		preview.remove ( lyr.asWidget() ) ;
	
		preview.insert ( lyr.asWidget(), i + 1 , new AbsoluteData( graphic.getLeft () - 2, graphic.getTop () - 2 ) ) ;
		
		preview.layout ( true ) ;
		
		toolbar.enableBringBack( ! isLayerOnBack( lyr ) && lyr.isMoveable() ) ;
		
		toolbar.enableBringFront( ! isLayerOnFront( lyr ) && lyr.isMoveable() ) ;
		
	}

	
	protected void selectedLayerBack () {

		Layer<? extends Graphic> lyr = getSelectedLayer () ;
		
		if( lyr == null ) return ;
		
		Graphic graphic = currentEditor.getGraphic() ;
		
		if ( ! graphic.isMoveable () || isLayerOnBack ( lyr ) )  return ;
		
		int i = preview.indexOf ( ( Component ) lyr.asWidget() ) ;
		
		graphics.remove( graphic ) ;
		
		graphics.add( i - 1 , graphic ) ;
		
		preview.remove ( lyr.asWidget() ) ;
		
		preview.insert (  lyr.asWidget(), i - 1, new AbsoluteData( graphic.getLeft () - 2  , graphic.getTop () - 2 ) ) ;

		preview.layout ( true ) ;
		
		toolbar.enableBringBack( ! isLayerOnBack( lyr ) && lyr.isMoveable() ) ;
		
		toolbar.enableBringFront( ! isLayerOnFront( lyr ) && lyr.isMoveable() ) ;
			
		
	}


	public void cancel() {
		canvasProperty = null ;
		clear() ;
		currentEditor = null ;

	}



	@Override
	public void onLayerRenamed(LayerEvent event) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void onLayerDeleted(LayerEvent event) {
		// TODO Auto-generated method stub
		
	}
	
	
	
	

	
}
