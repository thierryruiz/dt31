package com.dotemplate.core.client.canvas;

import com.dotemplate.core.client.events.PropertyChangeEvent;

import com.dotemplate.core.shared.canvas.ImageGraphic;


public class ImageLayerButton extends LayerButton< ImageGraphic > {

	public ImageLayerButton( Layer< ImageGraphic > layer) {
		super( layer );
	}

	@Override
	public void onPropertyChanged( PropertyChangeEvent e ) {
	}

	
}
