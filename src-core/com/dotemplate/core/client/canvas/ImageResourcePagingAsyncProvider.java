package com.dotemplate.core.client.canvas;

import com.dotemplate.core.client.widgets.resource.DesignResourcePageAsyncProvider;
import com.dotemplate.core.shared.canvas.ImageResource;
import com.dotemplate.core.shared.command.GetDesignResources;
import com.dotemplate.core.shared.command.GetDesignResourcesTags;
import com.dotemplate.core.shared.command.GetImageResourceTags;
import com.dotemplate.core.shared.command.GetImageResources;

@Deprecated
public class ImageResourcePagingAsyncProvider extends DesignResourcePageAsyncProvider< ImageResource > {
	
	
	protected GetImageResources getImagesCommand;
	
	protected GetImageResourceTags getImageResourcesTagsCommand;
	
	
	
	public void setTag( String tag ) {
		this.tag = tag;
	}
	
	
	@Override
	protected GetDesignResources<ImageResource> getDesignResourcesCommand() {

		if ( getImagesCommand == null ) {
			getImagesCommand = new GetImageResources();
		}

		getImagesCommand.setTag ( tag ) ;

		return getImagesCommand;

	}


	@Override
	protected GetDesignResourcesTags< ImageResource > getDesignResourcesTagsCommand() {
		if ( getImageResourcesTagsCommand == null ) {
			getImageResourcesTagsCommand = new GetImageResourceTags();
		}

		return getImageResourcesTagsCommand;
	}

}
