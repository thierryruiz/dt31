package com.dotemplate.core.client.canvas;


import com.extjs.gxt.ui.client.Style.ButtonArrowAlign;
import com.extjs.gxt.ui.client.Style.ButtonScale;
import com.extjs.gxt.ui.client.Style.HorizontalAlignment;
import com.extjs.gxt.ui.client.Style.IconAlign;
import com.extjs.gxt.ui.client.event.MenuEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.util.IconHelper;
import com.extjs.gxt.ui.client.widget.menu.Menu;
import com.extjs.gxt.ui.client.widget.toolbar.ToolBar;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.AbstractImagePrototype.ImagePrototypeElement;



public class CanvasEditorToolbar extends HorizontalPanel {

	final static int HEIGHT = 73 ;

	private ToolBar editToolbar ;
	
	private ToolBar exitToolbar ;
	
	private Button addTextIcon ;
	
	private Button addImageIcon ;
	
	private MenuItem uploadImageMenuItem ;
	
	private MenuItem galleryImageMenuItem ;
	
	private MenuItem galleryLogoMenuItem ;
	
	private Button deleteIcon ;
	
	private Button bringFrontIcon ;
	
	private Button bringBackIcon ;
	
	private Button applyIcon ;
	
	private Button cancelIcon ;
	
	
	CanvasEditorToolbar ( ){
	
		DOM.setElementAttribute( getElement(), "id", "ez-canvas-toolbar" ) ;
		
		
		setWidth( "100%" ) ;
		editToolbar = new ToolBar() ;
		editToolbar.setHeight( HEIGHT ) ;

		editToolbar.add( addTextIcon = new Button ( "Add Text", "add-text.png" ) ) ;
		editToolbar.add( addImageIcon = new Button ( "Add Image", "add-img.png" ) ) ;
		
		editToolbar.add( bringFrontIcon = new Button ( "Bring front", "bring-front.png" ) ) ;
		editToolbar.add( bringBackIcon = new Button ( "Bring back", "bring-back.png" ) ) ;
		
		editToolbar.add( deleteIcon = new Button ( "Delete", "delete.png" ) ) ;
		
		Menu addImageMenu = new Menu();
		addImageIcon.setMenu ( addImageMenu ) ;
		addImageIcon.setArrowAlign( ButtonArrowAlign.BOTTOM );  
		
		
    	uploadImageMenuItem = new MenuItem( "Upload from your computer..." ) ;
    	galleryImageMenuItem = new MenuItem( "Choose from image gallery..." ) ;
    	galleryLogoMenuItem = new MenuItem( "Choose from logo gallery..." ) ;    	
		
    	addImageMenu.add( uploadImageMenuItem ) ;
    	addImageMenu.add( galleryImageMenuItem ) ;
    	addImageMenu.add( galleryLogoMenuItem ) ;
    	
		exitToolbar = new ToolBar() ;
		exitToolbar.setHeight( HEIGHT ) ;

		exitToolbar.add( applyIcon = new Button ( "Apply", "apply.png" ) ) ;
		exitToolbar.add( cancelIcon = new Button ( "Cancel", "cancel.png" ) ) ;    	
    	exitToolbar.setAlignment( HorizontalAlignment.RIGHT ) ;
	
		add ( editToolbar ) ;
		add ( exitToolbar ) ;
		
		setCellHorizontalAlignment( editToolbar, HorizontalPanel.ALIGN_LEFT ) ;
		setCellHorizontalAlignment( exitToolbar, HorizontalPanel.ALIGN_RIGHT ) ;
		
		setCellWidth( editToolbar, "70%" ) ;
		setCellWidth( exitToolbar, "30%" ) ;
		
		
	}

	
	public HasClickHandlers getAddTextButton(){
		return addTextIcon ;
	}
	
	
	public HasClickHandlers getGalleryImageMenuItem() {
		return galleryImageMenuItem ;
	}
	

	public HasClickHandlers getGalleryLogoMenuItem() {
		return galleryLogoMenuItem ;
	}
	
	
	public HasClickHandlers getUploadImageMenuItem() {
		return uploadImageMenuItem ;
	}
		
	
	public HasClickHandlers getDeleteButton() {
		return deleteIcon ;
	}
	

	public HasClickHandlers getBringFrontButton() {
		return bringFrontIcon ;
	}
	

	public HasClickHandlers getBringBackButton() {
		return bringBackIcon ;
	}
	
	public HasClickHandlers getApplyButton() {
		return applyIcon ;
	}
	
	public HasClickHandlers getCancelButton() {
		return cancelIcon ;
	}
	
	
	
	
	public void enableBringFront( boolean e ) {
		if ( e )
			bringFrontIcon.enable ();
		else 
			bringFrontIcon.disable ();
	}
	
	
	public void enableBringBack( boolean e ) {
		if ( e )
			bringBackIcon.enable ();
		else 
			bringBackIcon.disable ();
	}
	
	
	public void enableDelete( boolean e ) {
		if ( e )
			deleteIcon.enable ();
		else 
			deleteIcon.disable ();
	}
	

	class Button extends com.extjs.gxt.ui.client.widget.button.Button implements HasClickHandlers {
		
		protected ImagePrototypeElement iconElement ;
		
		
		public Button( String label, String icon ) {
			
			setScale( ButtonScale.LARGE ) ;
			setIconAlign( IconAlign.TOP ) ;
			setHeight( HEIGHT ) ;
			
			setText( label ) ;

			setIcon( IconHelper.createPath( "client/icons/CanvasEditorToolbar/" + icon, 32, 32 ) ) ;
			
		}
		
		
		@Override
		public HandlerRegistration addClickHandler( ClickHandler handler ) {
			return addHandler( handler, ClickEvent.getType() );
		}
		
		
		public ImagePrototypeElement getIconElement() {
			return iconElement;
		}
		
	}
	
	
	class MenuItem extends com.extjs.gxt.ui.client.widget.menu.MenuItem implements HasClickHandlers {
		
		MenuItem ( String text ){
			super( text ) ;
			addSelectionListener( new SelectionListener<MenuEvent>() {  
				public void componentSelected( MenuEvent ev ) {			
					fireEvent( new ClickEvent(){
			    		  public Object getSource() {
				    			return MenuItem.this;
				    		}  
					}) ;
			      }
			});
		}

		@Override
		public HandlerRegistration addClickHandler(ClickHandler handler ) {
			return addHandler ( handler, ClickEvent.getType () );
		}
		
	}
	
	
	
}
