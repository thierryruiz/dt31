package com.dotemplate.core.client.canvas;

import com.dotemplate.core.client.canvas.event.LayerEvent;
import com.dotemplate.core.client.canvas.event.LayerEventHandler;
import com.dotemplate.core.shared.canvas.BackgroundGraphic;

import com.google.gwt.event.shared.HandlerRegistration;


public class BackgroundLayer extends Layer< BackgroundGraphic > {
	
	public BackgroundLayer () {
		super() ;
	}
	
	
	@Override
	public boolean isMoveable () {
		return false;
	}

	
	@Override
	public String getLabel() {
		
		String label = getGraphic().getLabel() ;
		
		if ( label != null && label.length() != 0 ){
			return label ;
		}
				
		return "Background" ;
	}
	


	
	public void refresh ( String url, int rWidth, int rHeight ) {
		img.setUrl ( url );			
	}

	public void select() {
		if ( selected ) return ; 
		selected = true ;
		//fireSelected() ;
		//selector.select() ;	
	}
	
	
	public void unselect(){
		if ( !selected ) return ; 
		selected = false ;
		//selector.unselect() ;
	}
	
	
}
