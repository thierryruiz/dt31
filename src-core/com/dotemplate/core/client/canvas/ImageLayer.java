package com.dotemplate.core.client.canvas;

import com.dotemplate.core.shared.canvas.ImageGraphic;


public class ImageLayer extends Layer< ImageGraphic > {
		

	public ImageLayer() {
	}

	
	public boolean isMoveable () {
		return true ;
	}


	@Override
	public String getLabel() {
		return editor.getGraphic().getLabel() ;
	}

	
}
