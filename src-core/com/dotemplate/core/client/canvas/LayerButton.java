package com.dotemplate.core.client.canvas;

import com.dotemplate.core.client.events.PropertyChangeEventHandler;
import com.dotemplate.core.client.frwk.Utils;
import com.dotemplate.core.shared.canvas.Graphic;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.Widget;



public abstract class  LayerButton < G extends Graphic > implements IsWidget, HasClickHandlers, PropertyChangeEventHandler, LayerLabelDialog.Callback {
	
	protected HorizontalPanel layout ; 

	protected Layer< G > layer ;

	protected HTML label ;
	
	protected boolean selected ;
	
	
	LayerButton( Layer < G > layer ){
	
		this.layer = layer ;
		//this.layer.setSelector ( this ) ;
		this.selected = false ;
		
		layout = new HorizontalPanel() ;
		
		layout.add( label = new HTML() ) ;
	
		
		PushButton editBtn = new PushButton(  new Image ( "client/icons/LayerButton/edit.png" ) ) ;
		
		
		DOM.setStyleAttribute ( editBtn.getElement (), "cursor", "pointer" ) ;
		editBtn.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				editLabel() ;
			}
		});
		
		layout.add( editBtn ) ;
		
		
		
		setButtonText() ;

		Utils.alignMiddle( layout, label ) ;
		Utils.alignMiddle( layout, editBtn ) ;

		Utils.alignRight( layout, editBtn ) ;
		Utils.marginRight( editBtn, 7 ) ;
		
		layout.setStyleName( "ez-canvas-layer-button" ) ;
		
		
	}
	
	
	protected void editLabel() {
		LayerLabelDialog.open( layer.getLabel(), this ) ;
	}


	
	@Override
	public Widget asWidget() {
		return layout ;
	}
	
	
	protected void setButtonText() {
		
		String lbl = layer.getLabel() ;
		
		if ( lbl.length() > 21 ){
			lbl = lbl.substring( 0, 20 ) + "..." ;
 		}
		
		label.setHTML( lbl ) ;

	}
	
	
	
	void unselect() {
		if ( !selected ) return ;
		layout.removeStyleName( "active" ) ;
		selected = false ;
	}

	
	void select() {
		if ( selected ) return ;
		layout.addStyleName( "active" ) ;
		selected = true ;
	}
	
	
	@Override
	public HandlerRegistration addClickHandler(ClickHandler handler) {
		return label.addClickHandler( handler );
	}
	
	
	@Override
	public void fireEvent(GwtEvent<?> event) {
		label.fireEvent( event ) ;
	}
	
	
	@Override
	public void onOk(String label ) {
		layer.getGraphic().setLabel( label ) ;
		setButtonText() ;
	}
	
	
}
