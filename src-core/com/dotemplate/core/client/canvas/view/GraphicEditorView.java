package com.dotemplate.core.client.canvas.view;

import com.dotemplate.core.client.frwk.View;
import com.dotemplate.core.shared.canvas.Graphic;

public interface GraphicEditorView< GRAPHIC extends Graphic > extends View {

}
