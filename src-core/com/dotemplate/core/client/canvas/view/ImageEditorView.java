package com.dotemplate.core.client.canvas.view;

import com.dotemplate.core.shared.canvas.ImageGraphic;
import com.google.gwt.user.client.ui.HasValue;



public interface ImageEditorView extends GraphicEditorView< ImageGraphic > {
	
	HasValue< Double > getOpacityControl() ;
	
	HasValue< Double > getLeftFadingControl() ;
	
	HasValue< Double > getRightFadingControl() ;
	
	HasValue< Double > getTopFadingControl() ;
	
	HasValue< Double > getBottomFadingControl() ;
	
	HasValue< Double > getBlurControl() ;
	
	
}
