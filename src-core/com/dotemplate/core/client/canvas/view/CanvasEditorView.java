package com.dotemplate.core.client.canvas.view;

import com.dotemplate.core.client.canvas.Layer;
import com.dotemplate.core.client.canvas.LayerButton;
import com.dotemplate.core.client.canvas.presenter.GraphicEditor;
import com.dotemplate.core.client.frwk.View;
import com.dotemplate.core.shared.canvas.Graphic;
import com.google.gwt.event.dom.client.HasClickHandlers;

public interface CanvasEditorView extends View {
	
	
	void show();
	
	void hide();
	
	void setCanvasSize( int width, int height ) ;

	void clear();
	
	void addLayer( Layer<? extends Graphic > layer, int left, int top  ) ;
	
	@Deprecated
	void addLayerButton( LayerButton<? extends Graphic > layerButton ) ;

	void addLayerEditor( GraphicEditor<? extends Graphic> editor ) ;
	
	void setEditor(GraphicEditor<? extends Graphic> currentEditor );
	
	HasClickHandlers getAddGalleryImageControl() ;
	
	HasClickHandlers getAddFlickrImageControl() ;
	
	HasClickHandlers getUploadImageControl() ;
	
	HasClickHandlers getAddLogoControl() ;
	
	HasClickHandlers getAddTextControl() ;

	HasClickHandlers getDeleteControl() ;
	
	HasClickHandlers getMoveForwardControl() ;
	
	HasClickHandlers getMoveBackwardControl() ;

	void enableDeleteControl(boolean enable);
	
	void enableMoveBackwardControl(boolean enable);
	
	void enableMoveForwardControl(boolean enable);

	void removeLayerEditor(GraphicEditor<? extends Graphic> currentEditor);

	int indexOfLayer(Layer<? extends Graphic> layer);

	void removeLayer(Layer<? extends Graphic> layer);

	void insertLayer(Layer<? extends Graphic> layer, int i, int j, int k);

	boolean isLayerOnFront(Layer<? extends Graphic> layer);
	
	
	
	

	
	
}

