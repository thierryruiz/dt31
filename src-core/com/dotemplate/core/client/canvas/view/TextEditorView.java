package com.dotemplate.core.client.canvas.view;

import com.dotemplate.core.shared.canvas.TextGraphic;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.Widget;



public interface TextEditorView extends GraphicEditorView< TextGraphic > {
	
	HasValue< String > getTextInput() ;
	
	boolean validateTextInput() ;
	
	HasValue< Integer > getFontSizeSelector() ;
	
	HasClickHandlers getFontFamilyControl() ; 
	
	void setSubEditorViews( Widget subEditors ) ;

	void setStyleEditorView( Widget styleEditor );

	
}
