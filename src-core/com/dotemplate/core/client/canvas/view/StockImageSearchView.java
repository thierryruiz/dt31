package com.dotemplate.core.client.canvas.view;

import java.util.ArrayList;

import com.dotemplate.core.client.apis.StockImage;
import com.dotemplate.core.client.canvas.event.HasSelectStockImageHandlers;
import com.dotemplate.core.client.frwk.View;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;

public interface StockImageSearchView extends View, HasSelectStockImageHandlers {
	
	HasValueChangeHandlers< String > getSearchInput() ;
	
	HasClickHandlers getMoreImageControl() ;
	
	void showMoreImageControl( boolean show ) ;
	
	String getSearchSring() ;
	
	void setSearchPlaceHolder( String placeholder ) ; 
	
	void setPageImages( ArrayList < StockImage > images ) ;
	
	void clearImages() ;
	
	
}
