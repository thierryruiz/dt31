package com.dotemplate.core.client.canvas.view;


import com.dotemplate.core.client.frwk.View;
import com.dotemplate.core.shared.canvas.ImageResource;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;


public interface NewImageView extends View, HasValueChangeHandlers< ImageResource > {
	
	enum InitialState { GALLERY_IMAGE, FLICKR_IMAGE, LOGO }
	
	void show( InitialState state ) ;
	
	void hide() ;

	void setImageGalleryView( ImageGalleryView view) ;
	
	void setFlickImageView ( StockImageSearchView view ) ;
	
	
}

