package com.dotemplate.core.client.canvas.view;

import java.util.ArrayList;

import com.dotemplate.core.client.frwk.View;
import com.dotemplate.core.shared.canvas.ImageResource;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.HasValue;

public interface ImageGalleryView extends View, HasValue< ImageResource > {
	
	void setTags( String[] tags  ) ;
	
	void selectTag ( String tag ) ;
	
	void clearResults() ;
	
	public HandlerRegistration addSelectTagHandler( ChangeHandler handler ) ;

	String getSelectedTag();

	void setUnlockedImages(ArrayList<String> unlockedImages);
	
	
}
