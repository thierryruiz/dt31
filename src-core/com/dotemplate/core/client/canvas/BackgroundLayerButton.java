package com.dotemplate.core.client.canvas;

import com.dotemplate.core.client.events.PropertyChangeEvent;
import com.dotemplate.core.shared.canvas.BackgroundGraphic;


public class BackgroundLayerButton extends LayerButton< BackgroundGraphic > {

	public BackgroundLayerButton( Layer< BackgroundGraphic > layer) {
		super( layer );
	}

	@Override
	public void onPropertyChanged(PropertyChangeEvent e) {
	}



}
