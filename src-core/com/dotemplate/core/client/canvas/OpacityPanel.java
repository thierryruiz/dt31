package com.dotemplate.core.client.canvas;


import com.dotemplate.core.client.widgets.PercentSlider;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.HasChangeHandlers;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;

@Deprecated
public class OpacityPanel extends VerticalPanel implements HasChangeHandlers {

	private PercentSlider slider ;
	
	private Image icon ;
		

	public OpacityPanel ( ) {
				
		setSpacing ( 25 ) ;
		
		add ( icon =  new Image( "client/icons/OpacityPanel/opacity.jpg" ) ) ;
		
		setCellHorizontalAlignment ( icon ,  VerticalPanel.ALIGN_CENTER) ;
		setCellVerticalAlignment ( icon ,  VerticalPanel.ALIGN_MIDDLE) ;
		
		

		Label title = new Label( "Image transparency" ) ;
		
		add ( title ) ;
		add (  slider = new PercentSlider( 130 ) )  ;
		

		setCellHorizontalAlignment ( title,  VerticalPanel.ALIGN_CENTER ) ;		
		setCellHorizontalAlignment ( slider,  VerticalPanel.ALIGN_CENTER ) ;
		
		setWidth ( "250px" ) ;
		
	}
	
	
	public float getOpacity( ) {
		return 1 - slider.getFloatValue () ;
	}
	
	
	public void setOpacity ( float opacity ){
		slider.setFloatValue( 1 - opacity ) ;
	}
	
	
	
	@Override
	public HandlerRegistration addChangeHandler ( ChangeHandler handler ) {
		return slider.addChangeHandler ( handler ) ;
	}
	
	
}
