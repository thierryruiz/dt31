package com.dotemplate.core.client.canvas;

import com.dotemplate.core.client.frwk.Utils;
import com.dotemplate.core.client.widgets.TextBox;
import com.dotemplate.core.client.widgets.Window;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;


public class LayerLabelDialog extends Window {

	protected Label label ;
	
	protected TextBox input ; 
				
	protected Callback callback ;
	
	private static LayerLabelDialog instance ;
	
	
	public LayerLabelDialog() {
		
		setHeading( "Change layer name" ) ;
		
		VerticalPanel root = new VerticalPanel() ;
		
		root.add( label = new Label( "Layer name" ) ) ;
		root.add( input = new TextBox() ) ;
		
		input.setWidth( 200 ) ;
		input.setMaxLength( 30 ) ;
		
        input.addKeyPressHandler ( new KeyPressHandler() {
        	@Override
        	public void onKeyPress ( KeyPressEvent keyEvt ) {   
        		if ( keyEvt.getNativeEvent().getKeyCode() == KeyCodes.KEY_ENTER ) {
        			apply() ;
        			keyEvt.stopPropagation () ;
        		}
        	}
        });
		
		add( root ) ;
		
		addOkButton().addClickHandler( new ClickHandler() {
			@Override
			public void onClick( ClickEvent e ) {
				apply() ;
			}
		}) ;
		
		addCancelButton().addClickHandler( new ClickHandler() {
			@Override
			public void onClick( ClickEvent e ) {
				close() ;
			}
		}) ;
	
	}
	
	
	protected void apply() {
		if ( ! validate() ) {
			Utils.alert( "Please enter a valid layer name." ) ;
		} else {
			close() ;
			callback.onOk(  input.getText() ) ;
		}
	}
	
	
	protected boolean validate() {
		return true ;
	}

	
	

	public static void open( String layerName, Callback callback ) {
		if ( instance == null ) {
			instance = new LayerLabelDialog() ;
		}
		
		instance.callback = callback ;
		instance.input.setText( layerName ) ;
		instance.openCenter() ;
	}

		

	
	
	
	public interface Callback {
		
		void onOk ( String value ) ; 
		
	}
	
	
	
}
