package com.dotemplate.core.client.canvas;


import com.dotemplate.core.shared.canvas.TextGraphic;


public class TextLayer extends Layer< TextGraphic > {

	
	public TextLayer () {
	}
	
	
	
	@Override
	protected void makeResizeable() {
		// not resizeable
	}
	
	
	public boolean isMoveable () {
		return true;
	}
	
	@Override
	public String getLabel() {
	
		String label = getGraphic().getLabel() ;
		
		if ( label != null && label.length() != 0 ){
			return label ;
		}
				
		return getGraphic().getText();
		
	}
	

	
}
