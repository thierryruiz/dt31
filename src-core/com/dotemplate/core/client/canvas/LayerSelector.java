package com.dotemplate.core.client.canvas;


import com.dotemplate.core.client.widgets.ContainerPanel;
import com.dotemplate.core.shared.canvas.Graphic;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.Widget;

public class LayerSelector implements IsWidget {

	protected ContainerPanel container ;
	
	protected ScrollPanel scroller ;
	
	protected LayerSelectorList list ;
	
	
	public LayerSelector() {
		container = new ContainerPanel( "Layers" ) ;		
		container.setWidget ( scroller = new ScrollPanel() ) ;
		
		scroller.setStyleName( "ez-canvas-layer-selector" ) ;
		scroller.add( list = new LayerSelectorList() ) ;
	}
	
	
	void add( LayerButton<? extends Graphic > layerButton ){
		list.insert( layerButton.asWidget(), 0  ) ;
	}

	
	void clear() {
		list.clear() ;
	}

	
	void remove ( LayerButton< ? extends Graphic > layerButton ) {
		list.remove( layerButton.asWidget() ) ;
	}

	
	@Override
	public Widget asWidget() {
		return container.asWidget();
	}
			
	
	private class LayerSelectorList extends FlowPanel {
		
	}





	

}
