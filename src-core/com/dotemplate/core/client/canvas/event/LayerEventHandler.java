package com.dotemplate.core.client.canvas.event;



import com.google.gwt.event.shared.EventHandler;


public interface LayerEventHandler extends EventHandler {
	
	void onLayerSelected( LayerEvent event ) ;
	
	void onLayerMoved( LayerEvent event ) ;
	
	void onLayerResized( LayerEvent event ) ;
	
	void onLayerRenamed( LayerEvent event ) ;
	
	void onLayerDeleted( LayerEvent event ) ;
	
}
