package com.dotemplate.core.client.canvas.event;


public class LayerEventAdapter implements LayerEventHandler {
	
	@Override
	public void onLayerMoved(LayerEvent event) {
	}
	
	@Override
	public void onLayerResized(LayerEvent event) {
	}
	
	@Override
	public void onLayerSelected(LayerEvent event) {
	}

	@Override
	public void onLayerRenamed(LayerEvent event) {
		
	}

	@Override
	public void onLayerDeleted(LayerEvent event) {
	}
	
}
