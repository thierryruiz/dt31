package com.dotemplate.core.client.canvas.event;

import com.dotemplate.core.client.canvas.Layer;
import com.dotemplate.core.client.frwk.Event;




public class LayerEvent extends Event< LayerEventHandler > {

	public static final Type < LayerEventHandler> TYPE = new Type< LayerEventHandler >();
	
	public enum EventType { SELECTED, MOVED , RESIZED, RENAMED, DELETED } ;
	
	private EventType eventType ;
		
	private Layer< ? > layer ;
	
	
	public LayerEvent( Layer< ? > layer, EventType type ) {
		this.eventType = type ;
		this.layer = layer ; 
	}
	
	
	public Layer< ? > getLayer () {
		return layer;
	}
	

	@Override
	protected void dispatch ( LayerEventHandler handler ) {
		switch ( eventType ){
			case SELECTED : handler.onLayerSelected ( this ) ; return ;
			case MOVED : handler.onLayerMoved ( this ) ; return ;
			case RESIZED : handler.onLayerResized ( this ) ; return ;
			case RENAMED : handler.onLayerRenamed ( this ) ; return ;
			case DELETED : handler.onLayerDeleted ( this ) ; return ;
			default:return;
		}
	}


	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<LayerEventHandler> getAssociatedType() {
		return TYPE;
	}
	
	
}
