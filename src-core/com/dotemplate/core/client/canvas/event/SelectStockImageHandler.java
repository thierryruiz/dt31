package com.dotemplate.core.client.canvas.event;


import com.dotemplate.core.client.apis.StockImage;
import com.dotemplate.core.client.apis.StockImageDetails;
import com.google.gwt.event.shared.EventHandler;


public interface SelectStockImageHandler extends EventHandler {

	void onStockImageSelected ( StockImage image, StockImageDetails details ) ;
	
	
}
