package com.dotemplate.core.client.canvas.event;

import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.event.shared.HasHandlers;


public interface HasSelectStockImageHandlers extends HasHandlers {
	
	HandlerRegistration addHandler ( SelectStockImageHandler h ) ; 
	
}
