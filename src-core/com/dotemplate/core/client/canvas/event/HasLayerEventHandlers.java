package com.dotemplate.core.client.canvas.event;


import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.event.shared.HasHandlers;


public interface HasLayerEventHandlers extends HasHandlers {
	
	HandlerRegistration addLayerEventHandler ( LayerEventHandler h ) ; 
	
	
}
