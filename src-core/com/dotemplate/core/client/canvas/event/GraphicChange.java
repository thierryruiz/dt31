package com.dotemplate.core.client.canvas.event;

import com.dotemplate.core.client.frwk.Event;


public class GraphicChange extends Event < GraphicChangeHandler >{
	
	public static final Type < GraphicChangeHandler> TYPE = new Type< GraphicChangeHandler >();
	
	private boolean renderNeeded = false ;
	
	
	public GraphicChange ( boolean renderNeeded ) {
		this.renderNeeded = renderNeeded ;
	}

	protected void dispatch( GraphicChangeHandler handler ) {
		handler.onGraphicChange ( this ) ;
	}
	
	
	public boolean isRenderNeeded () {
		return renderNeeded;
	}

	@Override
	public Type<GraphicChangeHandler> getAssociatedType () {
		return TYPE ;
	}
	
	

}
