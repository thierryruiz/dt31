package com.dotemplate.core.client.canvas.event;

import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.event.shared.HasHandlers;


public interface HasGraphicChangeHandlers extends HasHandlers {
	
	HandlerRegistration addGraphicChangeHandler ( GraphicChangeHandler h ) ; 
	
}
