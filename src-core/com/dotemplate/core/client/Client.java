package com.dotemplate.core.client;



import com.dotemplate.core.client.frwk.Utils;
import com.dotemplate.core.client.resources.css.CSSBundles;
import com.dotemplate.core.shared.Design;
import com.dotemplate.core.shared.EditService;
import com.dotemplate.core.shared.EditServiceAsync;
import com.dotemplate.core.shared.properties.CanvasProperty;
import com.dotemplate.core.shared.properties.Property;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.HandlerManager;


public abstract class Client<D extends Design> implements EntryPoint {
	
	protected HandlerManager handlerManager ;

	private D design ;
	
	protected EditServiceAsync designService ;

	private static Client<? extends Design> instance ; 

	
	public static void systemError ( Object source, String msg ) {
		Utils.alert ( msg );
	}

	public static void systemError ( Throwable t ) {
		Utils.alert ( t.getMessage () );
	}

	
	public Client() {
		
		instance = this ;

		Utils.useStylesheet(CSSBundles.INSTANCE.getCoreEditorCss() );
		
		handlerManager = new HandlerManager( this ) ;

		designService = ( EditServiceAsync ) GWT.create ( EditService.class ) ;
		
	}
	
	
	public D getDesign() {
		if ( design == null ){
			throw new IllegalStateException( "Undefined design" ) ;
		}
		return design;
	}

	
	public void setDesign( D design ) {
		this.design = design;
	}
	
	
	// --------------------------- STATIC METHODS ---------------------------------------


	public EditServiceAsync getDesignService() {
		return designService;
	}
	
	
	public static Client<? extends Design> get() {
		return instance ;
	}
	
	
	public abstract void editCanvas ( CanvasProperty set )  ;

	
	public abstract void updateProperty( Property property ) ;

	
	
	public void addUserColor( String color ) {
		
		String userScheme = design.getUserScheme() ;
		
		if ( userScheme == null ){
			design.setUserScheme( color ) ;
			return ;
		}
		
		if( userScheme.contains( color ) ){
			return ;
		}
		
		design.setUserScheme( userScheme + "," + color ) ;
		
	}


	public abstract void invitePurchase() ;
	
	
}
